ALTER TABLE `db_uav_maintenance`.`companies`
    ADD COLUMN `activate` tinyint NULL DEFAULT 0 COMMENT '维修点启用状态（1-启用，0-禁用）';

CREATE TABLE `templates` (
                             `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
                             `tid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板内部id',
                             `name` varchar(255) NOT NULL COMMENT '模板名称',
                             `code` varchar(255) NOT NULL COMMENT '模板标识',
                             `description` varchar(255) NOT NULL COMMENT '模板描述',
                             `created_at` bigint NOT NULL COMMENT '创建时间',
                             `updated_at` bigint NOT NULL COMMENT '更新时间',
                             `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '默认模板',
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UK_jm5que0lfyb3fdx3a59rga8ax` (`tid`),
                             UNIQUE KEY `idx_code` (`code`) USING BTREE,
                             KEY `tid` (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `template_files` (
                                  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `tfid` char(36) NOT NULL COMMENT '模板文件内部id',
                                  `tid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模板内部id',
                                  `type` varchar(255) NOT NULL COMMENT '文件类型',
                                  `did` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '操作部门',
                                  `uid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作人',
                                  `created_at` bigint NOT NULL COMMENT '创建时间',
                                  `updated_at` bigint NOT NULL COMMENT '更新时间',
                                  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件名',
                                  `size` bigint DEFAULT NULL COMMENT '文件大小',
                                  `display_name` varchar(255) NOT NULL COMMENT '文件展示名称',
                                  `cid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '单位',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `UK_co31cmvrkfaq6e09ebvpwhhv0` (`tfid`),
                                  KEY `f_tf_t` (`tid`),
                                  KEY `f_tf_u` (`uid`),
                                  KEY `f_tf_c` (`cid`),
                                  CONSTRAINT `f_tf_c` FOREIGN KEY (`cid`) REFERENCES `customers` (`cid`),
                                  CONSTRAINT `f_tf_t` FOREIGN KEY (`tid`) REFERENCES `templates` (`tid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
                                  CONSTRAINT `f_tf_u` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `template_file_histories` (
                                           `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
                                           `tfhid` char(36) NOT NULL COMMENT '模板文件历史id',
                                           `tfid` char(36) NOT NULL COMMENT '模板文件id',
                                           `uid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户id',
                                           `did` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门id',
                                           `created_at` bigint NOT NULL COMMENT '创建时间',
                                           `updated_at` bigint NOT NULL COMMENT '更新时间',
                                           `name` varchar(255) NOT NULL COMMENT '文件名',
                                           `size` bigint NOT NULL COMMENT '大小',
                                           `display_name` varchar(255) NOT NULL COMMENT '展示名称',
                                           PRIMARY KEY (`id`),
                                           KEY `FK8rtt5smav6wrnuicnwcx1q6su` (`tfid`),
                                           CONSTRAINT `FK8rtt5smav6wrnuicnwcx1q6su` FOREIGN KEY (`tfid`) REFERENCES `template_files` (`tfid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;