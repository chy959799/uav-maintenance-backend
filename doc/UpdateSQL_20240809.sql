ALTER TABLE `db_uav_maintenance`.`stores`
    ADD COLUMN `store_type` tinyint NULL DEFAULT 1 COMMENT '1:备件仓库；2:备品仓库' AFTER `uid`;
UPDATE `db_uav_maintenance`.`stores`
    SET store_type =1;
ALTER TABLE `db_uav_maintenance`.`order_settle`
    ADD COLUMN `invoice_out` tinyint(1) NULL COMMENT '是否已开票' AFTER `uid`;
ALTER TABLE `db_uav_maintenance`.`insurance_material`
    ADD COLUMN `invoice_out` tinyint(1) NULL COMMENT '是否已开票' AFTER `invoice_amt`;
ALTER TABLE `db_uav_maintenance`.`stock_inouts`
    CHANGE COLUMN `uid` `review_uid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL AFTER `storeid`;
ALTER TABLE `db_uav_maintenance`.`stock_inouts`
    ADD COLUMN `stc_brw_id` varchar(64) NULL AFTER `stc_prch_code`;
ALTER TABLE `db_uav_maintenance`.`insurance_material`
    ADD COLUMN `work_type` varchar(255) NULL COMMENT '工作类型' AFTER `invoice_out`,
ADD COLUMN `damaged` varchar(255) NULL COMMENT '受损情况' AFTER `work_type`;
ALTER TABLE `db_uav_maintenance`.`stocks`
    ADD COLUMN `brw_count` int NULL COMMENT '借用数量' AFTER `storeid`;