package com.thinglinks.uav.common.utils;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author iwiFool
 * @date 2024/3/28
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StringEncryptionUtilTest {

	@Test
	void validate() {
		boolean validate = StringEncryptionUtil.validate("Yuan*4088");
		assertTrue(validate);
	}
}