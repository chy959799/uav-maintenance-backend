package com.thinglinks.uav.common.utils;

import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.common.entity.City;
import com.thinglinks.uav.common.entity.County;
import com.thinglinks.uav.common.entity.Province;
import com.thinglinks.uav.common.repository.CityRepository;
import com.thinglinks.uav.common.repository.CountyRepository;
import com.thinglinks.uav.common.repository.ProvinceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author iwiFool
 * @date 2024/4/18
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EMSExpressUtilsTest {

	@Resource
	private EMSExpressUtils emsExpressUtils;

	@Resource
	private CountyRepository countyRepository;

	@Resource
	private CityRepository cityRepository;

	@Resource
	private ProvinceRepository provinceRepository;

	@Resource
	private AddressRepository addressRepository;

	@Test
	void pickupOrder() {
		String orderId = CommonUtils.uuid();
		String sendId = "f137045d51cf40bc8b6be78fbf66644f";
		String receiveId = "7eae5cc795904518acd51fcd85097b87";

		Address sendAddress = addressRepository.getByAid(sendId);
		EMSExpressUtils.AddressDTO sender = getAddress(sendAddress);

		Address receiveAddress = addressRepository.getByAid(receiveId);
		EMSExpressUtils.AddressDTO receiver = getAddress(receiveAddress);

		EMSExpressUtils.DeliveryDTO delivery = getDelivery(sender);

		String pickupOrder = emsExpressUtils.pickupOrder(orderId, sender, receiver, delivery);
		System.out.println(pickupOrder);
		if (emsExpressUtils.isSuccess(pickupOrder)) {
			System.out.println("waybillNo:" + emsExpressUtils.getWaybillNo(pickupOrder));
		} else {
			System.out.println(emsExpressUtils.getErrorMessage(pickupOrder));
		}
	}

	private static EMSExpressUtils.DeliveryDTO getDelivery(EMSExpressUtils.AddressDTO sender) {
		EMSExpressUtils.DeliveryDTO deliveryDTO = new EMSExpressUtils.DeliveryDTO();
		deliveryDTO.setAddress(sender.getAddress());
		deliveryDTO.setCounty(sender.getCounty());
		deliveryDTO.setProv(sender.getProv());
		deliveryDTO.setCity(sender.getCity());
		return deliveryDTO;
	}

	private EMSExpressUtils.AddressDTO getAddress(Address address) {
		EMSExpressUtils.AddressDTO addressDTO = new EMSExpressUtils.AddressDTO();
		County county = countyRepository.findByAreaCode(address.getCounty());
		address.setCounty(county.getAreaCode());
		City city = cityRepository.findByCityCode(address.getCity());
		Province province = provinceRepository.findByProvinceCode(address.getProvince());
		addressDTO.setName(address.getContactUser());
		addressDTO.setMobile(address.getContactTel());
		addressDTO.setProv(province.getProvinceName());
		addressDTO.setCity(city.getCityName());
		addressDTO.setCounty(county.getAreaName());
		addressDTO.setAddress(address.getDetail());
		return addressDTO;
	}

	@Test
	void pickupOrderCancel() {
		String pickupOrderCancel = emsExpressUtils.pickupOrderCancel("A2404273502000174");
		System.out.println(pickupOrderCancel);
		if (emsExpressUtils.isSuccess(pickupOrderCancel)) {
			System.out.println("取消上门派件成功");
		} else {
			System.out.println(emsExpressUtils.getErrorMessage(pickupOrderCancel));
		}
	}

	@Test
	void trackInfo() {
		String trackInfo = emsExpressUtils.trackInfo("9747150015587");
		System.out.println(trackInfo);
		if (emsExpressUtils.isSuccess(trackInfo)) {
			System.out.println(emsExpressUtils.getTrackInfoListString(trackInfo));
		} else {
			System.out.println(emsExpressUtils.getErrorMessage(trackInfo));
		}
	}

	@Test
	void queryRoutes() {
		String queryRoutes = emsExpressUtils.queryRoutes("9747150015587");
//		System.out.println(queryRoutes);
		if (emsExpressUtils.isSuccess(queryRoutes)) {
			byte[] waybillPdf = emsExpressUtils.getWaybillPdf(queryRoutes);
			String projectPath = System.getProperty("user.dir");
			emsExpressUtils.getFile(waybillPdf,projectPath,"9747150015587.pdf");
		} else {
			System.out.println(emsExpressUtils.getErrorMessage(queryRoutes));
		}
	}
}