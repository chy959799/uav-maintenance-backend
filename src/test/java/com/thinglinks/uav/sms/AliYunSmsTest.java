package com.thinglinks.uav.sms;

import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.common.sms.AliYunSmsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AliYunSmsTest {

	@Autowired
	private AliYunSmsService aliyunSmsService;

	@Test
	void testSendSmsSuccess() throws Exception {
		String phoneNum = "13684482106";
		String message = "1234";
		assertTrue(aliyunSmsService.sendSms(phoneNum, message));
	}

	@Test
	void testSendSmsError() throws Exception {
		String phoneNum = "1368448210";
		String message = "1234";
		assertFalse(aliyunSmsService.sendSms(phoneNum, message));
	}

	@Test
	void testSendOrderSmsSuccess() throws Exception {
		String phoneNum = "18883870491";
		String message = "1234";
		JSONObject templateParam = new JSONObject();
		templateParam.put("innerCode","GD20240516003");
		templateParam.put("sn", "11");
		aliyunSmsService.sendSms(phoneNum, templateParam.toJSONString(), "SMS_466440024");
	}
}
