package com.thinglinks.uav;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.minio.ContentType;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/25
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MinIOTest {

    @Resource
    private CustomMinIOClient customMinIOClient;

    @Resource
    private FileRepository fileRepository;


    @Test
    public void minIOTest() throws Exception {
        // String url1 = customMinIOClient.getObjectUrl("b-common", "WechatIMG6360.jpg");
        // System.out.println(url1);
        String url2 = customMinIOClient.getProxyUrl("product", "f7d01f1f-4d201-4bf6-91c3-1aee80cda56b.jpg");
        System.out.println(url2);
    }

    @Test
    public void qrcodeTest() throws Exception {
        QrConfig qrConfig = new QrConfig();
        qrConfig.setBackColor(Color.WHITE);
        qrConfig.setForeColor(Color.BLACK);
        qrConfig.setWidth(OrderConstants.QR_COED_WIDTH);
        qrConfig.setHeight(OrderConstants.QR_COED_HEIGHT);
        BufferedImage codeImage = QrCodeUtil.generate("202404110002", qrConfig);
        //设置文本图片宽高
        BufferedImage textImage = CommonUtils.strToImage("202404110002", OrderConstants.QR_COED_WIDTH, OrderConstants.QR_FONT_HEIGHT);
        Graphics2D graph = codeImage.createGraphics();
        //开启文字抗锯齿
        graph.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int width = textImage.getWidth(null);
        int height = textImage.getHeight(null);
        //画图 文字图片最终显示位置 在Y轴偏移量 从上往下算
        graph.drawImage(textImage, 0, OrderConstants.QR_COED_WIDTH, width, height, null);
        graph.dispose();
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ImageIO.write(codeImage, ContentType.PNG.getSuffix(), outStream);
        byte[] bytes = outStream.toByteArray();
        InputStream is = new ByteArrayInputStream(bytes);
        CustomFile customFile = new CustomFile();
        customFile.setFid(CommonUtils.uuid());
        customFile.setOrdid(CommonUtils.uuid());
        customFile.setSize((long) bytes.length);
        customFile.setName(CommonUtils.uuid().concat(CommonConstants.DOT).concat(ContentType.PNG.getSuffix()));
        customFile.setExtension(CommonUtils.getExtension(customFile.getName()));
        customFile.setDownload(Boolean.TRUE);
        customFile.setDirectory(OrderConstants.ORDER_BUCKET);
        customFile.setMime(ContentType.PNG.getType());
        customFile.setType(CommonConstants.FILE_TYPE_QR_CODE);
        String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
        customMinIOClient.uploadFile(OrderConstants.ORDER_BUCKET, objectName, is);
        System.out.println(customMinIOClient.getProxyUrl(OrderConstants.ORDER_BUCKET, objectName));
    }

    @Test
    public void getUrlTest() throws Exception{
        CustomFile customFile = fileRepository.findByFid("d9e7b6f620ef4d3c918c3ac84225472c");
        String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
        String url = customMinIOClient.getProxyUrl(customFile.getDirectory(), objectName);
        System.out.println(url);

    }

}
