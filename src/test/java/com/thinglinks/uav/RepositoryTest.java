package com.thinglinks.uav;

import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.entity.OrderQuote;
import com.thinglinks.uav.order.repository.OrderQuoteRepository;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.stock.entity.StockInOut;
import com.thinglinks.uav.stock.entity.Store;
import com.thinglinks.uav.stock.repository.StockInOutRepository;
import com.thinglinks.uav.stock.repository.StoreRepository;
import com.thinglinks.uav.stock.service.StoreService;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author: fanhaiqiu
 * @date: 2023/3/17
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RepositoryTest {

    @Resource
    private UserRepository userRepository;

    @Resource
    private StockInOutRepository stockInOutRepository;


    @Resource
    private StoreRepository storeRepository;


    @Resource
    private ProductRepository productsRepository;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderQuoteRepository orderQuoteRepository;

    @Resource
    private AddressRepository addressRepository;


    @Resource
    private CustomersRepository customersRepository;


    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private StoreService storeService;


    @Test
    public void userRepositoryTest() {
        User users = userRepository.findByMobile("13896120339");
        System.out.println(users);
//        Address address = addressRepository.getByAid("1");
//        assert Objects.isNull(address);
    }

    @Test
    public void storeRepositoryTest() {
        Store store = storeRepository.findByStoreid("b6c5464c097e400cbb87966ab476776e");
        System.out.println(store.getName());
    }

    @Test
    public void stockInOutsRepositoryTest() {
        List<StockInOut> users = stockInOutRepository.findAll();
        assert users.size() > 0;
    }

    @Test
    public void productsRepositoryTest() {
        Product products = productsRepository.findByPid("8696502f-f992-481c-9c5b-7049a07349d3");
        System.out.println(products.getRemark());

        Specification<Product> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            list.add(criteriaBuilder.equal(root.get("pid"), "8696502f-f992-481c-9c5b-7049a07349d3"));
            Predicate[] p = new Predicate[list.size()];
            return criteriaBuilder.and(list.toArray(p));
        };
        List<Product> p = productsRepository.findAll(specification);
        assert p.size() > 0;
    }

    @Test
    public void orderRepositoryTest() {
        Order order = orderRepository.getByOrdid("1");
        assert Objects.nonNull(order);
    }

    @Test
    public void orderQuoteRepositoryTest() {
        OrderQuote orderQuote = orderQuoteRepository.findFirstByOrdidAndTypeOrderByOrderDesc("1", "1");
        assert Objects.nonNull(orderQuote);
    }

    @Test
    public void customerRepositoryTest() {
        Customer customer = customersRepository.findByCid("a38b768b10c344fbba64e3d96d907635");
        assert Objects.nonNull(customer);
    }
}
