package com.thinglinks.uav;

import com.thinglinks.uav.common.utils.HttpUtils;
import com.thinglinks.uav.order.service.ExpressesService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/25
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ExpressesTest {

    @Test
    public void tokenTest() {
        String url = "https://sfapi-sbox.sf-express.com/oauth2/accessToken";
        Map<String, String> forms = new HashMap<>();
        forms.put("partnerID", "XJJYKMYC2HXX");
        forms.put("secret", "l917GFMBZ7UaeivRiQDFw3uHh5JlYZhY");
        forms.put("grantType", "password");
        String response = HttpUtils.httpPostForm(url, forms);
        System.out.println(response);
    }
}
