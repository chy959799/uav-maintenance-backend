package com.thinglinks.uav;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.order.dto.PageOrder;
import com.thinglinks.uav.order.dto.PageOrderQuery;
import com.thinglinks.uav.order.service.OrderService;
import com.thinglinks.uav.stock.dto.PageStockInOut;
import com.thinglinks.uav.stock.dto.PageStockInOutQuery;
import com.thinglinks.uav.stock.service.StockService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author: fanhaiqiu
 * @date: 2023/4/6
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ServiceTest {


    @Resource
    private OrderService orderService;

    @Resource
    private StockService stockService;

    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Test
    public void orderServicePageTest() {
        PageOrderQuery request = new PageOrderQuery();
        BasePageResponse<PageOrder> pageOrderBasePageResponse = orderService.pageOrder(request);
        System.out.println(pageOrderBasePageResponse.getCode());
    }

    @Test
    public void stockServicePageTest() {
        PageStockInOutQuery request = new PageStockInOutQuery();
        BasePageResponse<PageStockInOut> pageOrderBasePageResponse = stockService.pageStocksInOut(request);
        System.out.println(pageOrderBasePageResponse.getCode());
    }

    @Test
    public void quoteDownTest() throws Exception{
        orderService.downloadCostQuote(null, "4322715c745c4e1bbfdca7fb78eda65e");
    }

    @Test
    public void redisTest() throws Exception{
        customRedisTemplate.setUserToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiI3ZTBkZWFjNy01YWYzLTQ5NzAtYmM2YS0xMmYyOWE4M2MyYTUiLCJ1c2VyX3R5cGUiOjEsImNwaWQiOiJhMzdjNWQ1OS0wN2MwLTRlYTktYmUyNC03YzIwMzJiMGI1NTciLCJpZCI6MSwiaWF0IjoxNzE5MzA3NTEzLCJkaWQiOiIwNjRkMDhjYi02ZTE4LTQxZTAtOGEzMS00N2JjZjI0OTEyMzYiLCJqdGkiOiI4Yjk2YjU0NDU0MDk0NTVkODQ5ZmU5MTQwZmI4OTU3YiJ9.YkcLMuFSEWiXZROTJDFzTyEx8qD-2aJtRxL43ajNs08", "7e0deac7-5af3-4970-bc6a-12f29a83c2a5");
    }
}
