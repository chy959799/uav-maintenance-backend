package com.thinglinks.uav.message.service;

import com.thinglinks.uav.common.constants.MessageConstants;
import com.thinglinks.uav.system.common.Constant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;

/**
 * @author iwiFool
 * @date 2024/5/15
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MessageServiceTest {

    @Autowired
    private MessageService messageService;

    @Test
    void sendMessage() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        ArrayList<String> uidList = new ArrayList<>();
        uidList.add("7e0deac7-5af3-4970-bc6a-12f29a83c2a5");
        messageService.sendMessage("创建工单", "进行了【创建工单】操作，当前订单处于【待派单】状态，请及时处理！", MessageConstants.TYPE_NORMAL, MessageConstants.LEVEL_PERSONAL, "d8b9dccb267c4863b5e6278b5adb3113", uidList, null);
    }

    @Test
    void testSendSystemOrderMessage() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        messageService.sendSystemOrderMessage("创建工单", "进行了【创建工单】操作，当前订单处于【待派单】状态，请及时处理！", MessageConstants.LEVEL_PERSONAL, "d8b9dccb267c4863b5e6278b5adb3113", "7e0deac7-5af3-4970-bc6a-12f29a83c2a5");
        messageService.sendPersonalOrderMessage("初版资料待提交", "申请已提交，请登陆系统进入\"工单中心\"-\"工单详情\"-\"保险资料\"提交工单保险初版资料。", MessageConstants.LEVEL_PERSONAL, "d8b9dccb267c4863b5e6278b5adb3113", "7e0deac7-5af3-4970-bc6a-12f29a83c2a5");
//        messageService.sendSystemOrderMessage("创建工单", "进行了【创建工单】操作，当前订单处于【待派单】状态，请及时处理！", MessageConstants.LEVEL_SYSTEM, "6702f9db299a4566b63d2fd02c140cc4", null);
    }
}