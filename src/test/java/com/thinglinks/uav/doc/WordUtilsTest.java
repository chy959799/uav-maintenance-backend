package com.thinglinks.uav.doc;

import com.github.jsonzou.jmockdata.JMockData;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.template.*;
import com.thinglinks.uav.common.utils.JwtUtils;
import com.thinglinks.uav.common.utils.WordUtils;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.system.exception.CustomBizException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/18 10:25
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WordUtilsTest {


    @Resource
    private WordUtils wordUtils;

    @Resource
    private CustomConfig customConfig;

    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Test
    void testClaimData() throws Exception {
        // 生成赔付意向书
        Repay mock = JMockData.mock(Repay.class);
        File file = wordUtils.generatePdf(mock);
        System.out.println(file.getAbsolutePath());
    }

    @Test
    void testClaimApplicationData() throws Exception {
        // 生成赔付申请书
        ClaimApplication mock = JMockData.mock(ClaimApplication.class);
        File file = wordUtils.generatePdf(mock);
        System.out.println(file.getAbsolutePath());
    }

    @Test
    void testLossItemData() throws IOException, CustomBizException {
        // 生成损失清单
        List<LossItem> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(JMockData.mock(LossItem.class));
        }
        wordUtils.generateLossItems(list);
    }

    @Test
    void testFaultReport() throws Exception {
        // 生成故障报告
        FaultReport mock = JMockData.mock(FaultReport.class);
        mock.setAccidentTimeDay("2024年4月19日");
        mock.setSnCode("SN2022520820");
        mock.setFaultReason("无人机故障可能由多种原因引起。首先，电池故障是常见的问题之一。无人机的电池可能因老化、损坏或不正确的充电而导致故障。这可能导致电量不足或电池突然失去功率，从而影响无人机的飞行稳定性和安全性。\n" +
                "\n" +
                "另一个常见的故障原因是传感器故障。无人机依赖于各种传感器（例如陀螺仪、加速度计、罗盘）来感知和维持飞行姿态。如果这些传感器出现故障或失灵，无人机可能会失去平衡，导致飞行不稳定甚至失控。\n" +
                "\n" +
                "通信故障也是引起无人机故障的常见原因之一。飞行控制器与遥控器之间的通信故障可能导致无人机无法响应操纵指令，甚至失去控制。\n" +
                "\n" +
                "机械故障也可能导致无人机故障。无人机的机械部件，例如电机、螺旋桨等，可能因磨损、破损或设计缺陷而出现故障，从而影响无人机的飞行性能和安全性。\n" +
                "\n" +
                "此外，软件故障也是无人机故障的一个重要原因。无人机的飞行控制软件可能受到错误的指令、程序错误或系统崩溃等问题的影响，导致无法正确执行飞行任务。\n" +
                "\n" +
                "最后，环境因素也可能对无人机造成影响。恶劣的天气条件、强风、雷击等恶劣的环境因素可能导致无人机故障或失控。\n" +
                "\n" +
                "综上所述，无人机故障可能由多种原因引起，包括电池故障、传感器故障、通信故障、机械故障、软件故障和环境因素。因此，对无人机进行定期维护和检查，以及在飞行前进行充分的环境和设备检查，对于减少故障发生、确保飞行安全至关重要。");
        wordUtils.generatePdf(mock);
    }

    @Test
    void testFaultPhoto() throws Exception {
        // 生成故障照片文件
        FaultPhoto faultPhoto = new FaultPhoto();
        faultPhoto.setAccidentScene("product/2b0ccb9373f74422922b01bf1d971cf6.jpg");
        faultPhoto.setManifest("product/2e8ede69a46247a9a87eb4ed49bf9c08.jpg");
        faultPhoto.setDamageDevice("product/a19383d2-aa63-46f9-9526-96fcb025a714.jpg");
        faultPhoto.setFront("product/a4f218a0b9f14155a1aeef32303ef256.jpg");
        faultPhoto.setBack("product/ae172910-4559-4ec2-91dd-43f6ed73c748.jpg");
        faultPhoto.setOther("product/ce38e80674f94e5e94e99f8106cce0e4.png;product/efa8b436-a15a-4285-99de-1ab0e04b6509.png");
        wordUtils.generatePdf(faultPhoto);
    }

    @Test
    void testIncidentReport() throws Exception {
        // 生成案件报告
        IncidentReport mock = JMockData.mock(IncidentReport.class);
        mock.setAccidentPhoto("product/2b0ccb9373f74422922b01bf1d971cf6.jpg");
        mock.setDeviceBackPhoto("product/efa8b436-a15a-4285-99de-1ab0e04b6509.png;product/a4f218a0b9f14155a1aeef32303ef256.jpg");
        wordUtils.generatePdf(mock);
    }

    @Test
    void testQuotationPrice() throws IOException, CustomBizException, IllegalAccessException {
        QuotationPrice mock = JMockData.mock(QuotationPrice.class);
        File file = wordUtils.generateQuotationPrice(mock, Boolean.TRUE);
        System.out.println(file.getAbsolutePath());
    }

    @Test
    void generateToken() {
        String token = JwtUtils.generateToken(1);

        customRedisTemplate.setUserToken(token, "7e0deac7-5af3-4970-bc6a-12f29a83c2a5");

        System.out.println(token);
    }
}
