package com.thinglinks.uav;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTests {

    @Test
    public void contextLoads() throws Exception {
        Resource resource = new ClassPathResource("template/OrderExport.xlsx");
        String path = resource.getFile().getPath();
        File file = resource.getFile();
        System.out.println(path);
    }

}
