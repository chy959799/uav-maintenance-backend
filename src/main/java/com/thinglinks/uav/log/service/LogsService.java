package com.thinglinks.uav.log.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.log.dto.LogsPageQuery;
import com.thinglinks.uav.log.entity.Logs;
import com.thinglinks.uav.log.vo.LogsVO;

/**
 * (Logs)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-17 14:05:56
 */
public interface LogsService {

	BasePageResponse<LogsVO> getPage(LogsPageQuery pageQuery);

	BaseResponse<LogsVO> getByLogid(String uid);

	Logs insert(Logs logs);
	Logs update(Logs logs);
}


