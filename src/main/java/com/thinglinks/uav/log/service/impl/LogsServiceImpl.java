package com.thinglinks.uav.log.service.impl;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.log.dto.LogsPageQuery;
import com.thinglinks.uav.log.entity.Logs;
import com.thinglinks.uav.log.repository.LogsRepository;
import com.thinglinks.uav.log.service.LogsService;
import com.thinglinks.uav.log.vo.LogsVO;
import com.thinglinks.uav.user.dto.UserDTO;
import com.thinglinks.uav.user.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * (Logs)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-17 14:05:56
 */
@Service("logsService")
public class LogsServiceImpl implements LogsService {
	@Resource
	private LogsRepository logsRepository;

	@Override
	public BasePageResponse<LogsVO> getPage(LogsPageQuery pageQuery) {

		Specification<Logs> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			if (pageQuery.getStatus() != null && !pageQuery.getStatus().isEmpty()) {
				String code = getCode(pageQuery.getStatus());
				predicate = builder.and(predicate, builder.equal(root.get("code"), code));
			}
			if (pageQuery.getName() != null && !pageQuery.getName().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("operation"), "%" + pageQuery.getName() + "%"));
			}
			if (pageQuery.getStartTime() != null && pageQuery.getEndTime() != null) {
				long startTime = DateUtils.convertTimestamp(pageQuery.getStartTime());
				long endTime = DateUtils.convertTimestamp(pageQuery.getEndTime());
				predicate = builder.and(predicate, builder.between(root.get("ct"), startTime, endTime));
			}
			return predicate;
		};

		Page<Logs> logsPage = logsRepository.findAll(specification, pageQuery.of());
		List<Logs> logs = logsPage.getContent();

		List<LogsVO> logsVOList = logs.stream()
				.map(this::convertToLogsVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(logsPage, logsVOList);
	}

	private String getCode(String status) {
		switch (status) {
			case "success":
				return "0";
			case "failed":
				return "1";
			default:
				return null;
		}
	}

	private LogsVO convertToLogsVO(Logs logs) {
		LogsVO logsVO = new LogsVO();
		BeanUtils.copyProperties(logs, logsVO);
		logsVO.setCreatedAt(DateUtils.formatDateTime(logs.getCt()));
		logsVO.setUpdatedAt(DateUtils.formatDateTime(logs.getUt()));
		UserDTO userDTO = new UserDTO();
		User user = logs.getUser();
		BeanUtils.copyProperties(user, userDTO);
		userDTO.setCreatedAt(DateUtils.formatDateTime(user.getCt()));
		userDTO.setUpdatedAt(DateUtils.formatDateTime(user.getUt()));
		userDTO.setLoginTime(DateUtils.formatDateTime(user.getLoginTime()));
		logsVO.setUser(userDTO);
		return logsVO;
	}

	@Override
	public BaseResponse<LogsVO> getByLogid(String logid) {
		Logs log = logsRepository.findByLogid(logid);
		return BaseResponse.success(convertToLogsVO(log));
	}

	@Override
	public Logs insert(Logs logs) {
		return logsRepository.save(logs);
	}


	@Override
	public Logs update(Logs logs) {
		return logsRepository.save(logs);
	}


}


