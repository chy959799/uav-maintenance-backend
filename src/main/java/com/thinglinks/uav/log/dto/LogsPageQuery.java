package com.thinglinks.uav.log.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LogsPageQuery extends BasePageRequest {

	@ApiModelProperty(value = "状态：成功-success, 失败-failed")
	private String status;

	@ApiModelProperty(value = "操作事项")
	private String name;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String endTime;
}
