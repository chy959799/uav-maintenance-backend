package com.thinglinks.uav.log.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.log.dto.LogsPageQuery;
import com.thinglinks.uav.log.service.LogsService;
import com.thinglinks.uav.log.vo.LogsVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (Logs)表控制层
 *
 * @author iwiFool
 * @since 2024-03-17 14:05:56
 */
@Api(tags = "日志")
@RestController
@RequestMapping("logs")
public class LogsController {

	@Resource
	private LogsService logsService;

	@GetMapping
	@Operation(summary = "获取日志列表")
	public BasePageResponse<LogsVO> getLogsPage(LogsPageQuery pageQuery) {
		return logsService.getPage(pageQuery);
	}

	@GetMapping("/{logid}")
	@Operation(summary = "获取日志详情")
	public BaseResponse<LogsVO> getLogInfo(@PathVariable("logid") String logid) {
		return logsService.getByLogid(logid);
	}

}


