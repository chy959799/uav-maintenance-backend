package com.thinglinks.uav.log.repository;

import com.thinglinks.uav.log.entity.Logs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * (Logs)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-17 14:05:56
 */
public interface LogsRepository extends JpaRepository<Logs, Integer>, JpaSpecificationExecutor<Logs> {


	Logs findByLogid(String logid);

    @Modifying
    @Transactional
    @Query("DELETE FROM Logs WHERE ct < :cutoffDateInMillis")
    void deleteAllOlderThan(@Param("cutoffDateInMillis") long cutoffDateInMillis);
}


