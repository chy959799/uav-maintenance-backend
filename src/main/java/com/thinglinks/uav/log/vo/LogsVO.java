package com.thinglinks.uav.log.vo;

import com.thinglinks.uav.user.dto.UserDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@ApiModel(description = "日志对象")
@Data
public class LogsVO {
	@ApiModelProperty(value = "日志ID", example = "3fbae057-8811-4e47-84f3-26c0ef8d2fe3")
	private String logid;

	@ApiModelProperty(value = "操作", example = "获取未读消息数量")
	private String operation;

	@ApiModelProperty(value = "返回码", example = "0")
	private String code;

	@ApiModelProperty(value = "消息", example = "success")
	private String message;

	@ApiModelProperty(value = "请求类型", example = "GET")
	private String requestType;

	@ApiModelProperty(value = "请求内容", example = "{}")
	private String requestContent;

	@ApiModelProperty(value = "请求IP", example = "171.223.38.205")
	private String requestIp;

	@ApiModelProperty(value = "请求Agent", example = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0")
	private String requestAgent;

	@ApiModelProperty(value = "创建时间", example = "2024-03-17 16:05:32")
	private String createdAt;

	@ApiModelProperty(value = "更新时间", example = "2024-03-17 16:05:32")
	private String updatedAt;

	@ApiModelProperty(value = "用户ID", example = "88fe6fcc-de2c-4a42-8383-fc098456fb43")
	private String uid;

	@ApiModelProperty(value = "用户对象")
	private UserDTO user;
}

