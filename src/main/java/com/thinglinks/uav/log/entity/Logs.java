package com.thinglinks.uav.log.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * (Logs)实体类
 *
 * @author iwiFool
 * @since 2024-03-17 14:05:56
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "logs")
@Data
public class Logs extends BaseEntity {

	/**
	 * 日志ID
	 */
	private String logid;
	/**
	 * 操作名称
	 */
	private String operation;
	/**
	 * 错误代号
	 */
	private String code;
	/**
	 * 错误消息
	 */
	private String message;
	/**
	 * 操作类型
	 */
	private String requestType;
	/**
	 * 操作内容
	 */
	private String requestContent;
	/**
	 * 请求IP地址
	 */
	private String requestIp;
	/**
	 * 请求代理
	 */
	private String requestAgent;

	private String uid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;
}
