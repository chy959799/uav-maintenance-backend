package com.thinglinks.uav.system.config;


import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.ErrorMessage;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.system.exception.CustomBizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

/**
 * @author fanhaiqiu
 * @date 2019/6/27
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {


    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorMessage> defaultErrorHandler(HttpServletRequest request, Exception e) {
        log.error("global error", e);
        BaseResponse<ErrorMessage> response = new BaseResponse<>();
        response.setCode(StatusEnum.FAIL.getCode());
        //自定义业务错误
        if (e instanceof CustomBizException || e instanceof BusinessException) {
            response.setMsg(e.getMessage());
            return response;
        }
        //系统未知错误
        response.setMsg(StatusEnum.FAIL.getMessage());
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setErrorMessage(e.getMessage());
        errorMessage.setUrl(request.getRequestURL().toString());
        response.setResults(errorMessage);
        return response;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseResponse<Void> methodArgumentNotValidException(MethodArgumentNotValidException validException) {
        String message = validException.getBindingResult().getFieldErrors().stream()
                .map(x -> String.join(":", x.getField(), x.getDefaultMessage())).collect(Collectors.joining(", "));
        return BaseResponse.fail(StatusEnum.PARAM_ERROR.getCode(), message);
    }

}

