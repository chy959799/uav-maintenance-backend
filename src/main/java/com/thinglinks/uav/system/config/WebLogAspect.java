package com.thinglinks.uav.system.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.log.entity.Logs;
import com.thinglinks.uav.log.repository.LogsRepository;
import com.thinglinks.uav.user.dto.LoginResponse;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author fanhaiqiu
 * @date 2022/1/23
 */
@Aspect
@Component
@Slf4j
public class WebLogAspect {

    @Resource
    private LogsRepository logsRepository;

    @Pointcut("execution(public * com.thinglinks.uav.*.controller.*.*(..))")
    public void webLog() {

    }

    @Around("webLog()")
    public Object doBefore(ProceedingJoinPoint jp) throws Throwable {
        HttpServletRequest request = getHttpServletRequest();
        // 推送接口不需要打日志
        if (isExcludedPath(request)) {
            return jp.proceed();
        }
        // 打印请求的内容
        log.info("请求Url : {}", request.getRequestURL().toString());
        log.info("请求参数 : {}", Arrays.toString(jp.getArgs()));

        Object result = jp.proceed();
        // 处理完请求，返回内容
        String jsonString = JSON.toJSONString(result);
        log.info("请求返回 : {}", jsonString);

        String message = null;
        String code = null;
        if (!"null".equals(jsonString)) {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            message = jsonObject.getString("msg");
            code = jsonObject.getString("code");
        }

        String uid = getUid(jsonString);

        handleLog(jp, uid, message, code);

        return result;
    }

    private String getUid(String jsonString) {
        String uid = CommonUtils.getUid();
        if (uid == null && jsonString != null) {
            JSONObject jsonObject = JSON.parseObject(jsonString);
            LoginResponse results = jsonObject.getObject("results", LoginResponse.class);
            if (results != null) {
                uid = results.getUid();
            }
        }
        return uid;
    }

    @AfterThrowing(pointcut = "webLog()", throwing = "exception")
    public void doAfterThrowing(JoinPoint jp, Exception exception) {
        String message = exception.getMessage();
        log.error("异常信息:{}", message);
        String code = StatusEnum.FAIL.getCode().toString();

        String uid = getUid(null);
        handleLog(jp, uid, message, code);
    }

    private void handleLog(JoinPoint jp, String uid, String message, String code) {
        if (Objects.nonNull(uid)){
            Logs logs = new Logs();
            logs.setUid(uid);
            logs.setLogid(CommonUtils.uuid());

            HttpServletRequest request = getHttpServletRequest();
            logs.setRequestType(request.getMethod());
            logs.setRequestAgent(request.getHeader("User-Agent"));
            logs.setRequestIp(CommonUtils.getClientIp(request));

            MethodSignature ms = (MethodSignature) jp.getSignature();
            Operation operation = ms.getMethod().getDeclaredAnnotation(Operation.class);
            logs.setOperation(operation != null ? operation.summary() : "");
            logs.setRequestContent(Arrays.toString(jp.getArgs()));

            logs.setMessage(message != null ? message : "未提供消息");
            logs.setCode(code != null ? code : "未提供状态码");

            logsRepository.save(logs);
        }
    }

    private boolean isExcludedPath(HttpServletRequest request) {
        AntPathRequestMatcher matcher = new AntPathRequestMatcher("/open/receive/**");
        return matcher.matches(request) ||
               new AntPathRequestMatcher("/captchas/*").matches(request) ||
               new AntPathRequestMatcher("/insurances/export").matches(request) ||
               new AntPathRequestMatcher("/customers/tree").matches(request) ||
               new AntPathRequestMatcher("/express/open/**").matches(request) ||
               new AntPathRequestMatcher("/system/open/**").matches(request) ||
               new AntPathRequestMatcher("/files/upload").matches(request);
    }

    private HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }
}
