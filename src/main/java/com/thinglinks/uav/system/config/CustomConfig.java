package com.thinglinks.uav.system.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/20
 */
@Data
@Configuration
public class CustomConfig {

    @Value("${disruptor.buffer-size}")
    private int bufferSize;

    @Value("${expresses.sf.url}")
    private String sfUrl;

    @Value("${expresses.sf.partnerID}")
    private String sfPartnerID;

    @Value("${expresses.sf.secret}")
    private String sfSecret;

    @Value("${expresses.sf.monthlyCard}")
    private String monthlyCard;

    @Value("${expresses.sf.templateCode}")
    private String templateCode;

    @Value("${pdf.temp.dir:/data/uav-maintenance-backend/temp/}")
    private String pdfTempDir;

    @Value("${confirm.device.template:SMS_466440024}")
    private String confirmDeviceTemplate1;

    @Value("${confirm.device.template:SMS_466400036}")
    private String confirmDeviceTemplate2;

    @Value("${confirm.quote.template:SMS_466390028}")
    private String confirmQuote;

    @Value("${submit.first-insurance.template:SMS_466440026}")
    private String summitFirstInsurance;

    @Value("${review.first-insurance.template:SMS_466380027}")
    private String reviewFirstInsurance;

    @Value("${submit.last-insurance.template:SMS_466345061}")
    private String summitLastInsurance;

    @Value("${review.last-insurance.template:SMS_466350049}")
    private String reviewLastInsurance;

    @Value("${quote.isSign:false}")
    private Boolean isSign;

    @Value("${uav.service.ip:192.168.3.20}")
    private String uavServiceIp;
}
