package com.thinglinks.uav.system.config;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.system.annotation.Authority;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
@Aspect
@Configuration
public class CustomAuthConfig {

    @Pointcut(value = "@annotation(com.thinglinks.uav.system.annotation.Authority)")
    public void auth() {

    }

    @Around("auth()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		List<String> actions = CommonUtils.getUserAuth();
        Integer userType = CommonUtils.getUserType();
        MethodSignature ms = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = ms.getMethod();
        Authority permission = method.getAnnotation(Authority.class);
        String action = permission.action();
        UserTypeEnum[] userTypeEnums = permission.userType();
        if (CommonUtils.isAdmin()) {
            if (permission.allowAdmin()) {
                return proceedingJoinPoint.proceed();
            }
        } else if (checkAuth(action, actions, userType, userTypeEnums)) {
            return proceedingJoinPoint.proceed();
        }

        Class<?> returnType = method.getReturnType();
        if (returnType == BaseResponse.class) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        } else {
            return BasePageResponse.fail(StatusEnum.NO_AUTH);
        }
    }

    public boolean checkAuth(String action, List<String> actions, Integer t, UserTypeEnum[] types) {
        //没有注明权限，表示都可访问
        if (StringUtils.isEmpty(action) && Objects.isNull(types)) {
            return true;
        }
        if (actions.contains(action)) {
            return true;
        }
        for (UserTypeEnum type : types) {
            if (type.getCode().equals(t)) {
                return true;
            }
        }
        return false;
    }
}
