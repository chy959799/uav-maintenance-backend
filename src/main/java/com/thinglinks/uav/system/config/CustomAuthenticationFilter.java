package com.thinglinks.uav.system.config;

import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.common.constants.UserConstants;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.JwtUtils;
import com.thinglinks.uav.system.common.Constant;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.user.entity.Permission;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.entity.UserRole;
import com.thinglinks.uav.user.repository.PermissionRepository;
import com.thinglinks.uav.user.repository.UserRepository;
import com.thinglinks.uav.user.repository.UserRoleRepository;
import com.thinglinks.uav.user.service.AuthService;
import com.thinglinks.uav.user.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author fanhaiqiu
 * @date 2019/6/25
 */

@Component
@Slf4j
public class CustomAuthenticationFilter extends OncePerRequestFilter {

    private static final String TOKEN_KEY = "token";
    private static final String CHARSET_UTF_8 = "UTF-8";
    private static final String ACCESS_TOKEN = "Authorization";

    @Resource
    private CustomRedisTemplate customRedisTemplate;
    @Resource
    private UserRoleRepository userRoleRepository;
    @Resource
    private UserRepository userRepository;
    @Resource
    private RoleService roleService;
    @Resource
    private PermissionRepository permissionRepository;
    @Resource
    private AuthService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (CommonUtils.isExcludedPath()) {
            filterChain.doFilter(request, response);
            return;
        }
        String tokenValue = request.getHeader(ACCESS_TOKEN);
        try {
            Integer status = checkToken(request, tokenValue);
            if (Objects.equals(HttpServletResponse.SC_OK, status)) {
                filterChain.doFilter(request, response);
                return;
            }
            response.setCharacterEncoding(CHARSET_UTF_8);
            response.setStatus(status);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            PrintWriter writer = response.getWriter();
            JSONObject token = new JSONObject();
            token.put(TOKEN_KEY, tokenValue);
            writer.write(JSON.toJSONString(BaseResponse.invalid(token)));
            writer.flush();
            writer.close();
        } catch (BusinessException e) {
            response.setCharacterEncoding(CHARSET_UTF_8);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            PrintWriter writer = response.getWriter();
            writer.write(JSON.toJSONString(BaseResponse.fail(StatusEnum.INVALID_TOKEN.getCode(), e.getMessage())));
            writer.flush();
            writer.close();
        }
    }


    /**
     * token校验
     *
     * @param tokenValue
     * @return
     */
    private Integer checkToken(HttpServletRequest request, String tokenValue) {
        if (StringUtils.isEmpty(tokenValue)) {
            return HttpServletResponse.SC_FORBIDDEN;
        }
        log.info("begin check token:{}", tokenValue);
        try {
            if (tokenValue.startsWith("Bearer ")) {
                tokenValue = tokenValue.substring(7);
            }

            Map<String, Object> payload = JwtUtils.parseToken(tokenValue);

            setRequestAttributes(request, payload);

            // 获取redis中的token
            String uid = CommonUtils.getUid();
            String userToken = customRedisTemplate.getUserToken(uid);
            // 校验token是否有效
            if (StringUtils.isEmpty(userToken)) {
                return HttpServletResponse.SC_UNAUTHORIZED;
            }
            if (!CommonUtils.isAdmin() && !userToken.equals(tokenValue)) {
                return HttpServletResponse.SC_UNAUTHORIZED;
            }

            String cpid = CommonUtils.getCpid();
            // 更新redis中的token
            customRedisTemplate.refreshUserToken(uid);
            customRedisTemplate.refreshCompanyUserToken(cpid, uid);

            return HttpServletResponse.SC_OK;
        } catch (BusinessException e) {
            log.error("token error", e);
            throw e;
        } catch (Exception e) {
            log.error("token error", e);
        }
        return HttpServletResponse.SC_BAD_REQUEST;
    }

    private void setRequestAttributes(HttpServletRequest request, Map<String, Object> payload) {

        request.setAttribute(Constant.SYSTEM, true);

        Integer userId = Convert.toInt(payload.get(Constant.USER_ID));
        if (userId != null) {
            userRepository.findById(userId).ifPresent(user -> {

                request.setAttribute(Constant.USER_ID, userId);
                request.setAttribute(Constant.USER_TYPE, user.getUserType());
                String uid = user.getUid();
                request.setAttribute(Constant.UID, uid);
                request.setAttribute(Constant.DID, user.getDid());
                request.setAttribute(Constant.CID, user.getCid());
                request.setAttribute(Constant.CPID, user.getCpid());

                List<Integer> roleIds = customRedisTemplate.getUserRoles(uid);
                if (roleIds == null) {
                    List<UserRole> roles = userRoleRepository.findByUserId(userId);
                    roleIds = roles.stream().map(UserRole::getRoleId).collect(Collectors.toList());
                    customRedisTemplate.updateUserRoles(uid, roleIds);
                }
                ArrayList<Role> roles = new ArrayList<>();
                for (Integer roleId : roleIds) {
                    Role role = roleService.getRoleById(roleId);
                    roles.add(role);
                }
                List<String> rolesCode = new ArrayList<>();
                List<String> permissionsCode = new ArrayList<>();
                List<Permission> permissions;
                if (!roles.isEmpty()) {
                    rolesCode = roles.stream().map(Role::getCode).collect(Collectors.toList());
                    if (rolesCode.contains(UserConstants.ADMIN_ROLE)) {
                        permissions = permissionRepository.findAll();
                    } else {
                        permissions = roles.stream()
                                .flatMap(role -> role.getPermissions().stream())
                                .collect(Collectors.toList());
                    }
                    permissionsCode = permissions.stream().map(Permission::getCode).collect(Collectors.toList());
                }
                request.setAttribute(Constant.ROLES, rolesCode);
                request.setAttribute(Constant.USER_AUTH, permissionsCode);

                if (!CommonUtils.isAdmin()) {
                    authService.checkUser(user);
                }
            });
        }

        request.setAttribute(Constant.SYSTEM, false);
    }

}
