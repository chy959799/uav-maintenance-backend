package com.thinglinks.uav.system.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/7/4
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException{

    public BusinessException(String msg, Exception e) {
        super(msg, e);
    }

    public BusinessException(String msg) {
        super(msg);
    }

    public BusinessException() {

    }

    public BusinessException(Exception e) {
        super(e);
    }
}
