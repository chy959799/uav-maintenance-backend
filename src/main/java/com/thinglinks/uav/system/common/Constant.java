package com.thinglinks.uav.system.common;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
public class Constant {

    public static final String UID = "uid";

    public static final String USER_TYPE = "user_type";
    public static final String USER_AUTH = "user_auth";

    public static final String SEPARATOR = "#";

	public static final String USER_ID = "id";
	public static final String CPID = "cpid";
	public static final String ROLES = "roles";
	public static final String DID = "did";
    public static final String CID = "cid";

    public static final String SYSTEM = "system";

    private Constant() {
	}
}
