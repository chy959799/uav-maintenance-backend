package com.thinglinks.uav.system.annotation;

import com.thinglinks.uav.common.enums.UserTypeEnum;

import java.lang.annotation.*;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Authority {

    String action() default "";

    UserTypeEnum[] userType() default {};

    String name() default "";

    boolean allowAdmin() default true;
}
