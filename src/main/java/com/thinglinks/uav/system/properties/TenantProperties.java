package com.thinglinks.uav.system.properties;

import com.thinglinks.uav.system.annotation.TenantTable;
import lombok.Data;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@Component
public class TenantProperties {

    /**
     * 需要进行租户解析的租户表
     */
    private List<String> tables = new ArrayList<>();

    @PostConstruct
    public void init() {
        Reflections reflections = new Reflections("com.thinglinks.uav");
        Set<Class<?>> annotatedClasses = reflections.getTypesAnnotatedWith(TenantTable.class);

        for (Class<?> clazz : annotatedClasses) {
            Table table = clazz.getAnnotation(Table.class);
            if (table != null) {
                String tableName = table.name();
                tables.add(tableName);
            }
        }
    }
}