package com.thinglinks.uav.spare.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SparePriceApprovalPageQuery extends BasePageRequest {

	@ApiModelProperty(value = "申请单编号")
	private String code;

	@ApiModelProperty(value = "项目名称")
	private String projectName;

	@ApiModelProperty(value = "申请状态[0-待初审，1-待终审，2-已完成，3-已驳回]")
	private String status;
}
