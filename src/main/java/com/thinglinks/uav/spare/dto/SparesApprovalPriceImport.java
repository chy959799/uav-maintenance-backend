package com.thinglinks.uav.spare.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.thinglinks.uav.common.dto.BaseImportError;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/5/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SparesApprovalPriceImport extends BaseImportError {

    @ExcelProperty(value = "项目名称")
    private String projectName;

    @NotBlank(message = "项目编码不能为空")
    @ExcelProperty(value = "项目编码")
    private String projectCode;

    @ExcelProperty(value = "备件名称")
    private String spareName;

    @NotBlank(message = "备件编码不能为空")
    @ExcelProperty(value = "备件编码")
    private String spareCode;

    @NotNull(message = "销售价不能为空")
    @Min(value = 0, message = "销售价不能为负数")
    @ExcelProperty(value = "销售价（元）")
    private BigDecimal salePrice;

    @NotNull(message = "工时不能为空")
    @Min(value = 0, message = "工时不能为负数")
    @ExcelProperty(value = "工时（小时）")
    private BigDecimal workHour;

    @NotNull(message = "工时单价不能为空")
    @Min(value = 0, message = "工时单价不能为负数")
    @ExcelProperty(value = "工时单价（元）")
    private BigDecimal workHourUnitCost;
}
