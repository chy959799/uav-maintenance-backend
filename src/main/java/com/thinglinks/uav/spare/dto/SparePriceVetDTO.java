package com.thinglinks.uav.spare.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
public class SparePriceVetDTO {

	@ApiModelProperty(value = "审批意见[0-驳回，1-通过]", example = "1")
	@NotBlank(message = "审批意见不能为空")
	@Pattern(regexp = "^[0-1]$", message = "审批意见格式错误")
	private String status;

	@ApiModelProperty(value = "备注")
	private String remark;
}
