package com.thinglinks.uav.spare.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
public class SparesApprovalDTO {

	@ApiModelProperty(value = "项目id")
	@NotBlank(message = "项目id不能为空")
	private String pjid;

	@ApiModelProperty(value = "申请说明")
	private String description;

	@ApiModelProperty(value = "配件清单")
	@NotNull(message = "配件清单不能为空")
	private List<SparesApprovalPriceDTO> spareList;
}
