package com.thinglinks.uav.spare.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SparePricePageQuery extends BasePageRequest {

	@ApiModelProperty(value = "项目名称")
	private String projectName;

	@ApiModelProperty(value = "备件名称")
	private String spareName;

	@ApiModelProperty(value = "备件类型")
	private String catid;

	@ApiModelProperty(value = "备件编码")
	private String code;

	@ApiModelProperty(value = "项目id")
	private String pjid;
}
