package com.thinglinks.uav.spare.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author iwiFool
 * @date 2024/7/30
 */
@Data
public class OrderSparePriceQuery {

    /**
     * pjid
     */
    @NotNull(message = "pjid不能为空")
    @ApiModelProperty(value = "pjid")
    private String pjid;

    /**
     * catid
     */
    @NotNull(message = "catid不能为空")
    @ApiModelProperty(value = "catid")
    private String catid;

    /**
     * 类型：1-成本报价 2-客户报价
     */
    @NotNull(message = "type不能为空")
    @ApiModelProperty(value = "类型：1-成本报价 2-客户报价")
    private Integer type;
}
