package com.thinglinks.uav.spare.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
@ExcelIgnoreUnannotated
public class SparesApprovalPriceDTO {

	@ApiModelProperty(value = "pid")
	@NotNull(message = "pid不能为空", groups = {Insert.class})
	private String pid;

	@ApiModelProperty(value = "销售价（元）")
	@NotNull(message = "销售价不能为空")
	@Min(value = 0, message = "销售价不能为负数")
	@ExcelProperty(value = "销售价（元）")
	private BigDecimal salePrice;

	@ApiModelProperty(value = "工时（小时）")
	@NotNull(message = "工时不能为空")
	@Min(value = 0, message = "工时不能为负数")
	@ExcelProperty(value = "工时（小时）")
	private BigDecimal workHour;

	@ApiModelProperty(value = "工时单价（元）")
	@NotNull(message = "工时单价不能为空")
	@Min(value = 0, message = "工时单价不能为负数")
	@ExcelProperty(value = "工时单价（元）")
	private BigDecimal workHourUnitCost;

	@ExcelProperty(value = "项目编码")
	private String projectCode;

	@ExcelProperty(value = "备件编码")
	private String spareCode;

	public interface Insert extends Default {

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
            return true;
        }
		if (o == null || getClass() != o.getClass()) {
            return false;
        }
		SparesApprovalPriceDTO that = (SparesApprovalPriceDTO) o;
		return Objects.equals(spareCode, that.spareCode) || Objects.equals(pid, that.pid);
	}

	@Override
	public int hashCode() {
		return Objects.hash(spareCode, pid);
	}
}
