package com.thinglinks.uav.spare.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * (SparesApprovalRecords)实体类
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:04
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "spares_approval_records")
@Data
public class SparesApprovalRecord extends BaseEntity {

	/**
	 * 备件价格申请审批表外部id
	 */
	private String sarid;
	/**
	 * 审批状态[0-驳回，1-通过]
	 */
	private String status;
	/**
	 * 审批备注
	 */
	private String remark;
	/**
	 * 备件价格申请表外部id
	 */
	private String said;
	/**
	 * 用户ID
	 */
	private String uid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "said", referencedColumnName = "said", insertable = false, updatable = false)
	private SparesApproval sparesApproval;
}
