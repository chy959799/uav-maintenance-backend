package com.thinglinks.uav.spare.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * (SparesApprovals)实体类
 *
 * @author iwiFool
 * @since 2024-03-29 15:11:03
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "spares_approvals")
@Data
public class SparesApproval extends BaseEntity {

	/**
	 * 备件申请表外部id
	 */
	private String said;
	/**
	 * 申请单编号
	 */
	private String code;
	/**
	 * 项目ID
	 */
	private String pjid;
	/**
	 * 申请类型
	 */
	private String type;
	/**
	 * 申请状态[0-待初审，1-待终审，2-已完成，3-已驳回]
	 */
	private String status;
	/**
	 * 申请说明
	 */
	private String description;
	/**
	 * 部门ID唯一
	 */
	private String did;
	/**
	 * 用户ID
	 */
	private String uid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pjid", referencedColumnName = "pjid", insertable = false, updatable = false)
	private Project project;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "did", referencedColumnName = "did", insertable = false, updatable = false)
	private Department department;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "sparesApproval")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<SparesApprovalPrice> sparesApprovalPrices;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "sparesApproval")
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<SparesApprovalRecord> sparesApprovalRecords;
}
