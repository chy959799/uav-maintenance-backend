package com.thinglinks.uav.spare.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * (ProjectProducts)实体类
 *
 * @author iwiFool
 * @since 2024-03-29 16:17:06
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "project_products")
@Data
public class ProjectProduct extends BaseEntity {

	/**
	 * 项目备件外部id
	 */
	private String ppid;
	/**
	 * 备件id
	 */
	private String pid;
	/**
	 * 项目id
	 */
	private String pjid;
	/**
	 * 销售价
	 */
	private BigDecimal salePrice;
	/**
	 * 工时
	 */
	private BigDecimal workHour;
	/**
	 * 工时单价
	 */
	private BigDecimal workHourUnitCost;
	/**
	 * 工时费
	 */
	private BigDecimal workHourCost;
	/**
	 * 用户id
	 */
	private String uid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pjid", referencedColumnName = "pjid", insertable = false, updatable = false)
	private Project project;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
	private Product product;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;
}
