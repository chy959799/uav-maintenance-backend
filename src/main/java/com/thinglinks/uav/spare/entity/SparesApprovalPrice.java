package com.thinglinks.uav.spare.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * (SparesApprovalPrices)实体类
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:30
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "spares_approval_prices")
@Data
public class SparesApprovalPrice extends BaseEntity {

	/**
	 * 申请备件价格表外部id
	 */
	private String sapid;
	/**
	 * 销售价
	 */
	private BigDecimal salePrice;
	/**
	 * 工时
	 */
	private BigDecimal workHour;
	/**
	 * 工时单价
	 */
	private BigDecimal workHourUnitCost;
	/**
	 * 工时费
	 */
	private BigDecimal workHourCost;
	/**
	 * 备件申请表外部id
	 */
	private String said;
	/**
	 * 用户ID
	 */
	private String uid;
	/**
	 * 产品内部ID
	 */
	private String pid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "said", referencedColumnName = "said", insertable = false, updatable = false)
	private SparesApproval sparesApproval;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
	private Product product;
}
