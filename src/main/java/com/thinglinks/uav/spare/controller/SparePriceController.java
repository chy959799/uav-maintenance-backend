package com.thinglinks.uav.spare.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.order.dto.OrderSparePrice;
import com.thinglinks.uav.spare.dto.*;
import com.thinglinks.uav.spare.service.ProjectProductService;
import com.thinglinks.uav.spare.service.SparesApprovalService;
import com.thinglinks.uav.spare.vo.SparePriceApprovalInfoVO;
import com.thinglinks.uav.spare.vo.SparePriceApprovalVO;
import com.thinglinks.uav.spare.vo.SparePriceVO;
import com.thinglinks.uav.spare.vo.SpareVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author iwiFool
 * @since 2024-03-29 16:17:05
 */
@Api(tags = "备件价格管理")
@RestController
@RequestMapping("sparePrices")
@Validated
public class SparePriceController {

    @Resource
    private ProjectProductService projectProductService;

    @Resource
    private SparesApprovalService sparesApprovalService;

    @Authority(action = "sparePriceCreate")
    @PostMapping("/apply")
    @Operation(summary = "申请添加备品价格")
    public BaseResponse<String> insert(@RequestBody @Validated(SparesApprovalPriceDTO.Insert.class) SparesApprovalDTO dto) {
        return sparesApprovalService.insert(dto);
    }

    @Authority(action = "sparePriceDelete")
    @DeleteMapping("{ppid}/apply")
    @Operation(summary = "申请删除备品价格")
    public BaseResponse<String> delete(@PathVariable String ppid) {
        return sparesApprovalService.delete(ppid);
    }

    @Authority(action = "sparePriceDelete")
    @DeleteMapping("/all/apply")
    @Operation(summary = "申请批量删除备品价格")
    public BaseResponse<String> deleteAll(@RequestBody DeleteDTO dto) {
        return sparesApprovalService.deleteAll(dto);
    }

    @Authority(action = "sparePriceEdit")
    @PatchMapping("{ppid}/apply")
    @Operation(summary = "申请修改备品价格")
    public BaseResponse<String> update(@PathVariable String ppid, @RequestBody @Validated SparesApprovalPriceDTO dto) {
        return sparesApprovalService.update(ppid, dto);
    }

    @GetMapping
    @Operation(summary = "分页获取备件价格列表")
    public BasePageResponse<SparePriceVO> page(SparePricePageQuery pageQuery) {
        return projectProductService.page(pageQuery);
    }

    @GetMapping("/approval")
    @Operation(summary = "分页获取备品价格申请列表")
    public BasePageResponse<SparePriceApprovalVO> approval(SparePriceApprovalPageQuery pageQuery) {
        return sparesApprovalService.page(pageQuery);
    }

    @DeleteMapping("/approval/{said}")
    @Operation(summary = "删除申请单")
    public BaseResponse<String> deleteApproval(@PathVariable String said) {
        return sparesApprovalService.deleteApproval(said);
    }

    @GetMapping("/approval/{said}")
    @Operation(summary = "获取申请单详情")
    public BaseResponse<SparePriceApprovalInfoVO> detailApproval(@PathVariable String said) {
        return sparesApprovalService.detailApproval(said);
    }

    @Authority(action = "sparePriceApprovalVetting")
    @PatchMapping("/approval/{said}/vetting")
    @Operation(summary = "审核备品价格申请表")
    public BaseResponse<String> update(@PathVariable String said, @RequestBody @Validated SparePriceVetDTO dto) {
        return sparesApprovalService.vetting(said, dto);
    }

    @Authority(action = "sparePriceImport")
    @PostMapping(path = "import")
    @Operation(summary = "备件价格导入")
    public BaseResponse<String> importSparePrice(@RequestPart(value = "file") MultipartFile file, HttpServletResponse response) throws IOException {
        return sparesApprovalService.importSparePrice(file, response);
    }

    @Authority(action = "sparePriceExport")
    @PostMapping("/export")
    @Operation(summary = "备件价格导出")
    public void export(@RequestBody List<String> ppids, HttpServletResponse response) throws IOException {
        projectProductService.exportSparePrice(ppids, response);
    }

    @GetMapping(path = "one")
    @Operation(summary = "获取项目下配件的价格")
    public BaseResponse<SparePriceVO> getSparePrice(@RequestParam(value = "pjid") String pjid, @RequestParam(value = "pid") String pid) {
        return projectProductService.getSparePrice(pjid, pid);
    }

    @GetMapping(path = "spares")
    @Operation(summary = "备件信息列表")
    public BaseResponse<List<SpareVO>> getSpareList() {
        return projectProductService.getSpareList();
    }

    @Operation(summary = "备件价格查询")
    @GetMapping(path = "orderSparePrice")
    public BaseResponse<List<OrderSparePrice>> getOrderSparePrice(@Validated OrderSparePriceQuery query) {
        return projectProductService.getOrderSparePriceList(query);
    }
}


