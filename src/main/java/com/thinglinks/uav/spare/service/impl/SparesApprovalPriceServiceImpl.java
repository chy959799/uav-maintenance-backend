package com.thinglinks.uav.spare.service.impl;

import com.thinglinks.uav.common.constants.SparesConstants;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.spare.dto.SparesApprovalPriceDTO;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import com.thinglinks.uav.spare.entity.SparesApprovalPrice;
import com.thinglinks.uav.spare.repository.ProjectProductsRepository;
import com.thinglinks.uav.spare.repository.SparesApprovalPriceRepository;
import com.thinglinks.uav.spare.service.SparesApprovalPriceService;
import com.thinglinks.uav.system.exception.BusinessException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (SparesApprovalPrices)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:30
 */
@Service
public class SparesApprovalPriceServiceImpl implements SparesApprovalPriceService {

	@Resource
	private SparesApprovalPriceRepository sparesApprovalPriceRepository;
	@Resource
	private ProjectProductsRepository projectProductsRepository;
	@Resource
	private ProductRepository productRepository;


	@Override
	public void insertAll(List<SparesApprovalPriceDTO> spareList, String said) {
		List<SparesApprovalPrice> list = spareList.stream()
				.map(dto -> coverToEntity(said, dto))
				.filter(sparesApprovalPrice -> !sparesApprovalPriceRepository.existsBySaidAndPid(said, sparesApprovalPrice.getPid()))
				.collect(Collectors.toList());
		sparesApprovalPriceRepository.saveAll(list);
	}

	@Override
	public void insert(SparesApprovalPriceDTO sparesApprovalPriceDTO, String said) {
		sparesApprovalPriceRepository.save(coverToEntity(said, sparesApprovalPriceDTO));
	}

	@Override
	public void executeSave(String said, String pjid) {
		List<SparesApprovalPrice> sparesApprovalPriceList = sparesApprovalPriceRepository.findAllBySaid(said);
		HashSet<String> distinctest = new HashSet<>();
		List<ProjectProduct> projectProductList = sparesApprovalPriceList.stream()
				.filter(sparesApprovalPrice -> distinctest.add(sparesApprovalPrice.getPid()))
				.map(sparesApprovalPrice -> {
					ProjectProduct projectProduct = projectProductsRepository.findByPjidAndPid(pjid, sparesApprovalPrice.getPid());
					if (Objects.isNull(projectProduct)) {
						projectProduct = new ProjectProduct();
						projectProduct.setPpid(CommonUtils.uuid());
						projectProduct.setPid(sparesApprovalPrice.getPid());
						projectProduct.setPjid(pjid);
					}
					projectProduct.setSalePrice(sparesApprovalPrice.getSalePrice());
					projectProduct.setWorkHour(sparesApprovalPrice.getWorkHour());
					projectProduct.setWorkHourUnitCost(sparesApprovalPrice.getWorkHourUnitCost());
					projectProduct.setWorkHourCost(sparesApprovalPrice.getWorkHourCost());
					projectProduct.setUid(CommonUtils.getUid());

					return projectProduct;
				}).collect(Collectors.toList());
		projectProductsRepository.saveAll(projectProductList);
	}

	@Override
	public void executeDelete(String said, String pjid) {
		List<SparesApprovalPrice> sparesApprovalPriceList = sparesApprovalPriceRepository.findAllBySaid(said);
		List<ProjectProduct> projectProductList = sparesApprovalPriceList.stream()
				.map(sparesApprovalPrice -> projectProductsRepository.findByPjidAndPid(pjid, sparesApprovalPrice.getPid())).collect(Collectors.toList());
		projectProductsRepository.deleteAll(projectProductList);
	}

	@Override
	public Long countUnValid(String pjid, String pid) {
		Specification<SparesApprovalPrice> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			predicate = builder.and(predicate, builder.equal(root.get("pid"), pid));
			predicate = builder.and(predicate, builder.equal(root.join("sparesApproval").get("pjid"), pjid));
			ArrayList<Object> list = new ArrayList<>();
			list.add(SparesConstants.STATUS_FINISH);
			list.add(SparesConstants.STATUS_REJECT);
			predicate = builder.and(predicate, builder.not(root.join("sparesApproval").get("status").in(list)));

			return predicate;
		};

        return sparesApprovalPriceRepository.count(specification);
	}

	private SparesApprovalPrice coverToEntity(String said, SparesApprovalPriceDTO dto) {
		SparesApprovalPrice sparesApprovalPrice = new SparesApprovalPrice();
		sparesApprovalPrice.setSapid(CommonUtils.uuid());
		sparesApprovalPrice.setUid(CommonUtils.getUid());
		Product product;
		if (Objects.nonNull(dto.getPid())) {
			product = productRepository.findByPid(dto.getPid());
		} else {
			product = productRepository.findByCode(dto.getSpareCode());
		}
		if (Objects.isNull(product)) {
			throw new BusinessException("备件不存在");
		}
		sparesApprovalPrice.setPid(product.getPid());
		sparesApprovalPrice.setSaid(said);

		sparesApprovalPrice.setSalePrice(dto.getSalePrice());

		BigDecimal workHour = dto.getWorkHour();
		BigDecimal workHourUnitCost = dto.getWorkHourUnitCost();
		sparesApprovalPrice.setWorkHour(workHour);
		sparesApprovalPrice.setWorkHourUnitCost(workHourUnitCost);
		sparesApprovalPrice.setWorkHourCost(workHour.multiply(workHourUnitCost));

		return sparesApprovalPrice;
	}
}


