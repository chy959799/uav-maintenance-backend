package com.thinglinks.uav.spare.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.order.dto.OrderSparePrice;
import com.thinglinks.uav.spare.dto.OrderSparePriceQuery;
import com.thinglinks.uav.spare.dto.SparePricePageQuery;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import com.thinglinks.uav.spare.vo.SparePriceVO;
import com.thinglinks.uav.spare.vo.SpareVO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * (ProjectProducts)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-29 16:17:05
 */
public interface ProjectProductService {

	BasePageResponse<SparePriceVO> page(SparePricePageQuery pageQuery);

	ProjectProduct getByPpid(String ppid);

	List<ProjectProduct> getListByPpidIn(List<String> ids);

	void exportSparePrice(List<String> ppids, HttpServletResponse response) throws IOException;

	BaseResponse<SparePriceVO> getSparePrice(String pjid, String pid);

	BaseResponse<List<SpareVO>> getSpareList();

    BaseResponse<List<OrderSparePrice>> getOrderSparePriceList(OrderSparePriceQuery query);
}


