package com.thinglinks.uav.spare.service;

import com.thinglinks.uav.spare.entity.SparesApprovalRecord;

/**
 * (SparesApprovalRecords)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:04
 */
public interface SparesApprovalRecordService {

	void insert(SparesApprovalRecord sparesApprovalRecord);
}


