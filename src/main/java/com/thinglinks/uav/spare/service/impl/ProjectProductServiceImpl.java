package com.thinglinks.uav.spare.service.impl;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.EasyExcelUtils;
import com.thinglinks.uav.order.dto.OrderSparePrice;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.spare.dto.OrderSparePriceQuery;
import com.thinglinks.uav.spare.dto.SparePricePageQuery;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import com.thinglinks.uav.spare.repository.ProjectProductsRepository;
import com.thinglinks.uav.spare.service.ProjectProductService;
import com.thinglinks.uav.spare.vo.SparePriceVO;
import com.thinglinks.uav.spare.vo.SpareVO;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.service.StockService;
import com.thinglinks.uav.user.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (ProjectProducts)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-29 16:17:06
 */
@Service
public class ProjectProductServiceImpl implements ProjectProductService {

    @Resource
    private ProjectProductsRepository projectProductsRepository;
    @Resource
    private StockService stockService;
    @Resource
    private ProductRepository productRepository;

    @Override
    public BasePageResponse<SparePriceVO> page(SparePricePageQuery pageQuery) {
        Specification<ProjectProduct> specification = (root, query, builder) -> {
            Predicate predicate = builder.conjunction();
            // 只显示启用的项目
            predicate = builder.and(predicate, builder.equal(root.get("project").get("enable"), 1));

            if (Objects.nonNull(pageQuery.getProjectName()) && !pageQuery.getProjectName().isEmpty()) {
                predicate = builder.and(predicate, builder.like(root.get("project").get("name"), CommonUtils.assembleLike(pageQuery.getProjectName())));
            }

            if (Objects.nonNull(pageQuery.getSpareName()) && !pageQuery.getSpareName().isEmpty()) {
                predicate = builder.and(predicate, builder.like(root.get("product").get("name"), CommonUtils.assembleLike(pageQuery.getSpareName())));
            }

            if (Objects.nonNull(pageQuery.getCatid()) && !pageQuery.getCatid().isEmpty()) {
                predicate = builder.and(predicate, builder.equal(root.get("product").get("catid"), pageQuery.getCatid()));
            }

            if (Objects.nonNull(pageQuery.getPjid()) && !pageQuery.getPjid().isEmpty()) {
                predicate = builder.and(predicate, builder.equal(root.get("pjid"), pageQuery.getPjid()));
            }

            if (Objects.nonNull(pageQuery.getCode()) && !pageQuery.getCode().isEmpty()) {
                predicate = builder.and(predicate, builder.equal(root.get("product").get("code"), pageQuery.getCode()));
            }

            return predicate;
        };

        Page<ProjectProduct> page = projectProductsRepository.findAll(specification, pageQuery.of());
        List<SparePriceVO> list = page.getContent().stream()
                .map(this::convertToVO)
                .collect(Collectors.toList());

        return BasePageResponse.success(page, list);
    }

    private SparePriceVO convertToVO(ProjectProduct projectProduct) {
        SparePriceVO sparePriceVO = new SparePriceVO();
        sparePriceVO.setPpid(projectProduct.getPpid());
        sparePriceVO.setProjectName(projectProduct.getProject().getName());
        sparePriceVO.setPid(projectProduct.getPid());
        Product product = projectProduct.getProduct();
        sparePriceVO.setSpareName(product.getName());
        Category productCategory = product.getCategory();
        sparePriceVO.setCategory(productCategory.getName());
        sparePriceVO.setCode(product.getCode());
        sparePriceVO.setAgent(product.getAgent());
        sparePriceVO.setOrigin(product.getOrigin());
        sparePriceVO.setModel(product.getModel());
        sparePriceVO.setPurchasePrice(getPurchasePrice(product));
        sparePriceVO.setFactoryPrice(product.getFactoryPrice());
        sparePriceVO.setSalePrice(projectProduct.getSalePrice());
        sparePriceVO.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost());
        sparePriceVO.setWorkHour(projectProduct.getWorkHour());
        sparePriceVO.setWorkHourCost(projectProduct.getWorkHourCost());
        User user = projectProduct.getUser();
        sparePriceVO.setUserName(user.getUserName());
        sparePriceVO.setCreateTime(DateUtils.formatDateTime(projectProduct.getCt()));
        sparePriceVO.setProduct(product);
        sparePriceVO.setCategoryObject(productCategory);
        return sparePriceVO;
    }

    private BigDecimal getPurchasePrice(Product product) {
        Stock stock = stockService.getByPid(product.getPid());
        if (Objects.nonNull(stock)) {
            return BigDecimal.valueOf(stock.getRealPrice());
        }
        return BigDecimal.ZERO;
    }

    @Override
    public ProjectProduct getByPpid(String ppid) {
        return projectProductsRepository.getByPpid(ppid);
    }

    @Override
    public List<ProjectProduct> getListByPpidIn(List<String> ids) {
        return projectProductsRepository.getByPpidIn(ids);
    }

    @Override
    public void exportSparePrice(List<String> ppids, HttpServletResponse response) throws IOException {
        List<SparePriceVO> dataList = projectProductsRepository.findAllByPpidIn(ppids).stream()
                .map(this::convertToVO)
                .collect(Collectors.toList());
        EasyExcelUtils.exportExcel(response, "备件价格", "备件价格", dataList, SparePriceVO.class);
    }

    @Override
    public BaseResponse<SparePriceVO> getSparePrice(String pjid, String pid) {
        ProjectProduct projectProduct = projectProductsRepository.findByPjidAndPid(pjid, pid);
        if (Objects.isNull(projectProduct)) {
            return BaseResponse.fail("未找到对应的配件");
        }
        return BaseResponse.success(this.convertToVO(projectProduct));
    }

    @Override
    public BaseResponse<List<SpareVO>> getSpareList() {
        List<SpareVO> collect = productRepository.findAll().stream().map(product -> {
            SpareVO spareVO = new SpareVO();
            BeanUtils.copyProperties(product, spareVO);
            spareVO.setPurchasePrice(getPurchasePrice(product));
            return spareVO;
        }).collect(Collectors.toList());
        return BaseResponse.success(collect);
    }

    /**
     * 本地维修
     * 成本报价：仓库价格
     * 客户报价：价格库销售价
     * <p>
     * 返厂维修
     * 成本报价：产品表价格
     * 客户报价：价格库销售价
     * <p>
     * 工时费统一由价格库获取
     * @param query
     * @return
     */
    @Override
    public BaseResponse<List<OrderSparePrice>> getOrderSparePriceList(OrderSparePriceQuery query) {
        List<ProjectProduct> projectProducts = projectProductsRepository.findByPjidAndProductCatid(query.getPjid(), query.getCatid());
        if (Objects.isNull(projectProducts) || projectProducts.isEmpty()) {
            return BaseResponse.success(new ArrayList<>());
        }

        List<OrderSparePrice> orderSparePrices;
        if (Objects.equals(query.getType(), 2)) {
            orderSparePrices = projectProducts.stream().map(projectProduct -> {
                Product product = projectProduct.getProduct();
                OrderSparePrice orderSparePrice = new OrderSparePrice();
                orderSparePrice.setFactoryPrice(product.getFactoryPrice().doubleValue());
                orderSparePrice.setSalePrice(projectProduct.getSalePrice().doubleValue());
                orderSparePrice.setWorkHour(projectProduct.getWorkHour().doubleValue());
                orderSparePrice.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost().doubleValue());
                orderSparePrice.setWorkHourCost(projectProduct.getWorkHourCost().doubleValue());
                orderSparePrice.setPid(product.getPid());
                orderSparePrice.setProduct(product);
                return orderSparePrice;
            }).collect(Collectors.toList());
        } else {
            orderSparePrices = projectProducts.stream().map(projectProduct -> {
                OrderSparePrice orderSparePrice = new OrderSparePrice();
                Product product = projectProduct.getProduct();
                Stock stock = stockService.getByPid(product.getPid());
                if (Objects.isNull(stock) || stock.getCount() <= 0) {
                    return null;
                }
                orderSparePrice.setRealPrice(stock.getRealPrice());
                orderSparePrice.setFactoryPrice(product.getFactoryPrice().doubleValue());
                orderSparePrice.setSalePrice(projectProduct.getSalePrice().doubleValue());
                orderSparePrice.setWorkHour(projectProduct.getWorkHour().doubleValue());
                orderSparePrice.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost().doubleValue());
                orderSparePrice.setWorkHourCost(projectProduct.getWorkHourCost().doubleValue());
                orderSparePrice.setPid(product.getPid());
                orderSparePrice.setProduct(product);
                return orderSparePrice;
            }).filter(Objects::nonNull).collect(Collectors.toList());
        }
        return BaseResponse.success(orderSparePrices);
    }
}


