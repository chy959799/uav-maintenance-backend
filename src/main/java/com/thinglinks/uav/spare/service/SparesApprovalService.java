package com.thinglinks.uav.spare.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.spare.dto.SparePriceApprovalPageQuery;
import com.thinglinks.uav.spare.dto.SparePriceVetDTO;
import com.thinglinks.uav.spare.dto.SparesApprovalDTO;
import com.thinglinks.uav.spare.dto.SparesApprovalPriceDTO;
import com.thinglinks.uav.spare.vo.SparePriceApprovalInfoVO;
import com.thinglinks.uav.spare.vo.SparePriceApprovalVO;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * (SparesApprovals)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-29 15:11:02
 */
public interface SparesApprovalService {

	/**
	 * 申请添加备品价格
	 */
	BaseResponse<String> insert(SparesApprovalDTO dto);

	/**
	 * 申请删除备品价格
	 */
	@Transactional
	BaseResponse<String> delete(String ppid);

	/**
	 * 申请批量删除备品价格
	 */
	BaseResponse<String> deleteAll(DeleteDTO dto);

	/**
	 * 申请修改备品价格
	 */
	@Transactional
	BaseResponse<String> update(String ppid, SparesApprovalPriceDTO dto);

	/**
	 * 分页获取备品价格申请列表
	 */
	BasePageResponse<SparePriceApprovalVO> page(SparePriceApprovalPageQuery pageQuery);

	/**
	 * 删除备品价格申请
	 */
	@Transactional
	BaseResponse<String> deleteApproval(String said);

	/**
	 * 获取备品价格申请详情
	 */
	BaseResponse<SparePriceApprovalInfoVO> detailApproval(String said);

	/**
	 * 审核备品价格申请
	 */
	@Transactional
	BaseResponse<String> vetting(String said, SparePriceVetDTO dto);

	BaseResponse<String> importSparePrice(MultipartFile file, HttpServletResponse response) throws IOException;
}


