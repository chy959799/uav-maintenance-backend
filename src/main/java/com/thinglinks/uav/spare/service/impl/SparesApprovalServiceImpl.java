package com.thinglinks.uav.spare.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.constants.SparesConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.EasyExcelUtils;
import com.thinglinks.uav.common.utils.ValidationUtils;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.spare.dto.*;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import com.thinglinks.uav.spare.entity.SparesApproval;
import com.thinglinks.uav.spare.entity.SparesApprovalPrice;
import com.thinglinks.uav.spare.entity.SparesApprovalRecord;
import com.thinglinks.uav.spare.repository.SparesApprovalRepository;
import com.thinglinks.uav.spare.service.ProjectProductService;
import com.thinglinks.uav.spare.service.SparesApprovalPriceService;
import com.thinglinks.uav.spare.service.SparesApprovalRecordService;
import com.thinglinks.uav.spare.service.SparesApprovalService;
import com.thinglinks.uav.spare.vo.SparePriceApprovalInfoVO;
import com.thinglinks.uav.spare.vo.SparePriceApprovalVO;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.service.StockService;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (SparesApprovals)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-29 15:11:02
 */
@Service
@Slf4j
public class SparesApprovalServiceImpl implements SparesApprovalService {

	@Resource
	private SparesApprovalRepository sparesApprovalRepository;

	@Resource
	private SparesApprovalPriceService sparesApprovalPriceService;

	@Resource
	private SparesApprovalRecordService sparesApprovalRecordService;

	@Resource
	private ProjectProductService projectProductService;

	@Resource
	private ProjectRepository projectRepository;

	@Resource
	private StockService stockService;

	@Resource
	private ProductRepository productRepository;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BaseResponse<String> insert(SparesApprovalDTO dto) {
		SparesApproval sparesApproval = saveAddSparesApproval(dto.getPjid(), dto.getDescription());

		List<SparesApprovalPriceDTO> filteredList = dto.getSpareList().stream()
				.distinct()
				.collect(Collectors.toList());

		sparesApprovalPriceService.insertAll(filteredList, sparesApproval.getSaid());
		return BaseResponse.success();
	}

	/**
	 * 保存新增申请单
	 * @param pjid 项目id
	 * @param description 备注
	 * @return SparesApproval
	 */
	private SparesApproval saveAddSparesApproval(String pjid, String description) {
		return saveSparesApproval(pjid, description, SparesConstants.TYPE_ADD);
	}

	/**
	 * 保存申请单
	 * @param pjid 项目id
	 * @param description 备注
	 * @param type  申请单类型
	 * @return SparesApproval
	 */
	private SparesApproval saveSparesApproval(String pjid, String description, String type) {
		SparesApproval sparesApproval = new SparesApproval();
		sparesApproval.setSaid(CommonUtils.uuid());
		sparesApproval.setPjid(pjid);
		sparesApproval.setType(type);
		// 待审核
		sparesApproval.setStatus(SparesConstants.STATUS_FIRST_AUDIT);
		sparesApproval.setCode(DateUtils.codeByTime());
		sparesApproval.setDescription(description);
		sparesApproval.setDid(CommonUtils.getDid());
		sparesApproval.setUid(CommonUtils.getUid());

		sparesApprovalRepository.save(sparesApproval);
		return sparesApproval;
	}

	@Override
	@Transactional
	public BaseResponse<String> delete(String ppid) {
		ProjectProduct projectProduct = projectProductService.getByPpid(ppid);
		if (Objects.isNull(projectProduct)) {
			throw new BusinessException("备件价格不存在");
		}

		if (hasPendingApproval(projectProduct.getPjid(), projectProduct.getPid())) {
			throw new BusinessException("当前备件有正在处理中的申请，请完成后在进行操作");
		}

		SparesApproval sparesApproval = saveDeleteSparesApprovalPrice(projectProduct.getPjid());

		SparesApprovalPriceDTO sparesApprovalPriceDTO = new SparesApprovalPriceDTO();
		sparesApprovalPriceDTO.setPid(projectProduct.getPid());
		sparesApprovalPriceDTO.setSalePrice(projectProduct.getSalePrice());
		sparesApprovalPriceDTO.setWorkHour(projectProduct.getWorkHour());
		sparesApprovalPriceDTO.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost());
		sparesApprovalPriceService.insert(sparesApprovalPriceDTO, sparesApproval.getSaid());

		return BaseResponse.success();
	}

	/**
	 * 保存删除申请单
	 * @param pjid 项目id
	 * @return SparesApproval
	 */
	private SparesApproval saveDeleteSparesApprovalPrice(String pjid) {
		return saveSparesApproval(pjid, null, SparesConstants.TYPE_DELETE);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public BaseResponse<String> deleteAll(DeleteDTO dto) {
		if (Objects.isNull(dto.getIds()) || dto.getIds().isEmpty()) {
			throw new BusinessException("请选择要删除的价格");
		}

		List<ProjectProduct> projectProducts = projectProductService.getListByPpidIn(dto.getIds());
		if (projectProducts.isEmpty()) {
			throw new BusinessException("备件价格不存在");
		}

		Map<String, List<ProjectProduct>> map = projectProducts.stream()
				.filter(projectProduct -> {
					if (hasPendingApproval(projectProduct.getPjid(), projectProduct.getPid())) {
						throw new BusinessException("当前备件有正在处理中的申请，请完成后在进行操作");
					}
					return true;
				})
				.collect(Collectors.groupingBy(ProjectProduct::getPjid));

		map.forEach((pjid, list) -> processProjectProducts(list, pjid));

		return BaseResponse.success();
	}

	/**
	 * 判断是否有正在处理的申请
	 * @param pjid 项目ID
	 * @param pid 产品ID
	 */
	private boolean hasPendingApproval(String pjid, String pid) {
		Long count = sparesApprovalPriceService.countUnValid(pjid, pid);
		return count > 0;
	}

	private void processProjectProducts(List<ProjectProduct> projectProducts, String pjid) {
		SparesApproval sparesApproval = saveDeleteSparesApprovalPrice(pjid);

		List<SparesApprovalPriceDTO> dtos = projectProducts.stream().map(projectProduct -> {
			SparesApprovalPriceDTO sparesApprovalPriceDTO = new SparesApprovalPriceDTO();
			sparesApprovalPriceDTO.setPid(projectProduct.getPid());
			sparesApprovalPriceDTO.setSalePrice(projectProduct.getSalePrice());
			sparesApprovalPriceDTO.setWorkHour(projectProduct.getWorkHour());
			sparesApprovalPriceDTO.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost());
			return sparesApprovalPriceDTO;
		}).collect(Collectors.toList());

		sparesApprovalPriceService.insertAll(dtos, sparesApproval.getSaid());
	}

	@Override
	@Transactional
	public BaseResponse<String> update(String ppid, SparesApprovalPriceDTO dto) {
		ProjectProduct projectProduct = projectProductService.getByPpid(ppid);
		if (Objects.isNull(projectProduct)) {
            throw new BusinessException("备件价格不存在");
		}

		if (hasPendingApproval(projectProduct.getPjid(), projectProduct.getPid())) {
            throw new BusinessException("当前备件有正在处理中的申请，请完成后在进行操作");
		}

		SparesApproval sparesApproval = saveUpdateSparesApprovalPrice(projectProduct.getPjid());

		dto.setPid(projectProduct.getPid());
		sparesApprovalPriceService.insert(dto, sparesApproval.getSaid());

		return BaseResponse.success();
	}

	/**
	 * 保存修改申请单
	 * @param pjid 项目id
	 * @return SparesApproval
	 */
	private SparesApproval saveUpdateSparesApprovalPrice(String pjid) {
		return saveSparesApproval(pjid, null, SparesConstants.TYPE_UPDATE);
	}

	@Override
	public BasePageResponse<SparePriceApprovalVO> page(SparePriceApprovalPageQuery pageQuery) {

		Specification<SparesApproval> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();

			if (Objects.nonNull(pageQuery.getCode()) && !pageQuery.getCode().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("code"), pageQuery.getCode()));
			}

			if (Objects.nonNull(pageQuery.getProjectName()) && !pageQuery.getProjectName().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("project").get("name"), CommonUtils.assembleLike(pageQuery.getProjectName())));
			}

			if (Objects.nonNull(pageQuery.getStatus()) && !pageQuery.getStatus().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("status"), pageQuery.getStatus()));
			}

			return predicate;
		};

		Page<SparesApproval> page = sparesApprovalRepository.findAll(specification, pageQuery.of());
		List<SparePriceApprovalVO> list = page.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(page, list);
	}

	private SparePriceApprovalVO convertToVO(SparesApproval sparesApproval) {
		SparePriceApprovalVO sparePriceApprovalVO = new SparePriceApprovalVO();
		sparePriceApprovalVO.setSaid(sparesApproval.getSaid());
		sparePriceApprovalVO.setCode(sparesApproval.getCode());
		Project project = sparesApproval.getProject();
		if (Objects.nonNull(project)) {
			sparePriceApprovalVO.setProjectName(project.getName());
		}
		sparePriceApprovalVO.setType(sparesApproval.getType());
		sparePriceApprovalVO.setStatus(sparesApproval.getStatus());
		Department department = sparesApproval.getDepartment();
		if (Objects.nonNull(department)) {
			sparePriceApprovalVO.setDepartment(department.getName());
		}
		User user = sparesApproval.getUser();
		if (Objects.nonNull(user)) {
			sparePriceApprovalVO.setUserName(user.getUserName());
		}
		sparePriceApprovalVO.setCreateTime(DateUtils.formatDateTime(sparesApproval.getCt()));
		return sparePriceApprovalVO;
	}

	@Override
	public BaseResponse<String> deleteApproval(String said) {
		SparesApproval sparesApproval = sparesApprovalRepository.getBySaid(said);
		if (Objects.isNull(sparesApproval)) {
			return BaseResponse.fail("申请单不存在");
		}

		if (!couldDelete(sparesApproval)) {
			return BaseResponse.fail("该申请单不能删除");
		}

		sparesApprovalRepository.delete(sparesApproval);
		return BaseResponse.success();
	}

	private boolean couldDelete(SparesApproval sparesApproval) {
		String status = sparesApproval.getStatus();
		// 待初审的可以被创建人删除，结束的需要有删除权限
		if (SparesConstants.STATUS_FIRST_AUDIT.equals(status)) {
			return CommonUtils.getUid().equals(sparesApproval.getUid()) || CommonUtils.checkAuth("sparePriceApprovalDelete");
		} else if (SparesConstants.STATUS_FINISH.equals(status) || SparesConstants.STATUS_REJECT.equals(status)) {
			return CommonUtils.checkAuth("sparePriceApprovalDelete");
		}
		return false;
	}

	@Override
	public BaseResponse<SparePriceApprovalInfoVO> detailApproval(String said) {
		SparesApproval sparesApproval = sparesApprovalRepository.getBySaid(said);

		SparePriceApprovalInfoVO sparePriceApprovalInfoVO = new SparePriceApprovalInfoVO();
		sparePriceApprovalInfoVO.setSaid(sparesApproval.getSaid());
		sparePriceApprovalInfoVO.setCode(sparesApproval.getCode());
        Project project = sparesApproval.getProject();
		if (Objects.nonNull(project)) {
			sparePriceApprovalInfoVO.setProjectName(project.getName());
		}
		sparePriceApprovalInfoVO.setType(sparesApproval.getType());
		sparePriceApprovalInfoVO.setStatus(sparesApproval.getStatus());
		Department department = sparesApproval.getDepartment();
		if (Objects.nonNull(department)) {
			sparePriceApprovalInfoVO.setDepartment(department.getName());
		}
		User user = sparesApproval.getUser();
		sparePriceApprovalInfoVO.setUserName(user.getUserName());
		sparePriceApprovalInfoVO.setDescription(sparesApproval.getDescription());
		sparePriceApprovalInfoVO.setCreateTime(DateUtils.formatDateTime(sparesApproval.getCt()));

		List<SparesApprovalRecord> sparesApprovalRecords = sparesApproval.getSparesApprovalRecords();
		List<SparePriceApprovalInfoVO.RecordVO> recordVOList = sparesApprovalRecords.stream()
				.map(sparesApprovalRecord -> {
					SparePriceApprovalInfoVO.RecordVO recordVO = new SparePriceApprovalInfoVO.RecordVO();
					User recordUseruser = sparesApprovalRecord.getUser();
					recordVO.setUserName(recordUseruser.getUserName());
					recordVO.setStatus(sparesApprovalRecord.getStatus());
					recordVO.setCreateTime(DateUtils.formatDateTime(sparesApprovalRecord.getCt()));
					recordVO.setRemark(sparesApprovalRecord.getRemark());
					return recordVO;
				}).collect(Collectors.toList());
		sparePriceApprovalInfoVO.setRecordList(recordVOList);

		List<SparesApprovalPrice> sparesApprovalPrices = sparesApproval.getSparesApprovalPrices();
		List<SparePriceApprovalInfoVO.SpareVO> spareVOList = sparesApprovalPrices.stream()
				.map(sparesApprovalPrice -> {
					SparePriceApprovalInfoVO.SpareVO spareVO = new SparePriceApprovalInfoVO.SpareVO();
					spareVO.setSapid(sparesApprovalPrice.getSapid());
					Product product = sparesApprovalPrice.getProduct();
					spareVO.setSpareName(product.getName());
					Category productCategory = product.getCategory();
					spareVO.setCategory(productCategory.getName());
					spareVO.setCode(product.getCode());
					spareVO.setAgent(product.getAgent());
					spareVO.setOrigin(product.getOrigin());
					spareVO.setModel(product.getModel());
					spareVO.setFactoryPrice(product.getFactoryPrice());

					Stock stock = stockService.getByPid(product.getPid());
					if (Objects.nonNull(stock)) {
						spareVO.setPurchasePrice(BigDecimal.valueOf(stock.getRealPrice()));
					}

					spareVO.setSalePrice(sparesApprovalPrice.getSalePrice());
					spareVO.setWorkHour(sparesApprovalPrice.getWorkHour());
					spareVO.setWorkHourUnitCost(sparesApprovalPrice.getWorkHourUnitCost());
					spareVO.setWorkHourCost(sparesApprovalPrice.getWorkHourCost());
					return spareVO;
				}).collect(Collectors.toList());
		sparePriceApprovalInfoVO.setSpareList(spareVOList);

		return BaseResponse.success(sparePriceApprovalInfoVO);
	}

	@Override
	@Transactional
	public BaseResponse<String> vetting(String said, SparePriceVetDTO dto) {
		SparesApproval sparesApproval = sparesApprovalRepository.getBySaid(said);
		if (Objects.isNull(sparesApproval)) {
			return BaseResponse.fail("申请单不存在");
		}

		String status = sparesApproval.getStatus();
		if (!cloudVet(status)) {
			return BaseResponse.fail("该申请单不能审批");
		}

		switch (status) {
			case SparesConstants.STATUS_FIRST_AUDIT: {
				if (SparesConstants.PASS.equals(dto.getStatus())) {
					sparesApproval.setStatus(SparesConstants.STATUS_FINAL_AUDIT);
				} else {
					sparesApproval.setStatus(SparesConstants.STATUS_REJECT);
				}
				break;
			}
			case SparesConstants.STATUS_FINAL_AUDIT: {
				if (SparesConstants.PASS.equals(dto.getStatus())) {
					sparesApproval.setStatus(SparesConstants.STATUS_FINISH);
				} else {
					sparesApproval.setStatus(SparesConstants.STATUS_REJECT);
				}
				break;
			}
		}
		sparesApproval = sparesApprovalRepository.save(sparesApproval);

		SparesApprovalRecord sparesApprovalRecord = new SparesApprovalRecord();
		sparesApprovalRecord.setSarid(CommonUtils.uuid());
		sparesApprovalRecord.setSaid(sparesApproval.getSaid());
		sparesApprovalRecord.setUid(CommonUtils.getUid());
		sparesApprovalRecord.setStatus(dto.getStatus());
		sparesApprovalRecord.setRemark(dto.getRemark());
		sparesApprovalRecordService.insert(sparesApprovalRecord);

		// 审批通过后，修改价格
		if (SparesConstants.STATUS_FINISH.equals(sparesApproval.getStatus())) {
			switch (sparesApproval.getType()) {
				case SparesConstants.TYPE_ADD :
				case SparesConstants.TYPE_UPDATE : {
					sparesApprovalPriceService.executeSave(said, sparesApproval.getPjid());
					break;
				}

				case SparesConstants.TYPE_DELETE : {
					sparesApprovalPriceService.executeDelete(said, sparesApproval.getPjid());
					break;
				}
			}
		}

		return BaseResponse.success();
	}

	private boolean cloudVet(String status) {
		return SparesConstants.STATUS_FIRST_AUDIT.equals(status) || SparesConstants.STATUS_FINAL_AUDIT.equals(status);
	}

	@Override
	public BaseResponse<String> importSparePrice(MultipartFile file, HttpServletResponse response) throws IOException {
		EasyExcel.read(file.getInputStream(), SparesApprovalPriceImport.class, new ReadListener<SparesApprovalPriceImport>() {

			/**
			 * 单次缓存的数据量
			 */
			public static final int BATCH_COUNT = 100;

			/**
			 * 临时存储
			 */
			private final List<SparesApprovalPriceDTO> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

			private String cacheProjectCode;

			private String errorCode;

			private String pjid;

			private String apid;

			private final ArrayList<SparesApprovalPriceImport> errorData = new ArrayList<>();

			@Override
			public void onException(Exception exception, AnalysisContext context) {
				log.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
				if (exception instanceof ExcelDataConvertException) {
					ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException)exception;
					String logMessage = String.format("第%d行，第%d列解析异常，数据为:%s",
							excelDataConvertException.getRowIndex() + 1,
							excelDataConvertException.getColumnIndex() + 1,
							excelDataConvertException.getCellData().getStringValue());
					SparesApprovalPriceImport e = new SparesApprovalPriceImport();
					e.setError(logMessage);
					log.error(logMessage);
					errorData.add(e);
				}
			}

			@Override
			public void invoke(SparesApprovalPriceImport data, AnalysisContext context) {
                try {
                    ValidationUtils.validateObject(data);
                } catch (CustomBizException e) {
	                data.setError(e.getMessage());
	                errorData.add(data);
					return;
                }

				if(!productRepository.existsByCode(data.getSpareCode())) {
					data.setError("备件编码不存在");
					errorData.add(data);
					return;
				}

                String projectCode = data.getProjectCode();
				// 如果是第一次读取，检查项目是否存在
				if (cachedDataList.isEmpty()) {
                    try {
                        checkProjectCode(projectCode);
                    } catch (CustomBizException e) {
						data.setError(e.getMessage());
	                    errorData.add(data);
						return;
                    }
					SparesApprovalPriceDTO sparesApprovalPriceDTO = new SparesApprovalPriceDTO();
					BeanUtils.copyProperties(data, sparesApprovalPriceDTO);
					cachedDataList.add(sparesApprovalPriceDTO);
				} else {
					if (!cacheProjectCode.equals(projectCode)) {
						saveData();
						// 存储完成清空列表
						cachedDataList.clear();
						// 重新检查项目是否存在
                        try {
                            checkProjectCode(projectCode);
                        } catch (CustomBizException e) {
	                        data.setError(e.getMessage());
	                        errorData.add(data);
                        }
                    } else {
						SparesApprovalPriceDTO sparesApprovalPriceDTO = new SparesApprovalPriceDTO();
						BeanUtils.copyProperties(data, sparesApprovalPriceDTO);
						cachedDataList.add(sparesApprovalPriceDTO);
						if (cachedDataList.size() >= BATCH_COUNT) {
							saveData();
							// 存储完成清空列表
							cachedDataList.clear();
						}
					}
				}
			}

			private void checkProjectCode(String projectCode) throws CustomBizException {
				if (Objects.nonNull(errorCode) && errorCode.equals(projectCode)) {
					throw new CustomBizException("项目编码不存在");
				}

				Project project = projectRepository.findByCode(projectCode);
				if (Objects.isNull(project)) {
					errorCode = projectCode;
					throw new CustomBizException("项目编码不存在");
				}

				cacheProjectCode = projectCode;
				pjid = project.getPjid();
			}

			@Override
			public void doAfterAllAnalysed(AnalysisContext context) {
				saveData();
				if (!errorData.isEmpty()) {
					try {
						EasyExcelUtils.exportExcel(response, "导入错误数据", "错误信息", errorData, SparesApprovalPriceImport.class);
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
			}

			/**
			 * 存储数据到数据库
			 */
			private void saveData() {
				if (cachedDataList.isEmpty()) {
					return;
				}

				SparesApprovalDTO sparesApprovalDTO = new SparesApprovalDTO();
				sparesApprovalDTO.setPjid(pjid);
				sparesApprovalDTO.setDescription("自动导入");
				sparesApprovalDTO.setSpareList(cachedDataList);

				if (Objects.isNull(apid)) {
					SparesApproval sparesApproval = saveAddSparesApproval(sparesApprovalDTO.getPjid(), sparesApprovalDTO.getDescription());
					apid = sparesApproval.getSaid();
				}
				List<SparesApprovalPriceDTO> filteredList = sparesApprovalDTO.getSpareList().stream()
						.distinct()
						.collect(Collectors.toList());

				sparesApprovalPriceService.insertAll(filteredList, apid);
			}
		}).sheet().doRead();
		return BaseResponse.success();
	}

}