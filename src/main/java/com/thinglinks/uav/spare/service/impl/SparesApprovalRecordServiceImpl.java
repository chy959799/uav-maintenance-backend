package com.thinglinks.uav.spare.service.impl;

import com.thinglinks.uav.spare.entity.SparesApprovalRecord;
import com.thinglinks.uav.spare.repository.SparesApprovalRecordRepository;
import com.thinglinks.uav.spare.service.SparesApprovalRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (SparesApprovalRecords)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:04
 */
@Service
public class SparesApprovalRecordServiceImpl implements SparesApprovalRecordService {

	@Resource
	private SparesApprovalRecordRepository sparesApprovalRecordRepository;

	@Override
	public void insert(SparesApprovalRecord sparesApprovalRecord) {
		sparesApprovalRecordRepository.save(sparesApprovalRecord);
	}
}


