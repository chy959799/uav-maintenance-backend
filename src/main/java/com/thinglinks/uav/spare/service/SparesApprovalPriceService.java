package com.thinglinks.uav.spare.service;

import com.thinglinks.uav.spare.dto.SparesApprovalPriceDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (SparesApprovalPrices)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:30
 */
public interface SparesApprovalPriceService {

	@Transactional
	void insertAll(List<SparesApprovalPriceDTO> spareList, String said);

	@Transactional
	void insert(SparesApprovalPriceDTO sparesApprovalPriceDTO, String said);

	@Transactional
	void executeSave(String said, String ppid);

	@Transactional
	void executeDelete(String said, String pjid);

	/**
	 * 获取未处理完的申请总数
	 * @param pjid  项目ID
	 * @param pid   产品ID
	 * @return 获取未处理完的申请总数
	 */
	Long countUnValid(String pjid, String pid);
}


