package com.thinglinks.uav.spare.repository;

import com.thinglinks.uav.spare.entity.SparesApprovalRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * (SparesApprovalRecords)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:04
 */
public interface SparesApprovalRecordRepository extends JpaRepository<SparesApprovalRecord, Long> {


}


