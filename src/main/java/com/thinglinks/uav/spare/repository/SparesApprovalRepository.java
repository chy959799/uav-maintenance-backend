package com.thinglinks.uav.spare.repository;

import com.thinglinks.uav.spare.entity.SparesApproval;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * (SparesApprovals)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-29 15:11:02
 */
public interface SparesApprovalRepository extends JpaRepository<SparesApproval, Long>, JpaSpecificationExecutor<SparesApproval> {

	SparesApproval getBySaid(String said);
}


