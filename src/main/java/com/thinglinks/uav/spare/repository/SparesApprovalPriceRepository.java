package com.thinglinks.uav.spare.repository;

import com.thinglinks.uav.spare.entity.SparesApprovalPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (SparesApprovalPrices)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-29 15:12:30
 */
public interface SparesApprovalPriceRepository extends JpaRepository<SparesApprovalPrice, Long>, JpaSpecificationExecutor<SparesApprovalPrice> {


	List<SparesApprovalPrice> findAllBySaid(String said);

    boolean existsBySaidAndPid(String said, String pid);
}


