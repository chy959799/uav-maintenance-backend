package com.thinglinks.uav.spare.repository;

import com.thinglinks.uav.spare.entity.ProjectProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (ProjectProducts)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-29 16:17:05
 */
public interface ProjectProductsRepository extends JpaRepository<ProjectProduct, Integer>, JpaSpecificationExecutor<ProjectProduct> {

	ProjectProduct getByPpid(String ppid);

	List<ProjectProduct> getByPjid(String pjid);

	List<ProjectProduct> getByPpidIn(List<String> ids);

	ProjectProduct findByPjidAndPid(String pjid, String pid);

	List<ProjectProduct> findAllByPpidIn(List<String> ppids);

	boolean existsByPidIn(List<String> ids);

	List<ProjectProduct> findByPjidAndProductCatid(String pjid, String catid);
}


