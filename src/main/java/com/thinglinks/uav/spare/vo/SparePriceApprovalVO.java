package com.thinglinks.uav.spare.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
public class SparePriceApprovalVO {

	@ApiModelProperty(value = "申请单id", example = "1")
	private String said;

	@ApiModelProperty(value = "申请单编号", example = "20210428001")
	private String code;

	@ApiModelProperty(value = "项目名称", example = "项目A")
	private String projectName;

	@ApiModelProperty(value = "申请类型", example = "新增")
	private String type;

	@ApiModelProperty(value = "申请状态[0-待初审，1-待终审，2-已完成，3-已驳回]", example = "0")
	private String status;

	@ApiModelProperty(value = "申请部门", example = "售后部")
	private String department;

	@ApiModelProperty(value = "申请人", example = "用户A")
	private String userName;

	@ApiModelProperty(value = "申请时间", example = "2024-03-29 10:00:00")
	private String createTime;
}
