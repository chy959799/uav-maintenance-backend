package com.thinglinks.uav.spare.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
public class SparePriceApprovalInfoVO {

	@ApiModelProperty(value = "申请单id", example = "1")
	private String said;

	@ApiModelProperty(value = "申请单编号", example = "20210428001")
	private String code;

	@ApiModelProperty(value = "项目名称", example = "项目A")
	private String projectName;

	@ApiModelProperty(value = "申请类型", example = "新增")
	private String type;

	@ApiModelProperty(value = "申请状态[0-待初审，1-待终审，2-已完成，3-已驳回]", example = "0")
	private String status;

	@ApiModelProperty(value = "申请部门", example = "售后部")
	private String department;

	@ApiModelProperty(value = "申请人", example = "用户A")
	private String userName;

	@ApiModelProperty(value = "申请时间", example = "2024-03-29 10:00:00")
	private String createTime;

	@ApiModelProperty(value = "申请说明", example = "xxxxxxx")
	private String description;

	@ApiModelProperty(value = "备件信息")
	private List<SpareVO> spareList;

	@ApiModelProperty(value = "审批信息")
	private List<RecordVO> recordList;

	@Data
	public static class SpareVO {

		@ApiModelProperty(value = "sapid", example = "1")
		private String sapid;

		@ApiModelProperty(value = "备件名称", example = "配件1")
		private String spareName;

		@ApiModelProperty(value = "备件类型", example = "类型A")
		private String category;

		@ApiModelProperty(value = "备件编码", example = "001001")
		private String code;

		@ApiModelProperty(value = "厂商编码", example = "x001001")
		private String agent;

		@ApiModelProperty(value = "生产厂家", example = "厂家1")
		private String origin;

		@ApiModelProperty(value = "规格型号", example = "型号1")
		private String model;

		@ApiModelProperty(value = "返厂成本价", example = "10.00")
		private BigDecimal factoryPrice;

		@ApiModelProperty(value = "采购成本价", example = "15.00")
		private BigDecimal purchasePrice;

		@ApiModelProperty(value = "销售价", example = "20.00")
		private BigDecimal salePrice;

		@ApiModelProperty(value = "工时", example = "10.00")
		private BigDecimal workHour;

		@ApiModelProperty(value = "工时单价", example = "10.00")
		private BigDecimal workHourUnitCost;

		@ApiModelProperty(value = "工时费", example = "100.00")
		private BigDecimal workHourCost;
	}

	@Data
	public static class RecordVO {
		@ApiModelProperty(value = "审批人", example = "用户A")
		private String userName;
		@ApiModelProperty(value = "审批时间", example = "2024-03-29 10:00:00")
		private String createTime;
		@ApiModelProperty(value = "审批意见", example = "xxxxxxx")
		private String remark;
		@ApiModelProperty(value = "审批状态[0-驳回，1-通过]", example = "0")
		private String status;
	}
}
