package com.thinglinks.uav.spare.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
@ExcelIgnoreUnannotated
public class SparePriceVO {

	@ApiModelProperty(value = "项目备件id", example = "1")
	private String ppid;

	@ApiModelProperty(value = "项目名称", example = "项目A")
	@ExcelProperty("项目名称")
	private String projectName;

	@ApiModelProperty(value = "备件id", example = "1")
	private String pid;

	@ApiModelProperty(value = "备件名称", example = "备件1")
	@ExcelProperty("备件名称")
	private String spareName;

	@ApiModelProperty(value = "备件类型", example = "类型A")
	@ExcelProperty("备件类型")
	private String category;

	@ApiModelProperty(value = "备件编码", example = "00100")
	@ExcelProperty("备件编码")
	private String code;

	@ApiModelProperty(value = "厂商编码", example = "x00100")
	@ExcelProperty("厂商编码")
	private String agent;

	@ApiModelProperty(value = "生产厂家", example = "厂家1")
	@ExcelProperty("生产厂家")
	private String origin;

	@ApiModelProperty(value = "规格型号", example = "型号1")
	@ExcelProperty("规格型号")
	private String model;

	@ApiModelProperty(value = "返厂成本价", example = "10.00")
	@ExcelProperty("返厂成本价")
	private BigDecimal factoryPrice;

	@ApiModelProperty(value = "采购成本价", example = "15.00")
	@ExcelProperty("采购成本价")
	private BigDecimal purchasePrice;

	@ApiModelProperty(value = "销售价", example = "20.00")
	@ExcelProperty("销售价")
	private BigDecimal salePrice;

	@ApiModelProperty(value = "工时", example = "10.00")
	@ExcelProperty("工时")
	private BigDecimal workHour;

	@ApiModelProperty(value = "工时单价", example = "10.00")
	@ExcelProperty("工时单价")
	private BigDecimal workHourUnitCost;

	@ApiModelProperty(value = "工时费", example = "100.00")
	@ExcelProperty("工时费")
	private BigDecimal workHourCost;

	@ApiModelProperty(value = "添加人", example = "Admin")
	@ExcelProperty("添加人")
	private String userName;

	@ApiModelProperty(value = "添加时间", example = "2024-03-29 10:00:00")
	@ExcelProperty("添加时间")
	private String createTime;

	private Product product;

	private Category categoryObject;
}
