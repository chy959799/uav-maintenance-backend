package com.thinglinks.uav.spare.vo;

import com.thinglinks.uav.product.entity.Product;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/5/11
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SpareVO extends Product {

    private BigDecimal purchasePrice;
}
