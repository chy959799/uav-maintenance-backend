package com.thinglinks.uav.common.task;

import com.thinglinks.uav.order.entity.InsuranceMaterial;
import com.thinglinks.uav.order.entity.OrderSettle;
import com.thinglinks.uav.order.repository.InsuranceMaterialRepository;
import com.thinglinks.uav.order.repository.OrderSettleRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/12
 */
@Configuration
public class InitScriptTask {


    @Resource
    private OrderSettleRepository orderSettleRepository;

    @Resource
    private InsuranceMaterialRepository insuranceMaterialRepository;

    @PostConstruct
    private void init() {
        this.invoiceOut();
    }

    /**
     * 是否开票--保险  结算---上线过后屏蔽
     */
    private void invoiceOut() {
        List<OrderSettle> orderSettles = orderSettleRepository.findAll();
        orderSettles.forEach(x -> {
            if (Objects.isNull(x.getInvoiceNo())) {
                x.setInvoiceOut(Boolean.FALSE);
            } else {
                x.setInvoiceOut(Boolean.TRUE);
            }
        });
        if (CollectionUtils.isNotEmpty(orderSettles)) {
            orderSettleRepository.saveAll(orderSettles);
        }
        List<InsuranceMaterial> insuranceMaterials = insuranceMaterialRepository.findAll();
        insuranceMaterials.forEach(x -> {
            if (Objects.isNull(x.getInvoiceNo())) {
                x.setInvoiceOut(Boolean.FALSE);
            } else {
                x.setInvoiceOut(Boolean.TRUE);
            }
        });
        if (CollectionUtils.isNotEmpty(insuranceMaterials)) {
            insuranceMaterialRepository.saveAll(insuranceMaterials);
        }
    }
}
