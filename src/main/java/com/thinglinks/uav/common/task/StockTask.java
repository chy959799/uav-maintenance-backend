package com.thinglinks.uav.common.task;

import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.entity.StockBorrowItem;
import com.thinglinks.uav.stock.repository.StockBorrowItemRepository;
import com.thinglinks.uav.stock.repository.StockRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class StockTask {

    private static final Integer LIMIT_SIZE = 64;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private StockBorrowItemRepository stockBorrowItemRepository;

    @Scheduled(fixedRate = 3600000)
    public void borrowCountTask() {
        try {
            PageRequest pageRequest = PageRequest.ofSize(LIMIT_SIZE);
            int fetchSize = 0;
            do {
                Page<Stock> stocks = stockRepository.findAll(pageRequest);
                fetchSize = stocks.getContent().size();
                stocks.stream().forEach(x -> {
                    List<StockBorrowItem> stockBorrowItems = stockBorrowItemRepository.findByStoreidAndPid(x.getStoreid(), x.getPid());
                    long brwCount = stockBorrowItems.stream().mapToLong(y -> (y.getCount() - y.getReturnCount())).sum();
                    if (!Objects.equals(brwCount, x.getBorrowCount())) {
                        x.setBorrowCount(brwCount);
                        stockRepository.save(x);
                    }
                });
                pageRequest = pageRequest.next();
            } while (LIMIT_SIZE.equals(fetchSize));
        } catch (Exception e) {
            log.error("borrowCountTask error");
        }

    }
}
