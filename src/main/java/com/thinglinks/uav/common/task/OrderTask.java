package com.thinglinks.uav.common.task;

import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.order.entity.Expresses;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.entity.OrderLog;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.ExpressesRepository;
import com.thinglinks.uav.order.repository.OrderLogRepository;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

import static com.thinglinks.uav.common.constants.OrderConstants.ORDER_IN_INSURANCE;
import static com.thinglinks.uav.order.enums.InsuranceStatusEnums.INSURANCE_FIRST_FINISH;
import static com.thinglinks.uav.order.enums.OrderStatusEnums.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/17
 */

@Component
@Slf4j
public class OrderTask {

    private static final Integer LIMIT_SIZE = 64;

    private static final Long DAY_MILLS_1 = 86400000L;

    private static final Long DAY_MILLS_30 = 30 * 86400000L;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderLogRepository orderLogRepository;

    @Resource
    private ExpressesRepository expressesRepository;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private UserRepository userRepository;

    @Scheduled(fixedRate = 60000)
    public void doAutoTask() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            PageRequest pageRequest = PageRequest.ofSize(LIMIT_SIZE);
            int fetchSize = 0;
            Specification<Order> specification = (root, criteriaQuery, criteriaBuilder) -> {
                List<Predicate> list = new ArrayList<>();
                CriteriaBuilder.In<Object> in = criteriaBuilder.in(root.get("status"));
                in.value(OrderStatusEnums.ORDER_STATUS_WAIT_EXPRESSES.getCode());
                in.value(OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode());
                list.add(in);
                Predicate[] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            };
            do {
                Page<Order> orders = orderRepository.findAll(specification, pageRequest);
                fetchSize = orders.getContent().size();
                orders.stream().forEach(x -> {
                    //
                    if (ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode().equals(x.getStatus())) {
                        Expresses expresses = expressesRepository.findFirstByOrdidAndType(x.getOrdid(), CommonConstants.EXPRESSES_DEVICE_RECEIVE);
                        //收件超过了一天，自动确认
                        if ((expresses.getExpressesAt() + DAY_MILLS_1) < System.currentTimeMillis()) {
                            User user = userRepository.findByUid(x.getCuid());
                            //签署了工单自动确认协议的才能系统自动确认
                            if (CommonConstants.TURE_VALUE.equals(user.getOrderProtocol())) {
                                Device device = deviceRepository.findByDevid(x.getDevid());
                                if (ORDER_IN_INSURANCE.equals(x.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
                                    if (INSURANCE_FIRST_FINISH.getCode().equals(x.getReviewInsuranceStatus())) {
                                        x.setStatus(ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
                                    } else {
                                        x.setStatus(ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode());
                                    }
                                } else {
                                    x.setStatus(ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
                                }
                                orderRepository.save(x);
                                OrderLog orderLog = new OrderLog();
                                orderLog.setDescription("系统自动确认");
                                orderLog.setMethod(OrderConstants.ORDER_LOG_METHOD_CONFIRM_DEVICE);
                                orderLog.setOrdid(x.getOrdid());
                                orderLog.setStatus(x.getStatus());
                                orderLog.setOrdlogid(CommonUtils.uuid());
                                orderLogRepository.save(orderLog);
                            }
                        }
                    }
                    //待收件七天，则失效
                    if (ORDER_STATUS_WAIT_EXPRESSES.getCode().equals(x.getStatus())) {
                        if ((x.getCt() + DAY_MILLS_30) < System.currentTimeMillis()) {
                            x.setStatus(ORDER_STATUS_INVALIDATE.getCode());
                            x.setReviewInsuranceStatus(null);
                            orderRepository.save(x);
                            OrderLog orderLog = new OrderLog();
                            orderLog.setDescription("过期自动失效");
                            orderLog.setMethod(OrderConstants.ORDER_LOG_METHOD_AUTO_INVALIDATE);
                            orderLog.setOrdid(x.getOrdid());
                            orderLog.setStatus(x.getStatus());
                            orderLog.setOrdlogid(CommonUtils.uuid());
                            orderLogRepository.save(orderLog);
                        }
                    }
                });
                pageRequest = pageRequest.next();
            } while (LIMIT_SIZE.equals(fetchSize));
            stopWatch.stop();
            log.info("doAutoTask spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("doAutoTask task error", e);
        }
    }

}
