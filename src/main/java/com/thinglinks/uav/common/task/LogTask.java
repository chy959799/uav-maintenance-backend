package com.thinglinks.uav.common.task;

import com.thinglinks.uav.log.repository.LogsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author iwiFool
 * @date 2024/5/24
 */
@Component
@Slf4j
public class LogTask {

    private static final Long DAY_MILLS_15 = 15 * 86400000L;
    @Resource
    private LogsRepository logsRepository;

    @Scheduled(cron = "0 0 4 * * ?") // 每天早上4点执行
    public void cleanupLogs() {
        logsRepository.deleteAllOlderThan(System.currentTimeMillis() - DAY_MILLS_15);
        log.info("清理15天前的日志");
    }
}
