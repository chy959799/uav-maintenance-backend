package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 赔付意向书
 * @date 2024/4/17 12:39
 */
@Data
public class Repay {
    // 出险单位
    private String customerName;
    //保单号
    private String insurancesNumber;
    // 报案号
    private String incidentNumber;
    // 出险日期
    private String year;
    private String month;
    private String day;
    // 事故地点
    private String accidentLocation;
    // 事故原因
    private String accidentCause;
    // 赔偿金额
    private String amountWords;
    private String amount;
    // date
    private String nowYear;
    private String nowMonth;
    private String nowDay;
}
