package com.thinglinks.uav.common.template;

import lombok.Data;

@Data
public class AccidentCertificate {
    private String date;
    // 飞手名称
    private String name;
    // 出险单位
    private String company;
    // 地点
    private String location;
    /// 作业类型
    private String type;
    // 事故原因
    private String reason;
    // 受损情况
    private String situation;
    // 设备sn码
    private String code;
}
