package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 电力业务出险通知及索赔申请
 * @date 2024/4/18 18:36
 */
@Data
public class ClaimApplication {
    // 出险人单位 公司
    private String company1;
    // 保单号
    private String insurancesNumber;
    // 报案号
    private String incidentNumber;
    // 产品名称
    private String productName;
    // 出险人单位
    private String company2;
    // 联系人姓名
    private String contactName;
    // 联系人电话
    private String contactPhone;
    // 出险时间
    private String accidentTimeDay;
    // 事故处置经过
    private String accidentReason;
    // 出现时间到小时
    private String accidentTimeHour;
    //飞手名称
    private String pilotName;
}
