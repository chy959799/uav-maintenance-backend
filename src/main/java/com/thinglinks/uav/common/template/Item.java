package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/20 14:10
 */
@Data
public class Item {
    private String productName;
    //产品规格 “机型”
    private String productSpec;
    //产品单位
    private String productUnit;
    // 损失数量
    private String lossQuantity;
    // 客户报价金额
    private String customerPrice;
    //设备SN码
    private String snCode;
}
