package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 案件报告数据
 * @date 2024/4/21 0:31
 */
@Data
public class IncidentReport {
    // 被保险人
    private String customer;
    // 出险时间
    private String accidentTime;
    // 保险单号
    private String policyNo;
    // 报案号
    private String reportNo;
    // 险种
    private String insuranceType;
    // 损失标的
    private String lossSubjectMatter;
    // 出险地点
    private String accidentLocation;
    // 赔付金额
    private String compensationAmount;
    // 机身编码
    private String deviceCode;

    // 承保时间
    private String underwritingTime;
    // 承保信息 (需提前编辑好录入系统数据库)
    private String underwritingInformation;

    // 事发经过及情况
    private String accidentReason;

    // 事故照片 照片路径(bucketName/objectName) 多个照片用;分隔

    // 事故现场照片
    private String accidentPhoto;
    // 设备SN号照片
    private String snCodePhoto;
    // 设备正面照片
    private String deviceFrontPhoto;
    // 设备背面照片
    private String deviceBackPhoto;
    // 设备其他照片
    private String deviceOtherPhoto;

    // 保险生效日期
    private String startDate;

    // 保险失效日期
    private String endDate;

    // 出险日期
    private String accidentDate;

    // 无人机维修报价单
    private String maintenanceQuotation;

    private String reportTime;
}
