package com.thinglinks.uav.common.template;

import lombok.Data;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 维修报价单
 * @date 2024/4/30 19:07
 */
@Data
public class QuotationPrice {
    //客户名称
    private String customer;
    //飞机型号
    private String deviceModel;
    //飞机SN码
    private String snCode;
    //报价日期
    private String quotationDate;
    //联系人
    private String contact;
    //定损结果 产品部分
    private String product;
    //定损结果 原因部分
    private String lossAssessmentResult;
    // 标准价格
    private String price;
    // 总计
    private String totalAmount;
    // 物料清单
    private List<MaterialItem> materialList;
    // 日期
    private String date;
}
