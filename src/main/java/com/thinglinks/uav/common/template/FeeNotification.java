package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 收费通知
 * @date 2024/5/1 23:21
 */
@Data
public class FeeNotification {
    // 保单号
    private String insurancesNumber;
    // 报案号
    private String incidentNumber;
    // 险种
    private String insuranceType;
    // 出险原因
    private String accidentReason;
    // 出险时间
    private String accidentTimeDay;
    // 报案联系人
    private String reportContact;
    // 出险地点
    private String accidentLocation;
    // 委托日期
    private String entrustDate;
    // 联系电话
    private String contactPhone;
    // 维修费小计
    private String repairFeeSubtotal;
    // 费用合计
    private String totalFee;
}
