package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 报案文件
 * @date 2024/4/30 17:09
 */
@Data
public class Report {
    // 日期
    private String date;

    // 报案人
    private String reporter;

    // 报案人联系电话
    private String reporterPhone;

    //保单号
    private String insurancesNumber;
    // 单位
    private String company;
    // 出险时间
    private String accidentTimeDay;
    // 机身编码
    private String deviceCode;
    //  出险地点
    private String accidentLocation;
    // 操作员
    private String operator;
    // 报损金额
    private String damageAmount;

}
