package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 事故照片实体类  图片均为 "bucketName/objectName" 格式
 * @date 2024/4/19 17:12
 */
@Data
public class FaultPhoto {
    // 事故现场
    private String accidentScene;
    // 损坏物品清单
    private String manifest;
    // 损坏设备
    private String damageDevice;
    // 正面照片
    private String front;
    // 背面照片
    private String back;
    // 其他照片
    private String other;
}
