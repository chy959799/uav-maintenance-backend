package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/30 19:17
 */
@Data
public class MaterialItem {
    // 物料名称
    private String materialName;
    // 单位
    private String unit;
    // 数量
    private String number;
    // 单价
    private String unitPrice;
    // 工时费
    private String manHourCost;
    // 金额
    private String amount;
}
