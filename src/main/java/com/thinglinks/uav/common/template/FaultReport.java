package com.thinglinks.uav.common.template;

import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 无人机故障报告
 * @date 2024/4/19 10:17
 */
@Data
public class FaultReport {
    // 出险时间 精确到天
    private String accidentTimeDay;

    // 设备SN号
    private String snCode;

    // 故障处理经过
    private String handlingProcess;

    // 设备型号 产品名称+设备型号+设备sn码
    private String deviceModel;

    // 生成厂家
    private String manufacturer;

    // 产品 产品名称+规格型号
    private String product;

    // 故障原因
    private String faultReason;
}
