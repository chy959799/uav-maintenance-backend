package com.thinglinks.uav.common.template;

import lombok.Data;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 损失清单
 * @date 2024/4/18 21:25
 */
@Data
public class LossItem {
    // 出险单位
    private String customerName;
    // 故障地点
    private String faultPlace;
    private List<Item> items;
}
