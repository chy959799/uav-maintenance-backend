package com.thinglinks.uav.common.sms;

import com.alibaba.fastjson.JSON;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.dysmsapi20170525.models.SendSmsResponseBody;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.thinglinks.uav.system.exception.CustomBizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * @author iwiFool
 * @date 2024/3/17
 */
@Slf4j
@Service
public class AliYunSmsServiceImpl implements AliYunSmsService {

    @Value("${sms.aliyun.accessKeyID}")
    private String accessKeyID;

    @Value("${sms.aliyun.accessKeySecret}")
    private String accessKeySecret;

    @Value("${sms.aliyun.endpoint}")
    private String endpoint;

    @Value("${sms.aliyun.templateCode}")
    private String templateCode;

    @Value("${sms.aliyun.signName}")
    private String signName;

    @Value("${sms.send:false}")
    private boolean sendSms;

    @Value("${sms.code:1234}")
    private String code;

    @Override
    public boolean sendSms(String phoneNum, String message) throws Exception {
        //是否可以发短信，不能发送，默认使用默认验证码
        if (!sendSms) {
            throw new DefaultMobileCaptchaException(code);
        }
        Config profile = new Config()
                .setAccessKeyId(accessKeyID)
                .setAccessKeySecret(accessKeySecret)
                .setEndpoint(endpoint);
        Client client = new Client(profile);

        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName(signName)
                .setTemplateCode(templateCode)
                .setPhoneNumbers(phoneNum)
                .setTemplateParam("{\"code\":\"" + message + "\"}");

        RuntimeOptions runtime = new RuntimeOptions();
        try {
            SendSmsResponse sendSmsResponse = client.sendSmsWithOptions(sendSmsRequest, runtime);
            SendSmsResponseBody smsResponseBody = sendSmsResponse.body;
            //返回 OK 代表请求成功。
            if ("OK".equals(smsResponseBody.code)) {
                return true;
            } else {
                log.info("发送短信失败，错误信息：{}", smsResponseBody.message);
                throw new CustomBizException(smsResponseBody.message);
            }
        } catch (CustomBizException e) {
            throw e;
        } catch (Exception e) {
            log.error("阿里云发送短信时发生异常" + e.getMessage());
            return false;
        }
    }

    @Override
    public void sendSms(String phoneNum, String templateParam, String templateCode) throws Exception {
        if (!sendSms) {
            return;
        }
        Config profile = new Config()
                .setAccessKeyId(accessKeyID)
                .setAccessKeySecret(accessKeySecret)
                .setEndpoint(endpoint);
        Client client = new Client(profile);

        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName(signName)
                .setTemplateCode(templateCode)
                .setPhoneNumbers(phoneNum)
                .setTemplateParam(templateParam);

        RuntimeOptions runtime = new RuntimeOptions();
        try {
            log.info("发送短信请求：phoneNum:{}, templateParam:{},templateCode:{}", phoneNum, templateParam, templateCode);
            SendSmsResponse sendSmsResponse = client.sendSmsWithOptions(sendSmsRequest, runtime);
            log.info("发送短信返回：{}", JSON.toJSONString(sendSmsResponse.body));
        } catch (Exception e) {
            log.error("阿里云发送短信时发生异常" + e.getMessage());
        }
    }
}