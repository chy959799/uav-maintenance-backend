package com.thinglinks.uav.common.sms;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
public interface AliYunSmsService {
    /**
     * 发送短信的接口
     * @param phoneNum 手机号
     * @param message     消息
     * @return boolean 是否发送成功
     */
    boolean sendSms(String phoneNum, String message) throws Exception;

    /**
     * 发送短信的接口
     * @param phoneNum 手机号
     * @param templateParam     消息
     * @return boolean 是否发送成功
     */
    void sendSms(String phoneNum, String templateParam, String templateCode) throws Exception;
}