package com.thinglinks.uav.common.sms;

import com.thinglinks.uav.system.exception.BusinessException;
import lombok.Getter;

/**
 * @author iwiFool
 * @date 2024/7/13
 */
@Getter
public class DefaultMobileCaptchaException extends BusinessException {

    private final String defaultMobileCaptcha;

    public DefaultMobileCaptchaException(String defaultMobileCaptcha) {
        this.defaultMobileCaptcha = defaultMobileCaptcha;
    }

}
