package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: fanhaiqiu
 * @date: 2023/6/29
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TimePageRequest extends BasePageRequest {

    @ApiModelProperty(value = "开始时间戳")
    private Long startTime;

    @ApiModelProperty(value = "结束时间戳")
    private Long endTime;

}
