package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@Data
public class FilesRequest {

    @ApiModelProperty(value = "文件0")
    private MultipartFile file0;

    @ApiModelProperty(value = "文件1")
    private MultipartFile file1;

    @ApiModelProperty(value = "文件2")
    private MultipartFile file2;

    @ApiModelProperty(value = "文件3")
    private MultipartFile file3;

    @ApiModelProperty(value = "文件4")
    private MultipartFile file4;

    @ApiModelProperty(value = "文件5")
    private MultipartFile file5;

    @ApiModelProperty(value = "文件6")
    private MultipartFile file6;

    @ApiModelProperty(value = "文件7")
    private MultipartFile file7;

    @ApiModelProperty(value = "文件8")
    private MultipartFile file8;

    @ApiModelProperty(value = "文件9")
    private MultipartFile file9;

    public Boolean checkFileIsNull() {
        return Objects.isNull(this.file0);
    }
}
