package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author fanhaiqiu
 * @date 2022/2/8
 */
@Data
public class PageMeta {

    @ApiModelProperty(value = "页大小")
    private Integer limit;

    @ApiModelProperty(value = "跳过元素数量")
    private Integer offset;

    @ApiModelProperty(value = "排序规则")
    private String order;

    @ApiModelProperty(value = "元素数量")
    private Long count;
}
