package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileVO {

    @ApiModelProperty("内部id")
    private String fid;

    @ApiModelProperty("访问的链接地址")
    private String url;

    @ApiModelProperty("文件名称")
    private String name;

    @ApiModelProperty("是否可下载")
    private Boolean download;

    @ApiModelProperty("文件创建时间")
    private String createTime;
}
