package com.thinglinks.uav.common.dto;

import javax.validation.groups.Default;

/**
 * @author iwiFool
 * @date 2024/7/30
 */
public class BaseDTO {

    public interface Insert extends Default {

    }
}
