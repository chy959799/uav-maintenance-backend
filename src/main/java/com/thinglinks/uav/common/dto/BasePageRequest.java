package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/20
 */
@Data
public class BasePageRequest {

    @ApiModelProperty(value = "当前叶，从0开始")
    private int offset = 0;

    @ApiModelProperty(value = "每页大小，默认10")
    private int limit = 10;

    @ApiModelProperty(value = "排序规则：eg:-createdAt表示按创建时间倒序；createdAt表示正序")
    private String order;

    /**
     * 分页参数转换
     *
     * @return
     */
    public PageRequest of() {
        Sort sort = Sort.by(Sort.Direction.DESC, "ut");
        if (StringUtils.isNotBlank(order)) {
            if (order.startsWith("-")) {
                if (order.equals("-createdAt")) {
                    sort = Sort.by(Sort.Direction.DESC, "ct");
                } else if (order.equals("-updatedAt")) {
                    Sort.by(Sort.Direction.DESC, "ut");
                } else {
                    sort = Sort.by(Sort.Direction.DESC, order.substring(1));
                }
            } else {
                if (order.equals("createdAt")) {
                    sort = Sort.by(Sort.Direction.ASC, "ct");
                } else if (order.equals("updatedAt")) {
                    sort = Sort.by(Sort.Direction.ASC, "ut");
                } else {
                    sort = Sort.by(Sort.Direction.ASC, order);
                }
            }
        }
        return PageRequest.of(offset / limit, limit, sort);
    }

}
