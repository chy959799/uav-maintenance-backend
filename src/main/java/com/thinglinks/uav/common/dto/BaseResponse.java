package com.thinglinks.uav.common.dto;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.enums.StatusEnum;

import java.io.Serializable;

/**
 * @author fanhaiqiu
 * @date 2019/4/9
 */
public class BaseResponse<T> implements Serializable {

    @JSONField(ordinal = 1)
    @JsonProperty(index = 1)
    private int code;

    @JSONField(ordinal = 2)
    @JsonProperty(index = 2)
    private String msg;

    @JSONField(ordinal = 3)
    @JsonProperty(index = 3)
    private T results;

    /**
     * 创建成功的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> success() {
        BaseResponse<T> response = new BaseResponse<>();
        response.setCode(StatusEnum.SUCCESS.getCode());
        response.setMsg(StatusEnum.SUCCESS.getMessage());
        return response;
    }

    /**
     * 创建成功的返回结构
     *
     * @param results
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> success(T results) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setCode(StatusEnum.SUCCESS.getCode());
        response.setMsg(StatusEnum.SUCCESS.getMessage());
        response.setResults(results);
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> fail() {
        BaseResponse<T> response = new BaseResponse<>();
        response.setCode(StatusEnum.FAIL.getCode());
        response.setMsg(StatusEnum.FAIL.getMessage());
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> fail(String message) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setCode(StatusEnum.FAIL.getCode());
        response.setMsg(message);
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> fail(StatusEnum statusEnum) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setCode(statusEnum.getCode());
        response.setMsg(statusEnum.getMessage());
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param code
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> fail(int code, String message) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setCode(code);
        response.setMsg(message);
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BaseResponse<T> invalid(T token) {
        BaseResponse<T> response = new BaseResponse<>();
        response.setResults(token);
        response.setCode(StatusEnum.INVALID_TOKEN.getCode());
        response.setMsg(StatusEnum.INVALID_TOKEN.getMessage());
        return response;
    }

    /**
     * 创建builder
     *
     * @param <T>
     * @return
     */
    public static <T> Builder<T> builder() {
        return new Builder<T>();
    }

    public static class Builder<t> {

        private int code;

        private String msg;

        private t results;

        public Builder code(int code) {
            this.code = code;
            return this;
        }

        public Builder msg(String message) {
            this.msg = message;
            return this;
        }

        public Builder results(t results) {
            this.results = results;
            return this;
        }

        public BaseResponse<t> build() {
            BaseResponse<t> response = new BaseResponse();
            response.setResults(this.results);
            response.setCode(this.code);
            response.setMsg(this.msg);
            return response;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T results) {
        this.results = results;
    }

}
