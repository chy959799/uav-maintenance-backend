package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/9
 */
@Data
public class Remark {

    @ApiModelProperty(value = "备注,如果你客户发起取消，不需要填原因")
    private String remark;
}
