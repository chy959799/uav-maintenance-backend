package com.thinglinks.uav.common.dto;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.enums.StatusEnum;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * @author fanhaiqiu
 * @date 2019/4/9
 */
@Data
public class BasePageResponse<T> implements Serializable {

    @JSONField(ordinal = 1)
    @JsonProperty(index = 1)
    private int code;

    @JSONField(ordinal = 2)
    @JsonProperty(index = 2)
    private String msg;

    @JSONField(ordinal = 3)
    @JsonProperty(index = 3)
    private List<T> results;

    @JSONField(ordinal = 4)
    @JsonProperty(index = 4)
    private PageMeta meta;

    /**
     * 创建成功的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BasePageResponse<T> success(Page page, List<T> results) {
        BasePageResponse<T> response = new BasePageResponse<>();
        response.setCode(StatusEnum.SUCCESS.getCode());
        response.setMsg(StatusEnum.SUCCESS.getMessage());
        response.setResults(results);
        PageMeta meta = new PageMeta();
        meta.setLimit(page.getSize());
        meta.setOffset(page.getSize() * page.getNumber());
        meta.setCount(page.getTotalElements());
        meta.setOrder("-default");
        response.setMeta(meta);
        return response;
    }

    /**
     * 创建成功的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BasePageResponse<T> success(Page page, List<T> results, String order) {
        BasePageResponse<T> response = BasePageResponse.success(page, results);
        response.getMeta().setOrder(order);
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BasePageResponse<T> fail() {
        BasePageResponse<T> response = new BasePageResponse<>();
        response.setCode(StatusEnum.FAIL.getCode());
        response.setMsg(StatusEnum.FAIL.getMessage());
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BasePageResponse<T> fail(String message) {
        BasePageResponse<T> response = new BasePageResponse<>();
        response.setCode(StatusEnum.FAIL.getCode());
        response.setMsg(message);
        return response;
    }

    /**
     * 创建失败的返回结构
     *
     * @param <T>
     * @return
     */
    public static <T> BasePageResponse<T> fail(StatusEnum statusEnum) {
        BasePageResponse<T> response = new BasePageResponse<>();
        response.setCode(statusEnum.getCode());
        response.setMsg(statusEnum.getMessage());
        return response;
    }

}
