package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/23
 */
@Data
public class ReviewResult {

    @ApiModelProperty(value = "审核结果：passed：通过；rejected：驳回")
    @NotBlank
    private String reviewResult;

    @ApiModelProperty(value = "审核备注")
    private String reviewRemark;

}
