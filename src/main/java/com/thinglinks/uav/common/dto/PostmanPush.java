package com.thinglinks.uav.common.dto;

import lombok.Data;

/**
 * 邮政排揽结果推送参数
 * @author iwiFool
 * @date 2024/4/27
 */
@Data
public class PostmanPush {

	/**
	 * 物流订单号（客户内部订单号）
	 */
	private String logisticsOrderNo;

	/**
	 * 物流运单号
	 */
	private String waybillNo;

	/**
	 * 揽收结果
	 * 揽收成功：1
	 * 揽收失败(拒收)：0
	 */
	private String status;

	/**
	 * 系统完结/手工完结
	 */
	private String finalStates;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 失败原因
	 * 失败时返回失败原因：空柜、
	 * 超重、违禁品、超范围、订单
	 * 信息错误（拒收原因待补充）
	 */
	private String desc;

	/**
	 * 总资费
	 */
	private Double postageTotal;

	/**
	 * 失败原因代码
	 * 揽收失败原因：
	 * 5、撤销
	 * 6、客户未下订单
	 * 7、超范围
	 * 8、超禁制
	 * 9、超规格
	 * 10、恶意下单
	 * 12、因资费原因放弃
	 * 13、上门揽收不及时放弃
	 * 16、已联系客户并确认重复下单
	 * 19、客户自交寄
	 * 20、客户转交其他公司
	 * 21、其他原因
	 */
	private String pickupErrorCode;

	/**
	 * 附加原因
	 */
	private String returnAdditionReason;

	/**
	 * 保价金额
	 */
	private Double insuranceAmount;

	/**
	 * 重量
	 */
	private Double weight;
}
