package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/24
 */
@Data
@ApiModel(description = "批量删除DTO")
public class DeleteDTO {
	@ApiModelProperty(value = "*id数组", example = "01a5dc1c45e04df3aae894fe2ac55e04,43ff4e4da62a48538ae2ac0ebab5f6e1")
	private List<String> ids;
}
