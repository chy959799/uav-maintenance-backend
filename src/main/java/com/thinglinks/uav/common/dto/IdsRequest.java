package com.thinglinks.uav.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class IdsRequest {

    @ApiModelProperty(value = "id集合")
    @NotNull
    private List<String> ids;
}
