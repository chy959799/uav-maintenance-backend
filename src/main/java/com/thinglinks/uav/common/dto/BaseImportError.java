package com.thinglinks.uav.common.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/5/27
 */
@Data
public class BaseImportError {

    @ExcelProperty("错误信息")
    private String error;
}
