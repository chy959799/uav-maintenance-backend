package com.thinglinks.uav.common.repository;

import com.thinglinks.uav.common.entity.County;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2023/6/7
 */
public interface CountyRepository extends JpaRepository<County, Long> {

    List<County> findByCityCode(String code);

    County findByAreaCode(String code);

    boolean existsByAreaCode(String code);

    County findByAreaName(String name);
}
