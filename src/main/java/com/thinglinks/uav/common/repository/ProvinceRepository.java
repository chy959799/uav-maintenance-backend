package com.thinglinks.uav.common.repository;


import com.thinglinks.uav.common.entity.Province;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author: fanhaiqiu
 * @date: 2023/6/7
 */
public interface ProvinceRepository extends JpaRepository<Province, Long> {

    Province findByProvinceCode(String code);

    Province findByProvinceName(String name);
}
