package com.thinglinks.uav.common.repository;


import com.thinglinks.uav.common.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2023/6/7
 */
public interface CityRepository extends JpaRepository<City, Long> {

    List<City> findByProvinceCode(String code);

    City findByCityCode(String code);

    boolean existsByCityCode(String code);

    City findByCityName(String name);
}
