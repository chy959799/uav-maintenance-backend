package com.thinglinks.uav.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;

/**
 * @author iwiFool
 * @date 2024/7/30
 */
public class AbbreviationUtils {

    public static String getAbbreviation(String name) {
        StringBuilder abbreviation = new StringBuilder();
        for (char c : name.toCharArray()) {
            if (Character.isWhitespace(c)) {
                continue;
            }
            String[] pinyins = PinyinHelper.toHanyuPinyinStringArray(c);
            if (pinyins != null && pinyins.length > 0) {
                abbreviation.append(Character.toUpperCase(pinyins[0].charAt(0)));
            } else {
                abbreviation.append(Character.toUpperCase(c));
            }
        }
        return abbreviation.toString();
    }
}
