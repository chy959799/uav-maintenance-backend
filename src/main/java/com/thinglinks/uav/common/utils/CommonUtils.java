package com.thinglinks.uav.common.utils;

import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.constants.UserConstants;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.system.common.Constant;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.system.exception.CustomBizException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import sun.font.FontDesignMetrics;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.image.BufferedImage;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: fanhaiqiu
 * @date: 2023/3/17
 */
public class CommonUtils {

    private static String FILE_PATTERN = "(.*?)\\.(png|jpeg|jpg|gif)$";

    private static final Integer FILE_LENGTH = 2;

    /**
     * 中文数字
     */
    final static private String[] CHINESE_NUMBER = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    /**
     * 中文数字单位
     */
    final static private String[] CHINESE_NUMBER_UNIT = {"", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟"};
    /**
     * 人民币单位
     */
    final static private String[] CHINESE_MONEY_UNIT = {"圆", "角", "分"};

    /**
     * 获取用户主键id
     *
     * @return
     */
    public static Integer getUserId() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (Integer) session.getAttribute(Constant.USER_ID);
    }

    /**
     * 获取用户id
     *
     * @return
     */
    public static String getUid() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (String) session.getAttribute(Constant.UID);
    }

    public static String getDid() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (String) session.getAttribute(Constant.DID);
    }

    public static String getCid() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (String) session.getAttribute(Constant.CID);
    }

    public static String getCpid() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (String) session.getAttribute(Constant.CPID);
    }

    /**
     * 获取用户类型
     *
     * @return
     */
    public static int getUserType() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (Integer) session.getAttribute(Constant.USER_TYPE);
    }

    /**
     * 获取用户角色
     */
    public static List<String> getUserRoles() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (List<String>) session.getAttribute(Constant.ROLES);
    }

    /**
     * 获取用户权限
     *
     * @return
     */
    public static List<String> getUserAuth() {
        HttpServletRequest session = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return (List<String>) session.getAttribute(Constant.USER_AUTH);
    }

    /**
     * uuid
     *
     * @return
     */
    public static String uuid() {
        return StringUtils.replaceChars(UUID.randomUUID().toString(), "-", "");
    }

    public static Boolean isAdmin() {
        List<String> userRoles = getUserRoles();
        return userRoles.contains(UserConstants.ADMIN_ROLE);
    }

    public static Boolean isSystemManagement() {
        List<String> userRoles = getUserRoles();
        return userRoles.contains(UserConstants.ADMIN_ROLE) || userRoles.contains(UserConstants.SYSTEM_MANAGEMENT_ROLE);
    }

    public static Boolean isSystemUser() {
        return getUserType() == UserTypeEnum.ADMIN.getCode();
    }

    public static Boolean isConsumerUser() {
        if (getUserType() == UserTypeEnum.USER.getCode()) {
            return true;
        }

        List<String> userRoles = getUserRoles();
        return userRoles.contains(UserConstants.USER_ROLE);
    }

    public static Boolean isSystem() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (Objects.isNull(requestAttributes)) {
            return true;
        }

        HttpServletRequest session = ((ServletRequestAttributes) requestAttributes).getRequest();
        return (Boolean) session.getAttribute(Constant.SYSTEM);
    }

    public static Boolean isExcludedPath() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        AntPathRequestMatcher ant1 = new AntPathRequestMatcher("/open/**");
        AntPathRequestMatcher ant2 = new AntPathRequestMatcher("/swagger3-doc/**");
        AntPathRequestMatcher ant3 = new AntPathRequestMatcher("/*/api-docs");
        AntPathRequestMatcher ant4 = new AntPathRequestMatcher("/captchas/*");
        AntPathRequestMatcher ant5 = new AntPathRequestMatcher("/login");
        AntPathRequestMatcher ant6 = new AntPathRequestMatcher("/register");
        AntPathRequestMatcher ant7 = new AntPathRequestMatcher("/customers/tree");
        //快递推送接口
        AntPathRequestMatcher ant8 = new AntPathRequestMatcher("/express/open/**");
        AntPathRequestMatcher ant9 = new AntPathRequestMatcher("/files/upload");
        AntPathRequestMatcher ant10 = new AntPathRequestMatcher("/system/open/**");
        boolean b = ant1.matches(request) || ant2.matches(request) || ant3.matches(request) || ant4.matches(request) || ant5.matches(request)
                || ant6.matches(request) || ant7.matches(request) || ant8.matches(request) || ant9.matches(request) || ant10.matches(request);

        if (b) {
            request.setAttribute(Constant.SYSTEM, true);
        }
        return b;
    }

    public static Boolean checkAuth(String auth) {
        List<String> userAuth = getUserAuth();
        return isAdmin() || userAuth.contains(auth);
    }

    /**
     * 图片格式校验
     *
     * @param fileName
     * @return
     */
    public static boolean checkPictureFormat(String fileName) {
        Pattern r = Pattern.compile(FILE_PATTERN);
        Matcher m = r.matcher(fileName);
        return m.matches();
    }

    /**
     * 获取文件后缀名
     *
     * @param fileName
     * @return
     */
    public static String getExtension(String fileName) {
        String extension = StringUtils.EMPTY;
        if (StringUtils.isBlank(fileName)) {
            return extension;
        }
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }

    /**
     * int2BBoolean
     *
     * @param value
     * @return
     */
    public static Boolean int2BBoolean(Integer value) {
        return (Objects.isNull(value) || value == 0) ? Boolean.FALSE : Boolean.TRUE;
    }

    /**
     * int2BBoolean
     *
     * @param value
     * @return
     */
    public static Integer boolean2Int(Boolean value) {
        return (Objects.isNull(value) || !value) ? 0 : 1;
    }

    public static String getClientIp(HttpServletRequest request) {
        String remoteAddress = request.getHeader("X-Forwarded-For");
        if (remoteAddress == null || remoteAddress.isEmpty() || "unknown".equalsIgnoreCase(remoteAddress)) {
            remoteAddress = request.getHeader("X-Real-IP");
        }
        if (remoteAddress == null || remoteAddress.isEmpty() || "unknown".equalsIgnoreCase(remoteAddress)) {
            remoteAddress = request.getRemoteAddr();
        }
        return remoteAddress;
    }

    /**
     * like
     *
     * @param v
     * @return
     */
    public static String assembleLike(String v) {
        return "%".concat(v).concat("%");
    }

    /**
     * 保留两位小数
     *
     * @param v
     * @return
     */
    public static double format2Point(double v) {
        return BigDecimal.valueOf(v).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * 处理文字大小 生成文字图片
     *
     * @param str
     * @param width
     * @param height
     * @return 文本图片
     */
    public static BufferedImage strToImage(String str, int width, int height) {
        BufferedImage textImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) textImage.getGraphics();
        //开启文字抗锯齿
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setBackground(Color.WHITE);
        g2.clearRect(0, 0, width, height);
        g2.setPaint(Color.BLACK);
        FontRenderContext context = g2.getFontRenderContext();
        Font font = new Font("微软雅黑", Font.BOLD, 22);
        g2.setFont(font);
        LineMetrics lineMetrics = font.getLineMetrics(str, context);
        FontMetrics fontMetrics = FontDesignMetrics.getMetrics(font);
        int offset = (width - fontMetrics.stringWidth(str)) / 2;
        float y = (height + lineMetrics.getAscent() - lineMetrics.getDescent() - lineMetrics.getLeading()) / 2;
        g2.drawString(str, offset, (int) y);
        return textImage;
    }

    /**
     * 获取等于空的属性
     *
     * @param source
     * @return
     */
    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    /**
     * 工单类型转换
     *
     * @return
     */
    public static String orderTypeConvert(String type) throws Exception {
        if (OrderConstants.ORDER_MAINTAIN.equals(type)) {
            return "维修";
        } else if (OrderConstants.ORDER_UPKEEP.equals(type)) {
            return "保养";
        } else {
            throw new CustomBizException("工单类型错误");
        }
    }

    /**
     * 工单服务类型转换
     *
     * @return
     */
    public static String orderServiceTypeConvert(String serviceType) throws Exception {
        if (OrderConstants.ORDER_IN_INSURANCE.equals(serviceType)) {
            return "保内维修";
        } else if (OrderConstants.ORDER_OUT_INSURANCE.equals(serviceType)) {
            return "框架合同维修";
        } else if (OrderConstants.ORDER_SERVICE_QUICKLY.equals(serviceType)) {
            return "快修快换";
        } else if (OrderConstants.ORDER_SERVICE_INVALID.equals(serviceType)) {
            return "作废";
        } else {
            throw new CustomBizException("工单服务类型错误");
        }
    }

    /**
     * 工单保险类型转换
     *
     * @return
     */
    public static String orderInsuranceTypeConvert(String insuranceType) throws Exception {
        if (OrderConstants.INSURANCE_DJI_CARE.equals(insuranceType)) {
            return "DJI Care续享";
        } else if (OrderConstants.INSURANCE_DJI_CARE_X.equals(insuranceType)) {
            return "DJI Care续享";
        } else if (OrderConstants.INSURANCE_HYWY_BASIC.equals(insuranceType)) {
            return "行业无忧基础版";
        } else if (OrderConstants.INSURANCE_HYWY_BASIC_X.equals(insuranceType)) {
            return "行业无忧基础续享版";
        } else if (OrderConstants.INSURANCE_HYWY_PRO.equals(insuranceType)) {
            return "行业无忧旗舰版";
        } else if (OrderConstants.INSURANCE_HYWY_PRO_X.equals(insuranceType)) {
            return "行业无忧旗舰续享版";
        } else if (OrderConstants.INSURANCE_YDC.equals(insuranceType)) {
            return "英大财险";
        } else if (OrderConstants.INSURANCE_DH.equals(insuranceType)) {
            return "鼎和财险";
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * int2BBoolean
     *
     * @param value
     * @return
     */
    public static String boolean2String(Boolean value) {
        return (Objects.isNull(value) || !value) ? "否" : "是";
    }

    /**
     * @param sourceMoney 要转换的数值，最多支持到亿
     * @return 结果
     */
    public static String toChineseMoney(BigDecimal sourceMoney) {
        if (new BigDecimal("1000000000000").compareTo(sourceMoney) <= 0
                && BigDecimal.ZERO.compareTo(sourceMoney) >= 0) {
            throw new BusinessException("支持转换的金额范围为0~1万亿");
        }
        StringBuilder sb = new StringBuilder();
        // 整数部分
        BigDecimal intPart = sourceMoney.setScale(0, RoundingMode.DOWN);
        // 小数部分
        BigDecimal decimalPart = sourceMoney.subtract(intPart).multiply(new BigDecimal(100)).setScale(0,
                RoundingMode.DOWN);

        // 处理整数部分圆
        if (intPart.compareTo(BigDecimal.ZERO) > 0) {
            String intPartNumberString = intPart.toPlainString();
            int length = intPartNumberString.length();
            // 统计末尾的零，末尾零不做处理
            int zeroCount = 0;
            for (int i = length - 1; i >= 0; i--) {
                int number = Integer.parseInt(String.valueOf(intPartNumberString.charAt(i)));
                if (number == 0) {
                    zeroCount++;
                } else {
                    break;
                }
            }
            for (int i = 0; i < length; i++) {
                // 如果转换到末尾0，则停止转换
                if (i + zeroCount == length) {
                    break;
                }
                int number = Integer.parseInt(String.valueOf(intPartNumberString.charAt(i)));
                // 获取中文数字
                String chineseNumber = CHINESE_NUMBER[number];
                // 获取中文数字单位
                String chineseNumberUnit = CHINESE_NUMBER_UNIT[length - i - 1];
                sb.append(chineseNumber).append(chineseNumberUnit);
            }
            // 统计完后加上金额单位
            sb.append(CHINESE_MONEY_UNIT[0]);
        } else {
            sb.append(CHINESE_NUMBER[0]).append(CHINESE_MONEY_UNIT[0]);
        }

        // 处理小数部分
        if (decimalPart.compareTo(new BigDecimal(10)) >= 0) {
            // 角
            String jiao = decimalPart.toPlainString();
            int number = Integer.parseInt(String.valueOf(jiao.charAt(0)));
            if (number != 0) {
                String chineseNumber = CHINESE_NUMBER[number];
                sb.append(chineseNumber).append(CHINESE_MONEY_UNIT[1]);
            }

            // 分
            String fen = decimalPart.toPlainString();
            number = Integer.parseInt(String.valueOf(fen.charAt(1)));
            if (number != 0) {
                String chineseNumber = CHINESE_NUMBER[number];
                sb.append(chineseNumber).append(CHINESE_MONEY_UNIT[2]);
            }
        } else if (decimalPart.compareTo(BigDecimal.ZERO) > 0) {
            // 分
            String fen = decimalPart.toPlainString();
            int number = Integer.parseInt(String.valueOf(fen.charAt(0)));
            if (number != 0) {
                String chineseNumber = CHINESE_NUMBER[number];
                sb.append(chineseNumber).append(CHINESE_MONEY_UNIT[2]);
            }
        } else {
            sb.append("整");
        }
        return sb.toString();
    }

    /**
     * 获取文件名称
     *
     * @param file
     * @return
     */
    public static String fileName(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        if (StringUtils.isNotBlank(originalFilename)) {
            String[] name = originalFilename.split("\\.");
            if (name.length == FILE_LENGTH) {
                return originalFilename;
            }
        }
        return file.getName();
    }

    /**
     * fa
     *
     * @param value
     * @return
     */
    public static String stringNotNull(String value) {
        return Objects.isNull(value) ? StringUtils.EMPTY : value;
    }

    /**
     * @param status
     * @param beforeStatus
     * @return
     */
    public static boolean checkStatus(String status, String... beforeStatus) {
        return Arrays.asList(beforeStatus).contains(status);
    }

    /**
     * @param data
     * @return
     */
    public static String formatTosepara(Double data) {
        if (Objects.isNull(data)) {
            return StringUtils.EMPTY;
        }
        DecimalFormat df = new DecimalFormat("#,###.00");
        return df.format(data);
    }
}
