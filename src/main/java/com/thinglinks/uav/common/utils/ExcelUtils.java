package com.thinglinks.uav.common.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;
import java.util.Map;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/23 15:29
 */
public class ExcelUtils {
    public static boolean checkTemplate(String[] title, List<List<Object>> fileTitle) {
        for (int i = 0; i < fileTitle.get(0).size(); i++) {
            if (!fileTitle.get(0).get(i).equals(title[i])) {
                return false;
            }
        }
        return true;
    }

    public static <T> Workbook createErrExcel(Map<T, String> data, Map<String, String> headersMap, String fileName) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(fileName);
        // 创建表头
        Row headerRow = sheet.createRow(0);
        int colNum = 0;
        for (Map.Entry<String, String> entry : headersMap.entrySet()) {
            Cell cell = headerRow.createCell(colNum);
            cell.setCellValue(entry.getValue());
            colNum++;
        }
        Cell cell = headerRow.createCell(colNum);
        cell.setCellValue("错误原因");
        // 写入数据
        int rowNum = 1;
        for (Map.Entry<T, String> entry : data.entrySet()) {
            Row row = sheet.createRow(rowNum++);
            T item = entry.getKey();
            String validationMessage = entry.getValue();

            colNum = 0;
            for (Map.Entry<String, String> header : headersMap.entrySet()) {
                String propertyName = header.getKey();
                try {
                    Object propertyValue = item.getClass().getMethod("get" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1)).invoke(item);
                    row.createCell(colNum).setCellValue(propertyValue.toString());
                } catch (Exception e) {
                    row.createCell(colNum).setCellValue("");
                }
                colNum++;
            }
            row.createCell(colNum).setCellValue(validationMessage);
        }
        return workbook;
    }

}
