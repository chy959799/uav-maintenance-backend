package com.thinglinks.uav.common.utils;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.RegisteredPayload;
import com.thinglinks.uav.system.common.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT 工具类
 *
 * @author haoxr
 * @since 2.6.0
 */
@Component
@Slf4j
public class JwtUtils {

    /**
     * JWT 加解密使用的密钥
     */
    private static byte[] key;

    @Value("${jwt.key}")
    public void setKey(String key) {
        JwtUtils.key = key.getBytes();
    }

    /**
     * 生成 JWT Token
     */
    public static String generateToken(Integer id) {
        Map<String, Object> payload = new HashMap<>();
        payload.put(Constant.USER_ID, id);
        Date now = new Date();
        payload.put(RegisteredPayload.ISSUED_AT, now);
        payload.put(RegisteredPayload.JWT_ID, IdUtil.simpleUUID());
        return JWTUtil.createToken(payload, JwtUtils.key);
    }

    /**
     * 解析 JWT Token 获取载体信息
     *
     * @param token JWT Token
     * @return 载体信息
     */
    public static Map<String, Object> parseToken(String token) {
        try {
            if (CharSequenceUtil.isBlank(token)) {
                return Collections.emptyMap();
            }
            if (token.startsWith("Bearer ")) {
                token = token.substring(7);
            }
            JWT jwt = JWTUtil.parseToken(token);
            if (jwt.setKey(JwtUtils.key).validate(0)) {
                return jwt.getPayloads();
            }
        } catch (Exception e) {
            log.error("parseToken error", e);
        }
        return Collections.emptyMap();
    }


}
