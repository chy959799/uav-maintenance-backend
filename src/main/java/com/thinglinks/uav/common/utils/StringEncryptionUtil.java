package com.thinglinks.uav.common.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author iwiFool
 * @date 2024/3/27
 */
public class StringEncryptionUtil {

	/**
	 * 字符串加密
	 * @param str 要加密的字符串
	 * @return 加密后的字符串
	 */
	/**
	 * 字符串加密
	 * @param str 要加密的字符串
	 * @return 加密后的字符串
	 */
	public static String encode(String str) {
		String encoded = Base64.getUrlEncoder().encodeToString(str.getBytes(StandardCharsets.UTF_8));
		encoded = replaceEndingEqualsWithLength(encoded);
		return new StringBuilder(encoded).reverse().toString();
	}

	public static String replaceEndingEqualsWithLength(String str) {
		String regex = "=*$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);

		if (matcher.find()) {
			int length = matcher.group().length();
			return str.substring(0, str.length() - length) + length;
		}

		return str;
	}

	/**
	 * 字符串解密
	 * @param str 要解密的字符串
	 * @return 解密后的字符串
	 */
	public static String decode(String str) {
		if (str != null && !str.isEmpty()) {
			String reversedStr = new StringBuilder(str).reverse().toString();
			String base64Str = reversedStr.substring(0, reversedStr.length() - 1);
			int paddingLength = Integer.parseInt(reversedStr.substring(reversedStr.length() - 1));
			String decodedStr = base64Str + new String(new char[paddingLength]).replace("\0", "=");
			byte[] decodedBytes = Base64.getDecoder().decode(decodedStr);
			return new String(decodedBytes);
		}
		return "";
	}

	public static boolean validate(String password) {
		// 密码必须包含至少一个大写字母、一个小写字母和一个特殊字符
		String pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[*?!&￥$%^#,./@\";:><\\[\\]}{\\-=+_\\\\|》《。，、？'‘“”~ `]).+$";
		return Pattern.matches(pattern, password);
	}
}

