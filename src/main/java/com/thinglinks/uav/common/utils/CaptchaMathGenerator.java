package com.thinglinks.uav.common.utils;

import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.core.math.Calculator;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author iwiFool
 * @date 2024/5/20
 */
public class CaptchaMathGenerator implements CodeGenerator {
    private static final long serialVersionUID = -5514819971774091076L;

    private static final String operators = "+-*";

    /** 参与计算数字最大长度 */
    private final int numberLength;

    /**
     * 构造
     */
    public CaptchaMathGenerator() {
        this(2);
    }

    /**
     * 构造
     *
     * @param numberLength 参与计算最大数字位数
     */
    public CaptchaMathGenerator(int numberLength) {
        this.numberLength = numberLength;
    }

    @Override
    public String generate() {
        final int limit = getLimit();
        int number1 = RandomUtil.randomInt(1, limit);
        int number2 = RandomUtil.randomInt(1, limit);
        char operator = RandomUtil.randomChar(operators);

        if (operator == '-') {
            if (number1 < number2) {
                int temp = number1;
                number1 = number2;
                number2 = temp;
            }
        }

        String formattedNumber1 = StrUtil.padAfter(String.valueOf(number1), this.numberLength, CharUtil.SPACE);
        String formattedNumber2 = StrUtil.padAfter(String.valueOf(number2), this.numberLength, CharUtil.SPACE);

        return StrUtil.builder()
                .append(formattedNumber1)
                .append(operator)
                .append(formattedNumber2)
                .append('=').toString();
    }

    @Override
    public boolean verify(String code, String userInputCode) {
        int result;
        try {
            result = Integer.parseInt(userInputCode);
        } catch (NumberFormatException e) {
            // 用户输入非数字
            return false;
        }

        final int calculateResult = (int) Calculator.conversion(code);
        return result == calculateResult;
    }

    /**
     * 获取验证码长度
     *
     * @return 验证码长度
     */
    public int getLength() {
        return this.numberLength * 2 + 2;
    }

    /**
     * 根据长度获取参与计算数字最大值
     *
     * @return 最大值
     */
    private int getLimit() {
        return Integer.parseInt("1" + StrUtil.repeat('0', this.numberLength));
    }
}