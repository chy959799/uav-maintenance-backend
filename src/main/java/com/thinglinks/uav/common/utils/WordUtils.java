package com.thinglinks.uav.common.utils;

import com.spire.doc.*;
import com.spire.doc.documents.*;
import com.spire.doc.fields.DocPicture;
import com.spire.doc.fields.TextRange;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.graphics.PdfImageType;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.template.*;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.system.exception.CustomBizException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/18 10:17
 */
@Component
@Slf4j
public class WordUtils {
    // 赔付意向书模板
    private static final String REPAY_TEMPLATE_PATH = "template/intention_to_pay.docx";
    // 电力业务出险通知及索赔申请书
    private static final String POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM_TEMPLATE_PATH = "template/power_risk_claim.docx";
    private static final String POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM_TEMPLATE_V2_PATH = "template/power_risk_claim_v2.docx";
    // 损失清单
    private static final String LOSS_LIST_TEMPLATE_PATH = "template/claim_statement.docx";
    // 故障报告
    private static final String FAULT_REPORT_TEMPLATE_PATH = "template/failure_report.docx";
    // 事故照片
    private static final String FAULT_PHOTO_TEMPLATE_PATH = "template/accident_photograph.docx";
    // 案件报告
    private static final String INCIDENT_REPORT_TEMPLATE_PATH = "template/case_report.docx";
    // 报案文件
    private static final String REPORT_DOCUMENT_TEMPLATE_PATH = "template/report.docx";
    // 报价单
    private static final String QUOTATION_PRICE_TEMPLATE_PATH = "template/quotation_price_sheet.docx";
    // 收费通知书
    private static final String NOTIFICATION_TEMPLATE_PATH = "template/notification.docx";
    // 事故证明
    private static final String ACCIDENT_CERTIFICATE_TEMPLATE_PATH = "template/accident_certificate.docx";
    private static final String PDF = ".pdf";
    private static final String MAINTENANCE_QUOTATION = "maintenanceQuotation";
    private static final String GENERATE_SUFFIX = ".docx";
    private static final String PNG_SUFFIX = ".png";
    private static final String NOT_MATCH_TEMPLATE = "无对应模板";
    private static final String TEMPLATE_FILE_EXCEPTION = "模板文件异常";
    private static final String IMAGE_PATH_ERROR = "图片路径错误";
    // 签章图片路径
    private static final String STAMP_IMG_URL = "template/sign_1.png";
    private static final String IMAGE_TARGET = "image:";
    private static final String TEMPLATE_REGEX = "\\[([^\\[\\]]*)]";
    private static final Pattern TEMPLATE_PATTERN = Pattern.compile(TEMPLATE_REGEX);
    private static final String IMAGE_REGEX = "(.+/.+;.+/.+)|(.+/.+)";
    private static final int MIN_ROW_COUNT = 18;

    @Resource
    private CustomMinIOClient minIOClient;

    @Resource
    private CustomConfig customConfig;

    private static final Map<Class<?>, String> templateMap = new HashMap<>();

    static {
        templateMap.put(Repay.class, REPAY_TEMPLATE_PATH);
        templateMap.put(ClaimApplication.class, POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM_TEMPLATE_PATH);
        templateMap.put(ClaimApplicationV2.class, POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM_TEMPLATE_V2_PATH);
        templateMap.put(LossItem.class, LOSS_LIST_TEMPLATE_PATH);
        templateMap.put(FaultReport.class, FAULT_REPORT_TEMPLATE_PATH);
        templateMap.put(FaultPhoto.class, FAULT_PHOTO_TEMPLATE_PATH);
        templateMap.put(IncidentReport.class, INCIDENT_REPORT_TEMPLATE_PATH);
        templateMap.put(Report.class, REPORT_DOCUMENT_TEMPLATE_PATH);
        templateMap.put(QuotationPrice.class, QUOTATION_PRICE_TEMPLATE_PATH);
        templateMap.put(FeeNotification.class, NOTIFICATION_TEMPLATE_PATH);
        templateMap.put(AccidentCertificate.class, ACCIDENT_CERTIFICATE_TEMPLATE_PATH);
    }

    ;

    /**
     * @param data
     * @return File
     * @Author huiyongchen
     * @Description 常规生成pdf文件
     * @Date 1:21 2024/4/30
     **/
    public File generatePdf(Object data) throws Exception {
        String template = checkTemplate(data);
        if (template == null) {
            throw new CustomBizException(NOT_MATCH_TEMPLATE);
        }
        ClassPathResource resource = new ClassPathResource(template);
        InputStream inputStream = resource.getInputStream();

        Map<String, Object> map = WordUtils.convertObjectToMap(data);
        Document document = new Document();
        document.loadFromStream(inputStream, FileFormat.Docx);
        TextSelection[] selections = document.findAllPattern(TEMPLATE_PATTERN);
        for (TextSelection selection : selections) {
            TextRange range = selection.getAsOneRange();
            String text = selection.getSelectedText();
            text = text.replaceAll("\\[|\\]", "");
            // 填充图片内容
            if (text.startsWith(IMAGE_TARGET) && map.containsKey(text.replace(IMAGE_TARGET, ""))) {
                text = text.replace(IMAGE_TARGET, "");
                Paragraph paragraph = range.getOwnerParagraph();
                paragraph.setText("");
                Object o = map.get(text);
                String imageUrl = o.toString();
                // 处理案件报告
                if (INCIDENT_REPORT_TEMPLATE_PATH.equals(template) && MAINTENANCE_QUOTATION.equals(text)) {
                    String bucketName = imageUrl.split("/")[0];
                    String objectName = imageUrl.split("/")[1];
                    byte[] fileBytes = minIOClient.downloadFile(bucketName, objectName);
                    try (FileOutputStream fos = new FileOutputStream(customConfig.getPdfTempDir().concat(objectName))) {
                        fos.write(fileBytes);
                    }
                    File file = pdfToPng(new File(customConfig.getPdfTempDir().concat(objectName)));
                    DocPicture picture = paragraph.appendPicture(file.getAbsolutePath());
                    int targetWidth = 120;
                    double aspectRatio = (double) picture.getWidth() / picture.getHeight(); // 计算原始图像的宽高比
                    int targetHeight;
                    if (picture.getWidth() > targetWidth) {
                        targetHeight = (int) (targetWidth / aspectRatio);
                    } else {
                        targetHeight = (int) (targetWidth * aspectRatio);
                    }
                    picture.setWidth(targetWidth * 4);
                    picture.setHeight(targetHeight * 4);
                    continue;
                }
                // 判断图片路径是否正确
                if (!imageUrl.matches(IMAGE_REGEX)) {
                    paragraph.setText(IMAGE_PATH_ERROR);
                    continue;
                }
                String[] split = imageUrl.split(";");
                for (String url : split) {
                    String bucketName = url.split("/")[0];
                    String objectName = url.split("/")[1];
                    byte[] fileBytes = minIOClient.downloadFile(bucketName, objectName);
                    DocPicture picture = paragraph.appendPicture(fileBytes);
                    int targetWidth = 120;
                    double aspectRatio = (double) picture.getWidth() / picture.getHeight(); // 计算原始图像的宽高比
                    int targetHeight;
                    if (picture.getWidth() > targetWidth) {
                        targetHeight = (int) (targetWidth / aspectRatio);
                    } else {
                        targetHeight = (int) (targetWidth * aspectRatio);
                    }
                    picture.setWidth(targetWidth * 2);
                    picture.setHeight(targetHeight * 2);
                }
            }
            // 填充文本内容
            if (map.containsKey(text)) {
                range.setText(map.get(text).toString());
            }
        }
        String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid().concat(GENERATE_SUFFIX));
        document.saveToFile(fileName, FileFormat.Docx);
        document.dispose();
        // 转换为pdf文件
        File tempDocfile = new File(fileName);
        // 案件报告和案件文件只需要生成docx文档
        if (REPORT_DOCUMENT_TEMPLATE_PATH.equals(template) || INCIDENT_REPORT_TEMPLATE_PATH.equals(template)) {
            return tempDocfile;
        }
        String path = tempDocfile.getAbsolutePath();
        String command = "libreoffice --headless --invisible --convert-to pdf:writer_pdf_Export " + path + " --outdir " + customConfig.getPdfTempDir();
        try {
            boolean result = executeLinuxCmd(command);
            if (result) {
                log.info("转换成功");
            }
        } catch (Exception e) {
            log.error("转换失败,原因：执行命令时出现异常。", e);
        }
        // 删除临时doc文件
        FileUtils.deleteQuietly(tempDocfile);
        return new File(fileName.replace(GENERATE_SUFFIX, PDF));
    }

    /**
     * @param templateFile data
     * @return File
     * @Author huiyongchen
     * @Description 根据传入的模板文件常规生成pdf文件
     * @Date 1:21 2024/4/30
     **/
    public File generatePdf(File templateFile, Object data) throws Exception {
        InputStream inputStream = Files.newInputStream(templateFile.toPath());
        if (!templateFileValidation(templateFile, data)) {
            throw new CustomBizException(TEMPLATE_FILE_EXCEPTION);

        }
        Map<String, Object> map = WordUtils.convertObjectToMap(data);
        Document document = new Document();
        document.loadFromStream(inputStream, FileFormat.Docx);
        TextSelection[] selections = document.findAllPattern(TEMPLATE_PATTERN);
        for (TextSelection selection : selections) {
            TextRange range = selection.getAsOneRange();
            String text = selection.getSelectedText();
            text = text.replaceAll("\\[|\\]", "");
            // 填充图片内容
            if (text.startsWith(IMAGE_TARGET) && map.containsKey(text.replace(IMAGE_TARGET, ""))) {
                text = text.replace(IMAGE_TARGET, "");
                Paragraph paragraph = range.getOwnerParagraph();
                paragraph.setText("");
                Object o = map.get(text);
                String imageUrl = o.toString();
                // 处理案件报告
                if (data instanceof QuotationPrice && MAINTENANCE_QUOTATION.equals(text)) {
                    String bucketName = imageUrl.split("/")[0];
                    String objectName = imageUrl.split("/")[1];
                    byte[] fileBytes = minIOClient.downloadFile(bucketName, objectName);
                    try (FileOutputStream fos = new FileOutputStream(customConfig.getPdfTempDir().concat(objectName))) {
                        fos.write(fileBytes);
                    }
                    File file = pdfToPng(new File(customConfig.getPdfTempDir().concat(objectName)));
                    DocPicture picture = paragraph.appendPicture(file.getAbsolutePath());
                    int targetWidth = 120;
                    double aspectRatio = (double) picture.getWidth() / picture.getHeight(); // 计算原始图像的宽高比
                    int targetHeight;
                    if (picture.getWidth() > targetWidth) {
                        targetHeight = (int) (targetWidth / aspectRatio);
                    } else {
                        targetHeight = (int) (targetWidth * aspectRatio);
                    }
                    picture.setWidth(targetWidth * 4);
                    picture.setHeight(targetHeight * 4);
                    continue;
                }
                // 判断图片路径是否正确
                if (!imageUrl.matches(IMAGE_REGEX)) {
                    paragraph.setText(IMAGE_PATH_ERROR);
                    continue;
                }
                String[] split = imageUrl.split(";");
                for (String url : split) {
                    String bucketName = url.split("/")[0];
                    String objectName = url.split("/")[1];
                    byte[] fileBytes = minIOClient.downloadFile(bucketName, objectName);
                    DocPicture picture = paragraph.appendPicture(fileBytes);
                    int targetWidth = 120;
                    double aspectRatio = (double) picture.getWidth() / picture.getHeight(); // 计算原始图像的宽高比
                    int targetHeight;
                    if (picture.getWidth() > targetWidth) {
                        targetHeight = (int) (targetWidth / aspectRatio);
                    } else {
                        targetHeight = (int) (targetWidth * aspectRatio);
                    }
                    picture.setWidth(targetWidth * 2);
                    picture.setHeight(targetHeight * 2);
                }
            }
            // 填充文本内容
            if (map.containsKey(text)) {
                range.setText(map.get(text).toString());
            }
        }
        String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid().concat(GENERATE_SUFFIX));
        document.saveToFile(fileName, FileFormat.Docx);
        document.dispose();
        // 转换为pdf文件
        File tempDocfile = new File(fileName);
        // 案件报告和案件文件只需要生成docx文档
//        if (REPORT_DOCUMENT_TEMPLATE_PATH.equals(template) || INCIDENT_REPORT_TEMPLATE_PATH.equals(template)) {
        if (data instanceof Report || data instanceof IncidentReport) {
            return tempDocfile;
        }
        String path = tempDocfile.getAbsolutePath();
        String command = "libreoffice --headless --invisible --convert-to pdf:writer_pdf_Export " + path + " --outdir " + customConfig.getPdfTempDir();
        try {
            boolean result = executeLinuxCmd(command);
            if (result) {
                log.info("转换成功");
            }
        } catch (Exception e) {
            log.error("转换失败,原因：执行命令时出现异常。", e);
        }
        // 删除临时doc文件
        FileUtils.deleteQuietly(tempDocfile);
        return new File(fileName.replace(GENERATE_SUFFIX, PDF));
    }

    /**
     * author huiyongchen
     * 生成损耗物品清单 需要循环遍历数据所以单独封装
     *
     * @param lossItems 损失清单列表
     * @return 生成的PDF文件路径
     */
    public File generateLossItems(List<LossItem> lossItems) throws IOException, CustomBizException {
        String templatePath = checkTemplate(lossItems.get(0));
        if (templatePath == null) {
            throw new CustomBizException(NOT_MATCH_TEMPLATE);
        }
        ClassPathResource resource = new ClassPathResource(templatePath);
        InputStream inputStream = resource.getInputStream();
        return getLossItemsFile(lossItems, inputStream);
    }

    /**
     * author huiyongchen
     * 生成损耗物品清单 需要循环遍历数据所以单独封装
     *
     * @param lossItems 损失清单列表
     * @return 生成的PDF文件路径
     */
    public File generateLossItems(List<LossItem> lossItems, InputStream inputStream) throws IOException, CustomBizException {
        return getLossItemsFile(lossItems, inputStream);
    }

    /**
     * author huiyongchen
     * 生成损耗物品清单 需要循环遍历数据所以单独封装(根据传入的模板文件生成)
     *
     * @param templateFile lossItems 损失清单列表
     * @return 生成的PDF文件路径
     */
    public File generateLossItems(File templateFile, List<LossItem> lossItems) throws IOException, CustomBizException {
        InputStream inputStream = Files.newInputStream(templateFile.toPath());
        return getLossItemsFile(lossItems, inputStream);
    }

    private @NotNull File getLossItemsFile(List<LossItem> lossItems, InputStream inputStream) {
        Document document = new Document();
        document.loadFromStream(inputStream, FileFormat.Docx);
        // 填充数据
        Map<String, Object> map = new HashMap<>();
        map.put("customerName", lossItems.get(0).getCustomerName());
        TextSelection[] selections = document.findAllPattern(TEMPLATE_PATTERN);
        for (TextSelection selection : selections) {
            TextRange range = selection.getAsOneRange();
            String text = selection.getSelectedText();
            text = text.replaceAll("\\[|\\]", "");
            if (map.containsKey(text)) {
                range.setText(map.get(text).toString());
            }
        }

        Section section = document.getSections().get(0);
        Table table = section.getTables().get(0);
        int rowNum = 0;
        int lastNum = 0;
        for (LossItem lossItem : lossItems) {
            rowNum += lossItem.getItems().size();
        }
        for (int i = 0; i < rowNum; i++) {
            table.addRow();
        }
        if (rowNum < MIN_ROW_COUNT) {
            lastNum = MIN_ROW_COUNT;
            for (int i = 0; i < lastNum - rowNum; i++) {
                table.addRow();
            }
        }

        for (int i = 2, j = 0; i < rowNum + 2; j++) {
            int size = lossItems.get(j).getItems().size();
            table.applyVerticalMerge(0, i, i + size - 1);
            table.applyVerticalMerge(1, i, i + size - 1);
            table.get(i, 0).addParagraph().appendText(String.valueOf(j + 1))
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 1).addParagraph().appendText(lossItems.get(j).getFaultPlace())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            List<Item> items = lossItems.get(j).getItems();
            for (int q = 0; q < items.size(); q++, i++) {
                Item item = items.get(q);
                table.get(i, 2).addParagraph().appendText(item.getProductName())
                        .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
                table.get(i, 3).addParagraph().appendText(item.getProductSpec())
                        .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
                table.get(i, 4).addParagraph().appendText(item.getProductUnit())
                        .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
                table.get(i, 5).addParagraph().appendText(item.getLossQuantity())
                        .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
                table.get(i, 7).addParagraph().appendText(item.getCustomerPrice())
                        .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
                table.get(i, 8).addParagraph().appendText(item.getSnCode())
                        .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            }
        }
        // 构建表尾
        table.get(lastNum + 2, 0).addParagraph().appendText("合计")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Left);
        table.applyHorizontalMerge(lastNum + 2, 1, 8);
        table.autoFit(AutoFitBehaviorType.Fixed_Column_Widths);
        table.getTableFormat().setHorizontalAlignment(RowAlignment.Center);

        String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid().concat(GENERATE_SUFFIX));
        document.saveToFile(fileName, FileFormat.Docx);
        document.dispose();
        // 转换为pdf文件
        File file = new File(fileName);
        String path = file.getAbsolutePath();
        String command = "libreoffice --headless --invisible --convert-to pdf:writer_pdf_Export " + path + " --outdir " + customConfig.getPdfTempDir();
        try {
            boolean result = executeLinuxCmd(command);
            if (result) {
                log.info("转换成功");
            }
        } catch (Exception e) {
            log.error("转换失败,原因：执行命令时出现异常。", e);
        }
        // 删除临时doc文件
        File tempDocfile = new File(fileName);
        FileUtils.deleteQuietly(tempDocfile);
        return new File(fileName.replace(GENERATE_SUFFIX, PDF));
    }

    /**
     * @param "QuotationPrice" 报价清单数据
     * @return File
     * @Author huiyongchen
     * @Description 生成报价清单
     * @Date 23:16 2024/5/1
     **/
    public File generateQuotationPrice(QuotationPrice quotationPrice, Boolean isSign) throws IOException, CustomBizException, IllegalAccessException {
        String templatePath = checkTemplate(quotationPrice);
        if (templatePath == null) {
            throw new CustomBizException(NOT_MATCH_TEMPLATE);
        }
        ClassPathResource resource = new ClassPathResource(templatePath);
        InputStream inputStream = resource.getInputStream();
        return getQuotationPriceFile(quotationPrice, isSign, inputStream);
    }

    /**
     * @param "templateFile QuotationPrice" 报价清单数据
     * @return File
     * @Author huiyongchen
     * @Description 生成报价清单
     * @Date 23:16 2024/5/1
     **/
    public File generateQuotationPrice(File templateFile, QuotationPrice quotationPrice, Boolean isSign) throws IOException, CustomBizException, IllegalAccessException {
        InputStream inputStream = Files.newInputStream(templateFile.toPath());
        return getQuotationPriceFile(quotationPrice, isSign, inputStream);
    }

    private @NotNull File getQuotationPriceFile(QuotationPrice quotationPrice, Boolean isSign, InputStream inputStream) throws IllegalAccessException, IOException {
        Document document = new Document();
        document.loadFromStream(inputStream, FileFormat.Docx);
        Table table = document.getSections().get(0).getTables().get(0);
        // 填充数据
        Map<String, Object> map = WordUtils.convertObjectToMap(quotationPrice);
        TextSelection[] selections = document.findAllPattern(TEMPLATE_PATTERN);
        for (TextSelection selection : selections) {
            TextRange range = selection.getAsOneRange();
            String text = selection.getSelectedText();
            text = text.replaceAll("\\[|\\]", "");
            if (map.containsKey(text)) {
                range.setText(map.get(text).toString());
            }
        }
        int rowNum = quotationPrice.getMaterialList().size();
        for (int i = 0; i < rowNum; i++) {
            table.addRow();
        }
        for (int i = 11, j = 1; i < rowNum + 11; i++, j++) {
            table.get(i, 0).addParagraph().appendText(String.valueOf(j))
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 1).addParagraph().appendText(quotationPrice.getMaterialList().get(j - 1).getMaterialName())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 2).addParagraph().appendText(quotationPrice.getMaterialList().get(j - 1).getUnit())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 3).addParagraph().appendText(quotationPrice.getMaterialList().get(j - 1).getNumber())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 4).addParagraph().appendText(quotationPrice.getMaterialList().get(j - 1).getUnitPrice())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 5).addParagraph().appendText(quotationPrice.getMaterialList().get(j - 1).getManHourCost())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
            table.get(i, 6).addParagraph().appendText(quotationPrice.getMaterialList().get(j - 1).getAmount())
                    .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
        }
        // 构建表尾
        table.addRow();
        table.addRow();
        table.addRow();
        table.addRow();
        table.addRow();
        table.addRow();
        // 基准行
        int row = rowNum + 11;
        table.get(row, 0).addParagraph().appendText("标准价格")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
        table.applyHorizontalMerge(row, 0, 1);
        table.get(row, 2).addParagraph().appendText(quotationPrice.getPrice())
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
        table.applyHorizontalMerge(row, 2, 6);

        table.get(row + 1, 0).addParagraph().appendText("总计")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
        table.applyHorizontalMerge(row + 1, 0, 1);
        table.get(row + 1, 2).addParagraph().appendText("人民币：")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Right);
        table.applyHorizontalMerge(row + 1, 2, 3);
        table.get(row + 1, 4).addParagraph().appendText("RMB".concat(quotationPrice.getTotalAmount()))
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Left);
        table.applyHorizontalMerge(row + 1, 4, 6);

        table.get(row + 2, 0).addParagraph().appendText("开票内容")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
        table.applyHorizontalMerge(row + 2, 0, 1);
        table.get(row + 2, 2).addParagraph().appendText("维修费用")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Center);
        table.applyHorizontalMerge(row + 2, 2, 6);

        table.get(row + 3, 0).addParagraph().appendText("备注：")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Left);
        table.applyHorizontalMerge(row + 3, 0, 6);

        table.get(row + 4, 0).addParagraph().appendText("报价单位：南方电网通用航空服务有限公司")
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Right);
        table.applyHorizontalMerge(row + 4, 0, 6);

        table.get(row + 5, 0).addParagraph().appendText("日期：".concat(quotationPrice.getDate()))
                .getOwnerParagraph().getFormat().setHorizontalAlignment(HorizontalAlignment.Right);
        table.applyHorizontalMerge(row + 5, 0, 6);
        // 盖章
        TextSelection[] textSelections = document.findAllString("有限公司", false, false);
        //是否需要签章，默认配置false, 参数和配置都true 才会签章
        if (customConfig.getIsSign() && isSign && ObjectUtils.isNotEmpty(textSelections)) {
            Paragraph paragraph = textSelections[textSelections.length - 1].getAsOneRange().getOwnerParagraph();
            ClassPathResource urlResource = new ClassPathResource(STAMP_IMG_URL);
            DocPicture docPicture = paragraph.appendPicture(urlResource.getInputStream());
            docPicture.setTextWrappingStyle(TextWrappingStyle.In_Front_Of_Text);
            docPicture.setHorizontalPosition(300f);
            docPicture.setVerticalPosition(-55f);
            docPicture.setWidth(120f);
            docPicture.setHeight(120f);
        }
        table.autoFit(AutoFitBehaviorType.Fixed_Column_Widths);
        table.getTableFormat().setHorizontalAlignment(RowAlignment.Center);
        String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid().concat(PDF));
        document.saveToFile(fileName, FileFormat.PDF);
        return new File(fileName);
    }

    /**
     * 将PDF文件转换为PNG格式的图片
     *
     * @param file 要转换的PDF文件
     * @return 转换后的PNG格式图片文件
     */
    public File pdfToPng(File file) throws IOException {
        PdfDocument pdf = new PdfDocument();
        String path = file.getAbsolutePath();
        pdf.loadFromFile(path);
        BufferedImage image = pdf.saveAsImage(0, PdfImageType.Bitmap, 500, 500);
        String pngFile = customConfig.getPdfTempDir().concat(CommonUtils.uuid().concat(PNG_SUFFIX));
        ImageIO.write(image, "PNG", new File(pngFile));
        return new File(pngFile);
    }

    private static boolean executeLinuxCmd(String cmd) throws Exception {
        try {
            DefaultExecutor exec = DefaultExecutor.builder().get();
            // 同步等待
            Semaphore semaphore = new Semaphore(1);
            semaphore.acquire();
            ExecuteResultHandler erh = new ExecuteResultHandler() {
                @Override
                public void onProcessComplete(int i) {
                    semaphore.release();
                    // 转换完成逻辑
                    log.info("executeLinuxCmd success");
                }

                @Override
                public void onProcessFailed(ExecuteException e) {
                    semaphore.release();
                    // 转换失败逻辑
                    log.error("executeLinuxCmd error", e);
                }
            };
            log.info("执行命令为:{}", cmd);
            exec.execute(CommandLine.parse(cmd), erh);
            // 等待执行完成
            semaphore.acquire();
            return true;
        } catch (Exception e) {
            log.error("executeLinuxCmd error", e);
        }
        return false;
    }

    /**
     * author huiyongchen
     * 将对象转换为Map的工具类
     *
     * @param obj 要转换的对象
     * @return 转换后的Map
     * @throws IllegalAccessException 如果无法访问对象的属性时抛出异常
     */
    public static Map<String, Object> convertObjectToMap(Object obj) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true); // 设置属性可访问，避免 private 或 protected 属性无法访问
            Object value = field.get(obj);
            if (value != null) {
                map.put(field.getName(), value);
            } else {
                map.put(field.getName(), " ");
            }
        }
        return map;
    }

    /**
     * author huiyongchen
     * 检查模板类型并返回对应的模板路径
     *
     * @param data 待检查的数据对象
     * @return 对应模板的路径，如果未匹配到则返回null
     */
    public static String checkTemplate(Object data) {
        if (data == null) {
            return null;
        }
        return templateMap.get(data.getClass());
    }

    public boolean templateFileValidation(File file, Object data) throws IllegalAccessException, FileNotFoundException {
        FileInputStream templateFileInput = new FileInputStream(file);
        Map<String, Object> dataMap = convertObjectToMap(data);
        Document document = new Document();
        document.loadFromStream(templateFileInput, FileFormat.Docx);
        TextSelection[] selections = document.findAllPattern(TEMPLATE_PATTERN);
        if (dataMap == null || selections == null) {
            return false;
        }
        if (data instanceof QuotationPrice || data instanceof LossItem) {
            return true;
        }
        List<String> unmatchedKeys = new ArrayList<>(dataMap.keySet());
        for (TextSelection selection : selections) {
            String selectionText = selection.getSelectedText();
            for (String key : unmatchedKeys) {
                Pattern pattern = Pattern.compile("\\[" + key + "\\]");
                Matcher matcher = pattern.matcher(selectionText);
                if (matcher.find()) {
                    unmatchedKeys.remove(key);
                    break;
                }
            }
        }
        return unmatchedKeys.isEmpty();
    }

}