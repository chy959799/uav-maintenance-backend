package com.thinglinks.uav.common.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author: fanhaiqiu
 * @date: 2021/7/21
 */
public class DateUtils {

    private static final String FMT_DATETIME = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter FORMATTER_DATETIME = DateTimeFormatter.ofPattern(FMT_DATETIME);

    private static final String FMT_DATE = "yyyyMMdd";
    private static final DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern(FMT_DATE);

    private static final String FMT_MONTH = "yyyyMM";
    private static final DateTimeFormatter FORMATTER_MONTH = DateTimeFormatter.ofPattern(FMT_MONTH);

    private static final String FMT_DATE_CHINESE = "yyyy年MM月dd日";
    private static final DateTimeFormatter FORMATTER_DATE_CHINESE = DateTimeFormatter.ofPattern(FMT_DATE_CHINESE);

    private static final String FMT_DATE_CHINESE_2 = "yyyy年MM月dd日HH时mm分";
    private static final DateTimeFormatter FORMATTER_DATE_CHINESE_2 = DateTimeFormatter.ofPattern(FMT_DATE_CHINESE_2);

    private static final String FMT_DATE_CHINESE_1 = "MM月dd日HH点";
    private static final DateTimeFormatter FORMATTER_DATE_CHINESE_1 = DateTimeFormatter.ofPattern(FMT_DATE_CHINESE_1);
    private static final String FMT_DATE_DOT = "yyyy.MM.dd";
    private static final DateTimeFormatter FORMATTER_DATE_DOT = DateTimeFormatter.ofPattern(FMT_DATE_DOT);

    private static final String FMT_DATE_LINE = "yyyy-MM-dd";
    private static final DateTimeFormatter FORMATTER_DATE_LINE = DateTimeFormatter.ofPattern(FMT_DATE_LINE);

    private static final String FMT_DATETIME_CODE = "yyyyMMddHHmmss";
    private static final DateTimeFormatter FORMATTER_DATETIME_CODE = DateTimeFormatter.ofPattern(FMT_DATETIME_CODE);

    /**
     * 格式化时间
     * yyyy-MM-dd HH:mm:ss
     *
     * @param time
     * @return
     */
    public static String formatDateTime(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATETIME.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }

    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatDate(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATE.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }


    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatMonth(Long time) {
        return FORMATTER_MONTH.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }

    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatHourChinese(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATE_CHINESE_1.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }

    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatDateChinese(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATE_CHINESE.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }

    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatDateChinese2(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATE_CHINESE_2.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }

    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatDateDot(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATE_DOT.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }


    /**
     * 格式化时间
     *
     * @param time
     * @return
     */
    public static String formatDateLine(Long time) {
        if (time == null) {
            return null;
        }
        return FORMATTER_DATE_LINE.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()));
    }


    /**
     * 格式化时间
     * yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static String codeByTime() {
        String time = FORMATTER_DATETIME_CODE.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault()));
        return time.concat(RandomStringUtils.randomNumeric(5));
    }

    public static long convertTimestamp(String timestampString) {
        Instant instant = Instant.parse(timestampString);
        return instant.toEpochMilli();
    }

    /***
     * 智能分析字符串格式，然后进行转化为日期对象。
     * @param source
     * @return
     * @throws ParseException
     */
    public static Date getDate(String source) throws ParseException {
        if (source == null || source.isEmpty()) {
            return null;
        }
        DateFormat sdf;
        if (source.matches("^[1-9][0-9]{3}-[0-1][0-9]-[0-3][0-9]$")) {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
        } else if (source.matches("^[1-9][0-9]{3}[0-1][0-9][0-3][0-9]$")) {
            sdf = new SimpleDateFormat("yyyyMMdd");
        } else if (source.matches("^[1-9][0-9]{3}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$")) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else if (source.matches("^[1-9][0-9]{3}-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]\\.[0-9]{3}$")) {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        } else if (source.matches("^[1-9][0-9]{3}/[0-1]?[0-9]/[0-3]?[0-9] [0-2]?[0-9]:[0-5][0-9]$")) {
            sdf = new SimpleDateFormat("yyyy/M/d HH:mm");
        } else {
            throw new ParseException("暂不支持该格式的字符串！" + source, 0);
        }
        return sdf.parse(source);
    }

    public static long parseDateStringToTimestamp(String dateString) {
        // 使用DateTimeFormatter解析日期字符串
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FMT_DATETIME);
        LocalDateTime dateTime = LocalDateTime.parse(dateString, formatter);
        // 将LocalDateTime转换为long类型的时间戳（毫秒数）
        return dateTime.atZone(java.time.ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
