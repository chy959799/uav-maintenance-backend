package com.thinglinks.uav.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;

import java.util.concurrent.Semaphore;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/29
 */
@Slf4j
public class FileUtils {

    /**
     *
     * @param inputFile
     * @return
     * @throws Exception
     */
    public static void convert2Pdf(String inputFile, String outDir) throws Exception {
        DefaultExecutor exec = new DefaultExecutor();
        // 同步等待
        Semaphore semaphore = new Semaphore(1);
        semaphore.acquire();
        ExecuteResultHandler erh = new ExecuteResultHandler() {
            @Override
            public void onProcessComplete(int i) {
                semaphore.release();
                //转换完成逻辑
                log.info("convert2Pdf success");
            }
            @Override
            public void onProcessFailed(ExecuteException e) {
                semaphore.release();
                //转换失败逻辑
                log.error("convert2Pdf error", e);
            }
        };
        String command = "libreoffice --headless --invisible --convert-to pdf:writer_pdf_Export " + inputFile + " --outdir " + outDir;
        log.info("执行office文件转换任务，命令为:{}", command);
        exec.execute(CommandLine.parse(command), erh);
        // 等待执行完成
        semaphore.acquire();
    }
}
