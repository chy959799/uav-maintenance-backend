package com.thinglinks.uav.common.utils;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/25
 */
public class PoiExcelUtils {


    /**
     * @param region
     * @param sheet
     */
    public static void setRegionBorder(CellRangeAddress region, Sheet sheet) {
        RegionUtil.setBorderRight(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderBottom(BorderStyle.THIN, region, sheet);
        RegionUtil.setBorderTop(BorderStyle.THIN, region, sheet);
    }

    /**
     * @param cellStyle
     */
    public static void setCellBorder(CellStyle cellStyle) {
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
    }


    public static void main(String[] args) throws Exception {
        Workbook workbook = new XSSFWorkbook();
        //文字居中
        CellStyle frontCenter = workbook.createCellStyle();
        frontCenter.setAlignment(HorizontalAlignment.CENTER);
        frontCenter.setVerticalAlignment(VerticalAlignment.CENTER);
        PoiExcelUtils.setCellBorder(frontCenter);
        //文字居中
        CellStyle frontCenterAuto = workbook.createCellStyle();
        frontCenterAuto.setAlignment(HorizontalAlignment.CENTER);
        frontCenterAuto.setVerticalAlignment(VerticalAlignment.CENTER);
        frontCenterAuto.setWrapText(true);
        PoiExcelUtils.setCellBorder(frontCenterAuto);
        //文字居左
        CellStyle frontLeft = workbook.createCellStyle();
        frontLeft.setAlignment(HorizontalAlignment.LEFT);
        frontLeft.setVerticalAlignment(VerticalAlignment.CENTER);
        PoiExcelUtils.setCellBorder(frontLeft);
        //文字居中+背景色
        CellStyle frontCenterBackGround = workbook.createCellStyle();
        frontCenterBackGround.setAlignment(HorizontalAlignment.CENTER);
        frontCenterBackGround.setVerticalAlignment(VerticalAlignment.CENTER);
        frontCenterBackGround.setFillForegroundColor(IndexedColors.RED.index);
        frontCenterBackGround.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        PoiExcelUtils.setCellBorder(frontCenterBackGround);
        //文字居中+字体
        CellStyle bigFrontCenter = workbook.createCellStyle();
        bigFrontCenter.setAlignment(HorizontalAlignment.CENTER);
        bigFrontCenter.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 16);
        bigFrontCenter.setFont(font);
        PoiExcelUtils.setCellBorder(bigFrontCenter);
        //背景
        CellStyle background = workbook.createCellStyle();
        background.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        background.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        PoiExcelUtils.setCellBorder(background);

        Sheet costSheet = workbook.createSheet("成本报价单");
        CellRangeAddress r1 = new CellRangeAddress(0, 1, 0, 7);
        costSheet.addMergedRegion(r1);
        Row row0 = costSheet.createRow(0);
        Cell c10 = row0.createCell(0);
        c10.setCellValue("无人机维修成本报价单");
        c10.setCellStyle(bigFrontCenter);
        PoiExcelUtils.setRegionBorder(r1, costSheet);

        Row row2 = costSheet.createRow(2);
        Cell c20 = row2.createCell(0);
        c20.setCellStyle(background);
        CellRangeAddress r2 = new CellRangeAddress(2, 2, 0, 7);
        costSheet.addMergedRegion(r2);
        PoiExcelUtils.setRegionBorder(r2, costSheet);

        Row row3 = costSheet.createRow(3);
        //Row row33 = costSheet.createRow(3);
        Cell c30 = row3.createCell(0);
        c30.setCellValue("基本信息");
        c30.setCellStyle(frontCenter);
        CellRangeAddress r3 = new CellRangeAddress(3, 7, 0, 0);
        costSheet.addMergedRegion(r3);
        PoiExcelUtils.setRegionBorder(r3, costSheet);
        Cell c31 = row3.createCell(1);
        c31.setCellValue("客户名称");
        c31.setCellStyle(frontCenterBackGround);

        Row row4 = costSheet.createRow(4);
        Cell c41 = row4.createCell(1);
        c41.setCellValue("飞机型号");
        c41.setCellStyle(frontCenterBackGround);

        Row row5 = costSheet.createRow(5);
        Cell c51 = row5.createCell(1);
        c51.setCellValue("飞机SN码");
        c51.setCellStyle(frontCenterBackGround);

        Row row6 = costSheet.createRow(6);
        Cell c61 = row6.createCell(1);
        c61.setCellValue("报价日期");
        c61.setCellStyle(frontCenterBackGround);

        Row row7 = costSheet.createRow(7);
        Cell c70 = row7.createCell(0);
        c70.setCellStyle(frontCenter);
        Cell c71 = row7.createCell(1);
        c71.setCellValue("联系人");
        c71.setCellStyle(frontCenterBackGround);

        //File file = new File("D:\\temp\\template.xlsx");
         File file = new File("/Users/fanhaiqiu/Documents/temp/template_1.xlsx");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        workbook.write(fileOutputStream);
        workbook.close();
        fileOutputStream.close();
    }
}
