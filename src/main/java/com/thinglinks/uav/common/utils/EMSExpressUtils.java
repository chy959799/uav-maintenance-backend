package com.thinglinks.uav.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.common.constants.ExpressionConstants;
import lombok.Data;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author iwiFool
 * @date 2024/4/18
 */
@Component
public class EMSExpressUtils {

	@Value("${expresses.ems.senderNo}")
	private String senderNo;
	@Value("${expresses.ems.dev.authorization}")
	private String devAuthorization;
	@Value("${expresses.ems.dev.key}")
	private String devKey;
	@Value("${expresses.ems.prod.authorization}")
	private String prodAuthorization;
	@Value("${expresses.ems.prod.key}")
	private String prodKey;
	@Value("${expresses.ems.test}")
	private Boolean test;

	/**
	 * 判断是否成功
	 * @param responseString 响应的字符串
	 */
	public Boolean isSuccess(String responseString) {
		return JSONObject.parseObject(responseString).getString("retCode").equals(ExpressionConstants.SUCCESS_CODE);
	}

	/**
	 * 获取错误信息
	 * @param responseString 响应的字符串
	 */
	public String getErrorMessage(String responseString) {
		return JSONObject.parseObject(responseString).getString("retMsg");
	}

	/**
	 * 获取运单号
	 * @param responseString 响应的字符串
	 */
	public String getWaybillNo(String responseString) {
		return JSONObject.parseObject(responseString).getJSONObject("retBody").getString("waybillNo");
	}

	/**
	 * 获取轨迹信息列表
	 * @param responseString 响应的字符串
	 */
	public String getTrackInfoListString(String responseString) {
		return JSONObject.parseObject(responseString).getJSONObject("retBody").getString("responseItems");
	}

	/**
	 * 获取面单PDF流
	 * @param responseString 响应的字符串
	 */
	public byte[] getWaybillPdf(String responseString) {
		String retBody = JSONObject.parseObject(responseString).getString("retBody");
		return Base64.decodeBase64(retBody);
	}

	/**
	 * 上门取件
	 * @param orderId 物流订单号
	 * @param sender 寄件人信息
	 * @param receiver 收件人信息
	 * @param delivery 上门取件人信息
	 */
	public String pickupOrder(String orderId, AddressDTO sender, AddressDTO receiver, DeliveryDTO delivery) {
		Map<String, String> parameters = getCommonParameter(ExpressionConstants.PICKUP_ORDER);
		String logitcsInterface = getPickupOrderLogitcsInterface(orderId, sender, receiver, delivery);
		logitcsInterface = getLogitcsInterface(logitcsInterface);
		parameters.put("logitcsInterface", logitcsInterface);
		return HttpUtils.httpPostForm(getUrl(), parameters);
	}

	/**
	 * 上门取件取消
	 * @param orderId 物流订单号
	 */
	public String pickupOrderCancel(String orderId) {
		Map<String, String> parameters = getCommonParameter(ExpressionConstants.PICKUP_ORDER_CANCEL);
		String logitcsInterface = getPickupOrderCancelLogitcsInterface(orderId);
		logitcsInterface = getLogitcsInterface(logitcsInterface);
		parameters.put("logitcsInterface", logitcsInterface);
		return HttpUtils.httpPostForm(getUrl(), parameters);
	}

	/**
	 * 运单轨迹信息获取
	 * @param waybillNo 物流运单号
	 */
	public String trackInfo(String waybillNo) {
		Map<String, String> parameters = getCommonParameter(ExpressionConstants.TRACK_INFO);
		String logitcsInterface = getTrackInfoLogitcsInterface(waybillNo);
		logitcsInterface = getLogitcsInterface(logitcsInterface);
		parameters.put("logitcsInterface", logitcsInterface);
		return HttpUtils.httpPostForm(getUrl(), parameters);
	}

	/**
	 * 面单查询
	 */
	public String queryRoutes(String waybillNo) {
		Map<String, String> parameters = getCommonParameter(ExpressionConstants.QUERY_ROUTES);
		String logitcsInterface = getQueryRoutesLogitcsInterface(waybillNo);
		logitcsInterface = getLogitcsInterface(logitcsInterface);
		parameters.put("logitcsInterface", logitcsInterface);
		return HttpUtils.httpPostForm(getUrl(), parameters);
	}

	public void getFile(byte[] bfile, String filePath,String fileName) {
		BufferedOutputStream bos = null;
		FileOutputStream fos;
		File file;
		try {
			File dir = new File(filePath);
			//判断文件目录是否存在
			if (!dir.exists() && dir.isDirectory()) {
				dir.mkdirs();
			}
			file = new File(filePath + "\\" + fileName);
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			bos.write(bfile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	private String getQueryRoutesLogitcsInterface(String waybillNo) {
		JSONObject logitcsInterface = new JSONObject();
		// 129：总部模板76129, 149：总部模板76149, 179：总部模板, 100179
		logitcsInterface.put("type", "129");
		logitcsInterface.put("waybillNo", waybillNo);
		return logitcsInterface.toJSONString();
	}

	private String getTrackInfoLogitcsInterface(String waybillNo) {
		JSONObject logitcsInterface = new JSONObject();
		// waybillNo 运单号
		logitcsInterface.put("waybillNo", waybillNo);
		return logitcsInterface.toJSONString();
	}

	private String getPickupOrderCancelLogitcsInterface(String orderId) {
		JSONObject logitcsInterface = new JSONObject();
		// 撤单原因码 13：上门揽收不及时放弃，19：客户自交寄，20：客户转交其他公司，16：已联系客户并确认重复下单，8：超禁限，9：超规格，28：客户取消订单，12：因资费原因放弃，22：客户更改取件地址，23：EMS到达时限慢，24：测试单，25：客户要求到付，26：其他，30:计划有变，31:误操作，32:邮件时限，33:资费原因，34:上门不及时，35:服务态度不好，99：揽收员不存在
		logitcsInterface.put("cancelCode", "28");
		// logisticsOrderNo 物流订单号
		logitcsInterface.put("logisticsOrderNo", orderId);
		return logitcsInterface.toJSONString();
	}

	private String getPickupOrderLogitcsInterface(String orderId, AddressDTO sender, AddressDTO receiver, DeliveryDTO delivery) {
		return getPickupOrderLogitcsInterface(orderId, "3", "11", "1", sender, receiver, delivery);
	}

	private String getPickupOrderLogitcsInterface(String logisticsOrderNo, String contentsAttribute, String bizProductNo, String pushMethod, AddressDTO sender, AddressDTO receiver, DeliveryDTO delivery) {
		JSONObject logitcsInterface = new JSONObject();
		// 物流订单号（客户内部订单号，需要每次传入是唯一的）
		logitcsInterface.put("logisticsOrderNo", logisticsOrderNo);
		// 内件性质 1-文件 2-信函 3-物品
		logitcsInterface.put("contentsAttribute", contentsAttribute);
		// 业务产品分类 1-特快专递 2-快递包裹 3-特快到付 9-国内即日 10-电商标快 11-国内标快
		logitcsInterface.put("bizProductNo", bizProductNo);
		// 推送方式
		// 1-实时派揽-根据预约时间上门
		// 2、定时派揽-固定频次上门按照机构配置时间分上下午推送，未维护默认上午9:00和下午16:00两个频次)
		logitcsInterface.put("pushMethod", pushMethod);
		// 寄件人信息
		logitcsInterface.put("sender", sender);
		// 收件人信息
		logitcsInterface.put("receiver", receiver);
		// 上门取件人信息
		logitcsInterface.put("delivery", delivery);
		return logitcsInterface.toJSONString();
	}

	/**
	 * 获取公共参数
	 * @param apiCode 接口代码
	 */
	private Map<String, String> getCommonParameter(String apiCode) {
		Map<String, String> parameters = new HashMap<>();
		// 接口代码
		parameters.put("apiCode", apiCode);
		// 客户代码
		parameters.put("senderNo", senderNo);
		// 授权码（区分测试和生产）
		parameters.put("authorization", getAuthorization());
		// 请 求 时 间 yyyy-MM-dd HH:mm:ss
		parameters.put("timeStamp", DateUtils.formatDateTime(System.currentTimeMillis()));
		return parameters;
	}

	private String getLogitcsInterface(String logitcsInterface) {
		String content = logitcsInterface + getKey();
		logitcsInterface = CryptoThirdSM4Tools.sm4Encrypt(content, getKey());
		return logitcsInterface;
	}

	private String getKey() {
		return test ? devKey : prodKey;
	}

	private String getUrl() {
		String prodURL = "https://api.ems.com.cn/amp-prod-api/f/amp/api/open";
		String devURL = "https://api.ems.com.cn/amp-prod-api/f/amp/api/test";
		return test ? devURL : prodURL;
	}

	private String getAuthorization() {
		return test ? devAuthorization : prodAuthorization;
	}

	public String getSenderNo() {
		return senderNo;
	}

	@Data
	public static class AddressDTO {
		/**
		 * 用户姓名
		 */
		private String name;
		/**
		 * 用户移动电话
		 */
		private String mobile;
		/**
		 * 用户所在省，使用国标全称
		 */
		private String prov;
		/**
		 * 用户所在市，使用国标全称
		 */
		private String city;
		/**
		 * 用户所在县（区），使用国标全称
		 */
		private String county;
		/**
		 * 用户详细地址
		 */
		private String address;
	}

	@Data
	public static class DeliveryDTO {
		/**
		 * 上门取件地址
		 */
		private String address;
		/**
		 * 上门取件地址所在省
		 */
		private String prov;
		/**
		 * 上门取件地址所在市
		 */
		private String city;
		/**
		 * 上门取件地址所在区县
		 */
		private String county;
	}
}
