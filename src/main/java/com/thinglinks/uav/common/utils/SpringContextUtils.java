package com.thinglinks.uav.common.utils;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class SpringContextUtils implements ApplicationContextAware {

    /**
     * -- GETTER --
     *  获得当前的ApplicationContext
     *
     * @return org.springframework.context.ApplicationContext
     */
    @Getter
    private static ApplicationContext applicationContext = null;

    private static final Logger logger = LoggerFactory.getLogger(SpringContextUtils.class);

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(Objects.isNull(SpringContextUtils.applicationContext)){
            logger.info("=====>ApplicationUtils初始化...");
            SpringContextUtils.applicationContext = applicationContext;
        }
        logger.info("=====>ApplicationUtils初始化成功!");
    }

    /**
     * 根据名称拿到Bean
     *
     * @author LiDong
     * @date 2020/11/20
     * @param '[name]'
     * @return java.lang.Object
     */
    public static Object getBean(String name){
        return getApplicationContext().getBean(name);
    }

    /**
     * 从ApplicationContext中获得Bean并且转型
     *
     * @author LiDong
     * @date 2020/11/20
     * @param '[tClass]'
     * @return T
     */
    public static <T> T getBean(Class<T> tClass){
        return getApplicationContext().getBean(tClass);
    }
}
