package com.thinglinks.uav.common.utils;

import cn.hutool.core.util.URLUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.event.SyncReadListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.style.column.LongestMatchColumnWidthStyleStrategy;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class EasyExcelUtils {


    /**
     * 导出XLS
     *
     * @param fileName  导出的文件名
     * @param sheetName 工作表名
     * @param list<T>   导出数据
     */
    public static <T> void exportXlsx(HttpServletResponse response, String fileName, String sheetName, List<T> list, Class clazz) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                + URLEncoder.encode(fileName, "UTF-8") + ".xlsx");
        OutputStream outputStream = response.getOutputStream();
        ExcelWriterBuilder write = EasyExcel.write(outputStream, clazz).excelType(ExcelTypeEnum.XLSX);
        ExcelWriterSheetBuilder sheet = write.sheet(sheetName);
        sheet.doWrite(list);
    }

    /**
     * Excel导出
     *
     * @param response  response
     * @param fileName  文件名
     * @param sheetName sheetName
     * @param list      数据List
     * @param pojoClass 对象Class
     */
    public static void exportExcel(HttpServletResponse response, String fileName, String sheetName, List<?> list, Class<?> pojoClass) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("UTF-8");
        fileName = URLUtil.encode(fileName, StandardCharsets.UTF_8);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), pojoClass)
                .excelType(ExcelTypeEnum.XLSX)
                .registerWriteHandler(new LongestMatchColumnWidthStyleStrategy())
//                .registerConverter(new LongStringConverter())
                .sheet(sheetName)
                .doWrite(list);
    }

    /**
     * 动态表头导出
     *
     * @param fileName  导出的文件名
     * @param sheetName 工作表名
     */
    public static <T> void exportXlsx(HttpServletResponse response, String fileName, String sheetName, List<List<String>> headList, List<List<Object>> dataList) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                + URLEncoder.encode(fileName, "UTF-8") + ".xlsx");
        OutputStream outputStream = response.getOutputStream();
        ExcelWriterBuilder write = EasyExcel.write(outputStream).head(headList).excelType(ExcelTypeEnum.XLSX);
        ExcelWriterSheetBuilder sheet = write.sheet(sheetName);
        sheet.doWrite(dataList);
    }

    /**
     * 模板写入
     *
     * @param fileName  导出的文件名
     * @param sheetName 工作表名
     */
    public static <T> void exportXlsx(HttpServletResponse response, String fileName, String sheetName, String filePath, List<T> fillData ) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                + URLEncoder.encode(fileName, "UTF-8") + ".xlsx");
        OutputStream outputStream = response.getOutputStream();
        ExcelWriterBuilder write = EasyExcel.write(outputStream).withTemplate(filePath).excelType(ExcelTypeEnum.XLSX);
        ExcelWriterSheetBuilder sheet = write.sheet(sheetName);
        sheet.doFill(fillData);
    }

    /**
     * 模板写入
     *
     * @param fileName  导出的文件名
     * @param sheetName 工作表名
     */
    public static <T> void exportXlsx(HttpServletResponse response, String fileName, String sheetName, File file, List<T> fillData ) throws Exception {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                + URLEncoder.encode(fileName, "UTF-8") + ".xlsx");
        OutputStream outputStream = response.getOutputStream();
        ExcelWriterBuilder write = EasyExcel.write(outputStream).withTemplate(file).excelType(ExcelTypeEnum.XLSX);
        ExcelWriterSheetBuilder sheet = write.sheet(sheetName);
        sheet.doFill(fillData);
    }

    /**
     * 读 excel
     */
    public static <T> List readXlsx(InputStream inputStream, Class<T> clazz) throws Exception {
        SyncReadListener syncReadListener = new SyncReadListener();
        EasyExcel.read(inputStream, clazz, syncReadListener).doReadAllSync();
        return syncReadListener.getList();
    }

}
