package com.thinglinks.uav.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2023/6/14
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "county")
@Data
public class County extends BaseEntity{
    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "city_code")
    private String cityCode;
}
