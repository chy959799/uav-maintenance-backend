package com.thinglinks.uav.common.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2023/6/14
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "province")
@Data
public class Province extends BaseEntity{

    @Column(name = "province_code")
    private String provinceCode;

    @Column(name = "province_name")
    private String provinceName;
}
