package com.thinglinks.uav.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author fanhaiqiu
 * @date 2019/6/28
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
public abstract class BaseEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "数据库主键")
    private Integer id;

    @Column(name = "created_at")
    @CreatedDate
    @JsonIgnore
    private Long ct;

    @Column(name = "updated_at")
    @LastModifiedDate
    @JsonIgnore
    private Long ut;

}
