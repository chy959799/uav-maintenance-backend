package com.thinglinks.uav.common.delay;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.HttpUtils;
import com.thinglinks.uav.order.service.ExpressesService;
import com.thinglinks.uav.system.config.CustomConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Executors;

@Component
@Slf4j
public class DelayQueueManager implements CommandLineRunner {

    private final DelayQueue<DelayTask> delayQueue = new DelayQueue<>();

    @Resource
    private CustomConfig customConfig;

    @Resource
    private ExpressesService expressesService;

    /**
     * 加入到延时队列中
     *
     * @param task
     */
    public void put(DelayTask task) {
        log.info("join delay task：{}", task);
        delayQueue.put(task);
    }

    /**
     * 取消延时任务
     *
     * @param task
     * @return
     */
    public boolean remove(DelayTask task) {
        log.info("remove delay task：{}", task);
        return delayQueue.remove(task);
    }

    /**
     * 取消延时任务
     *
     * @param taskId
     * @return
     */
    public boolean remove(String taskId) {
        return remove(new DelayTask(new TaskBase(taskId), 0));
    }

    @Override
    public void run(String... args) throws Exception {
        Executors.newSingleThreadExecutor().execute(new Thread(this::executeThread));
    }

    /**
     * 延时任务执行线程
     */
    private void executeThread() {
        while (true) {
            try {
                DelayTask task = delayQueue.take();
                processTask(task);
            } catch (Exception e) {
                log.error("executeThread error", e);
            }
        }
    }

    /**
     * 内部执行延时任务
     *
     * @param task
     */
    private void processTask(DelayTask task) throws Exception {
        log.info("do delay task：{}", task);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("partnerID", customConfig.getSfPartnerID());
        parameters.put("requestID", CommonUtils.uuid());
        parameters.put("timestamp", String.valueOf(System.currentTimeMillis()));
        parameters.put("accessToken", expressesService.getToken());
        parameters.put("serviceCode", "COM_RECE_CLOUD_PRINT_WAYBILLS");
        JSONObject msgData = new JSONObject();
        msgData.put("templateCode", customConfig.getTemplateCode());
        msgData.put("version", "2.0");
        msgData.put("fileType", "pdf");
        JSONArray documents = new JSONArray();
        JSONObject document = new JSONObject();
        document.put("masterWaybillNo", task.getData().getIdentifier());
        documents.add(document);
        msgData.put("documents", documents);
        parameters.put("msgData", msgData.toJSONString());
        String ret = HttpUtils.httpPostForm(customConfig.getSfUrl(), parameters);
        log.info("processTask ret:{}", ret);
        JSONObject jsonObject = JSONObject.parseObject(ret);
        String apiResultCode = jsonObject.getString("apiResultCode");
        if (CommonConstants.SUCCESS_CODE.equals(apiResultCode)) {
            return;
        }
        //失败了再执行
        task.setExpire(System.currentTimeMillis() + 200);
        this.put(task);
    }
}
