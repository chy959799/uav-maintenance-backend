package com.thinglinks.uav.common.delay;

import com.alibaba.fastjson.JSON;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TaskBase {

    private String identifier;

    public TaskBase(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
