package com.thinglinks.uav.common.enums;

import lombok.Getter;

/**
 * @author iwiFool
 * @date 2024/7/12
 */
@Getter
public enum RoleEnum {

    User("Users", "用户", "单位用户");

    private final String code;
    private final String name;
    private final String remark;

    RoleEnum(String code, String name, String remark) {
        this.code = code;
        this.name = name;
        this.remark = remark;
    }

}
