package com.thinglinks.uav.common.enums;

import java.util.HashMap;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
public enum UserTypeEnum {

    UNKNOWN(0, "unknown"),
    //管理员
    ADMIN(1, "admin"),
    //用户
    USER(2, "user");

    private final static HashMap<Integer, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (UserTypeEnum type : UserTypeEnum.values()) {
            CODE_TO_VALUE.put(type.code, type.value);
        }
    }

    public static String fromCode(int code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.value;
        }
        return value;
    }

    private final int code;

    private final String value;


    UserTypeEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }


}
