package com.thinglinks.uav.common.enums;

import lombok.Getter;

@Getter
public enum TemplateCode {

    T_0001("1001"),
    T_0002("1002");

    private final String code;

    TemplateCode(String code) {
        this.code = code;
    }
}
