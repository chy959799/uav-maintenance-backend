package com.thinglinks.uav.common.enums;

import lombok.Getter;

/**
 * @author iwiFool
 * @date 2024/8/25
 */
@Getter
public enum TemplateEnum {

    intention_to_pay("001", "赔付意向书", "template/intention_to_pay.docx"),
    power_risk_claim("002", "电力业务事故通知与索赔申请单", "template/power_risk_claim.docx"),
    claim_statement("003", "损失清单", "template/claim_statement.docx"),
    failure_report("004", "无人机故障报告", "template/failure_report.docx"),
    accident_photograph("005", "事故照片", "template/accident_photograph.docx"),
    notification("006", "收费通知单", "template/notification.docx"),
    case_report("007", "案件报告", "template/case_report.docx"),
    report("008", "报案文件", "template/report.docx");

    private final String type;
    private final String name;
    private final String filePath;

    TemplateEnum(String type, String name, String filePath) {
        this.type = type;
        this.name = name;
        this.filePath = filePath;
    }
}
