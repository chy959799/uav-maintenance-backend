package com.thinglinks.uav.common.enums;

import lombok.Getter;

/**
 * @author iwiFool
 * @date 2024/7/30
 */
@Getter
public enum InsuranceEnum {

    DingHe("8", "鼎和保险");

    private final String code;
    private final String name;

    InsuranceEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }
    public static String getNameByCode(String code) {
        for (InsuranceEnum insuranceEnum : InsuranceEnum.values()){
            if (insuranceEnum.getCode().equals(code)){
                return insuranceEnum.getName();
            }
        }
        return null;
    }
}