package com.thinglinks.uav.common.redis;

import com.alibaba.fastjson.JSON;
import com.thinglinks.uav.common.constants.RedisConstants;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.Role;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Component
public class CustomRedisTemplate {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 获取库存内部编码计数
     */
    public Long getStocksCountByDay() {
        String key = "KC".concat(DateUtils.formatDate(System.currentTimeMillis()));
        return stringRedisTemplate.opsForValue().increment(key);
    }

    /**
     * 获取库存内部编码计数
     */
    public Long getOrderCountByDay() {
        String key = "GD".concat(DateUtils.formatDate(System.currentTimeMillis()));
        return stringRedisTemplate.opsForValue().increment(key);
    }

    public Long getOrderCountByMonth() {
        String key = "GD".concat(DateUtils.formatMonth(System.currentTimeMillis()));
        return stringRedisTemplate.opsForValue().increment(key);
    }

    public Long getOrderConsultCountByDay() {
        String key = "GDZX".concat(DateUtils.formatDate(System.currentTimeMillis()));
        return stringRedisTemplate.opsForValue().increment(key);
    }

    /**
     * 存放手机验证码
     */
    public void setMobileCaptcha(String mobile, String captcha) throws CustomBizException {
        String key = RedisConstants.REGISTER_CODE_KEY + mobile;
        String storedCaptcha = stringRedisTemplate.opsForValue().get(key);
        Long timeLeft = stringRedisTemplate.getExpire(key);
        if (StringUtils.hasText(storedCaptcha) && timeLeft != null && (timeLeft > 0 && timeLeft <= RedisConstants.CAPTCHA_COUNTDOWN_TTL)) {
                // 冷却时间内，不允许重新获取验证码
                throw new CustomBizException("一分钟内不可重复获取验证码");
        }

        // 存储验证码并设置过期时间
        stringRedisTemplate.opsForValue().set(key, captcha, RedisConstants.CAPTCHA_EXPIRY_TIME_TTL, TimeUnit.SECONDS);
    }

    /**
     * 获取手机验证码
     */
    public String getMobileCaptcha(String mobile) {
        String key = RedisConstants.REGISTER_CODE_KEY + mobile;
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除手机验证码
     */
    public void deleteMobileCaptcha(String mobile) {
        String key = RedisConstants.REGISTER_CODE_KEY + mobile;
        stringRedisTemplate.delete(key);
    }

    /**
     * 存放图形验证码
     */
    public void setCaptcha(String captcha, String clientIp) {
        String key = RedisConstants.LOGIN_CODE_KEY + clientIp;
        stringRedisTemplate.opsForValue().set(key, captcha, RedisConstants.LOGIN_CODE_TTL, TimeUnit.SECONDS);
    }

    /**
     * 获取图形验证码
     */
    public String getCaptcha(String clientIp) {
        String key = RedisConstants.LOGIN_CODE_KEY + clientIp;
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除图形验证码
     */
    public void deleteCaptcha(String clientIp) {
        String key = RedisConstants.LOGIN_CODE_KEY + clientIp;
        stringRedisTemplate.delete(key);
    }

    /**
     * 存放用户token
     */
    public void setUserToken(String token, String uid) {
        String key = RedisConstants.LOGIN_USER_KEY + uid;
        stringRedisTemplate.opsForValue().set(key, token, RedisConstants.LOGIN_USER_TTL, TimeUnit.SECONDS);
    }

    /**
     * 获取用户token
     */
    public String getUserToken(String uid) {
        String key = RedisConstants.LOGIN_USER_KEY + uid;
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除用户token
     */
    public void delUserToken(String uid) {
        String key = RedisConstants.LOGIN_USER_KEY + uid;
        stringRedisTemplate.delete(key);
    }

    /**
     * 刷新用户token
     */
    public void refreshUserToken(String uid) {
        String key = RedisConstants.LOGIN_USER_KEY + uid;
        stringRedisTemplate.expire(key, RedisConstants.LOGIN_USER_TTL, TimeUnit.SECONDS);
    }

    /**
     * 存放维修点用户token
     * @param token
     * @param cpid
     * @param uid
     */
    public void setCompanyUserToken(String token, String cpid, String uid) {
        String key = RedisConstants.LOGIN_COMPANY_USER_KEY + cpid + ":" + uid;
        stringRedisTemplate.opsForValue().set(key, token, RedisConstants.LOGIN_USER_TTL, TimeUnit.SECONDS);
    }

    /**
     * 刷新维修点用户token
     */
    public void refreshCompanyUserToken(String cpid, String uid) {
        String key = RedisConstants.LOGIN_COMPANY_USER_KEY + cpid + ":" + uid;
        stringRedisTemplate.expire(key, RedisConstants.LOGIN_USER_TTL, TimeUnit.SECONDS);
    }

    /**
     * 判断维修点用户是否存在
     */
    public Boolean isCompanyUserTokenExist(String cpid, String uid) {
        String key = RedisConstants.LOGIN_COMPANY_USER_KEY + cpid + ":" + uid;
        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 获取维修点用户总数
     */
    public int getOnlineUserCount(String cpid) {
        String pattern = RedisConstants.LOGIN_COMPANY_USER_KEY + cpid + ":*";
        Set<String> keys = stringRedisTemplate.keys(pattern);
        return Objects.isNull(keys) ? 0 : keys.size();
    }

    /**
     * 更新角色信息
     * @param r 角色
     */
    public void updateRole(Role r) {
        stringRedisTemplate.opsForValue().set(RedisConstants.ROLE_KEY + r.getId(), JSON.toJSONString(r));
    }

    public Role getRole(Integer id) {
        return JSON.parseObject(stringRedisTemplate.opsForValue().get(RedisConstants.ROLE_KEY + id), Role.class);
    }

    /**
     * 更新用户角色信息
     * @param uid 用户uid
     * @param roles 角色id集合
     */
    public void updateUserRoles(String uid, List<Integer> roles) {
        stringRedisTemplate.opsForValue().set(RedisConstants.USER_ROLE_KEY + uid, JSON.toJSONString(roles));
    }

    /**
     * 获取用户角色信息
     */
    public List<Integer> getUserRoles(String uid) {
        return JSON.parseArray(stringRedisTemplate.opsForValue().get(RedisConstants.USER_ROLE_KEY + uid), Integer.class);
    }

    /**
     * 删除用户角色信息
     * @param uid 用户uid
     */
    public void deleteUserRole(String uid) {
        stringRedisTemplate.delete(RedisConstants.USER_ROLE_KEY + uid);
    }
}
