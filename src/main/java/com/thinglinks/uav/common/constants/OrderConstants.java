package com.thinglinks.uav.common.constants;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/29
 */
public class OrderConstants {

    public static final String ORIGIN_WEB_SYSTEM = "1";
    public static final String ORIGIN_WEB_USER = "2";

    public static final String ORDER_BUCKET = "b-order";

    public static final int QR_COED_WIDTH = 350;

    public static final int QR_COED_HEIGHT = 400;


    public static final int QR_FONT_HEIGHT = 50;

    //工单类型
    //维修
    public static final String ORDER_MAINTAIN = "1";
    //保养
    public static final String ORDER_UPKEEP = "2";

    //工单维修类型
    //保内维修
    public static final String ORDER_IN_INSURANCE = "1";
    //框架合同维修
    public static final String ORDER_OUT_INSURANCE = "2";
    //快修快换
    public static final String ORDER_SERVICE_QUICKLY = "3";
    //作废
    public static final String ORDER_SERVICE_INVALID = "4";

    //设备状态
    //正常
    public static final String DEVICE_STATUS_NORMAL = "1";
    //维修
    public static final String DEVICE_STATUS_MAINTAIN = "2";
    //保养
    public static final String DEVICE_STATUS_UPKEEP = "3";

    //保险类型
    //DJI Care
    public static final String INSURANCE_DJI_CARE = "1";
    //DJI Care续享
    public static final String INSURANCE_DJI_CARE_X = "2";
    //行业无忧基础版
    public static final String INSURANCE_HYWY_BASIC = "3";
    //行业无忧基础续享版
    public static final String INSURANCE_HYWY_BASIC_X = "4";
    //行业无忧旗舰版
    public static final String INSURANCE_HYWY_PRO = "5";
    //行业无忧旗舰续享版
    public static final String INSURANCE_HYWY_PRO_X = "6";
    //英大财险
    public static final String INSURANCE_YDC = "7";
    //鼎和财险
    public static final String INSURANCE_DH = "8";

    //工单操作类型
    //工单取消
    public static final String ORDER_LOG_METHOD_CANCEL = "0000";
    //工单取消审核
    public static final String ORDER_LOG_METHOD_REVIEW_CANCEL = "0001";
    //工单快递取消
    public static final String ORDER_LOG_METHOD_EXPRESSES_CANCEL = "0002";
    //系统自动失效
    public static final String ORDER_LOG_METHOD_AUTO_INVALIDATE = "0003";
    //客户撤回取消
    public static final String ORDER_LOG_METHOD_REVOCATION = "0004";
    //撤回取消审核
    public static final String ORDER_LOG_METHOD_REVIEW_REVOCATION = "0005";
    //工单报废审核
    public static final String ORDER_LOG_METHOD_REVIEW_SCRAP = "0006";
    //工单报废处理
    public static final String ORDER_LOG_METHOD_HANDLE_SCRAP = "0007";
    //创建工单
    public static final String ORDER_LOG_METHOD_CREATE = "1000";
    //收件确认
    public static final String ORDER_LOG_METHOD_CONFIRM_EXPRESSES = "1001";
    //客户设备确认
    public static final String ORDER_LOG_METHOD_CONFIRM_DEVICE = "2000";
    //设备保险确认
    public static final String ORDER_LOG_METHOD_REVIEW_INSURANCE = "2001";
    //客户初版资料提交
    public static final String ORDER_LOG_METHOD_FIRST_INSURANCE_SUBMIT = "2002";
    //客户初版资料审核
    public static final String ORDER_LOG_METHOD_FIRST_INSURANCE_REVIEW = "2003";
    //客户终版资料提交
    public static final String ORDER_LOG_METHOD_LAST_INSURANCE_SUBMIT = "2004";
    // 客户终版资料审核
    public static final String ORDER_LOG_METHOD_LAST_INSURANCE_REVIEW = "2005";
    // 保险理赔录入
    public static final String ORDER_LOG_METHOD_RECORD_INVOICE = "2006";
    // 保险理赔完成
    public static final String ORDER_LOG_METHOD_FINISH_INSURANCE = "2007";
    // 保险回退
    public static final String ORDER_LOG_METHOD_BACK_INSURANCE = "2008";
    //保险归档资料上传
    public static final String ORDER_LOG_INSURANCE_ARCHIVED = "2009";
    //定损
    public static final String ORDER_LOG_METHOD_CONFIRM_LOSS = "3000";
    //定损审核
    public static final String ORDER_LOG_METHOD_WAIT_LOSS_AUDIT = "3001";
    //返厂寄件
    public static final String ORDER_LOG_METHOD_RETURN_EXPRESSES = "3002";
    //定损报价
    public static final String ORDER_LOG_METHOD_QUOTATION = "4000";
    //报价审核
    public static final String ORDER_LOG_METHOD_QUOTATION_AUDIT = "4001";
    //客户确认报价
    public static final String ORDER_LOG_METHOD_CONFIRM_QUOTATION = "4002";
    //工单结算
    public static final String ORDER_LOG_METHOD_SETTLE = "5000";
    //提交工单结算
    public static final String ORDER_LOG_METHOD_SETTLE_SUBMIT = "5001";
    //返厂收件
    public static final String ORDER_LOG_METHOD_RETURN_RECEIVE = "5002";
    //派单
    public static final String ORDER_LOG_METHOD_DISPATCH = "5003";
    //设备维修反馈
    public static final String ORDER_LOG_METHOD_MAINTAIN_FEEDBACK = "5004";
    //设备维修回退
    public static final String ORDER_LOG_METHOD_MAINTAIN_FAIL = "5005";
    //设备维修完成
    public static final String ORDER_LOG_METHOD_MAINTAINED = "5006";
    //重新定损
    public static final String ORDER_LOG_METHOD_CONFIRM_RE_LOSS = "5007";
    //维修检测完成
    public static final String ORDER_LOG_METHOD_CHECKED = "6001";
    //客户寄件
    public static final String ORDER_LOG_METHOD_CUSTOMER_EXPRESSES = "6002";

    //服务方式
    //本地维修
    public static final String ORDER_SERVICE_METHOD_LOCAL_MAINTAIN = "1";
    //返厂维修
    public static final String ORDER_SERVICE_METHOD_FACTORY_MAINTAIN = "2";
    //质保更新
    public static final String ORDER_SERVICE_METHOD_NEW_MAINTAIN = "3";

    //1：快递自寄；
    public static final String EXPRESSES_SELF = "1";

    //2：上门取件
    public static final String EXPRESSES_PICKUP = "2";

    //报价单类型
    //成本报价单
    public static final String QUOTE_TYPE_COST = "1";
    //客户报价单
    public static final String QUOTE_TYPE_CUSTOMER = "2";


    //报价结算类型
    //成本报价单
    public static final String SETTLE_TYPE_CUSTOMER = "1";
    //厂家报价单
    public static final String SETTLE_TYPE_FACTORY = "2";

    //取消类型
    //客户取消
    public static final String CANCEL_TYPE_USER = "1";
    //系统取消
    public static final String CANCEL_TYPE_SYSTEM = "2";

    //工单导入类型
    //框架工单导入
    public static final Integer ORDER_IMPORT_OUT_INSURANCE = 1;
    //DJ
    public static final Integer ORDER_IMPORT_DJ = 2;
    //鼎和保险
    public static final Integer ORDER_IMPORT_DH = 3;
    //二次返修
    public static final Integer ORDER_IMPORT_SECONDARY = 4;
}
