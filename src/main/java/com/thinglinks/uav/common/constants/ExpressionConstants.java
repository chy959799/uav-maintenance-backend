package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/4/17
 */
public class ExpressionConstants {

	/**
	 * 面单查询
	 */
	public static final String QUERY_ROUTES = "010004";

	/**
	 * 上门取件
	 */
	public static final String PICKUP_ORDER = "030001";

	/**
	 * 上门取件取消
	 */
	public static final String PICKUP_ORDER_CANCEL = "030004";

	/**
	 * 运单轨迹信息获取
	 */
	public static final String TRACK_INFO = "040001";

	/**
	 * 请求成功状态码
	 */
	public static final String SUCCESS_CODE = "00000";
	/**
	 * 请求失败状态码
	 */
	public static final String FAIL_CODE = "S00001";
}
