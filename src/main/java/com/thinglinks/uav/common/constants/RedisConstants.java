package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/3/20
 */
public class RedisConstants {

    private RedisConstants(){}

	public static final String LOGIN_CODE_KEY = "login:code:";
	public static final String LOGIN_COMPANY_USER_KEY = "login:company:";
	public static final Long LOGIN_CODE_TTL = 300L;
	public static final String LOGIN_USER_KEY = "login:token:";
	public static final Long LOGIN_USER_TTL = 7200L;
	public static final String REGISTER_CODE_KEY = "register:code:";
	public static final String USER_ROLE_KEY = "user:role:";
	public static final String ROLE_KEY = "role:";

	/**
	 * 验证码有效时间
	 */
	public static final Long CAPTCHA_EXPIRY_TIME_TTL = 300L;

	/**
	 * 验证码冷却时间
	 */
	public static final Long CAPTCHA_COUNTDOWN_TTL = 60L;
}
