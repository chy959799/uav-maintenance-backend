package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/4/15
 */
public class UserConstants {

	public static final Integer USER_ACTIVATE = 1;

	public static final String USER_BUCKET = "b-user";

	public static final String ADMIN_ROLE = "root";

	public static final String SYSTEM_MANAGEMENT_ROLE = "systemManagement";

	public static final String USER_ROLE = "Users";

	/**
	 * 登录类型: 账号密码
	 */
	public static final Integer LOGIN_TYPE_PASSWORD = 1;

	/**
	 * 登录类型: 微信授权
	 */
	public static final Integer LOGIN_TYPE_WECHAT = 2;
}
