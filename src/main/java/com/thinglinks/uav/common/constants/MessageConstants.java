package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/4/15
 */
public class MessageConstants {

	/**
	 * 普通消息
	 */
	public static final String TYPE_NORMAL = "1";

	/**
	 * 工单消息
	 */
	public static final String TYPE_ORDER = "2";

	/**
	 * 工单详情地址
	 */
	public static final String SYSTEM_ORDER_URL = "/order/detail?ordid=";

	/**
	 * 用户侧工单详情地址
	 */
	public static final String PERSONAL_ORDER_URL = "/personal/orderDetail?ordid=";

	public static final String PROJECT_URL = "/projects/";

	/**
	 * 个人级
	 */
	public static final String LEVEL_PERSONAL = "1";

	/**
	 * 部门级
	 */
	public static final String LEVEL_DEPARTMENT = "2";

	/**
	 * 公司级
	 */
	public static final String LEVEL_COMPANY = "3";

	/**
	 * 系统级
	 */
	public static final String LEVEL_SYSTEM = "4";


	/**
	 * 未读
	 */
	public static final String STATUS_UNREAD = "0";

	/**
	 * 已读
	 */
	public static final String STATUS_READ = "1";
}
