package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/4/3
 */
public class SparesConstants {

	/**
	 * 待初审
	 */
	public static final String STATUS_FIRST_AUDIT = "0";
	/**
	 * 待终审
	 */
	public static final String STATUS_FINAL_AUDIT = "1";
	/**
	 * 已完成
	 */
	public static final String STATUS_FINISH = "2";
	/**
	 * 已驳回
	 */
	public static final String STATUS_REJECT = "3";

	/**
	 * 通过
	 */
	public static final String PASS = "1";
	/**
	 * 驳回
	 */
	public static final String REJECT = "0";

	/**
	 * 新增
	 */
	public static final String TYPE_ADD = "新增";
	/**
	 * 修改
	 */
	public static final String TYPE_UPDATE = "修改";
	/**
	 * 删除
	 */
	public static final String TYPE_DELETE = "删除";
}
