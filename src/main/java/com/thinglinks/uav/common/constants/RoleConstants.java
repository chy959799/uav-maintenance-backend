package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/5/21
 */
public class RoleConstants {

    /**
     * 默认权限菜单编码
     */
    public static final String DEFAULT_MENU_NAME = "Personnel";
}
