package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/7/12
 */
public class CustomerConstants {

    /**
     * 网内单位
     */
    public static final String CUSTOMER_TYPE_INSIDE = "1";

    /**
     * 网外单位
     */
    public static final String CUSTOMER_TYPE_OUTSIDE = "2";
}
