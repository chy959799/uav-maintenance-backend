package com.thinglinks.uav.common.constants;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
public class StocksConstants {
    public static final Integer TYPE_IN = 1;

    public static final Integer TYPE_OUT = 2;

    public static final Integer AWAITING = 1;

    public static final Integer FINISH = 2;

    public static final Integer REJECT = 3;

    public static final Integer MIN_LIMIT = 20;

    public static final String STOCK_OUT_ORDER = "1";
    public static final String STOCK_OUT_BORROW = "5";

    //备件仓库
    public static final Integer STORE_SPARE = 1;

    //备品仓库
    public static final Integer STORE_STANDBY = 2;
    //仓库日志编码
    //申购初审
    public static final String PURCHASE_FIRST_REVIEW = "1001";
    //申购终审
    public static final String PURCHASE_LAST_REVIEW = "1002";
    //取消
    public static final String PURCHASE_CANCEL = "1003";
    //借用审核
    public static final String STOCK_BORROW_REVIEW = "2001";
    //物品领取
    public static final String STOCK_GET = "2002";
    //物品归还
    public static final String STOCK_RETURN = "2003";

    //物品操作类型
    //物品领取
    public static final Integer TYPE_GET = 1;
    //物品归还
    public static final Integer TYPE_RETURN = 2;
}
