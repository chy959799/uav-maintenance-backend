package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/4/15
 */
public class FaultConstants {
	public static final String FAULT_BUCKET = "b-fault";

	/**
	 * 手动创建
	 */
	public static final String FAULT_ORIGIN = "1";
}
