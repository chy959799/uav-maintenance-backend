package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/8/24
 */
public class TemplateConstants {

    public static final String DEFAULT_TEMPLATE_CODE = "1001";

    public static final String DEFAULT_TEMPLATE_NAME = "默认模板";

    public static final String DEFAULT_TEMPLATE_DESCRIPTION = "默认模板描述";

    public static final String TEMPLATE_BUCKET = "b-template";
}
