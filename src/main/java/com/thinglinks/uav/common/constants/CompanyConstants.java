package com.thinglinks.uav.common.constants;

/**
 * @author iwiFool
 * @date 2024/7/30
 */
public class CompanyConstants {

    /**
     * 系统公司
     */
    public static final String COMPANY_TYPE_SYSTEM = "1";

    /**
     * 租户公司
     */
    public static final String COMPANY_TYPE_TENANT = "2";
}
