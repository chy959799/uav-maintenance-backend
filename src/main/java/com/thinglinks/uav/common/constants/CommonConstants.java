package com.thinglinks.uav.common.constants;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/29
 */
public class CommonConstants {


    public static final Object NULL = null;

    public static final String TEMP_BUCKET = "b-temp";

    public static final Long HOUR_MILLS = 3600000L;

    public static final String DOT = ".";
    public static final String COMMA = ",";
    public static final String FILE_PDF = "pdf";
    public static final String FILE_DOCX = "docx";

    public static final String FILE_XLSX = "xlsx";

    public static final String MONEY = "¥";
    public static final Integer TURE_VALUE = 1;
    public static final String ONE_VALUE = "1";

    public static final Integer FALSE_VALUE = 0;
    public static final Integer ZERO = 0;

    public static final String TURE_VALUE_STRING = "1";

    public static final String FALSE_VALUE_STRING = "0";

    public static final String PASS = "passed";

    public static final String REJECTED = "rejected";

    //快递类型
    //设备收件
    public static final String EXPRESSES_DEVICE_RECEIVE = "1";
    //返厂寄件
    public static final String EXPRESSES_RETURN_FACTORY = "2";
    //客户寄件
    public static final String EXPRESSES_SEND_CUSTOMER = "3";

    //快递状态
    //正常
    public static final String EXPRESS_STATUS_NORMAL = "1";
    //取消
    public static final String EXPRESS_STATUS_CANCEL = "2";
    //完成
    public static final String EXPRESS_STATUS_FINISH = "3";

    public static final String SUCCESS_CODE = "A1000";

    //文件类型
    //工单报价文件
    public static final String FILE_TYPE_QUOTE = "1";
    //工单二维码
    public static final String FILE_TYPE_QR_CODE = "2";
    //保单编码截图
    public static final String FILE_TYPE_INSURANCE_SHOT = "3";
    //飞手证书
    public static final String FILE_TYPE_PILOT_CERT = "4";
    //固定资产卡片
    public static final String FILE_TYPE_FIXED_ASSET = "5";
    //事故照片-事故现场
    public static final String FILE_TYPE_ACCIDENT_SCENE = "6";
    //事故照片-损坏物品清单
    public static final String FILE_TYPE_ACCIDENT_DAMAGE_ITEMS = "7";
    //事故照片-损坏设备
    public static final String FILE_TYPE_ACCIDENT_DAMAGE_DEVICES = "8";
    //事故照片-设备正面
    public static final String FILE_TYPE_DEVICE_FRONT = "9";
    //事故照片-设备反面
    public static final String FILE_TYPE_DEVICE_REVERSE = "10";
    //事故照片-其他
    public static final String FILE_TYPE_ACCIDENT_OTHER = "11";
    //巡视工作单
    public static final String FILE_TYPE_LINE_WALK = "12";
    //情况说明
    public static final String FILE_TYPE_PRESENTATION = "13";
    //终版赔付资料
    public static final String FILE_TYPE_LAST_INSURANCE = "14";
    //赔付意向书模板
    public static final String INTENTION_TO_PAY = "15";
    //电力业务出现通知及索赔申请书
    public static final String POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM = "16";
    //损失清单
    public static final String LOSS_LIST = "17";
    //无人机故障报告
    public static final String FAULT_REPORT_TEMPLATE_PATH = "18";
    //事故照片
    public static final String FAULT_PHOTO = "19";
    //案件报告
    public static final String INCIDENT_REPORT = "20";
    //案件文件--无报案号则生成
    public static final String REPORT_DOCUMENT = "21";
    //客户报价单
    public static final String CUSTOMER_QUOTE = "22";
    //收费通知单
    public static final String FEE_NOTIFICATION = "23";
    //系统报价单
    public static final String SYSTEM_QUOTE = "24";
    //客户报价单不签名
    public static final String CUSTOMER_QUOTE_WITHOUT_SIGN = "25";
    //保险资料归档
    public static final String INSURANCE_ARCHIVED = "26";
    //emd
    public static final String EXPRESS_EMS = "ems";

    //收寄件人员
    public static final String ROLE_RECEIVE = "receiving";
    //客服人员
    public static final String ROLE_CUSTOMER_SERVICE = "customerService";
    //保险审核员
    public static final String ROLE_INSURANCE_AUDITOR = "insuranceAuditor";
    //定损工程师
    public static final String ROLE_LOSS_ENGINEER = "lossAssessmentEngineer";
    //综合员
    public static final String ROLE_COMPREHENSIVE_STAFF = "comprehensiveStaff";
    //维修工程师
    public static final String ROLE_MAINTENANCE_ENGINEER = "maintenanceEngineer";
    //检测工程师
    public static final String ROLE_TEST_ENGINEER = "testingEngineer";
    //寄件人员
    public static final String ROLE_SENDER = "sender";
    // 用户
    public static final String ROLE_USER = "User";
    //20空字符
    public static final String EMPTY_20 = "                    ";
    //查询所有工单列表权限
    public static final String PERMISSION_ALL_ORDER = "viewAllWorkOrders";
}
