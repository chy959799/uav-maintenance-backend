package com.thinglinks.uav.stock.service.impl;

import com.google.common.collect.Lists;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.StocksConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.ReviewResult;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.EasyExcelUtils;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.stock.dto.DetailStockLog;
import com.thinglinks.uav.stock.dto.excel.PurchaseItemExport;
import com.thinglinks.uav.stock.dto.purchase.*;
import com.thinglinks.uav.stock.entity.StockLog;
import com.thinglinks.uav.stock.entity.StockPurchItem;
import com.thinglinks.uav.stock.entity.StockPurchase;
import com.thinglinks.uav.stock.entity.Store;
import com.thinglinks.uav.stock.enums.PurchaseStatus;
import com.thinglinks.uav.stock.enums.PurchaseType;
import com.thinglinks.uav.stock.repository.StockLogRepository;
import com.thinglinks.uav.stock.repository.StockPurchItemRepository;
import com.thinglinks.uav.stock.repository.StockPurchaseRepository;
import com.thinglinks.uav.stock.repository.StoreRepository;
import com.thinglinks.uav.stock.service.PurchaseService;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    private static final String CHECK_STATUS_MESSAGE = "申购状态为:%s,无法进行当前操作";

    @Resource
    private StockPurchaseRepository stockPurchaseRepository;

    @Resource
    private StockPurchItemRepository stockPurchItemRepository;

    @Resource
    private StockLogRepository stockLogRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private StoreRepository storeRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addPurchase(AddPurchase request) throws Exception {
        String uid = CommonUtils.getUid();
        User user = userRepository.findByUid(uid);
        StockPurchase stockPurchase = new StockPurchase();
        stockPurchase.setPurchaseType(request.getPurchaseType());
        stockPurchase.setDeliveryAt(request.getDeliveryAt());
        stockPurchase.setRemark(request.getRemark());
        stockPurchase.setStcPrchId(CommonUtils.uuid());
        stockPurchase.setInnerCode(this.getInnerCode());
        stockPurchase.setUid(uid);
        stockPurchase.setStatus(PurchaseStatus.WAITING_FIRST_REVIEW.getCode());
        stockPurchase.setApplyAt(System.currentTimeMillis());
        stockPurchase.setDid(user.getDid());
        stockPurchase.setStoreId(request.getStoreId());
        stockPurchase.setCpid(CommonUtils.getCpid());
        stockPurchaseRepository.save(stockPurchase);
        request.getItems().forEach(x -> {
            StockPurchItem item = new StockPurchItem();
            item.setStcPrchId(stockPurchase.getStcPrchId());
            item.setStockCount(0);
            item.setCount(x.getCount());
            item.setPid(x.getPid());
            item.setStcPrchItemId(CommonUtils.uuid());
            stockPurchItemRepository.save(item);
        });
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> editPurchase(String stcPrchId, AddPurchase request) {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        if (Objects.isNull(stockPurchase)) {
            return BaseResponse.fail("申购ID错误");
        }
        if (!CommonUtils.checkStatus(stockPurchase.getStatus(), PurchaseStatus.WAITING_FIRST_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockPurchase.getStatus())));
        }
        stockPurchase.setPurchaseType(request.getPurchaseType());
        stockPurchase.setDeliveryAt(request.getDeliveryAt());
        stockPurchase.setRemark(request.getRemark());
        stockPurchase.setStoreId(request.getStoreId());
        stockPurchaseRepository.save(stockPurchase);
        stockPurchItemRepository.deleteByStcPrchId(stcPrchId);
        request.getItems().forEach(x -> {
            StockPurchItem item = new StockPurchItem();
            item.setStcPrchId(stockPurchase.getStcPrchId());
            item.setCount(x.getCount());
            item.setStockCount(0);
            item.setPid(x.getPid());
            item.setStcPrchItemId(CommonUtils.uuid());
            stockPurchItemRepository.save(item);
        });
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> firsReviewPurchase(String stcPrchId, ReviewResult request) throws Exception {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        if (Objects.isNull(stockPurchase)) {
            return BaseResponse.fail("申购ID错误");
        }
        if (!CommonUtils.checkStatus(stockPurchase.getStatus(), PurchaseStatus.WAITING_FIRST_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockPurchase.getStatus())));
        }
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            stockPurchase.setStatus(PurchaseStatus.WAITING_LAST_REVIEW.getCode());
        } else {
            stockPurchase.setStatus(PurchaseStatus.REJECT.getCode());
        }
        stockPurchase.setReviewFirstRemark(request.getReviewRemark());
        stockPurchaseRepository.save(stockPurchase);
        this.savePurchaseLog(stcPrchId, StocksConstants.PURCHASE_FIRST_REVIEW);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> lastReviewPurchase(String stcPrchId, ReviewResult request) throws Exception {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        if (Objects.isNull(stockPurchase)) {
            return BaseResponse.fail("申购ID错误");
        }
        if (!CommonUtils.checkStatus(stockPurchase.getStatus(), PurchaseStatus.WAITING_LAST_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockPurchase.getStatus())));
        }
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            stockPurchase.setStatus(PurchaseStatus.WAITING_PURCHASE.getCode());
        } else {
            stockPurchase.setStatus(PurchaseStatus.REJECT.getCode());
        }
        stockPurchase.setReviewAt(System.currentTimeMillis());
        stockPurchase.setReviewLastRemark(request.getReviewRemark());
        stockPurchaseRepository.save(stockPurchase);
        this.savePurchaseLog(stcPrchId, StocksConstants.PURCHASE_LAST_REVIEW);
        return BaseResponse.success();
    }

    @Override
    public BasePageResponse<PagePurchase> pagePurchase(PagePurchaseQuery request) {
        PageRequest pageRequest = request.of();
        Specification<StockPurchase> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> listAnd = new ArrayList<>();
            if (!CommonUtils.isAdmin()) {
                criteriaBuilder.equal(root.get("cpid"), CommonUtils.getCpid());
            }
            if (StringUtils.isNotBlank(request.getInnerCode())) {
                listAnd.add(criteriaBuilder.like(root.get("innerCode"), CommonUtils.assembleLike(request.getInnerCode())));
            }
            if (Objects.nonNull(request.getPurchaseType())) {
                listAnd.add(criteriaBuilder.equal(root.get("purchaseType"), request.getPurchaseType()));
            }
            if (StringUtils.isNotBlank(request.getStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
            }
            if (StringUtils.isNotBlank(request.getDid())) {
                listAnd.add(criteriaBuilder.equal(root.get("did"), request.getDid()));
            }
            if (StringUtils.isNotBlank(request.getUserName())) {
                Join<StockPurchase, User> join = root.join("user", JoinType.LEFT);
                listAnd.add(criteriaBuilder.like(join.get("userName"), CommonUtils.assembleLike(request.getUserName())));
            }
            if (Objects.nonNull(request.getDeliveryEndAt())) {
                listAnd.add(criteriaBuilder.greaterThan(root.get("deliveryAt"), request.getDeliveryStartAt()));
            }
            if (Objects.nonNull(request.getDeliveryEndAt())) {
                listAnd.add(criteriaBuilder.lessThan(root.get("deliveryAt"), request.getDeliveryEndAt()));
            }
            if (Objects.nonNull(request.getCompleteStartAt())) {
                listAnd.add(criteriaBuilder.greaterThan(root.get("completeAt"), request.getCompleteStartAt()));
            }
            if (Objects.nonNull(request.getCompleteEndAt())) {
                listAnd.add(criteriaBuilder.lessThan(root.get("completeAt"), request.getCompleteEndAt()));
            }
            Predicate[] p1 = new Predicate[listAnd.size()];
            return criteriaBuilder.and(listAnd.toArray(p1));
        };
        Page<StockPurchase> page = stockPurchaseRepository.findAll(specification, pageRequest);
        List<PagePurchase> results = page.stream().map(x -> {
            PagePurchase data = new PagePurchase();
            BeanUtils.copyProperties(x, data);
            data.setDeliveryTime(DateUtils.formatDateLine(x.getDeliveryAt()));
            if (Objects.nonNull(x.getApplyAt())) {
                data.setApplyTime(DateUtils.formatDateTime(x.getApplyAt()));
            }
            if (Objects.nonNull(x.getReviewAt())) {
                data.setReviewTime(DateUtils.formatDateTime(x.getReviewAt()));
            }
            if (Objects.nonNull(x.getCompleteAt())) {
                data.setCompleteTime(DateUtils.formatDateTime(x.getCompleteAt()));
            }
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<DetailPurchase> detailPurchase(String stcPrchId) {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        if (Objects.isNull(stockPurchase)) {
            return BaseResponse.fail("申购ID错误");
        }
        DetailPurchase data = new DetailPurchase();
        BeanUtils.copyProperties(stockPurchase, data);
        data.setDeliveryTime(DateUtils.formatDateLine(stockPurchase.getDeliveryAt()));
        data.setCreateTime(DateUtils.formatDateTime(stockPurchase.getCt()));
        if (Objects.nonNull(stockPurchase.getApplyAt())) {
            data.setApplyTime(DateUtils.formatDateTime(stockPurchase.getApplyAt()));
        }
        if (Objects.nonNull(stockPurchase.getReviewAt())) {
            data.setReviewTime(DateUtils.formatDateTime(stockPurchase.getReviewAt()));
        }
        if (Objects.nonNull(stockPurchase.getCompleteAt())) {
            data.setCompleteTime(DateUtils.formatDateTime(stockPurchase.getCompleteAt()));
        }
        data.setStore(storeRepository.findByStoreid(stockPurchase.getStoreId()));
        return BaseResponse.success(data);
    }

    @Override
    public BasePageResponse<PagePurchaseItem> pagePurchaseItem(PagePurchaseItemQuery request) {
        PageRequest pageRequest = request.of();
        StockPurchItem stockPurchItem = new StockPurchItem();
        stockPurchItem.setStcPrchId(request.getStcPrchId());
        if (Objects.nonNull(request.getInnerCode())) {
            StockPurchase stockPurchase = stockPurchaseRepository.findByInnerCode(request.getInnerCode());
            if (Objects.nonNull(stockPurchase)) {
                stockPurchItem.setStcPrchId(stockPurchase.getStcPrchId());
            }
        }
        Example<StockPurchItem> example = Example.of(stockPurchItem);
        Page<StockPurchItem> page = stockPurchItemRepository.findAll(example, pageRequest);
        List<PagePurchaseItem> results = page.stream().map(x -> {
            PagePurchaseItem item = new PagePurchaseItem();
            BeanUtils.copyProperties(x, item);
            return item;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<List<DetailStockLog>> queryPurchaseLogs(String stcPrchId) {
        List<StockLog> logs = stockLogRepository.findByStcPrchId(stcPrchId);
        return BaseResponse.success(logs.stream().map(x -> {
            DetailStockLog detail = new DetailStockLog();
            BeanUtils.copyProperties(x, detail);
            detail.setOperateTime(DateUtils.formatDateTime(x.getCt()));
            return detail;
        }).collect(Collectors.toList()));
    }

    @Override
    public void exportPurchaseItem(HttpServletResponse response, String stcPrchId) throws Exception {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        List<StockPurchItem> stockPurchItems = stockPurchItemRepository.findByStcPrchId(stcPrchId);
        Store store = storeRepository.findByStoreid(stockPurchase.getStoreId());
        List<PurchaseItemExport> data = new ArrayList<>();
        int size = stockPurchItems.size();
        for (int i = 0; i < size; i++) {
            StockPurchItem item = stockPurchItems.get(i);
            PurchaseItemExport export = new PurchaseItemExport();
            export.setIndex(i + 1);
            export.setInnerCode(stockPurchase.getInnerCode());
            export.setPurchaseType(PurchaseType.fromCode(stockPurchase.getPurchaseType()));
            export.setStoreName(store.getName());
            Product product = item.getProduct();
            if (Objects.nonNull(product)) {
                export.setProductName(product.getName());
                Category category = product.getCategory();
                if (Objects.nonNull(category)) {
                    export.setCategoryName(category.getName());
                    export.setCategoryCode(category.getInnerCode());
                }
                export.setModel(product.getModel());
                export.setUnit(product.getUnit());
                export.setOrigin(product.getOrigin());
            }
            export.setCount(item.getCount());
            data.add(export);
        }
        String fileName = "申购单_".concat(stockPurchase.getInnerCode());
        EasyExcelUtils.exportXlsx(response, fileName, "申购清单", data, PurchaseItemExport.class);
    }

    @Override
    public BaseResponse<List<DetailPurchase>> queryPurchaseCode(String code, String storeId) {
        Sort sort = Sort.by(Sort.Direction.DESC, "ut");
        Specification<StockPurchase> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> listAnd = new ArrayList<>();
            if (!CommonUtils.isAdmin()) {
                criteriaBuilder.equal(root.get("cpid"), CommonUtils.getCpid());
            }
            if (StringUtils.isNotBlank(code)) {
                listAnd.add(criteriaBuilder.like(root.get("innerCode"), CommonUtils.assembleLike(code)));
            }
            if (StringUtils.isNotBlank(storeId)) {
                listAnd.add(criteriaBuilder.equal(root.get("storeId"), storeId));
            }
            ArrayList<String> status = Lists.newArrayList(PurchaseStatus.PART_PURCHASE.getCode(), PurchaseStatus.WAITING_PURCHASE.getCode());
            CriteriaBuilder.In<Object> in = criteriaBuilder.in(root.get("status"));
            in.value(status);
            listAnd.add(in);
            Predicate[] p1 = new Predicate[listAnd.size()];
            return criteriaBuilder.and(listAnd.toArray(p1));
        };
        List<StockPurchase> stockPurchases = stockPurchaseRepository.findAll(specification, sort);
        return BaseResponse.success(stockPurchases.stream().map(x -> {
            DetailPurchase data = new DetailPurchase();
            BeanUtils.copyProperties(x, data);
            data.setDeliveryTime(DateUtils.formatDateLine(x.getDeliveryAt()));
            data.setCreateTime(DateUtils.formatDateTime(x.getCt()));
            if (Objects.nonNull(x.getApplyAt())) {
                data.setApplyTime(DateUtils.formatDateTime(x.getApplyAt()));
            }
            if (Objects.nonNull(x.getReviewAt())) {
                data.setReviewTime(DateUtils.formatDateTime(x.getReviewAt()));
            }
            if (Objects.nonNull(x.getCompleteAt())) {
                data.setCompleteTime(DateUtils.formatDateTime(x.getCompleteAt()));
            }
            data.setStore(storeRepository.findByStoreid(x.getStoreId()));
            return data;
        }).collect(Collectors.toList()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deletePurchase(String stcPrchId) {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        if (Objects.isNull(stockPurchase)) {
            return BaseResponse.fail("申购ID错误");
        }
        if (!CommonUtils.checkStatus(stockPurchase.getStatus(), PurchaseStatus.CANCEL.getCode(), PurchaseStatus.FINISH.getCode(), PurchaseStatus.REJECT.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockPurchase.getStatus())));
        }
        stockPurchaseRepository.deleteByStcPrchId(stcPrchId);
        stockPurchItemRepository.deleteByStcPrchId(stcPrchId);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deletePurchase(List<String> stcPrchIds) throws Exception {
        if (CollectionUtils.isEmpty(stcPrchIds)) {
            return BaseResponse.success();
        }
        stcPrchIds.forEach(this::deletePurchase);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> cancelPurchase(String stcPrchId) {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        if (Objects.isNull(stockPurchase)) {
            return BaseResponse.fail("申购ID错误");
        }
        if (!CommonUtils.checkStatus(stockPurchase.getStatus(), PurchaseStatus.WAITING_FIRST_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockPurchase.getStatus())));
        }
        stockPurchase.setStatus(PurchaseStatus.CANCEL.getCode());
        stockPurchaseRepository.save(stockPurchase);
        this.savePurchaseLog(stcPrchId, StocksConstants.PURCHASE_LAST_REVIEW);
        return BaseResponse.success();
    }

    /**
     * 获取工单编号
     *
     * @return
     */
    private String getInnerCode() throws Exception {
        int maxLoop = 10000;
        int loop = 1;
        while (loop < maxLoop) {
            String inderCode = "SG".concat(DateUtils.formatMonth(System.currentTimeMillis())).concat(String.format("%04d", loop));
            if (!stockPurchaseRepository.existsByInnerCode(inderCode)) {
                return inderCode;
            }
            loop++;
        }
        throw new CustomBizException("本月编号已用完");
    }


    /**
     * 保存申购日志
     */
    private void savePurchaseLog(String stcPrchId, String method) {
        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stcPrchId);
        StockLog stockLog = new StockLog();
        StringBuilder sb = new StringBuilder();
        if (StocksConstants.PURCHASE_FIRST_REVIEW.equals(method)) {
            if (PurchaseStatus.WAITING_LAST_REVIEW.getCode().equals(stockPurchase.getStatus())) {
                stockLog.setApprove(CommonConstants.PASS);
            } else {
                stockLog.setApprove(CommonConstants.REJECTED);
            }
            sb.append(stockPurchase.getReviewFirstRemark());
        } else if (StocksConstants.PURCHASE_LAST_REVIEW.equals(method)) {
            if (PurchaseStatus.WAITING_PURCHASE.getCode().equals(stockPurchase.getStatus())) {
                stockLog.setApprove(CommonConstants.PASS);
            } else {
                stockLog.setApprove(CommonConstants.REJECTED);
            }
            sb.append(stockPurchase.getReviewLastRemark());
        } else if (StocksConstants.PURCHASE_CANCEL.equals(method)) {
            sb.append("申购取消");
        }
        stockLog.setDescription(sb.toString());
        stockLog.setMethod(method);
        stockLog.setUid(CommonUtils.getUid());
        stockLog.setStcPrchId(stcPrchId);
        stockLog.setStatus(stockPurchase.getStatus());
        stockLog.setStcLogId(CommonUtils.uuid());
        stockLogRepository.save(stockLog);
    }
}
