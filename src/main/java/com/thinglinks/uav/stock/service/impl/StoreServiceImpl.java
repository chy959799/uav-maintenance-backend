package com.thinglinks.uav.stock.service.impl;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.stock.dto.store.AddStore;
import com.thinglinks.uav.stock.dto.store.EditStore;
import com.thinglinks.uav.stock.dto.store.PageStore;
import com.thinglinks.uav.stock.dto.store.PageStoreQuery;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.entity.Store;
import com.thinglinks.uav.stock.repository.StockPurchaseRepository;
import com.thinglinks.uav.stock.repository.StockRepository;
import com.thinglinks.uav.stock.repository.StoreRepository;
import com.thinglinks.uav.stock.service.StoreService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
@Service
public class StoreServiceImpl implements StoreService {

    private static final Integer DEFAULT_STORES_TYPE = 1;

    @Resource
    private StoreRepository storeRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private StockPurchaseRepository stockPurchaseRepository;

    @Override
    public BasePageResponse<PageStore> page(PageStoreQuery request) {
        PageRequest pageRequest = request.of();
        Store store = new Store();
        store.setName(request.getName());
        store.setStoreType(request.getStoreType());
        if (!CommonUtils.isAdmin()) {
            store.setCpid(CommonUtils.getCpid());
        }
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatcher::contains);
        Example<Store> example = Example.of(store, matcher);
        Page<Store> page = storeRepository.findAll(example, pageRequest);
        List<PageStore> results = page.stream().map(x -> {
            PageStore data = new PageStore();
            BeanUtils.copyProperties(x, data);
            data.setCreatedAt(DateUtils.formatDateTime(x.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(x.getUt()));
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addStores(AddStore request) {
        Store store = new Store();
        store.setName(request.getName());
        store.setCode(request.getCode());
        store.setRemark(request.getRemark());
        store.setMinLimit(request.getMinLimit());
        store.setMaxLimit(request.getMaxLimit());
        store.setRatio(request.getRatio());
        store.setAllowEmpty(Boolean.TRUE);
        store.setDefaultStores(request.getDefaultStores());
        store.setStoreType(request.getStoreType());
        store.setStoreid(CommonUtils.uuid());
        store.setUid(CommonUtils.getUid());
        store.setCpid(CommonUtils.getCpid());
        store.setAllowEmpty(request.getAllowEmpty());
        store.setDefaultStores(request.getDefaultStores());
        if (StringUtils.isNotBlank(request.getType())) {
            store.setType(Integer.valueOf(request.getType()));
        }
        //同时只能有一个默认仓库,如果不是设置默认仓库，且数据库中没有默认仓库，则设置为默认仓库
        if (Objects.nonNull(request.getDefaultStores()) && request.getDefaultStores()) {
            List<Store> stores = storeRepository.findByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, request.getStoreType(), CommonUtils.getCpid());
            stores.forEach(x -> x.setDefaultStores(Boolean.FALSE));
            storeRepository.saveAll(stores);
        } else {
            if (!storeRepository.existsByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, request.getStoreType(), CommonUtils.getCpid())) {
                store.setDefaultStores(Boolean.TRUE);
            }
        }
        storeRepository.save(store);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> editStores(String storesId, EditStore request) {
        Store store = storeRepository.findByStoreid(storesId);
        if (Objects.isNull(store)) {
            return BaseResponse.fail("仓库不存在");
        }
        //同时只能有一个默认仓库
        if (Objects.nonNull(request.getDefaultStores()) && request.getDefaultStores()) {
            List<Store> stores = storeRepository.findByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, store.getStoreType(), CommonUtils.getCpid());
            if (CollectionUtils.isNotEmpty(stores)) {
                stores.forEach(x -> x.setDefaultStores(Boolean.FALSE));
                storeRepository.saveAll(stores);
            }
        }
        Integer bt = store.getType();
        BeanUtils.copyProperties(request, store, CommonUtils.getNullPropertyNames(request));
        store.setStoreid(storesId);
        store.setType(bt);
        if (StringUtils.isNotBlank(request.getType())) {
            store.setType(Integer.valueOf(request.getType()));
        }
        store.setAllowEmpty(request.getAllowEmpty());
        store.setDefaultStores(request.getDefaultStores());
        storeRepository.save(store);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> delStores(String storesId) {
        List<Stock> stocks = stockRepository.findByStoreid(storesId);
        List<Stock> hasStock = stocks.stream().filter(x -> x.getCount() > 0).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(hasStock)) {
            return BaseResponse.fail("不能删除有库存的仓库");
        }
        if (stockPurchaseRepository.existsByStoreId(storesId)) {
            return BaseResponse.fail("不能删除有申购的仓库");
        }
        storeRepository.deleteByStoreid(storesId);
        return BaseResponse.success();
    }
}
