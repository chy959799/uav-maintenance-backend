package com.thinglinks.uav.stock.service.impl;

import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.StocksConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.Remark;
import com.thinglinks.uav.common.dto.ReviewResult;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.stock.dto.*;
import com.thinglinks.uav.stock.dto.borrow.*;
import com.thinglinks.uav.stock.entity.*;
import com.thinglinks.uav.stock.enums.BorrowStatus;
import com.thinglinks.uav.stock.enums.PurchaseStatus;
import com.thinglinks.uav.stock.enums.StockInType;
import com.thinglinks.uav.stock.repository.*;
import com.thinglinks.uav.stock.service.BorrowService;
import com.thinglinks.uav.stock.service.StockService;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BorrowServiceImpl implements BorrowService {
    private static final String CHECK_STATUS_MESSAGE = "借用状态为:%s,无法进行当前操作";

    @Resource
    private StockBorrowRepository stockBorrowRepository;

    @Resource
    private StockBorrowItemRepository stockBorrowItemRepository;

    @Resource
    private StockReturnRepository stockReturnRepository;

    @Resource
    private StockReturnItemRepository stockReturnItemRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private StockInOutRepository stockInOutRepository;

    @Resource
    private StockInOutItemRepository stockInOutItemRepository;

    @Resource
    private StockService stockService;

    @Resource
    private ProductRepository productRepository;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addBorrow(AddBorrow request) throws Exception {
        String uid = CommonUtils.getUid();
        for (BorrowItem item : request.getItems()) {
            Stock stock = stockRepository.findByPidAndStoreid(item.getPid(), request.getStoreId());
            if (Objects.isNull(stock)) {
                Product product = productRepository.findByPid(item.getPid());
                return BaseResponse.fail("库存数量异常：".concat(product.getName()));
            }
            if (item.getCount() > stock.getCount()) {
                return BaseResponse.fail("借用数量不能大于库存数量");
            }
        }
        User user = userRepository.findByUid(uid);
        StockBorrow stockBorrow = new StockBorrow();
        stockBorrow.setApplyUid(uid);
        stockBorrow.setApplyAt(System.currentTimeMillis());
        stockBorrow.setStcBrwId(CommonUtils.uuid());
        stockBorrow.setDid(user.getDid());
        stockBorrow.setStoreId(request.getStoreId());
        stockBorrow.setType(request.getType());
        stockBorrow.setStatus(BorrowStatus.WAITING_REVIEW.getCode());
        stockBorrow.setPlanReturnAt(request.getPlanReturnAt());
        stockBorrow.setUsage(request.getUsage());
        stockBorrow.setInnerCode(this.getInnerCode());
        stockBorrow.setCpid(CommonUtils.getCpid());
        List<StockBorrowItem> items = request.getItems().stream().map(x -> {
            StockBorrowItem item = new StockBorrowItem();
            item.setCount(x.getCount());
            item.setPid(x.getPid());
            item.setStcBrwItemId(CommonUtils.uuid());
            item.setStcBrwId(stockBorrow.getStcBrwId());
            item.setStoreid(request.getStoreId());
            item.setReturnCount(CommonConstants.ZERO);
            return item;
        }).collect(Collectors.toList());
        stockBorrowItemRepository.saveAll(items);
        stockBorrowRepository.save(stockBorrow);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewBorrow(String stcBrwId, ReviewResult request) throws Exception {
        String uid = CommonUtils.getUid();
        StockBorrow stockBorrow = stockBorrowRepository.findByStcBrwId(stcBrwId);
        if (Objects.isNull(stockBorrow)) {
            return BaseResponse.fail("借用ID错误");
        }
        if (!CommonUtils.checkStatus(stockBorrow.getStatus(), BorrowStatus.WAITING_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockBorrow.getStatus())));
        }
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            stockBorrow.setStatus(BorrowStatus.WAITING_GET.getCode());
        } else {
            stockBorrow.setStatus(BorrowStatus.REJECT.getCode());
        }
        stockBorrow.setReviewAt(System.currentTimeMillis());
        stockBorrow.setReviewRemark(request.getReviewRemark());
        stockBorrow.setReviewResult(request.getReviewResult());
        stockBorrow.setReviewUid(uid);
        stockBorrowRepository.save(stockBorrow);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deleteBorrow(String stcBrwId) {
        StockBorrow stockBorrow = stockBorrowRepository.findByStcBrwId(stcBrwId);
        if (Objects.isNull(stockBorrow)) {
            return BaseResponse.fail("借用ID错误");
        }
        if (!CommonUtils.checkStatus(stockBorrow.getStatus(), BorrowStatus.CANCEL.getCode(), BorrowStatus.FINISH.getCode(), BorrowStatus.REJECT.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockBorrow.getStatus())));
        }
        stockBorrowRepository.deleteByStcBrwId(stcBrwId);
        stockBorrowItemRepository.deleteByStcBrwId(stcBrwId);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> cancelBorrow(String stcBrwId) {
        StockBorrow stockBorrow = stockBorrowRepository.findByStcBrwId(stcBrwId);
        if (Objects.isNull(stockBorrow)) {
            return BaseResponse.fail("借用ID错误");
        }
        if (!CommonUtils.checkStatus(stockBorrow.getStatus(), BorrowStatus.WAITING_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockBorrow.getStatus())));
        }
        stockBorrow.setStatus(PurchaseStatus.CANCEL.getCode());
        stockBorrowRepository.save(stockBorrow);
        return BaseResponse.success();
    }

    @Override
    public BasePageResponse<PageBorrow> pageBorrow(PageBorrowQuery request) {
        PageRequest pageRequest = request.of();
        Specification<StockBorrow> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> listAnd = new ArrayList<>();
            if (!CommonUtils.isAdmin()) {
                criteriaBuilder.equal(root.get("cpid"), CommonUtils.getCpid());
            }
            if (StringUtils.isNotBlank(request.getInnerCode())) {
                listAnd.add(criteriaBuilder.like(root.get("innerCode"), CommonUtils.assembleLike(request.getInnerCode())));
            }
            if (Objects.nonNull(request.getBorrowType())) {
                listAnd.add(criteriaBuilder.equal(root.get("type"), request.getBorrowType()));
            }
            if (StringUtils.isNotBlank(request.getStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
            }
            if (StringUtils.isNotBlank(request.getDid())) {
                listAnd.add(criteriaBuilder.equal(root.get("did"), request.getDid()));
            }
            if (StringUtils.isNotBlank(request.getStoreId())) {
                listAnd.add(criteriaBuilder.equal(root.get("storeId"), request.getStoreId()));
            }
            if (StringUtils.isNotBlank(request.getUserName())) {
                Join<StockBorrow, User> join = root.join("applyUser", JoinType.LEFT);
                listAnd.add(criteriaBuilder.like(join.get("userName"), CommonUtils.assembleLike(request.getUserName())));
            }
            if (Objects.nonNull(request.getApplyStartAt())) {
                listAnd.add(criteriaBuilder.greaterThan(root.get("applyAt"), request.getApplyStartAt()));
            }
            if (Objects.nonNull(request.getApplyEndAt())) {
                listAnd.add(criteriaBuilder.lessThan(root.get("applyAt"), request.getApplyEndAt()));
            }
            if (Objects.nonNull(request.getReturnStartAt())) {
                listAnd.add(criteriaBuilder.greaterThan(root.get("returnAt"), request.getReturnStartAt()));
            }
            if (Objects.nonNull(request.getReturnEndAt())) {
                listAnd.add(criteriaBuilder.lessThan(root.get("returnAt"), request.getReturnEndAt()));
            }
            Predicate[] p1 = new Predicate[listAnd.size()];
            return criteriaBuilder.and(listAnd.toArray(p1));
        };
        Page<StockBorrow> page = stockBorrowRepository.findAll(specification, pageRequest);
        List<PageBorrow> results = page.stream().map(x -> {
            PageBorrow data = new PageBorrow();
            BeanUtils.copyProperties(x, data);
            if (Objects.nonNull(x.getApplyAt())) {
                data.setApplyTime(DateUtils.formatDateTime(x.getApplyAt()));
            }
            if (Objects.nonNull(x.getReviewAt())) {
                data.setReviewTime(DateUtils.formatDateTime(x.getReviewAt()));
            }
            if (Objects.nonNull(x.getPlanReturnAt())) {
                data.setPlanReturnTime(DateUtils.formatDateTime(x.getPlanReturnAt()));
            }
            if (Objects.nonNull(x.getReturnAt())) {
                data.setReturnTime(DateUtils.formatDateTime(x.getReturnAt()));
            }
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<DetailBorrow> detailBorrow(String stcBrwId) {
        StockBorrow stockBorrow = stockBorrowRepository.findByStcBrwId(stcBrwId);
        if (Objects.isNull(stockBorrow)) {
            return BaseResponse.fail("借用ID错误");
        }
        DetailBorrow data = new DetailBorrow();
        BeanUtils.copyProperties(stockBorrow, data);
        if (Objects.nonNull(stockBorrow.getApplyAt())) {
            data.setApplyTime(DateUtils.formatDateTime(stockBorrow.getApplyAt()));
        }
        if (Objects.nonNull(stockBorrow.getReviewAt())) {
            data.setReviewTime(DateUtils.formatDateTime(stockBorrow.getReviewAt()));
        }
        if (Objects.nonNull(stockBorrow.getPlanReturnAt())) {
            data.setPlanReturnTime(DateUtils.formatDateTime(stockBorrow.getPlanReturnAt()));
        }
        if (Objects.nonNull(stockBorrow.getReturnAt())) {
            data.setReturnTime(DateUtils.formatDateTime(stockBorrow.getReturnAt()));
        }
        return BaseResponse.success(data);
    }

    @Override
    public BasePageResponse<PageBorrowItem> pageBorrowItem(PageBorrowItemQuery request) {
        PageRequest pageRequest = request.of();
        StockBorrowItem stockBorrowItem = new StockBorrowItem();
        stockBorrowItem.setStcBrwId(request.getStcBrwId());
        if (Objects.nonNull(request.getInnerCode())) {
            StockBorrow stockBorrow = stockBorrowRepository.findByInnerCode(request.getInnerCode());
            if (Objects.nonNull(stockBorrow)) {
                stockBorrowItem.setStcBrwId(stockBorrow.getStcBrwId());
            }
        }
        Example<StockBorrowItem> example = Example.of(stockBorrowItem);
        Page<StockBorrowItem> page = stockBorrowItemRepository.findAll(example, pageRequest);
        List<PageBorrowItem> results = page.stream().map(x -> {
            PageBorrowItem item = new PageBorrowItem();
            BeanUtils.copyProperties(x, item);
            return item;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> getBorrow(String stcBrwId, Remark remark) throws Exception {
        StockBorrow stockBorrow = stockBorrowRepository.findByStcBrwId(stcBrwId);
        if (Objects.isNull(stockBorrow)) {
            return BaseResponse.fail("借用ID错误");
        }
        if (!CommonUtils.checkStatus(stockBorrow.getStatus(), BorrowStatus.WAITING_GET.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, PurchaseStatus.fromCode(stockBorrow.getStatus())));
        }
        //出库
        StockOut stockOut = new StockOut();
        List<StockBorrowItem> borrowItems = stockBorrowItemRepository.findByStcBrwId(stcBrwId);
        stockOut.setOutType(StocksConstants.STOCK_OUT_BORROW);
        stockOut.setStoreid(stockBorrow.getStoreId());
        List<StockOutItem> stockOutItems = borrowItems.stream().map(x -> {
            StockOutItem outItem = new StockOutItem();
            outItem.setPid(x.getPid());
            outItem.setCount(x.getCount());
            return outItem;
        }).collect(Collectors.toList());
        stockOut.setItems(stockOutItems);
        stockOut.setStcBrwId(stcBrwId);
        BaseResponse<StockInOut> ret = stockService.stocksOut(stockOut);
        if (!StatusEnum.SUCCESS.getCode().equals(ret.getCode())) {
            throw new CustomBizException(ret.getMsg());
        }
        //自动审核
        StockReviewResult reviewResult = new StockReviewResult();
        reviewResult.setReviewResult(CommonConstants.PASS);
        reviewResult.setReviewRemark("自动审核通过");
        stockService.stocksOutReview(ret.getResults().getStcInoutId(), reviewResult);
        stockBorrow.setStatus(BorrowStatus.WAITING_RETURN.getCode());
        stockBorrowRepository.save(stockBorrow);
        //更新借用数量
        borrowItems.forEach(x -> {
            Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), x.getStoreid());
            stock.setBorrowCount(Optional.ofNullable(stock.getBorrowCount()).orElse(0L) + x.getCount());
            stockRepository.save(stock);
        });
        //保留领用记录
        StockReturn stockReturn = new StockReturn();
        stockReturn.setStcRetId(CommonUtils.uuid());
        stockReturn.setType(StocksConstants.TYPE_GET);
        stockReturn.setRemark(remark.getRemark());
        stockReturn.setUid(CommonUtils.getUid());
        stockReturn.setStcBrwId(stockBorrow.getStcBrwId());
        stockReturn.setStcInoutId(ret.getResults().getStcInoutId());
        stockReturn.setOutInCode(ret.getResults().getInnerCode());
        stockReturnRepository.save(stockReturn);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> stockReturn(AddReturn request) throws Exception {
        StockBorrow stockBorrow = stockBorrowRepository.findByStcBrwId(request.getStcBrwId());
        if (Objects.isNull(stockBorrow)) {
            return BaseResponse.fail("借用ID错误");
        }
        StockInOut stockOut = stockInOutRepository.findByStcBrwIdAndType(stockBorrow.getStcBrwId(), StocksConstants.TYPE_OUT);
        StockIn stockIn = new StockIn();
        stockIn.setInType(StockInType.BORROW_RETURN.getCode());
        stockIn.setRemark(request.getRemark());
        stockIn.setStoreid(stockBorrow.getStoreId());
        List<StockBorrowItem> borrowItems = new ArrayList<>();
        StockReturn stockReturn = new StockReturn();
        stockReturn.setStcRetId(CommonUtils.uuid());
        List<StockReturnItem> returnItems = new ArrayList<>();
        List<StockInItem> stockInItems = request.getItems().stream().map(x -> {
            StockInItem stockInItem = new StockInItem();
            stockInItem.setCount(x.getCount());
            stockInItem.setPid(x.getPid());
            StockInOutItem outItem = stockInOutItemRepository.findByStcInOutIdAndPid(stockOut.getStcInoutId(), x.getPid());
            stockInItem.setPrice(outItem.getPrice());
            StockBorrowItem borrowItem = stockBorrowItemRepository.findByStcBrwIdAndPid(stockBorrow.getStcBrwId(), x.getPid());
            borrowItem.setReturnCount(Optional.ofNullable(borrowItem.getReturnCount()).orElse(0) + x.getCount());
            borrowItems.add(borrowItem);
            StockReturnItem returnItem = new StockReturnItem();
            returnItem.setCount(x.getCount());
            returnItem.setRemark(request.getRemark());
            returnItem.setPid(x.getPid());
            returnItem.setStcRetId(stockReturn.getStcRetId());
            returnItem.setStcRetItemId(CommonUtils.uuid());
            returnItems.add(returnItem);
            return stockInItem;
        }).collect(Collectors.toList());
        stockIn.setItems(stockInItems);
        BaseResponse<StockInOut> ret = stockService.stocksIn(stockIn);
        if (!StatusEnum.SUCCESS.getCode().equals(ret.getCode())) {
            throw new CustomBizException(ret.getMsg());
        }
        //更新借用数量
        stockInItems.forEach(x -> {
            Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), stockOut.getStoreid());
            stock.setBorrowCount(stock.getBorrowCount() - x.getCount());
            stockRepository.save(stock);
        });
        //自动审核
        StockReviewResult reviewResult = new StockReviewResult();
        reviewResult.setReviewResult(CommonConstants.PASS);
        reviewResult.setReviewRemark("自动审核通过");
        stockService.stocksInReview(ret.getResults().getStcInoutId(), reviewResult);
        stockBorrowItemRepository.saveAll(borrowItems);
        List<StockBorrowItem> allBorrowItems = stockBorrowItemRepository.findByStcBrwId(request.getStcBrwId());
        stockBorrow.setStatus(BorrowStatus.FINISH.getCode());
        for (StockBorrowItem item : allBorrowItems) {
            if (item.getReturnCount() < item.getCount()) {
                stockBorrow.setStatus(BorrowStatus.WAITING_RETURN.getCode());
                break;
            }
        }
        stockBorrowRepository.save(stockBorrow);
        //保留归还记录
        stockReturn.setType(StocksConstants.TYPE_RETURN);
        stockReturn.setCpid(CommonUtils.getCpid());
        stockReturn.setRemark(request.getRemark());
        stockReturn.setUid(CommonUtils.getUid());
        stockReturn.setStcBrwId(stockBorrow.getStcBrwId());
        stockReturn.setStcInoutId(ret.getResults().getStcInoutId());
        stockReturn.setOutInCode(ret.getResults().getInnerCode());
        stockReturnRepository.save(stockReturn);
        stockReturnItemRepository.saveAll(returnItems);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<List<PageReturn>> queryStockReturn(String stcBrwId) {
        Sort sort = Sort.by(Sort.Direction.DESC, "ut");
        StockReturn stockReturn = new StockReturn();
        stockReturn.setStcBrwId(stcBrwId);
        Example<StockReturn> example = Example.of(stockReturn);
        List<StockReturn> stockReturns = stockReturnRepository.findAll(example, sort);
        return BaseResponse.success(stockReturns.stream().map(x -> {
            PageReturn data = new PageReturn();
            BeanUtils.copyProperties(x, data);
            data.setOperateTime(DateUtils.formatDateTime(x.getCt()));
            return data;
        }).collect(Collectors.toList()));
    }

    @Override
    public BaseResponse<DetailReturn> detailReturn(String stcRetId) {
        StockReturn stockReturn = stockReturnRepository.findByStcRetId(stcRetId);
        if (Objects.isNull(stockReturn)) {
            return BaseResponse.fail("归还ID错误");
        }
        DetailReturn detailReturn = new DetailReturn();
        BeanUtils.copyProperties(stockReturn, detailReturn);
        List<StockReturnItem> returnItems = stockReturnItemRepository.findByStcRetId(stcRetId);
        detailReturn.setItems(returnItems);
        return BaseResponse.success(detailReturn);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deleteBorrow(List<String> stcBrwIds) throws Exception {
        if (CollectionUtils.isEmpty(stcBrwIds)) {
            return BaseResponse.success();
        }
        stcBrwIds.forEach(this::deleteBorrow);
        return BaseResponse.success();
    }

    /**
     * 获取工单编号
     *
     * @return
     */
    private String getInnerCode() throws Exception {
        int maxLoop = 10000;
        int loop = 1;
        while (loop < maxLoop) {
            String inderCode = "JY".concat(DateUtils.formatMonth(System.currentTimeMillis())).concat(String.format("%04d", loop));
            if (!stockBorrowRepository.existsByInnerCode(inderCode)) {
                return inderCode;
            }
            loop++;
        }
        throw new CustomBizException("本月编号已用完");
    }
}
