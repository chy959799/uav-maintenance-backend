package com.thinglinks.uav.stock.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.stock.dto.store.AddStore;
import com.thinglinks.uav.stock.dto.store.EditStore;
import com.thinglinks.uav.stock.dto.store.PageStore;
import com.thinglinks.uav.stock.dto.store.PageStoreQuery;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StoreService {

    /**
     * 仓库分页查询
     * @param request
     * @return
     */
    BasePageResponse<PageStore> page(PageStoreQuery request);

    /**
     * 新增仓库
     * @param request
     * @return
     */
    BaseResponse<Void> addStores(AddStore request);

    /**
     * 仓库编辑
     * @param request
     * @return
     */
    BaseResponse<Void> editStores(String storesId, EditStore request);

    /**
     * 仓库删除
     * @param storesId
     * @return
     */
    BaseResponse<Void> delStores(String storesId);
}
