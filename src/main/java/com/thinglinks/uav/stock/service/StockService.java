package com.thinglinks.uav.stock.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.stock.dto.*;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.entity.StockInOut;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
public interface StockService {


    /**
     * 入库
     * @param request
     */
    BaseResponse<StockInOut> stocksIn(StockIn request);

    /**
     * 入库
     * @param request
     */
    BaseResponse<StockInOut> stocksOut(StockOut request) throws Exception;

    /**
     * 删除库存
     * @param stocksInOutId
     * @return
     */
    BaseResponse<Void> delStocksInOut(String stocksInOutId) throws Exception;

    /**
     * 删除库存
     * @param stocksIds
     * @return
     */
    BaseResponse<Void> delStocksInOut(List<String> stocksIds) throws Exception;

    /**
     * 库存记录分页查询
     * @param request
     * @return
     */
    BasePageResponse<PageStockInOut> pageStocksInOut(PageStockInOutQuery request);

    /**
     * 库存记录详情
     * @param stocksInOutId
     * @return
     */
    BaseResponse<DetailStockInOut> detailStocksInOut(String stocksInOutId);


    /**
     * 库存分页查询
     * @param request
     * @return
     */
    BasePageResponse<PageStock> pageStocks(PageStockQuery request);


    /**
     * 库存记录
     * @param stocksId
     * @return
     */
    BaseResponse<DetailStock> detailStocks(String stocksId);


    /**
     * 库存详情配件出入库详情
     * @param request
     * @return
     */
    BasePageResponse<PageStocksRecord> stockRecord(StockRecordQuery request);

    /**
     * 设置库存安全值
     * @param request
     * @return
     */
    BaseResponse<Void> setMinLimit(String stocksId, SetMinLimit request);

    /**
     * 库存记录审批
     * @param stocksInOutId
     * @return
     */
    BaseResponse<Void> stocksInReview(String stocksInOutId, StockReviewResult request) throws Exception;

    /**
     * 库存记录审批
     * @param stocksInOutId
     * @return
     */
    BaseResponse<Void> stocksOutReview(String stocksInOutId, StockReviewResult request) throws Exception;

    /**
     * 编辑出入库记录
     * @param stocksInOutId
     * @param request
     * @return
     */
    BaseResponse<Void> editStocksIn(String stocksInOutId, EditStockIn request);


    /**
     * 编辑出入库记录
     * @param stocksInOutId
     * @param request
     * @return
     */
    BaseResponse<Void> editStocksOut(String stocksInOutId, EditStockOut request);

    /**
     * 根据pid查询库存
     * @param pid
     * @return
     */
    Stock getByPid(String pid);
}
