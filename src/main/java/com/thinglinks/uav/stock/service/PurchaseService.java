package com.thinglinks.uav.stock.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.ReviewResult;
import com.thinglinks.uav.stock.dto.DetailStockLog;
import com.thinglinks.uav.stock.dto.purchase.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface PurchaseService {

    /**
     * 新增申购
     *
     * @param request
     * @return
     */
    BaseResponse<Void> addPurchase(AddPurchase request) throws Exception;


    /**
     * 编辑申购
     *
     * @param stcPrchId
     * @param request
     * @return
     */
    BaseResponse<Void> editPurchase(String stcPrchId, AddPurchase request);


    /**
     * 初审
     *
     * @param stcPrchId
     * @param reviewResult
     * @return
     */
    BaseResponse<Void> firsReviewPurchase(String stcPrchId, ReviewResult reviewResult) throws Exception;

    /**
     * 终审
     *
     * @param stcPrchId
     * @param reviewResult
     * @return
     */
    BaseResponse<Void> lastReviewPurchase(String stcPrchId, ReviewResult reviewResult) throws Exception;

    /**
     * 申购分页查询
     *
     * @param request
     * @return
     */
    BasePageResponse<PagePurchase> pagePurchase(PagePurchaseQuery request);

    /**
     * 申购详情
     *
     * @param stcPrchId
     * @return
     */
    BaseResponse<DetailPurchase> detailPurchase(String stcPrchId);

    /**
     * 申购清单分页查询
     *
     * @param request
     * @return
     */
    BasePageResponse<PagePurchaseItem> pagePurchaseItem(PagePurchaseItemQuery request);

    /**
     * 申购日志查询
     *
     * @param stcPrchId
     * @return
     */
    BaseResponse<List<DetailStockLog>> queryPurchaseLogs(String stcPrchId);

    /**
     * 申购单导出
     *
     * @param stcPrchId
     * @return
     */
    void exportPurchaseItem(HttpServletResponse response, String stcPrchId) throws Exception;

    /**
     * 申购编号查询
     *
     * @param code
     * @return
     */
    BaseResponse<List<DetailPurchase>> queryPurchaseCode(String code, String storeId);

    /**
     * 申购删除
     *
     * @param stcPrchId
     * @return
     */
    BaseResponse<Void> deletePurchase(String stcPrchId);

    /**
     * 删除保单
     *
     * @param stcPrchIds
     * @return
     */
    BaseResponse<Void> deletePurchase(List<String> stcPrchIds) throws Exception;


    /**
     * 取消申购
     *
     * @param stcPrchId
     * @return
     */
    BaseResponse<Void> cancelPurchase(String stcPrchId);

}
