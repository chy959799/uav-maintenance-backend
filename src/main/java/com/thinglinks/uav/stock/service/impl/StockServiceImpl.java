package com.thinglinks.uav.stock.service.impl;

import com.thinglinks.uav.common.constants.StocksConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.stock.dto.*;
import com.thinglinks.uav.stock.entity.*;
import com.thinglinks.uav.stock.enums.PurchaseStatus;
import com.thinglinks.uav.stock.enums.StockInType;
import com.thinglinks.uav.stock.repository.*;
import com.thinglinks.uav.stock.service.StockService;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.repository.UserRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Service
public class StockServiceImpl implements StockService {


    @Resource
    private StoreRepository storeRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private ProductRepository productRepository;

    @Resource
    private StockInOutRepository stockInOutRepository;

    @Resource
    private StockInOutItemRepository stockInOutItemRepository;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private StockPurchaseRepository stockPurchaseRepository;

    @Resource
    private StockPurchItemRepository stockPurchItemRepository;

    @Resource
    private CustomRedisTemplate customRedisTemplate;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<StockInOut> stocksIn(StockIn request) {
        StockInOut stockInOut = new StockInOut();
        stockInOut.setStcInoutId(CommonUtils.uuid());
        stockInOut.setInnerCode("KC".concat(DateUtils.formatDate(System.currentTimeMillis())).concat(String.format("%07d", customRedisTemplate.getStocksCountByDay())));
        stockInOut.setType(StocksConstants.TYPE_IN);
        stockInOut.setStatus(StocksConstants.AWAITING);
        stockInOut.setRemark(request.getRemark());
        stockInOut.setCpid(CommonUtils.getCpid());
        stockInOut.setInType(request.getInType());
        if (StockInType.PURCHASE.getCode().equals(request.getInType())) {
            StockPurchase stockPurchase = stockPurchaseRepository.findByInnerCode(request.getPurchaseInnerCode());
            if (Objects.isNull(stockPurchase)) {
                return BaseResponse.fail("申购单编号错误");
            }
            if (PurchaseStatus.FINISH.getCode().equals(stockPurchase.getStatus())) {
                return BaseResponse.fail("申购已完成，无法入库");
            }
            stockInOut.setStcPrchId(stockPurchase.getStcPrchId());
            stockInOut.setStcPrchCode(stockPurchase.getInnerCode());
            for (StockInItem item : request.getItems()) {
                StockPurchItem stockPurchItem = stockPurchItemRepository.findByStcPrchIdAndPid(stockInOut.getStcPrchId(), item.getPid());
                if (Objects.isNull(stockPurchItem)) {
                    return BaseResponse.fail("产品ID错误：".concat(item.getPid()));
                }
                if (item.getCount() > (stockPurchItem.getCount() - stockPurchItem.getStockCount())) {
                    return BaseResponse.fail("产品ID入库数据错误：".concat(item.getPid()));
                }
            }
        }
        stockInOut.setApplyUid(CommonUtils.getUid());
        stockInOut.setStoreid(request.getStoreid());
        Store store = storeRepository.findByStoreid(request.getStoreid());
        stockInOut.setStockName(store.getName());
        request.getItems().forEach(x -> {
            StockInOutItem item = new StockInOutItem();
            item.setStcInOutItemId(CommonUtils.uuid());
            item.setRemark(x.getRemark());
            item.setPid(x.getPid());
            item.setCount(x.getCount());
            Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), store.getStoreid());
            if (Objects.nonNull(stock)) {
                item.setAvailable(stock.getCount());
            }
            item.setPrice(x.getPrice());
            item.setStoreid(request.getStoreid());
            item.setStcInOutId(stockInOut.getStcInoutId());
            item.setBatch(CommonUtils.uuid());
            item.setTotalPrice(x.getPrice() * x.getCount());
            stockInOutItemRepository.save(item);
        });
        stockInOutRepository.save(stockInOut);
        return BaseResponse.success(stockInOut);
    }

    @Override
    public BaseResponse<StockInOut> stocksOut(StockOut request) throws Exception {
        StockInOut stockInOut = new StockInOut();
        if (StocksConstants.STOCK_OUT_ORDER.equals(request.getOutType())) {
            if (StringUtils.isNotBlank(request.getOrdInnerCode())) {
                if (Objects.isNull(request.getOqid())) {
                    return BaseResponse.fail("报价参数错误");
                }
                Order order = orderRepository.findByInnerCode(request.getOrdInnerCode());
                if (Objects.isNull(order)) {
                    return BaseResponse.fail("工单编号错误");
                }
                if (stockInOutRepository.existsByOqidAndStatus(request.getOqid(), StocksConstants.AWAITING) || stockInOutRepository.existsByOqidAndStatus(request.getOqid(), StocksConstants.FINISH)) {
                    return BaseResponse.fail("工单报价已出库，无需重复提交");
                }
                stockInOut.setOrdid(order.getOrdid());
                stockInOut.setOqid(request.getOqid());
                stockInOut.setOrdInnerCode(order.getInnerCode());
            } else {
                return BaseResponse.fail("工单编号参数为必填");
            }
        }
        if (StocksConstants.STOCK_OUT_BORROW.equals(request.getOutType())) {
            if (stockInOutRepository.existsByStcBrwIdAndType(request.getStcBrwId(), StocksConstants.TYPE_OUT)) {
                return BaseResponse.fail("借用已出库，无需重复提交");
            }
            stockInOut.setStcBrwId(request.getStcBrwId());
        }
        stockInOut.setStcInoutId(CommonUtils.uuid());
        stockInOut.setInnerCode("KC".concat(DateUtils.formatDate(System.currentTimeMillis())).concat(String.format("%07d", customRedisTemplate.getStocksCountByDay())));
        stockInOut.setType(StocksConstants.TYPE_OUT);
        stockInOut.setStatus(StocksConstants.AWAITING);
        stockInOut.setRemark(request.getRemark());
        stockInOut.setCpid(CommonUtils.getCpid());
        stockInOut.setOutType(request.getOutType());
        stockInOut.setApplyUid(CommonUtils.getUid());
        Store store;
        if (Objects.nonNull(request.getStoreid())) {
            store = storeRepository.findByStoreid(request.getStoreid());
        } else {
            store = storeRepository.findFirstByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, StocksConstants.STORE_SPARE, CommonUtils.getCpid());
        }
        stockInOut.setStoreid(store.getStoreid());
        stockInOut.setStockName(store.getName());
        List<StockInOutItem> items = new ArrayList<>();
        for (StockOutItem x : request.getItems()) {
            StockInOutItem item = new StockInOutItem();
            Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), store.getStoreid());
            if (Objects.isNull(stock)) {
                Product product = productRepository.findByPid(x.getPid());
                throw new CustomBizException("库存调用失败：".concat(product.getName()));
            }
            //借用自动审批先判断是否库存满足
            if (StocksConstants.STOCK_OUT_BORROW.equals(request.getOutType())) {
                if (x.getCount() > stock.getCount()) {
                    Product product = productRepository.findByPid(x.getPid());
                    throw new CustomBizException("库存调用失败：".concat(product.getName()));
                }
            }
            item.setCount(x.getCount());
            item.setPrice(stock.getRealPrice());
            item.setTotalPrice(stock.getRealPrice() * x.getCount());
            item.setSalesPrice(store.getRatio() * stock.getRealPrice());
            item.setTotalSalesPrice(item.getSalesPrice() * item.getCount());
            item.setStcInOutItemId(CommonUtils.uuid());
            item.setPid(x.getPid());
            item.setStoreid(request.getStoreid());
            item.setStcInOutId(stockInOut.getStcInoutId());
            item.setBatch(CommonUtils.uuid());
            items.add(item);
        }
        stockInOutRepository.save(stockInOut);
        stockInOutItemRepository.saveAll(items);
        return BaseResponse.success(stockInOut);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> delStocksInOut(String stocksInOutId) throws Exception {
        String uid = CommonUtils.getUid();
        StockInOut stockInOut = stockInOutRepository.findByStcInoutId(stocksInOutId);
        //只有本人可以删除且为待审核的
        if (stockInOut.getApplyUid().equals(uid) && StocksConstants.AWAITING.equals(stockInOut.getStatus())) {
            stockInOutRepository.deleteByStcInoutId(stocksInOutId);
            stockInOutItemRepository.deleteByStcInOutId(stocksInOutId);
            return BaseResponse.success();
        }
        throw new CustomBizException("无删除权限");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> delStocksInOut(List<String> stocksIds) throws Exception {
        if (CollectionUtils.isEmpty(stocksIds)) {
            return BaseResponse.success();
        }
        for (String stocksId : stocksIds) {
            this.delStocksInOut(stocksId);
        }
        return BaseResponse.success();
    }

    @Override
    public BasePageResponse<PageStockInOut> pageStocksInOut(PageStockInOutQuery request) {
        PageRequest pageRequest = request.of();
        Specification<StockInOut> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            if (!CommonUtils.isAdmin()) {
                criteriaBuilder.equal(root.get("cpid"), CommonUtils.getCpid());
            }
            if (Objects.nonNull(request.getType())) {
                list.add(criteriaBuilder.equal(root.get("type"), request.getType()));
            }
            if (StringUtils.isNotBlank(request.getStcPurchaseCode())) {
                StockPurchase stockPurchase = stockPurchaseRepository.findByInnerCode(request.getStcPurchaseCode());
                if (Objects.nonNull(stockPurchase)) {
                    list.add(criteriaBuilder.equal(root.get("stcPrchId"), stockPurchase.getStcPrchId()));
                } else {
                    //加入一个错误的条件，防止查询所有的
                    list.add(criteriaBuilder.equal(root.get("stcPrchId"), request.getStcPurchaseCode()));
                }
            }
            if (StringUtils.isNotBlank(request.getOrderCode())) {
                list.add(criteriaBuilder.like(root.get("ordInnerCode"), CommonUtils.assembleLike(request.getOrderCode())));
            }
            if (StringUtils.isNotBlank(request.getInnerCode())) {
                list.add(criteriaBuilder.like(root.get("innerCode"), CommonUtils.assembleLike(request.getInnerCode())));
            }
            if (StringUtils.isNotBlank(request.getInType())) {
                list.add(criteriaBuilder.equal(root.get("inType"), request.getInType()));
            }
            if (StringUtils.isNotBlank(request.getOutType())) {
                list.add(criteriaBuilder.equal(root.get("outType"), request.getOutType()));
            }
            if (StringUtils.isNotBlank(request.getStoreid())) {
                list.add(criteriaBuilder.equal(root.get("storeid"), request.getStoreid()));
            }
            if (Objects.nonNull(request.getStatus())) {
                list.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
            }
            if (Objects.nonNull(request.getStartTime())) {
                list.add(criteriaBuilder.greaterThan(root.get("ct"), request.getStartTime()));
            }
            if (Objects.nonNull(request.getEndTime())) {
                list.add(criteriaBuilder.lessThan(root.get("ct"), request.getEndTime()));
            }
            Predicate[] p = new Predicate[list.size()];
            return criteriaBuilder.and(list.toArray(p));
        };
        Page<StockInOut> page = stockInOutRepository.findAll(specification, pageRequest);
        List<PageStockInOut> results = page.stream().map(x -> {
            PageStockInOut data = new PageStockInOut();
            BeanUtils.copyProperties(x, data);
            data.setStore(storeRepository.findByStoreid(x.getStoreid()));
            data.setUser(userRepository.findByUid(x.getReviewUid()));
            data.setApplyUser(userRepository.findByUid(x.getApplyUid()));
            data.setCreatedAt(DateUtils.formatDateTime(x.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(x.getUt()));
            if (StringUtils.isNotBlank(x.getOrdid())) {
                data.setOrder(orderRepository.findByOrdid(x.getOrdid()));
            }
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<DetailStockInOut> detailStocksInOut(String stocksInOutId) {
        StockInOut stockInOut = stockInOutRepository.findByStcInoutId(stocksInOutId);
        if (Objects.isNull(stockInOut)) {
            return BaseResponse.fail("参数Id错误");
        }
        DetailStockInOut data = new DetailStockInOut();
        BeanUtils.copyProperties(stockInOut, data);
        data.setStore(storeRepository.findByStoreid(stockInOut.getStoreid()));
        data.setUser(userRepository.findByUid(stockInOut.getReviewUid()));
        data.setApplyUser(userRepository.findByUid(stockInOut.getApplyUid()));
        data.setCreatedAt(DateUtils.formatDateTime(stockInOut.getCt()));
        data.setUpdatedAt(DateUtils.formatDateTime(stockInOut.getUt()));
        data.setItems(stockInOutItemRepository.findByStcInOutId(stocksInOutId));
        if (StringUtils.isNotBlank(stockInOut.getOrdid())) {
            data.setOrder(orderRepository.findByOrdid(stockInOut.getOrdid()));
        }
        return BaseResponse.success(data);
    }

    @Override
    public BasePageResponse<PageStock> pageStocks(PageStockQuery request) {
        PageRequest pageRequest = request.of();
        if (StocksConstants.STORE_STANDBY.equals(request.getStoreType())) {
            Sort sort = Sort.by(Sort.Direction.DESC, "borrowCount");
            pageRequest = pageRequest.withSort(sort);
        }
        Specification<Stock> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            if (!CommonUtils.isAdmin()) {
                criteriaBuilder.equal(root.get("cpid"), CommonUtils.getCpid());
            }
            if (Objects.nonNull(request.getType())) {
                list.add(criteriaBuilder.equal(root.get("catid"), request.getType()));
            }
            if (StringUtils.isNotBlank(request.getStoreid())) {
                list.add(criteriaBuilder.equal(root.get("storeid"), request.getStoreid()));
            }
            if (StringUtils.isNotBlank(request.getName())) {
                list.add(criteriaBuilder.like(root.get("pName"), CommonUtils.assembleLike(request.getName())));
            }
            if (StringUtils.isNotBlank(request.getInnerCode())) {
                list.add(criteriaBuilder.like(root.get("pCode"), CommonUtils.assembleLike(request.getInnerCode())));
            }
            if (Objects.nonNull(request.getStoreType())) {
                Join<Stock, Store> join = root.join("store", JoinType.LEFT);
                list.add(criteriaBuilder.equal(join.get("storeType"), request.getStoreType()));
            }
            Predicate[] p = new Predicate[list.size()];
            return criteriaBuilder.and(list.toArray(p));
        };
        Page<Stock> page = stockRepository.findAll(specification, pageRequest);
        List<PageStock> results = page.stream().map(x -> {
            PageStock data = new PageStock();
            BeanUtils.copyProperties(x, data);
            data.setCreatedAt(DateUtils.formatDateTime(x.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(x.getUt()));
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<DetailStock> detailStocks(String stocksId) {
        Stock stock = stockRepository.findByStcid(stocksId);
        if (Objects.isNull(stock)) {
            return BaseResponse.fail("参数Id错误");
        }
        DetailStock data = new DetailStock();
        BeanUtils.copyProperties(stock, data);
        data.setStore(storeRepository.findByStoreid(stock.getStoreid()));
        data.setProduct(productRepository.findByPid(stock.getPid()));
        data.setUser(userRepository.findByUid(stock.getUid()));
        data.setCreatedAt(DateUtils.formatDateTime(stock.getCt()));
        data.setUpdatedAt(DateUtils.formatDateTime(stock.getUt()));
        return BaseResponse.success(data);
    }

    @Override
    public BasePageResponse<PageStocksRecord> stockRecord(StockRecordQuery request) {
        PageRequest pageRequest = request.of();
        Specification<StockInOutItem> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> list = new ArrayList<>();
            if (StringUtils.isNotBlank(request.getPid())) {
                list.add(criteriaBuilder.equal(root.get("pid"), request.getPid()));
            }
            if (Objects.nonNull(request.getStatus())) {
                list.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
            }
            if (StringUtils.isNotBlank(request.getStoreid())) {
                list.add(criteriaBuilder.like(root.get("storeid"), CommonUtils.assembleLike(request.getStoreid())));
            }
            Predicate[] p = new Predicate[list.size()];
            return criteriaBuilder.and(list.toArray(p));
        };
        Page<StockInOutItem> page = stockInOutItemRepository.findAll(specification, pageRequest);
        List<PageStocksRecord> results = page.stream().map(x -> {
            PageStocksRecord data = new PageStocksRecord();
            BeanUtils.copyProperties(x, data);
            data.setStocksInOut(stockInOutRepository.findByStcInoutId(x.getStcInOutId()));
            data.setProduct(productRepository.findByPid(x.getPid()));
            data.setCreatedAt(DateUtils.formatDateTime(x.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(x.getUt()));
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<Void> setMinLimit(String stocksId, SetMinLimit request) {
        Stock stock = stockRepository.findByStcid(stocksId);
        stock.setMinLimit(request.getMinLimit());
        stockRepository.save(stock);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> stocksInReview(String stocksInOutId, StockReviewResult request) throws Exception {
        String uid = CommonUtils.getUid();
        String cpid = CommonUtils.getCpid();
        StockInOut stockInOut = stockInOutRepository.findByStcInoutId(stocksInOutId);
        if (Objects.isNull(stocksInOutId)) {
            return BaseResponse.fail("未找到入库记录");
        }
        Store store = storeRepository.findByStoreid(stockInOut.getStoreid());
        if (Objects.isNull(store)) {
            return BaseResponse.fail("未找到仓库");
        }
        if (StockInType.PURCHASE.getCode().equals(stockInOut.getInType())) {
            StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stockInOut.getStcPrchId());
            if (PurchaseStatus.FINISH.getCode().equals(stockPurchase.getStatus())) {
                return BaseResponse.fail("申购已完成，不需要入库");
            }
        }
        Integer status = request.getStatus();
        List<StockInOutItem> items = stockInOutItemRepository.findByStcInOutId(stocksInOutId);
        for (StockInOutItem x : items) {
            Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), store.getStoreid());
            //审核通过
            if (StocksConstants.FINISH.equals(status)) {
                if (Objects.isNull(stock)) {
                    stock = new Stock();
                    stock.setCount(x.getCount());
                    stock.setStoreid(store.getStoreid());
                    stock.setStockName(store.getName());
                    stock.setMinLimit(StocksConstants.MIN_LIMIT);
                    Product product = productRepository.findByPid(x.getPid());
                    stock.setPid(x.getPid());
                    stock.setPName(product.getName());
                    stock.setPCode(product.getCode());
                    stock.setCatid(product.getCatid());
                    stock.setStcid(CommonUtils.uuid());
                    stock.setTotalIn(x.getCount());
                    stock.setUid(uid);
                    stock.setRealTotalPrice(x.getTotalPrice());
                    stock.setRealPrice(CommonUtils.format2Point(stock.getRealTotalPrice() / stock.getCount()));
                } else {
                    stock.setCount(stock.getCount() + x.getCount());
                    stock.setTotalIn(stock.getTotalIn() + x.getCount());
                    stock.setRealTotalPrice(stock.getRealTotalPrice() + x.getTotalPrice());
                    stock.setRealPrice(CommonUtils.format2Point(stock.getRealTotalPrice() / stock.getCount()));
                }
                //申购入库
                if (StockInType.PURCHASE.getCode().equals(stockInOut.getInType())) {
                    StockPurchItem stockPurchItem = stockPurchItemRepository.findByStcPrchIdAndPid(stockInOut.getStcPrchId(), x.getPid());
                    x.setStcPrchItemId(stockPurchItem.getStcPrchItemId());
                    if (x.getCount() > (stockPurchItem.getCount() - Optional.ofNullable(stockPurchItem.getStockCount()).orElse(0))) {
                        throw new CustomBizException("入库数量大于申购数量：".concat(x.getPid()));
                    }
                    stockPurchItem.setStockCount(Optional.ofNullable(stockPurchItem.getStockCount()).orElse(0) + x.getCount());
                    stockPurchItemRepository.save(stockPurchItem);
                }
                stockRepository.save(stock);
                x.setAvailable(stock.getCount());
            }
            x.setStatus(status);
        }
        //申购入库 更新状态
        if (StockInType.PURCHASE.getCode().equals(stockInOut.getInType())) {
            if (StocksConstants.FINISH.equals(status)) {
                List<StockPurchItem> stockPurchItems = stockPurchItemRepository.findByStcPrchId(stockInOut.getStcPrchId());
                boolean isFinish = true;
                for (StockPurchItem stockPurchItem : stockPurchItems) {
                    if (stockPurchItem.getStockCount() > 0 && (stockPurchItem.getStockCount() < stockPurchItem.getCount())) {
                        StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stockInOut.getStcPrchId());
                        stockPurchase.setStatus(PurchaseStatus.PART_PURCHASE.getCode());
                        stockPurchaseRepository.save(stockPurchase);
                        isFinish = false;
                        break;
                    }
                }
                if (isFinish) {
                    StockPurchase stockPurchase = stockPurchaseRepository.findByStcPrchId(stockInOut.getStcPrchId());
                    stockPurchase.setStatus(PurchaseStatus.FINISH.getCode());
                    stockPurchase.setCompleteAt(System.currentTimeMillis());
                    stockPurchaseRepository.save(stockPurchase);
                }
            }
        }
        stockInOutItemRepository.saveAll(items);
        stockInOut.setReviewUid(uid);
        stockInOut.setCpid(cpid);
        stockInOut.setStatus(status);
        stockInOut.setReviewResult(request.getReviewResult());
        stockInOut.setReviewRemark(request.getReviewRemark());
        stockInOutRepository.save(stockInOut);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> stocksOutReview(String stocksInOutId, StockReviewResult request) throws Exception {
        String uid = CommonUtils.getUid();
        String cpid = CommonUtils.getCpid();
        StockInOut stockInOut = stockInOutRepository.findByStcInoutId(stocksInOutId);
        if (Objects.isNull(stocksInOutId)) {
            return BaseResponse.fail("未找到入库记录");
        }
        Store store = storeRepository.findByStoreid(stockInOut.getStoreid());
        if (Objects.isNull(store)) {
            return BaseResponse.fail("未找到仓库");
        }
        List<StockInOutItem> items = stockInOutItemRepository.findByStcInOutId(stocksInOutId);
        Integer status = request.getStatus();
        for (StockInOutItem x : items) {
            //审核通过
            if (StocksConstants.FINISH.equals(status)) {
                Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), store.getStoreid());
                if (x.getCount() > stock.getCount()) {
                    throw new CustomBizException("库存调用失败");
                }
                stock.setCount(stock.getCount() - x.getCount());
                x.setPrice(stock.getRealPrice());
                x.setTotalPrice(x.getPrice() * x.getCount());
                x.setSalesPrice(CommonUtils.format2Point(store.getRatio() * stock.getRealPrice()));
                x.setTotalSalesPrice(x.getSalesPrice() * x.getCount());
                x.setAvailable(stock.getCount());
                stock.setRealTotalPrice(stock.getRealTotalPrice() - x.getTotalPrice());
                stockRepository.save(stock);
            }
            x.setStatus(status);
        }
        stockInOutItemRepository.saveAll(items);
        stockInOut.setReviewUid(uid);
        stockInOut.setCpid(cpid);
        stockInOut.setStatus(status);
        stockInOut.setReviewResult(request.getReviewResult());
        stockInOut.setReviewRemark(request.getReviewRemark());
        stockInOutRepository.save(stockInOut);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> editStocksIn(String stocksInOutId, EditStockIn request) {
        StockInOut stockInOut = stockInOutRepository.findByStcInoutId(stocksInOutId);
        if (Objects.isNull(stockInOut)) {
            return BaseResponse.fail("库存记录Id错误");
        }
        if (StringUtils.isNotBlank(request.getRemark())) {
            stockInOut.setRemark(request.getRemark());
        }
        if (StringUtils.isNotBlank(request.getStoreid())) {
            stockInOut.setStoreid(request.getStoreid());
            Store store = storeRepository.findByStoreid(request.getStoreid());
            stockInOut.setStockName(store.getName());
        }
        if (CollectionUtils.isNotEmpty(request.getItems())) {
            stockInOutItemRepository.deleteByStcInOutId(stocksInOutId);
            List<StockInOutItem> items = new ArrayList<>();
            request.getItems().forEach(x -> {
                StockInOutItem item = new StockInOutItem();
                item.setStcInOutItemId(CommonUtils.uuid());
                item.setRemark(x.getRemark());
                item.setPid(x.getPid());
                item.setCount(x.getCount());
                Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), stockInOut.getStoreid());
                if (Objects.nonNull(stock)) {
                    item.setAvailable(stock.getCount());
                }
                item.setPrice(x.getPrice());
                item.setStoreid(request.getStoreid());
                item.setStcInOutId(stockInOut.getStcInoutId());
                item.setBatch(CommonUtils.uuid());
                item.setTotalPrice(x.getPrice() * x.getCount());
                items.add(item);
            });
            stockInOutItemRepository.saveAll(items);
        }
        stockInOutRepository.save(stockInOut);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> editStocksOut(String stocksInOutId, EditStockOut request) {
        StockInOut stockInOut = stockInOutRepository.findByStcInoutId(stocksInOutId);
        if (Objects.isNull(stockInOut)) {
            return BaseResponse.fail("库存记录Id错误");
        }
        if (StringUtils.isNotBlank(request.getRemark())) {
            stockInOut.setRemark(request.getRemark());
        }
        Store store = storeRepository.findByStoreid(stockInOut.getStoreid());
        if (StringUtils.isNotBlank(request.getStoreid())) {
            stockInOut.setStoreid(request.getStoreid());
            store = storeRepository.findByStoreid(request.getStoreid());
            stockInOut.setStockName(store.getName());
        }
        if (CollectionUtils.isNotEmpty(request.getItems())) {
            stockInOutItemRepository.deleteByStcInOutId(stocksInOutId);
            List<StockInOutItem> items = new ArrayList<>();
            Store finalStore = store;
            request.getItems().forEach(x -> {
                StockInOutItem item = new StockInOutItem();
                Stock stock = stockRepository.findByPidAndStoreid(x.getPid(), stockInOut.getStoreid());
                item.setCount(x.getCount());
                item.setPrice(stock.getRealPrice());
                item.setTotalPrice(stock.getRealPrice() * x.getCount());
                item.setSalesPrice(CommonUtils.format2Point(finalStore.getRatio() * stock.getRealPrice()));
                item.setTotalSalesPrice(item.getSalesPrice() * item.getCount());
                item.setStcInOutItemId(CommonUtils.uuid());
                item.setPid(x.getPid());
                item.setStoreid(request.getStoreid());
                item.setStcInOutId(stockInOut.getStcInoutId());
                item.setBatch(CommonUtils.uuid());
                items.add(item);
            });
            stockInOutItemRepository.saveAll(items);
        }
        stockInOutRepository.save(stockInOut);
        return BaseResponse.success();
    }

    @Override
    public Stock getByPid(String pid) {
        Store firstByDefaultStores = storeRepository.findFirstByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, StocksConstants.STORE_SPARE, CommonUtils.getCpid());
        return stockRepository.findByPidAndStoreid(pid, firstByDefaultStores.getStoreid());
    }
}
