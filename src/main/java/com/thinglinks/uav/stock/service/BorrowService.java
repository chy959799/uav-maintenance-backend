package com.thinglinks.uav.stock.service;

import com.thinglinks.uav.common.dto.*;
import com.thinglinks.uav.stock.dto.borrow.*;

import java.util.List;

public interface BorrowService {

    /**
     * 新增借用
     *
     * @param request
     * @return
     */
    BaseResponse<Void> addBorrow(AddBorrow request) throws Exception;

    /**
     * 初审
     *
     * @param stcBrwId
     * @param request
     * @return
     */
    BaseResponse<Void> reviewBorrow(String stcBrwId, ReviewResult request) throws Exception;

    /**
     * 申购删除
     *
     * @param stcBrwId
     * @return
     */
    BaseResponse<Void> deleteBorrow(String stcBrwId);


    /**
     * 取消申购
     *
     * @param stcBrwId
     * @return
     */
    BaseResponse<Void> cancelBorrow(String stcBrwId);

    /**
     * 借用分页查询
     *
     * @param request
     * @return
     */
    BasePageResponse<PageBorrow> pageBorrow(PageBorrowQuery request);

    /**
     * 借用详情
     * @param stcBrwId
     * @return
     */
    BaseResponse<DetailBorrow> detailBorrow(String stcBrwId);

    /**
     * 申购清单分页查询
     *
     * @param request
     * @return
     */
    BasePageResponse<PageBorrowItem> pageBorrowItem(PageBorrowItemQuery request);

    /**
     *  物品领取
     * @return
     */
    BaseResponse<Void> getBorrow(String stcBrwId, Remark remark) throws Exception;

    /**
     * 归还物品
     * @param request
     * @return
     */
    BaseResponse<Void> stockReturn(AddReturn request) throws Exception;

    /**
     * 物品领取记录拆线呢
     *
     * @param stcBrwId
     * @return
     */
    BaseResponse<List<PageReturn>> queryStockReturn(String stcBrwId);


    /**
     * 归还详细查询
     * @param stcRetId
     * @return
     */
    BaseResponse<DetailReturn> detailReturn(String stcRetId);

    /**
     * 删除借用
     *
     * @param stcBrwIds
     * @return
     */
    BaseResponse<Void> deleteBorrow(List<String> stcBrwIds) throws Exception;

}
