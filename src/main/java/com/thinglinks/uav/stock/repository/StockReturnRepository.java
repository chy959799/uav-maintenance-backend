package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockReturn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockReturnRepository extends JpaRepository<StockReturn, Long>, JpaSpecificationExecutor<StockReturn> {


    StockReturn findByStcRetId(String stcRetId);
}
