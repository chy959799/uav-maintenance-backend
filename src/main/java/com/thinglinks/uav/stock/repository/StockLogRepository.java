package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockLogRepository extends JpaRepository<StockLog, Long>, JpaSpecificationExecutor<StockLog> {


    List<StockLog> findByStcPrchId(String stcPrchId);

}
