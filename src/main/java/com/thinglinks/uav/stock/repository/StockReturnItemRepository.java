package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockReturnItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockReturnItemRepository extends JpaRepository<StockReturnItem, Long>, JpaSpecificationExecutor<StockReturnItem> {


    List<StockReturnItem> findByStcRetId(String stcRetId);
}
