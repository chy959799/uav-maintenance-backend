package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockBorrow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockBorrowRepository extends JpaRepository<StockBorrow, Long>, JpaSpecificationExecutor<StockBorrow> {

    StockBorrow findByStcBrwId(String StcBrwId);


    boolean existsByInnerCode(String code);

    void deleteByStcBrwId(String StcBrwId);

    StockBorrow findByInnerCode(String code);

}
