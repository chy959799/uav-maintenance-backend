package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockPurchItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockPurchItemRepository extends JpaRepository<StockPurchItem, Long>, JpaSpecificationExecutor<StockPurchItem> {


    List<StockPurchItem> findByStcPrchId(String stcPrchId);

    void deleteByStcPrchId(String stcPrchId);


    StockPurchItem findByStcPrchIdAndPid(String stcPrchId, String pid);
}
