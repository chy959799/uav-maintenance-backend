package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockBorrowItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockBorrowItemRepository extends JpaRepository<StockBorrowItem, Long>, JpaSpecificationExecutor<StockBorrowItem> {
    void deleteByStcBrwId(String StcBrwId);

    List<StockBorrowItem> findByStcBrwId(String StcBrwId);

    StockBorrowItem findByStcBrwIdAndPid(String StcBrwId, String pid);

    List<StockBorrowItem> findByStoreidAndPid(String storeId, String pid);
}
