package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockPurchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockPurchaseRepository extends JpaRepository<StockPurchase, Long>, JpaSpecificationExecutor<StockPurchase> {

    Boolean existsByInnerCode(String code);

    StockPurchase findByStcPrchId(String stcPrchId);

    Boolean existsByStoreId(String storeId);

    void  deleteByStcPrchId(String stcPrchId);

    StockPurchase findByInnerCode(String code);
}
