package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockRepository extends JpaRepository<Stock, Long>, JpaSpecificationExecutor<Stock> {

    Stock findByPidAndStoreid(String pid, String storeId);

    Stock findByStcid(String stocksId);

    boolean existsByPidIn(List<String> pidList);

    List<Stock> findByStoreid(String storeId);

    @Query("SELECT s FROM Stock s WHERE s.count < s.minLimit")
    List<Stock> findStocksWithLowCount();

    void deleteByStcid(String stcId);
}
