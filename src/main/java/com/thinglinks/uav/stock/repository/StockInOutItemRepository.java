package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockInOutItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockInOutItemRepository extends JpaRepository<StockInOutItem, Long>, JpaSpecificationExecutor<StockInOutItem> {

    List<StockInOutItem> findByStcInOutId(String stcInOutId);

    void deleteByStcInOutId(String stcInOutId);

    StockInOutItem findByStcInOutIdAndPid(String stcInOutId, String pid);
}
