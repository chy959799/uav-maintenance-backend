package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.StockInOut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StockInOutRepository extends JpaRepository<StockInOut, Long>, JpaSpecificationExecutor<StockInOut> {
    StockInOut findByStcInoutId(String stcInOutId);

    StockInOut findByOrdid(String orderId);

    @Transactional
    void deleteByStcInoutId(String stcInOutId);

    List<StockInOut> findByOrdidAndTypeAndOutTypeOrderByCt(String orderId, Integer type, String outType);

    StockInOut findByOqid(String oqid);

    StockInOut findByOqidAndStatus(String oqid, Integer status);

    Boolean existsByOqidAndStatus(String oqid, Integer status);

    Boolean existsByStcBrwIdAndType(String StcBrwId, Integer type);

    StockInOut findByStcBrwIdAndType(String StcBrwId, Integer type);
}
