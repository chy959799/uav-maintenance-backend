package com.thinglinks.uav.stock.repository;

import com.thinglinks.uav.stock.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */
public interface StoreRepository extends JpaRepository<Store, Long>, JpaSpecificationExecutor<Store> {

    Store findByStoreid(String storesId);

    @Transactional
    void deleteByStoreid(String storesId);

    Store findFirstByDefaultStoresAndStoreTypeAndCpid(Boolean defaultStores, Integer type, String cpid);

    List<Store> findByDefaultStoresAndStoreTypeAndCpid(Boolean defaultStores, Integer type, String cpid);

    boolean existsByDefaultStoresAndStoreTypeAndCpid(Boolean defaultStores, Integer type, String cpid);
}
