package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/11
 */
@Entity
@Table(name = "stock_return")
@Data
public class StockReturn extends BaseEntity {

    @Column(name = "stc_ret_id")
    @ApiModelProperty(value = "内部ID")
    private String stcRetId;

    @Column(name = "type")
    @ApiModelProperty(value = "1:物品领取；2:物品归还")
    private Integer type;
    
    @Column(name = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @Column(name = "out_in_code")
    @ApiModelProperty(value = "出入库编号")
    private String outInCode;

    @Column(name = "stc_brw_id")
    @ApiModelProperty(value = "借用内部ID")
    private String stcBrwId;

    @Column(name = "uid")
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;

    @Column(name = "stc_inout_id")
    @ApiModelProperty(value = "出入库记录内部ID")
    private String stcInoutId;

    @Column(name = "cpid")
    @ApiModelProperty(value = "单位ID")
    private String cpid;
}
