package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/11
 */
@Entity
@Table(name = "stock_return_items")
@Data
public class StockReturnItem extends BaseEntity {

    @Column(name = "stc_ret_item_id")
    @ApiModelProperty(value = "内部ID")
    private String stcRetItemId;

    @Column(name = "count")
    @ApiModelProperty(value = "数量")
    private Integer count;
    
    @Column(name = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @Column(name = "stc_ret_id")
    @ApiModelProperty(value = "归还记录内部ID")
    private String stcRetId;

    @Column(name = "pid")
    @ApiModelProperty(value = "产品ID")
    private String pid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
    private Product product;
}
