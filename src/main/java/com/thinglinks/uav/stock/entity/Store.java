package com.thinglinks.uav.stock.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/17
 */

@Entity
@Table(name = "stores")
@EntityListeners(AuditingEntityListener.class)
@Data
@TenantTable
public class Store extends BaseEntity {

    @Column(name = "storeid")
    @ApiModelProperty(value = "库存内部ID")
    private String storeid;

    @Column(name = "type")
    @ApiModelProperty(value = "库存类型, 1:自有仓库 2:他人仓库")
    private Integer type;

    @Column(name = "code")
    @ApiModelProperty(value = "仓库编号")
    private String code;

    @Column(name = "name")
    @ApiModelProperty(value = "仓库名称")
    private String name;

    @Column(name = "allow_empty")
    @ApiModelProperty(value = "是否允许空库存,若为否则工单定损时不出现空库存备件")
    private Boolean allowEmpty;

    @Column(name = "ratio")
    @ApiModelProperty(value = "零售价格倍率")
    private Float ratio;

    @Column(name = "min_limit")
    @ApiModelProperty(value = "告警下限")
    private Integer minLimit;

    @Column(name = "max_limit")
    @ApiModelProperty(value = "告警上限")
    private Integer maxLimit;

    @Column(name = "default_1")
    @JsonProperty(value = "default")
    @ApiModelProperty(value = "是否为默认仓库")
    private Boolean defaultStores;

    @Column(name = "remark")
    @ApiModelProperty(value = "库存备注")
    private String remark;

    @Column(name = "uid")
    private String uid;

    @Column(name = "cpid")
    private String cpid;

    @Column(name = "store_type")
    @ApiModelProperty(value = "仓库类型，1：备件仓库；2：备品仓库")
    private Integer storeType;

}

