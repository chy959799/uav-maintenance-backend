package com.thinglinks.uav.stock.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Entity
@Table(name = "stock_logs")
@Data
public class StockLog extends BaseEntity {

    @Column(name = "stc_log_id")
    @ApiModelProperty(value = "工单日志内部ID")
    private String stcLogId;

    @Column(name = "status")
    @JsonProperty(value = "status")
    @ApiModelProperty(value = "数据状态")
    private String status;

    @Column(name = "method")
    @ApiModelProperty(value = "操作类型 1001:申购初审 ；1002：申购终审；1003：申购取消;2001：借用审核；2001：物品领取；2003：物品归还")
    private String method;

    @Column(name = "approve")
    @ApiModelProperty(value = "审批结果")
    private String approve;

    @Column(name = "remark")
    @ApiModelProperty(value = "操作备注(同时作为数据缓存)")
    private String remark;

    @Column(name = "description")
    @ApiModelProperty(value = "操作记录")
    private String description;

    @Column(name = "stcid")
    private String stcid;

    @Column(name = "stc_prch_id")
    private String stcPrchId;

    @Column(name = "stc_brw_id")
    private String stcBrwId;

    @Column(name = "uid")
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;
}
