package com.thinglinks.uav.stock.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/18
 */

@Entity
@Table(name = "stocks")
@Data
public class Stock extends BaseEntity {

    //库存id
    @Column(name = "stcid")
    @ApiModelProperty(value = "库存ID")
    private String stcid;

    @Column(name = "count")
    @ApiModelProperty(value = "产品库存数量")
    private Integer count;

    @Column(name = "total_in")
    @ApiModelProperty(value = "累计入库数量")
    private Integer totalIn;

    @Column(name = "total_out")
    @ApiModelProperty(value = "累计出库数量")
    private Integer totalOut;

    @Column(name = "max_limit")
    @ApiModelProperty(value = "库存告警上限")
    private Integer maxLimit;

    @Column(name = "min_limit")
    @ApiModelProperty(value = "库存告警下限")
    private Integer minLimit;

    @Column(name = "real_price")
    @ApiModelProperty(value = "移动加权平均单价")
    private Double realPrice;

    @Column(name = "real_total_price")
    @ApiModelProperty(value = "产品累计购买总价")
    private Double realTotalPrice;

    @Column(name = "usage_ratio")
    @ApiModelProperty(value = "库存累计使用率")
    private Double usageRatio;

    @Column(name = "storeid")
    @ApiModelProperty(value = "所属仓库Id")
    private String storeid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeid", referencedColumnName = "storeid", insertable = false, updatable = false)
    private Store store;

    @Column(name = "stock_name")
    @ApiModelProperty(value = "所属仓库名称")
    private String stockName;

    @Column(name = "description")
    @ApiModelProperty(value = "产品库存描述")
    private String description;

    @Column(name = "cpid")
    @ApiModelProperty(value = "单位ID")
    private String cpid;

    @Column(name = "uid")
    private String uid;

    @Column(name = "pid")
    @JsonProperty(value = "产品Id")
    private String pid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
    private Product product;

    @ApiModelProperty("产品名称存")
    @Column(name="p_name")
    private String pName;

    @ApiModelProperty("产品编码")
    @Column(name="p_code")
    private String pCode;

    @ApiModelProperty("产品内部编码")
    @Column(name="p_inner_code")
    private String pInnerCode;

    @ApiModelProperty("分类Id")
    @Column(name="catid")
    private String catid;

    @ApiModelProperty("借用数量")
    @Column(name="brw_count")
    private Long borrowCount;
}
