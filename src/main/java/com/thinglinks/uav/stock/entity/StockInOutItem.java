package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Entity
@Table(name = "stock_inout_items")
@Data
public class StockInOutItem extends BaseEntity {

    @Column(name = "stc_inout_item_id")
    @ApiModelProperty(value = "出入库产品记录内部ID")
    private String stcInOutItemId;

    @Column(name = "batch")
    @ApiModelProperty(value = "入库批次")
    private String batch;

    @Column(name = "count")
    @ApiModelProperty(value = "出入库数量")
    private Integer count;

    @Column(name = "price")
    @ApiModelProperty(value = "出入库单价，入库为实际单价，出库为当前平均加权单价")
    private Double price;

    @Column(name = "sales_price")
    @ApiModelProperty(value = "销售价")
    private Double salesPrice;

    @Column(name = "available")
    @ApiModelProperty(value = "当前剩余可用数量")
    private Integer available;

    @Column(name = "total_price")
    @ApiModelProperty(value = "出入库总价")
    private Double totalPrice;

    @Column(name = "total_sales_price")
    @ApiModelProperty(value = "销售总价")
    private Double totalSalesPrice;

    //出入库状态 1: 待审核, 2: 已完成 3: 已驳回
    @Column(name = "status")
    @ApiModelProperty(value = "出入库状态 1: 待审核, 2: 已完成 3: 已驳回")
    private Integer status;

    @Column(name = "remark")
    @ApiModelProperty(value = "出入库备注")
    private String remark;

    @Column(name = "pid")
    private String pid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
    private Product product;

    @Column(name = "storeid")
    private String storeid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storeid", referencedColumnName = "storeid", insertable = false, updatable = false)
    private Store store;

    @Column(name = "stc_prch_item_id")
    private String stcPrchItemId;

    @Column(name = "stc_inout_id")
    private String stcInOutId;

}
