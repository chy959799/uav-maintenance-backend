package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/11
 */
@Entity
@Table(name = "stock_purch_items")
@Data
public class StockPurchItem extends BaseEntity {

    @Column(name = "stc_prch_item_id")
    @ApiModelProperty(value = "申购内部ID")
    private String stcPrchItemId;

    @Column(name = "count")
    @ApiModelProperty(value = "申购数量")
    private Integer count;

    @Column(name = "stock_count")
    @ApiModelProperty(value = "入库数量")
    private Integer stockCount;

    @Column(name = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @Column(name = "stc_prch_id")
    @ApiModelProperty(value = "申购内部ID")
    private String stcPrchId;

    @Column(name = "pid")
    @ApiModelProperty(value = "产品ID")
    private String pid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
    private Product product;
}
