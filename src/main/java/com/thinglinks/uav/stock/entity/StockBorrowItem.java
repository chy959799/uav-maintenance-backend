package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/11
 */
@Entity
@Table(name = "stock_borrow_items")
@Data
public class StockBorrowItem extends BaseEntity {

    @Column(name = "stc_brw_item_id")
    @ApiModelProperty(value = "内部ID")
    private String stcBrwItemId;

    @Column(name = "count")
    @ApiModelProperty(value = "数量")
    private Integer count;

    @Column(name = "ret_count")
    @ApiModelProperty(value = "归还数量")
    private Integer returnCount;

    @Column(name = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @Column(name = "stc_brw_id")
    @ApiModelProperty(value = "借用内部ID")
    private String stcBrwId;

    @Column(name = "pid")
    @ApiModelProperty(value = "产品ID")
    private String pid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
    private Product product;

    @Column(name = "storeid")
    @ApiModelProperty(value = "所属仓库Id")
    private String storeid;

}
