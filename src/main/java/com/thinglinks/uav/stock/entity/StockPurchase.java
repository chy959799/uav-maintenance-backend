package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/11
 */
@Entity
@Table(name = "stock_purches")
@Data
public class StockPurchase extends BaseEntity {

    @Column(name = "stc_prch_id")
    @ApiModelProperty(value = "申购内部ID")
    private String stcPrchId;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "内部编号")
    private String innerCode;

    @Column(name = "status")
    @ApiModelProperty(value = "申购单状态 100：待初审；200：待终审；300：待采购；400：部分采购；500：已完成；600：已驳回；700：已取消 ")
    private String status;

    @Column(name = "purch_type")
    @ApiModelProperty(value = "申购类型, 1:备件申购；2：备品申购")
    private Integer purchaseType;

    @Column(name = "delivery_at")
    @ApiModelProperty(value = "需求日期")
    private Long deliveryAt;

    @Column(name = "remark")
    @ApiModelProperty(value = "申购单备注")
    private String remark;

    @Column(name = "complete_at")
    @ApiModelProperty(value = "申购单实际完成时间")
    private Long completeAt;

    @Column(name = "apply_at")
    @ApiModelProperty(value = "申请时间")
    private Long applyAt;

    @Column(name = "review_at")
    @ApiModelProperty(value = "审批时间")
    private Long reviewAt;

    @Column(name = "storeid")
    @ApiModelProperty(value = "仓库id")
    private String storeId;

    @Column(name = "uid")
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;

    @Column(name = "cpid")
    private String cpid;

    @Column(name = "did")
    private String did;

    @Column(name = "review_last_remark")
    @ApiModelProperty(value = "终审备注")
    private String reviewLastRemark;

    @Column(name = "review_first_remark")
    @ApiModelProperty(value = "初审备注")
    private String reviewFirstRemark;
}
