package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "stock_borrow")
@Data
public class StockBorrow extends BaseEntity {

    @Column(name = "stc_brw_id")
    @ApiModelProperty(value = "内部ID")
    private String stcBrwId;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "借用内部编号")
    private String innerCode;

    @Column(name = "type")
    @ApiModelProperty(value = "借用类型：1：内部借用；2：外部借用")
    private Integer type;

    @Column(name = "plan_return_at")
    @ApiModelProperty(value = "预计归还时间")
    private Long planReturnAt;

    @Column(name = "status")
    @ApiModelProperty(value = "借用状态：100：待审核；200：待领取；300：带归还；400：已完成；500：已驳回；600：已取消")
    private String status;

    @Column(name = "store_id")
    @ApiModelProperty(value = "仓库内部ID")
    private String storeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", referencedColumnName = "storeid", insertable = false, updatable = false)
    private Store store;

    @Column(name = "usage_1")
    @ApiModelProperty(value = "物品用途")
    private String usage;

    @Column(name = "apply_at")
    @ApiModelProperty(value = "申请时间")
    private Long applyAt;

    @Column(name = "review_at")
    @ApiModelProperty(value = "审批时间")
    private Long reviewAt;

    @Column(name = "return_at")
    @ApiModelProperty(value = "归还时间")
    private Long returnAt;

    @Column(name = "apply_uid")
    @ApiModelProperty(value = "申请人id")
    private String applyUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "apply_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User applyUser;

    @Column(name = "did")
    private String did;

    @Column(name = "stc_inout_id")
    @ApiModelProperty(value = "出入库记录内部ID")
    private String stcInoutId;

    @Column(name = "review_result")
    @ApiModelProperty(value = "申购审核状态,passed：通过；rejected：驳回")
    private String reviewResult;

    @Column(name = "review_remark")
    @ApiModelProperty(value = "审批备注")
    private String reviewRemark;

    @Column(name = "review_uid")
    @ApiModelProperty(value = "审批人id")
    private String reviewUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "review_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User reviewUser;

    @Column(name = "cpid")
    @ApiModelProperty(value = "单位ID")
    private String cpid;
}
