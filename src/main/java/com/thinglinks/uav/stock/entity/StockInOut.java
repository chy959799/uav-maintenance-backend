package com.thinglinks.uav.stock.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Entity
@Table(name = "stock_inouts")
@Data
public class StockInOut extends BaseEntity {

    @Column(name = "stc_inout_id")
    @ApiModelProperty(value = "出入库记录内部ID")
    private String stcInoutId;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "出入库内部编号")
    private String innerCode;

    //库存类型 1:入库 2:出库
    @Column(name = "type")
    @ApiModelProperty(value = "日志操作类型 1:入库 2:出库")
    private Integer type;

    //出入库状态 1: 待审核, 2: 已完成 3: 已驳回
    @Column(name = "status")
    @ApiModelProperty(value = "出入库状态 1: 待审核, 2: 已完成 3: 已驳回")
    private Integer status;

    @Column(name = "review_result")
    @ApiModelProperty(value = "申购审核状态,passed：通过；rejected：驳回")
    private String reviewResult;

    @Column(name = "review_remark")
    @ApiModelProperty(value = "申购审核备注")
    private String reviewRemark;

    //入库类型, 1: 申购单入库 2: 直接入库 3: 销售退回
    @Column(name = "in_type")
    @ApiModelProperty(value = "入库类型, 1: 申购单入库 2: 直接入库 3: 销售退回")
    private String inType;

    //入库类型, 1: 申购单入库 2: 直接入库 3: 销售退回
    @Column(name = "out_type")
    @ApiModelProperty(value = "出库类型, 1: 工单出库 2: 直接出库 3: 销售出库 4: 破损出库")
    private String outType;

    @Column(name = "stock_name")
    @ApiModelProperty(value = "仓库名称")
    private String stockName;

    @Column(name = "remark")
    @ApiModelProperty(value = "出入库备注")
    private String remark;

    @Column(name = "cpid")
    private String cpid;

    @Column(name = "storeid")
    @ApiModelProperty(value = "仓库id")
    private String storeid;

    @Column(name = "review_uid")
    @ApiModelProperty(value = "审批人id")
    private String reviewUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "review_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User reviewUser;

    @Column(name = "apply_uid")
    @ApiModelProperty(value = "申请人id")
    private String applyUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "apply_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User applyUser;

    @Column(name = "ordid")
    @ApiModelProperty(value = "订单id")
    private String ordid;

    @Column(name = "ord_inner_code")
    @ApiModelProperty(value = "订单编号")
    private String ordInnerCode;

    @Column(name = "oqid")
    @ApiModelProperty(value = "订单的报价id")
    private String oqid;

    @Column(name = "stc_prch_id")
    @ApiModelProperty(value = "申购记录id")
    private String stcPrchId;

    @Column(name = "stc_prch_code")
    @ApiModelProperty(value = "申购编码")
    private String stcPrchCode;

    @Column(name = "stc_brw_id")
    @ApiModelProperty(value = "借用内部ID")
    private String stcBrwId;

}
