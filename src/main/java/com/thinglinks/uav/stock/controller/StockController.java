package com.thinglinks.uav.stock.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.IdsRequest;
import com.thinglinks.uav.stock.dto.*;
import com.thinglinks.uav.stock.entity.StockInOut;
import com.thinglinks.uav.stock.service.StockService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Api(tags = "库存管理")
@RestController
@RequestMapping(value = "stocks")
@Slf4j
public class StockController {

    @Resource
    private StockService stockService;

    @Authority(action = "stockInAdd")
    @Operation(summary = "配件入库")
    @PostMapping(path = "in")
    public BaseResponse<StockInOut> stocksIn(@Valid @RequestBody StockIn request) {
        return stockService.stocksIn(request);
    }

    @Authority(action = "stockOutAdd")
    @Operation(summary = "配件出库")
    @PostMapping(path = "out")
    public BaseResponse<StockInOut> stocksOut(@Valid @RequestBody StockOut request) throws Exception{
        return stockService.stocksOut(request);
    }

    @Authority(action = "stockInoutDelete")
    @Operation(summary = "删除出入库记录")
    @DeleteMapping(path = "inOuts/{stocksInOutId}")
    public BaseResponse<Void> delStocks(@PathVariable("stocksInOutId") String stocksInOutId) throws Exception{
        return stockService.delStocksInOut(stocksInOutId);
    }

    @Authority(action = "stockInoutDelete")
    @Operation(summary = "批量删除出入库记录")
    @DeleteMapping(path = "inOuts")
    public BaseResponse<Void> delStocksBatch(@Valid @RequestBody IdsRequest request) throws Exception{
        return stockService.delStocksInOut(request.getIds());
    }

    @Operation(summary = "出入库记录分页查询")
    @GetMapping(path = "inOuts/page")
    public BasePageResponse<PageStockInOut> pageStocksInOut(PageStockInOutQuery request) {
        return stockService.pageStocksInOut(request);
    }

    @Operation(summary = "出入库记录详情")
    @GetMapping(path = "inOuts/{stocksInOutId}")
    public BaseResponse<DetailStockInOut> detailStocksInOut(@PathVariable("stocksInOutId") String stocksInOutId) {
        return stockService.detailStocksInOut(stocksInOutId);
    }

    @Operation(summary = "库存分页查询")
    @GetMapping(path = "page")
    public BasePageResponse<PageStock> pageStocks(PageStockQuery request) {
        return stockService.pageStocks(request);
    }

    @Operation(summary = "库存详情")
    @GetMapping(path = "{stocksId}")
    public BaseResponse<DetailStock> detailStocks(@PathVariable("stocksId") String stocksId) {
        return stockService.detailStocks(stocksId);
    }

    @Operation(summary = "库存详情记录")
    @GetMapping(path = "records")
    public BasePageResponse<PageStocksRecord> stockRecord(StockRecordQuery request) {
        return stockService.stockRecord(request);
    }

    @Authority(action = "stockSecuritySetting")
    @Operation(summary = "库存下限设置")
    @PatchMapping(path = "{stocksId}")
    public BaseResponse<Void> stockRecord(@PathVariable("stocksId") String stocksId, @Valid @RequestBody SetMinLimit request) {
        return stockService.setMinLimit(stocksId, request);
    }

    @Authority(action = "stockInReview")
    @Operation(summary = "入库单审核")
    @PatchMapping(path = "inOuts/{stocksInOutId}/inReview")
    public BaseResponse<Void> stocksInReview(@PathVariable("stocksInOutId") String stocksInOutId, @Valid @RequestBody StockReviewResult request) throws Exception{
        return stockService.stocksInReview(stocksInOutId, request);
    }

    @Authority(action = "stockOutReview")
    @Operation(summary = "出库单审核")
    @PatchMapping(path = "inOuts/{stocksInOutId}/outReview")
    public BaseResponse<Void> stocksOutReview(@PathVariable("stocksInOutId") String stocksInOutId, @Valid @RequestBody StockReviewResult request) throws Exception{
        return stockService.stocksOutReview(stocksInOutId, request);
    }

    @Authority(action = "stockInEdit")
    @Operation(summary = "入库库存记录编辑")
    @PatchMapping(path = "inOuts/{stocksInOutId}/in")
    public BaseResponse<Void> editStocks(@PathVariable("stocksInOutId") String stocksInOutId, @Valid @RequestBody EditStockIn request) {
        return stockService.editStocksIn(stocksInOutId, request);
    }

    @Authority(action = "stockOutEdit")
    @Operation(summary = "出库库存记录编辑")
    @PatchMapping(path = "inOuts/{stocksInOutId}/out")
    public BaseResponse<Void> editStocksOut(@PathVariable("stocksInOutId") String stocksInOutId, @Valid @RequestBody EditStockOut request) {
        return stockService.editStocksOut(stocksInOutId, request);
    }
}
