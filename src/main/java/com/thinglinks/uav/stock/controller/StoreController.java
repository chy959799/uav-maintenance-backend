package com.thinglinks.uav.stock.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.stock.dto.store.AddStore;
import com.thinglinks.uav.stock.dto.store.EditStore;
import com.thinglinks.uav.stock.dto.store.PageStore;
import com.thinglinks.uav.stock.dto.store.PageStoreQuery;
import com.thinglinks.uav.stock.service.StoreService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/18
 */

@Api(tags = "仓库管理")
@RestController
@RequestMapping(value = "stores")
@Slf4j
public class StoreController {


    @Resource
    private StoreService storeService;

    @Operation(summary = "仓库分页查询")
    @GetMapping(path = "page")
    public BasePageResponse<PageStore> page(PageStoreQuery request) {
        return storeService.page(request);
    }

    @Authority(action = "storeAdd", allowAdmin = false)
    @Operation(summary = "新增仓库")
    @PostMapping
    public BaseResponse<Void> addStores(@Valid @RequestBody AddStore request) {
        return storeService.addStores(request);
    }

    @Authority(action = "storeDelete")
    @Operation(summary = "删除仓库")
    @DeleteMapping(path = "{storeId}")
    public BaseResponse<Void> delStores(@PathVariable("storeId") String storeId) {
        return storeService.delStores(storeId);
    }

    @Authority(action = "storeEdit")
    @Operation(summary = "编辑仓库")
    @PatchMapping(path = "{storeId}")
    public BaseResponse<Void> editStores(@PathVariable("storeId") String storeId, @Valid @RequestBody EditStore request) {
        return storeService.editStores(storeId, request);
    }
}
