package com.thinglinks.uav.stock.controller;

import com.thinglinks.uav.common.dto.*;
import com.thinglinks.uav.stock.dto.borrow.*;
import com.thinglinks.uav.stock.service.BorrowService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "借用管理")
@RestController
@RequestMapping(value = "borrow")
@Slf4j
public class BorrowController {

    @Resource
    private BorrowService borrowService;

    @Authority(action = "addBorrow", name = "新增借用")
    @Operation(summary = "新增借用")
    @PostMapping
    public BaseResponse<Void> addBorrow(@Valid @RequestBody AddBorrow request) throws Exception {
        return borrowService.addBorrow(request);
    }

    @Authority(action = "reviewBorrow", name = "借用审核")
    @Operation(summary = "借用审核")
    @PostMapping(path = "{stcBrwId}/reviewBorrow")
    public BaseResponse<Void> reviewBorrow(@PathVariable(name = "stcBrwId") String stcBrwId, @Valid @RequestBody ReviewResult request) throws Exception {
        return borrowService.reviewBorrow(stcBrwId, request);
    }

    @Authority(action = "deleteBorrow", name = "删除借用")
    @Operation(summary = "删除借用")
    @DeleteMapping(path = "{stcBrwId}/deleteBorrow")
    public BaseResponse<Void> deleteBorrow(@PathVariable(name = "stcBrwId") String stcBrwId) throws Exception {
        return borrowService.deleteBorrow(stcBrwId);
    }

    @Operation(summary = "取消借用")
    @PostMapping(path = "{stcBrwId}/cancelBorrow")
    public BaseResponse<Void> cancelBorrow(@PathVariable(name = "stcBrwId") String stcBrwId) throws Exception {
        return borrowService.cancelBorrow(stcBrwId);
    }

    @Operation(summary = "借用分页查询")
    @GetMapping(path = "page")
    public BasePageResponse<PageBorrow> pageBorrow(PageBorrowQuery request) {
        return borrowService.pageBorrow(request);
    }

    @Operation(summary = "借用详情查询")
    @GetMapping(path = "{stcBrwId}/detailBorrow")
    public BaseResponse<DetailBorrow> detailBorrow(@PathVariable(name = "stcBrwId") String stcBrwId) {
        return borrowService.detailBorrow(stcBrwId);
    }

    @Operation(summary = "借用清单分页查询")
    @GetMapping(path = "item/page")
    public BasePageResponse<PageBorrowItem> pageBorrowItem(PageBorrowItemQuery request) {
        return borrowService.pageBorrowItem(request);
    }

    @Authority(action = "getBorrow", name = "物品领用")
    @Operation(summary = "物品领用")
    @PostMapping(path = "{stcBrwId}/getBorrow")
    public BaseResponse<Void> getBorrow(@PathVariable(name = "stcBrwId") String stcBrwId, @RequestBody Remark request) throws Exception {
        return borrowService.getBorrow(stcBrwId, request);
    }

    @Authority(action = "stockReturn", name = "物品归还")
    @Operation(summary = "物品归还")
    @PostMapping(path = "stockReturn")
    public BaseResponse<Void> stockReturn(@RequestBody AddReturn request) throws Exception {
        return borrowService.stockReturn(request);
    }

    @Operation(summary = "归还清单查询")
    @GetMapping(path = "{stcBrwId}/stockReturn")
    public BaseResponse<List<PageReturn>> queryStockReturn(@PathVariable(name = "stcBrwId") String stcBrwId) {
        return borrowService.queryStockReturn(stcBrwId);
    }

    @Operation(summary = "归还清单详细")
    @GetMapping(path = "{stcRetId}/detailReturn")
    public BaseResponse<DetailReturn> detailReturn(@PathVariable(name = "stcRetId") String stcRetId) {
        return borrowService.detailReturn(stcRetId);
    }

    @Authority(action = "deleteBorrow", name = "删除借用")
    @Operation(summary = "批量删除借用")
    @DeleteMapping(path = "ids")
    public BaseResponse<Void> deleteBorrow(@RequestBody @Valid IdsRequest request) throws Exception {
        return borrowService.deleteBorrow(request.getIds());
    }
}
