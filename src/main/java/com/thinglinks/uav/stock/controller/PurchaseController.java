package com.thinglinks.uav.stock.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.IdsRequest;
import com.thinglinks.uav.common.dto.ReviewResult;
import com.thinglinks.uav.stock.dto.DetailStockLog;
import com.thinglinks.uav.stock.dto.purchase.*;
import com.thinglinks.uav.stock.service.PurchaseService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "申购管理")
@RestController
@RequestMapping(value = "purchase")
@Slf4j
public class PurchaseController {

    @Resource
    private PurchaseService purchaseService;

    @Authority(action = "addPurchase", name = "新增申购")
    @Operation(summary = "新增申购")
    @PostMapping
    public BaseResponse<Void> addPurchase(@Valid @RequestBody AddPurchase request) throws Exception {
        return purchaseService.addPurchase(request);
    }

    @Authority(action = "editPurchase", name = "编辑申购")
    @Operation(summary = "申购编辑")
    @PostMapping(path = "{stcPrchId}/editPurchase")
    public BaseResponse<Void> editPurchase(@PathVariable(name = "stcPrchId") String stcPrchId, @Valid @RequestBody AddPurchase request) throws Exception {
        return purchaseService.editPurchase(stcPrchId, request);
    }

    @Authority(action = "firsReviewPurchase", name = "申购初核")
    @Operation(summary = "申购初核")
    @PostMapping(path = "{stcPrchId}/firsReviewPurchase")
    public BaseResponse<Void> firsReviewPurchase(@PathVariable(name = "stcPrchId") String stcPrchId, @Valid @RequestBody ReviewResult request) throws Exception {
        return purchaseService.firsReviewPurchase(stcPrchId, request);
    }

        @Authority(action = "lastReviewPurchase", name = "申购终核")
    @Operation(summary = "申购终核")
    @PostMapping(path = "{stcPrchId}/lastReviewPurchase")
    public BaseResponse<Void> lastReviewPurchase(@PathVariable(name = "stcPrchId") String stcPrchId, @Valid @RequestBody ReviewResult request) throws Exception {
        return purchaseService.lastReviewPurchase(stcPrchId, request);
    }

    @Operation(summary = "申购分页查询")
    @GetMapping(path = "page")
    public BasePageResponse<PagePurchase> pagePurchase(PagePurchaseQuery request) {
        return purchaseService.pagePurchase(request);
    }

    @Operation(summary = "申购详情")
    @GetMapping(path = "{stcPrchId}/detail")
    public BaseResponse<DetailPurchase> detailPurchase(@PathVariable(name = "stcPrchId") String stcPrchId) throws Exception {
        return purchaseService.detailPurchase(stcPrchId);
    }

    @Operation(summary = "申购清单分页查询")
    @GetMapping(path = "item/page")
    public BasePageResponse<PagePurchaseItem> pagePurchase(PagePurchaseItemQuery request) {
        return purchaseService.pagePurchaseItem(request);
    }

    @Operation(summary = "申购日志")
    @GetMapping(path = "{stcPrchId}/purchaseLogs")
    public BaseResponse<List<DetailStockLog>> queryPurchaseLogs(@PathVariable(name = "stcPrchId") String stcPrchId) throws Exception {
        return purchaseService.queryPurchaseLogs(stcPrchId);
    }

    @Authority(action = "exportPurchaseItem", name = "申购导出")
    @Operation(summary = "申购导出")
    @PostMapping(path = "{stcPrchId}/exportPurchaseItem")
    public void exportPurchaseItem(HttpServletResponse response, @PathVariable(name = "stcPrchId") String stcPrchId) throws Exception {
        purchaseService.exportPurchaseItem(response, stcPrchId);
    }

    @Operation(summary = "申购编号查询")
    @GetMapping(path = "purchaseCode")
    public BaseResponse<List<DetailPurchase>> queryPurchaseCode(@RequestParam(name = "code", required = false) String code, @RequestParam(name = "storeId", required = false) String storeId) throws Exception {
        return purchaseService.queryPurchaseCode(code, storeId);
    }

    @Authority(action = "deletePurchase", name = "删除申购")
    @Operation(summary = "删除申购")
    @DeleteMapping(path = "{stcPrchId}/deletePurchase")
    public BaseResponse<Void> deletePurchase(@PathVariable(name = "stcPrchId") String stcPrchId) throws Exception {
        return purchaseService.deletePurchase(stcPrchId);
    }

    @Authority(action = "deletePurchase", name = "删除申购")
    @Operation(summary = "批量删除申购")
    @DeleteMapping(path = "ids")
    public BaseResponse<Void> deleteOrder(@RequestBody @Valid IdsRequest request) throws Exception {
        return purchaseService.deletePurchase(request.getIds());
    }

    @Operation(summary = "取消申购")
    @PostMapping(path = "{stcPrchId}/cancelPurchase")
    public BaseResponse<Void> cancelPurchase(@PathVariable(name = "stcPrchId") String stcPrchId) throws Exception {
        return purchaseService.cancelPurchase(stcPrchId);
    }
}
