package com.thinglinks.uav.stock.enums;

import lombok.Getter;

import java.util.HashMap;

public enum BorrowStatus {
    UNKNOWN("-1", "未知"),
    WAITING_REVIEW("100", "待审核"),
    WAITING_GET("200", "待领取"),
    WAITING_RETURN("300", "待归还"),
    FINISH("400", "已完成"),
    REJECT("500", "已驳回"),
    CANCEL("600", "已取消");

    private final static HashMap<String, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (BorrowStatus type : BorrowStatus.values()) {
            CODE_TO_VALUE.put(type.code, type.message);
        }
    }

    public static String fromCode(String code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.message;
        }
        return value;
    }


    /**
     * 枚举编码
     */
    @Getter
    private final String code;

    /**
     * 枚举值
     */
    private final String message;

    BorrowStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
