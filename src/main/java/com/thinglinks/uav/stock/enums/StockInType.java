package com.thinglinks.uav.stock.enums;

import lombok.Getter;

import java.util.HashMap;

public enum StockInType {

    UNKNOWN("-1", "未知"),
    PURCHASE("1", "申购单入库"),
    DIRECT("2", "直接入库"),
    SALE_RETURN("3", "销售退回"),
    BORROW_RETURN("4", "借用归还");

    private final static HashMap<String, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (StockInType type : StockInType.values()) {
            CODE_TO_VALUE.put(type.code, type.message);
        }
    }

    public static String fromCode(String code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.message;
        }
        return value;
    }


    /**
     * 枚举编码
     */
    @Getter
    private final String code;

    /**
     * 枚举值
     */
    private final String message;

    StockInType(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
