package com.thinglinks.uav.stock.enums;

import lombok.Getter;

import java.util.HashMap;

public enum PurchaseType {
    UNKNOWN(-1, "未知"),
    SPARE(1, "备件申购"),
    STANDBY(2, "备品申购");

    private final static HashMap<Integer, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (PurchaseType type : PurchaseType.values()) {
            CODE_TO_VALUE.put(type.code, type.message);
        }
    }

    public static String fromCode(Integer code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.message;
        }
        return value;
    }


    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 枚举值
     */
    @Getter
    private final String message;

    PurchaseType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
