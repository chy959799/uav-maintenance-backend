package com.thinglinks.uav.stock.enums;

import lombok.Getter;

import java.util.HashMap;

public enum PurchaseStatus {
    UNKNOWN("-1", "未知"),
    WAITING_FIRST_REVIEW("100", "待初审"),
    WAITING_LAST_REVIEW("200", "待终审"),
    WAITING_PURCHASE("300", "待采购"),
    PART_PURCHASE("400", "部分采购"),
    FINISH("500", "已完成"),
    REJECT("600", "已驳回"),
    CANCEL("700", "已取消");

    private final static HashMap<String, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (PurchaseStatus type : PurchaseStatus.values()) {
            CODE_TO_VALUE.put(type.code, type.message);
        }
    }

    public static String fromCode(String code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.message;
        }
        return value;
    }


    /**
     * 枚举编码
     */
    @Getter
    private final String code;

    /**
     * 枚举值
     */
    private final String message;

    PurchaseStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
