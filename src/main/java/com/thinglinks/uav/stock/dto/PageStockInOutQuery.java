package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.common.dto.TimePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class PageStockInOutQuery extends TimePageRequest {

    @ApiModelProperty(value = "库存类型 1:入库 2:出库")
    private Integer type;

    @ApiModelProperty(value = "申购编码")
    private String stcPurchaseCode;

    @ApiModelProperty(value = "出入库内部编号")
    private String innerCode;

    @ApiModelProperty(value = "工单编号")
    private String orderCode;

    @ApiModelProperty(value = "入库类型, 1: 申购单入库 2: 直接入库 3: 销售退回")
    private String inType;

    @ApiModelProperty(value = "出库类型, 1: 工单出库 2: 直接出库 3: 销售出库 4: 破损出库")
    private String outType;

    //出入库状态 1: 待审核, 2: 已完成 3: 已驳回
    @ApiModelProperty(value = "出入库状态 1: 待审核, 2: 已完成 3: 已驳回")
    private Integer status;

    @ApiModelProperty(value = "仓库Id")
    private String storeid;

}
