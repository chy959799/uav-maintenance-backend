package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Positive;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Data
public class StockInItem {

    @ApiModelProperty(value = "产品id")
    private String pid;

    @ApiModelProperty(value = "数量")
    @Positive
    private Integer count;

    @ApiModelProperty(value = "单价")
    private Double price;

    @ApiModelProperty(value = "备注")
    private String remark;
}
