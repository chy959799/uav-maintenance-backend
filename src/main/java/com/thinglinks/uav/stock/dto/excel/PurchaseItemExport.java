package com.thinglinks.uav.stock.dto.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

@Data
public class PurchaseItemExport {

    @ExcelProperty(value = "序号", index = 0)
    @ColumnWidth(10)
    private Integer index;

    @ExcelProperty(value = "申购单编号", index = 1)
    @ColumnWidth(15)
    private String innerCode;

    @ExcelProperty(value = "申购类型", index = 2)
    @ColumnWidth(15)
    private String purchaseType;

    @ExcelProperty(value = "仓库", index = 3)
    @ColumnWidth(15)
    private String storeName;

    @ExcelProperty(value = "名称", index = 4)
    @ColumnWidth(15)
    private String productName;

    @ExcelProperty(value = "类型", index = 5)
    @ColumnWidth(15)
    private String categoryName;

    @ExcelProperty(value = "编码", index = 6)
    @ColumnWidth(15)
    private String categoryCode;

    @ExcelProperty(value = "规格型号", index = 7)
    @ColumnWidth(15)
    private String model;

    @ExcelProperty(value = "原长地", index = 8)
    @ColumnWidth(15)
    private String origin;

    @ExcelProperty(value = "单位", index = 9)
    @ColumnWidth(15)
    private String unit;

    @ExcelProperty(value = "申购数量", index = 10)
    @ColumnWidth(15)
    private Integer count;

}
