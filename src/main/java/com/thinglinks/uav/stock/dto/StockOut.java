package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Data
public class StockOut {

    @ApiModelProperty(value = "出库类型, 1: 工单出库 2: 直接出库 3: 销售出库 4: 破损出库 5:领用出库")
    @NotBlank
    private String outType;

    @ApiModelProperty(value = "仓库Id")
    private String storeid;

    @ApiModelProperty(value = "工单编号")
    private String ordInnerCode;

    @ApiModelProperty(value = "工单成本报价id")
    private String oqid;

    @ApiModelProperty(value = "借用内部ID")
    private String stcBrwId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "备件列表")
    @NotNull
    private List<StockOutItem> items;

}
