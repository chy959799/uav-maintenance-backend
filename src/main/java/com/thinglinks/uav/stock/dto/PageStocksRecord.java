package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.stock.entity.StockInOut;
import com.thinglinks.uav.stock.entity.StockInOutItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class PageStocksRecord extends StockInOutItem {

    @ApiModelProperty(value = "出入库记录")
    private StockInOut stocksInOut;

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "产品")
    private Product product;

}
