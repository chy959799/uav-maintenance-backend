package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.stock.entity.StockLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/12
 */
@Data
public class DetailStockLog extends StockLog {

    @ApiModelProperty(value = "操作时间")
    private String operateTime;
}
