package com.thinglinks.uav.stock.dto.store;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/18
 */
@Data
public class PageStoreQuery extends BasePageRequest {

    @ApiModelProperty(value = "仓库名称")
    private String name;

    @ApiModelProperty(value = "仓库类型，1：备件仓库；2：备品仓库")
    private Integer storeType;
}
