package com.thinglinks.uav.stock.dto.borrow;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BorrowItem {

    @ApiModelProperty(value = "数量")
    private Integer count;

    @ApiModelProperty(value = "产品ID")
    private String pid;
}
