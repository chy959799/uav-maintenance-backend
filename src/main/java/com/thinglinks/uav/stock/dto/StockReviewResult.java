package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.StocksConstants;
import com.thinglinks.uav.common.dto.ReviewResult;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/23
 */
@Data
public class StockReviewResult extends ReviewResult {
    public Integer getStatus() {
        if (CommonConstants.PASS.equals(this.getReviewResult())) {
            return StocksConstants.FINISH;
        }
        return StocksConstants.REJECT;
    }
}
