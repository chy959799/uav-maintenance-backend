package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.stock.entity.StockBorrow;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PageBorrow extends StockBorrow {

    @ApiModelProperty(value = "预计归还时间")
    private String planReturnTime;

    @ApiModelProperty(value = "申请时间")
    private String applyTime;

    @ApiModelProperty(value = "审批时间")
    private String reviewTime;

    @ApiModelProperty(value = "归还时间")
    private String returnTime;
}
