package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.stock.entity.StockReturn;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PageReturn extends StockReturn {

    @ApiModelProperty(value = "操作时间")
    private String operateTime;
}
