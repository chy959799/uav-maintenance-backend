package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class DetailStock extends PageStock {

    @ApiModelProperty(value = "审核用户")
    private User user;
}
