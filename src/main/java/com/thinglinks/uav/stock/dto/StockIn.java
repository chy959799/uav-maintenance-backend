package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Data
public class StockIn {

    @ApiModelProperty(value = "入库类型, 1: 申购单入库 2: 直接入库 3: 销售退回 4:物品归还")
    @NotBlank
    private String inType;

    @ApiModelProperty(value = "申购单编号")
    private String purchaseInnerCode;

    @ApiModelProperty(value = "仓库Id")
    @NotBlank
    private String storeid;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "备件列表")
    @NotNull
    private List<StockInItem> items;

}
