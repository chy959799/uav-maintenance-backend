package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class StockRecordQuery extends BasePageRequest {

    @ApiModelProperty(value = "产品ID")
    private String pid;

    @ApiModelProperty(value = "出入库状态 1: 待审核, 2: 已完成 3: 已驳回")
    private Integer status;

    @ApiModelProperty(value = "所属仓库Id")
    private String storeid;


}
