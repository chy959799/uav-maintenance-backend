package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class PageStockQuery extends BasePageRequest {


    @ApiModelProperty(value = "所属仓库Id")
    private String storeid;

    @ApiModelProperty(value = "备件名称")
    private String name;

    @ApiModelProperty("备件编码")
    private String innerCode;

    @ApiModelProperty("备件类型或者备品类型")
    private String type;

    @ApiModelProperty(value = "库存类型类型, 1:备件；2：备品")
    private Integer storeType;
}
