package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Data
public class EditStockOut {

    @ApiModelProperty(value = "出库类型, 1: 工单出库 2: 直接出库 3: 销售出库 4: 破损出库")
    private String outType;

    @ApiModelProperty(value = "仓库Id")
    private String storeid;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "备件列表")
    private List<StockOutItem> items;

}
