package com.thinglinks.uav.stock.dto.borrow;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddReturn {

    @ApiModelProperty(value = "借用ID")
    @NotBlank
    private String stcBrwId;

    @ApiModelProperty(value = "归还备注")
    private String remark;

    @ApiModelProperty(value = "借用清单")
    @NotNull
    private List<ReturnItem> items;
}
