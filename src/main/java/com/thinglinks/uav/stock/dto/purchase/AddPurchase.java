package com.thinglinks.uav.stock.dto.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddPurchase {

    @ApiModelProperty(value = "申购类型, 1:备件申购；2：备品申购")
    @NotNull
    private Integer purchaseType;

    @ApiModelProperty(value = "需求日期")
    @NotNull
    private Long deliveryAt;

    @ApiModelProperty(value = "申购说明")
    @NotBlank
    private String remark;

    @ApiModelProperty(value = "入库仓库")
    @NotBlank
    private String storeId;

    @ApiModelProperty(value = "申购列表")
    @NotNull
    private List<PurchaseItem> items;
}
