package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/12
 */
@Data
public class PageBorrowItemQuery extends BasePageRequest {

    @ApiModelProperty(value = "借用内部编号")
    private String innerCode;


    @ApiModelProperty(value = "内部ID")
    private String stcBrwId;

}
