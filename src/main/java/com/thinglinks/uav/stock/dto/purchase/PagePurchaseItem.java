package com.thinglinks.uav.stock.dto.purchase;

import com.thinglinks.uav.stock.entity.StockPurchItem;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/12
 */
@Data
public class PagePurchaseItem extends StockPurchItem {

}
