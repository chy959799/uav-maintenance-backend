package com.thinglinks.uav.stock.dto.borrow;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddBorrow {

    @ApiModelProperty(value = "借用类型：1：内部借用；2：外部借用")
    @NotNull
    private Integer type;

    @ApiModelProperty(value = "预计归还时间")
    @NotNull
    private Long planReturnAt;

    @ApiModelProperty(value = "仓库内部ID")
    @NotBlank
    private String storeId;

    @ApiModelProperty(value = "物品用途")
    @NotBlank
    private String usage;

    @ApiModelProperty(value = "借用清单")
    @NotNull
    private List<BorrowItem> items;
}
