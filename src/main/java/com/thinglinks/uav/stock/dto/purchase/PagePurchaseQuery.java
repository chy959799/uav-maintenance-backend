package com.thinglinks.uav.stock.dto.purchase;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PagePurchaseQuery extends BasePageRequest {

    @ApiModelProperty(value = "申购内部编号")
    private String innerCode;

    @ApiModelProperty(value = "申购类型, 1:备件申购；2：备品申购")
    private Integer purchaseType;

    @ApiModelProperty(value = "申购单状态 100：待初审；200：待终审；300：待采购；400：部分采购；500：已完成；600：已驳回；700：已取消 ")
    private String status;

    @ApiModelProperty(value = "申请人部门")
    private String did;

    @ApiModelProperty(value = "申请人名称")
    private String userName;

    @ApiModelProperty(value = "开始需求时间")
    private Long deliveryStartAt;

    @ApiModelProperty(value = "结束需求时间")
    private Long deliveryEndAt;

    @ApiModelProperty(value = "开始申购单实际完成时间")
    private Long completeStartAt;

    @ApiModelProperty(value = "结束申购单实际完成时间")
    private Long completeEndAt;
}
