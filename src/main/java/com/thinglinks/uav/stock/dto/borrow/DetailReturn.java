package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.stock.entity.StockReturn;
import com.thinglinks.uav.stock.entity.StockReturnItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DetailReturn extends StockReturn {

    @ApiModelProperty(value = "归还详细清单")
    private List<StockReturnItem> items;
}
