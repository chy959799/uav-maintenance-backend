package com.thinglinks.uav.stock.dto.borrow;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/14
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DetailBorrow extends PageBorrow {



}
