package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.stock.entity.StockInOut;
import com.thinglinks.uav.stock.entity.Store;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class PageStockInOut extends StockInOut {

    @ApiModelProperty(value = "仓库")
    private Store store;

    @ApiModelProperty(value = "审批人")
    private User user;

    @ApiModelProperty(value = "申请入库用户")
    private User applyUser;

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "工单")
    private Order order;
}
