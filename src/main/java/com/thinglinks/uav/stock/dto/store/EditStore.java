package com.thinglinks.uav.stock.dto.store;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/18
 */
@Data
public class EditStore {

    @ApiModelProperty(value = "仓库名称")
    @NotBlank
    private String name;

    @ApiModelProperty(value = "仓库编码")
    private String code;

    @ApiModelProperty(value = "库存类型, 1:自有仓库 2:他人仓库")
    private String type;

    @ApiModelProperty(value = "告警下限")
    private Integer minLimit;

    @ApiModelProperty(value = "告警上限")
    private Integer maxLimit;

    @ApiModelProperty(value = "零售价格倍率")
    private Float ratio;

    @ApiModelProperty(value = "是否允许空库存,若为否则工单定损时不出现空库存备件")
    private Boolean allowEmpty;

    @ApiModelProperty(value = "库存备注")
    private String remark;

    @ApiModelProperty(value = "是否为默认仓库")
    @JsonProperty(value = "default")
    private Boolean defaultStores;
}
