package com.thinglinks.uav.stock.dto.purchase;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/12
 */
@Data
public class PagePurchaseItemQuery extends BasePageRequest {

    @ApiModelProperty(value = "申购内部编号")
    private String innerCode;

    @ApiModelProperty(value = "申购内部ID")
    private String stcPrchId;

}
