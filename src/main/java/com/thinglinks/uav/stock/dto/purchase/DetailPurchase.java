package com.thinglinks.uav.stock.dto.purchase;

import com.thinglinks.uav.stock.entity.Store;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/12
 */
@Data
public class DetailPurchase extends PagePurchase {

    @ApiModelProperty(value = "创建时间")
    private String createTime;

    @ApiModelProperty(value = "申购入库仓库")
    private Store store;
}
