package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class StockReview {

    @ApiModelProperty(value = "申购审核状态")
    private Integer reviewResult;

    @ApiModelProperty(value = "申购审核备注")
    private String reviewRemark;
}
