package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.stock.entity.StockInOutItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/31
 */
@Data
public class DetailStockInOut extends PageStockInOut {

    @ApiModelProperty(value = "备件列表")
    private List<StockInOutItem>  items;
}
