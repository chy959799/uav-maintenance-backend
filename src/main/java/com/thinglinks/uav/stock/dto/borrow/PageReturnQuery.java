package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PageReturnQuery extends BasePageRequest {

    @ApiModelProperty(value = "借用ID")
    private String stcBrwId;
}
