package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.stock.entity.StockBorrowItem;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/8/14
 */
@Data
public class PageBorrowItem extends StockBorrowItem {

}
