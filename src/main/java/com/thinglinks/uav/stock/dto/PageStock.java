package com.thinglinks.uav.stock.dto;

import com.thinglinks.uav.stock.entity.Stock;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class PageStock extends Stock {

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;
}
