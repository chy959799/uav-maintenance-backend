package com.thinglinks.uav.stock.dto.store;

import com.thinglinks.uav.stock.entity.Store;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/18
 */
@Data
public class PageStore extends Store {

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;
}
