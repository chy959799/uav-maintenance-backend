package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Data
public class StockOutItem {

    @ApiModelProperty(value = "产品id")
    private String pid;

    @ApiModelProperty(value = "数量")
    private Integer count;
}
