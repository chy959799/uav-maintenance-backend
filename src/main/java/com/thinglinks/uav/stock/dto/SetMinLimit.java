package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/21
 */
@Data
public class SetMinLimit {

    @ApiModelProperty(value = "安全设置的值")
    @NotNull
    private Integer minLimit;
}
