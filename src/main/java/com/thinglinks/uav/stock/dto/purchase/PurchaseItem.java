package com.thinglinks.uav.stock.dto.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PurchaseItem {

    @ApiModelProperty(value = "申购数量")
    @NotNull
    private Integer count;

    @ApiModelProperty(value = "产品ID")
    @NotBlank
    private String pid;
}
