package com.thinglinks.uav.stock.dto.purchase;

import com.thinglinks.uav.stock.entity.StockPurchase;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PagePurchase extends StockPurchase {

    @ApiModelProperty(value = "需求日期")
    private String deliveryTime;

    @ApiModelProperty(value = "采购完成时间")
    private String completeTime;

    @ApiModelProperty(value = "申请时间")
    private String applyTime;

    @ApiModelProperty(value = "审批时间")
    private String reviewTime;

}
