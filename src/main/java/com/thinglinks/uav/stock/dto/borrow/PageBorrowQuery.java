package com.thinglinks.uav.stock.dto.borrow;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PageBorrowQuery extends BasePageRequest {

    @ApiModelProperty(value = "借用内部编号")
    private String innerCode;

    @ApiModelProperty(value = "借用类型：1：内部借用；2：外部借用")
    private Integer borrowType;

    @ApiModelProperty(value = "借用状态：1：待审核；2：待领取；3：带归还；4：已完成；5：已驳回；5：已取消 ")
    private String status;

    @ApiModelProperty(value = "申请人部门")
    private String did;

    @ApiModelProperty(value = "仓库Id")
    private String storeId;

    @ApiModelProperty(value = "申请人名称")
    private String userName;

    @ApiModelProperty(value = "开始申请时间")
    private Long applyStartAt;

    @ApiModelProperty(value = "结束申请时间")
    private Long applyEndAt;

    @ApiModelProperty(value = "开始归还时间")
    private Long returnStartAt;

    @ApiModelProperty(value = "结束归还时间")
    private Long returnEndAt;
}
