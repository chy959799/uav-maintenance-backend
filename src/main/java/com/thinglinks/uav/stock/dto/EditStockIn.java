package com.thinglinks.uav.stock.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/20
 */
@Data
public class EditStockIn {

    @ApiModelProperty(value = "仓库Id")
    private String storeid;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "备件列表")
    private List<StockInItem> items;

}
