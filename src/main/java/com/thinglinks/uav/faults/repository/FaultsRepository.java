package com.thinglinks.uav.faults.repository;

import com.thinglinks.uav.faults.entity.Fault;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (Fault)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-21 18:42:27
 */
public interface FaultsRepository extends JpaRepository<Fault, Integer>, JpaSpecificationExecutor<Fault> {

	void deleteByFauid(String fauid);

	Fault findByFauid(String fauid);

	void deleteByFauidIn(List<String> ids);

	boolean existsByPidIn(List<String> ids);

	List<Fault> findAllByCtBetween(Long startTime, Long endTime);

	List<Fault> findAllByCatid(String catid);
}


