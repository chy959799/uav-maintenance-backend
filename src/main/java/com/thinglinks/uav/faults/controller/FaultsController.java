package com.thinglinks.uav.faults.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.faults.dto.FaultDTO;
import com.thinglinks.uav.faults.dto.FaultPageQuery;
import com.thinglinks.uav.faults.service.FaultsService;
import com.thinglinks.uav.faults.vo.FaultInfoVO;
import com.thinglinks.uav.faults.vo.FaultVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * (Fault)表控制层
 *
 * @author iwiFool
 * @since 2024-03-21 18:42:27
 */
@Api(tags = "故障管理")
@RestController
@RequestMapping("fault")
public class FaultsController {

	@Resource
	private FaultsService faultsService;

	@Authority(action = "faultAdd")
	@Operation(summary = "新增故障")
	@PostMapping
	public BaseResponse<String> addFault(@Valid FaultDTO dto) {
		return faultsService.insert(dto);
	}

	@Authority(action = "faultDelete")
	@Operation(summary = "删除故障")
	@DeleteMapping("{fauid}")
	public BaseResponse<String> deleteFault(@PathVariable String fauid) {
		return faultsService.deleteFault(fauid);
	}

	@Authority(action = "faultDelete")
	@Operation(summary = "批量删除故障")
	@DeleteMapping
	public BaseResponse<String> deleteAll(@RequestBody DeleteDTO dto) {
		return faultsService.deleteAll(dto);
	}

	@Authority(action = "faultEdit")
	@Operation(summary = "修改故障")
	@PatchMapping("{fauid}")
	public BaseResponse<String> updateFault(@PathVariable String fauid,@Valid FaultDTO dto) {
		return faultsService.updateFault(fauid, dto);
	}

	@Operation(summary = "故障列表")
	@GetMapping
	public BasePageResponse<FaultVO> getFaultPage(FaultPageQuery pageQuery) {
		return faultsService.getFaultPage(pageQuery);
	}

	@Operation(summary = "故障详情")
	@GetMapping("{fauid}")
	public BaseResponse<FaultInfoVO> getFaultInfo(@PathVariable String fauid) {
		return faultsService.getFaultInfo(fauid);
	}

}


