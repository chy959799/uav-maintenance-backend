package com.thinglinks.uav.faults.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.faults.dto.FaultDTO;
import com.thinglinks.uav.faults.dto.FaultPageQuery;
import com.thinglinks.uav.faults.vo.FaultInfoVO;
import com.thinglinks.uav.faults.vo.FaultVO;
import org.springframework.transaction.annotation.Transactional;

/**
 * (Fault)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-21 18:42:27
 */
public interface FaultsService {

	BaseResponse<String> insert(FaultDTO dto);

	@Transactional
	BaseResponse<String> deleteFault(String fauid);

	@Transactional
	BaseResponse<String> deleteAll(DeleteDTO dto);

	@Transactional
	BaseResponse<String> updateFault(String fauid, FaultDTO dto);

	BasePageResponse<FaultVO> getFaultPage(FaultPageQuery pageQuery);

	BaseResponse<FaultInfoVO> getFaultInfo(String fauid);
}


