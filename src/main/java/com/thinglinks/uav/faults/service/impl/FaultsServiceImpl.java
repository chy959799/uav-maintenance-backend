package com.thinglinks.uav.faults.service.impl;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.FaultConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.faults.dto.FaultDTO;
import com.thinglinks.uav.faults.dto.FaultPageQuery;
import com.thinglinks.uav.faults.entity.Fault;
import com.thinglinks.uav.faults.repository.FaultsRepository;
import com.thinglinks.uav.faults.service.FaultsService;
import com.thinglinks.uav.faults.vo.FaultInfoVO;
import com.thinglinks.uav.faults.vo.FaultVO;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.user.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (Fault)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-21 18:42:27
 */
@Slf4j
@Service
public class FaultsServiceImpl implements FaultsService {

	@Resource
	private FaultsRepository faultsRepository;

	@Resource
	private CustomMinIOClient customMinIOClient;

	@Resource
	private FileRepository fileRepository;

	@Override
	@Transactional
	public BaseResponse<String> insert(FaultDTO dto) {
		Fault fault = new Fault();
		BeanUtils.copyProperties(dto, fault);
		fault.setOrigin(FaultConstants.FAULT_ORIGIN);
		fault.setFauid(CommonUtils.uuid());
		fault.setUid(CommonUtils.getUid());
		fault.setCpid(CommonUtils.getCpid());

		faultsRepository.save(fault);
		uploadFile(dto.getFile(), fault.getFauid());
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> deleteFault(String fauid) {
		fileRepository.deleteAllByFauid(fauid);
		faultsRepository.deleteByFauid(fauid);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> deleteAll(DeleteDTO dto) {
		fileRepository.deleteAllByFauidIn(dto.getIds());
		faultsRepository.deleteByFauidIn(dto.getIds());
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> updateFault(String fauid, FaultDTO dto) {
		Fault fault = faultsRepository.findByFauid(fauid);
		if (Objects.isNull(fault)) {
			return BaseResponse.fail("该故障不存在");
		}
		BeanUtils.copyProperties(dto, fault);
		// 如果修改时未传递文件，则不修改文件
		if (Objects.nonNull(dto.getFile()) && !dto.getFile().isEmpty()) {
			fileRepository.deleteAllByFauid(fauid);
			uploadFile(dto.getFile(), fault.getFauid());
		}

		return BaseResponse.success();
	}

	private void uploadFile(List<MultipartFile> file, String fauid) {
		ArrayList<CustomFile> list = new ArrayList<>();
		if (Objects.isNull(file) || file.isEmpty()) {
			return;
		}
		for (MultipartFile multipartFile : file) {
			CustomFile f = new CustomFile();
			f.setFid(CommonUtils.uuid());
			f.setFauid(fauid);
			f.setSize(multipartFile.getSize());
			f.setName(multipartFile.getOriginalFilename());
			f.setExtension(CommonUtils.getExtension(multipartFile.getOriginalFilename()));
			f.setDownload(Boolean.TRUE);
			f.setDirectory(FaultConstants.FAULT_BUCKET);
			f.setMime(multipartFile.getContentType());
			String objectName = f.getFid().concat(CommonConstants.DOT).concat(f.getExtension());
			try {
				customMinIOClient.uploadFile(FaultConstants.FAULT_BUCKET, objectName, multipartFile.getInputStream());
			} catch (Exception e) {
				throw new BusinessException("文件上传失败");
			}
			list.add(f);
		}
		fileRepository.saveAll(list);
	}

	@Override
	public BasePageResponse<FaultVO> getFaultPage(FaultPageQuery pageQuery) {
		Specification<Fault> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			if (!Objects.isNull(pageQuery.getPid()) && !pageQuery.getPid().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("pid"), pageQuery.getPid()));
			}
			if (!Objects.isNull(pageQuery.getCatid()) && !pageQuery.getCatid().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("catid"), pageQuery.getCatid()));
			}
			if (!Objects.isNull(pageQuery.getPhenomenon()) && !pageQuery.getPhenomenon().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("phenomenon"), CommonUtils.assembleLike(pageQuery.getPhenomenon())));
			}
			return predicate;
		};

		Page<Fault> faultPage = faultsRepository.findAll(specification, pageQuery.of());
		List<FaultVO> customerVOList = faultPage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(faultPage, customerVOList);
	}

	@Override
	public BaseResponse<FaultInfoVO> getFaultInfo(String fauid) {
		Fault fault = faultsRepository.findByFauid(fauid);
		return BaseResponse.success(convertToInfoVO(fault));
	}

	private FaultInfoVO convertToInfoVO(Fault fault) {
		FaultVO faultVO = convertToVO(fault);
		FaultInfoVO faultInfoVO = new FaultInfoVO();
		BeanUtils.copyProperties(faultVO, faultInfoVO);
		List<CustomFile> customFiles = fileRepository.findAllByFauid(fault.getFauid());
		List<FaultInfoVO.File> fileList = new ArrayList<>();
		customFiles.forEach(file -> {
			FaultInfoVO.File faultFile = new FaultInfoVO.File();
			faultFile.setFid(file.getFid());
			faultFile.setName(file.getName());
			faultFile.setSize(file.getSize());
			faultFile.setUrl(getFileProxyUrl(file));
			fileList.add(faultFile);
		});
		faultInfoVO.setFiles(fileList);
		return faultInfoVO;
	}

	private FaultVO convertToVO(Fault fault) {
		FaultVO vo = new FaultVO();
		BeanUtils.copyProperties(fault, vo);
		vo.setCreatedAt(DateUtils.formatDateTime(fault.getCt()));
		vo.setUpdatedAt(DateUtils.formatDateTime(fault.getUt()));

		FaultVO.Category category = new FaultVO.Category();
		category.setName(fault.getCategory().getName());
		vo.setCategory(category);

		FaultVO.User user = new FaultVO.User();
		User faultUser = fault.getUser();
		user.setUsername(faultUser.getUserName());
		Department department = faultUser.getDepartment();
		user.setDepartment(department.getName());
		vo.setUser(user);

		FaultVO.Product product = new FaultVO.Product();
		Product faultProduct = fault.getProduct();

		Category productCategory = faultProduct.getCategory();
		FaultVO.Product.Category faultProductCategory = new FaultVO.Product.Category();

		faultProductCategory.setName(productCategory.getName());
		product.setCategory(faultProductCategory);
		product.setName(faultProduct.getName());

		vo.setProduct(product);
		return vo;
	}

	/**
	 * 获取文件的访问地址
	 *
	 * @param customFile
	 * @return
	 */
	private String getFileProxyUrl(CustomFile customFile) {
		try {
			String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
			return customMinIOClient.getProxyUrl(FaultConstants.FAULT_BUCKET, objectName);
		} catch (Exception e) {
			log.error("getFileProxyUrl error", e);
		}
		return StringUtils.EMPTY;
	}
}


