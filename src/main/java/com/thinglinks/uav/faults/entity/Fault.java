package com.thinglinks.uav.faults.entity;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.system.annotation.TenantTable;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * (Fault)实体类
 *
 * @author iwiFool
 * @since 2024-03-21 18:42:27
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "faults")
@Data
@TenantTable
public class Fault extends BaseEntity {

	/**
	 * 故障内部ID
	 */
	private String fauid;
	/**
	 * 故障原因
	 */
	private String reason;
	/**
	 * 故障现象
	 */
	private String phenomenon;
	/**
	 * 故障解决方案
	 */
	private String solution;
	/**
	 * 故障来源 1: 手动创建 2: 工单自动创建
	 */
	private String origin;

	private String cpid;

	private String uid;

	private String pid;

	private String catid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "catid", referencedColumnName = "catid", insertable = false, updatable = false)
	private Category category;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pid", referencedColumnName = "pid", insertable = false, updatable = false)
	private Product product;
}
