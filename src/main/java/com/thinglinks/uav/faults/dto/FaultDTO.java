package com.thinglinks.uav.faults.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/21
 */
@Data
@ApiModel(description = "故障DTO")
public class FaultDTO {

	@ApiModelProperty(value = "故障原因", example = "故障原因1")
	@NotBlank
	private String reason;

	@ApiModelProperty(value = "故障现象", example = "这是故障现象")
	@NotBlank
	private String phenomenon;

	@ApiModelProperty(value = "故障解决方案", example = "故障解决方案")
	@NotBlank
	private String solution;

	@ApiModelProperty(value = "catid", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
	@NotBlank
	private String catid;

	@ApiModelProperty(value = "pid", example = "16707e57-b239-4a45-80dd-6d65f45d6dea")
	@NotBlank
	private String pid;

	@ApiModelProperty(value = "文件")
	List<MultipartFile> file;
}
