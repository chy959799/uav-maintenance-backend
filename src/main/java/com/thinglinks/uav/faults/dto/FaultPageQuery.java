package com.thinglinks.uav.faults.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class FaultPageQuery extends BasePageRequest {

	@ApiModelProperty(value = "pid", example = "16707e57-b239-4a45-80dd-6d65f45d6dea")
	private String pid;

	@ApiModelProperty(value = "catid", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
	private String catid;

	@ApiModelProperty(value = "故障描述", example = "这是故障现象")
	private String phenomenon;
}
