package com.thinglinks.uav.faults.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/21
 */
@Data
@ApiModel(value = "故障信息VO")
public class FaultVO {

	@ApiModelProperty(value = "故障id", example = "60")
	private Long id;

	@ApiModelProperty(value = "fauid", example = "f934200c-d762-449d-b865-4c08e98f00e0")
	private String fauid;

	@ApiModelProperty(value = "故障原因", example = "故障原因分析")
	private String reason;

	@ApiModelProperty(value = "故障描述", example = "故障现象描述")
	private String phenomenon;

	@ApiModelProperty(value = "故障解决方案", example = "故障解决方案")
	private String solution;

	@ApiModelProperty(value = "故障来源 1: 手动创建 2: 工单自动创建", example = "1")
	private String origin;

	@ApiModelProperty(value = "创建时间", example = "2024-03-21 12:00:00")
	private String createdAt;

	@ApiModelProperty(value = "更新时间", example = "2024-03-21 12:00:00")
	private String updatedAt;

	@ApiModelProperty(value = "cpid", example = "16707e57-b239-4a45-80dd-6d65f45d6dea")
	private String cpid;

	@ApiModelProperty(value = "uid", example = "88fe6fcc-de2c-4a42-8383-fc098456fb43")
	private String uid;

	@ApiModelProperty(value = "pid", example = "16707e57-b239-4a45-80dd-6d65f45d6dea")
	private String pid;

	@ApiModelProperty(value = "故障分类id", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
	private String catid;

	@ApiModelProperty(value = "故障分类")
	private Category category;

	@ApiModelProperty(value = "user")
	private User user;

	@ApiModelProperty(value = "产品")
	private Product product;

	@Data
	public static class Category {
		@ApiModelProperty(value = "分类名称", example = "故障分类1")
		private String name;
	}

	@Data
	public static class User {
		@ApiModelProperty(value = "username", example = "测试企业")
		private String username;

		private String department;
	}

	@Data
	public static class Product {
		@ApiModelProperty(value = "产品名称", example = "产品名称1")
		private String name;
		@ApiModelProperty(value = "产品分类")
		private Category category;

		@Data
		public static class Category {
			@ApiModelProperty(value = "分类名称", example = "产品分类1")
			private String name;
		}
	}
}
