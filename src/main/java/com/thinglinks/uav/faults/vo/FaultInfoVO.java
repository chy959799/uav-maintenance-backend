package com.thinglinks.uav.faults.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "故障详情VO")
public class FaultInfoVO extends FaultVO{

	private List<File> files;


	@Data
	public static class File{
		@ApiModelProperty(value = "文件名称")
		private String name;
		@ApiModelProperty(value = "fid")
		private String fid;
		@ApiModelProperty(value = "文件大小(字节)")
		private Long size;
		@ApiModelProperty(value = "文件外链")
		private String url;
	}
}
