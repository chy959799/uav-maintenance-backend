package com.thinglinks.uav.alarms.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class QueryAlarmDTO extends BasePageRequest {

    private String tag;

    private long startTime;

    private long endTime;

    @NotNull(message = "告警类型不能为空")
    private String type;

    private Boolean status;
}
