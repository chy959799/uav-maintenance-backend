package com.thinglinks.uav.alarms.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class AlarmsHandleDTO {
    private String remark;

    @NotNull
    private String type;
}
