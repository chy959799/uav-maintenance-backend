package com.thinglinks.uav.alarms.dto;

import com.thinglinks.uav.alarms.entity.AlarmsParameter;
import lombok.Data;

import java.util.List;

@Data
public class EditAlarmsParameterDTO {
    List<AlarmsParameter> alarmsParameters;
}
