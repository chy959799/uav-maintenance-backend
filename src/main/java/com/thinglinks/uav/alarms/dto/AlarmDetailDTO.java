package com.thinglinks.uav.alarms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AlarmDetailDTO {

    @ApiModelProperty(value = "预警类型")
    private String alarmType;

    @ApiModelProperty(value = "处理状态")
    private Boolean status;

    @ApiModelProperty(value = "预警经办人")
    private String agent;

    @ApiModelProperty(value = "经办部门")
    private String agentDepartment;

    @ApiModelProperty(value = "保险类型")
    private String insuranceType;

    @ApiModelProperty(value = "保险到期时间")
    private long insuranceExpireAt;

    @ApiModelProperty(value = "预警内容")
    private String content;

    @ApiModelProperty(value = "预警处理时间")
    private long handleAt;

    @ApiModelProperty(value = "预警处理结果")
    private String result;

    @ApiModelProperty(value = "预警处理备注")
    private String remark;

    @ApiModelProperty(value = "预警时间")
    private long alarmTime;

}
