package com.thinglinks.uav.alarms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AlarmDTO{
    @ApiModelProperty(value = "预警id")
    private String alarmid;

    @ApiModelProperty(value = "预警类型")
    private String alarmType;

    @ApiModelProperty(value = "预警标签")
    private String tag;

    @ApiModelProperty(value = "预警详细内用")
    private String content;

//    @ApiModelProperty(value = "展示预警内容")
//    private String displayWarningContent;

    @ApiModelProperty(value = "保险类型")
    private String insuranceType;

    @ApiModelProperty(value = "保单号")
    private String insuranceCode;

    @ApiModelProperty(value = "告警处理结果")
    private String result;

    @ApiModelProperty(value = "处理状态")
    private Boolean status;

    @ApiModelProperty(value = "预警时间")
    private long alarmTime;

    @ApiModelProperty(value = "处理时间")
    private long handleAt;

    @ApiModelProperty(value = "处理备注")
    private String remark;

    @ApiModelProperty(value = "订单id")
    private String ordid;

    @ApiModelProperty(value = "客户单位")
    private String customer;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "联系方式")
    private String phone;

    @ApiModelProperty(value = "产品名称")
    private String product;

    @ApiModelProperty(value = "设备sn码")
    private String snCode;

    @ApiModelProperty(value = "接单人")
    private String receiver;


    @ApiModelProperty(value = "配件名称")
    private String stockName;

    @ApiModelProperty(value = "配件数量")
    private Integer stockNumber;

    @ApiModelProperty(value = "项目名称")
    private String projectName;
}
