package com.thinglinks.uav.alarms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AlarmsCount {
    private String type;
    private Long count;
}
