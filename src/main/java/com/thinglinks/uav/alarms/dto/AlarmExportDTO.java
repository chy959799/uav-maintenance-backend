package com.thinglinks.uav.alarms.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class AlarmExportDTO {
    @ExcelProperty(value = "告警类型")
    private String type;
    @ExcelProperty(value = "告警细分")
    private String tag;
    @ExcelProperty(value = "警告内容")
    private String content;

    @ExcelProperty(value = "处理状态")
    private String status;

    @ExcelProperty(value = "处理结果")
    private String result;

    @ExcelProperty(value = "处理时间")
    private String handleAt;

    @ExcelProperty(value = "处理备注")
    private String remark;
}
