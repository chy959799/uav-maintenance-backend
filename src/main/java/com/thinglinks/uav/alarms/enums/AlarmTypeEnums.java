package com.thinglinks.uav.alarms.enums;

import lombok.Getter;

public enum AlarmTypeEnums {
    // 预警类型
    INSURANCETYPE("保险预警", "1"),
    MAINTAINTYPE("保养到期预警", "2"),
    SERVICETYPE("服务超时", "3"),
    INVENTORYTYPE("库存预警", "4"),
    PROJECTTYPE("项目预警", "5"),

    // 保险预警
    INSURANCE("insurance","保险预警"),
    INSURANCEISABOUTTOEXPIRE("insuranceIsAboutToExpire","保险即将到期预警"),
    INSURANCEEXPIRED("insuranceExpired","保险已到期预警"),

    // 保养预警
    MAINTENANCE("maintenance", "保养到期预警"),
    BASEMAINTENANCE("baseMaintenance", "基础保养"),
    ROUTINEMAINTENANCE("routineMaintenance", "常规保养"),
    INDEPTHMAINTENANCE("InDepthMaintenance", "深度保养"),

    // 服务预警
    INSURANCECONFIRM("insuranceConfirm", "保险待确认超时"),
    LOSSASSESSMENT("lossAssessment", "定损超时"),
    FIX("fix","维修超时"),
    EXPRESS("express", "寄件超时"),
    INSURANCEFIRSTWAITINGSUBMIT("insuranceFirstWaitingSubmit", "初版资料待提交超时"),
    FIRSTREJECT("firstReject", "初版资料驳回修改超时"),
    INSURANCELASTWAITINGSUBMIT("insuranceLastWaitingSubmit", "终版资料提交超时"),
    FEES("fees","预付款工单处理预警"),

    // 库存预警
    INVENTORY("inventory", "库存预警"),
    INVENTORYCONTENT("inventoryContent", "配件已低于安全库存设置"),

    // 项目预警
    PROJECT("project", "项目预警"),
    PROJECTISABOUTTOEXPIRE("projectIsAboutToExpire", "合同即将到期"),
    PROJECTEXPIRED("projectExpired", "合同已到期");

    @Getter
    private final String name;
    @Getter
    private final String remark;

    public static boolean isExist(String name) {
        for (AlarmTypeEnums alarmTypeEnums : AlarmTypeEnums.values()) {
            if (alarmTypeEnums.name.equals(name)) {
                return true;
            }
        }
        return false;
    }

    AlarmTypeEnums(String name, String remark) {
        this.name = name;
        this.remark = remark;
    }

    public static String getNameByRemark(String remark) {
        for (AlarmTypeEnums alarmTypeEnums : AlarmTypeEnums.values()) {
            if (alarmTypeEnums.remark.equals(remark)) {
                return alarmTypeEnums.name;
            }
        }
        return null;
    }
}
