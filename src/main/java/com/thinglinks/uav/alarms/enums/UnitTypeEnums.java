package com.thinglinks.uav.alarms.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

public enum UnitTypeEnums {
    MONTH("month", "月"),
    WEEK("week", "周"),
    DAY("day", "天"),
    HOUR("hour", "小时"),
    MINUTE("minute", "分钟");

    @Getter
    private final String code;
    @Getter
    private final String name;

    public static boolean exists(String code) {
        for (UnitTypeEnums unitTypeEnums : UnitTypeEnums.values()) {
            if (unitTypeEnums.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    UnitTypeEnums(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static UnitTypeEnums getUnitTypeEnums(String code) {
        for (UnitTypeEnums unitTypeEnums : UnitTypeEnums.values()) {
            if (unitTypeEnums.getCode().equals(code)) {
                return unitTypeEnums;
            }
        }
        return null;
    }

    public static Optional<String> getNameByCode(String code) {
        return Arrays.stream(values())
                .filter(unit -> unit.getCode().equals(code))
                .map(UnitTypeEnums::getName)
                .findFirst();
    }

}
