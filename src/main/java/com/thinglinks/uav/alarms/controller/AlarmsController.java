package com.thinglinks.uav.alarms.controller;

import com.thinglinks.uav.alarms.dto.*;
import com.thinglinks.uav.alarms.entity.AlarmsParameter;
import com.thinglinks.uav.alarms.service.AlarmsParameterService;
import com.thinglinks.uav.alarms.service.AlarmsService;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.IdsRequest;
import com.thinglinks.uav.user.dto.IdRequest;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "预警")
@RestController
@RequestMapping("alarms")
public class AlarmsController {

    @Resource
    private AlarmsService alarmsService;

    @Resource
    private AlarmsParameterService alarmsParameterService;

    @GetMapping
    @Operation(summary = "获取预警列表")
    public BasePageResponse<AlarmDTO> getAlarmsList(@Valid QueryAlarmDTO request) {
        return alarmsService.getPage(request);
    }

    @GetMapping("{alarmid}")
    @Operation(summary = "获取预警详情")
    public BaseResponse<AlarmDetailDTO> getAlarmsDetail(@PathVariable("alarmid") String alarmid) {
        return alarmsService.getAlarmsDetail(alarmid);
    }

    @GetMapping("/statistics")
    @Operation(summary = "获取预警数据统计")
    public BaseResponse<List<AlarmsCount>> getStatistics() {
        return alarmsService.getStatistics();
    }

    // 预警处理
    @PostMapping("{alarmid}")
    @Operation(summary = "预警处理")
    public BaseResponse<Void> alarmHandle(@PathVariable String alarmid,@RequestBody @Valid AlarmsHandleDTO request) {
        return alarmsService.alarmHandle(alarmid,request);
    }

    // 批量预警处理
    @PostMapping("/batchAlarmHandle")
    @Operation(summary = "批量预警处理")
    public BaseResponse<Void> batchAlarmHandle(@RequestBody @Valid IdsRequest ids) {
        return alarmsService.batchAlarmHandle(ids);
    }


    // 获取预警参数
    @GetMapping("/settings")
    @Operation(summary = "获取预警参数")
    public BaseResponse<List<AlarmsParameter>> getAlarmsParameters() {
        return alarmsParameterService.getAlarmsParameters();
    }

    // 修改预警参数
    @PatchMapping("/settings")
    @Operation(summary = "修改预警参数")
    public BaseResponse<Void> editAlarmsParameters(@RequestBody EditAlarmsParameterDTO request) {
        return alarmsParameterService.editAlarmsParameters(request);
    }

    @Operation(summary = "预警信息导出")
    @PostMapping("/exportAlarms")
    public void exportAlarms(@RequestBody @Valid IdRequest request, HttpServletResponse response) {
        alarmsService.exportAlarms(request, response);
    }
}
