package com.thinglinks.uav.alarms.task;

import com.thinglinks.uav.alarms.entity.Alarms;
import com.thinglinks.uav.alarms.entity.AlarmsParameter;
import com.thinglinks.uav.alarms.enums.AlarmTypeEnums;
import com.thinglinks.uav.alarms.enums.UnitTypeEnums;
import com.thinglinks.uav.alarms.repository.AlarmsParameterRepository;
import com.thinglinks.uav.alarms.repository.AlarmsRepository;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.category.repository.CategoryRepository;
import com.thinglinks.uav.common.constants.MessageConstants;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.insurance.entity.Insurance;
import com.thinglinks.uav.insurance.repository.InsuranceRepository;
import com.thinglinks.uav.message.service.MessageService;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.enums.InsuranceStatusEnums;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.repository.StockRepository;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AlarmsTask {
    private static final String INSURANCE_TYPE = "1";

    private static final String MAINTENANCE_TYPE = "2";

    private static final String SERVICE_TYPE = "3";

    private static final String STOCK_TYPE = "4";

    private static final String PROJECT_TYPE = "5";

    // 保险二次预警 30天
    private static final AlarmsParameter SECOND_INSURANCE_REMINDER_THRESHOLD = new AlarmsParameter(30, UnitTypeEnums.DAY.getCode());

    // 基础保养预警周期
    private static final AlarmsParameter BASE_MAINTENANCE_WARNING_THRESHOLD = new AlarmsParameter(6, UnitTypeEnums.MONTH.getCode());
    // 常规保养预警周期
    private static final AlarmsParameter REGULAR_MAINTENANCE_WARNING_THRESHOLD = new AlarmsParameter(12, UnitTypeEnums.MONTH.getCode());
    // 深度保养预警周期
    private static final AlarmsParameter DEEP_MAINTENANCE_WARNING_THRESHOLD = new AlarmsParameter(18, UnitTypeEnums.MONTH.getCode());

    // 项目二次预警 2个月
    private static final AlarmsParameter SECOND_PROJECT_REMINDER_THRESHOLD = new AlarmsParameter(2, UnitTypeEnums.MONTH.getCode());

    @Resource
    private AlarmsRepository alarmsRepository;

    @Resource
    private InsuranceRepository insuranceRepository;

    @Resource
    private AlarmsParameterRepository alarmsParameterRepository;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private MessageService messageService;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private CustomersRepository customersRepository;

    @Resource
    private ProductRepository productRepository;

    @Resource
    private CategoryRepository categoryRepository;


    // 保养到期预警
    @Transactional(rollbackFor = Exception.class)
    @Scheduled(fixedRate = 1800000)
    public void maintenanceExpirationWarning() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();

            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.MAINTENANCE.name(), false);

            List<Device> allDevice = deviceRepository.findAll();
            if (allDevice.size() > 0) {
                AlarmsParameter alarmsParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.MAINTENANCE.name());
                allDevice.forEach(device -> {
                    long baseTime = warningTime(device.getLastestMainteanceAt(), BASE_MAINTENANCE_WARNING_THRESHOLD);
                    long regularTime = warningTime(device.getLastestMainteanceAt(), REGULAR_MAINTENANCE_WARNING_THRESHOLD);
                    long deepTime = warningTime(device.getLastestMainteanceAt(), DEEP_MAINTENANCE_WARNING_THRESHOLD);
                    long b = (baseTime - now) / getUnitTimestamp(alarmsParameter.getUnit());
                    long r = (regularTime - now) / getUnitTimestamp(alarmsParameter.getUnit());
                    long d = (deepTime - now) / getUnitTimestamp(alarmsParameter.getUnit());
                    if (b > 0 && b % alarmsParameter.getValue() == 0) {
                        createMaintainAlarm(device, baseTime);
                    }
                    if (r > 0 && b % alarmsParameter.getValue() == 0) {
                        createMaintainAlarm(device, regularTime);
                    }
                    if (d > 0 && b % alarmsParameter.getValue() == 0) {
                        createMaintainAlarm(device, deepTime);
                    }
                });
            }
            stopWatch.stop();
            log.info("maintenanceExpirationWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("保养到期预警任务执行失败: {}", e.getMessage());
        }
    }

    // 保险到期预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void insuranceMaturityWarning() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.INSURANCEISABOUTTOEXPIRE.getRemark(), false);
            // 获取预警参数信息
            AlarmsParameter parameter = alarmsParameterRepository.findByName(AlarmTypeEnums.INSURANCE.name());
            Long warningTime = calculatedWarningTime(parameter);
            List<Insurance> activeInsurances = insuranceRepository.findActiveInsurances(now);
            // 二次预警参数
            Long secondWarningTime = calculatedWarningTime(SECOND_INSURANCE_REMINDER_THRESHOLD);
            if (warningTime != null && secondWarningTime != null) {
                List<Insurance> expiringDays = activeInsurances.stream()
                        .filter(insurance -> insurance.getInsuranceExpireAt() <= warningTime)
                        .collect(Collectors.toList());
                createInsuranceAlarm(expiringDays);

                if (warningTime > secondWarningTime) {
                    List<Insurance> secondExpiringDays = activeInsurances.stream()
                            .filter(insurance -> insurance.getInsuranceExpireAt() <= secondWarningTime)
                            .collect(Collectors.toList());
                    createInsuranceAlarm(secondExpiringDays);
                }
            } else {
                log.warn("Warning time is null, skipping insurance expiration early warning task.");
            }
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.INSURANCEEXPIRED.getRemark(), false);
            // 已过期保险
            List<Insurance> expiredInsurances = insuranceRepository.findExpiredInsurances(now);
            if (expiredInsurances.size() > 0) {
                expiredInsurances.forEach(this::createInsuranceExpiredAlarm);
            }
            stopWatch.stop();
            log.info("insuranceMaturityWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("保险到期预警任务执行失败: {}", e.getMessage());
        }
    }

    // 预付款工单预警 TODO 目前还没付款工单 待确认
    @Transactional(rollbackFor = Exception.class)
    @Scheduled(fixedRate = 360000)
    public void servicePrepaidTicketWarningTask() {
        try {

        } catch (Exception e) {
            log.error("预付款预警任务执行失败: {}", e.getMessage());
        }
    }


    // 服务--保险待确认超时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void insurancePendingConfirmationOfOvertimeWarning() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.INSURANCECONFIRM.getRemark(), false);
            // 保险待确认超时
            List<Order> insuranceWaitConfirmList = orderRepository.findByStatus(OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
            if (insuranceWaitConfirmList.size() > 0) {
                AlarmsParameter alarmsParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.INSURANCECONFIRM.name());
                insuranceWaitConfirmList.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(alarmsParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % alarmsParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(alarmsParameter.getUnit()).get();
                        createInsuranceWaitConfirmAlarm(order, timeOut);
                    }
                });
            }
            stopWatch.stop();
            log.info("insurancePendingConfirmationOfOvertimeWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务--保险待确认超时预警任务执行失败: {}", e.getMessage());
        }
    }

    // 服务--定损超时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void lossAssessmentTimeoutWarning() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            List<Order> waitConfirmLossList = orderRepository.findByStatus(OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
            if (waitConfirmLossList.size() > 0) {
                AlarmsParameter lossAssessmentParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.LOSSASSESSMENT.name());
                waitConfirmLossList.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(lossAssessmentParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % lossAssessmentParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(lossAssessmentParameter.getUnit()).get();
                        String lossUid = order.getLossUid();
                        User user = userRepository.findByUid(lossUid);
                        createLossAssessmentAlarm(order, timeOut, user.getUserName());
                    }
                });
            }
            stopWatch.stop();
            log.info("lossAssessmentTimeoutWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务--定损超时预警任务执行失败: {}", e.getMessage());
        }
    }

    // 服务--维修超时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void maintenanceTimeoutWarning() {
        // 维修超时提醒
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.FIX.getRemark(), false);
            List<Order> maintainingList = orderRepository.findByStatus(OrderStatusEnums.ORDER_STATUS_MAINTAINING.getCode());
            if (maintainingList.size() > 0) {
                AlarmsParameter fixParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.FIX.name());
                maintainingList.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(fixParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % fixParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(fixParameter.getUnit()).get();
                        String maintainerUid = order.getMaintainerUid();
                        User user = userRepository.findByUid(maintainerUid);
                        createFixAlarm(order, timeOut, user.getUserName());
                    }
                });
            }
            stopWatch.stop();
            log.info("maintenanceTimeoutWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务--维修超时预警任务执行失败: {}", e.getMessage());
        }
    }

    // 服务--寄件时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void warningWhenShipping() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.EXPRESS.getRemark(), false);
            // 寄件超时
            List<Order> waitExpressesList = orderRepository.findByStatus(OrderStatusEnums.ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode());
            if (waitExpressesList.size() > 0) {
                AlarmsParameter expressAlarmsParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.EXPRESS.name());
                waitExpressesList.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(expressAlarmsParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % expressAlarmsParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(expressAlarmsParameter.getUnit()).get();
                        String returnSendUid = order.getReturnSendUid();
                        User user = userRepository.findByUid(returnSendUid);
                        createExpressAlarm(order, timeOut, user.getUserName());
                    }
                });
            }
            stopWatch.stop();
            log.info("warningWhenShipping spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务-寄件超时预警任务执行失败: {}", e.getMessage());
        }
    }

    // 服务--初版资料提交超时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void timeoutWarningForTheSubmissionOfTheFirstVersionOfTheData() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.INSURANCEFIRSTWAITINGSUBMIT.getRemark(), false);
            List<Order> firstWaitingSubmit = orderRepository.findByReviewInsuranceStatus(InsuranceStatusEnums.INSURANCE_FIRST_WAITING_SUBMIT.getCode());
            if (firstWaitingSubmit.size() > 0) {
                AlarmsParameter firstAlarmsParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.INSURANCEFIRSTWAITINGSUBMIT.name());
                firstWaitingSubmit.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(firstAlarmsParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % firstAlarmsParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(firstAlarmsParameter.getUnit()).get();
                        String uid = order.getUid();
                        User user = userRepository.findByUid(uid);
                        createFirstWaitingSubmitAlarm(order, timeOut, user.getUserName());
                    }
                });
            }
            stopWatch.stop();
            log.info("TimeoutWarningForTheSubmissionOfTheFirstVersionOfTheData spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务-初版保险资料不齐超时预警任务执行失败: {}", e.getMessage());
        }
    }

    // 服务--初版驳回修改提交超时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void timeoutWarningForTheSubmissionOfTheFinalVersionOfTheData() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.FIRSTREJECT.getRemark(), false);
            // 初版保险资料驳回修改超时
            List<Order> firstRejectList = orderRepository.findByReviewInsuranceStatus(InsuranceStatusEnums.INSURANCE_FIRST_REJECT.getCode());
            if (firstRejectList.size() > 0) {
                AlarmsParameter firstRejectParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.FIRSTREJECT.name());
                firstRejectList.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(firstRejectParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % firstRejectParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(firstRejectParameter.getUnit()).get();
                        String uid = order.getUid();
                        User user = userRepository.findByUid(uid);
                        createFirstRejectAlarm(order, timeOut, user.getUserName());
                    }
                });
            }
            stopWatch.stop();
            log.info("timeoutWarningForTheSubmissionOfTheFinalVersionOfTheData spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务-初版保险资料驳回修改超时预警任务执行失败: {}", e.getMessage());
        }
    }

    // 服务--终版资料提交超时预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void timeoutWarningForFinalDataSubmission() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();

            // 终版资料不齐超时预警
            List<Order> lastWaitingSubmitList = orderRepository.findByReviewInsuranceStatus(InsuranceStatusEnums.INSURANCE_LAST_WAITING_SUBMIT.getCode());
            if (lastWaitingSubmitList.size() > 0) {
                AlarmsParameter lastWaitingSubmitParameter = alarmsParameterRepository.findByName(AlarmTypeEnums.INSURANCELASTWAITINGSUBMIT.name());
                lastWaitingSubmitList.forEach(order -> {
                    long daysDifference = (now - order.getUt()) / getUnitTimestamp(lastWaitingSubmitParameter.getUnit());
                    if (daysDifference != 0 && daysDifference % lastWaitingSubmitParameter.getValue() == 0) {
                        String timeOut = daysDifference + UnitTypeEnums.getNameByCode(lastWaitingSubmitParameter.getUnit()).get();
                        String uid = order.getUid();
                        User user = userRepository.findByUid(uid);
                        createLastWaitingSubmitAlarm(order, timeOut, user.getUserName());
                    }
                });
            }

            stopWatch.stop();
            log.info("timeoutWarningForFinalDataSubmission spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("服务--终版资料提交超时预警任务执行失败: {}", e.getMessage());
        }

    }

    // 配件库存预警
    @Transactional(rollbackFor = Exception.class)
    @Scheduled(fixedRate = 30000)
    public void accessoriesInventoryWarning() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            // 删除未处理的库存预警 重新生成预警信息 保证其他地方处理了然而还存在库存预警
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.INVENTORYCONTENT.getRemark(), false);
            List<Stock> stocksWithLowCount = stockRepository.findStocksWithLowCount();
            if (stocksWithLowCount.size() > 0) {
                stocksWithLowCount.forEach(stock -> {
                    createStockEarlyWarningAlarm(stock.getStcid(), stock.getPName(), stock.getCount(), stock.getMinLimit());
                });
            }
            stopWatch.stop();
            log.info("accessoriesInventoryWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("配件库存预警任务执行失败: {}", e.getMessage());
        }
    }

    // 项目到期预警
    @Scheduled(fixedRate = 1800000)
    @Transactional(rollbackFor = Exception.class)
    public void projectExpirationWarning() {
        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            long now = System.currentTimeMillis();
            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.PROJECTISABOUTTOEXPIRE.getRemark(), false);
            // 获取首次预警参数信息
            AlarmsParameter parameter = alarmsParameterRepository.findByName(AlarmTypeEnums.PROJECT.name());
            String validity = parameter.getValue() + UnitTypeEnums.getNameByCode(parameter.getUnit()).get();
            Long warningTime = calculatedWarningTime(parameter);
            List<Project> activeProjects = projectRepository.findActiveProjects(now);
            // 二次预警参数
            Long secondWarningTime = calculatedWarningTime(SECOND_PROJECT_REMINDER_THRESHOLD);
            String secondValidity = SECOND_PROJECT_REMINDER_THRESHOLD.getValue() + UnitTypeEnums.getNameByCode(SECOND_PROJECT_REMINDER_THRESHOLD.getUnit()).get();
            if (warningTime != null && secondWarningTime != null) {
                List<Project> expiringDays = activeProjects.stream()
                        .filter(project -> project.getEndTime() <= warningTime)
                        .collect(Collectors.toList());
                createProjectAlarm(expiringDays, validity);
                if (warningTime > secondWarningTime) {
                    List<Project> secondExpiringDays = activeProjects.stream()
                            .filter(project -> project.getEndTime() <= secondWarningTime)
                            .collect(Collectors.toList());
                    createProjectAlarm(secondExpiringDays, secondValidity);
                }
            } else {
                log.warn("Warning time is null, skipping project expiration early warning task.");
            }

            alarmsRepository.deleteAllByTagAndStatus(AlarmTypeEnums.PROJECTEXPIRED.getRemark(), false);
            // 已经到期项目
            List<Project> expiredProjects = projectRepository.findExpiredProjects(now);
            if (!expiredProjects.isEmpty()) {
                expiredProjects.forEach(this::createProjectExpiredAlarm);
            }
            stopWatch.stop();
            log.info("projectExpirationWarning spend: {}ms", stopWatch.getLastTaskTimeMillis());
        } catch (Exception e) {
            log.error("项目预警任务执行失败: {}", e.getMessage());
        }
    }

    // 生成保险到期预警信息
    private void createInsuranceAlarm(List<Insurance> insurances) {
        insurances.forEach(insurance -> {
            Alarms alarm = new Alarms();
            alarm.setAlarmid(CommonUtils.uuid());
            alarm.setType(INSURANCE_TYPE);
            alarm.setContent(generateInsuranceEarlyWarningInformation(insurance.getIid()));
            alarm.setStatus(false);
            alarm.setTag(AlarmTypeEnums.INSURANCEISABOUTTOEXPIRE.getRemark());
            alarm.setIid(insurance.getIid());
            alarmsRepository.save(alarm);
        });
    }

    // 生成保险过期预警
    private void createInsuranceExpiredAlarm(Insurance insurance) {
        Alarms alarms = new Alarms();
        alarms.setAlarmid(CommonUtils.uuid());
        alarms.setType(INSURANCE_TYPE);
        alarms.setContent(generateInsuranceExpiredWarningInformation(insurance.getIid()));
        alarms.setStatus(false);
        alarms.setTag(AlarmTypeEnums.INSURANCEEXPIRED.getRemark());
        alarms.setIid(insurance.getIid());
        alarmsRepository.save(alarms);
        String uid = insurance.getUid();
        List<String> uids = new ArrayList<>();
        uids.add(uid);
        messageService.sendNormalMessage("保险到期提醒", generateInsuranceExpiredWarningInformation(insurance.getIid()),
                MessageConstants.LEVEL_PERSONAL, uids,null);
    }

    // 生成项目即将到期预警信息
    private void createProjectAlarm(List<Project> projects, String validityPeriod) {
        projects.forEach(project -> {
            Alarms alarm = new Alarms();
            alarm.setAlarmid(CommonUtils.uuid());
            alarm.setType(PROJECT_TYPE);
            alarm.setContent(generateProjectWarningInformation(project.getPjid(), validityPeriod));
            alarm.setStatus(false);
            alarm.setTag(AlarmTypeEnums.PROJECTISABOUTTOEXPIRE.getRemark());
            alarm.setPjid(project.getPjid());
            alarmsRepository.save(alarm);
        });
    }

    // 生成项目已到期预警信息
    private void createProjectExpiredAlarm(Project project) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(PROJECT_TYPE);
        alarm.setContent(generateProjectExpiredInformation(project.getPjid()));
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.PROJECTEXPIRED.getRemark());
        alarm.setPjid(project.getPjid());
        alarmsRepository.save(alarm);
    }

    // 生成保险待确认超时预警信息
    private void createInsuranceWaitConfirmAlarm(Order order, String timeOut) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(SERVICE_TYPE);
        alarm.setContent(generateInsuranceWaitConfirmWarningInformation(order, AlarmTypeEnums.INSURANCECONFIRM.getRemark(), timeOut));
        alarm.setOrdid(order.getOrdid());
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.INSURANCECONFIRM.getRemark());
        alarmsRepository.save(alarm);
    }

    // 生成定损超时预警信息
    private void createLossAssessmentAlarm(Order order, String timeOut, String userName) {
        if (!alarmsRepository.existsByContentAndStatus(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.LOSSASSESSMENT.getRemark(), timeOut, userName), false)) {
            Alarms alarm = new Alarms();
            alarm.setAlarmid(CommonUtils.uuid());
            alarm.setType(SERVICE_TYPE);
            alarm.setContent(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.LOSSASSESSMENT.getRemark(), timeOut, userName));
            alarm.setOrdid(order.getOrdid());
            alarm.setStatus(false);
            alarm.setTag(AlarmTypeEnums.LOSSASSESSMENT.getRemark());
            // 写入新预警信息之前 删除该工单ID的旧预警信息 保证最大深度的预警信息
            if (alarmsRepository.existsByOrdid(order.getOrdid())) {
                alarmsRepository.deleteByOrdid(order.getOrdid());
            }
            alarmsRepository.save(alarm);
        }
    }

    // 生成维修超时预警信息
    private void createFixAlarm(Order order, String validity, String name) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(SERVICE_TYPE);
        alarm.setContent(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.FIX.getRemark(), validity, name));
        alarm.setOrdid(order.getOrdid());
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.FIX.getRemark());
        alarmsRepository.save(alarm);
    }

    // 生成寄件超时预警信息
    private void createExpressAlarm(Order order, String validity, String userName) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(SERVICE_TYPE);
        alarm.setContent(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.EXPRESS.getRemark(), validity, userName));
        alarm.setOrdid(order.getOrdid());
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.EXPRESS.getRemark());
        alarmsRepository.save(alarm);
    }

    // 生成初版资料提交超时预警信息
    private void createFirstWaitingSubmitAlarm(Order order, String validity, String userName) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(SERVICE_TYPE);
        alarm.setContent(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.INSURANCEFIRSTWAITINGSUBMIT.getRemark(), validity, userName));
        alarm.setOrdid(order.getOrdid());
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.INSURANCEFIRSTWAITINGSUBMIT.getRemark());
        alarmsRepository.save(alarm);
    }

    // 生成初版驳回修改超时预警信息
    private void createFirstRejectAlarm(Order order, String validity, String userName) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(SERVICE_TYPE);
        alarm.setContent(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.FIRSTREJECT.getRemark(), validity, userName));
        alarm.setOrdid(order.getOrdid());
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.FIRSTREJECT.getRemark());
        alarmsRepository.save(alarm);
    }

    // 生成终版资料提交超时预警信息
    private void createLastWaitingSubmitAlarm(Order order, String validity, String userName) {
        if (!alarmsRepository.existsByContentAndStatus(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.INSURANCELASTWAITINGSUBMIT.getRemark(), validity, userName), false)) {
            Alarms alarm = new Alarms();
            alarm.setAlarmid(CommonUtils.uuid());
            alarm.setType(SERVICE_TYPE);
            alarm.setContent(generateOrderAlarmWithNameInformation(order, AlarmTypeEnums.INSURANCELASTWAITINGSUBMIT.getRemark(), validity, userName));
            alarm.setOrdid(order.getOrdid());
            alarm.setStatus(false);
            alarm.setTag(AlarmTypeEnums.INSURANCELASTWAITINGSUBMIT.getRemark());
            // 写入新预警信息之前 删除该工单ID的旧预警信息 保证最大深度的预警信息
            if (alarmsRepository.existsByOrdid(order.getOrdid())) {
                alarmsRepository.deleteByOrdid(order.getOrdid());
            }
            alarmsRepository.save(alarm);
        }
    }

    // 生成备件库存预警信息
    private void createStockEarlyWarningAlarm(String stcid, String pName, Integer count, Integer minLimit) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(STOCK_TYPE);
        alarm.setContent(generateStockEarlyWarningAlarmInformation(pName, count, minLimit));
        alarm.setTag(AlarmTypeEnums.INVENTORYCONTENT.getRemark());
        alarm.setStatus(false);
        alarm.setTag(AlarmTypeEnums.INVENTORYCONTENT.getRemark());
        alarm.setStcid(stcid);
        alarmsRepository.deleteByStcid(stcid);
        alarmsRepository.save(alarm);

    }

    // 生成保养预警信息
    private void createMaintainAlarm(Device device, long time) {
        Alarms alarm = new Alarms();
        alarm.setAlarmid(CommonUtils.uuid());
        alarm.setType(MAINTENANCE_TYPE);
        alarm.setContent(generateMaintainAlarmInformation(device, time));
        alarm.setTag(AlarmTypeEnums.MAINTENANCE.getRemark());
        alarm.setStatus(false);
        alarm.setDevid(device.getDevid());
        alarmsRepository.save(alarm);
    }

    private static Long warningTime(long lastTime, AlarmsParameter parameter) {
        if (parameter.getUnit().equals(UnitTypeEnums.MONTH.getCode())) {
            return calculateMonthsLater(lastTime, parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.WEEK.getCode())) {
            return calculateWeeksLater(lastTime, parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.DAY.getCode())) {
            return calculateDaysLater(lastTime, parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.HOUR.getCode())) {
            return calculateHoursLater(lastTime, parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.MINUTE.getCode())) {
            return calculateMinutesLater(lastTime, parameter.getValue());
        }
        return null;
    }

    private static Long calculatedWarningTime(AlarmsParameter parameter) {
        if (parameter.getUnit().equals(UnitTypeEnums.MONTH.getCode())) {
            return calculateMonthsLater(System.currentTimeMillis(), parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.WEEK.getCode())) {
            return calculateWeeksLater(System.currentTimeMillis(), parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.DAY.getCode())) {
            return calculateDaysLater(System.currentTimeMillis(), parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.HOUR.getCode())) {
            return calculateHoursLater(System.currentTimeMillis(), parameter.getValue());
        }
        if (parameter.getUnit().equals(UnitTypeEnums.MINUTE.getCode())) {
            return calculateMinutesLater(System.currentTimeMillis(), parameter.getValue());
        }
        return null;
    }


    private static long calculateMonthsLater(long now, int months) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(now);
        calendar.add(Calendar.MONTH, months);
        return calendar.getTimeInMillis();
    }

    private static long calculateWeeksLater(long now, int weeks) {
        return now + weeks * 7 * 24 * 60 * 60 * 1000L;
    }


    private static long calculateDaysLater(long now, int days) {
        return now + days * 24 * 60 * 60 * 1000L;
    }

    private static long calculateHoursLater(long now, int hours) {
        return now + hours * 60 * 60 * 1000L;
    }

    private static long calculateMinutesLater(long now, int minutes) {
        return now + minutes * 60 * 1000L;
    }

    private static long getUnitTimestamp(String unit) {
        try {
            UnitTypeEnums type = UnitTypeEnums.getUnitTypeEnums(unit);
            switch (type) {
                case MONTH:
                    return 30L * 1000 * 60 * 60 * 24;
                case WEEK:
                    return 7L * 1000 * 60 * 60 * 24;
                case DAY:
                    return 1000 * 60 * 60 * 24;
                case HOUR:
                    return 1000 * 60 * 60;
                case MINUTE:
                    return 1000 * 60;
                default:
                    throw new IllegalArgumentException("Unsupported unit: " + unit);
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Unsupported unit: " + unit, e);
        }
    }


    public String generateInsuranceEarlyWarningInformation(String iid) {
        Insurance insurance = insuranceRepository.findByIid(iid);
        Device device = deviceRepository.findByInsuranceCode(insurance.getInsuranceCode());
        Product product = productRepository.findByPid(device.getPid());
        Category category = categoryRepository.findByCatid(product.getCatid());
        Customer customer = customersRepository.findByCid(device.getCid());
        Date date = new Date(insurance.getInsuranceExpireAt());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = sdf.format(date);
        return "单位 【" + customer.getName() + "】 的设备【" + category.getName() + "】【" + product.getName() + "】【" + product.getCode() + "】保险将于" + formattedDate + "到期，请及时联系客户进行保养！";
    }

    public String generateInsuranceExpiredWarningInformation(String iid) {
        Insurance insurance = insuranceRepository.findByIid(iid);
        Device device = deviceRepository.findByInsuranceCode(insurance.getInsuranceCode());
        Product product = productRepository.findByPid(device.getPid());
        Category category = categoryRepository.findByCatid(product.getCatid());
        Customer customer = customersRepository.findByCid(device.getCid());
        return "单位 【" + customer.getName() + "】 的设备【" + category.getName() + "】【" + product.getName() + "】【" + product.getCode() + "】保险已经过期";
    }

    public String generateProjectWarningInformation(String pjid, String validity) {
        Project project = projectRepository.findByPjid(pjid);
        Date date = new Date(project.getEndTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = sdf.format(date);
        return "项目 【" + project.getName() + "】 将于【" + formattedDate + "】到期，请即时联系客户续签项目！";

//        validity = "距离合同到期剩余【" + validity + "】";
//        Project project = projectRepository.findByPjid(pjid);
//        if (project != null) {
//            return messageService.generateProjectAlarmContent(project, validity);
//        }
//        return "";
    }

    public String generateProjectExpiredInformation(String pjid) {
        Project project = projectRepository.findByPjid(pjid);
        return "项目 【" + project.getName() + "】 已经到期！";
    }

    public String generateInsuranceWaitConfirmWarningInformation(Order order, String tag, String validity) {
        return messageService.generateOrderAlarm(order, tag, validity);
    }

    public String generateOrderAlarmWithNameInformation(Order order, String tag, String validity, String name) {
        return messageService.generateOrderAlarmWithName(order, tag, validity, name);
    }

    public String generateStockEarlyWarningAlarmInformation(String pName, Integer count, Integer minLimit) {
        return "备件 【" + pName + "】 库存数量【" + count + "】,已经小于下限值【" + minLimit + "】请即时采购！";
    }

    public String generateMaintainAlarmInformation(Device device, long time) {
        Instant instant = Instant.ofEpochMilli(time);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDate = instant.atZone(ZoneId.systemDefault()).format(formatter);
        Customer customer = customersRepository.findByCid(device.getCid());
        Product product = productRepository.findByPid(device.getPid());
        Category category = categoryRepository.findByCatid(product.getCatid());
        return "单位【" + customer.getName() + "】的设备【" + category.getName() + "】【" + product.getName() + "】【" + product.getCatid() + "】下一次保养时间为：" + formattedDate + "，请及时联系客户进行保养！";
    }
}
