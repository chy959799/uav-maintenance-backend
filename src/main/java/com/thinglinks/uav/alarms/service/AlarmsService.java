package com.thinglinks.uav.alarms.service;

import com.thinglinks.uav.alarms.dto.*;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.IdsRequest;
import com.thinglinks.uav.user.dto.IdRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


public interface AlarmsService {
    BasePageResponse<AlarmDTO> getPage(QueryAlarmDTO request);

    BaseResponse<List<AlarmsCount>> getStatistics();

    BaseResponse<Void> alarmHandle(String alarmid,AlarmsHandleDTO alarmsHandleDTO);

    BaseResponse<Void> batchAlarmHandle(IdsRequest ids);

    BaseResponse<AlarmDetailDTO> getAlarmsDetail(String alarmid);

    void exportAlarms(IdRequest alarmIds, HttpServletResponse response);
}
