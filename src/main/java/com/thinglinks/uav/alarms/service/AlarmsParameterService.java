package com.thinglinks.uav.alarms.service;


import com.thinglinks.uav.alarms.dto.EditAlarmsParameterDTO;
import com.thinglinks.uav.alarms.entity.AlarmsParameter;
import com.thinglinks.uav.common.dto.BaseResponse;

import java.util.List;

public interface AlarmsParameterService {

    BaseResponse<List<AlarmsParameter>> getAlarmsParameters();

    BaseResponse<Void> editAlarmsParameters(EditAlarmsParameterDTO request);
}
