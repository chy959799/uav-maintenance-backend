package com.thinglinks.uav.alarms.service.impl;

import com.thinglinks.uav.alarms.dto.EditAlarmsParameterDTO;
import com.thinglinks.uav.alarms.entity.AlarmsParameter;
import com.thinglinks.uav.alarms.enums.AlarmTypeEnums;
import com.thinglinks.uav.alarms.enums.UnitTypeEnums;
import com.thinglinks.uav.alarms.repository.AlarmsParameterRepository;
import com.thinglinks.uav.alarms.service.AlarmsParameterService;
import com.thinglinks.uav.common.dto.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;


@Service
@Slf4j
public class AlarmsParameterServiceImpl implements AlarmsParameterService {

    @Resource
    private AlarmsParameterRepository alarmsParameterRepository;


    @Override
    public BaseResponse<List<AlarmsParameter>> getAlarmsParameters() {
        List<AlarmsParameter> alarmsParameters = alarmsParameterRepository.findAll();
        return BaseResponse.success(alarmsParameters);
    }

    @Override
    public BaseResponse<Void> editAlarmsParameters(EditAlarmsParameterDTO request) {
        for (AlarmsParameter alarmsParameter : request.getAlarmsParameters()) {
            if (Objects.nonNull(alarmsParameter.getName()) && Objects.nonNull(alarmsParameter.getUnit()) && Objects.nonNull(alarmsParameter.getValue()) && AlarmTypeEnums.isExist(alarmsParameter.getName()) && UnitTypeEnums.exists(alarmsParameter.getUnit())){
                AlarmsParameter parameter = alarmsParameterRepository.findByName(alarmsParameter.getName());
                parameter.setName(alarmsParameter.getName());
                parameter.setUnit(alarmsParameter.getUnit());
                parameter.setValue(alarmsParameter.getValue());
                alarmsParameterRepository.save(parameter);
            }else {
                return BaseResponse.fail("预警参数名称或单位错误");
            }
        }
        return BaseResponse.success();
    }
}
