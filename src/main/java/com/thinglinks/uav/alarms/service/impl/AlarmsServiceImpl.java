package com.thinglinks.uav.alarms.service.impl;

import com.alibaba.excel.EasyExcel;
import com.thinglinks.uav.alarms.dto.*;
import com.thinglinks.uav.alarms.entity.Alarms;
import com.thinglinks.uav.alarms.enums.AlarmTypeEnums;
import com.thinglinks.uav.alarms.repository.AlarmsRepository;
import com.thinglinks.uav.alarms.service.AlarmsService;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.IdsRequest;
import com.thinglinks.uav.common.enums.InsuranceEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.departments.repository.DepartmentsRepository;
import com.thinglinks.uav.insurance.entity.Insurance;
import com.thinglinks.uav.insurance.repository.InsuranceRepository;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.repository.StockRepository;
import com.thinglinks.uav.user.dto.IdRequest;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AlarmsServiceImpl implements AlarmsService {

    @Resource
    private AlarmsRepository alarmsRepository;

    @Resource
    private InsuranceRepository insuranceRepository;

    @Resource
    private OrderRepository orderRepository;
    @Resource
    private UserRepository userRepository;

    @Resource
    private CustomersRepository customersRepository;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private ProductRepository productRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private DepartmentsRepository departmentsRepository;

    /**
     * @param request
     * @return
     */
    @Override
    public BasePageResponse<AlarmDTO> getPage(QueryAlarmDTO request) {
        PageRequest pageRequest = request.of();
        Specification<Alarms> specification = (root, criteriaQuery, criteriaBuilder) -> {
            ArrayList<Predicate> predicates = new ArrayList<>();
            if (Objects.nonNull(request.getType())) {
                predicates.add(criteriaBuilder.equal(root.get("type"), request.getType()));
            }
            if (Objects.nonNull(request.getStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
            }
            if (request.getStartTime() > 0 && request.getEndTime() > 0) {
                predicates.add(criteriaBuilder.between(root.get("ut"), request.getStartTime(), request.getEndTime()));
            }
            if (StringUtils.isNotBlank(request.getTag())) {
                predicates.add(criteriaBuilder.equal(root.get("tag"), request.getTag()));
            }
            Predicate[] p = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(p));
        };
        Page<Alarms> page = alarmsRepository.findAll(specification, pageRequest);
        List<AlarmDTO> result = page.stream().map(alarm -> {
            AlarmDTO alarmDTO = new AlarmDTO();
            alarmDTO.setAlarmTime(alarm.getCt());
            Optional.ofNullable(alarm.getHandleAt()).ifPresent(alarmDTO::setHandleAt);
            alarmDTO.setAlarmid(alarm.getAlarmid());
            alarmDTO.setAlarmType(AlarmTypeEnums.getNameByRemark(alarm.getType()));
            alarmDTO.setContent(alarm.getContent());
            alarmDTO.setStatus(alarm.getStatus());
            alarmDTO.setResult(alarm.getResult());
            alarmDTO.setRemark(alarm.getRemark());
            alarmDTO.setTag(alarm.getTag());
            if (!StringUtils.isBlank(alarm.getIid())) {
                getInsuranceInformation(alarmDTO, alarm);
            }
            if (!StringUtils.isBlank(alarm.getOrdid())) {
                getOrderInformation(alarmDTO, alarm);
            }
            if (!StringUtils.isBlank(alarm.getStcid())) {
                getStockInformation(alarmDTO, alarm);
            }
            if (!StringUtils.isBlank(alarm.getPjid())) {
                getProjectInformation(alarmDTO, alarm);
            }
            return alarmDTO;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, result);
    }

    @Override
    public BaseResponse<List<AlarmsCount>> getStatistics() {
        List<Object[]> objects = alarmsRepository.countAlarmsByTypeWithUntreated();
        List<AlarmsCount> collect = objects.stream().map(o -> new AlarmsCount((String) o[0], (Long) o[1])).collect(Collectors.toList());
        return BaseResponse.success(collect);
    }

    @Override
    public BaseResponse<Void> alarmHandle(String alarmid, AlarmsHandleDTO request) {
        Alarms alarm = alarmsRepository.findByAlarmidAndType(alarmid, request.getType());
        if (Objects.nonNull(request.getRemark())) {
            alarm.setRemark(request.getRemark());
        }
        alarm.setStatus(true);
        alarm.setHandleAt(System.currentTimeMillis());
        alarm.setUid(CommonUtils.getUid());
        alarmsRepository.save(alarm);
        return BaseResponse.success();
    }

    /**
     * @param ids
     * @return
     */
    @Override
    public BaseResponse<Void> batchAlarmHandle(IdsRequest ids) {
        String uid = CommonUtils.getUid();
        ids.getIds().forEach(id -> {
            Alarms alarm = alarmsRepository.findByAlarmid(id);
            if (Objects.nonNull(alarm)) {
                alarm.setStatus(true);
                alarm.setHandleAt(System.currentTimeMillis());
                alarm.setUid(uid);
                alarmsRepository.save(alarm);
            }
        });
        return BaseResponse.success();
    }

    /**
     * @param alarmid
     * @return
     */
    @Override
    public BaseResponse<AlarmDetailDTO> getAlarmsDetail(String alarmid) {
        AlarmDetailDTO alarmDetailDTO = new AlarmDetailDTO();
        Alarms alarm = alarmsRepository.findByAlarmid(alarmid);
        if (Objects.nonNull(alarm)) {
            alarmDetailDTO.setContent(alarm.getContent());
            alarmDetailDTO.setAlarmTime(alarm.getCt());
            alarmDetailDTO.setStatus(alarm.getStatus());
            alarmDetailDTO.setAlarmType(AlarmTypeEnums.getNameByRemark(alarm.getType()));
            if (StringUtils.isNotBlank(alarm.getResult())) {
                alarmDetailDTO.setResult(alarm.getResult());
            }
            if (Objects.nonNull(alarm.getRemark())) {
                alarmDetailDTO.setRemark(alarm.getRemark());
            }
            if (Objects.nonNull(alarm.getHandleAt()) && alarm.getHandleAt() > 0) {
                alarmDetailDTO.setHandleAt(alarm.getHandleAt());
            }
            User user = userRepository.findByUid(alarm.getUid());
            if (Objects.nonNull(user) && StringUtils.isNotBlank(user.getDid())) {
                Department department = departmentsRepository.findByDid(user.getDid());
                alarmDetailDTO.setAgentDepartment(department.getName());
                alarmDetailDTO.setAgent(user.getUserName());
            }
            if (alarm.getType().equals(AlarmTypeEnums.INSURANCETYPE.getRemark())) {
                Insurance insurance = insuranceRepository.findByIid(alarm.getIid());
                alarmDetailDTO.setInsuranceType(InsuranceEnum.getNameByCode(insurance.getInsuranceType()));
                alarmDetailDTO.setInsuranceExpireAt(insurance.getInsuranceExpireAt());
            }
            return BaseResponse.success(alarmDetailDTO);
        }
        return BaseResponse.fail("预警信息不存在");
    }

    /**
     * @param alarmIds
     * @param response
     */
    @Override
    public void exportAlarms(IdRequest alarmIds, HttpServletResponse response) {
        List<Alarms> alarms = alarmsRepository.findByAlarmidIn(alarmIds.getIds());
        List<AlarmExportDTO> result = alarms.stream().map(alarm -> {
            AlarmExportDTO alarmExportDTO = new AlarmExportDTO();
            BeanUtils.copyProperties(alarm, alarmExportDTO);
            alarmExportDTO.setContent(removeTags(alarm.getContent()));
            String handleAt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(alarm.getCt());
            alarmExportDTO.setHandleAt(handleAt);
            if (alarm.getStatus()) {
                alarmExportDTO.setStatus("已处理");
            } else {
                alarmExportDTO.setStatus("待处理");
            }
            alarmExportDTO.setType(AlarmTypeEnums.getNameByRemark(alarm.getType()));
            return alarmExportDTO;
        }).collect(Collectors.toList());
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Transfer-Encoding", "binary");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
            String fileName = "预警信息" + ".xlsx";
            fileName = new String(fileName.getBytes(), "ISO-8859-1");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(AlarmExportDTO.class)
                    .sheet("导出列表")
                    .doWrite(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getInsuranceInformation(AlarmDTO alarmDTO, Alarms alarm) {
        Insurance insurance = insuranceRepository.findByIid(alarm.getIid());
        alarmDTO.setInsuranceCode(insurance.getInsuranceCode());
        alarmDTO.setInsuranceType(InsuranceEnum.getNameByCode(insurance.getInsuranceType()));
    }

    public void getOrderInformation(AlarmDTO alarmDTO, Alarms alarm) {
        Order order = orderRepository.findByOrdid(alarm.getOrdid());
        User user = userRepository.findByUid(order.getCuid());
        User receiveUser = userRepository.findByUid(order.getReceiveUid());
        if (Objects.nonNull(receiveUser)) {
            alarmDTO.setReceiver(receiveUser.getNickName());
        }
        Customer customer = customersRepository.findByCid(user.getCid());
        alarmDTO.setOrdid(order.getOrdid());
        alarmDTO.setCustomer(customer.getName());
        alarmDTO.setName(user.getNickName());
        alarmDTO.setPhone(user.getMobile());
        Device device = deviceRepository.findByDevid(order.getDevid());
        alarmDTO.setSnCode(device.getCode());
        Product product = productRepository.findByPid(device.getPid());
        alarmDTO.setProduct(product.getName());
    }

    public void getStockInformation(AlarmDTO alarmDTO, Alarms alarm) {
        Stock stock = stockRepository.findByStcid(alarm.getStcid());
        Product product = productRepository.findByPid(stock.getPid());
        alarmDTO.setStockName(product.getName());
        alarmDTO.setStockNumber(stock.getCount());
    }

    public void getProjectInformation(AlarmDTO alarmDTO, Alarms alarm) {
        Project project = projectRepository.findByPjid(alarm.getPjid());
        alarmDTO.setProjectName(project.getName());
    }

    public static String removeTags(String input) {
        String divPattern = "<div>";
        String divEndPattern = "</div>";
        String aPattern = "<a.*?>";
        String aEndPattern = "</a>";
        Pattern patternDiv = Pattern.compile(divPattern);
        Pattern patternDivEnd = Pattern.compile(divEndPattern);
        Pattern patternA = Pattern.compile(aPattern);
        Pattern patternAEnd = Pattern.compile(aEndPattern);
        Matcher matcherDiv = patternDiv.matcher(input);
        input = matcherDiv.replaceAll("");
        Matcher matcherDivEnd = patternDivEnd.matcher(input);
        input = matcherDivEnd.replaceAll("");
        Matcher matcherA = patternA.matcher(input);
        input = matcherA.replaceAll("");
        Matcher matcherAEnd = patternAEnd.matcher(input);
        input = matcherAEnd.replaceAll("");
        return input;
    }
}
