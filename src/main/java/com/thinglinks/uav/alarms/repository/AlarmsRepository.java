package com.thinglinks.uav.alarms.repository;

import com.thinglinks.uav.alarms.entity.Alarms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AlarmsRepository extends JpaRepository<Alarms, Integer>, JpaSpecificationExecutor<Alarms> {
    boolean existsByContentAndStatus(String content,boolean status);

    Alarms findByAlarmidAndType(String alarmid,String type);

    Alarms findByAlarmid(String alarmid);

    @Query("SELECT a.type, COUNT(a) FROM Alarms a WHERE a.status = false GROUP BY a.type")
    List<Object[]> countAlarmsByTypeWithUntreated();

    List<Alarms> findByAlarmidIn(List<String> alarmids);

    boolean existsByOrdid(String ordid);

    void deleteByIid(String iid);
    void deleteByOrdid(String ordid);
    void deleteByPjid(String pjid);
    void deleteByDevid(String devid);
    void deleteByStcid(String stcid);
    void deleteAllByTagAndStatus(String tag,boolean status);
}
