package com.thinglinks.uav.alarms.repository;


import com.thinglinks.uav.alarms.entity.AlarmsParameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AlarmsParameterRepository extends JpaRepository<AlarmsParameter, Integer>, JpaSpecificationExecutor<AlarmsParameter> {
    AlarmsParameter findByName(String name);
}
