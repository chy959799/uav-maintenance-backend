package com.thinglinks.uav.alarms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "alarms")
@Data
@TenantTable
public class Alarms extends BaseEntity {

    @Column(name = "alarmid", unique = true, columnDefinition = "告警内部ID 唯一")
    private String alarmid;

    @Column(name = "type", columnDefinition = "告警类型 1:保险到期 2:保养到期 3:服务超时 4:库存告警")
    @ApiModelProperty(value = "告警类型，1:保险到期，2:保养到期，3:服务超时，4:库存告警")
    private String type;

    @Column(name = "tag", columnDefinition = "列表展示的 预警类型 如保险即将到期、保险已到期 基础保养、常规保养、深度保养 保险待确认超时、定损超时、维修超时、寄件超时、初版保险资料不齐超时预警、终版资料不齐超时预警、预付款工单处理预警'")
    @ApiModelProperty(value = "告警内容")
    private String tag;

    @Column(name = "content", columnDefinition = "告警内容'")
    @ApiModelProperty(value = "告警内容")
    private String content;

    @Column(name = "status", columnDefinition = "'处理状态 true:已处理 false:未处理'")
    @ApiModelProperty(value = "处理状态，true:已处理，false:未处理")
    private Boolean status;

    @Column(name = "result", columnDefinition = "告警处理结果 1:保养 2:不保养 3:续保 4:不续保")
    @ApiModelProperty(value = "告警处理结果，1:保养，2:不保养，3:续保，4:不续保")
    private String result;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "handle_at", columnDefinition = "告警处理时间")
    @ApiModelProperty(value = "告警处理时间")
    private Long handleAt;

    @Column(name = "remark", columnDefinition = "处理备注")
    @ApiModelProperty(value = "处理备注")
    private String remark;

    @Column(name = "cpid", columnDefinition = "相关公司ID")
    @ApiModelProperty(value = "相关公司ID")
    private String cpid;

    @Column(name = "uid", columnDefinition = "预警处理 用户ID")
    @ApiModelProperty(value = "用户ID")
    private String uid;

    @Column(name = "devid", columnDefinition = "设备ID")
    @ApiModelProperty(value = "设备ID")
    private String devid;

    @Column(name = "stcid", columnDefinition = "库存ID")
    @ApiModelProperty(value = "库存ID")
    private String stcid;

    @Column(name = "ordid", columnDefinition = "订单ID")
    @ApiModelProperty(value = "订单ID")
    private String ordid;

    @Column(name = "iid", columnDefinition = "订单ID")
    @ApiModelProperty(value = "保险id")
    private String iid;

    @Column(name = "pjid", columnDefinition = "项目ID")
    @ApiModelProperty(value = "项目id")
    private String pjid;
}
