package com.thinglinks.uav.alarms.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "alarmsparameter")
@Data
public class AlarmsParameter extends BaseEntity {
    @Column(name = "name")
    private String name;
    @Column(name = "unit")
    private String unit;
    @Column(name = "value")
    private Integer value;
    @Column(name = "remark")
    private String remark;

    public AlarmsParameter() {
    }

    public AlarmsParameter(Integer value ,String unit) {
        this.unit = unit;
        this.value = value;
    }
}
