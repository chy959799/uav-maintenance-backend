package com.thinglinks.uav.insurance.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.insurance.dto.InsuranceDeviceDTO;
import com.thinglinks.uav.insurance.dto.InsuranceDevicePageQuery;
import com.thinglinks.uav.insurance.service.InsuranceDeviceService;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceInfoVO;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (InsuranceDevices)表控制层
 *
 * @author iwiFool
 * @since 2024-03-26 15:16:38
 */
@Api(tags = "保单设备管理")
@RestController
@RequestMapping("insuranceDevices")
public class InsuranceDeviceController {

	@Resource
	private InsuranceDeviceService insuranceDeviceService;

	@Authority(action = "insuranceDeviceDelete")
	@DeleteMapping("/{idid}")
	@Operation(summary = "删除保单设备")
	public BaseResponse<String> deleteInsuranceDevice(@PathVariable String idid) {
		return insuranceDeviceService.deleteInsuranceDevice(idid);
	}

	@Authority(action = "insuranceDeviceEdit")
	@PatchMapping("/{idid}")
	@Operation(summary = "修改保单设备")
	public BaseResponse<String> updateInsuranceDevice(@PathVariable String idid, @RequestBody InsuranceDeviceDTO dto) {
		return insuranceDeviceService.updateInsuranceDevice(idid, dto);
	}

	@GetMapping
	@Operation(summary = "分页查询保单设备")
	public BasePageResponse<InsuranceDeviceVO> getInsuranceDevicePage(InsuranceDevicePageQuery pageQuery) {
		return insuranceDeviceService.getInsuranceDevicePage(pageQuery);
	}

	@GetMapping("/{idid}")
	@Operation(summary = "获取保单设备详情")
	public BaseResponse<InsuranceDeviceInfoVO> getInsuranceDeviceInfo(@PathVariable String idid) {
		return insuranceDeviceService.getInsuranceInfo(idid);
	}
}


