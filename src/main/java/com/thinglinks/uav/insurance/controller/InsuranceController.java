package com.thinglinks.uav.insurance.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.insurance.dto.InsuranceDTO;
import com.thinglinks.uav.insurance.dto.InsurancePageQuery;
import com.thinglinks.uav.insurance.service.InsuranceService;
import com.thinglinks.uav.insurance.vo.InsuranceInfoVO;
import com.thinglinks.uav.insurance.vo.InsuranceVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * (Insurances)表控制层
 *
 * @author iwiFool
 * @since 2024-03-25 15:35:15
 */
@Api(tags = "保单管理")
@RestController
@RequestMapping("insurances")
public class InsuranceController {

	@Resource
	private InsuranceService insuranceService;

	@Authority(action = "insuranceCreate")
	@PostMapping
	@Operation(summary = "新增保单")
	public BaseResponse<String> insert(@RequestBody @Valid InsuranceDTO dto) {
		return insuranceService.insert(dto);
	}

	@Authority(action = "insuranceDelete")
	@DeleteMapping("/{iid}")
	@Operation(summary = "删除保单")
	public BaseResponse<String> delete(@PathVariable("iid") String iid) {
		return insuranceService.delete(iid);
	}

	@Authority(action = "insuranceEdit")
	@PatchMapping("/{iid}")
	@Operation(summary = "修改保单")
	public BaseResponse<String> update(@PathVariable("iid") String iid, @RequestBody @Valid InsuranceDTO dto) {
		return insuranceService.update(iid, dto);
	}

	@GetMapping("/{iid}")
	@Operation(summary = "获取保单详情")
	public BaseResponse<InsuranceInfoVO> getInsuranceInfo(@PathVariable String iid) {
		return insuranceService.getInsuranceInfo(iid);
	}

	@GetMapping
	@Operation(summary = "分页查询保单")
	public BasePageResponse<InsuranceVO> getInsurancePage(InsurancePageQuery pageQuery) {
		return insuranceService.getInsurancePage(pageQuery);
	}

	@Authority(action = "insuranceImport")
	@PostMapping(path = "import")
	@Operation(summary = "导入保单")
	public BaseResponse<String> importInsurance(@RequestPart(value = "file") MultipartFile file, HttpServletResponse response) throws IOException {
		return insuranceService.importInsurance(file, response);
	}

	@Authority(action = "insuranceExport")
	@PostMapping("/export")
	@Operation(summary = "导出保单")
	public void exportInsurance(@RequestBody List<String> iids, HttpServletResponse response) throws IOException {
		insuranceService.exportInsurance(iids, response);
	}
}


