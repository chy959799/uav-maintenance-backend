package com.thinglinks.uav.insurance.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/3/25
 */
@ApiModel(description = "保单VO")
@Data
public class InsuranceVO {

	@ApiModelProperty(value = "iid", example = "1")
	private String iid;

	@ApiModelProperty(value = "保单编号")
	private String insuranceCode;

	@ApiModelProperty(value = "保险类型[8:鼎和财险]", example = "8")
	private String insuranceType;

	@ApiModelProperty(value = "单位名称")
	private String customerName;

	@ApiModelProperty(value = "单位id")
	private String cid;

	@ApiModelProperty(value = "总承保金额")
	private BigDecimal totalAmount;

	@ApiModelProperty(value = "保险生效日期")
	private String insuranceStartAt;

	@ApiModelProperty(value = "保险失效日期")
	private String insuranceExpireAt;

	@ApiModelProperty(value = "添加人")
	private String userName;

	@ApiModelProperty(value = "添加时间")
	private String createdAt;
}
