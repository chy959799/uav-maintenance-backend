package com.thinglinks.uav.insurance.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/3/26
 */
@Data
@ApiModel(description = "导出数据VO")
public class InsuranceExportData {

	@ApiModelProperty(value = "保单编号")
	@ExcelProperty("保单编号")
	private String insuranceCode;

	@ApiModelProperty(value = "单位名称")
	@ExcelProperty("单位名称")
	private String customerName;

	@ApiModelProperty(value = "总承保金额")
	@ExcelProperty("总承保金额")
	private BigDecimal totalAmount;

	@ApiModelProperty(value = "保险生效日期")
	@ExcelProperty("保险生效日期")
	private String insuranceStartAt;

	@ApiModelProperty(value = "保险失效日期")
	@ExcelProperty("保险失效日期")
	private String insuranceExpireAt;

	@ApiModelProperty(value = "设备序列号")
	@ExcelProperty("设备SN码")
	@NotBlank(message = "设备序列号不能为空")
	private String code;

	@ApiModelProperty(value = "累计理赔金额")
	@ExcelProperty("累计理赔金额（元）")
	private BigDecimal amount;

	@ApiModelProperty(value = "保险注册号")
	@ExcelProperty("保险注册号")
	private String registrationNumber;
}
