package com.thinglinks.uav.insurance.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/25
 */
@ApiModel(description = "保单详情VO")
@Data
public class InsuranceInfoVO {

	@ApiModelProperty(value = "iid", example = "1")
	private String iid;

	@ApiModelProperty(value = "保单编号")
	private String insuranceCode;

	@ApiModelProperty(value = "保险类型[8:鼎和财险]", example = "8")
	private String insuranceType;

	@ApiModelProperty(value = "单位id")
	private String cid;

	@ApiModelProperty(value = "单位名称")
	private String customerName;

	@ApiModelProperty(value = "总承保金额")
	private BigDecimal totalAmount;

	@ApiModelProperty(value = "保险生效日期")
	private Long insuranceStartAt;

	@ApiModelProperty(value = "保险失效日期")
	private Long insuranceExpireAt;

	@ApiModelProperty(value = "保单设备列表")
	private List<InsuranceDevice> devices;

	@Data
	@ApiModel(description = "保单设备")
	public static class InsuranceDevice {

		@ApiModelProperty(value = "设备序列号")
		private String code;

		@ApiModelProperty(value = "保险注册号")
		private String registrationNumber;

		@ApiModelProperty(value = "累计理赔金额")
		private BigDecimal amount;
	}
}
