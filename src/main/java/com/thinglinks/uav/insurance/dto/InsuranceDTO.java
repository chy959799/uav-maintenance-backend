package com.thinglinks.uav.insurance.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/25
 */
@Data
@ApiModel(description = "保单DTO")
public class InsuranceDTO {

	@ApiModelProperty(value = "保单编号")
	@Pattern(regexp = "^[\\w\\s-]{1,64}$", message = "保单号格式错误")
	private String insuranceCode;

	@ApiModelProperty(value = "总承保金额")
	@Min(value = 0, message = "总承保金额不能为负值")
	private BigDecimal totalAmount;

	@ApiModelProperty(value = "保险生效日期")
	@NotNull(message = "保险生效日期不能为空")
	private Long insuranceStartAt;

	@ApiModelProperty(value = "保险失效日期")
	@NotNull(message = "保险失效日期不能为空")
	private Long insuranceExpireAt;

	@ApiModelProperty(value = "did")
	@NotBlank(message = "单位id不能为空")
	private String cid;

	@ApiModelProperty(value = "设备列表")
	private List<InsuranceDeviceDTO> devices;
}