package com.thinglinks.uav.insurance.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/3/25
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "设备DTO")
public class InsuranceDeviceDTO {

	@ApiModelProperty(value = "设备序列号")
	@NotBlank(message = "设备序列号不能为空")
	private String code;

	@ApiModelProperty(value = "保险注册号")
	@NotBlank(message = "保险注册号不能为空")
	private String registrationNumber;

	@ApiModelProperty(value = "保险金额（元）")
	private BigDecimal amount;
}
