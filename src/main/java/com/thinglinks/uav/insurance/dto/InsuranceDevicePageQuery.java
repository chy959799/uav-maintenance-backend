package com.thinglinks.uav.insurance.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/26
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class InsuranceDevicePageQuery extends BasePageRequest {

	@ApiModelProperty(value = "保单编号 ", example = "1")
	private String insuranceCode;
	@ApiModelProperty(value = "单位名称", example = "测试")
	private String name;
	@ApiModelProperty(value = "设备SN码")
	private String code;
	@ApiModelProperty(value = "保险注册号")
	private String registrationNumber;
}
