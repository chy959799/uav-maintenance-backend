package com.thinglinks.uav.insurance.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.thinglinks.uav.common.dto.BaseImportError;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/5/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class InsuranceDeviceImport extends BaseImportError {

    @ExcelProperty("设备SN码")
    private String code;

    @ExcelProperty("保险注册号")
    private String registrationNumber;

    @ExcelProperty("保险金额（元）")
    private BigDecimal amount;
}
