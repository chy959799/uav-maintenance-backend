package com.thinglinks.uav.insurance.service.impl;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.insurance.dto.InsuranceDeviceDTO;
import com.thinglinks.uav.insurance.dto.InsuranceDevicePageQuery;
import com.thinglinks.uav.insurance.entity.Insurance;
import com.thinglinks.uav.insurance.entity.InsuranceDevice;
import com.thinglinks.uav.insurance.repository.InsuranceDeviceRepository;
import com.thinglinks.uav.insurance.service.InsuranceDeviceService;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceInfoVO;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceVO;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.user.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * (InsuranceDevices)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-25 17:32:53
 */
@Service
public class InsuranceDeviceServiceImpl implements InsuranceDeviceService {

	@Resource
	private InsuranceDeviceRepository insuranceDeviceRepository;

	@Override
	public void insertAll(List<InsuranceDeviceDTO> devices, String iid) {
		if (devices == null || devices.isEmpty()) {
			return;
		}

		if (hasDuplicateDevices(devices)) {
			throw new BusinessException("设备编号或注册号不能重复");
		}

		List<String> codeList = new ArrayList<>();
		List<String> registrationNumberList = new ArrayList<>();

		for (InsuranceDeviceDTO dto : devices) {
			codeList.add(dto.getCode());
			registrationNumberList.add(dto.getRegistrationNumber());
		}

		if (existsInIid(codeList, registrationNumberList, iid)) {
			throw new BusinessException("设备编号或注册号已存在");
		}
		List<InsuranceDevice> list = devices.stream()
				.map(dto -> convertDTOToEntity(dto, iid))
				.collect(Collectors.toList());

		insuranceDeviceRepository.saveAll(list);
	}

	@Override
	public void insertAll(List<InsuranceDevice> devices) {
		insuranceDeviceRepository.saveAll(devices);
	}

	@Override
	public boolean hasDuplicateDevices(List<InsuranceDeviceDTO> devices) {
		if (devices == null || devices.isEmpty()){
			return false;
		}

		Set<String> serialNumbers = new HashSet<>();
		Set<String> registrationNumbers = new HashSet<>();

		for (InsuranceDeviceDTO device : devices) {
			// 检查序列号是否重复
			if (serialNumbers.contains(device.getCode())) {
				return true;
			}
			serialNumbers.add(device.getCode());

			// 检查注册号是否重复
			if (registrationNumbers.contains(device.getRegistrationNumber())) {
				return true;
			}
			registrationNumbers.add(device.getRegistrationNumber());
		}

		return false;
	}

	private boolean existsInIid(List<String> codes, List<String> registrationNumbers, String iid) {
		return insuranceDeviceRepository.existsByIidAndCodeInOrIidAndRegistrationNumberIn(iid, codes, iid, registrationNumbers);
	}

	@Override
	public Boolean existsInIid(String iid, String code, String registrationNumber) {
		return insuranceDeviceRepository.existsByIidAndCodeAndRegistrationNumber(iid, code, registrationNumber);
	}


	@Override
	public void deleteAllByIid(String iid) {
		insuranceDeviceRepository.deleteAllByIid(iid);
	}

	@Override
	public BaseResponse<String> deleteInsuranceDevice(String idid) {
		insuranceDeviceRepository.deleteByIdid(idid);
		return BaseResponse.success();
	}

	@Override
	public List<InsuranceDevice> findAllByIid(String iid) {
		return insuranceDeviceRepository.findAllByIid(iid);
	}

	@Override
	public BasePageResponse<InsuranceDeviceVO> getInsuranceDevicePage(InsuranceDevicePageQuery pageQuery) {
		Specification<InsuranceDevice> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			if (pageQuery.getInsuranceCode() != null && !pageQuery.getInsuranceCode().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("insurance").get("insuranceCode"), CommonUtils.assembleLike(pageQuery.getInsuranceCode())));
			}
			if (pageQuery.getCode() != null && !pageQuery.getCode().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("code"), CommonUtils.assembleLike(pageQuery.getCode())));
			}
			if (pageQuery.getRegistrationNumber() != null && !pageQuery.getRegistrationNumber().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("registrationNumber"), CommonUtils.assembleLike(pageQuery.getRegistrationNumber())));
			}
			if (pageQuery.getName() != null && !pageQuery.getName().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("insurance").get("customer").get("name"), CommonUtils.assembleLike(pageQuery.getName())));
			}
			return predicate;
		};
		Page<InsuranceDevice> insurancePage = insuranceDeviceRepository.findAll(specification, pageQuery.of());
		List<InsuranceDeviceVO> insuranceDeviceInfoVOS = insurancePage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());
		return BasePageResponse.success(insurancePage, insuranceDeviceInfoVOS);
	}

	@Override
	public BaseResponse<InsuranceDeviceInfoVO> getInsuranceInfo(String idid) {
		InsuranceDevice insuranceDevice = insuranceDeviceRepository.findByIdid(idid);
		return BaseResponse.success(convertToInfoVO(insuranceDevice));
	}

	@Override
	public InsuranceDeviceInfoVO getInsuranceInfoByCode(String code) {
		Specification<InsuranceDevice> specification = Specification.where((root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			long currentTimeMillis = System.currentTimeMillis();
			predicate = builder.and(predicate, builder.equal(root.get("code"), code));
			predicate = builder.and(predicate, builder.lessThanOrEqualTo(root.get("insurance").get("insuranceStartAt"), currentTimeMillis));
			predicate = builder.and(predicate, builder.greaterThanOrEqualTo(root.get("insurance").get("insuranceExpireAt"), currentTimeMillis));
			return predicate;
		});

		Sort sort = Sort.by(Sort.Direction.DESC, "ut");
		Pageable pageable = PageRequest.of(0, 1, sort);
		List<InsuranceDevice> insuranceDevices = insuranceDeviceRepository.findAll(specification, pageable).getContent();

		if (!insuranceDevices.isEmpty()) {
			InsuranceDevice insuranceDevice = insuranceDevices.get(0);
			return convertToInfoVO(insuranceDevice);
		}

		return null;
	}

	@Override
	public BaseResponse<String> updateInsuranceDevice(String idid, InsuranceDeviceDTO dto) {
		InsuranceDevice insuranceDevice = insuranceDeviceRepository.findByIdid(idid);
		if (insuranceDevice != null) {
			if (exists(insuranceDevice)) {
				return BaseResponse.fail("存在相同的设备序列号或注册号");
			}
			BeanUtils.copyProperties(dto, insuranceDevice);
			insuranceDeviceRepository.save(insuranceDevice);
			return BaseResponse.success();
		}
		return BaseResponse.fail("保单设备不存在");
	}

	@Override
	public InsuranceDevice convertDTOToEntity(InsuranceDeviceDTO dto, String iid) {
		InsuranceDevice insuranceDevice = new InsuranceDevice();
		BeanUtils.copyProperties(dto, insuranceDevice);
		insuranceDevice.setCode(dto.getCode().toUpperCase(Locale.ROOT));
		insuranceDevice.setRegistrationNumber(dto.getRegistrationNumber().toUpperCase(Locale.ROOT));
		insuranceDevice.setUid(CommonUtils.getUid());
		insuranceDevice.setIid(iid);
		insuranceDevice.setIdid(CommonUtils.uuid());

		return insuranceDevice;
	}

	private InsuranceDeviceVO convertToVO(InsuranceDevice insuranceDevice) {
		InsuranceDeviceVO insuranceDeviceVO = new InsuranceDeviceVO();
		BeanUtils.copyProperties(insuranceDevice, insuranceDeviceVO);
		insuranceDeviceVO.setCreateTime(DateUtils.formatDateTime(insuranceDevice.getCt()));

		Insurance insurance = insuranceDevice.getInsurance();
		insuranceDeviceVO.setInsuranceType(insurance.getInsuranceType());
		insuranceDeviceVO.setInsuranceCode(insurance.getInsuranceCode());
		insuranceDeviceVO.setTotalAmount(insurance.getTotalAmount());
		insuranceDeviceVO.setInsuranceStartAt(DateUtils.formatDateTime(insurance.getInsuranceStartAt()));
		insuranceDeviceVO.setInsuranceExpireAt(DateUtils.formatDateTime(insurance.getInsuranceExpireAt()));

		Customer customer = insurance.getCustomer();
		insuranceDeviceVO.setCid(customer.getCid());
		insuranceDeviceVO.setCustomerName(customer.getName());

		User user = insurance.getUser();
		insuranceDeviceVO.setUserName(user.getUserName());

		return insuranceDeviceVO;
	}

	private InsuranceDeviceInfoVO convertToInfoVO(InsuranceDevice insuranceDevice) {
		InsuranceDeviceInfoVO insuranceDeviceInfoVO = new InsuranceDeviceInfoVO();
		BeanUtils.copyProperties(insuranceDevice, insuranceDeviceInfoVO);
		insuranceDeviceInfoVO.setCreateTime(DateUtils.formatDateTime(insuranceDevice.getCt()));

		Insurance insurance = insuranceDevice.getInsurance();
		insuranceDeviceInfoVO.setInsuranceType(insurance.getInsuranceType());
		insuranceDeviceInfoVO.setInsuranceCode(insurance.getInsuranceCode());
		insuranceDeviceInfoVO.setTotalAmount(insurance.getTotalAmount());
		insuranceDeviceInfoVO.setInsuranceStartAt(insurance.getInsuranceStartAt());
		insuranceDeviceInfoVO.setInsuranceExpireAt(insurance.getInsuranceExpireAt());

		Customer customer = insurance.getCustomer();
		insuranceDeviceInfoVO.setCid(customer.getCid());
		insuranceDeviceInfoVO.setCustomerName(customer.getName());

		User user = insurance.getUser();
		insuranceDeviceInfoVO.setUserName(user.getUserName());

		return insuranceDeviceInfoVO;
	}

	public boolean exists(InsuranceDevice insuranceDevice) {
		Specification<InsuranceDevice> specification = (root, query, builder) -> builder.and(
						builder.equal(root.get("iid"), insuranceDevice.getIid()),
						builder.or(
								builder.equal(root.get("code"), insuranceDevice.getCode()),
								builder.equal(root.get("registrationNumber"), insuranceDevice.getRegistrationNumber())
						),
						builder.notEqual(root.get("id"), insuranceDevice.getId())
				);
		return insuranceDeviceRepository.count(specification) > 0;
	}
}


