package com.thinglinks.uav.insurance.service.impl;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.util.ListUtils;
import com.thinglinks.uav.common.constants.CustomerConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.InsuranceEnum;
import com.thinglinks.uav.common.utils.*;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.insurance.dto.InsuranceDTO;
import com.thinglinks.uav.insurance.dto.InsuranceDeviceDTO;
import com.thinglinks.uav.insurance.dto.InsuranceDeviceImport;
import com.thinglinks.uav.insurance.dto.InsurancePageQuery;
import com.thinglinks.uav.insurance.entity.Insurance;
import com.thinglinks.uav.insurance.entity.InsuranceDevice;
import com.thinglinks.uav.insurance.repository.InsuranceRepository;
import com.thinglinks.uav.insurance.service.InsuranceDeviceService;
import com.thinglinks.uav.insurance.service.InsuranceService;
import com.thinglinks.uav.insurance.vo.InsuranceExportData;
import com.thinglinks.uav.insurance.vo.InsuranceInfoVO;
import com.thinglinks.uav.insurance.vo.InsuranceVO;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * (Insurances)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-25 15:35:15
 */
@Service
@Slf4j
public class InsuranceServiceImpl implements InsuranceService {

	@Resource
	private InsuranceRepository insuranceRepository;
	@Resource
	private InsuranceDeviceService insuranceDeviceService;
	@Resource
	private CustomersRepository customersRepository;

	@Override
	public BaseResponse<String> insert(InsuranceDTO dto) {
		if (dto.getInsuranceStartAt() > dto.getInsuranceExpireAt()) {
			return BaseResponse.fail("失效时间不能小于等于生效时间");
		}

		String insuranceCode = dto.getInsuranceCode().toUpperCase(Locale.ROOT);
		if (insuranceRepository.existsByInsuranceCode(insuranceCode)) {
			return BaseResponse.fail("保单编号已存在");
		}

		Insurance insurance = new Insurance();
		BeanUtils.copyProperties(dto, insurance);
		String iid = CommonUtils.uuid();
		insurance.setIid(iid);
		insurance.setUid(CommonUtils.getUid());
		// 保险类型系统默认只有鼎和保险
		insurance.setInsuranceType(InsuranceEnum.DingHe.getCode());
		insurance.setInsuranceCode(insuranceCode);
		insuranceRepository.save(insurance);

		List<InsuranceDeviceDTO> devices = dto.getDevices();
		insuranceDeviceService.insertAll(devices, iid);

		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> delete(String iid) {
		insuranceDeviceService.deleteAllByIid(iid);
		insuranceRepository.deleteByIid(iid);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> update(String iid, InsuranceDTO dto) {
		Insurance insurance = insuranceRepository.findByIid(iid);
		if (Objects.isNull(insurance)) {
			return BaseResponse.fail("保单不存在");
		}

		if (dto.getInsuranceStartAt() > dto.getInsuranceExpireAt()) {
			return BaseResponse.fail("失效时间不能小于等于生效时间");
		}

		String newInsuranceCode = dto.getInsuranceCode().toUpperCase(Locale.ROOT);
		if (!insurance.getInsuranceCode().equals(newInsuranceCode) && (insuranceRepository.existsByInsuranceCode(newInsuranceCode))) {
				return BaseResponse.fail("保单编号已存在");

		}

		List<InsuranceDeviceDTO> devices = dto.getDevices();

		insuranceDeviceService.deleteAllByIid(iid);
		insuranceDeviceService.insertAll(devices, iid);

		BeanUtils.copyProperties(dto, insurance);
		insuranceRepository.save(insurance);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<InsuranceInfoVO> getInsuranceInfo(String iid) {
		Insurance insurance = insuranceRepository.findByIid(iid);
		return BaseResponse.success(convertToInfoVO(insurance));
	}

	@Override
	public BasePageResponse<InsuranceVO> getInsurancePage(InsurancePageQuery pageQuery) {
		Page<Insurance> insurancePage = getPage(pageQuery);
		List<InsuranceVO> insuranceVOList = insurancePage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(insurancePage, insuranceVOList);
	}

	private Page<Insurance> getPage(InsurancePageQuery pageQuery) {
		Specification<Insurance> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			if (pageQuery.getInsuranceCode() != null && !pageQuery.getInsuranceCode().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("insuranceCode"), CommonUtils.assembleLike(pageQuery.getInsuranceCode())));
			}
			if (pageQuery.getName() != null && !pageQuery.getName().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("customer").get("name"), CommonUtils.assembleLike(pageQuery.getName())));
			}
			return predicate;
		};

		Page<Insurance> insurancePage = insuranceRepository.findAll(specification, pageQuery.of());
		return insurancePage;
	}

	@Transactional
	@Override
	public BaseResponse<String> importInsurance(MultipartFile file, HttpServletResponse response) throws IOException {
		EasyExcelFactory.read(file.getInputStream(), InsuranceDeviceImport.class, new AnalysisEventListener<InsuranceDeviceImport>() {

					/**
					 * 单次缓存的数据量
					 */
					public static final int BATCH_COUNT = 100;

					/**
					 *临时存储
					 */
					private List<InsuranceDeviceDTO> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

					private final Set<String> serialNumbers = new HashSet<>(BATCH_COUNT);
					private final Set<String> registrationNumbers = new HashSet<>(BATCH_COUNT);

					private String iid;

					private final ArrayList<InsuranceDeviceImport> errorData = new ArrayList<>();

					@Override
					public void onException(Exception exception, AnalysisContext context) {
						log.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
						if (exception instanceof ExcelDataConvertException) {
							ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException)exception;
							String logMessage = String.format("第%d行，第%d列解析异常，数据为:%s",
									excelDataConvertException.getRowIndex() + 1,
									excelDataConvertException.getColumnIndex() + 1,
									excelDataConvertException.getCellData().getStringValue());
							InsuranceDeviceImport e = new InsuranceDeviceImport();
							e.setError(logMessage);
							log.error(logMessage);
							errorData.add(e);
						}
					}
					@Override
					public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
						int rowIndex = context.readRowHolder().getRowIndex();
						if (rowIndex == 1) {
                            // 存储第二行表头
                            String insuranceCode = headMap.get(0).toLowerCase(Locale.ROOT);
                            Insurance insurance = insuranceRepository.findByInsuranceCode(insuranceCode);
                            if (Objects.isNull(insurance)) {
                                insurance = new Insurance();
                                insurance.setIid(CommonUtils.uuid());
                                insurance.setInsuranceCode(insuranceCode);
                            }

                            String s = headMap.get(1);
                            Customer customer = customersRepository.findByName(s);
                            if (Objects.isNull(customer)) {
								customer = new Customer();
								customer.setName(s);
								customer.setType(CustomerConstants.CUSTOMER_TYPE_INSIDE);
	                            customer.setCid(CommonUtils.uuid());
	                            customersRepository.save(customer);
                            }
                            String cid = customer.getCid();
                            insurance.setCid(cid);

							try {
								String totalAmountStr = headMap.get(2).replace(",", "");
								BigDecimal totalAmount = new BigDecimal(totalAmountStr);
								insurance.setTotalAmount(totalAmount);
							} catch (Exception e) {
								throw new BusinessException("金额格式错误,格式：数值型");
							}

                            try {
	                            insurance.setInsuranceStartAt(DateUtils.getDate(headMap.get(3)).getTime());
	                            insurance.setInsuranceExpireAt(DateUtils.getDate(headMap.get(4)).getTime());
                            } catch (Exception e) {
								throw new BusinessException("日期格式错误,格式：日期型");
                            }

                            insurance.setUid(CommonUtils.getUid());
                            insurance.setInsuranceType(InsuranceEnum.DingHe.getCode());
                            iid = insurance.getIid();
                            insuranceRepository.save(insurance);
                        }
					}

					@Override
					public void invoke(InsuranceDeviceImport data, AnalysisContext context) {
						InsuranceDeviceDTO insuranceDeviceDTO = new InsuranceDeviceDTO();
						BeanUtils.copyProperties(data, insuranceDeviceDTO);
						try {
							ValidationUtils.validateObject(insuranceDeviceDTO);
						} catch (CustomBizException e) {
							data.setError(e.getMessage());
                            errorData.add(data);
							return;
                        }

						if (insuranceDeviceService.existsInIid(iid, data.getCode(), data.getRegistrationNumber())) {
							data.setError("设备序列号或保险注册号已存在");
							errorData.add(data);
							return;
						}

						if (serialNumbers.contains(data.getCode())) {
							data.setError("设备SN码重复");
							errorData.add(data);
							return;
						}
						serialNumbers.add(data.getCode());

						if (registrationNumbers.contains(data.getRegistrationNumber())) {
							data.setError("保险注册号重复");
							errorData.add(data);
							return;
						}
						registrationNumbers.add(data.getRegistrationNumber());

                        cachedDataList.add(insuranceDeviceDTO);
						if (cachedDataList.size() >= BATCH_COUNT) {
							saveData();
							// 存储完成清理 list
							cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
							serialNumbers.clear();
							registrationNumbers.clear();
						}
					}

					@Override
					public void doAfterAllAnalysed(AnalysisContext context) {
						saveData();
						if (!errorData.isEmpty()) {
                            try {
                                EasyExcelUtils.exportExcel(response, "导入错误数据", "错误信息", errorData, InsuranceDeviceImport.class);
                            } catch (IOException e) {
                                throw new BusinessException(e.getMessage());
                            }
                        }
					}

					/**
					 * 加上存储数据库
					 */
					private void saveData() {
						if (cachedDataList.isEmpty()) {
							return;
						}
						List<InsuranceDevice> devices = cachedDataList.stream()
								.map(dto -> insuranceDeviceService.convertDTOToEntity(dto, iid))
								.collect(Collectors.toList());
						insuranceDeviceService.insertAll(devices);
					}
				}).sheet()
				.headRowNumber(3).doRead();
		return BaseResponse.success();
	}

	@Override
	public void exportInsurance(List<String> iids, HttpServletResponse response) throws IOException {
		List<InsuranceExportData> dataList = insuranceRepository.findAllByIidIn(iids).stream()
				.flatMap(insurance -> createInsuranceData(insurance).stream())
				.collect(Collectors.toList());
		EasyExcelUtils.exportExcel(response, "保险信息", "保险信息", dataList, InsuranceExportData.class);
	}

	private List<InsuranceExportData> createInsuranceData(Insurance insurance) {
		String customerName = insurance.getCustomer().getName();
		String insuranceExpireAt = DateUtils.formatDateTime(insurance.getInsuranceExpireAt());
		String insuranceStartAt = DateUtils.formatDateTime(insurance.getInsuranceStartAt());
		String insuranceCode = insurance.getInsuranceCode();
		BigDecimal totalAmount = insurance.getTotalAmount();

		List<InsuranceExportData> insuranceExportDataList = new ArrayList<>();
		List<InsuranceDevice> insuranceDevices = insuranceDeviceService.findAllByIid(insurance.getIid());
		for (InsuranceDevice insuranceDevice : insuranceDevices) {
			InsuranceExportData insuranceExportData = new InsuranceExportData();
			BeanUtils.copyProperties(insuranceDevice, insuranceExportData);
			insuranceExportData.setInsuranceCode(insuranceCode);
			insuranceExportData.setCustomerName(customerName);
			insuranceExportData.setTotalAmount(totalAmount);
			insuranceExportData.setInsuranceStartAt(insuranceStartAt);
			insuranceExportData.setInsuranceExpireAt(insuranceExpireAt);
			insuranceExportDataList.add(insuranceExportData);
		}

		return insuranceExportDataList;
	}


	private InsuranceVO convertToVO(Insurance insurance) {
		InsuranceVO insuranceVO = new InsuranceVO();
		BeanUtils.copyProperties(insurance, insuranceVO);
		insuranceVO.setCreatedAt(DateUtils.formatDateTime(insurance.getCt()));
		insuranceVO.setInsuranceStartAt(DateUtils.formatDateTime(insurance.getInsuranceStartAt()));
		insuranceVO.setInsuranceExpireAt(DateUtils.formatDateTime(insurance.getInsuranceExpireAt()));

		Customer customer = insurance.getCustomer();
		insuranceVO.setCustomerName(customer.getName());

		User user = insurance.getUser();
		insuranceVO.setUserName(user.getUserName());

		return insuranceVO;
	}

	private InsuranceInfoVO convertToInfoVO(Insurance insurance) {
		InsuranceInfoVO insuranceInfoVO = new InsuranceInfoVO();
		BeanUtils.copyProperties(insurance, insuranceInfoVO);
		insuranceInfoVO.setCustomerName(insurance.getCustomer().getName());

		List<InsuranceDevice> insuranceDevices = insuranceDeviceService.findAllByIid(insurance.getIid());
		insuranceInfoVO.setDevices(insuranceDevices.stream()
				.map(insuranceDevice -> {
					InsuranceInfoVO.InsuranceDevice insuranceDeviceVO = new InsuranceInfoVO.InsuranceDevice();
					insuranceDeviceVO.setCode(insuranceDevice.getCode());
					insuranceDeviceVO.setRegistrationNumber(insuranceDevice.getRegistrationNumber());
					insuranceDeviceVO.setAmount(insuranceDevice.getAmount());
					return insuranceDeviceVO;
				})
				.collect(Collectors.toList()));

		return insuranceInfoVO;
	}
}


