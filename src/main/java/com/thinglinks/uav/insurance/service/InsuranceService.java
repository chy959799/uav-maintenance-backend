package com.thinglinks.uav.insurance.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.insurance.dto.InsuranceDTO;
import com.thinglinks.uav.insurance.dto.InsurancePageQuery;
import com.thinglinks.uav.insurance.vo.InsuranceInfoVO;
import com.thinglinks.uav.insurance.vo.InsuranceVO;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * (Insurances)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-25 15:35:15
 */
public interface InsuranceService {

	BaseResponse<String> insert(InsuranceDTO dto);

	@Transactional
	BaseResponse<String> delete(String iid);

	BaseResponse<String> update(String iid, InsuranceDTO dto);

	BaseResponse<InsuranceInfoVO> getInsuranceInfo(String iid);

	BasePageResponse<InsuranceVO> getInsurancePage(InsurancePageQuery pageQuery);

	BaseResponse<String> importInsurance(MultipartFile file, HttpServletResponse response) throws IOException;

	void exportInsurance(List<String> iids, HttpServletResponse response) throws IOException;
}


