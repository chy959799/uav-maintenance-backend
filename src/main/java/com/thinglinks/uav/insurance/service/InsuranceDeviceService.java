package com.thinglinks.uav.insurance.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.insurance.dto.InsuranceDeviceDTO;
import com.thinglinks.uav.insurance.dto.InsuranceDevicePageQuery;
import com.thinglinks.uav.insurance.entity.InsuranceDevice;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceInfoVO;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (InsuranceDevices)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-25 17:32:53
 */
public interface InsuranceDeviceService {

	void insertAll(List<InsuranceDeviceDTO> devices, String iid);

	void insertAll(List<InsuranceDevice> devices);

	/**
	 * 	校验保险设备列表中是否存在重复的序列号或注册号
	 */
	boolean hasDuplicateDevices(List<InsuranceDeviceDTO> devices);

	Boolean existsInIid(String iid, String code, String registrationNumber);

	@Transactional
	void deleteAllByIid(String iid);

	@Transactional
	BaseResponse<String> deleteInsuranceDevice(String idid);

	BaseResponse<String> updateInsuranceDevice(String idid, InsuranceDeviceDTO dto);

	List<InsuranceDevice> findAllByIid(String iid);

	BasePageResponse<InsuranceDeviceVO> getInsuranceDevicePage(InsuranceDevicePageQuery pageQuery);

	BaseResponse<InsuranceDeviceInfoVO> getInsuranceInfo(String idid);

	/**
	 * 根据设备SN码获取有效保单信息
	 */
	InsuranceDeviceInfoVO getInsuranceInfoByCode(String code);

	InsuranceDevice convertDTOToEntity(InsuranceDeviceDTO dto, String iid);
}


