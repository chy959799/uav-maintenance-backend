package com.thinglinks.uav.insurance.repository;

import com.thinglinks.uav.insurance.entity.InsuranceDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (InsuranceDevices)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-25 17:32:53
 */
public interface InsuranceDeviceRepository extends JpaRepository<InsuranceDevice, Integer>, JpaSpecificationExecutor<InsuranceDevice> {


	void deleteAllByIid(String iid);

	List<InsuranceDevice> findAllByIid(String iid);

	InsuranceDevice findByIdid(String idid);

	void deleteByIdid(String idid);

	/**
	 * 判断保单中是否存在相同的设备SN码或登记号
	 * @param iid
	 * @param codes
	 * @param iid1
	 * @param registrationNumbers
	 * @return
	 */
	boolean existsByIidAndCodeInOrIidAndRegistrationNumberIn(String iid, List<String> codes, String iid1, List<String> registrationNumbers);


	InsuranceDevice findByCode(String code);

	InsuranceDevice findByRegistrationNumber(String registrationNumber);

    Boolean existsByIidAndCodeAndRegistrationNumber(String iid, String code, String registrationNumber);
}


