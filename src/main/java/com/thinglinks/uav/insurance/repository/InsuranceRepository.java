package com.thinglinks.uav.insurance.repository;

import com.thinglinks.uav.insurance.entity.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * (Insurances)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-25 15:35:15
 */
public interface InsuranceRepository extends JpaRepository<Insurance, Integer>, JpaSpecificationExecutor<Insurance> {


	void deleteByIid(String iid);

	Insurance findByIid(String iid);

	boolean existsByInsuranceCode(String insuranceCode);

	List<Insurance> findAllByIidIn(List<String> iids);

	Insurance findByInsuranceCode(String insuranceCode);

	@Query("SELECT i FROM Insurance i WHERE i.insuranceExpireAt >= :now")
	List<Insurance> findActiveInsurances(@Param("now") Long now);

	@Query("SELECT i FROM Insurance i WHERE i.insuranceExpireAt < :now")
	List<Insurance> findExpiredInsurances(@Param("now") Long now);
}


