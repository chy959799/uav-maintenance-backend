package com.thinglinks.uav.insurance.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * (InsuranceDevices)实体类
 *
 * @author iwiFool
 * @since 2024-03-25 17:32:53
 */
@EqualsAndHashCode(exclude = {"insurance", "user"},callSuper = true)
@Entity
@Table(name = "insurance_devices")
@Data
public class InsuranceDevice extends BaseEntity {

	/**
	 * 保单设备内部ID
	 */
	private String idid;
	/**
	 * 保单外部ID
	 */
	private String iid;
	/**
	 * 设备序列号
	 */
	private String code;
	/**
	 * 保险金额
	 */
	private BigDecimal amount;
	/**
	 * 保险注册号
	 */
	private String registrationNumber;

	private String uid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "iid", referencedColumnName = "iid", insertable = false, updatable = false)
	private Insurance insurance;
}
