package com.thinglinks.uav.insurance.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * (Insurances)实体类
 *
 * @author iwiFool
 * @since 2024-03-25 15:35:15
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "insurances")
@Data
public class Insurance extends BaseEntity {

	private String iid;
	/**
	 * 保单编号
	 */
	private String insuranceCode;
	/**
	 * 保险类型 8:鼎和财险
	 */
	private String insuranceType;
	/**
	 * 保险开始时间
	 */
	private Long insuranceStartAt;
	/**
	 * 保险过期时间
	 */
	private Long insuranceExpireAt;
	/**
	 * 总承保金额
	 */
	private BigDecimal totalAmount;

	private String cid;

	private String uid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cid", referencedColumnName = "cid", insertable = false, updatable = false)
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
	private User user;
}
