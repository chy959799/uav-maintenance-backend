package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


/**
 * @author huiyongchen
 */
public interface ContactRepository extends JpaRepository<Contact, Integer>, JpaSpecificationExecutor<Contact> {
    Contact findByCtid(String ctid);

    Integer deleteByCtid(String ctid);

    Boolean existsByCidIn(List<String> ids);

    boolean existsByCid(String cid);

    Contact findByCid(String cid);
}


