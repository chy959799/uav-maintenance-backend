package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (Customer)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-17 20:39:36
 */
public interface CustomersRepository extends JpaRepository<Customer, Integer>, JpaSpecificationExecutor<Customer> {


	Integer deleteByCid(String cid);

	void deleteAllByCidIn(List<String> ids);


	Customer getByCid(String cid);

	Customer findByName(String name);

    boolean existsByCid(String cid);

	boolean existsByNameAndCpid(String name, String cpid);

	boolean existsByName(String name);

	Customer findByCid(String cid);

	boolean existsByPcid(String cid);

	boolean existsByPcidIn(List<String> ids);

	List<Customer> findAllByType(String type);
}


