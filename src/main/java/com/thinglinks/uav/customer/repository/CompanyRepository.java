package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (Companies)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-19 18:39:46
 */
public interface CompanyRepository extends JpaRepository<Company, Integer>, JpaSpecificationExecutor<Company> {

    void deleteByCpid(String cpid);

    void deleteAllByCpidIn(List<String> ids);

    Company findByCpid(String cpid);

    List<Company> findAllByTemplateCode(String code);
}


