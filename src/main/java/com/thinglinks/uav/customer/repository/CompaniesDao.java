package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.Company;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * (Companies)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-19 18:39:46
 */
public interface CompaniesDao extends JpaRepository<Company, Integer> {


}


