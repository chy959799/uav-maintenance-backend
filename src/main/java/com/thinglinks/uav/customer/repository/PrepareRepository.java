package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.Prepare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author huiyongchen
 */
public interface PrepareRepository extends JpaRepository<Prepare, Long>, JpaSpecificationExecutor<Prepare> {
    boolean existsByDevid(String devId);
    void deleteByDevid(String devId);
}
