package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author huiyongchen
 */
public interface DeviceRepository extends JpaRepository<Device, Long>, JpaSpecificationExecutor<Device> {
    Device findByDevid(String devid);
    boolean existsByCode(String code);

    void deleteByDevid(String devid);
    boolean existsByUid(String uid);
    Device findByCode(String code);
    boolean existsByPidIn(List<String> ids);
    Device findByInsuranceCode(String insuranceCode);

    List<Device> findByDevidIn(List<String> devIdList);
}
