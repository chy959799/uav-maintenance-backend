package com.thinglinks.uav.customer.repository;

import com.thinglinks.uav.customer.entity.DeviceLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author huiyongchen
 */
public interface DeviceLogRepository extends JpaRepository<DeviceLog, Long>, JpaSpecificationExecutor<DeviceLog> {
}
