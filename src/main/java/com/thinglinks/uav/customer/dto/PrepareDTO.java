package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.customer.entity.Prepare;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 21:11
 */
@Data
public class PrepareDTO extends Prepare {

    @ApiModelProperty("创建时间")
    private String createdAt;

    @ApiModelProperty("更新时间")
    private String updatedAt;
}
