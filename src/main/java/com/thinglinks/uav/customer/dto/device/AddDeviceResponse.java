package com.thinglinks.uav.customer.dto.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 15:17
 */
@Data
public class AddDeviceResponse {

    /**
     * devid : 20c954b6-ccd5-4879-a620-81d21b9abf6f
     * status : 1
     * id : 1039
     * cid : 8271a5cc-2070-4a5e-8d9b-523a5514696b
     * pid : 1ba037f0-18be-4f79-8c95-61436c3667a8
     * code : SDSADSA
     * exchange_count : 0
     * quota : 0
     * inner_code : DJ0371.DSD2221.0001039
     * updated_at : 2024-03-26T07:08:55.979Z
     * created_at : 2024-03-26T07:08:55.979Z
     */

    @ApiModelProperty("设备内部ID唯一")
    private String devid;

    @ApiModelProperty("设备状态 1:正常 2:维修中 3:保养中")
    private String status;

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("客户ID")
    private String cid;

    @ApiModelProperty("产品ID")
    private String pid;

    @ApiModelProperty("设备序列号")
    private String code;

    @ApiModelProperty("设备内部编码")
    private String innerCode;

    @ApiModelProperty("更新时间")
    private String updatedTime;

    @ApiModelProperty("创建时间")
    private String createdTime;
}
