package com.thinglinks.uav.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@Data
@ApiModel(description = "删除客户信息DTO")
public class DeleteDTO {
	@ApiModelProperty(value = "cid数组", example = "01a5dc1c45e04df3aae894fe2ac55e04,43ff4e4da62a48538ae2ac0ebab5f6e1")
	private List<String> ids;
}
