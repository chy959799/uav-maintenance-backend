package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.customer.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.customer.dto
 * @className: CustomerPage
 * @author: rp
 * @description: TODO
 */
@Data
public class CustomerPage extends Customer {
    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "父级分类")
    private Customer parent;
}
