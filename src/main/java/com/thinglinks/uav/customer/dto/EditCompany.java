package com.thinglinks.uav.customer.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @package: com.thinglinks.uav.company.vo
 * @className: EditCompany
 * @author: rp
 * @description: TODO
 */
@Data
public class EditCompany {
    @ApiModelProperty(value = "公司名称")
    private String name;
    @ApiModelProperty(value = "公司代号(缩写)")
    private String code;
    @ApiModelProperty(value = "所属省代号")
    private String province;
    @ApiModelProperty(value = "所属市代号")
    private String city;
    @ApiModelProperty(value = "所属区/县代号")
    private String county;
    @ApiModelProperty(value = "公司地址")
    private String address;
    @ApiModelProperty(value = "公司联系人")
    private String contactName;
    @ApiModelProperty(value = "公司联系人电话")
    private String contactMobile;
    @ApiModelProperty(value = "过期时间")
    private LocalDateTime expiredAt;
    @ApiModelProperty(value = "允许创建的用户数")
    private Integer userCount;
    @ApiModelProperty(value = "顺丰月结卡号")
    private String monthlyCard;
}
