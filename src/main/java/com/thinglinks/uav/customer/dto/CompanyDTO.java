package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.common.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author iwiFool
 * @date 2024/7/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CompanyDTO extends BaseDTO {

    /**
     * 维修点名称
     */
    @NotBlank(message = "维修点名称不能为空", groups = {Insert.class})
    @ApiModelProperty(value = "维修点名称")
    private String name;

    /**
     * 维修点联系人
     */
    @NotBlank(message = "维修点联系人不能为空")
    @ApiModelProperty(value = "维修点联系人")
    private String contactName;

    /**
     * 联系人电话
     */
    @NotBlank(message = "联系人电话不能为空")
    @ApiModelProperty(value = "联系人电话")
    private String contactMobile;

    /**
     * 地址区域-所属省代号
     */
    @NotBlank(message = "省代号不能为空")
    @ApiModelProperty(value = "地址区域-所属省代号")
    private String province;

    /**
     * 地址区域-所属市代号
     */
    @NotBlank(message = "市代号不能为空")
    @ApiModelProperty(value = "地址区域-所属市代号")
    private String city;

    /**
     * 地址区域-所属区/县代号
     */
    @NotBlank(message = "区/县代号不能为空")
    @ApiModelProperty(value = "地址区域-所属区/县代号")
    private String county;

    /**
     * 详细地址
     */
    @NotBlank(message = "详细地址不能为空")
    @ApiModelProperty(value = "详细地址")
    private String address;

    /**
     * 授权用户数
     */
    @Min(value = 1, message = "授权用户数不能小于1")
    @ApiModelProperty(value = "授权用户数")
    private Integer userCount;

    /**
     * 过期时间
     */
    @NotNull(message = "过期时间不能为空")
    @ApiModelProperty(value = "过期时间", example = "2024-08-05 00:00:00")
    private String expiredAt;

    /**
     * 维修点备注
     */
    @NotBlank(message = "维修点备注不能为空")
    @ApiModelProperty(value = "维修点备注")
    private String description;

    /**
     * 维修点模板编码
     */
    @NotBlank(message = "维修点模板编码不能为空")
    @ApiModelProperty(value = "维修点模板编码")
    private String templateCode;
}
