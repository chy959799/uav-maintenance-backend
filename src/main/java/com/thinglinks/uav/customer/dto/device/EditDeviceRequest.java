package com.thinglinks.uav.customer.dto.device;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 16:54
 */
@Data
public class EditDeviceRequest {

    /**
     * cid : 8271a5cc-2070-4a5e-8d9b-523a5514696b
     * pid : ef16ed4f-e817-4792-8e02-e1a709fbe0ca
     * code : SN003
     * produce_at : 2024-03-05T16:00:00.000Z
     * activate_at : 2024-03-26T16:00:00.000Z
     * insurance_type : 2
     * insurance_start_at : 2024-03-29T16:00:00.000Z
     * insurance_expire_at : 2024-04-03T16:00:00.000Z
     * exchange_count : 13
     * quota : 65.2
     * material_code : 123
     * warranty_type : 2
     * description : test description
     */

    @ApiModelProperty(notes = "分类ID")
    private String catid;

    @ApiModelProperty(notes = "产品ID")
    private String pid;

    @ApiModelProperty(notes = "设备SN")
    private String code;

    @ApiModelProperty(notes = "生产时间")
    private String produceTime;

    @ApiModelProperty(notes = "激活时间")
    private String activateTime;

    @ApiModelProperty(notes = "物料编码")
    private String materialCode;

    @ApiModelProperty(notes = "描述")
    private String description;
}
