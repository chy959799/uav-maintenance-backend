package com.thinglinks.uav.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@Data
@ApiModel(description = "客户DTO对象")
public class CustomerDTO {

	@ApiModelProperty(value = "客户类型 [1:单位客户, 2:个人客户, 3:系统客户-系统默认客户，用于挂载多租户公司自有设备]", example = "1")
	@NotBlank(message = "客户类型不能为空")
	private String type;

	@ApiModelProperty(value = "客户名，企业下唯一", example = "这是测试的客户名称3")
	@NotBlank(message = "客户名不能为空")
	private String name;

	@ApiModelProperty(value = "所属省代号", example = "540000")
	@NotBlank(message = "所属省代号不能为空")
	private String province;

	@ApiModelProperty(value = "所属市代号", example = "540001")
	@NotBlank(message = "所属市代号不能为空")
	private String city;

	@ApiModelProperty(value = "所属区/县代号", example = "540002")
	@NotBlank(message = "所属区/县代号不能为空")
	private String county;

	@ApiModelProperty(value = "详细地址", example = "这是客户的详细地址")
	private String address;

	@ApiModelProperty(value = "客户备注", example = "这是客户备注")
	private String remark;

	@ApiModelProperty(value = "父级单位ID")
	private String pcid;
}
