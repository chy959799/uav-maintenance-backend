package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.customer.dto
 * @className: ContactPageQuery
 * @author: rp
 * @description: TODO
 */
@Data
public class ContactPageQuery extends BasePageRequest {
    @ApiModelProperty(value = "企业id")
    private String cid;
    @ApiModelProperty(value = "联系人姓名")
    private String name;
    @ApiModelProperty(value = "联系人职务")
    private String job;
    @ApiModelProperty(value = "部门")
    private String department;
}
