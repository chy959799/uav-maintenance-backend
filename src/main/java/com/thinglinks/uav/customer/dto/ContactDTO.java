package com.thinglinks.uav.customer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.customer.entity.Contact;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;

/**
 * @package: com.thinglinks.uav.customer.dto
 * @className: ContactDto
 * @author: rp
 * @description: TODO
 */
@Data
public class ContactDTO {

    @ApiModelProperty("联系人姓名")
    private String name;


    @ApiModelProperty("联系人职务")
    private String job;


    @ApiModelProperty("所属部门")
    private String department;


    @ApiModelProperty("联系人电话")
    private String telphone;


    @ApiModelProperty("联系人邮箱")
    private String email;

}
