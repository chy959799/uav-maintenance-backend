package com.thinglinks.uav.customer.dto.device;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 9:01
 */
@Data
public class PageDeviceQuery extends BasePageRequest {
    @ApiModelProperty("单位名称")
    private String  cid;
    @ApiModelProperty("产品名称")
    private String  name;
    @ApiModelProperty("设备SN码")
    private String code;
    @ApiModelProperty("设备产品id")
    private String pid;
}
