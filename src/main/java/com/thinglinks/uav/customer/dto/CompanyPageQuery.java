package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @package: com.thinglinks.uav.company.vo
 * @className: CompanyPageQuery
 * @author: rp
 * @description: TODO
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CompanyPageQuery extends BasePageRequest {

    @ApiModelProperty(value = "维修点名称")
    private String name;
}
