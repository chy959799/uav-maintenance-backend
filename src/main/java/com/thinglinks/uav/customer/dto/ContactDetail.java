package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.customer.entity.Contact;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.customer.dto
 * @className: ContactDetail
 * @author: rp
 * @description: TODO
 */
@Data
public class ContactDetail extends Contact {
}
