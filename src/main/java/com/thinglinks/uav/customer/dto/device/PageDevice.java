package com.thinglinks.uav.customer.dto.device;

import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 9:01
 */
@Data
public class PageDevice extends Device {

    @ApiModelProperty("生产时间")
    private String produceTime;

    @ApiModelProperty("生成时间")
    private String activateTime;

    @ApiModelProperty("最近一次保养时间, 初始化为激活时间")
    private String lastestMainteanceTime;

    @ApiModelProperty("保险开始时间")
    private String insuranceStartTime;

    @ApiModelProperty("保险过期时间")
    private String insuranceExpireTime;

    @ApiModelProperty("设备企业")
    private Customer customer;

    @ApiModelProperty("是否为备品")
    private boolean prepare;

    @ApiModelProperty("创建时间")
    private String createdTime;

    @ApiModelProperty("更新时间")
    private String updatedTime;
}
