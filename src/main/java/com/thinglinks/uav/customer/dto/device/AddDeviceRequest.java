package com.thinglinks.uav.customer.dto.device;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 14:36
 */
@Data
public class AddDeviceRequest {

    /**
     * cid : 8271a5cc-2070-4a5e-8d9b-523a5514696b
     * pid : 1ba037f0-18be-4f79-8c95-61436c3667a8
     * code : FSFKSHFKFHDKSF
     * produce_at : 2024-03-04T16:00:00.000Z
     * activate_at : 2024-03-10T16:00:00.000Z
     * insurance_type : 1
     * insurance_start_at : 2024-03-11T16:00:00.000Z
     * insurance_expire_at : 2024-03-12T16:00:00.000Z
     * exchange_count : 6
     * material_code : 123456
     * quota : 15.3
     * warranty_type : 1
     * description : 测试描述
     */

    //分类id  产品id  设备 SN


    @ApiModelProperty(value = "产品分类ID")
    @NotNull
    private String catid;

    @ApiModelProperty(value = "产品ID")
    @NotNull
    private String pid;

    @ApiModelProperty(value = "SN码")
    @NotNull
    private String code;

    @ApiModelProperty(value = "物料编码")
    private String materialCode;

    @ApiModelProperty(value = "生产日期")
    private String produceTime;

    @ApiModelProperty(value = "激活日期")
    private String activateTime;

    @ApiModelProperty(value = "描述")
    private String description;

}
