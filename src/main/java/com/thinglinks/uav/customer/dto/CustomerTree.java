package com.thinglinks.uav.customer.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @package: com.thinglinks.uav.customer.dto.device
 * @className: CustomerTree
 * @author: rp
 * @description: TODO
 */
@Data
public class CustomerTree {
    @ApiModelProperty(value = "单位ID")
    private String cid;
    @ApiModelProperty(value = "单位名称")
    private String name;
    @ApiModelProperty(value = "父级单位ID")
    private String pcid;
    @ApiModelProperty(name = "单位数量")
    private int count;
    @ApiModelProperty(name = "子单位")
    private List<CustomerTree> children;

    public CustomerTree() {
    }
    public CustomerTree(String cid, String name, String pcid) {
        this.cid = cid;
        this.name = name;
        this.pcid = pcid;
        this.children = new ArrayList<>();
        this.count = 0;
    }
    public void updateCount() {
        int totalCount = children.size();
        for (CustomerTree child : children) {
            child.updateCount();
            totalCount += child.getCount();
        }
        count = totalCount;
    }

    public static List<CustomerTree> buildTree(List<CustomerTree> customerTree) {
        List<CustomerTree> trees = new ArrayList<>();
        for (CustomerTree tree : customerTree) {
            if (tree.getPcid() == null || StringUtils.isEmpty(tree.getPcid())) {
                CustomerTree node = buildNode(tree, customerTree);
                trees.add(node);
            }
        }
        return trees;
    }

    public static List<CustomerTree> buildTree2(String cid,List<CustomerTree> customerTree) {
        List<CustomerTree> trees = new ArrayList<>();
        for (CustomerTree tree : customerTree) {
            if (Objects.equals(tree.getPcid(), cid)) {
                CustomerTree node = buildNode2(tree, customerTree);
                trees.add(node);
            }
        }
        return trees;
    }
    private static CustomerTree buildNode(CustomerTree customer, List<CustomerTree> customerTree) {
        CustomerTree node = new CustomerTree(customer.getCid(), customer.getName(), customer.getPcid());
        List<CustomerTree> children = customerTree.stream()
                .filter(c -> customer.getCid().equals(c.getPcid()))
                .map(c -> buildNode(c, customerTree))
                .collect(Collectors.toList());
        node.children.addAll(children);
        node.updateCount();
        return node;
    }
    private static CustomerTree buildNode2(CustomerTree customer, List<CustomerTree> customerTree) {
        CustomerTree node = new CustomerTree(customer.getCid(), customer.getName(), customer.getPcid());
        List<CustomerTree> children = customerTree.stream()
                .filter(c -> customer.getCid().equals(c.getPcid()))
                .map(c -> buildNode2(c, customerTree))
                .collect(Collectors.toList());
        node.children.addAll(children);
        node.updateCount();
        return node;
    }
}
