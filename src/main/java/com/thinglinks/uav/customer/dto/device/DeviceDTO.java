package com.thinglinks.uav.customer.dto.device;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 14:56
 */
@Data
public class DeviceDTO extends BaseEntity {
    @Column(name = "devid")
    @ApiModelProperty(value = "设备内部ID唯一")
    private String devid;

    @Column(name = "code")
    @ApiModelProperty(value = "设备序列号", example = "设备序列号")
    private String code;

    @Column(name = "material_code")
    @ApiModelProperty(value = "物料编码")
    private String materialCode;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "设备内部编码")
    private String innerCode;

    @Column(name = "produce_at")
    @ApiModelProperty(value = "生产日期")
    private String produceAt;

    @Column(name = "activate_at")
    @ApiModelProperty(value = "激活时间")
    private String activateAt;

    @Column(name = "lastest_mainteance_at")
    @ApiModelProperty(value = "最近一次保养时间, 初始化为激活时间")
    private String lastestMainteanceAt;

    @Column(name = "insurance_code")
    @ApiModelProperty(value = "保单编号")
    private String insuranceCode;

    @Column(name = "insurance_type")
    @ApiModelProperty(value = "保险类型 1:DJI Care 2:DJI Care续享 3:行业无忧基础版 4:行业无忧基础续享版 5:行业无忧旗舰版 6:行业无忧旗舰续享版 7:英大财险 8:鼎和财险")
    private String insuranceType;

    @Column(name = "insurance_start_at")
    @ApiModelProperty(value = "保险开始时间")
    private String insuranceStartAt;

    @Column(name = "insurance_expire_at")
    @ApiModelProperty(value = "保险过期时间")
    private String insuranceExpireAt;

    @Column(name = "warranty_type")
    @ApiModelProperty(value = "质保类型 1:保内 2:保外")
    private String warrantyType;

    @Column(name = "exchange_count")
    @ApiModelProperty(value = "剩余换新次数")
    private Integer exchangeCount;

    @Column(name = "quota")
    @ApiModelProperty(value = "剩余额度")
    private Double quota;

    @Column(name = "status")
    @ApiModelProperty(value = "设备状态 1:正常 2:维修中 3:保养中")
    private String status;

    @Column(name = "description")
    @ApiModelProperty(value = "设备描述")
    private String description;

    @Column(name = "pid")
    @ApiModelProperty(value = "产品ID")
    private String pid;

    @Column(name = "cid")
    @ApiModelProperty(value = "客户ID")
    private String cid;

    @ApiModelProperty("创建时间")
    private String createdTime;

    @ApiModelProperty("更新时间")
    private String updatedTime;
}
