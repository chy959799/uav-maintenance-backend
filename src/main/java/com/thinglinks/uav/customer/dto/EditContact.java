package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.customer.entity.Contact;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.customer.dto
 * @className: EditContact
 * @author: rp
 * @description: TODO
 */
@Data
public class EditContact extends Contact {
}
