package com.thinglinks.uav.customer.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.customer.dto
 * @className: CustomerAddressRequest
 * @author: rp
 * @description: TODO
 */
@Data
public class CustomerAddressRequest extends BasePageRequest {
    @ApiModelProperty(value = "客户id")
    private String cid;

    @ApiModelProperty(value = "是否默认地址")
    @JsonProperty(value = "default")
    private Boolean isDefault;
}
