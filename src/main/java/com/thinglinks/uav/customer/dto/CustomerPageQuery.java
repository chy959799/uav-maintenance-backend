package com.thinglinks.uav.customer.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CustomerPageQuery extends BasePageRequest {
	@ApiModelProperty(value = "单位ID", example = "1")
	private String cid;
	@ApiModelProperty(value = "单位类型 ", example = "1")
	private String type;
	@ApiModelProperty(value = "单位名称", example = "测试")
	private String name;
}
