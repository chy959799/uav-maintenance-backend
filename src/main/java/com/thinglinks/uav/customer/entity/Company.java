package com.thinglinks.uav.customer.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * (Companies)实体类
 *
 * @author iwiFool
 * @since 2024-03-19 18:39:46
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "companies")
public class Company extends BaseEntity {
	/**
	 * 公司外部ID唯一
	 */
	@ApiModelProperty(value = "公司外部ID唯一")
	@Column(name = "cpid")
	private String cpid;
	/**
	 * 公司类型 1:系统公司 2:租户公司
	 */
	@ApiModelProperty(value = "公司类型 1:系统公司 2:租户公司")
	@Column(name = "type")
	private String type;
	/**
	 * 公司LOGO
	 */
	@ApiModelProperty(value = "公司LOGO")
	@Column(name = "logo")
	private String logo;
	/**
	 * 公司名称
	 */
	@ApiModelProperty(value = "公司名称")
	@Column(name = "name")
	private String name;
	/**
	 * 公司联系人
	 */
	@ApiModelProperty(value = "公司联系人")
	@Column(name = "contact_name")
	private String contactName;
	/**
	 * 公司联系人电话
	 */
	@ApiModelProperty(value = "公司联系人电话")
	@Column(name = "contact_mobile")
	private String contactMobile;
	/**
	 * 公司代号(缩写)
	 */
	@ApiModelProperty(value = "公司代号(缩写)")
	@Column(name = "code")
	private String code;
	/**
	 * 所属省代号
	 */
	@ApiModelProperty(value = "所属省代号")
	@Column(name = "province")
	private String province;
	/**
	 * 所属市代号
	 */
	@ApiModelProperty(value = "所属市代号")
	@Column(name = "city")
	private String city;
	/**
	 * 所属区/县代号
	 */
	@ApiModelProperty(value = "所属区/县代号")
	@Column(name = "county")
	private String county;
	/**
	 * 详细地址
	 */
	@ApiModelProperty(value = "详细地址")
	@Column(name = "address")
	private String address;
	/**
	 * 公司备注
	 */
	@ApiModelProperty(value = "公司备注")
	@Column(name = "description")
	private String description;
	/**
	 * 默认收货地址
	 */
	@ApiModelProperty(value = "默认收货地址")
	@Column(name = "default_address")
	private String defaultAddress;
	/**
	 * 允许创建的用户数
	 */
	@ApiModelProperty(value = "允许创建的用户数")
	@Column(name = "user_count")
	private Integer userCount;
	/**
	 * 过期时间
	 */
	@ApiModelProperty(value = "过期时间")
	@Column(name = "expired_at")
	private Long expiredAt;
	/**
	 * 顺丰月结卡号
	 */
	@ApiModelProperty(value = "顺丰月结卡号")
	@Column(name = "monthly_card")
	private String monthlyCard;
	/**
	 * 顺丰合作伙伴编码
	 */
	@ApiModelProperty(value = "顺丰合作伙伴编码")
	@Column(name = "partner_id")
	private String partnerId;
	/**
	 * 合作伙伴秘钥
	 */
	@ApiModelProperty(value = "合作伙伴秘钥")
	@Column(name = "secret")
	private String secret;
	/**
	 * 维修点模板编码
	 */
	@ApiModelProperty(value = "维修点模板编码")
	@Column(name = "template_code")
	private String templateCode;

	/**
	 * 父级公司ID
	 */
	@ApiModelProperty(value = "父级公司ID")
	@Column(name = "pcpid")
	private String pcpid;

	/**
	 * 维修点启用状态（1-启用，0-禁用）
	 */
	@Column(name = "activate")
	@ApiModelProperty(value = "维修点启用状态（1-启用，0-禁用）")
	private Integer activate;
}
