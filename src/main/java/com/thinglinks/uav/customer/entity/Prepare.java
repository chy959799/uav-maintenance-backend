package com.thinglinks.uav.customer.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/26 10:41
 */
@Entity
@Data
@Table(name = "prepares")
@TenantTable
public class Prepare extends BaseEntity {
    @Column(name = "prpid")
    @ApiModelProperty(value = "备品内部ID")
    private String prpid;

    @Column(name = "status")
    @ApiModelProperty(value = "备品状态 1:正常 2:故障 3:丢失 4:报废")
    private String status;

    @Column(name = "usage_status")
    @ApiModelProperty(value = "备品使用状态 1:闲置 2:在用 3:已报废 4:已转移")
    private String usageStatus;

    @Column(name = "remark")
    @ApiModelProperty(value = "备品备注")
    private String remark;

    @Column(name = "invalid_reason")
    @ApiModelProperty(value = "报废原因")
    private String invalidReason;

    @Column(name = "cpid")
    @ApiModelProperty(value = "公司ID")
    private String cpid;

    @Column(name = "devid")
    @ApiModelProperty(value = "设备ID")
    private String devid;
}
