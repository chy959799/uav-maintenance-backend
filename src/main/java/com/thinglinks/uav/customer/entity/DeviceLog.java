package com.thinglinks.uav.customer.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/24 23:23
 */
@Data
@Entity
@Table(name = "device_logs")
public class DeviceLog extends BaseEntity {
    @Column(name = "devlogid")
    @ApiModelProperty(value = "设备SN日志内部ID")
    private String devlogid;

    @Column(name = "new_code")
    @ApiModelProperty(value = "新的序列号")
    private String newCode;

    @Column(name = "old_code")
    @ApiModelProperty(value = "旧的序列号")
    private String oldCode;

    @Column(name = "remark")
    @ApiModelProperty(value = "日志备注")
    private String remark;

    @Column(name = "uid")
    @ApiModelProperty(value = "用户ID")
    private String uid;

    @Column(name = "devid")
    @ApiModelProperty(value = "设备ID")
    private String devid;

    @Column(name = "ordid")
    @ApiModelProperty(value = "订单ID")
    private String ordid;

    @Column(name = "lordid")
    @ApiModelProperty(value = "丢失订单ID")
    private String lordid;
}
