package com.thinglinks.uav.customer.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/24 23:12
 */
@Entity
@Data
@Table(name = "device_periods")
public class DevicePeriod extends BaseEntity {
    @Column(name = "perid")
    @ApiModelProperty(value = "设备保养周期ID")
    private String perid;

    @Column(name = "index")
    @ApiModelProperty(value = "第几次保养")
    private String index;

    @Column(name = "mainteance_at")
    @ApiModelProperty(value = "本次保养时间")
    private String mainteanceAt;

    @Column(name = "next_mainteance_at")
    @ApiModelProperty(value = "下次保养时间")
    private String nextMainteanceAt;

    @Column(name = "cid")
    @ApiModelProperty(value = "客户ID")
    private String cid;

    @Column(name = "devid")
    @ApiModelProperty(value = "设备ID")
    private String devid;
}
