package com.thinglinks.uav.customer.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/25 22:55
 */
@Entity
@Table(name = "contacts")
@Data
@TenantTable
public class Contact extends BaseEntity {
    @Column(name = "ctid")
    @ApiModelProperty("联系人内部ID唯一")
    private String ctid;

    @Column(name = "name", nullable = false)
    @ApiModelProperty("联系人姓名")
    private String name;

    @Column(name = "job")
    @ApiModelProperty("联系人职务")
    private String job;

    @Column(name = "department")
    @ApiModelProperty("所属部门")
    private String department;

    @Column(name = "telphone", nullable = false)
    @ApiModelProperty("联系人电话")
    private String telphone;

    @Column(name = "email")
    @ApiModelProperty("联系人邮箱")
    private String email;

    @Column(name = "`default`")
    @ApiModelProperty("是否默认联系人")
    @JsonProperty("default")
    private Boolean defaultContact;

    @Column(name = "cid")
    @ApiModelProperty("客户ID")
    private String cid;

    @Column(name = "cpid")
    @ApiModelProperty("公司ID")
    private String cpid;

}
