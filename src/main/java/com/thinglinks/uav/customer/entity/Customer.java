package com.thinglinks.uav.customer.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/**
 * (Customer)实体类
 *
 * @author iwiFool
 * @since 2024-03-17 20:39:36
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "customers")
@Data
@TenantTable
public class Customer extends BaseEntity {
	@Column(name = "cid")
	@ApiModelProperty("客户外部ID唯一")
	private String cid;

	@Column(name = "type", columnDefinition = "varchar(255) default '2'")
	@ApiModelProperty("客户类型 [1:单位客户, 2:个人客户, 3:系统客户-系统默认客户，用于挂载多租户公司自有设备]")
	private String type;

	@Column(name = "name", nullable = false)
	@ApiModelProperty("客户名，企业下唯一")
	private String name;

	@Column(name = "province")
	@ApiModelProperty("所属省代号")
	private String province;

	@Column(name = "city")
	@ApiModelProperty("所属市代号")
	private String city;

	@Column(name = "county")
	@ApiModelProperty("所属区/县代号")
	private String county;

	@Column(name = "address")
	@ApiModelProperty("详细地址")
	private String address;

	@Column(name = "remark")
	@ApiModelProperty("客户备注")
	private String remark;

	@Column(name = "cpid")
	@ApiModelProperty("公司ID")
	private String cpid;

	@Column(name = "pcid")
	@ApiModelProperty(value = "父级单位ID")
	private String pcid;

	@Transient
	private List<Customer> children;

	@ApiModelProperty(value = "父单位")
	@JsonProperty("parent")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pcid", referencedColumnName = "cid", insertable = false, updatable = false)
	private Customer customerParents;
}
