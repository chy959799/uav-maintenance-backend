package com.thinglinks.uav.customer.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.*;
import com.thinglinks.uav.customer.entity.Contact;
import com.thinglinks.uav.customer.service.ContactService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;

/**
 * @author huiyongchen
 */
@Api(tags = "单位联系人")
@RestController
@RequestMapping("contact")
public class ContactController {

	@Resource
	private ContactService contactService;

	@Authority(action = "customerUserManage")
	@PostMapping("")
	@Operation(summary = "新增联系人")
	public BaseResponse<String> insert(@RequestBody @Valid ContactDTO dto) {
		return contactService.insert(dto);
	}
	@GetMapping("")
	@Operation(summary = "分页查询联系人")
	public BasePageResponse<Contact> pageContact(ContactPageQuery request) {
		return contactService.pageContact(request);
	}
	@GetMapping("{ctid}")
	@Operation(summary = "获取联系人详情")
	public BaseResponse<ContactDetail> getContactDetail(@PathVariable("ctid") String ctid) {
		return contactService.getContactDetail(ctid);
	}

	@Authority(action = "customerUserManage")
	@PatchMapping("{ctid}")
	@Operation(summary = "修改联系人信息")
	public BaseResponse<Void> editContact(@PathVariable("ctid") String ctid, @RequestBody @Valid EditContact request) {
		return contactService.editContact(ctid, request);
	}

	@Authority(action = "customerUserManage")
	@DeleteMapping("{ctid}")
	@Operation(summary = "删除联系人")
	public BaseResponse<Void> deleteContact(@PathVariable("ctid") String ctid) {
		return contactService.deleteContact(ctid);
	}

	@Authority(action = "customerUserManage")
	@DeleteMapping("")
	@Operation(summary = "批量删除联系人")
	public BaseResponse<Void> deleteContacts(@RequestBody ArrayList<String> ctids) {
		return contactService.deleteContacts(ctids);
	}
}


