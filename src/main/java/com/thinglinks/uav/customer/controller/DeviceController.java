package com.thinglinks.uav.customer.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.device.*;
import com.thinglinks.uav.customer.service.DeviceService;
import com.thinglinks.uav.customer.vo.DeviceInfoVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * @author huiyongchen
 */
@Api(tags = "单位设备管理")
@RestController
@RequestMapping("devices")
public class DeviceController {

	@Resource
	private DeviceService deviceService;
	@Operation(summary = "企业设备分页查询")
	@GetMapping
	public BasePageResponse<PageDevice> pageDevice(PageDeviceQuery request) {
		return deviceService.pageDevice(request);

	}

	@Authority(action = "customerDeviceCreate")
	@Operation(summary = "新建设备")
	@PostMapping
	public BaseResponse<AddDeviceResponse> addDevice(@RequestBody @Valid AddDeviceRequest request){
		return deviceService.addDevice(request);
	}

	@Authority(action = "customerDeviceEdit")
	@Operation(summary = "更新设备信息")
	@PatchMapping("{devid}")
	public BaseResponse<Void> editDevice(@PathVariable("devid") String devid ,@RequestBody @Valid EditDeviceRequest request){
		return deviceService.editDevice(devid,request);
	}

	@Authority(action = "customerDeviceDelete")
	@Operation(summary = "删除")
	@DeleteMapping("{devid}")
	public BaseResponse<Void> delDevice(@PathVariable("devid") String devid){
		return deviceService.delDevice(devid);
	}

	@Operation(summary = "设备详情")
	@GetMapping("{devid}")
	public BaseResponse<DeviceInfoVO> deviceDetails(@PathVariable("devid") String devid){
		return deviceService.deviceDetails(devid);
	}
}


