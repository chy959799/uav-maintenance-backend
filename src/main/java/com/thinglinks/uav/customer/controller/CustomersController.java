package com.thinglinks.uav.customer.controller;

import com.thinglinks.uav.address.dto.AddressDTO;
import com.thinglinks.uav.address.service.AddressService;
import com.thinglinks.uav.address.vo.AddressVO;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.*;
import com.thinglinks.uav.customer.service.CustomerService;
import com.thinglinks.uav.customer.vo.CustomerVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * (Customer)表控制层
 *
 * @author iwiFool
 * @since 2024-03-17 20:39:36
 */
@Api(tags = "单位管理")
@RestController
@RequestMapping("customers")
public class CustomersController {

	@Resource
	private CustomerService customerService;

	@Resource
	private AddressService addressService;

	@Authority(action = "customerCreate")
	@PostMapping
	@Operation(summary = "新增单位")
	public BaseResponse<Void> addCustomer(@RequestBody @Valid CustomerDTO dto) {
		return customerService.addCustomer(dto);
	}

	@Authority(action = "customerDelete")
	@DeleteMapping("/{cid}")
	@Operation(summary = "删除指定单位")
	public BaseResponse<String> delete(@PathVariable("cid") String cid) {
		return customerService.deleteByCid(cid);
	}

	@Authority(action = "customerDelete")
	@DeleteMapping
	@Operation(summary = "批量删除单位")
	public BaseResponse<String> deleteAll(@RequestBody DeleteDTO dto) {
		return customerService.deleteAllByCids(dto);
	}

	@Authority(action = "customerEdit")
	@PatchMapping("/{cid}")
	@Operation(summary = "修改客户单位")
	public BaseResponse<CustomerVO> update(@PathVariable("cid") String cid, @RequestBody @Valid CustomerDTO dto) {
		return customerService.update(cid, dto);
	}

	@GetMapping
	@Operation(summary = "获取单位列表")
	public BasePageResponse<CustomerPage> queryCustomers(CustomerPageQuery pageQuery) {
		return customerService.queryCustomers(pageQuery);
	}
	@GetMapping("/tree")
	@Operation(summary = "树形单位列表")
	public BaseResponse<List<CustomerTree>> treeCustomer(String type) {
		return customerService.treeCustomer(type);
	}
	@GetMapping("/{cid}")
	@Operation(summary = "获取单位详情")
	public BaseResponse<CustomerVO> getCustomerInfo(@PathVariable String cid) {
		return customerService.getByCid(cid);
	}

	@Authority(action = "customerAddressCreate")
	@PostMapping("{cid}/address")
	@Operation(summary = "新增单位地址")
	public BaseResponse<String> insert(@PathVariable String cid,@RequestBody @Valid AddressDTO dto) {
		return addressService.insert(cid, dto);
	}

	@GetMapping("address")
	@Operation(summary = "获取单位地址列表")
	public BasePageResponse<AddressVO> getAddressPage(CustomerAddressRequest pageQuery) {
		return addressService.getPage(pageQuery.getCid(), pageQuery);
	}

	@Authority(action = "customerAddressManage")
	@PatchMapping("{cid}/defaultAddress/{aid}")
	@Operation(summary = "修改默认单位地址")
	public BaseResponse<String> update(@PathVariable("cid") String cid, @PathVariable("aid") String aid) {
		return addressService.update(cid, aid);
	}
	@GetMapping("{cid}/defaultAddress")
	@Operation(summary = "获取默认单位地址")
	public BaseResponse<AddressVO> getDefaultAddress(@PathVariable("cid") String cid) {
		return addressService.getDefault(cid);
	}
}


