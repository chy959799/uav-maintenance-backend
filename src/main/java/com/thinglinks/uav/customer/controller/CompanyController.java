package com.thinglinks.uav.customer.controller;

import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.customer.dto.CompanyDTO;
import com.thinglinks.uav.customer.dto.CompanyPageQuery;
import com.thinglinks.uav.customer.service.CompanyService;
import com.thinglinks.uav.customer.vo.CompanyInfoVO;
import com.thinglinks.uav.customer.vo.CompanyUserVO;
import com.thinglinks.uav.customer.vo.CompanyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Companies)表控制层
 *
 * @author iwiFool
 * @since 2024-03-19 18:39:45
 */
@Api(tags = "维修点管理")
@RestController
@RequestMapping("/companies")
@Slf4j
public class CompanyController {

	@Resource
	private CompanyService companyService;

	@PostMapping
	@Operation(summary = "新增维修点")
	public BaseResponse<String> insert(@RequestBody @Validated(CompanyDTO.Insert.class) CompanyDTO dto) {
		return companyService.insert(dto);
	}

	@Operation(summary = "删除维修点")
	@DeleteMapping("{cpid}")
	public BaseResponse<String> delete(@PathVariable String cpid) {
		return companyService.delete(cpid);
	}

	@Operation(summary = "批量删除维修点")
	@DeleteMapping
	public BaseResponse<String> deleteAll(@RequestBody DeleteDTO dto) {
		return companyService.deleteAll(dto);
	}

	@Operation(summary = "编辑维修点")
	@PatchMapping("{cpid}")
	public BaseResponse<String> update(@PathVariable String cpid,@RequestBody @Validated CompanyDTO dto) {
		return companyService.update(cpid, dto);
	}

	@Operation(summary = "修改维修点启用状态")
	@PatchMapping("{cpid}/enable")
	@ApiImplicitParam(name = "activate", value = "维修点启用状态（1-启用，0-禁用）", required = true, dataType = "Integer", paramType = "query")
	public BaseResponse<String> updateActivate(@PathVariable String cpid,@RequestParam("activate") Integer activate) {
		return companyService.updateActivateByCpid(cpid, activate);
	}

	@GetMapping
	@Operation(summary = "分页获取维修点列表")
	public BasePageResponse<CompanyVO> queryCustomers(CompanyPageQuery pageQuery) {
		return companyService.page(pageQuery);
	}

	@Operation(summary = "维修点基础信息")
	@GetMapping("{cpid}")
	public BaseResponse<CompanyInfoVO> getInfo(@PathVariable String cpid) {
		return companyService.getInfo(cpid);
	}

	@Operation(summary = "维修点用户信息")
	@GetMapping("{cpid}/users")
	public BasePageResponse<CompanyUserVO> getCompanyUsers(@PathVariable String cpid, BasePageRequest pageQuery) {
		return companyService.getCompanyUsers(cpid, pageQuery);
	}
}


