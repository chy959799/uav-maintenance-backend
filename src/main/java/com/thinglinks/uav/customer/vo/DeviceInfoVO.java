package com.thinglinks.uav.customer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author iwiFool
 * @date 2024/3/27
 */
@Data
@ApiModel(description = "设备详情 VO")
public class DeviceInfoVO {

	@ApiModelProperty(value = "设备信息")
	private DeviceInfo deviceInfo;

	@ApiModelProperty(value = "保险信息")
	private InsuranceInfo insuranceInfo;

	@Data
	public static class DeviceInfo {

		@ApiModelProperty("产品名称")
		private String productName;

		@ApiModelProperty(value = "产品类型")
		private String categoryName;

		@ApiModelProperty(value = "品牌")
		private String brandName;

		@ApiModelProperty(value = "规格型号")
		private String model;

		@ApiModelProperty(value = "设备SN码")
		private String code;

		@ApiModelProperty(value = "物料编码")
		private String materialCode;

		@ApiModelProperty(value = "生产日期")
		private String produceAt;

		@ApiModelProperty(value = "激活时间")
		private String activateAt;

		@ApiModelProperty(value = "设备描述")
		private String description;
	}

	@Data
	public static class InsuranceInfo {

		@ApiModelProperty(value = "保单编号")
		private String insuranceCode;

		@ApiModelProperty(value = "保险类型[8:鼎和财险]", example = "8")
		private String insuranceType;

		@ApiModelProperty(value = "保险注册号")
		private String registrationNumber;

		@ApiModelProperty(value = "累计理赔金额")
		private BigDecimal amount;

		@ApiModelProperty(value = "保险生效日期")
		private String insuranceStartAt;

		@ApiModelProperty(value = "保险失效日期")
		private String insuranceExpireAt;
	}
}
