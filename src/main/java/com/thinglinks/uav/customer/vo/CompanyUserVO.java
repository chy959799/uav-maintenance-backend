package com.thinglinks.uav.customer.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/7/29
 */
@Data
public class CompanyUserVO {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名", example = "云南电力无人")
    private String username;

    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称", example = "叮叮当")
    private String nickName;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别", example = "1")
    private String gender;

    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话", example = "13688803497")
    private String mobile;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱", example = "13688803497@163.com")
    private String email;

    /**
     * 最近登录IP
     */
    @ApiModelProperty(value = "最近登录IP", example = "112.96.134.140")
    private String loginIp;

    /**
     * 最近登录时间
     */
    @ApiModelProperty(value = "最近登录时间", example = "2024-07-05 00:00:00")
    private String loginTime;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", example = "1")
    private Integer activate;
}
