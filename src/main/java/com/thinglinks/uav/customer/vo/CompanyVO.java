package com.thinglinks.uav.customer.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/19
 */
@Data
public class CompanyVO {

	/**
	 * cpid
	 */
	@ApiModelProperty(value = "cpid")
	private String cpid;

	/**
	 * 维修点
	 */
	@ApiModelProperty(value = "维修点", example = "维修点")
	private String name;

	/**
	 * 所在区域-所属省代号
	 */
	@ApiModelProperty(value = "所在区域-所属省代号")
	private String province;

	/**
	 * 所在区域-所属市代号
	 */
	@ApiModelProperty(value = "所在区域-所属市代号")
	private String city;

	/**
	 * 所在区域-所属区/县代号
	 */
	@ApiModelProperty(value = "所在区域-所属区/县代号")
	private String county;

	/**
	 * 详细地址
	 */
	@ApiModelProperty(value = "详细地址")
	private String address;

	/**
	 * 联系人
	 */
	@ApiModelProperty(value = "联系人")
	private String contactName;

	/**
	 * 联系电话
	 */
	@ApiModelProperty(value = "联系电话")
	private String contactMobile;

	/**
	 * 授权用户数
	 */
	@ApiModelProperty(value = "授权用户数", example = "10")
	private Integer userCount;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间", example = "2024-01-01 12:00:00")
	private String createdAt;

	/**
	 * 过期时间
	 */
	@ApiModelProperty(value = "过期时间", example = "2024-01-01 12:00:00")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private String expiredAt;

	/**
	 * 维修点启用状态（1-启用，0-禁用）
	 */
	@ApiModelProperty(value = "维修点启用状态（1-启用，0-禁用）", example = "1")
	private Integer activate;

	/**
	 * 维修点备注
	 */
	@ApiModelProperty(value = "维修点备注")
	private String description;

	/**
	 * 维修点模板编码
	 */
	@ApiModelProperty(value = "维修点模板编码")
	private String templateCode;
}

