package com.thinglinks.uav.customer.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/7/29
 */
@Data
public class CompanyInfoVO {

    /**
     * 维修点名称
     */
    @ApiModelProperty(value = "维修点名称")
    private String name;

    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人")
    private String contactName;

    /**
     * 联系人电话
     */
    @ApiModelProperty(value = "联系人电话")
    private String contactMobile;

    /**
     * 所在区域-所属省代号
     */
    @ApiModelProperty(value = "所在区域-所属省代号")
    private String province;

    /**
     * 所在区域-所属市代号
     */
    @ApiModelProperty(value = "所在区域-所属市代号")
    private String city;

    /**
     * 所在区域-所属区/县代号
     */
    @ApiModelProperty(value = "所在区域-所属区/县代号")
    private String county;

    /**
     * 授权用户数
     */
    @ApiModelProperty(value = "授权用户数")
    private Integer userCount;

    /**
     * 顺丰月结卡号
     */
    @ApiModelProperty(value = "顺丰月结卡号")
    private String monthlyCard;

    /**
     * 合作伙伴编号
     */
    @ApiModelProperty(value = "合作伙伴编号")
    private String partnerId;

    /**
     * 合作伙伴秘钥
     */
    @ApiModelProperty(value = "合作伙伴秘钥")
    private String secret;

    /**
     * 过期时间
     */
    @ApiModelProperty(value = "过期时间")
    private String expiredAt;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private String updatedAt;

    /**
     * 维修点备注
     */
    @ApiModelProperty(value = "维修点备注")
    private String description;

    /**
     * 维修点模板
     */
    @ApiModelProperty(value = "维修点模板")
    private String templateCode;
}
