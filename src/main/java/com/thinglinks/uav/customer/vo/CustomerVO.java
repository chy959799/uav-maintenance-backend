package com.thinglinks.uav.customer.vo;

import com.thinglinks.uav.customer.entity.Customer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@Data
@ApiModel(description = "客户信息 VO")
public class CustomerVO {
	@ApiModelProperty(value = "地址", example = "北京市朝阳区")
	private String address;

	@ApiModelProperty(value = "唯一标识ID", example = "123456789")
	private String cid;

	@ApiModelProperty(value = "城市", example = "北京")
	private String city;

	@ApiModelProperty(value = "县区", example = "朝阳区")
	private String county;

	@ApiModelProperty(value = "所属企业ID", example = "987654321")
	private String cpid;

	@ApiModelProperty(value = "名称", example = "ABC公司")
	private String name;

	@ApiModelProperty(value = "省份", example = "北京市")
	private String province;

	@ApiModelProperty(value = "备注", example = "重要客户")
	private String remark;

	@ApiModelProperty(value = "规模", example = "大型企业")
	private String scale;

	@ApiModelProperty(value = "类型", example = "合作伙伴")
	private String type;

	@ApiModelProperty(value = "创建时间", example = "2021-01-01 10:00:00")
	private String createdAt;

	@ApiModelProperty(value = "更新时间", example = "2021-02-01 15:30:00")
	private String updatedAt;

	@ApiModelProperty(value = "父级单位", example = "987654321")
	private Customer customerParents;
}
