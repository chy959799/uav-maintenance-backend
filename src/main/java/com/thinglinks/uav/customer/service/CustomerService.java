package com.thinglinks.uav.customer.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.*;
import com.thinglinks.uav.customer.vo.CustomerVO;

import java.util.List;

/**
 * (Customer)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-17 20:39:36
 */
public interface CustomerService {

	BaseResponse<String> insert(CustomerDTO customer);

	BaseResponse<String> deleteByCid(String cid);

	BaseResponse<String> deleteAllByCids(DeleteDTO ids);

	BaseResponse<CustomerVO> update(String cid, CustomerDTO dto);

	BasePageResponse<CustomerVO> getPage(CustomerPageQuery pageQuery);

	BaseResponse<CustomerVO> getByCid(String cid);

    BaseResponse<Void> addCustomer(CustomerDTO dto);

	BaseResponse<List<CustomerTree>> treeCustomer(String type);

	BasePageResponse<CustomerPage> queryCustomers(CustomerPageQuery pageQuery);
}


