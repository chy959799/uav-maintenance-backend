package com.thinglinks.uav.customer.service.impl;

import com.thinglinks.uav.customer.repository.DeviceLogRepository;
import com.thinglinks.uav.customer.service.DeviceLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/24 23:16
 */
@Service
public class DeviceLogServiceImpl implements DeviceLogService {
    @Resource
    private DeviceLogRepository deviceLogRepository;
}
