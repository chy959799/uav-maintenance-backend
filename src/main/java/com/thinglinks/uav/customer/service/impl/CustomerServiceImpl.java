package com.thinglinks.uav.customer.service.impl;

import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.dto.*;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.repository.ContactRepository;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.service.CustomerService;
import com.thinglinks.uav.customer.vo.CustomerVO;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (Customer)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-17 20:39:36
 */
@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {
	@Resource
	private CustomersRepository customerRepository;
	@Resource
	private ContactRepository contactRepository;
	@Resource
	private UserRepository userRepository;
	@Resource
	private AddressRepository addressRepository;
	@Resource
	private OrderRepository orderRepository;
	@Override
	@Transactional
	public BaseResponse<String> deleteAllByCids(DeleteDTO ids) {
		if (customerRepository.existsByPcidIn(ids.getIds())||orderRepository.existsByCidIn(ids.getIds())
		||addressRepository.existsByCidIn(ids.getIds())||userRepository.existsByCidIn(ids.getIds())){
			return BaseResponse.fail("存在关联不能删除");
		}
		customerRepository.deleteAllByCidIn(ids.getIds());
		return BaseResponse.success();
	}

	@Override
	public BasePageResponse<CustomerVO> getPage(CustomerPageQuery pageQuery) {

		Specification<Customer> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			if (pageQuery.getType() != null && !pageQuery.getType().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("type"), pageQuery.getType()));
			}
			if (pageQuery.getName() != null && !pageQuery.getName().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("name"), "%" + pageQuery.getName() + "%"));
			}
			return predicate;
		};

		Page<Customer> customerPage = customerRepository.findAll(specification, pageQuery.of());
		List<CustomerVO> customerVOList = customerPage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(customerPage, customerVOList);
	}
	@Override
	public BasePageResponse<CustomerPage> queryCustomers(CustomerPageQuery request) {
		PageRequest pageRequest = request.of();
		Specification<Customer> specification = (root, criteriaQuery, criteriaBuilder)->{
			List<Predicate> predicates = new ArrayList<>();

			if (Objects.nonNull(request.getType())){
				predicates.add(criteriaBuilder.equal(root.get("type"), request.getType()));
			}

			if (Objects.nonNull(request.getCid())){
				List<Customer> all = customerRepository.findAll();
				List<CustomerTree> collect = all.stream().map(c -> {
					CustomerTree customerTree = new CustomerTree();
					BeanUtils.copyProperties(c, customerTree);
					return customerTree;
				}).collect(Collectors.toList());

				List<CustomerTree> allList = CustomerTree.buildTree2(request.getCid(), collect);
				List<String> customerIds = getCustomerIds(allList);
				customerIds.add(request.getCid());
				predicates.add(root.get("cid").in(criteriaBuilder.literal(customerIds)));
			}

			if (Objects.nonNull(request.getName())){
				predicates.add(criteriaBuilder.like(root.get("name"), "%"+request.getName()+"%"));
			}

			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		};

		Page<Customer> page = customerRepository.findAll(specification, pageRequest);
		List<CustomerPage> customerPages = page.stream().map(c -> {
			CustomerPage customerPage = new CustomerPage();
			BeanUtils.copyProperties(c, customerPage);
			customerPage.setCreatedAt(DateUtils.formatDateTime(c.getCt()));
			customerPage.setUpdatedAt(DateUtils.formatDateTime(c.getUt()));
			if (Objects.nonNull(c.getPcid())){
				Customer parent = customerRepository.findByCid(c.getPcid());
				customerPage.setParent(parent);
			}
			return customerPage;
		}).collect(Collectors.toList());
		return BasePageResponse.success(page, customerPages);
	}

	public List<String> getCustomerIds(List<CustomerTree> customerList) {
		List<String> customerIds = new ArrayList<>();
		for (CustomerTree c : customerList) {
			customerIds.add(c.getCid());
			customerIds.addAll(getCustomerIds(c.getChildren()));
		}
		return customerIds;
	}
	@Override
	public BaseResponse<CustomerVO> getByCid(String cid) {
		Customer customer = customerRepository.getByCid(cid);
		CustomerVO customerVO = new CustomerVO();
		BeanUtils.copyProperties(customer, customerVO);
		customerVO.setCustomerParents(customer.getCustomerParents());
		return BaseResponse.success(customerVO);
	}

	@Override
	public BaseResponse<CustomerVO> update(String cid, CustomerDTO dto) {
		Customer customer = customerRepository.getByCid(cid);
		if (customer != null) {
			BeanUtils.copyProperties(dto, customer);
			customerRepository.save(customer);
			Customer customer1 = customerRepository.getByCid(cid);
			CustomerVO customerVO = new CustomerVO();
			BeanUtils.copyProperties(customer1, customerVO);
			customerVO.setCustomerParents(customer1.getCustomerParents());
			return BaseResponse.success(customerVO);
		} else {
			return BaseResponse.fail("客户信息不存在");
		}
	}

	private CustomerVO convertToVO(Customer customer) {
		CustomerVO customerVO = new CustomerVO();
		BeanUtils.copyProperties(customer, customerVO);
		customerVO.setCreatedAt(DateUtils.formatDateTime(customer.getCt()));
		customerVO.setUpdatedAt(DateUtils.formatDateTime(customer.getUt()));
		return customerVO;
	}

	@Override
	public BaseResponse<String> insert(CustomerDTO dto) {
		Customer customer = new Customer();
		BeanUtils.copyProperties(dto, customer);
		customer.setCid(CommonUtils.uuid());
		customer.setCpid(CommonUtils.getCpid());
		customerRepository.save(customer);
		return BaseResponse.success();
	}
	@Override
	public BaseResponse<Void> addCustomer(CustomerDTO request) {
		Customer customer = new Customer();
		BeanUtils.copyProperties(request, customer);

		if (customerRepository.existsByName(request.getName())) {
			return BaseResponse.fail("单位名重复");
		}
		else {
			Customer parent = customerRepository.findByCid(request.getPcid());
			customer.setCustomerParents(parent);
		}
		customer.setCid(CommonUtils.uuid());
		customerRepository.save(customer);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<List<CustomerTree>> treeCustomer(String type) {
		List<Customer> customers;
		if(type != null){
			customers = customerRepository.findAllByType(type);
		}else {
			customers = customerRepository.findAll();
		}
			List<CustomerTree> customerTrees = customers.stream()
					.map(customer -> {
						CustomerTree customerTree = new CustomerTree();
						BeanUtils.copyProperties(customer, customerTree);
						return customerTree;
					})
					.collect(Collectors.toList());
			List<CustomerTree> tree = CustomerTree.buildTree(customerTrees);
			return BaseResponse.success(tree);
	}

	@Override
	@Transactional
	public BaseResponse<String> deleteByCid(String cid) {
		if (customerRepository.existsByPcid(cid)||userRepository.existsByCid(cid)||addressRepository.existsByCid(cid)||orderRepository.existsByCid(cid)){
			return BaseResponse.fail("存在关联不能删除");
		}
		if (!customerRepository.existsByCid(cid)){
			return BaseResponse.fail("单位信息不存在");
		}
		customerRepository.deleteByCid(cid);
		return BaseResponse.success();
	}
}


