package com.thinglinks.uav.customer.service.impl;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.customer.dto.ContactDTO;
import com.thinglinks.uav.customer.dto.ContactDetail;
import com.thinglinks.uav.customer.dto.ContactPageQuery;
import com.thinglinks.uav.customer.dto.EditContact;
import com.thinglinks.uav.customer.entity.Contact;
import com.thinglinks.uav.customer.repository.ContactRepository;
import com.thinglinks.uav.customer.service.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author huiyongchen
 */
@Slf4j
@Service
public class ContactServiceImpl implements ContactService {
	@Resource
	private ContactRepository contactRepository;

	@Override
	public BaseResponse<String> insert(ContactDTO dto) {
		Contact contact = new Contact();
		contact.setCtid(CommonUtils.uuid());
		contact.setName(dto.getName());
		contact.setJob(dto.getJob());
		contact.setDepartment(dto.getDepartment());
		contact.setTelphone(dto.getTelphone());
		contact.setEmail(dto.getEmail());
		return BaseResponse.success(contactRepository.save(contact).getId().toString());
	}

	@Override
	public BasePageResponse<Contact> pageContact(ContactPageQuery request) {
		PageRequest pageRequest = request.of();
		String cid = request.getCid();
		String name = request.getName();
		String job = request.getJob();
		String department = request.getDepartment();
		Contact contact = new Contact();
		contact.setName(name);
		contact.setJob(job);
		contact.setCid(cid);
		contact.setDepartment(department);
		contact.setDepartment(department);
		ExampleMatcher matcher = ExampleMatcher.matching()
				.withMatcher("cid", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("job", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("department", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains());
		Example<Contact> example = Example.of(contact, matcher);
		Page<Contact> page = contactRepository.findAll(example, pageRequest);
		List<Contact> collect = page.stream().map(c -> {
			return c;
		}).collect(Collectors.toList());
		return BasePageResponse.success(page, collect);
	}

	@Override
	public BaseResponse<ContactDetail> getContactDetail(String ctid) {
		if (contactRepository.findByCtid(ctid)!=null){
			ContactDetail contactDetail = new ContactDetail();
			Contact contact = contactRepository.findByCtid(ctid);
			BeanUtils.copyProperties(contact,contactDetail);
			return BaseResponse.success(contactDetail);
		}
		return BaseResponse.fail();
	}
	@Override
	public BaseResponse<Void> deleteContact(String ctid) {
		if (contactRepository.deleteByCtid(ctid)==1){
			return BaseResponse.success();
		}
		return BaseResponse.fail();
	}

	@Override
	public BaseResponse<Void> deleteContacts(ArrayList<String> ctids) {
		if (ctids.size()==0){
			return BaseResponse.fail();
		}
		for (String s:ctids
		) {
			if (contactRepository.findByCtid(s)==null){
				return BaseResponse.fail();
			}
		}
		for (String s:ctids
		) {
			contactRepository.deleteByCtid(s);
		}
		return BaseResponse.success();
	}
	@Override
	public BaseResponse<Void> editContact(String ctid, EditContact request) {
		if (contactRepository.findByCtid(ctid) == null) {
			return BaseResponse.fail("联系人不存在");
		}
		Contact c = contactRepository.findByCtid(ctid);
		c.setName(request.getName());
		c.setDepartment(request.getDepartment());
		c.setJob(request.getJob());
		c.setTelphone(request.getTelphone());
		c.setEmail(request.getEmail());
		c.setCid(request.getCid());
		contactRepository.save(c);
		return BaseResponse.success();
	}
}


