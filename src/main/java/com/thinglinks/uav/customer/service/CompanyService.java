package com.thinglinks.uav.customer.service;

import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.customer.dto.CompanyDTO;
import com.thinglinks.uav.customer.dto.CompanyPageQuery;
import com.thinglinks.uav.customer.vo.CompanyInfoVO;
import com.thinglinks.uav.customer.vo.CompanyUserVO;
import com.thinglinks.uav.customer.vo.CompanyVO;

/**
 * (Companies)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-19 18:39:46
 */
public interface CompanyService {

    BaseResponse<String> insert(CompanyDTO dto);

    BaseResponse<String> delete(String cpid);

    BaseResponse<String> deleteAll(DeleteDTO dto);

    BaseResponse<String> update(String cpid, CompanyDTO dto);

    BasePageResponse<CompanyVO> page(CompanyPageQuery pageQuery);

    BaseResponse<CompanyInfoVO> getInfo(String cpid);

    BasePageResponse<CompanyUserVO> getCompanyUsers(String cpid, BasePageRequest pageQuery);

    BaseResponse<String> updateActivateByCpid(String cpid, Integer activate);

}


