package com.thinglinks.uav.customer.service.impl;

import com.thinglinks.uav.customer.repository.DevicePeriodRepository;
import com.thinglinks.uav.customer.service.DevicePeriodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/24 23:16
 */
@Service
public class DevicePeriodServiceImpl implements DevicePeriodService {
    @Resource
    private DevicePeriodRepository devicePeriodRepository;
}
