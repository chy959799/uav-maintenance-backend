package com.thinglinks.uav.customer.service;


import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.ContactDTO;
import com.thinglinks.uav.customer.dto.ContactDetail;
import com.thinglinks.uav.customer.dto.ContactPageQuery;
import com.thinglinks.uav.customer.dto.EditContact;
import com.thinglinks.uav.customer.entity.Contact;

import java.util.ArrayList;

/**
 * @author huiyongchen
 */
public interface ContactService {
    BaseResponse<String> insert(ContactDTO dto);

    BasePageResponse<Contact> pageContact(ContactPageQuery request);

    BaseResponse<ContactDetail> getContactDetail(String ctid);

    BaseResponse<Void> deleteContact(String ctid);

    BaseResponse<Void> deleteContacts(ArrayList<String> ctids);

    BaseResponse<Void> editContact(String ctid, EditContact request);
}


