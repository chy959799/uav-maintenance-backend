package com.thinglinks.uav.customer.service.impl;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.dto.device.*;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.customer.service.DeviceService;
import com.thinglinks.uav.customer.vo.DeviceInfoVO;
import com.thinglinks.uav.insurance.service.InsuranceDeviceService;
import com.thinglinks.uav.insurance.vo.InsuranceDeviceInfoVO;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/24 23:16
 */
@Service
public class DeviceServiceImpl implements DeviceService {

    static String DEVICE_DEFAULT_STATUS = "1";
    @Resource
    private DeviceRepository deviceRepository;
    @Resource
    private OrderRepository orderRepository;
    @Resource
    private InsuranceDeviceService insuranceDeviceService;
    @Autowired
    private UserRepository userRepository;

    @Override
    public BasePageResponse<PageDevice> pageDevice(PageDeviceQuery request) {
        PageRequest pageRequest = request.of();
        Specification<Device> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if(Objects.nonNull(request.getPid())){
                predicates.add(criteriaBuilder.equal(root.get("pid"), request.getPid()));
            }

            if (Objects.nonNull(request.getCode())) {
                predicates.add(criteriaBuilder.like(root.get("code"), CommonUtils.assembleLike(request.getCode())));
            }
            if (Objects.nonNull(request.getName())) {
                Join<Device, Product> productJoin = root.join("product", JoinType.LEFT);
                predicates.add(criteriaBuilder.like(productJoin.get("name"), CommonUtils.assembleLike(request.getName())));
            }
            if(Objects.nonNull(request.getCid())){
                Join<Device, Customer> customerJoin = root.join("customer",JoinType.LEFT);
                predicates.add(criteriaBuilder.equal(customerJoin.get("cid"), request.getCid()));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<Device> page = deviceRepository.findAll(specification, pageRequest);
        List<PageDevice> result = page.stream().map(d -> {
            PageDevice data = new PageDevice();
            BeanUtils.copyProperties(d, data);
            data.setCreatedTime(DateUtils.formatDateTime(d.getCt()));
            data.setUpdatedTime(DateUtils.formatDateTime(d.getUt()));


            data.setActivateTime(d.getActivateAt() != null ? DateUtils.formatDateTime(d.getActivateAt()) : "");
            data.setProduceTime(d.getProduceAt() != null ? DateUtils.formatDateTime(d.getProduceAt()) : "");
            data.setLastestMainteanceTime(d.getLastestMainteanceAt() != null ? DateUtils.formatDateTime(d.getLastestMainteanceAt()) : "");
            return data;
        }).collect(Collectors.toList());

        return BasePageResponse.success(page, result);
    }

    @Override
    public BaseResponse<AddDeviceResponse> addDevice(AddDeviceRequest request) {
        if (deviceRepository.existsByCode(request.getCode())) {
            return BaseResponse.fail("设备编号已经存在");
        }
        Device device = new Device();
        BeanUtils.copyProperties(request, device);
        if (Objects.nonNull(request.getProduceTime())){
            device.setProduceAt(DateUtils.parseDateStringToTimestamp(request.getProduceTime()));
        }
        if (Objects.nonNull(request.getActivateTime())){
            device.setActivateAt(DateUtils.parseDateStringToTimestamp(request.getActivateTime()));
        }
        device.setStatus(DEVICE_DEFAULT_STATUS);
        device.setInnerCode(UUID.randomUUID().toString());
        device.setDevid(CommonUtils.uuid());
        User user = userRepository.findByUid(CommonUtils.getUid());
        device.setCid(user.getCid());
        Device newDevice = deviceRepository.save(device);
        AddDeviceResponse result = new AddDeviceResponse();
        BeanUtils.copyProperties(newDevice, result);
        result.setCreatedTime(DateUtils.formatDateTime(newDevice.getCt()));
        result.setUpdatedTime(DateUtils.formatDateTime(newDevice.getUt()));
        return BaseResponse.success(result);
    }

    @Override
    public BaseResponse<Void> editDevice(String devid, EditDeviceRequest request) {
        Device device = deviceRepository.findByDevid(devid);
        device.setPid(request.getPid());
        if (!request.getCode().equals(device.getCode()) && deviceRepository.existsByCode(request.getCode())) {
            return BaseResponse.fail("设备SN号已存在");
        } else {
            device.setCode(request.getCode());
        }
        Optional.ofNullable(request.getProduceTime()).map(DateUtils::parseDateStringToTimestamp).ifPresent(device::setProduceAt);
        Optional.ofNullable(request.getActivateTime()).map(DateUtils::parseDateStringToTimestamp).ifPresent(device::setActivateAt);
        Optional.ofNullable(request.getMaterialCode()).ifPresent(device::setMaterialCode);
        Optional.ofNullable(request.getDescription()).ifPresent(device::setDescription);
        deviceRepository.save(device);
        return BaseResponse.success();
    }

    @Transactional
    @Override
    // TODO
    public BaseResponse<Void> delDevice(String devid) {
        if (orderRepository.existsByDevid(devid)){
            return BaseResponse.fail("删除失败，存在关联设备");
        }
        deviceRepository.deleteByDevid(devid);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<DeviceInfoVO> deviceDetails(String devid) {
        Device device = deviceRepository.findByDevid(devid);
        return BaseResponse.success(convertToInfoVO(device));
    }

    private DeviceInfoVO convertToInfoVO(Device device) {
        DeviceInfoVO deviceInfoVO = new DeviceInfoVO();

        DeviceInfoVO.DeviceInfo deviceInfo = new DeviceInfoVO.DeviceInfo();
        Product product = device.getProduct();
        deviceInfo.setProductName(product.getName());
        Category productCategory = product.getCategory();
        deviceInfo.setCategoryName(productCategory.getName());
        // TODO 品牌
        deviceInfo.setModel(product.getModel());
        deviceInfo.setCode(device.getCode());
        deviceInfo.setMaterialCode(device.getMaterialCode());
        deviceInfo.setProduceAt(DateUtils.formatDateTime(device.getProduceAt()));
        deviceInfo.setActivateAt(DateUtils.formatDateTime(device.getActivateAt()));
        deviceInfo.setDescription(device.getDescription());
        deviceInfoVO.setDeviceInfo(deviceInfo);

        DeviceInfoVO.InsuranceInfo insuranceInfo = new DeviceInfoVO.InsuranceInfo();
        InsuranceDeviceInfoVO insuranceDeviceInfo = insuranceDeviceService.getInsuranceInfoByCode(device.getCode());
        if (Objects.nonNull(insuranceDeviceInfo)) {
            insuranceInfo.setInsuranceCode(insuranceDeviceInfo.getInsuranceCode());
            insuranceInfo.setAmount(insuranceDeviceInfo.getAmount());
            insuranceInfo.setInsuranceExpireAt(DateUtils.formatDateTime(insuranceDeviceInfo.getInsuranceExpireAt()));
            insuranceInfo.setInsuranceStartAt(DateUtils.formatDateTime(insuranceDeviceInfo.getInsuranceStartAt()));
            insuranceInfo.setRegistrationNumber(insuranceDeviceInfo.getRegistrationNumber());
            insuranceInfo.setInsuranceType(insuranceDeviceInfo.getInsuranceType());
        }
        deviceInfoVO.setInsuranceInfo(insuranceInfo);

        return deviceInfoVO;
    }
}
