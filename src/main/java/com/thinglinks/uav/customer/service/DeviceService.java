package com.thinglinks.uav.customer.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.device.*;
import com.thinglinks.uav.customer.vo.DeviceInfoVO;

/**
 * @author huiyongchen
 */
public interface DeviceService {
    BasePageResponse<PageDevice> pageDevice(PageDeviceQuery request);

    BaseResponse<AddDeviceResponse> addDevice(AddDeviceRequest request);

    BaseResponse<Void> editDevice(String devid, EditDeviceRequest request);

    BaseResponse<Void> delDevice(String devid);

    BaseResponse<DeviceInfoVO> deviceDetails(String devid);
}
