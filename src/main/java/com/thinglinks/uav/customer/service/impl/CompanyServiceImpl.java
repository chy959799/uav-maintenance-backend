package com.thinglinks.uav.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.CompanyConstants;
import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.common.utils.AbbreviationUtils;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.dto.CompanyDTO;
import com.thinglinks.uav.customer.dto.CompanyPageQuery;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.repository.CompanyRepository;
import com.thinglinks.uav.customer.service.CompanyService;
import com.thinglinks.uav.customer.vo.CompanyInfoVO;
import com.thinglinks.uav.customer.vo.CompanyUserVO;
import com.thinglinks.uav.customer.vo.CompanyVO;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.template.repository.TemplatesRepository;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (Companies)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-19 18:39:46
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Resource
    private CompanyRepository companyRepository;
    @Resource
    private UserRepository userRepository;
    @Resource
    private TemplatesRepository templatesRepository;

    @Override
    public BaseResponse<String> insert(CompanyDTO dto) {
        Company company = new Company();
        BeanUtils.copyProperties(dto, company);
        company.setCpid(CommonUtils.uuid());
        company.setCode(AbbreviationUtils.getAbbreviation(dto.getName()));
        company.setType(CompanyConstants.COMPANY_TYPE_TENANT);
        company.setExpiredAt(DateUtils.parseDateStringToTimestamp(dto.getExpiredAt()));
        company.setActivate(CommonConstants.TURE_VALUE);
        companyRepository.save(company);
        return BaseResponse.success();
    }

    @Transactional
    @Override
    public BaseResponse<String> delete(String cpid) {
        if (userRepository.countByCpid(cpid) < 0) {
            throw new BusinessException("维修点下存在用户，无法删除");
        }
        companyRepository.deleteByCpid(cpid);
        return BaseResponse.success();
    }

    @Transactional
    @Override
    public BaseResponse<String> deleteAll(DeleteDTO dto) {
        if (userRepository.countByCpidIn(dto.getIds()) > 0) {
            throw new BusinessException("维修点下存在用户，无法删除");
        }
        companyRepository.deleteAllByCpidIn(dto.getIds());
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<String> update(String cpid, CompanyDTO dto) {
        Company company = companyRepository.findByCpid(cpid);
        if (Objects.isNull(company)) {
            throw new BusinessException("维修点不存在");
        }

        company.setTemplateCode(dto.getTemplateCode());
        company.setContactName(dto.getContactName());
        company.setContactMobile(dto.getContactMobile());
        company.setProvince(dto.getProvince());
        company.setCity(dto.getCity());
        company.setCounty(dto.getCounty());
        company.setAddress(dto.getAddress());
        company.setUserCount(dto.getUserCount());
        company.setExpiredAt(DateUtils.parseDateStringToTimestamp(dto.getExpiredAt()));
        company.setDescription(dto.getDescription());
        companyRepository.save(company);
        return BaseResponse.success();
    }

    @Override
    public BasePageResponse<CompanyVO> page(CompanyPageQuery pageQuery) {
        Specification<Company> specification = (root, query, builder) -> {
            Predicate predicate = builder.conjunction();

            if (StringUtils.isNotBlank(pageQuery.getName())) {
                predicate = builder.and(predicate, builder.like(root.get("name"), CommonUtils.assembleLike(pageQuery.getName())));
            }

            return predicate;
        };

        Page<Company> page = companyRepository.findAll(specification, pageQuery.of());
        List<CompanyVO> list = page.getContent().stream()
                .map(this::convertToVO)
                .collect(Collectors.toList());

        return BasePageResponse.success(page, list);
    }

    @Override
    public BaseResponse<CompanyInfoVO> getInfo(String cpid) {
        Company company = companyRepository.findByCpid(cpid);
        return BaseResponse.success(convertToInfoVO(company));
    }

    @Override
    public BasePageResponse<CompanyUserVO> getCompanyUsers(String cpid, BasePageRequest pageQuery) {
        Specification<User> specification = (root, query, builder) -> {
            Predicate predicate = builder.conjunction();
            predicate = builder.and(predicate, builder.equal(root.get("cpid"), cpid));

            return predicate;
        };
        Page<User> page = userRepository.findAll(specification, pageQuery.of());
        List<CompanyUserVO> list = page.getContent().stream()
                .map(user -> {
                    CompanyUserVO companyUserVO = new CompanyUserVO();
                    BeanUtil.copyProperties(user, companyUserVO);
                    companyUserVO.setUsername(user.getUserName());
                    companyUserVO.setLoginTime(DateUtils.formatDateTime(user.getLoginTime()));
                    return companyUserVO;
                }).collect(Collectors.toList());
        return BasePageResponse.success(page, list);
    }

    @Override
    public BaseResponse<String> updateActivateByCpid(String cpid, Integer activate) {
        Company company = companyRepository.findByCpid(cpid);
        if (Objects.isNull(company)) {
            throw new BusinessException("维修点不存在");
        }

        company.setActivate(activate);
        companyRepository.save(company);
        return BaseResponse.success();
    }

    private CompanyInfoVO convertToInfoVO(Company company) {
        CompanyInfoVO infoVO = new CompanyInfoVO();
        BeanUtil.copyProperties(company, infoVO);
        infoVO.setCreatedAt(DateUtils.formatDateTime(company.getCt()));
        infoVO.setUpdatedAt(DateUtils.formatDateTime(company.getUt()));
        infoVO.setExpiredAt(DateUtils.formatDateTime(company.getExpiredAt()));
        return infoVO;
    }

    private CompanyVO convertToVO(Company company) {
        CompanyVO vo = new CompanyVO();
        BeanUtils.copyProperties(company, vo);
        vo.setCreatedAt(DateUtils.formatDateTime(company.getCt()));
        vo.setExpiredAt(DateUtils.formatDateTime(company.getExpiredAt()));
        return vo;
    }
}


