package com.thinglinks.uav.category.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * (Categories)实体类
 *
 * @author makejava
 * @since 2024-03-20 11:43:40
 */
@Entity
@Table(name = "categories")
@Data
@TenantTable
public class Category extends BaseEntity {
    /**
     * 分类内部ID唯一
     */
    @Column(name = "catid")
    @ApiModelProperty( value ="分类内部ID唯一")
    private String catid;
    /**
     * 分类类型, 1:产品分类 2:资料分类 3:故障分类 4:备件分类 5:故障原因分类
     */
    @Column(name = "type")
    @ApiModelProperty(value = "分类类型")
    private String type;
    /**
     * 分类等级
     */
    @Column(name = "level")
    @ApiModelProperty(value = "分类等级")
    private Integer level;
    /**
     * 分类名存在且不重复
     */
    @Column(name = "name")
    @ApiModelProperty(value = "分类名")
    private String name;
    /**
     * 分类编码存在且不重复
     */
    @Column(name = "inner_code")
    @ApiModelProperty(value = "分类编码")
    private String innerCode;
    /**
     * 分类描述
     */
    @Column(name = "description")
    @ApiModelProperty(value = "分类描述")
    private String description;
    /**
     * 启用状态,默认为启用
     */
    @Column(name = "enable")
    @ApiModelProperty(value = "启用状态")
    private Integer enable;

    @Column(name = "cpid")
    @ApiModelProperty(value = "产品ID")
    private String cpid;

    @Column(name = "uid")
    @ApiModelProperty(value = "用户ID")
    @JsonProperty("uid")
    private String uId;
    /**
     * 父级分类ID
     */
    @Column(name = "pcatid")
    @ApiModelProperty(value = "父级分类ID")
    private String pcatid;

    @Transient
    private List<Category> children;

    @ApiModelProperty(value = "产品父分类")
    @JsonProperty("parent")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcatid", referencedColumnName = "catid", insertable = false, updatable = false)
    private Category productParents;
}

