package com.thinglinks.uav.category.service;

import com.thinglinks.uav.category.dto.*;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @package: com.thinglinks.uav.category.service
 * @className: CategoryService
 * @author: rp
 * @description: TODO
 */
public interface CategoryService {

    BaseResponse<Void> addCategory(AddCategory request);

    BaseResponse<CategoryDetail> getCategoryDetail(String cid);

    BasePageResponse<CategoryPage> queryCategories(CategoryPageQuery request);

    BaseResponse<List<CategoryTreeRequest>> treeCategories(String type, boolean fake);

    BaseResponse<Void> batchDelCategory(IdRequest ids);

    BaseResponse<Void> importCategory(MultipartFile file, HttpServletResponse response) throws IOException;

    BaseResponse<Void> editCategory(String catid, EditCategory request);
}
