package com.thinglinks.uav.category.service.Impl;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.thinglinks.uav.category.dto.*;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.category.repository.CategoryRepository;
import com.thinglinks.uav.category.service.CategoryService;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.ExcelUtils;
import com.thinglinks.uav.product.repository.ProductRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @package: com.thinglinks.uav.category.service.Impl
 * @className: CategoryServiceImpl
 * @author: rp
 * @description: TODO
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private static final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);
    static String[] CATEGORY_TITLE = {"分类类别", "分类名", "上级分类", "分类描述"};
    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private ProductRepository productRepository;

    public static Integer CATEGORY_DEFAULT_ENABLE=1;
    public static Integer CATEGORY_DEFAULT_LEVEL=1;
    public static String SPARE_TYPE="4";
    public static String SPARE_PARTS_TYPE="6";

    @Override
    public BaseResponse<Void> addCategory(AddCategory request) {

        Map<String, String> authMap = new HashMap<>();
        authMap.put("1", "productCategoryAdd");
        authMap.put("3", "faultCategoryAdd");
        authMap.put("4", "spareCategoryAdd");
        authMap.put("5", "reasonCategoryAdd");
        authMap.put("6", "spareCategoryAdd");

        List<String> userAuth = CommonUtils.getUserAuth();
        String requiredAuth = authMap.get(request.getType());
        if (requiredAuth != null && !userAuth.contains(requiredAuth)) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        }

        Category category = new Category();
        BeanUtils.copyProperties(request, category);

        if (categoryRepository.existsByNameAndPcatid(request.getName(), request.getPcatid())) {
            return BaseResponse.fail("分类名重复");
        }
        if (Objects.isNull(request.getPcatid())) {
            category.setLevel(CATEGORY_DEFAULT_LEVEL);

        } else {
            Category parent = categoryRepository.findAllByCatid(request.getPcatid());
            category.setLevel(parent.getLevel() + 1);
        }
        category.setEnable(CATEGORY_DEFAULT_ENABLE);
        category.setCatid(CommonUtils.uuid());
        category.setInnerCode(CommonUtils.uuid());
        category.setUId(CommonUtils.getUid());
        category.setCpid(CommonUtils.getCpid());

        categoryRepository.save(category);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<CategoryDetail> getCategoryDetail(String catid) {
        Category category = categoryRepository.findByCatid(catid);
        if (category != null) {
            CategoryDetail categoryDetail = new CategoryDetail();
            categoryDetail.setEnable(category.getEnable());
            categoryDetail.setPcatid(category.getPcatid());
            categoryDetail.setCatid(category.getCatid());
            categoryDetail.setName(category.getName());
            categoryDetail.setInnerCode(category.getInnerCode());
            categoryDetail.setDescription(category.getDescription());
            categoryDetail.setLevel(category.getLevel());
            categoryDetail.setType(category.getType());
            return BaseResponse.success(categoryDetail);
        }
        return BaseResponse.fail("查找失败");
    }

    @Override
    public BasePageResponse<CategoryPage> queryCategories(CategoryPageQuery request) {
        List<String> newTypes = Arrays.asList(request.getTypes().split("-"));
        PageRequest pageRequest = request.of();
        Specification<Category> specification = (root, criteriaQuery, criteriaBuilder)->{
            List<Predicate> predicates = new ArrayList<>();
            if (!newTypes.isEmpty()) {
                predicates.add(root.get("type").in(newTypes));
            }

            if (request.getCatid() != null) {
                List<Category> all = categoryRepository.findAllByTypeIn(newTypes);
                List<CategoryTreeRequest> collect = all.stream()
                        .map(category -> {
                            CategoryTreeRequest categoryTreeRequest = new CategoryTreeRequest();
                            BeanUtils.copyProperties(category, categoryTreeRequest);
                            return categoryTreeRequest;
                        })
                        .collect(Collectors.toList());
                List<CategoryTreeRequest> allList = CategoryTreeRequest.buildTree2(request.getCatid(), collect);
                List<String> categoryIds = getCategoryIds(allList);
                categoryIds.add(request.getCatid());
                predicates.add(root.get("catid").in(criteriaBuilder.literal(categoryIds)));
            }
            if (Objects.nonNull(request.getName())){
                predicates.add(criteriaBuilder.like(root.get("name"), "%"+request.getName()+"%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<Category> page = categoryRepository.findAll(specification, pageRequest);
        List<CategoryPage> categoryPages = page.stream().map(c -> {
            CategoryPage categoryPage = new CategoryPage();
            BeanUtils.copyProperties(c, categoryPage);
            categoryPage.setCreatedAt(DateUtils.formatDateTime(c.getCt()));
            categoryPage.setUpdatedAt(DateUtils.formatDateTime(c.getUt()));
            if (Objects.nonNull(c.getPcatid())){
                Category parent = categoryRepository.findByCatid(c.getPcatid());
                categoryPage.setParent(parent);
            }
            return categoryPage;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, categoryPages);
    }

    @Override
    public BaseResponse<List<CategoryTreeRequest>> treeCategories(String type, boolean fake) {
        if (fake && (type.equals(SPARE_TYPE) || type.equals(SPARE_PARTS_TYPE))){
            List<CategoryTreeRequest> spareTree = generateTree(SPARE_TYPE);
            List<CategoryTreeRequest> sparePartsTree = generateTree(SPARE_PARTS_TYPE);
            List<CategoryTreeRequest> categoryTree = new ArrayList<>();
            CategoryTreeRequest fakeSpareTree = new CategoryTreeRequest();
            fakeSpareTree.setName("备件");
            fakeSpareTree.setType(SPARE_TYPE);
            CategoryTreeRequest fakeSparePartsTree = new CategoryTreeRequest();
            fakeSparePartsTree.setName("备品");
            fakeSparePartsTree.setType(SPARE_PARTS_TYPE);
            fakeSpareTree.setChildren(spareTree);
            fakeSparePartsTree.setChildren(sparePartsTree);
            categoryTree.add(fakeSpareTree);
            categoryTree.add(fakeSparePartsTree);
            return BaseResponse.success(categoryTree);
        }
        return BaseResponse.success(generateTree(type));
    }

    @Override
    public BaseResponse<Void> batchDelCategory(IdRequest ids) {

        Map<String, String> authMap = new HashMap<>();
        authMap.put("1", "productCategoryDelete");
        authMap.put("3", "faultCategoryDelete");
        authMap.put("4", "spareCategoryDelete");
        authMap.put("5", "reasonCategoryDelete");

        List<Category> categoryList = categoryRepository.findAllByCatidIn(ids.getIds());

        List<String> userAuth = CommonUtils.getUserAuth();
        Set<String> requiredAuth = categoryList.stream()
                .map(category -> authMap.get(category.getType()))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        if (!requiredAuth.isEmpty() && !new HashSet<>(userAuth).containsAll(requiredAuth)) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        }

        if (categoryRepository.existsByPcatidIn(ids.getIds()) || productRepository.existsByCatidIn(ids.getIds())) {
            return BaseResponse.fail("删除失败 存在与其关联的数据");
        }
        categoryRepository.deleteAllByCatidIn(ids.getIds());
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> importCategory(MultipartFile file, HttpServletResponse response) throws IOException {
        InputStream fileInputStream = file.getInputStream();
        ExcelReader reader = ExcelUtil.getReader(fileInputStream);
        List<List<Object>> fileTitle = reader.read(0, 0);
        fileTitle.forEach(row -> {
            row.replaceAll(cell -> {
                if (cell instanceof String) {
                    return ((String) cell).trim();
                } else {
                    return cell;
                }
            });
        });
        if (!ExcelUtils.checkTemplate(CATEGORY_TITLE,fileTitle)){
            return BaseResponse.fail("模板不合法");
        }
        reader.addHeaderAlias("分类类别","type")
                .addHeaderAlias("分类名","name")
                .addHeaderAlias("上级分类","pcatid")
                .addHeaderAlias("分类描述","description");
        List<Category> categorys = reader.readAll(Category.class);
        HashMap<Category, String> failCategorys = new HashMap<>();
        for (Category category : categorys) {
            String fileParentName = category.getPcatid();

            if (!Strings.isNotEmpty(category.getName()) || !Strings.isNotEmpty(category.getType())) {
                failCategorys.put(category, "信息不完整");
                continue;
            }
            if (!category.getType().matches("[1-6]")) {
                failCategorys.put(category,"分类类别不合法");
                continue;
            }

            if(categoryRepository.existsByNameAndPcatid(category.getName(), category.getPcatid())){
                failCategorys.put(category,"分类名已存在");
                continue;
            }

            if(StringUtils.isBlank(category.getPcatid())){
                category.setUId(CommonUtils.getUid());
                category.setEnable(1);
                category.setCatid(CommonUtils.uuid());
                category.setInnerCode(CommonUtils.uuid());
                category.setLevel(1);
                categoryRepository.save(category);
                continue;
            }
            // 校验分类
            String[] split = category.getPcatid().split(Pattern.quote(">"));

            for (int i = 0; i < split.length; i++) {
                split[i] = split[i].trim();
            }

            ArrayList<Category> categoryList = new ArrayList<>();
            for (int i = 0; i < split.length; i++) {

                if(categoryRepository.existsByNameAndType(split[i], category.getType())){
                    if (i ==0){
                        Category cate = categoryRepository.findByNameAndType(split[i], category.getType()).get(0);
                        categoryList.add(cate);
                    }else {
                        List<Category> cateList = categoryRepository.findByNameAndType(split[i], category.getType());
                        boolean flag = false;
                        for (Category c : cateList) {
                            if (c.getPcatid().equals(categoryList.get(i - 1).getCatid())) {
                                categoryList.add(c);
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) {
                            Category cate = new Category();
                            cate.setLevel(i + 1);
                            cate.setName(split[i]);
                            cate.setType(category.getType());
                            cate.setCatid(CommonUtils.uuid());
                            cate.setPcatid(categoryList.get(i - 1).getCatid());
                            cate.setInnerCode(CommonUtils.uuid());
                            cate.setEnable(1);
                            Category newCategory = categoryRepository.save(cate);
                            categoryList.add(newCategory);
                        }
                    }
                }else {
                    Category cate = new Category();
                    if (i == 0) {
                        cate.setLevel(1);
                        cate.setName(split[i]);
                        cate.setType(category.getType());
                        cate.setCatid(CommonUtils.uuid());
                        cate.setInnerCode(CommonUtils.uuid());
                        cate.setEnable(1);
                        Category newCategory = categoryRepository.save(cate);
                        categoryList.add(newCategory);
                    } else {
                        cate.setLevel(i + 1);
                        cate.setName(split[i]);
                        cate.setType(category.getType());
                        cate.setCatid(CommonUtils.uuid());
                        cate.setPcatid(categoryList.get(i - 1).getCatid());
                        cate.setInnerCode(CommonUtils.uuid());
                        cate.setEnable(1);
                        Category newCategory = categoryRepository.save(cate);
                        categoryList.add(newCategory);
                    }
                }
            }
            if (categoryRepository.existsByNameAndPcatid(category.getName(), categoryList.get(categoryList.size() - 1).getCatid())) {
                failCategorys.put(category, "分类名重复");
                continue;
            }
            category.setPcatid(categoryList.get(categoryList.size()-1).getCatid());
            category.setLevel(categoryList.get(categoryList.size()-1).getLevel()+1);
            category.setUId(CommonUtils.getUid());
            category.setEnable(1);
            category.setCatid(CommonUtils.uuid());
            category.setInnerCode(CommonUtils.uuid());
            categoryRepository.save(category);
        }
        if (!failCategorys.isEmpty()){
            //todo 写出文件
            //设置写出错误文件表头 添加错误描述
            Map<String, String> headersMap = new HashMap<>();
            headersMap.put("type", "分类类别");
            headersMap.put("name", "分类名");
            headersMap.put("pcatId", "上级分类");
            headersMap.put("description", "描述");
            Workbook workbook = ExcelUtils.createErrExcel(failCategorys, headersMap, "categoryErrorInfo");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
            String fileName = URLEncoder.encode("failProductsSpare","UTF-8");
            response.setHeader("Content-Disposition","attachment;fileName="+fileName+".xlsx");
            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> editCategory(String catid, EditCategory request) {
        Category category = categoryRepository.findByCatid(catid);

        Map<String, String> authMap = new HashMap<>();
        authMap.put("1", "productCategoryEdit");
        authMap.put("3", "faultCategoryEdit");
        authMap.put("4", "spareCategoryEdit");
        authMap.put("5", "reasonCategoryEdit");

        List<String> userAuth = CommonUtils.getUserAuth();
        String requiredAuth = authMap.get(category.getType());
        if (requiredAuth != null && !userAuth.contains(requiredAuth)) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        }

        category.setName(request.getName());
        category.setDescription(request.getDescription());
        categoryRepository.save(category);
        return BaseResponse.success();
    }

    public List<CategoryTreeRequest> generateTree(String type){
        List<Category> categories = categoryRepository.findAllByType(type);
        List<CategoryTreeRequest> categoryTree = categories.stream()
                .map(category -> {
                    CategoryTreeRequest categoryTreeRequest = new CategoryTreeRequest();
                    BeanUtils.copyProperties(category, categoryTreeRequest);
                    return categoryTreeRequest;
                })
                .collect(Collectors.toList());
        List<CategoryTreeRequest> tree = CategoryTreeRequest.buildTree(categoryTree);
        return tree;
    }


    public List<String> getCategoryIds(List<CategoryTreeRequest> categoryList) {
        List<String> categoryIds = new ArrayList<>();
        for (CategoryTreeRequest category : categoryList) {
            categoryIds.add(category.getCatid());
            categoryIds.addAll(getCategoryIds(category.getChildren()));
        }
        return categoryIds;
    }
}