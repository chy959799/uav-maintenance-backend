package com.thinglinks.uav.category.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/1 22:50
 */
@Data
public class EditCategory {
    @ApiModelProperty("分类名称")
    private String name;

    @ApiModelProperty("分类描述")
    private String description;
}
