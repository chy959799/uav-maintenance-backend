package com.thinglinks.uav.category.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.category.dto
 * @className: CategoryPageQuery
 * @author: rp
 * @description: TODO
 */
@Data
public class CategoryPageQuery extends BasePageRequest {
    @ApiModelProperty(value = "目录分类")
    private String catid ;
    @ApiModelProperty(value = "分类名")
    private String name ;
    @ApiModelProperty(value = "分类类型")
    private String types;
}
