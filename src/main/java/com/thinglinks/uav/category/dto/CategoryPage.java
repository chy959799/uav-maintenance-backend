package com.thinglinks.uav.category.dto;

import com.thinglinks.uav.category.entity.Category;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.category.dto
 * @className: CategoryPage
 * @author: rp
 * @description: TODO
 */
@Data
public class CategoryPage extends Category {
    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "父级分类")
    private Category parent;
}
