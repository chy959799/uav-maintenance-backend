package com.thinglinks.uav.category.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/21 15:44
 */
@Data
public class IdRequest {
    @ApiModelProperty(value =  "id集合")
    @NotNull
    private List<String> ids;
}
