package com.thinglinks.uav.category.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @package: com.thinglinks.uav.category.dto
 * @className: AddFultCategory
 * @author: rp
 * @description: TODO
 */
@Data
public class AddCategory{

    @ApiModelProperty(value = "分类名称")
    @NotBlank
    private String name;

    @ApiModelProperty(value = "分类描述")
    private String description;

    @ApiModelProperty(value = "父亲分类id")
    private String pcatid;

    @ApiModelProperty(value = "分类类型")
    @NotBlank
    private String type;
}
