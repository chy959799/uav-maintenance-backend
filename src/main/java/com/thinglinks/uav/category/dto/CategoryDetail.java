package com.thinglinks.uav.category.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @package: com.thinglinks.uav.category.dto
 * @className: CategoryDetail
 * @author: rp
 * @description: TODO
 */
@Data
public class CategoryDetail {
    /**
     * 分类内部ID唯一
     */
    @ApiModelProperty(value = "分类内部id")
    private String catid;
    /**
     * 分类类型, 1:产品分类 2:资料分类 3:故障分类 4:备件分类 5:故障原因分类
     */
    @ApiModelProperty(value = "分类类型")
    private String type;
    /**
     * 分类等级
     */
    @ApiModelProperty(value = "分类等级")
    private Integer level;
    /**
     * 分类名存在且不重复
     */
    @ApiModelProperty(value = "分类名", required = true)
    @NotBlank
    private String name;
    /**
     * 分类编码存在且不重复
     */
    @ApiModelProperty(value = "分类编码")
    private String innerCode;
    /**
     * 分类描述
     */
    @ApiModelProperty(value = "分类描述")
    private String description;
    /**
     * 启用状态,默认为启用
     */
    @ApiModelProperty(value = "启用状态")
    private Integer enable;
    /**
     * 父级分类ID
     */
    @ApiModelProperty(value = "父级分类ID")
    private String pcatid;
}
