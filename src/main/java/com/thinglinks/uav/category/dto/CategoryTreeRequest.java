package com.thinglinks.uav.category.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author huiyongchen
 */
@Data
public class CategoryTreeRequest{
    @ApiModelProperty(name = "分类ID")
    private String catid;
    @ApiModelProperty(name = "分类名称")
    private String name;
    @ApiModelProperty(name = "父分类ID")
    private String pcatid;
    @ApiModelProperty(name = "分类类别")
    private String type;
    @ApiModelProperty(name = "分类数量")
    private int count;
    @ApiModelProperty(name = "子分类")
    private List<CategoryTreeRequest> children;

    public CategoryTreeRequest() {
    }

    public CategoryTreeRequest(String catid, String name, String pcatid, String type) {
        this.catid = catid;
        this.name = name;
        this.pcatid = pcatid;
        this.type = type;
        this.children = new ArrayList<>();
        this.count = 0;
    }

    public void addChild(CategoryTreeRequest child) {
        children.add(child);
    }

    public List<CategoryTreeRequest> getChildren() {
        return children;
    }

    public int getCount() {
        return count;
    }

    public void updateCount() {
        int totalCount = children.size();
        for (CategoryTreeRequest child : children) {
            child.updateCount();
            totalCount += child.getCount();
        }
        count = totalCount;
    }

    public static List<CategoryTreeRequest> buildTree(List<CategoryTreeRequest> categoryTree) {
        List<CategoryTreeRequest> tree = new ArrayList<>();
        for (CategoryTreeRequest category : categoryTree) {
            if (category.getPcatid() == null || category.getPcatid().isEmpty()) {
                CategoryTreeRequest node = buildNode(category, categoryTree);
                tree.add(node);
            }
        }
        return tree;
    }

    private static CategoryTreeRequest buildNode(CategoryTreeRequest category, List<CategoryTreeRequest> categoryTree) {
        CategoryTreeRequest node = new CategoryTreeRequest(category.getCatid(), category.getName(), category.getPcatid(), category.getType());
        List<CategoryTreeRequest> children = categoryTree.stream()
                .filter(c -> category.getCatid().equals(c.getPcatid()))
                .map(c -> buildNode(c, categoryTree))
                .collect(Collectors.toList());
        node.children.addAll(children);
        node.updateCount();
        return node;
    }


    public static List<CategoryTreeRequest> buildTree2(String catid,List<CategoryTreeRequest> categoryTree) {
        List<CategoryTreeRequest> tree = new ArrayList<>();
        for (CategoryTreeRequest category : categoryTree) {
            if (Objects.equals(category.getPcatid(), catid)) {
                CategoryTreeRequest node = buildNode2(category, categoryTree);
                tree.add(node);
            }
        }
        return tree;
    }

    private static CategoryTreeRequest buildNode2(CategoryTreeRequest category, List<CategoryTreeRequest> categoryTree) {
        CategoryTreeRequest node = new CategoryTreeRequest(category.getCatid(), category.getName(), category.getPcatid(), category.getType());
        List<CategoryTreeRequest> children = categoryTree.stream()
                .filter(c -> category.getCatid().equals(c.getPcatid()))
                .map(c -> buildNode2(c, categoryTree))
                .collect(Collectors.toList());
        node.children.addAll(children);
        node.updateCount();
        return node;
    }
}
