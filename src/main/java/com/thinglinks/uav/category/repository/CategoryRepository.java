package com.thinglinks.uav.category.repository;

import com.thinglinks.uav.category.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @package: com.thinglinks.uav.category.repository
 * @className: CategoryRepository
 * @author: rp
 * @description: TODO
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> , JpaSpecificationExecutor<Category> {
        Category findByName(String name);
        Category findByCatid(String catid);

        List<Category> findAllByType(String s);

        boolean existsByNameAndType(String name,String type);

        Category findAllByCatid(String catid);

        boolean existsByPcatidIn(List<String> pcatidList);

        void deleteAllByCatidIn(List<String> catidList);

        List<Category> findByNameAndType(String name,String type);

        boolean existsByNameAndPcatid(String name,String pcatid);

        List<Category> findAllByCatidIn(List<String> ids);

        List<Category> findAllByTypeIn(List<String> types);

        Category findByNameAndTypeAndAndLevel(String name,String type,Integer level);
}
