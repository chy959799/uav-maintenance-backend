package com.thinglinks.uav.category.controller;

import com.thinglinks.uav.category.dto.*;
import com.thinglinks.uav.category.service.CategoryService;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * @package: com.thinglinks.uav.category.controller
 * @className: CategoryController
 * @author: rp
 * @description: TODO
 */
@Api(tags = "分类管理")
@RestController
@RequestMapping(value = "/category")
@Slf4j
public class CategoryController {
    @Autowired
    private  CategoryService categoryService;

    @Operation(summary = "新增分类")
    @PostMapping
    public BaseResponse<Void> addCategory(@RequestBody @Valid AddCategory request) {
        return categoryService.addCategory(request);
    }

    @Operation(summary = "修改分类")
    @PatchMapping(path = "/{catid}")
    public BaseResponse<Void> editCategory(@PathVariable("catid") String catid, @RequestBody @Valid EditCategory request) {
        return categoryService.editCategory(catid,request);
    }

    @Operation(summary = "批量删除分类")
    @DeleteMapping
    public BaseResponse<Void> batchDelCategory(@RequestBody @Valid IdRequest ids) {
        return categoryService.batchDelCategory(ids);
    }
    @Operation(summary = "分类详情")
    @GetMapping(path = "{catid}")
    public BaseResponse<CategoryDetail> getCategoryDetail(@PathVariable("catid") String cid) {
        return categoryService.getCategoryDetail(cid);
    }
    @GetMapping
    @Operation(summary = "分类分页查询")
    public BasePageResponse<CategoryPage> queryCategories(CategoryPageQuery request) {
        return categoryService.queryCategories(request);
    }
    @GetMapping(path = "tree")
    @Operation(summary = "分类树形菜单查询")
    public BaseResponse<List<CategoryTreeRequest>> treeCategories(@RequestParam("type") String type,@RequestParam(value = "fake",defaultValue = "false", required = false) boolean fake) {
        return categoryService.treeCategories(type,fake);
    }

    @Operation(summary = "批量导入分类")
    @PostMapping(path = "import")
    public BaseResponse<Void> importCategory(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response) throws IOException {
        return categoryService.importCategory(file,response);
    }
}
