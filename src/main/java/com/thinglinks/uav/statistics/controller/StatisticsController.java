package com.thinglinks.uav.statistics.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.statistics.dto.*;
import com.thinglinks.uav.statistics.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@Api(tags = "数据统计")
@RestController
@RequestMapping(value = "statistics")
@Slf4j
public class StatisticsController {

    @Resource
    private StatisticsService statisticsService;


    // 工单概况
    @Operation(summary = "工单概况")
    @GetMapping("/order")
    public BaseResponse<OrderOverviewDTO> getOrderOverview(String target) {
        return statisticsService.getOrderOverview(target);
    }

    // 项目统计
    @Operation(summary = "项目统计")
    @GetMapping("/project")
    public BasePageResponse<ProjectOverviewDTO> getProjectOverview(TargetRequest target) {
        return statisticsService.getProjectOverview(target);
    }

    // 工单故障类别统计
    @Operation(summary = "工单故障类别统计")
    @GetMapping("/faultType")
    public BasePageResponse<OrderCategoriesDTO> getFaultOverview(TargetRequest target) {
        return statisticsService.getFaultOverview(target);
    }

    // 工单故障类别 产品占比详情
    @Operation(summary = "工单故障类别 产品占比详情")
    @GetMapping("/faultTypeDetail")
    public BasePageResponse<ProductPercentageDTO> getFaultTypeDetail(TargetRequest target) {
        return statisticsService.getProductPercentageDTO(target);
    }


    @Operation(summary = "工单故障类别导出")
    @PostMapping("/faultTypeExport")
    public void getFaultOverviewExport(HttpServletResponse response,@RequestBody ExportDTO target) throws IOException {
        statisticsService.getFaultOverviewExport(response, target);
    }

    // 故障原因统计
//    @Operation(summary = "工单故障原因统计")
//    @GetMapping("/faultReason")
//    public BasePageResponse<FaultOverviewDTO> getReasonOverview(TargetRequest target) {
//        return statisticsService.getReasonOverview(target);
//    }

    // 机型数据统计
    @Operation(summary = "机型数据统计")
    @GetMapping("/model")
    public BasePageResponse<ModelOverviewDTO> getModelOverview(@Valid TargetRequest target) {
        return statisticsService.getModelOverview(target);
    }

    @Operation(summary = "机型数据统计导出")
    @PostMapping("/modelExport")
    public void getModelOverviewExport(HttpServletResponse response,@RequestBody ExportDTO target) throws IOException {
        statisticsService.getModelOverviewExport(response, target);
    }

    // 客户送修飞机数量
    @Operation(summary = "客户送修飞机数量")
    @GetMapping("/customer")
    public BasePageResponse<RepairNumDTO> getRepairNum(TargetRequest target) {
        return statisticsService.getRepairNum(target);
    }

    @Operation(summary = "客户送修飞机数量导出")
    @PostMapping("/customerExport")
    public void getRepairNumExport(HttpServletResponse response,@RequestBody ExportDTO target) throws IOException {
        statisticsService.getRepairNumExport(response, target);
    }

    @Operation(summary = "客户送修飞机数量详情")
    @GetMapping("/customerDetail")
    public BasePageResponse<DeviceDetailDTO> getRepairNumDetail(TargetRequest target) {
        return statisticsService.getRepairNumDetail(target);
    }
}
