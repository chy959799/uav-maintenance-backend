package com.thinglinks.uav.statistics.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.statistics.dto.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface StatisticsService {

    BaseResponse<OrderOverviewDTO> getOrderOverview(String target);

    BasePageResponse<ProjectOverviewDTO> getProjectOverview(TargetRequest target);

    BasePageResponse<OrderCategoriesDTO> getFaultOverview(TargetRequest target);

//    BasePageResponse<FaultOverviewDTO> getReasonOverview(TargetRequest target);

    BasePageResponse<ModelOverviewDTO> getModelOverview(TargetRequest target);

    BasePageResponse<RepairNumDTO> getRepairNum(TargetRequest target);

    BasePageResponse<DeviceDetailDTO> getRepairNumDetail(TargetRequest target);

    void getFaultOverviewExport(HttpServletResponse response, ExportDTO target) throws IOException;

    void getModelOverviewExport(HttpServletResponse response, ExportDTO target) throws IOException;

    void getRepairNumExport(HttpServletResponse response, ExportDTO target) throws IOException;

    BasePageResponse<ProductPercentageDTO> getProductPercentageDTO(TargetRequest target);
}
