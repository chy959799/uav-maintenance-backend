package com.thinglinks.uav.statistics.service.impl;

import com.alibaba.excel.EasyExcel;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.category.repository.CategoryRepository;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.PageMeta;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.faults.repository.FaultsRepository;
import com.thinglinks.uav.order.entity.Expresses;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.entity.OrderCategory;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.ExpressesRepository;
import com.thinglinks.uav.order.repository.OrderCategoryRepository;
import com.thinglinks.uav.order.repository.OrderQuoteRepository;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.statistics.dto.*;
import com.thinglinks.uav.statistics.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StatisticsServiceImpl implements StatisticsService {

    private static final String ADMIN = "1";
    private static final String APP = "2";
    private static final String REPAIR = "1";
    private static final String MAINTAIN = "2";
    private static final String INNER = "1";
    private static final String MONTH = "month";
    private static final String DAY = "day";
    private static final String EQUIPMENT_RECEIPT_TYPE = "1";
    private static final String EXPORT_FAULT_TYPE_FILENAME = "工单故障类别统计表";
    private static final String EXPORT_MODEL_FILENAME = "工单机型统计表";
    private static final String EXPORT_CUSTOMER_REPAIR_FILENAME = "客户送修统计表";

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private OrderQuoteRepository orderQuoteRepository;

    @Resource
    private OrderCategoryRepository orderCategoryRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private FaultsRepository faultsRepository;

    @Resource
    private ExpressesRepository expressesRepository;
    @Autowired
    private DeviceRepository deviceRepository;

    @Resource
    private ProductRepository productRepository;
    @Autowired
    private CustomersRepository customersRepository;

    @Override
    public BaseResponse<OrderOverviewDTO> getOrderOverview(String target) {
        // 查询需要统计的工单
        List<Order> allOrders = orderRepository.findAllAfterStatus(OrderStatusEnums.ORDER_STATUS_WAIT_EXPRESSES.getCode());
        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            allOrders.removeIf(o -> !o.getCpid().equals(cpid));
        }
        OrderOverviewDTO result = new OrderOverviewDTO();
        if (target.matches("\\d{4}")) {
            long[] longs = getYearTimeStamps(Integer.parseInt(target));
            List<Order> orders = allOrders.stream().filter(o -> o.getCt() >= longs[0] && o.getCt() <= longs[1]).collect(Collectors.toList());
            result.setOrderCount(orders.size());
            result.setCustomerCount(orders.stream().map(Order::getCid).distinct().count());
            result.setDeviceCount(orders.stream().map(Order::getDevid).distinct().count());
            result.setAverageProcessingTime(orders.stream().filter(order -> order.getTotalTime() != null).mapToInt(Order::getTotalTime).average().orElse(0.0));
            result.setNewOrderCount(orders.size());
            result.setCancelOrderCount(orders.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_CANCEL.getCode().equals(o.getStatus())).count());
            result.setFinishOrderCount(orders.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_FINISH.getCode().equals(o.getStatus())).count());
            result.setAdminOrderCount(orders.stream().filter(o -> ADMIN.equals(o.getOrigin())).count());
            result.setAppOrderCount(orders.stream().filter(o -> APP.equals(o.getOrigin())).count());
            result.setRepairOrderCount(orders.stream().filter(o -> REPAIR.equals(o.getType())).count());
            result.setMaintainOrderCount(orders.stream().filter(o -> MAINTAIN.equals(o.getType())).count());
            result.setInnerOrderCount(orders.stream().filter(o -> INNER.equals(o.getServiceType())).count());
            result.setOuterOrderCount(orders.stream().filter(o -> !INNER.equals(o.getServiceType())).count());
            List<OrderItem> orderItems = new ArrayList<>();
            for (int i = 0; i < 12; i++) {
                OrderItem orderItem = new OrderItem();
                String tar = String.format("%s-%d", target, i + 1);
                orderItem.setTag(tar);
                orderItem.setCycle(MONTH);
                long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(target.split("-")[0]), i + 1);
                List<Order> list = orders.stream().filter(o -> o.getCt() >= monthTimeStamps[0] && o.getCt() <= monthTimeStamps[1]).collect(Collectors.toList());
                orderItem.setNewOrder(list.size());
                orderItem.setCancelOrder(list.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_CANCEL.getCode().equals(o.getStatus())).count());
                orderItem.setFinishOrder(list.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_FINISH.getCode().equals(o.getStatus())).count());
                orderItems.add(orderItem);
            }
            result.setOrderItems(orderItems);
            return BaseResponse.success(result);

        } else if (target.matches("\\d{4}-\\d{1,2}")) {
            String[] parts = target.split("-");
            String year = parts[0];
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            List<Order> orders = allOrders.stream().filter(o -> o.getCt() >= monthTimeStamps[0] && o.getCt() <= monthTimeStamps[1]).collect(Collectors.toList());
            result.setOrderCount(orders.size());
            result.setCustomerCount(orders.stream().map(Order::getCid).distinct().count());
            result.setDeviceCount(orders.stream().map(Order::getDevid).distinct().count());
            result.setAverageProcessingTime(orders.stream().filter(order -> order.getTotalTime() != null).mapToInt(Order::getTotalTime).average().orElse(0.0));
            result.setNewOrderCount(orders.size());
            result.setCancelOrderCount(orders.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_CANCEL.getCode().equals(o.getStatus())).count());
            result.setFinishOrderCount(orders.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_FINISH.getCode().equals(o.getStatus())).count());
            result.setAdminOrderCount(orders.stream().filter(o -> ADMIN.equals(o.getOrigin())).count());
            result.setAppOrderCount(orders.stream().filter(o -> APP.equals(o.getOrigin())).count());
            result.setRepairOrderCount(orders.stream().filter(o -> REPAIR.equals(o.getType())).count());
            result.setMaintainOrderCount(orders.stream().filter(o -> MAINTAIN.equals(o.getType())).count());
            result.setInnerOrderCount(orders.stream().filter(o -> INNER.equals(o.getServiceType())).count());
            result.setOuterOrderCount(orders.stream().filter(o -> !INNER.equals(o.getServiceType())).count());
            List<OrderItem> orderItems = new ArrayList<>();
            int days = getDaysInMonth(Integer.parseInt(year), Integer.parseInt(month));
            for (int i = 0; i < days; i++) {
                OrderItem orderItem = new OrderItem();
                String tar = String.format("%s-%d", target, i + 1);
                orderItem.setTag(tar);
                orderItem.setCycle(DAY);
                long[] dayTimeStamps = getDayTimeStamps(Integer.parseInt(year), Integer.parseInt(month), i + 1);
                List<Order> list = orders.stream().filter(o -> o.getCt() >= dayTimeStamps[0] && o.getCt() <= dayTimeStamps[1]).collect(Collectors.toList());
                orderItem.setNewOrder(list.size());
                orderItem.setCancelOrder(list.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_CANCEL.getCode().equals(o.getStatus())).count());
                orderItem.setFinishOrder(list.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_FINISH.getCode().equals(o.getStatus())).count());
                orderItems.add(orderItem);
            }
            result.setOrderItems(orderItems);
            return BaseResponse.success(result);
        }
        return BaseResponse.fail("参数错误");
    }

    @Override
    public BasePageResponse<ProjectOverviewDTO> getProjectOverview(TargetRequest target) {
        PageRequest pageRequest = target.of();

        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        Specification<Project> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.between(root.get("ct"), yearStamps[0], yearStamps[1]));

            if (CommonUtils.getCpid() != null && !CommonUtils.getCpid().isEmpty()){
                predicates.add(criteriaBuilder.equal(root.get("cpid"), CommonUtils.getCpid()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<Project> projects = projectRepository.findAll(specification, pageRequest);


        if (tar.matches("\\d{4}")) {
            List<ProjectOverviewDTO> result = projects.stream().map(p -> {
                List<Order> orders = orderRepository.findAllByPjid(p.getPjid());
                ProjectOverviewDTO dto = new ProjectOverviewDTO();
                dto.setPjid(p.getPjid());
                dto.setName(p.getName());
                dto.setTotal(orders.size());
                dto.setFinish(orders.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_FINISH.getCode().equals(o.getStatus())).count());
                AtomicReference<Double> fees = new AtomicReference<>(0.0);
                orders.forEach(order -> {
                    fees.updateAndGet(v -> v + orderQuoteRepository.sumPricesByOrdid(order.getOrdid()));
                });
                dto.setFees(fees.get());
                return dto;
            }).collect(Collectors.toList());

            return BasePageResponse.success(projects, result);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            List<Project> projectList = projects.stream().filter(p -> p.getCt() >= monthTimeStamps[0] && p.getCt() <= monthTimeStamps[1]).collect(Collectors.toList());
            List<ProjectOverviewDTO> result = projectList.stream().map(p -> {
                List<Order> orders = orderRepository.findAllByPjid(p.getPjid());
                ProjectOverviewDTO dto = new ProjectOverviewDTO();
                dto.setPjid(p.getPjid());
                dto.setName(p.getName());
                dto.setTotal(orders.size());
                dto.setFinish(orders.stream().filter(o -> OrderStatusEnums.ORDER_STATUS_FINISH.getCode().equals(o.getStatus())).count());
                AtomicReference<Double> fees = new AtomicReference<>(0.0);
                orders.forEach(order -> {
                    fees.updateAndGet(v -> v + orderQuoteRepository.sumPricesByOrdid(order.getOrdid()));
                });
                dto.setFees(fees.get());
                return dto;
            }).collect(Collectors.toList());
            return BasePageResponse.success(projects, result);
        }
        return BasePageResponse.fail("参数错误");
    }

    @Override
    public BasePageResponse<OrderCategoriesDTO> getFaultOverview(TargetRequest target) {
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            return getFaultOverview(target.getOffset(), target.getLimit(), yearStamps[0], yearStamps[1]);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            return getFaultOverview(target.getOffset(), target.getLimit(), monthTimeStamps[0], monthTimeStamps[1]);
        }
        return BasePageResponse.fail("参数错误");
    }

    public BasePageResponse<OrderCategoriesDTO> getFaultOverview(int offset, int limit, long start, long end) {
        List<OrderCategory> allOrderCategories = orderCategoryRepository.findAllByCtBetween(start, end);
        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            allOrderCategories.removeIf(o -> cpid.equals(orderRepository.findByOrdid(o.getOrdid()).getCpid()));
        }
        Map<String, List<OrderCategory>> collect = allOrderCategories.stream().collect(Collectors.groupingBy(OrderCategory::getCatid));
        List<OrderCategoriesDTO> orderCategoriesDTOList = collect.entrySet().stream().map(e -> {
            List<String> ordIds = e.getValue().stream().map(OrderCategory::getOrdid).collect(Collectors.toList());
            OrderCategoriesDTO orderCategoriesDTO = new OrderCategoriesDTO();
            Category category = categoryRepository.findAllByCatid(e.getKey());
            orderCategoriesDTO.setCateName(category.getName());
            orderCategoriesDTO.setCatid(e.getKey());
            orderCategoriesDTO.setOrdid(ordIds);
            orderCategoriesDTO.setCount(ordIds.size());
            return orderCategoriesDTO;
        }).collect(Collectors.toList());
        List<OrderCategoriesDTO> result = orderCategoriesDTOList.stream().skip(offset).limit(limit).collect(Collectors.toList());
        return customePaging(offset, limit, result);
    }


//    @Override
//    public BasePageResponse<FaultOverviewDTO> getReasonOverview(TargetRequest target) {
//        String tar = target.getTarget();
//        String[] parts = tar.split("-");
//        String year = parts[0];
//        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
//        if (tar.matches("\\d{4}")){
//            return getFaultReasonOverview(target.getOffset(),target.getLimit(),yearStamps[0],yearStamps[1]);
//        }else if (tar.matches("\\d{4}-\\d{1,2}")){
//            String month = parts[1];
//            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
//            return getFaultReasonOverview(target.getOffset(),target.getLimit(),monthTimeStamps[0],monthTimeStamps[1]);
//        }
//        return BasePageResponse.fail("参数错误");
//    }

//    public BasePageResponse<FaultOverviewDTO> getFaultReasonOverview(int offset,int limit,long start , long end){
////        List<OrderCategory> orderCategories = orderCategoryRepository.findAllByCtBetween(start, end);
////        List<OrderFaultReasonDTO> orderFaultReasonDTOS = new ArrayList<>();
////        orderCategories.forEach(orderCategory -> {
////            OrderFaultReasonDTO orderFaultReasonDTO = new OrderFaultReasonDTO();
////            orderFaultReasonDTO.setCatId(orderCategory.getCatid());
////            orderFaultReasonDTO.setOrderId(orderCategory.getOrdid());
////            faultsRepository.findAllByCatid(orderCategory.getCatid());
////
////            orderFaultReasonDTO.setReason(orderCategory.getMethod());
////            orderFaultReasonDTOS.add(orderFaultReasonDTO);
////        });
//
//        List<Fault> allFaults = faultsRepository.findAllByCtBetween(start, end);
//        Map<String, Long> collect = allFaults.stream().collect(Collectors.groupingBy(Fault::getReason, Collectors.counting()));
//        List<FaultOverviewDTO> result = collect.entrySet().stream().map(e -> {
//            FaultOverviewDTO faultOverviewDTO = new FaultOverviewDTO();
//            faultOverviewDTO.setName(e.getKey());
//            faultOverviewDTO.setTotal(e.getValue());
//            return faultOverviewDTO;
//        }).collect(Collectors.toList());
//        return customePaging(offset,limit,result);
//    }

    /**
     * @param target
     * @return
     */
    @Override
    public BasePageResponse<ModelOverviewDTO> getModelOverview(TargetRequest target) {
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            return getModelOverview(target.getOffset(), target.getLimit(), yearStamps[0], yearStamps[1]);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            return getModelOverview(target.getOffset(), target.getLimit(), monthTimeStamps[0], monthTimeStamps[1]);
        }
        return BasePageResponse.fail("参数错误");
    }

    /**
     * @param target
     * @return
     */
    @Override
    public BasePageResponse<RepairNumDTO> getRepairNum(TargetRequest target) {
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            return getRepairNum(target.getOffset(), target.getLimit(), yearStamps[0], yearStamps[1]);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            return getRepairNum(target.getOffset(), target.getLimit(), monthTimeStamps[0], monthTimeStamps[1]);
        }
        return BasePageResponse.fail("参数错误");
    }

    /**
     * @param target
     * @return
     */
    @Override
    public BasePageResponse<DeviceDetailDTO> getRepairNumDetail(TargetRequest target) {
        if (target.getCuid() == null || target.getCuid().isEmpty()) {
            return BasePageResponse.fail("参数错误");
        }
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            return getRepairNumDetail(target.getOffset(), target.getLimit(), yearStamps[0], yearStamps[1], target.getCuid());
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            return getRepairNumDetail(target.getOffset(), target.getLimit(), monthTimeStamps[0], monthTimeStamps[1], target.getCuid());
        }
        return BasePageResponse.fail("参数错误");
    }

    /**
     * @param response
     * @param target
     */
    @Override
    public void getFaultOverviewExport(HttpServletResponse response, ExportDTO target) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(EXPORT_FAULT_TYPE_FILENAME, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            List<FaultTypeExport> exportList = getFaultExportList(yearStamps[0], yearStamps[1], target);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(FaultTypeExport.class)
                    .sheet("统计详情")
                    .doWrite(exportList);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            List<FaultTypeExport> exportList = getFaultExportList(monthTimeStamps[0], monthTimeStamps[1], target);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(FaultTypeExport.class)
                    .sheet("统计详情")
                    .doWrite(exportList);
        }
    }

    /**
     * @param response
     * @param target
     */
    @Override
    public void getModelOverviewExport(HttpServletResponse response, ExportDTO target) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(EXPORT_MODEL_FILENAME, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            List<ModelExportDTO> modelExportList = getModelExportList(yearStamps[0], yearStamps[1], target);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(ModelExportDTO.class)
                    .sheet("统计详情")
                    .doWrite(modelExportList);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            List<ModelExportDTO> modelExportList = getModelExportList(monthTimeStamps[0], monthTimeStamps[1], target);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(ModelExportDTO.class)
                    .sheet("统计详情")
                    .doWrite(modelExportList);
        }
    }

    /**
     * @param response
     * @param target
     */
    @Override
    public void getRepairNumExport(HttpServletResponse response, ExportDTO target) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(EXPORT_CUSTOMER_REPAIR_FILENAME, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            List<CustomerRepairExportDTO> customerRepairExportList = getCustomerRepairExportList(yearStamps[0], yearStamps[1], target);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(CustomerRepairExportDTO.class)
                    .sheet("统计详情")
                    .doWrite(customerRepairExportList);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            List<CustomerRepairExportDTO> customerRepairExportList = getCustomerRepairExportList(yearStamps[0], yearStamps[1], target);
            EasyExcel.write(response.getOutputStream())
                    .autoCloseStream(Boolean.FALSE)
                    .head(CustomerRepairExportDTO.class)
                    .sheet("统计详情")
                    .doWrite(customerRepairExportList);
        }
    }

    /**
     * @param target
     * @return
     */
    @Override
    public BasePageResponse<ProductPercentageDTO> getProductPercentageDTO(TargetRequest target) {
        if (target.getCuid() == null || target.getCuid().isEmpty()) {
            return BasePageResponse.fail("参数错误");
        }
        String tar = target.getTarget();
        String[] parts = tar.split("-");
        String year = parts[0];
        long[] yearStamps = getYearTimeStamps(Integer.parseInt(year));
        if (tar.matches("\\d{4}")) {
            return getProductPercentageDTO(yearStamps[0], yearStamps[1], target);
        } else if (tar.matches("\\d{4}-\\d{1,2}")) {
            String month = parts[1];
            long[] monthTimeStamps = getMonthTimeStamps(Integer.parseInt(year), Integer.parseInt(month));
            return getProductPercentageDTO(monthTimeStamps[0], monthTimeStamps[1], target);
        }
        return BasePageResponse.fail("参数错误");
    }

    public BasePageResponse<ProductPercentageDTO> getProductPercentageDTO(long start, long end, TargetRequest target) {
        List<OrderCategory> allOrderCategories = orderCategoryRepository.findAllByCtBetween(start, end);
        List<String> ordids = allOrderCategories.stream().filter(o -> target.getCuid().equals(o.getCatid())).map(OrderCategory::getOrdid).collect(Collectors.toList());

        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            ordids.removeIf(o -> cpid.equals(orderRepository.findByOrdid(o).getCpid()));
        }

        List<Order> orderList = orderRepository.findByOrdidIn(ordids);
        Map<String, Long> count = orderList.stream()
                .map(order -> order.getDevice().getProduct().getName())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        List<ProductPercentageDTO> productPercentageDTOList = count.entrySet().stream()
                .map(entry -> new ProductPercentageDTO(entry.getKey(), String.valueOf(entry.getValue())))
                .collect(Collectors.toList());
        List<ProductPercentageDTO> result = productPercentageDTOList.stream().skip(target.getOffset()).limit(target.getLimit()).collect(Collectors.toList());
        return customePaging(target.getOffset(), target.getLimit(), result);
    }

    public List<CustomerRepairExportDTO> getCustomerRepairExportList(long start, long end, ExportDTO target) {
        List<Order> orders = orderRepository.findByCtBetween(start, end);

        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            orders.removeIf(o -> cpid.equals(o.getCpid()));
        }

        List<CustomerRepairExportDTO> customerRepairExportList = new ArrayList<>();
        for (Order order : orders) {
            if (target.getIds().contains(order.getCuid())) {
                CustomerRepairExportDTO customerRepairExportDTO = new CustomerRepairExportDTO();
                Customer customer = customersRepository.findByCid(order.getCuid());
                Customer company = customersRepository.findByCid(order.getCid());
                Device device = deviceRepository.findByDevid(order.getDevid());
                customerRepairExportDTO.setCustomerName(customer.getName());
                customerRepairExportDTO.setOrderCode(order.getInnerCode());
                customerRepairExportDTO.setDeviceCode(device.getCode());
                customerRepairExportDTO.setCompanyName(company.getName());
                customerRepairExportList.add(customerRepairExportDTO);
            }
        }
        return customerRepairExportList;
    }

    public List<ModelExportDTO> getModelExportList(long start, long end, ExportDTO target) {
        List<Expresses> expresses = expressesRepository.findByTypeAndExpressesAtBetween(EQUIPMENT_RECEIPT_TYPE, start, end);
        List<String> ordeIdList = expresses.stream().map(Expresses::getOrdid).collect(Collectors.toList());

        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            ordeIdList.removeIf(o -> cpid.equals(orderRepository.findByOrdid(o).getCpid()));
        }

        List<Order> orderList = orderRepository.findByOrdidIn(ordeIdList);
        List<ModelExportDTO> modelExportList = new ArrayList<>();
        for (Order order : orderList) {
            ModelExportDTO modelExportDTO = new ModelExportDTO();
            // 获取每个订单的device对象
            Device device = order.getDevice();
            // 从device对象中获取category对象
            Product product = device.getProduct();
            Category category = product.getCategory();
            String catid = category.getCatid();
            if (target.getIds().contains(catid)) {
                modelExportDTO.setModelName(category.getName());
                modelExportDTO.setOrderCode(order.getInnerCode());
                modelExportList.add(modelExportDTO);
            }
        }
        return modelExportList;
    }

    public List<FaultTypeExport> getFaultExportList(long start, long end, ExportDTO target) {
        List<OrderCategory> allOrderCategories = orderCategoryRepository.findAllByCtBetween(start, end);

        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            allOrderCategories.removeIf(o -> cpid.equals(orderRepository.findByOrdid(o.getOrdid()).getCpid()));
        }
        Map<String, List<OrderCategory>> collect = allOrderCategories.stream().collect(Collectors.groupingBy(OrderCategory::getCatid));
        List<OrderCategoriesDTO> orderCategoriesDTOList = collect.entrySet().stream().map(e -> {
            List<String> ordIds = e.getValue().stream().map(OrderCategory::getOrdid).collect(Collectors.toList());
            OrderCategoriesDTO orderCategoriesDTO = new OrderCategoriesDTO();
            Category category = categoryRepository.findByCatid(e.getKey());
            orderCategoriesDTO.setCateName(category.getName());
            orderCategoriesDTO.setCatid(e.getKey());
            orderCategoriesDTO.setOrdid(ordIds);
            orderCategoriesDTO.setCount(ordIds.size());
            return orderCategoriesDTO;
        }).collect(Collectors.toList());
        List<FaultTypeExport> faultTypeExportList = new ArrayList<>();
        List<OrderCategoriesDTO> res = orderCategoriesDTOList.stream().filter(e -> target.getIds().contains(e.getCatid())).collect(Collectors.toList());
        res.forEach(r -> {
            List<Order> orders = orderRepository.findAllByOrdidIn(r.getOrdid());
            orders.forEach(o -> {
                FaultTypeExport typeExport = new FaultTypeExport();
                typeExport.setName(r.getCateName());
                typeExport.setOrderCode(o.getInnerCode());
                faultTypeExportList.add(typeExport);
            });
        });
        return faultTypeExportList;
    }

    public BasePageResponse<RepairNumDTO> getRepairNum(int offset, int limit, long start, long end) {
        List<Order> orders = orderRepository.findByCtBetween(start, end);
        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            orders.removeIf(o -> cpid.equals(o.getCpid()));
        }
        Map<String, List<Order>> groupOrders = new HashMap<>();
        List<RepairNumDTO> repairNumDTOS = new ArrayList<>();
        for (Order order : orders) {
            if (!groupOrders.containsKey(order.getCuid())) {
                groupOrders.put(order.getCuid(), new ArrayList<>());
            }
            groupOrders.get(order.getCuid()).add(order);
        }
        for (Map.Entry<String, List<Order>> entry : groupOrders.entrySet()) {
            Customer customer = customersRepository.findByCid(entry.getKey());
            if (customer != null) {
                List<String> orderids = new ArrayList<>();
                for (Order order : entry.getValue()) {
                    orderids.add(order.getOrdid());
                }
                repairNumDTOS.add(new RepairNumDTO(customer.getCid(), customer.getName(), orderids, orderids.size()));
            }
        }
        List<RepairNumDTO> result = repairNumDTOS.stream().skip(offset).limit(limit).collect(Collectors.toList());
        return customePaging(offset, limit, result);
    }

    public BasePageResponse<DeviceDetailDTO> getRepairNumDetail(int offset, int limit, long start, long end, String cuid) {
        List<DeviceDetailDTO> deviceDetailDTOS = new ArrayList<>();
        List<Order> orders = orderRepository.findByCtBetween(start, end);

        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()) {
            orders.removeIf(o -> cpid.equals(o.getCpid()));
        }

        List<Order> collect = orders.stream().filter(o -> cuid.equals(o.getCuid())).collect(Collectors.toList());
        if (!collect.isEmpty()) {
            for (Order order : collect) {
                DeviceDetailDTO deviceDetail = new DeviceDetailDTO();
                Device device = deviceRepository.findByDevid(order.getDevid());
                Product product = productRepository.findByPid(device.getPid());
                deviceDetail.setName(product.getName());
                deviceDetail.setCode(device.getCode());
                Customer company = customersRepository.findByCid(order.getCid());
                deviceDetail.setCompanyName(company.getName());
                deviceDetailDTOS.add(deviceDetail);
            }
        }
        return customePaging(offset, limit, deviceDetailDTOS);
    }

    public BasePageResponse<ModelOverviewDTO> getModelOverview(int offset, int limit, long start, long end) {
        List<Expresses> expresses = expressesRepository.findByTypeAndExpressesAtBetween(EQUIPMENT_RECEIPT_TYPE, start, end);
        List<String> ordeIdList = expresses.stream().map(Expresses::getOrdid).collect(Collectors.toList());
        String cpid = CommonUtils.getCpid();
        if(cpid != null && !cpid.isEmpty()){
            ordeIdList.removeIf(o -> cpid.equals(orderRepository.findByOrdid(o).getCpid()));
        }
        List<Order> orderList = orderRepository.findByOrdidIn(ordeIdList);
        Map<String, List<Order>> groupedOrders = new HashMap<>();
        List<ModelOverviewDTO> modelOverviewList = new ArrayList<>();
        for (Order order : orderList) {
            // 获取每个订单的device对象
            Device device = order.getDevice();
            // 从device对象中获取category对象
            Product product = device.getProduct();
            Category category = product.getCategory();
            String catid = category.getCatid();
            if (!groupedOrders.containsKey(catid)) {
                groupedOrders.put(catid, new ArrayList<>());
            }

            groupedOrders.get(catid).add(order);
        }

        for (Map.Entry<String, List<Order>> entry : groupedOrders.entrySet()) {
            List<String> orderids = new ArrayList<>();
            Category category = categoryRepository.findByCatid(entry.getKey());
            for (Order order : entry.getValue()) {
                orderids.add(order.getOrdid());
            }
            // 直接创建ModelOverviewDTO对象并添加到列表中
            modelOverviewList.add(new ModelOverviewDTO(entry.getKey(), category.getName(), orderids, orderids.size()));
        }
        List<ModelOverviewDTO> result = modelOverviewList.stream().skip(offset).limit(limit).collect(Collectors.toList());
        return customePaging(offset, limit, result);
    }

    public static <T> BasePageResponse<T> customePaging(int offset, int limit, List<T> result) {
        BasePageResponse<T> response = new BasePageResponse<>();
        response.setCode(StatusEnum.SUCCESS.getCode());
        response.setMsg(StatusEnum.SUCCESS.getMessage());
        response.setResults(result);
        PageMeta meta = new PageMeta();
        meta.setOffset(offset);
        meta.setLimit(limit);
        meta.setCount((long) result.size());
        meta.setOrder("-default");
        response.setMeta(meta);
        return response;
    }

    public static int getDaysInMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);  // 月份从 0 开始计数，所以要减 1
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static long[] getYearTimeStamps(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, Calendar.JANUARY, 1, 0, 0, 0);
        long startOfYearTimestamp = calendar.getTimeInMillis();
        calendar.set(year + 1, Calendar.JANUARY, 0, 23, 59, 59);
        long endOfYearTimestamp = calendar.getTimeInMillis();
        return new long[]{startOfYearTimestamp, endOfYearTimestamp};
    }

    public static long[] getMonthTimeStamps(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - Calendar.FEBRUARY, 1, 0, 0, 0);
        long startOfMonthTimestamp = calendar.getTimeInMillis();
        calendar.set(year, month, 0, 23, 59, 59);
        long endOfMonthTimestamp = calendar.getTimeInMillis();
        return new long[]{startOfMonthTimestamp, endOfMonthTimestamp};
    }

    public static long[] getDayTimeStamps(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day, 0, 0, 0);
        long startOfDayTimestamp = calendar.getTimeInMillis();

        calendar.set(year, month - 1, day, 23, 59, 59);
        long endOfDayTimestamp = calendar.getTimeInMillis();

        return new long[]{startOfDayTimestamp, endOfDayTimestamp};
    }
}



