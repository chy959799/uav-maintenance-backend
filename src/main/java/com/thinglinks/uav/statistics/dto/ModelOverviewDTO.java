package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ModelOverviewDTO {
    @ApiModelProperty(value = "机型分类id")
    private String catid;

    @ApiModelProperty(value = "机型")
    private String category;

    @ApiModelProperty(value = "对应订单id")
    private List<String> orderid;

    @ApiModelProperty(value = "数量")
    private int num;
}
