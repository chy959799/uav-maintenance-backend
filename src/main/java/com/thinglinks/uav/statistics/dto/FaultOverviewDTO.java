package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FaultOverviewDTO {
    @ApiModelProperty(value = "对应工单")
    private String orderId;

    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "数量")
    private long total;
}
