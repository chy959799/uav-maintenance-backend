package com.thinglinks.uav.statistics.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ExportDTO {
    @NotNull
    private String target;

    private List<String> ids;
}
