package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrderOverviewDTO {
    @ApiModelProperty(value = "累计服务工单")
    private long orderCount;

    @ApiModelProperty(value = "服务客户数量")
    private long customerCount;

    @ApiModelProperty(value = "维保设备数量")
    private long deviceCount;

    @ApiModelProperty(value = "平均处理时长 单位秒")
    private Double averageProcessingTime;

    @ApiModelProperty(value = "新增工单数量")
    private long newOrderCount;

    @ApiModelProperty(value = "取消工单数量")
    private long cancelOrderCount;

    @ApiModelProperty(value = "完成工单数量")
    private long finishOrderCount;

    @ApiModelProperty(value = "管理后台工单")
    private long adminOrderCount;

    @ApiModelProperty(value = "小程序工单")
    private long appOrderCount;

    @ApiModelProperty(value = "维修工单")
    private long repairOrderCount;

    @ApiModelProperty(value = "保养工单")
    private long maintainOrderCount;

    @ApiModelProperty(value = "保内工单")
    private long innerOrderCount;

    @ApiModelProperty(value = "保外工单")
    private long outerOrderCount;

    @ApiModelProperty(value = "周期数据统计")
    List<OrderItem> orderItems;
}