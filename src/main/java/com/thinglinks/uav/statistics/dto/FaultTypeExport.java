package com.thinglinks.uav.statistics.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class FaultTypeExport {
    @ExcelProperty(value = "故障类型名称")
    private String name;
    @ExcelProperty(value = "工单编码")
    private String orderCode;
}
