package com.thinglinks.uav.statistics.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class CustomerRepairExportDTO {
    @ExcelProperty(value = "客户名称")
    private String customerName;

    @ExcelProperty(value = "订单编号")
    private String orderCode;

    @ExcelProperty(value = "设备型号")
    private String DeviceCode;

    @ExcelProperty(value = "送修单位")
    private String companyName;
}
