package com.thinglinks.uav.statistics.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ModelExportDTO {
    @ExcelProperty(value = "机型名称")
    private String modelName;
    @ExcelProperty(value = "工单编码")
    private String orderCode;
}
