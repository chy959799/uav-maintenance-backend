package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DeviceDetailDTO {
    @ApiModelProperty(value = "设备产品名称")
    private String name;

    @ApiModelProperty(value = "设备型号")
    private String code;

    @ApiModelProperty(value = "送修单位")
    private String companyName;
}
