package com.thinglinks.uav.statistics.dto;

import lombok.Data;

import java.util.List;

@Data
public class OrderCategoriesDTO {
    private String catid;
    private String cateName;

    private List<String> ordid;

    private int count;
}
