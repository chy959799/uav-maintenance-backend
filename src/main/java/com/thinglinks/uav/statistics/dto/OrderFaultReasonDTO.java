package com.thinglinks.uav.statistics.dto;

import lombok.Data;

@Data
public class OrderFaultReasonDTO {
    private String orderId;
    private String catId;
    private String reason;
}
