package com.thinglinks.uav.statistics.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TargetRequest extends BasePageRequest {
    @NotNull
    private String target;

    private String cuid;
}
