package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RepairNumDTO {

    @ApiModelProperty(value = "客户id")
    private String cid;

    @ApiModelProperty(value = "客户名称")
    private String name;

    @ApiModelProperty(value = "对应订单")
    private List<String> ordid;

    @ApiModelProperty(value = "客户送修飞机数量")
    private long num;
}
