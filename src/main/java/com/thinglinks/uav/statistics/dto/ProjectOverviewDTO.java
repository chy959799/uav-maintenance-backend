package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProjectOverviewDTO {

    @ApiModelProperty(value = "项目id")
    private String pjid;

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "工单总数")
    private long total;

    @ApiModelProperty(value = "项目完成数")
    private long finish;

    @ApiModelProperty(value = "工单合计费用")
    private Double fees;
}
