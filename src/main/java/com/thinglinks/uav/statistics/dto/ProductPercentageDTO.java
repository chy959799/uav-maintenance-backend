package com.thinglinks.uav.statistics.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductPercentageDTO {
    private String productName;
    private String num;
}
