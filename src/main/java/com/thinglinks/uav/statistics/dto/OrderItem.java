package com.thinglinks.uav.statistics.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderItem {
    @ApiModelProperty(value = "周期")
    private String cycle;
    @ApiModelProperty(value = "标签")
    private String tag;
    @ApiModelProperty(value = "新增工单数")
    private long newOrder;
    @ApiModelProperty(value = "取消工单数")
    private long cancelOrder;
    @ApiModelProperty(value = "完成工单数")
    private long finishOrder;
}
