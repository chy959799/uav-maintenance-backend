package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "order_quote")
@Data
public class OrderQuote extends BaseEntity {

    @Column(name = "oqid")
    @ApiModelProperty(value = "内部ID")
    private String oqid;

    @Column(name = "type")
    @ApiModelProperty(value = "1:成本报价单；2：客户报价单")
    private String type;

    @Column(name = "order_1")
    @ApiModelProperty(value = "报价顺序,成本报价可能有多次")
    private Integer order;

    @Column(name = "spare_price")
    @ApiModelProperty(value = "备件费")
    private Double sparePrice;

    @Column(name = "expresses_price")
    @ApiModelProperty(value = "快递费")
    private Double expressesPrice;

    @Column(name = "other_price")
    @ApiModelProperty(value = "其他费用")
    private Double otherPrice;

    @Column(name = "other_remark")
    @ApiModelProperty(value = "other_remark")
    private String otherRemark;

    @Column(name = "ordid")
    @ApiModelProperty(value = "工单id")
    private String ordid;

    @Column(name = "uid")
    private String uid;

}
