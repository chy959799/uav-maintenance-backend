package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/27
 */
@Entity
@Table(name = "order_settle")
@Data
public class OrderSettle extends BaseEntity {

    @Column(name = "settle_id")
    @ApiModelProperty(value = "结算内部ID")
    private String settleId;

    @Column(name = "type")
    @ApiModelProperty(value = "1:客户结算；2：返厂预付款")
    private String type;

    @Column(name = "receivable")
    @ApiModelProperty(value = "工程应收款")
    private Double receivable;

    @Column(name = "received")
    @ApiModelProperty(value = "是否已收款")
    private Boolean received;

    @Column(name = "receive_at")
    @ApiModelProperty(value = "收款时间")
    private Long receiveAt;

    @Column(name = "invoice_amount")
    @ApiModelProperty(value = "开票金额")
    private String invoiceAmount;

    @Column(name = "invoice_no")
    @ApiModelProperty(value = "发票号")
    private String invoiceNo;

    @Column(name = "invoice_code")
    @ApiModelProperty(value = "发票代码")
    private String invoiceCode;

    @Column(name = "invoice_at")
    @ApiModelProperty(value = "发票日期")
    private Long invoiceAt;

    @Column(name = "advanced")
    @ApiModelProperty(value = "是否预付款")
    private Boolean advanced;

    @Column(name = "advance_object")
    @ApiModelProperty(value = "付款对象")
    private String advanceObject;

    @Column(name = "advance_amount")
    @ApiModelProperty(value = "付款金额")
    private String advanceAmount;

    @Column(name = "advance_at")
    @ApiModelProperty(value = "付款时间")
    private Long advanceAt;

    @Column(name = "finance_no")
    @ApiModelProperty(value = "财务单号")
    private String financeNo;

    @Column(name = "ordid")
    private String ordid;

    @Column(name = "uid")
    private String uid;

    @Column(name = "invoice_out")
    @ApiModelProperty(value = "是否开票")
    private Boolean invoiceOut;

}
