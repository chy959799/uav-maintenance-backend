package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "expresses")
@Data
public class Expresses extends BaseEntity {

    @Column(name = "expid")
    @ApiModelProperty(value = "快递内部ID")
    private String expid;

    @Column(name = "type")
    @ApiModelProperty(value = "快递来源 1:设备收件 2:返厂寄件 3:客户寄件 ")
    private String type;

    @Column(name = "waybill_list")
    @ApiModelProperty(value = "运单列表")
    private String waybillList;

    @Column(name = "waybill_type")
    @ApiModelProperty(value = "运单号类型1:母单 2:子单 3:签回单")
    private String waybillType;

    @Column(name = "waybill_no")
    @ApiModelProperty(value = "运单号")
    private String waybillNo;

    @Column(name = "filter_result")
    @ApiModelProperty(value = "1:人工确认 2:可收派 3:不可以收派")
    private String filterResult;

    @Column(name = "remark")
    @ApiModelProperty(value = "1：收方超范围 2：派方超范围 3：其它原因 高峰管控提示信息 【数字】：【高峰管控提示信息】 (如 4：温馨提示 ，1：春运延时)")
    private String remark;

    @Column(name = "price")
    @ApiModelProperty(value = "快递金额")
    private Double price;

    @Column(name = "pkg_info")
    @ApiModelProperty(value = "包裹信息 meterageWeightQty:包裹计费重量单位kg productName:如顺丰特惠，顺丰标快 quantity:托寄物包裹数量 volume: 托寄物总体积")
    private String pkgInfo;

    @Column(name = "fees_list")
    @ApiModelProperty(value = "费用清单 字段与https://open.sf-express.com/Api/ApiDetails?level3=189&interName=%E6%B8%85%E5%8D%95%E8%BF%90%E8%B4%B9%E6%8E%A8%E9%80%81%E6%8E%A5%E5%8F%A3-EXP_RECE_WAYBILLS_FEE_PUSH 保持一致")
    private String feesList;

    @Column(name = "state")
    @ApiModelProperty(value = "预约状态")
    private String state;

    @Column(name = "status")
    @ApiModelProperty(value = "快递状态:1:正常 2:取消 3:完成")
    private String status;

    @Column(name = "ordid")
    private String ordid;

    @Column(name = "lordid")
    private String lordid;

    @Column(name = "sender_id")
    private String senderId;

    @Column(name = "receiver_id")
    private String receiverId;

    @Column(name = "expresses_at")
    @ApiModelProperty(value = "快递收件或者快递寄件时间")
    private Long expressesAt;

    @Column(name = "uid")
    private String uid;

    @Column(name = "pickup")
    @ApiModelProperty(value = "取件方式 1:快递自寄 2:快递上门")
    private String pickup;

    @Column(name = "express_type")
    @ApiModelProperty(value = "快递类型 0-顺丰快递，1-邮政快递")
    private Integer expressType;
}
