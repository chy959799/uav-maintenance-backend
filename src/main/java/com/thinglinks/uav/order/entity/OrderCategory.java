package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Entity
@Table(name = "order_categories")
@Data
public class OrderCategory extends BaseEntity {

    @Column(name = "ordcatid")
    @ApiModelProperty(value = "内部ID")
    private String ordcatid;

    @Column(name = "method")
    @ApiModelProperty(value = "故障解决方案")
    private String method;

    @Column(name = "ordid")
    private String ordid;

    @Column(name = "catid")
    private String catid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "catid", referencedColumnName = "catid", insertable = false, updatable = false)
    private Category category;
}
