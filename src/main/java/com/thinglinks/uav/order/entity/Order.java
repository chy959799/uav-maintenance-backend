package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */

@Entity
@Table(name = "orders")
@Data
public class Order extends BaseEntity {

    @Column(name = "ordid")
    @ApiModelProperty(value = "工单内部ID")
    private String ordid;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "工单编号")
    private String innerCode;

    @Column(name = "origin")
    @ApiModelProperty(value = "工单来源 1:管理后台 2:小程序")
    private String origin;

    @Column(name = "type")
    @ApiModelProperty(value = "工单服务类型 1:维修 2:保养")
    private String type;

    @Column(name = "service_type")
    @ApiModelProperty(value = "工单服务类型 1:保内维修 2:框架合同维修；3:快修快换；4:电池报废")
    private String serviceType;

    @Column(name = "history_method")
    @ApiModelProperty(value = "历史维修记录，逗号隔开")
    private String historyMethod;

    @Column(name = "service_method")
    @ApiModelProperty(value = "工单服务方式 维修-1:本地维修 2:返厂维修")
    private String serviceMethod;

    @Deprecated
    @Column(name = "service_level")
    @ApiModelProperty(value = "保养 - 0:常规保养 1:基础保养 2:深度保养, 本地维修 - 0:0级维修 1:1级维修 2:2级维修 3:3级维修 4:4级维修")
    private String serviceLevel;

    @Column(name = "level")
    @ApiModelProperty(value = "1:不紧急 2:一般 3: 紧急 4: 非常紧急")
    private String level;

    @Column(name = "status")
    @ApiModelProperty(value = "工单状态: 0000:已取消;0001: 已失效;0002:取消待审核;0003:撤销取消待审核:1000:待收件;1001:设备待确认;1002:设备已确认;1003:设备保险待确认;1004:报废待审核;1005:报废待处理;2000:待定损;2001:定损待审核;2002:返厂待寄件;3000:待报价;3001:待报价审核;3002:报价待确认;3003:保险资料收集;4000:待结算;4001:返厂待收件;4002:待派单;4003:维修中;5000:待检测;5001:待寄件;6000:已完成")
    private String status;

    @Column(name = "pre_status")
    @ApiModelProperty(value = "工单的前一个状态")
    private String preStatus;

    @Column(name = "cancel_type")
    @ApiModelProperty(value = "取消类型：1:客户取消；2:后台取消")
    private String cancelType;

    @Column(name = "review_insurance_status")
    @ApiModelProperty(value = "保险资料审核状态；00：保险信息待确认；10：出版资料待提交；11：初版资料待审核；12：出版资料已完成;13：出版资料已驳回；20：终版资料待提交；21：终版资料待审核；22理赔待完成；23：终版资料待驳回；30：已完成")
    private String reviewInsuranceStatus;

    @Column(name = "remark")
    @ApiModelProperty(value = "工单备注")
    private String remark;

    @Column(name = "description")
    @ApiModelProperty(value = "工单问题描述")
    private String description;

    @Column(name = "failure_analysis")
    @ApiModelProperty(value = "工单故障原因分析")
    private String failureAnalysis;

    @Column(name = "maintenance_method")
    @ApiModelProperty(value = "工单解决方案")
    private String maintenanceMethod;

    @ApiModelProperty(value = "工单最终处理结果")
    private Long finishAt;

    @Column(name = "total_time")
    @ApiModelProperty(value = "工单耗时,单位秒")
    private Integer totalTime;

    @Column(name = "service_remark")
    @ApiModelProperty(value = "服务评价")
    private String serviceRemark;

    @Column(name = "service_attitude")
    @ApiModelProperty(value = "服务态度")
    private Float serviceAttitude;

    @Column(name = "service_efficiency")
    @ApiModelProperty(value = "服务效率")
    private Float serviceEfficiency;

    @Column(name = "service_tech_level")
    @ApiModelProperty(value = "服务技术水平")
    private Float serviceTechLevel;

    @Column(name = "secondary")
    @ApiModelProperty(value = "是否二次维修：1:是；0:否")
    private Integer secondary;

    @Column(name = "review_insurance_remark")
    @ApiModelProperty(value = "保险审核备注")
    private String reviewInsuranceRemark;

    @Column(name = "back_insurance_remark")
    @ApiModelProperty(value = "保险回退备注")
    private String backInsuranceRemark;

    @Column(name = "cpid")
    @ApiModelProperty(value = "维修点")
    private String cpid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cpid", referencedColumnName = "cpid", insertable = false, updatable = false)
    private Company company;

    @Column(name = "cid")
    private String cid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cid", referencedColumnName = "cid", insertable = false, updatable = false)
    private Customer customer;

    @Column(name = "ctid")
    private String ctid;

    @Column(name = "aid")
    private String aid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aid", referencedColumnName = "aid", insertable = false, updatable = false)
    private Address address;

    @Column(name = "devid")
    private String devid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "devid", referencedColumnName = "devid", insertable = false, updatable = false)
    private Device device;

    @Column(name = "iid")
    private String iid;

    @Column(name = "uid")
    @ApiModelProperty(value = "创建用户ID")
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;

    @Column(name = "cuid")
    @ApiModelProperty(value = "客户的ID")
    private String cuid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cuid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User cuser;

    @Column(name = "maintainer_uid")
    @ApiModelProperty(value = "维修用户ID")
    private String maintainerUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "maintainer_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User maintainerUser;

    @Column(name = "receive_no")
    @ApiModelProperty(value = "收件单号")
    private String receiveNo;

    @Column(name = "receive_uid")
    @ApiModelProperty(value = "收件用户ID")
    private String receiveUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "receive_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User receiveUser;

    @Column(name = "review_insurance_uid")
    @ApiModelProperty(value = "保险审核用户ID")
    private String reviewInsuranceUid;

    @Column(name = "pjid")
    @ApiModelProperty(value = "项目id")
    private String pjid;

    //故障类型 暂时不管
    @Column(name = "catid")
    private String catid;

    @Column(name = "return_case_no")
    @ApiModelProperty(value = "返厂案例号")
    private String returnCaseNumber;

    @Column(name = "loss_uid")
    @ApiModelProperty(value = "定损工程师")
    private String lossUid;

    @Column(name = "review_loss_uid")
    @ApiModelProperty(value = "定损审核工程师")
    private String reviewLossUid;

    @Column(name = "review_loss_remark")
    @ApiModelProperty(value = "定损审核备注")
    private String reviewLossRemark;

    @Column(name = "loss_remark")
    @ApiModelProperty(value = "定损备注")
    private String lossRemark;

    @Column(name = "return_send_uid")
    @ApiModelProperty(value = "返厂寄件用户ID")
    private String returnSendUid;

    @Column(name = "quote_remark")
    @ApiModelProperty(value = "报价备注")
    private String quoteRemark;

    @Column(name = "quote_uid")
    @ApiModelProperty(value = "报价用户id")
    private String quoteUid;

    @Column(name = "review_quote_uid")
    @ApiModelProperty(value = "报价审核用户ID")
    private String reviewQuoteUid;

    @Column(name = "review_quote_remark")
    @ApiModelProperty(value = "报价审核备注")
    private String reviewQuoteRemark;

    @Column(name = "settle_uid")
    @ApiModelProperty(value = "结算用户ID")
    private String settleUid;

    @Column(name = "return_receive_uid")
    @ApiModelProperty(value = "返厂收件用户Id")
    private String returnReceiveUid;

    @Column(name = "dispatch_uid")
    @ApiModelProperty(value = "派单用户ID")
    private String dispatchUid;

    @Column(name = "maintain_feedback")
    @ApiModelProperty(value = "维修反馈内容")
    private String maintainFeedback;

    @Column(name = "maintain_result")
    @ApiModelProperty(value = "维修结果")
    private String maintainResult;

    @Column(name = "maintain_back")
    @ApiModelProperty(value = "维修退回原因")
    private String maintainBack;

    @Column(name = "check_uid")
    @ApiModelProperty(value = "检修工程师")
    private String checkUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "check_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User checkUser;

    @Column(name = "check_draw")
    @ApiModelProperty(value = "是否拉距测试")
    private Boolean checkDraw;

    @Column(name = "check_remark")
    @ApiModelProperty(value = "检修备注-检修不通过")
    private String checkRemark;

    @Column(name = "customer_send_uid")
    @ApiModelProperty(value = "客户寄件用户ID")
    private String customerSendUid;

    @Column(name = "insurance_reject_reason")
    @ApiModelProperty(value = "保险审核不通过原因")
    private String insuranceRejectReason;

    @Column(name = "cancel_reason")
    @ApiModelProperty(value = "取消工单原因")
    private String cancelReason;

    @Column(name = "cancel_uid")
    @ApiModelProperty(value = "取消用户ID")
    private String cancelUid;

    @Column(name = "cancel_at")
    @ApiModelProperty(value = "取消时间")
    private Long cancelAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cancel_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User cancelUser;

    @Column(name = "review_revocation_remark")
    @ApiModelProperty(value = "撤销取消原因")
    private String reviewRevocationRemark;

    @Column(name = "review_revocation_uid")
    @ApiModelProperty(value = "撤销取消用户")
    private String reviewRevocationUid;

    @Column(name = "review_cancel_uid")
    @ApiModelProperty(value = "审核取消用户ID")
    private String reviewCancelUid;

    @Column(name = "review_cancel_remark")
    @ApiModelProperty(value = "审核取消备注")
    private String reviewCancelRemark;

    @Column(name = "first_review_uid")
    @ApiModelProperty(value = "保险初版资料审核用户id")
    private String firstReviewUid;

    @Column(name = "first_review_remark")
    @ApiModelProperty(value = "保险初版资料审核备注")
    private String firstReviewRemark;

    @Column(name = "last_review_uid")
    @ApiModelProperty(value = "保险终版资料审核用户id")
    private String lastReviewUid;

    @Column(name = "last_review_remark")
    @ApiModelProperty(value = "保险终版资料审核备注")
    private String lastReviewRemark;

    @ApiModelProperty(value = "工单关联编号-二次维修")
    @Column(name = "relation_order_code")
    private String relationOrderCode;

    @ApiModelProperty(value = "设备sn是否变更")
    @Column(name = "code_change")
    private Boolean codeChange;

    @ApiModelProperty(value = "遥控器sn是否变更")
    @Column(name = "remote_change")
    private Boolean remoteChange;

    @ApiModelProperty(value = "电池sn是否变更")
    @Column(name = "battery_change")
    private Boolean batteryChange;

    @ApiModelProperty(value = "是否需要重新定损")
    @Column(name = "re_quote")
    private Boolean reQuote;

    @ApiModelProperty(value = "报废审核用户ID")
    @Column(name = "review_scrap_uid")
    private String reviewScrapUid;

    @ApiModelProperty(value = "报废审核备注")
    @Column(name = "review_scrap_remark")
    private String reviewScrapRemark;

    @ApiModelProperty(value = "报废处理用户ID")
    @Column(name = "handle_scrap_uid")
    private String handleScrapUid;

    @Column(name = "be_settle")
    @ApiModelProperty(value = "客户是否收费")
    private Boolean beSettle;

    @Column(name = "manual")
    @ApiModelProperty(value = "是否手动填写,默认ture")
    private Boolean manual;

}
