package com.thinglinks.uav.order.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Entity
@Table(name = "order_logs")
@Data
public class OrderLog extends BaseEntity {

    @Column(name = "ordlogid")
    @ApiModelProperty(value = "工单日志内部ID")
    private String ordlogid;

    @Column(name = "status")
    @JsonProperty(value = "status")
    @ApiModelProperty(value = "工单状态")
    private String status;

    @Column(name = "method")
    @ApiModelProperty(value = "操作类型 0000:工单取消;0001:工单取消审核;0002:工单快递取消;0003:系统自动失效;0004:客户撤回取消;0005:工单报废审核;0006:工单报废处理;1000:创建工单;1001:确认收件;2000:客户设备确认;2001:保险资料审核;2002:客户初版资料提交;2003:客户初版资料审核;2004:客户终版资料提交;2005:客户终版资料审核;2006:保险理赔录入;2007 保险理赔完成;3000:定损;3001:定损审核;3002:返厂寄件;4000:定损报价;4001:报价审核;4002:客户确认报价;5000:工单结算;5001:提交工单结算;5002:返厂收件;5003:派单;5004:设备维修反馈;5005:设备维修回退;5006:设备维修完成;5007:重新定损;6000:开始维修检测;6001:维修检测完成;6002:寄件")
    private String method;

    @Column(name = "approve")
    @ApiModelProperty(value = "审批结果")
    private Integer approve;

    @Column(name = "remark")
    @ApiModelProperty(value = "操作备注(同时作为数据缓存)")
    private String remark;

    @Column(name = "description")
    @ApiModelProperty(value = "操作记录")
    private String description;

    @Column(name = "ordid")
    private String ordid;

    @Column(name = "uid")
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;
}
