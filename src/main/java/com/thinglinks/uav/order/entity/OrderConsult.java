package com.thinglinks.uav.order.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.system.annotation.TenantTable;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Entity
@Table(name = "order_consults")
@Data
@TenantTable
public class OrderConsult extends BaseEntity {

    @Column(name = "ordid")
    @JsonProperty(value = "ordid")
    @ApiModelProperty(value = "工单内部ID")
    private String ordid;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "工单外部编号")
    private String innerCode;

    @Column(name = "consult_at")
    @ApiModelProperty(value = "工单咨询时间")
    private Long consultTime;

    @Column(name = "description")
    @ApiModelProperty(value = "咨询工单问题描述")
    private String description;

    @Column(name = "cpid")
    private String cpid;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "客户")
    @JoinColumn(name = "cpid", referencedColumnName = "cpid", insertable = false, updatable = false)
    private Company company;

    @Column(name = "cid")
    private String cid;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "单位")
    @JoinColumn(name = "cid", referencedColumnName = "cid", insertable = false, updatable = false)
    private Customer customer;

    @Column(name = "ctid")
    private String ctid;

    @ApiModelProperty(value = "创建人uid")
    @Column(name = "created_by")
    private String createdBy;

    @ApiModelProperty(value = "联系人id")
    @Column(name = "uid")
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "联系人")
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;

    @Column(name = "recorder_uid")
    private String recorderUid;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "接待人")
    @JoinColumn(name = "recorder_uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User recorderUser;
}
