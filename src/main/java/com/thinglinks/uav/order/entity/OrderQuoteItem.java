package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "order_quote_item")
@Data
public class OrderQuoteItem extends BaseEntity {

    @Column(name = "oqiid")
    @ApiModelProperty(value = "内部ID")
    private String oqiid;

    @Column(name = "oqid")
    @ApiModelProperty(value = "报价单id")
    private String oqid;

    @Column(name = "pid")
    @ApiModelProperty(value = "产品id")
    private String pid;

    @Column(name = "sale_price")
    @ApiModelProperty(value = "销售价")
    private Double salePrice;

    @Column(name = "work_hour")
    @ApiModelProperty(value = "工时")
    private Double workHour;

    @Column(name = "work_hour_cost")
    @ApiModelProperty(value = "工时费")
    private Double workHourCost;

    @Column(name = "total_cost")
    @ApiModelProperty(value = "总费用")
    private Double totalCost;

    @Column(name = "catid")
    @ApiModelProperty(value = "分类id")
    private String catid;

    @Column(name = "count")
    @ApiModelProperty(value = "备件数量")
    private Integer count;

}
