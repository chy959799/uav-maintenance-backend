package com.thinglinks.uav.order.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/27
 */
@Entity
@Table(name = "approve")
@Data
public class Approve extends BaseEntity {

    @Column(name = "approve_id")
    @ApiModelProperty(value = "审批记录内部id")
    private String approveId;

    @Column(name = "type")
    @JsonProperty(value = "type")
    @ApiModelProperty(value = "业务类型：1:工单审批")
    private Integer type;

    @Column(name = "bsy_id")
    @ApiModelProperty(value = "业务数据id")
    private String bsyId;

    @Column(name = "bys_status")
    @ApiModelProperty(value = "业务数据状态")
    private String bysStatus;

    @Column(name = "status")
    @JsonProperty(value = "status")
    @ApiModelProperty(value = "审批状态：1：通过；2:驳回")
    private String status;

    @Column(name = "remark")
    @JsonProperty(value = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    @Column(name = "order")
    @JsonProperty(value = "order")
    @ApiModelProperty(value = "审批顺序")
    private Integer order;

    @Column(name = "uid")
    private String uid;

}
