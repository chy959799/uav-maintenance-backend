package com.thinglinks.uav.order.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/14
 */
@Entity
@Table(name = "insurance_material")
@Data
public class InsuranceMaterial extends BaseEntity {

    @Column(name = "imid")
    @ApiModelProperty(value = "内部ID")
    private String imid;

    @Column(name = "accident_user")
    @ApiModelProperty(value = "出险人")
    private String accidentUser;

    @Column(name = "accident_tel")
    @ApiModelProperty(value = "出险人联系电话")
    private String accidentTel;

    @Column(name = "cid")
    @ApiModelProperty(value = "出险人单位")
    private String cid;

    @Column(name = "be_report")
    @ApiModelProperty(value = "是否已报案")
    private Boolean beReport;

    @Column(name = "report_no")
    @ApiModelProperty(value = "报案号")
    private String reportNo;

    @Column(name = "report_amt")
    @ApiModelProperty(value = "报案金额")
    private Double reportAmt;

    @Column(name = "report_at")
    @ApiModelProperty(value = "报案时间")
    private Long reportAt;

    @Column(name = "fault_type")
    @ApiModelProperty(value = "故障类别：1:坠机；2:碰撞")
    private String faultType;

    @Column(name = "fault_at")
    @ApiModelProperty(value = "故障发生时间")
    private Long faultAt;

    @Column(name = "fault_location")
    @ApiModelProperty(value = "故障地点")
    private String faultLocation;

    @Column(name = "fault_reason")
    @ApiModelProperty(value = "故障原因")
    private String faultReason;

    @Column(name = "fault_process")
    @ApiModelProperty(value = "故障处置经过")
    private String faultProcess;

    @Column(name = "pilot_name")
    @ApiModelProperty(value = "飞手名字")
    private String pilotName;

    @Column(name = "id_type")
    @ApiModelProperty(value = "证件类型：1:CAAC;2:AOPA;3:UTC;4:内部合格证书")
    private String idType;

    @Column(name = "effective_start_at")
    @ApiModelProperty(value = "证件生效时间")
    private Long effectiveStartAt;

    @Column(name = "effective_end_at")
    @ApiModelProperty(value = "证件失效时间")
    private Long effectiveEndAt;

    @Column(name = "ordid")
    private String ordid;

    @Column(name = "uid")
    private String uid;

    @Column(name = "invoice_amt")
    @ApiModelProperty(value = "开票金额")
    private String invoiceAmount;

    @Column(name = "invoice_no")
    @ApiModelProperty(value = "发票号")
    private String invoiceNo;

    @Column(name = "invoice_code")
    @ApiModelProperty(value = "发票代码")
    private String invoiceCode;

    @Column(name = "invoice_at")
    @ApiModelProperty(value = "发票日期")
    private Long invoiceAt;

    @Column(name = "invoice_out")
    @ApiModelProperty(value = "是否开票")
    private Boolean invoiceOut;

    @ApiModelProperty(value = "工作类型")
    @Column(name = "work_type")
    private String workType;

    @ApiModelProperty(value = "受损情况")
    @Column(name = "damaged")
    private String damaged;
}
