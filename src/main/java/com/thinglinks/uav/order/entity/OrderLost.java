package com.thinglinks.uav.order.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Entity
@Table(name = "order_losts")
@Data
@TenantTable
public class OrderLost extends BaseEntity {

    @Column(name = "ordid")
    @JsonProperty(value = "ordid")
    @ApiModelProperty(value = "工单内部ID")
    private String ordid;

    @Column(name = "inner_code")
    @ApiModelProperty(value = "工单外部编号")
    private String innerCode;

    @Column(name = "insurance_code")
    @ApiModelProperty(value = "保险单号")
    private String insuranceCode;

    @Column(name = "insurance_time")
    @ApiModelProperty(value = "出险时间")
    private Long insuranceTime;

    @Column(name = "insurance_at")
    @ApiModelProperty(value = "出险地点")
    private String insuranceAt;

    @Column(name = "insurance_reason")
    @ApiModelProperty(value = "出险原因")
    private String insuranceReason;

    @Column(name = "report_code")
    @ApiModelProperty(value = "报案号")
    private String reportCode;

    @Column(name = "report_time")
    @ApiModelProperty(value = "报案号")
    private Long reportTime;

    @Column(name = "insurance_cost")
    @ApiModelProperty(value = "报损金额")
    private String insuranceCost;

    @Column(name = "material_time")
    @ApiModelProperty(value = "收资料时间")
    private Long materialTime;

    @Column(name = "invoice_code")
    @ApiModelProperty(value = "开票票号")
    private String invoiceCode;

    @Column(name = "invoice_cost")
    @ApiModelProperty(value = "开票金额")
    private String invoiceCost;

    @Column(name = "invoice_time")
    @ApiModelProperty(value = "开票时间")
    private Long invoiceTime;

    @Column(name = "payment")
    @ApiModelProperty(value = "支付对象")
    private String payment;

    @Column(name = "new_sn")
    @ApiModelProperty(value = "更换后的SN码")
    private String newSn;

    @Column(name = "deliver_time")
    @ApiModelProperty(value = "新机交付时间")
    private String deliverTime;

    @Column(name = "status")
    @ApiModelProperty(value = "工单状态 1:待审批 2:审批-寄件 3:审批-退款")
    private String status;

    @Column(name = "remark")
    @JsonProperty(value = "remark")
    @ApiModelProperty(value = "审批备注")
    private String remark;

    @Column(name = "review_time")
    @ApiModelProperty(value = "工单审核时间")
    private Long reviewTime;

    @Column(name = "description")
    @ApiModelProperty(value = "工单描述")
    private String description;

    @Column(name = "finish_time")
    @ApiModelProperty(value = "工单完成时间")
    private String finishTime;

    @Column(name = "cpid")
    private String cpid;

    @Column(name = "cid")
    private String cid;

    @Column(name = "ctid")
    private String ctid;

    @Column(name = "devid")
    private String devid;

    @Column(name = "uid")
    private String uid;

    @Column(name = "review_uid")
    private String reviewUid;
}
