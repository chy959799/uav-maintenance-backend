package com.thinglinks.uav.order.controller;

import com.thinglinks.uav.common.dto.*;
import com.thinglinks.uav.order.dto.*;
import com.thinglinks.uav.order.dto.excel.CellEnum;
import com.thinglinks.uav.order.dto.excel.ExportCell;
import com.thinglinks.uav.order.dto.excel.ExportField;
import com.thinglinks.uav.order.dto.quote.DetailOrderQuote;
import com.thinglinks.uav.order.dto.quote.OrderQuoteRequest;
import com.thinglinks.uav.order.dto.quote.QuoteSpare;
import com.thinglinks.uav.order.service.OrderService;
import com.thinglinks.uav.stock.dto.DetailStockInOut;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */

@Api(tags = "工单管理")
@RestController
@RequestMapping(value = "orders")
@Slf4j
public class OrderController {

    @Resource
    private OrderService orderService;

    @Operation(summary = "工单分页查询")
    @GetMapping(path = "page")
    public BasePageResponse<PageOrder> pageOrder(PageOrderQuery request) {
        return orderService.pageOrder(request);
    }

    @Operation(summary = "查询工单导出字段")
    @GetMapping(path = "export")
    public BaseResponse<List<ExportCell>> queryExportCell() {
        return BaseResponse.success(CellEnum.getCells());
    }

    @Authority(action = "orderExport")
    @Operation(summary = "工单导出")
    @PostMapping(path = "export")
    public void exportOrder(HttpServletResponse response, PageOrderQuery query, @RequestBody ExportField exportField) throws Exception {
        orderService.exportOrder(response, query, exportField);
    }

    @Authority(action = "orderCreate", name = "工单创建")
    @Operation(summary = "收件人员新增工单")
    @PostMapping
    public BaseResponse<Void> addOrderByReceive(@Valid OrderByReceive request) throws Exception {
        return orderService.addOrderByReceive(request);
    }

    @Authority(action = "userOrderCreate", name = "新增工单申请")
    @Operation(summary = "客户新增工单")
    @PostMapping(path = "user")
    public BaseResponse<Void> addOrderByUser(@Valid @RequestBody OrderByUser request) throws Exception {
        return orderService.addOrderByUser(request);
    }

    @Operation(summary = "新增报废工单")
    @PostMapping(path = "scrap")
    @Authority(action = "orderCreate", name = "工单创建")
    public BaseResponse<Void> addScrapOrder(@Valid ScrapOrderByReceive request) throws Exception {
        return orderService.addScrapOrder(request);
    }

    @Operation(summary = "出版资料提交生成文件预览")
    @PostMapping(path = "material/preview")
    public BaseResponse<List<FilePreview>> materialPreview(@Valid @RequestBody InsurancePreview request) throws Exception {
        return orderService.materialPreview(request);
    }

    @Authority(action = "receiveExpress", name = "确认收件")
    @Operation(summary = "确认收件")
    @PostMapping(path = "{orderId}/receiveExpress")
    public BaseResponse<Void> receiveExpress(@PathVariable(name = "orderId") String orderId, @Valid ReceiveExpress request) throws Exception {
        return orderService.receiveExpress(orderId, request);
    }

    @Authority(action = "confirmDevice", name = "确认设备")
    @Operation(summary = "确认设备")
    @PostMapping(path = "{orderId}/confirmDevice")
    public BaseResponse<Void> confirmDevice(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.confirmDevice(orderId);
    }

    @Authority(action = "reviewFirstInsurance", name = "初版资料审核")
    @Operation(summary = "初版资料审核")
    @PostMapping(path = "{orderId}/reviewFirstInsurance")
    public BaseResponse<Void> reviewFirstInsurance(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReviewResult request) throws Exception {
        return orderService.reviewFirstInsurance(orderId, request);
    }

    @Authority(action = "reviewLastInsurance", name = "终版资料审核")
    @Operation(summary = "终版资料审核")
    @PostMapping(path = "{orderId}/reviewLastInsurance")
    public BaseResponse<Void> reviewLastInsurance(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReviewResult request) throws Exception {
        return orderService.reviewLastInsurance(orderId, request);
    }

    @Authority(action = "finishInsurance", name = "保险理赔完成")
    @Operation(summary = "保险理赔完成")
    @PostMapping(path = "{orderId}/finishInsurance")
    public BaseResponse<Void> finishInsurance(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.finishInsurance(orderId);
    }

    @Authority(action = "reviewInsurance", name = "确认保险")
    @Operation(summary = "保险资料审核")
    @PostMapping(path = "{orderId}/reviewInsurance")
    public BaseResponse<Void> reviewInsurance(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody AuditInsurance request) throws Exception {
        return orderService.reviewInsurance(orderId, request);
    }

    @Authority(action = "backInsurance", name = "工单保险回退")
    @Operation(summary = "工单保险回退")
    @PostMapping(path = "{orderId}/backInsurance")
    public BaseResponse<Void> backInsurance(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody Remark request) throws Exception {
        return orderService.backInsurance(orderId, request);
    }

    @Authority(action = "orderDelete", name = "工单删除")
    @Operation(summary = "删除工单")
    @DeleteMapping(path = "{orderId}")
    public BaseResponse<Void> deleteOrder(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.deleteOrder(orderId);
    }

    @Authority(action = "orderDelete", name = "工单删除")
    @Operation(summary = "删除工单")
    @DeleteMapping(path = "ids")
    public BaseResponse<Void> deleteOrder(@RequestBody @Valid IdsRequest request) throws Exception {
        return orderService.deleteOrder(request.getIds());
    }

    @Operation(summary = "工单详情")
    @GetMapping(path = "{orderId}")
    public BaseResponse<DetailOrder> detailOrder(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.detailOrder(orderId);
    }

    @Authority(action = "orderCancel", name = "工单取消")
    @Operation(summary = "系统用户取消工单")
    @PostMapping(path = "{orderId}/cancelOrderIn")
    public BaseResponse<Void> cancelOrderIn(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody Remark request) {
        return orderService.cancelOrderIn(orderId, request);
    }

    @Authority(action = "userOrderCancel", name = "客户取消工单")
    @Operation(summary = "客户取消工单")
    @PostMapping(path = "{orderId}/cancelOrderOut")
    public BaseResponse<Void> cancelOrderOut(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody Remark request) {
        return orderService.cancelOrderOut(orderId, request);
    }

    @Authority(action = "orderCancelReview", name = "取消审批")
    @Operation(summary = "取消工单审核")
    @PostMapping(path = "{orderId}/reviewCancel")
    public BaseResponse<Void> reviewCancel(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReviewResult request) {
        return orderService.reviewCancel(orderId, request);
    }

    @Authority(action = "revocationOrder", name = "客户撤销取消")
    @Operation(summary = "客户撤销取消")
    @PostMapping(path = "{orderId}/revocationOrder")
    public BaseResponse<Void> revocationOrder(@PathVariable(name = "orderId") String orderId) {
        return orderService.revocationOrder(orderId);
    }

    @Authority(action = "reviewRevocation", name = "撤销取消工单审核")
    @Operation(summary = "审核客户撤销取消")
    @PostMapping(path = "{orderId}/reviewRevocation")
    public BaseResponse<Void> reviewRevocation(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReviewResult request) {
        return orderService.reviewRevocation(orderId, request);
    }

    @Authority(action = "confirmLoss", name = "工单定损")
    @Operation(summary = "工单定损")
    @PostMapping(path = "{orderId}/loss")
    public BaseResponse<Void> confirmLoss(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ConfirmLoss request) throws Exception {
        return orderService.confirmLoss(orderId, request);
    }

    @Authority(action = "reConfirmLoss", name = "重新定损")
    @Operation(summary = "重新定损")
    @PostMapping(path = "{orderId}/reloss")
    public BaseResponse<Void> reConfirmLoss(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.reConfirmLoss(orderId);
    }

    @Authority(action = "orderMaintenanceApproval", name = "定损审批")
    @Operation(summary = "定损审核")
    @PostMapping(path = "{orderId}/reviewLoss")
    public BaseResponse<Void> reviewLoss(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReviewResult request) {
        return orderService.reviewLoss(orderId, request);
    }

    @Authority(action = "returnExpress", name = "返厂寄件")
    @Operation(summary = "返厂寄件")
    @PostMapping(path = "{orderId}/returnExpress")
    public BaseResponse<Void> returnExpress(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReturnExpresses request) throws Exception {
        return orderService.returnExpress(orderId, request);
    }

    @Authority(action = "quoteOrder", name = "编辑报价")
    @Operation(summary = "工单报价")
    @PostMapping(path = "{orderId}/quoteOrder")
    public BaseResponse<Void> quoteOrder(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody OrderQuoteRequest request) throws Exception {
        return orderService.quoteOrder(orderId, request);
    }

    @Authority(action = "submitQuote", name = "提交报价")
    @Operation(summary = "提交工单报价")
    @PostMapping(path = "{orderId}/submitQuote")
    public BaseResponse<Void> submitQuote(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody Remark request) throws Exception {
        return orderService.submitQuote(orderId, request);
    }

    @Authority(action = "reviewQuote", name = "报价审批")
    @Operation(summary = "审核工单报价")
    @PostMapping(path = "{orderId}/reviewQuote")
    public BaseResponse<Void> reviewQuote(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReviewQuoteResult request) throws Exception {
        return orderService.reviewQuote(orderId, request);
    }

    @Authority(action = "confirmQuote", name = "确认报价")
    @Operation(summary = "客户确认报价")
    @PostMapping(path = "{orderId}/confirmQuote")
    public BaseResponse<Void> confirmQuote(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.confirmQuote(orderId);
    }

    @Authority(action = "customerSettle", name = "客户结算编辑")
    @Operation(summary = "客户结算编辑（每次传全量数据过来，有ID则更新，无ID则新增）")
    @PostMapping(path = "{orderId}/customerSettle")
    public BaseResponse<Void> customerSettle(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody CustomerSettle request) throws Exception {
        return orderService.customerSettle(orderId, request);
    }

    @Authority(action = "advanceSettle", name = "返厂预付款结算编辑")
    @Operation(summary = "返厂预付款结算编辑（每次传全量数据过来，有ID则更新，无ID则新增）")
    @PostMapping(path = "{orderId}/advanceSettle")
    public BaseResponse<Void> advanceSettle(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody AdvanceSettle request) throws Exception {
        return orderService.advanceSettle(orderId, request);
    }

    @Authority(action = "submitSettle", name = "工单结算")
    @Operation(summary = "工单结算完成")
    @PostMapping(path = "{orderId}/submitSettle")
    public BaseResponse<Void> submitSettle(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.submitSettle(orderId);
    }

    @Authority(action = "returnReceive", name = "返厂收件")
    @Operation(summary = "返厂收件")
    @PostMapping(path = "{orderId}/returnReceive")
    public BaseResponse<Void> submitSettle(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReturnReceive request) throws Exception {
        return orderService.returnReceive(orderId, request);
    }

    @Authority(action = "orderDispatch", name = "工单派单")
    @Operation(summary = "工单派单")
    @PostMapping(path = "{orderId}/dispatchOrder")
    public BaseResponse<Void> dispatchOrder(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody DispatchOrder request) throws Exception {
        return orderService.dispatchOrder(orderId, request);
    }

    @Authority(action = "maintainFeedback", name = "维修记录")
    @Operation(summary = "工单维修记录")
    @PostMapping(path = "{orderId}/maintainFeedback")
    public BaseResponse<Void> maintainFeedback(@PathVariable(name = "orderId") String orderId, @Valid MaintainFeedback request) throws Exception {
        return orderService.maintainFeedback(orderId, request);
    }

    @Authority(action = "finishMaintain", name = "维修完成")
    @Operation(summary = "工单维修完工")
    @PostMapping(path = "{orderId}/finishMaintain")
    public BaseResponse<Void> finishMaintain(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody Remark request) throws Exception {
        return orderService.finishMaintain(orderId, request);
    }

    @Authority(action = "failMaintain", name = "任务回退")
    @Operation(summary = "工单维修回退")
    @PostMapping(path = "{orderId}/failMaintain")
    public BaseResponse<Void> failMaintain(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody Remark request) throws Exception {
        return orderService.failMaintain(orderId, request);
    }

    @Authority(action = "orderChecked", name = "检测完成")
    @Operation(summary = "工单检修完成")
    @PostMapping(path = "{orderId}/orderChecked")
    public BaseResponse<Void> orderChecked(@PathVariable(name = "orderId") String orderId, @Valid OrderCheck request) throws Exception {
        return orderService.orderChecked(orderId, request);
    }

    @Authority(action = "customerExpresses", name = "设备寄件")
    @Operation(summary = "客户寄件")
    @PostMapping(path = "{orderId}/customerExpresses")
    public BaseResponse<Void> customerExpresses(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody CustomerExpresses request) throws Exception {
        return orderService.customerExpresses(orderId, request);
    }

    @Operation(summary = "报价查询")
    @GetMapping(path = "{orderId}/quoteOrder")
    public BaseResponse<DetailOrderQuote> queryOrderQuote(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.queryOrderQuote(orderId);
    }

    @Operation(summary = "工单出库查询")
    @GetMapping(path = "{orderId}/stockOut")
    public BaseResponse<List<DetailStockInOut>> queryOrderStockOut(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.queryOrderStockOut(orderId);
    }

    @Operation(summary = "工单快递列表查询")
    @GetMapping(path = "{orderId}/expresses")
    public BaseResponse<List<DetailExpress>> queryOrderExpresses(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.queryOrderExpresses(orderId);
    }

    @Operation(summary = "工单进度查询")
    @GetMapping(path = "{orderId}/logs")
    public BaseResponse<List<OrderLogDetail>> queryOrderLog(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.queryOrderLog(orderId);
    }

    @Authority(action = "firstInsurance", name = "客户初版保险资料提交")
    @Operation(summary = "客户初版保险资料提交")
    @PostMapping(path = "{orderId}/firstInsurance")
    public BaseResponse<Void> firstInsurance(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody FirstInsurance request) throws Exception {
        return orderService.firstInsurance(orderId, request);
    }

    @Authority(action = "lastInsurance", name = "客户终版保险资料提交")
    @Operation(summary = "客户终版保险资料提交")
    @PostMapping(path = "{orderId}/lastInsurance")
    public BaseResponse<Void> lastInsurance(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody List<String> request) throws Exception {
        return orderService.lastInsurance(orderId, request);
    }

    @Operation(summary = "保险材料查询")
    @GetMapping(path = "{orderId}/insuranceMaterial")
    public BaseResponse<InsuranceMaterialDetail> queryInsuranceMaterial(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.queryInsuranceMaterial(orderId);
    }

    @Authority(action = "reportInvoice", name = "报案发票录入")
    @Operation(summary = "报案发票录入")
    @PostMapping(path = "{orderId}/reportInvoice")
    public BaseResponse<Void> reportInvoice(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ReportInvoice request) throws Exception {
        return orderService.reportInvoice(orderId, request);
    }

    @Operation(summary = "结算详情查询")
    @GetMapping(path = "{orderId}/detailSettle")
    public BaseResponse<DetailSettle> detailSettle(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.detailSettle(orderId);
    }

    @Operation(summary = "备件价格查询")
    @GetMapping(path = "{orderId}/orderSparePrice")
    public BaseResponse<List<OrderSparePrice>> orderSparePrice(@PathVariable(name = "orderId") String orderId, @RequestParam(value = "catid") String catid) throws Exception {
        return orderService.orderSparePrice(orderId, catid);
    }

    @Operation(summary = "系统报价单下载")
    @GetMapping(path = "{orderId}/downloadCostQuote")
    public void downloadCostQuote(HttpServletResponse response, @PathVariable(name = "orderId") String orderId) throws Exception {
        orderService.downloadCostQuote(response, orderId);
    }

    @Operation(summary = "客户报价单下载")
    @GetMapping(path = "{orderId}/downloadCustomerQuote")
    public void downloadCustomerQuote(HttpServletResponse response, @PathVariable(name = "orderId") String orderId) throws Exception {
        orderService.downloadCustomerQuote(response, orderId);
    }

    @Operation(summary = "系统报价单下载-获取下载地址")
    @GetMapping(path = "{orderId}/downloadCostQuote/v2")
    public BaseResponse<String> downloadCostQuote(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.downloadCostQuote(orderId);
    }

    @Operation(summary = "客户报价单下载-获取下载地址")
    @GetMapping(path = "{orderId}/downloadCustomerQuote/v2")
    public BaseResponse<String> downloadCustomerQuote(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.downloadCustomerQuote(orderId);
    }

    @Operation(summary = "查询工单的备件明细")
    @GetMapping(path = "{orderId}/orderQuoteSpare")
    public BaseResponse<List<QuoteSpare>> orderQuoteSpare(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.orderQuoteSpare(orderId);
    }

    @Operation(summary = "查询工单文件列表")
    @GetMapping(path = "{orderId}/orderFile")
    public BaseResponse<List<OrderFile>> orderFile(@PathVariable(name = "orderId") String orderId) throws Exception {
        return orderService.orderFile(orderId);
    }

    @Authority(action = "reviewScrap", name = "报废审核")
    @Operation(summary = "工单报废审核")
    @PostMapping(path = "{orderId}/reviewScrap")
    public BaseResponse<Void> reviewScrap(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ScrapReview request) throws Exception {
        return orderService.reviewScrap(orderId, request);
    }

    @Authority(action = "handleScrap", name = "报废处理")
    @Operation(summary = "工单报废处理")
    @PostMapping(path = "{orderId}/handleScrap")
    public BaseResponse<Void> handleScrap(@PathVariable(name = "orderId") String orderId, @Valid @RequestBody ScrapHandle request) throws Exception {
        return orderService.handleScrap(orderId, request);
    }

    @Authority(action = "insuranceArchived", name = "理赔保险资料归档")
    @Operation(summary = "理赔保险资料归档")
    @PostMapping(path = "{orderId}/insuranceArchived")
    public BaseResponse<Void> insuranceArchived(@PathVariable(name = "orderId") String orderId, @RequestBody List<String> request) throws Exception {
        return orderService.insuranceArchived(orderId, request);
    }

    @Operation(summary = "工单导入")
    @PostMapping(path = "import")
    public void orderImport(MultipartFile file, Integer type, HttpServletResponse response) throws Exception {
        orderService.orderImport(file, type, response);
    }

}

