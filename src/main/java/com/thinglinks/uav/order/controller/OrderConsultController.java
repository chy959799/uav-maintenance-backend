package com.thinglinks.uav.order.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.order.dto.consult.*;
import com.thinglinks.uav.order.service.OrderConsultService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "咨询工单管理")
@RestController
@RequestMapping(value = "orderConsult")
@Slf4j
public class OrderConsultController {
    @Resource
    private OrderConsultService orderConsultService;

    @Operation(summary = "获取咨询工单详情")
    @GetMapping("{ordid}")
    public BaseResponse<OrderConsultDetail> getOrderConsult(@PathVariable(name = "ordid") String ordid) throws Exception {
        return orderConsultService.getOrderConsult(ordid);
    }

    @Authority(action = "orderConsultCreate")
    @Operation(summary = "添加咨询工单")
    @PostMapping
    public BaseResponse<OrderConsultResponse> addOrderConsult(OrderConsultRequest request) throws Exception {
        return orderConsultService.addOrderConsult(request);
    }

    @Operation(summary = "分页查询咨询工单")
    @GetMapping
    public BasePageResponse<OrderConsultDto> pageOrder(OrderConsultPageRequest request) {
        return orderConsultService.pageOrderConsult(request);
    }

    @Authority(action = "orderConsultEdit")
    @Operation(summary = "修改咨询工单")
    @PatchMapping("{ordid}")
    public BaseResponse<OrderConsultResponse> updateOrderConsult(@PathVariable String ordid ,OrderConsultRequest request) throws Exception {
        return orderConsultService.updateOrderConsult(ordid,request);
    }

    @Authority(action = "orderConsultDelete")
    @Operation(summary = "删除咨询工单")
    @DeleteMapping("{ordid}")
    public BaseResponse<String> deleteOrderConsult(@PathVariable String ordid) {
        return orderConsultService.deleteOrderConsult(ordid);
    }

    @Authority(action = "orderConsultEdit")
    @Operation(summary = "文件删除")
    @DeleteMapping(path = "file")
    public BaseResponse<Void> delFile(String url) throws Exception {
        return orderConsultService.delFile(url);
    }
}
