package com.thinglinks.uav.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.common.dto.BasePushResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.order.dto.DetailExpress;
import com.thinglinks.uav.order.dto.expresses.*;
import com.thinglinks.uav.order.service.ExpressesService;
import com.thinglinks.uav.order.service.express.EMSExpressionService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Api(tags = "快递管理")
@RequestMapping(value = "express")
@Slf4j
public class ExpressesController {

    @Resource
    private ExpressesService expressesService;
    @Resource
    private EMSExpressionService emsExpressionService;

    @Operation(summary = "面单pdf文件推送")
    @PostMapping(path = "open/waybillPush")
    public PushResponse waybillPush(PrintWaybills request) throws Exception {
        log.info("waybillPush receive:{}", JSONObject.toJSONString(request));
        return expressesService.waybillPush(request);
    }

    @Operation(summary = "订单状态推送")
    @PostMapping(path = "open/statePush")
    public BasePushResponse statePush(@RequestBody StatePush request) throws Exception {
        log.info("statePush receive:{}", JSONObject.toJSONString(request));
        return expressesService.statePush(request);
    }

    @Operation(summary = "邮政派揽状态推送")
    @PostMapping(path = "open/emsStatePush")
    public BasePushResponse emsStatePush(@RequestBody EmsStatePush request) throws Exception {
        log.info("emsStatePush receive:{}", JSONObject.toJSONString(request));
        return emsExpressionService.statePush(request);
    }

    @Operation(summary = "清单运费推送")
    @PostMapping(path = "open/feePush")
    public BasePushResponse feesPush(FeesPush request) throws Exception {
        log.info("feePush receive:{}", JSONObject.toJSONString(request));
        return expressesService.feesPush(request);
    }

    @Operation(summary = "邮政派揽结果推送")
    @PostMapping(path = "open/emsFeePush")
    public BasePushResponse emsFeePush(EmsFeesPush request) throws Exception {
        log.info("emsFeePush receive:{}", JSONObject.toJSONString(request));
        return emsExpressionService.feesPush(request);
    }

    @Operation(summary = "快递取消")
    @PostMapping(path = "{expid}/cancel")
    public BaseResponse<Void> cancelOrder(@PathVariable(name = "expid") String expid) throws Exception {
        return expressesService.cancelOrder(expid);
    }

    @Operation(summary = "快递路由查询")
    @GetMapping(path = "{expid}/searchRoutes")
    public BaseResponse<String> searchRoutes(@PathVariable(name = "expid") String expid) throws Exception {
        return expressesService.searchRoutes(expid);
    }

    @Operation(summary = "快递详情查询")
    @GetMapping(path = "{expid}/detail")
    public BaseResponse<DetailExpress> queryDetailExpress(@PathVariable(name = "expid") String expid) throws Exception {
        return expressesService.queryDetailExpress(expid);
    }


}
