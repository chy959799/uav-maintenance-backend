package com.thinglinks.uav.order.dto.consult;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;

/**
 * @package: com.thinglinks.uav.order.dto
 * @className: OrderConsultRequest
 * @author: rp
 * @description: TODO
 */
@Data
public class OrderConsultRequest {
    @ApiModelProperty(value = "客户id")
    @NotBlank
    private String cid;
    @ApiModelProperty(value = "联系人id")
    @NotBlank
    private String uid;
    @ApiModelProperty(value = "联系电话")
    @NotBlank
    private String telphone;

    @ApiModelProperty(value = "接待人部门id")
    private String recorderDid;
    @ApiModelProperty(value = "接待人id")
    private String recorderUid;

    @ApiModelProperty(value = "咨询工单问题描述")
    @NotBlank
    private String description;
    @ApiModelProperty(value = "咨询时间")
    @NotBlank
    private String consultAt;

    @ApiModelProperty(value = "附件1")
    MultipartFile file0;
    @ApiModelProperty(value = "附件2")
    MultipartFile file1;
    @ApiModelProperty(value = "附件3")
    MultipartFile file2;
    @ApiModelProperty(value = "附件4")
    MultipartFile file3;
    @ApiModelProperty(value = "附件5")
    MultipartFile file4;
    @ApiModelProperty(value = "附件6")
    MultipartFile file5;
}
