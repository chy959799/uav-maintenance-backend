package com.thinglinks.uav.order.dto.consult;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.order.dto
 * @className: OrderConsultPageRequest
 * @author: rp
 * @description: TODO
 */
@Data
public class OrderConsultPageRequest extends BasePageRequest {
    @ApiModelProperty("客户名称")
    String name;
    @ApiModelProperty("工单编号")
    String innerCode;
    @ApiModelProperty("开始时间")
    String startTime;
    @ApiModelProperty("结束时间")
    String endTime;
}

