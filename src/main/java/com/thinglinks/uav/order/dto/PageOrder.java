package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.project.entity.Project;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/25
 */
@Data
public class PageOrder extends Order {

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "项目")
    private Project project;

    @ApiModelProperty(value = "完工时间")
    private String finishTime;

    @ApiModelProperty(value = "收件时间")
    private String receiveTime;

    @ApiModelProperty(value = "客户寄件单号")
    private String customerWaybillNo;

    @ApiModelProperty(value = "客户寄件时间")
    private String customerExpressesTime;

}
