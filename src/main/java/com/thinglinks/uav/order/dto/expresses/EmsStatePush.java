package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.dto.BasePush;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EmsStatePush extends BasePush {

    private String status;

    private String logisticsOrderNo;

    private String downCount;

    private String orgNo;

    private String psegCode;

    private String workerName;

    private String workerPhone;
}
