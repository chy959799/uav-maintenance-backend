package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.FilesRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ReceiveExpress extends FilesRequest {

    @ApiModelProperty(value = "收件备注")
    private String remark;

    @ApiModelProperty(value = "收件时间")
    private Long receiveAt;

    @ApiModelProperty(value = "收件单号")
    private String receiveNo;

    @ApiModelProperty(value = "产品id")
    private String pid;

    @ApiModelProperty(value = "设备编码")
    private String code;

    @ApiModelProperty(value = "物料编码")
    private String materialCode;

    @ApiModelProperty(value = "生产日期，时间戳")
    private Long produceAt;

    @ApiModelProperty(value = "激活日期，时间戳")
    private Long activateAt;

    @ApiModelProperty(value = "电池sn编码")
    private String batteryCode;

    @ApiModelProperty(value = "遥感sn编码")
    private String remoteCode;

    @ApiModelProperty(value = "其他配件")
    private String another;

    @ApiModelProperty(value = "是否二次维修：1:是；0:否")
    private Integer secondary;

    @ApiModelProperty(value = "工单关联编号")
    private String relationOrderCode;
}
