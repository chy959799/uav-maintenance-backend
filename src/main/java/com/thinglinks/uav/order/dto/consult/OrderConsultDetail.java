package com.thinglinks.uav.order.dto.consult;

import com.thinglinks.uav.order.entity.OrderConsult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * @package: com.thinglinks.uav.order.dto
 * @className: OrderConsultDetail
 * @author: rp
 * @description: TODO
 */
@Data
public class OrderConsultDetail extends OrderConsult {

    @ApiModelProperty(value = "工单咨询时间")
    private String consultAt;

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "附件信息")
    private Map<String, String> files;
}
