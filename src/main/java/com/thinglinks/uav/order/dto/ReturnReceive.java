package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReturnReceive {

    @ApiModelProperty(value = "设备编码")
    private String code;

    @ApiModelProperty(value = "电池sn编码")
    private String batteryCode;

    @ApiModelProperty(value = "遥感sn编码")
    private String remoteCode;

}
