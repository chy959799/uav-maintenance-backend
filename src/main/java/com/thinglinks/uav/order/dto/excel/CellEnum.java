package com.thinglinks.uav.order.dto.excel;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/23
 */


public enum CellEnum {

    CUSTOMER_1("客户信息", "省份", "province"),
    CUSTOMER_2("客户信息", "所属单位（市）", "cityCompany"),
    CUSTOMER_3("客户信息", "送修单位（部门\\县区局）", "countyCompany"),
    CUSTOMER_4("客户信息", "联系人", "contacts"),
    CUSTOMER_5("客户信息", "联系电话", "contactsTel"),
    CUSTOMER_6("客户信息", "邮寄地址", "address"),
    DEVICE_1("设备信息", "设备型号", "deviceModel"),
    DEVICE_2("设备信息", "无人机SN码", "deviceCode"),
    DEVICE_3("设备信息", "遥控器编号", "remoteCode"),
    DEVICE_4("设备信息", "电池编号", "batteryCode"),
    DEVICE_5("设备信息", "其他备件", "deviceOther"),
    DEVICE_6("设备信息", "剩余维修金额", "maintainQuota"),
    DEVICE_7("设备信息", "是否有开箱照片", "unpackPicture"),
    DEVICE_8("设备信息", "收件时间", "receiveAt"),
    DEVICE_9("设备信息", "收件单号", "receiveNo"),
    MAINTAIN_1("维保信息", "初步定损时间", "lossAt"),
    MAINTAIN_2("维保信息", "返厂时间", "returnAt"),
    MAINTAIN_3("维保信息", "返厂案例号", "returnCaseNumber"),
    MAINTAIN_4("维保信息", "维修人员", "maintainUser"),
    MAINTAIN_5("维保信息", "维修完成时间", "maintainAt"),
    QUOTE_SETTLE_1("报价结算信息", "是否有报价单", "quotation"),
    QUOTE_SETTLE_2("报价结算信息", "成本价", "costQuote"),
    QUOTE_SETTLE_3("报价结算信息", "客户报价", "customerQuote"),
    QUOTE_SETTLE_4("报价结算信息", "报价单是否已发送", "sendQuote"),
    QUOTE_SETTLE_5("报价结算信息", "项目名称", "projectName"),
    QUOTE_SETTLE_6("报价结算信息", "是否收费", "charged"),
    QUOTE_SETTLE_7("报价结算信息", "二次定损新增费用", "secondLossAmt"),
    QUOTE_SETTLE_8("报价结算信息", "最终应收款", "lastAmt"),
    QUOTE_SETTLE_9("报价结算信息", "是否已到款", "receivedAmt"),
    QUOTE_SETTLE_10("报价结算信息", "到款时间", "paymentAt"),
    QUOTE_SETTLE_11("报价结算信息", "发票日期", "invoiceAt"),
    QUOTE_SETTLE_12("报价结算信息", "发票代码", "invoiceCode"),
    QUOTE_SETTLE_13("报价结算信息", "发票号", "invoiceNo"),
    QUOTE_SETTLE_14("报价结算信息", "开票金额", "invoiceAmt"),
    ADVANCED_PAY_1("预付款", "是否已预付款", "advancedPay"),
    ADVANCED_PAY_2("预付款", "付款对象", "advancePayObject"),
    ADVANCED_PAY_3("预付款", "付款金额", "advancedPayAmt"),
    ADVANCED_PAY_4("预付款", "付款日期", "advancedPayAt"),
    INSURANCE_1("保险信息", "初稿资料是否齐全", "firstMaterialComplete"),
    INSURANCE_2("保险信息", "终稿资料收集时间", "lastMaterialGatherAt"),
    INSURANCE_3("保险信息", "终稿资料是否齐全", "lastMaterialComplete"),
    INSURANCE_4("保险信息", "出险时间", "accidentAt"),
    INSURANCE_5("保险信息", "出险地点", "faultLocation"),
    INSURANCE_6("保险信息", "出险原因", "faultReason"),
    INSURANCE_7("保险信息", "保险单号", "insuranceNo"),
    INSURANCE_8("保险信息", "操作员", "pilotName"),
    INSURANCE_9("保险信息", "报案金额", "reportAmt"),
    INSURANCE_10("保险信息", "是否已报案", "reported"),
    INSURANCE_11("保险信息", "报案时间", "reportAt"),
    INSURANCE_12("保险信息", "报案号", "reportNo"),
    INSURANCE_13("保险信息", "资料是否已存档", "keepMaterial"),
    UAV_CHECK_1("无人机测试", "测试时间", "checkAt"),
    UAV_CHECK_2("无人机测试", "测试人员", "checkUser"),
    UAV_CHECK_3("无人机测试", "是否拉距测试", "checkDraw"),
    UAV_CHECK_4("无人机测试", "测试结果", "checkResult"),
    UAV_EXPRESSES_1("无人机寄出", "是否寄出", "hasExpresses"),
    UAV_EXPRESSES_2("无人机寄出", "寄出时间", "expressesAt"),
    UAV_EXPRESSES_3("无人机寄出", "寄出单号", "expressesNo");


    public static List<ExportCell> getCells() {
        return Arrays.stream(CellEnum.values()).map(x -> {
            ExportCell cell = new ExportCell();
            cell.setName(x.getName());
            cell.setTitle(x.getTitle());
            return cell;
        }).collect(Collectors.toList());
    }

    /**
     * 分类信息大类
     */
    private final String category;

    /**
     * 枚举值
     */
    private final String title;

    /**
     * 枚举编码
     */
    private final String name;

    CellEnum(String category, String title, String name) {
        this.category = category;
        this.title = title;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }
}
