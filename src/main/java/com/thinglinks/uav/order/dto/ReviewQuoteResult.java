package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.ReviewResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/5/28
 */
@Data
public class ReviewQuoteResult extends ReviewResult {

    @ApiModelProperty(value = "是否收费")
    private Boolean beSettle;
}
