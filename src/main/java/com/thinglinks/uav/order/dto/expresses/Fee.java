package com.thinglinks.uav.order.dto.expresses;

import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/8
 */
@Data
public class Fee {
    private String bizOwnerZoneCode;
    private String currencyCode;
    private String customerAcctCode;
    private String feeAmt;
    private String feeAmtInd;
    private Integer feeIndType;
    private String feeTypeCode;
    private String gatherEmpCode;
    private String gatherZoneCode;
    private Integer paymentChangeTypeCode;
    private String paymentTypeCode;
    private String serviceId;
    private String settlementTypeCode;
    private Integer versionNo;
    private String waybillId;
    private String waybillNo;

}
