package com.thinglinks.uav.order.dto.quote;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class QuoteExt {
    @ApiModelProperty(value = "数据库主键,新增的时候忽略")
    private Integer id;

    @ApiModelProperty(value = "工单成本报价id")
    private String oqid;

    @ApiModelProperty(value = "报价顺序,成本报价可能有多次")
    private Integer order;

    @ApiModelProperty(value = "快递费")
    private Double expressesPrice;

    @ApiModelProperty(value = "其他费用")
    private Double otherPrice;

    @ApiModelProperty(value = "其他费用备注")
    private String otherRemark;

    @ApiModelProperty(value = "备件清单")
    private List<QuoteItemExt> quoteItems;
}
