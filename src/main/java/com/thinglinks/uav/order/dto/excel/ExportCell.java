package com.thinglinks.uav.order.dto.excel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/23
 */
@Data
public class ExportCell {

    @ApiModelProperty(value = "字段名称")
    private String name;

    @ApiModelProperty(value = "列头显示")
    private String title;

}
