package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CustomerSettle {

    @ApiModelProperty(value = "数据库主键")
    private Integer id;

    @ApiModelProperty(value = "工程应收款")
    private Double receivable;

    @ApiModelProperty(value = "是否已收款")
    private Boolean received;

    @ApiModelProperty(value = "收款时间")
    private Long receiveAt;

    @ApiModelProperty(value = "开票金额")
    private String invoiceAmount;

    @ApiModelProperty(value = "发票号")
    private String invoiceNo;

    @ApiModelProperty(value = "发票代码")
    private String invoiceCode;

    @ApiModelProperty(value = "发票日期")
    private Long invoiceAt;

    @ApiModelProperty(value = "财务单号")
    private String financeNo;

    @ApiModelProperty(value = "是否已开票")
    private Boolean invoiceOut;
}
