package com.thinglinks.uav.order.dto;

import lombok.Data;

import java.io.File;

@Data
public class InsuranceTemplate {

    private String displayName;

    private File file;
}
