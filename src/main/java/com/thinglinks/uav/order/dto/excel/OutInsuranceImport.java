package com.thinglinks.uav.order.dto.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

@Data
public class OutInsuranceImport {

    @ExcelProperty(value = "送修单位",index = 0)
    @ColumnWidth(20)
    private String customerName;

    @ExcelProperty(value = "联系人",index = 1)
    @ColumnWidth(20)
    private String contact;

    @ExcelProperty(value = "联系电话",index = 2)
    @ColumnWidth(20)
    private String telphone;

    @ExcelProperty(value = "省",index = 3)
    @ColumnWidth(20)
    private String province;

    @ExcelProperty(value = "市",index = 4)
    @ColumnWidth(20)
    private String city;

    @ExcelProperty(value = "区",index = 5)
    @ColumnWidth(20)
    private String county;

    @ExcelProperty(value = "详细地址",index = 6)
    @ColumnWidth(20)
    private String address;

    @ExcelProperty(value = "工单编号",index = 7)
    @ColumnWidth(20)
    private String innerCode;

    @ExcelProperty(value = "产品名称",index = 8)
    @ColumnWidth(20)
    private String productName;

    @ExcelProperty(value = "产品编码",index = 9)
    @ColumnWidth(20)
    private String productCode;

    @ExcelProperty(value = "设备SN码",index = 10)
    @ColumnWidth(20)
    private String deviceCode;

    @ExcelProperty(value = "设备SN码",index = 11)
    @ColumnWidth(20)
    private String batteryCode;

    @ExcelProperty(value = "遥控器SN码",index = 12)
    @ColumnWidth(20)
    private String remoteCode;

    @ExcelProperty(value = "其他配件",index = 13)
    @ColumnWidth(20)
    private String another;

    @ExcelProperty(value = "问题描述",index = 14)
    @ColumnWidth(20)
    private String description;

    @ExcelProperty(value = "错误信息",index = 15)
    @ColumnWidth(20)
    private String message;
}
