package com.thinglinks.uav.order.dto.quote;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class QuoteItemExt extends QuoteItem {

    @ApiModelProperty(value = "仓库平均单价")
    private Double realPrice;

    @ApiModelProperty("产品出厂价格")
    private Double factoryPrice;

    @ApiModelProperty("价格库销售价")
    private Double salePrice;

    @ApiModelProperty(value = "工时")
    private Double workHour;

    @ApiModelProperty(value = "工时费")
    private Double workHourCost;

    @ApiModelProperty(value = "总费用")
    private Double totalCost;


}
