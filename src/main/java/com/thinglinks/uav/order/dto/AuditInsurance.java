package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/6
 */
@Data
public class AuditInsurance {

    @ApiModelProperty(value = "保险类型：保险类型 1:DJI Care 2:DJI Care续享 3:行业无忧基础版 4:行业无忧基础续享版 5:行业无忧旗舰版 6:行业无忧旗舰续享版 7:英大财险 8:鼎和财险")
    private String insuranceType;

    @ApiModelProperty(value = "工单服务类型 1:保内维修 2:框架合同维修；3:快修快换；4:报废 ")
    @NotBlank
    private String serviceType;

    @ApiModelProperty(value = "项目id ")
    @NotBlank
    private String pjid;

    @ApiModelProperty(value = "保险审核备注")
    private String reviewInsuranceRemark;

    @ApiModelProperty(value = "保单编号")
    private String insuranceCode;

    @ApiModelProperty(value = "保险注册号")
    private String registrationNumber;

    @ApiModelProperty(value = "保险开始时间, 时间戳")
    private Long insuranceStartAt;

    @ApiModelProperty(value = "保险结束时间，时间戳")
    private Long insuranceExpireAt;

    @ApiModelProperty(value = "保险总承保金额")
    private Double insuranceTotalQuota;

    @ApiModelProperty(value = "累计使用保障额度")
    private Double insuranceUseQuota;

    @ApiModelProperty(value = "剩余额度")
    private Double quota;

    @ApiModelProperty(value = "剩余换新次数")
    private Integer exchangeCount;


}
