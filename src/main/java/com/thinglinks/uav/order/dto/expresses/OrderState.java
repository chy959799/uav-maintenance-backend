package com.thinglinks.uav.order.dto.expresses;

import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/8
 */
@Data
public class OrderState {
    private String orderNo;
    private String waybillNo;
    private String orderStateCode;
    private String orderStateDesc;
    private String empCode;
    private String empPhone;
    private String netCode;
    private String lastTime;
    private String bookTime;
    private String carrierCode;
    private String createTm;

}
