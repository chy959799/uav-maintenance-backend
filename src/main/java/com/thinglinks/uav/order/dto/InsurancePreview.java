package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/5/5
 */
@Data
public class InsurancePreview extends FirstInsurance {

    @ApiModelProperty(value = "保单编号")
    private String insuranceCode;

    @ApiModelProperty(value = "剩余额度")
    private Double quota;

    @ApiModelProperty(value = "产品id")
    private String pid;

    @ApiModelProperty(value = "设备编码")
    private String code;

    @ApiModelProperty(value = "维修点ID")
    private String cpid;

}
