package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.FilesRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MaintainFeedback extends FilesRequest {

    @ApiModelProperty(value = "维修情况反馈")
    private String feedback;


}
