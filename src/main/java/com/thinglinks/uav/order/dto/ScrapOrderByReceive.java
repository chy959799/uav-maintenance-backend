package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ScrapOrderByReceive {

    @ApiModelProperty(value = "送修单位")
    @NotBlank
    private String cid;

    @ApiModelProperty(value = "省编码")
    @NotBlank
    private String province;

    @ApiModelProperty(value = "城市编码")
    @NotBlank
    private String city;

    @ApiModelProperty(value = "区县编码")
    @NotBlank
    private String county;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "联系人名称")
    @NotBlank
    private String contact;

    @ApiModelProperty(value = "联系人电话")
    @NotBlank
    private String telphone;

    @ApiModelProperty(value = "工单服务类型 1:维修 2:保养")
    @NotBlank
    private String type;

    @ApiModelProperty(value = "工单服务类型 1:保内维修 2:框架合同维修；3:快修快换；4:报废")
    @NotBlank
    private String serviceType;

    @ApiModelProperty(value = "1:不紧急 2:一般 3: 紧急 4: 非常紧急")//默认不紧急
    @NotBlank
    private String level = "1";

    @ApiModelProperty(value = "产品id")
    @NotBlank
    private String pid;

    @ApiModelProperty(value = "设备编码")
    @NotBlank
    private String code;

    @ApiModelProperty(value = "生产日期，时间戳")
    private Long produceAt;

    @ApiModelProperty(value = "激活日期，时间戳")
    private Long activateAt;

    @ApiModelProperty(value = "工单问题描述")
    private String description;

}
