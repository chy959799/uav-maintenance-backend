package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/6
 */
@Data
public class ConfirmLoss {

    @ApiModelProperty(value = "项目id ")
    @NotBlank
    private String pjid;

    @ApiModelProperty(value = "工单服务方式 维修-1:本地维修 2:返厂维修 3:质保换新 保养-1:本地保养")
    @NotBlank
    private String serviceMethod;

    @ApiModelProperty(value = "1:不紧急 2:一般 3: 紧急 4: 非常紧急")
    @NotBlank
    private String level;

    @ApiModelProperty(value = "备注")
    private String remark;

}
