package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderFile {

    @ApiModelProperty(value = "文件名称")
    private String name;

    @ApiModelProperty(value = "更新时间")
    private String updateTime;

    @ApiModelProperty(value = "文件地址")
    private String url;

    @ApiModelProperty(value = "文件类型：1：工单报价文件；2：工单二维码；3：保单编码截图；4：飞手证书；5：固定资产卡片；6：事故照片-事故现场；7：事故照片-损坏物品清单；8：事故照片-损坏设备；9：事故照片-设备正面；10：事故照片-设备反面；11：事故照片-其他；12：巡视工作单；13：情况说明；14：终版赔付资料；15：赔付意向书；16：电力业务出现通知及索赔申请书；17：损失清单；18：无人机故障报告；19：事故照片；20：案件报告；21；案件文件；22：客户报价单；23：收费通知单;24:系统报价单")
    private String type;

    @ApiModelProperty(value = "是否可以下载")
    private Boolean download;
}
