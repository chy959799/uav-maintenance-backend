package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.dto.BasePush;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EmsFeesPush extends BasePush {

    private String logisticsOrderNo;

    private String waybillNo;

    private String status;

    private String finalStates;

    private String remarks;

    private String desc;

    private Double postageTotal;

    private String pickupErrorCode;

    private String returnAdditionReason;

    private Double insuranceAmount;

    private Double weight;
}
