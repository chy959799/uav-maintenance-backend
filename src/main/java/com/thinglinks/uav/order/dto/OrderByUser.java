package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/15
 */
@Data
public class OrderByUser extends FirstInsurance{

    @ApiModelProperty(value = "工单服务类型 1:维修 2:保养")
    @NotBlank
    private String type;

    @ApiModelProperty(value = "工单服务类型 1:保内维修 2:框架合同维修；3:快修快换；4:报废")
    @NotBlank
    private String serviceType;

    @ApiModelProperty(value = "是否二次维修：1:是；0:否")
    @NotNull
    private Integer secondary;

    @ApiModelProperty(value = "设备编码")
    @NotBlank
    private String code;

    @ApiModelProperty(value = "产品id")
    @NotBlank
    private String pid;

    @ApiModelProperty(value = "生产日期，时间戳")
    private Long produceAt;

    @ApiModelProperty(value = "激活日期，时间戳")
    private Long activateAt;

    @ApiModelProperty(value = "电池sn编码")
    private String batteryCode;

    @ApiModelProperty(value = "遥感sn编码")
    private String remoteCode;

    @ApiModelProperty(value = "其他配件")
    private String another;

    @ApiModelProperty(value = "工单问题描述")
    @NotBlank
    private String description;

    @ApiModelProperty(value = "工单附件")
    private List<String> orderAttachment;

    @ApiModelProperty(value = "保险类型：保险类型 1:DJI Care 2:DJI Care续享 3:行业无忧基础版 4:行业无忧基础续享版 5:行业无忧旗舰版 6:行业无忧旗舰续享版 7:英大财险 8:鼎和财险")
    private String insuranceType;

    @ApiModelProperty(value = "是否手工录入")
    private Boolean manual;

    @ApiModelProperty(value = "保单编号")
    private String insuranceCode;

    @ApiModelProperty(value = "保险注册号")
    private String registrationNumber;

    @ApiModelProperty(value = "剩余额度")
    private Double quota;

    @ApiModelProperty(value = "剩余换新次数")
    private Integer exchangeCount;

    @ApiModelProperty(value = "保险开始时间, 时间戳")
    private Long insuranceStartAt;

    @ApiModelProperty(value = "保险结束时间，时间戳")
    private Long insuranceExpireAt;

    @ApiModelProperty(value = "保单号截图")
    private String insuranceScreenshot;

    @ApiModelProperty(value = "省编码")
    @NotBlank
    private String province;

    @ApiModelProperty(value = "城市编码")
    @NotBlank
    private String city;

    @ApiModelProperty(value = "区县编码")
    @NotBlank
    private String county;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "联系人名称")
    @NotBlank
    private String contact;

    @ApiModelProperty(value = "联系人电话")
    @NotBlank
    private String telphone;

    @ApiModelProperty(value = "收件单号")
    private String receiveNo;

    @ApiModelProperty(value = "1:不紧急 2:一般 3: 紧急 4: 非常紧急")
    @NotBlank
    //默认不紧急
    private String level = "1";

    @ApiModelProperty(value = "维修点")
    @NotBlank
    private String cpid;
}
