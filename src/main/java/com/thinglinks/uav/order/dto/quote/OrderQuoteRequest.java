package com.thinglinks.uav.order.dto.quote;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrderQuoteRequest {

    @ApiModelProperty(value = "故障类型集合")
    private List<String> ordcatids;

    @ApiModelProperty(value = "文件上传后的fid")
    private List<String> fids;

    @ApiModelProperty(value = "成本报价单")
    private Quote costQuotes;

    @ApiModelProperty(value = "客户报价单")
    private Quote customerQuote;
}
