package com.thinglinks.uav.order.dto.quote;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class QuoteSpare {

    @ApiModelProperty(value = "报价顺序,成本报价可能有多次")
    private Integer order;

    @ApiModelProperty(value = "备件总数量")
    private Integer totalCount;

    @ApiModelProperty(value = "工单成本报价id")
    private String oqid;

    @ApiModelProperty(value = "是否出库")
    private Boolean stockOut;

    @ApiModelProperty(value = "实际出库总量")
    private Integer stockOutCount;

    @ApiModelProperty(value = "备件明细")
    private List<SpareItem> items;
}
