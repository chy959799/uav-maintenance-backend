package com.thinglinks.uav.order.dto.expresses;

import lombok.Data;

@Data
public class PrintWaybills {

    private String logisticID;

    private String requestID;

    private String serviceCode;

    private Long timestamp;

    private String msgDigest;

    private String msgData;

    private String nonce;
}
