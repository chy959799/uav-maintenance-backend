package com.thinglinks.uav.order.dto.quote;

import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SpareItem {

    @ApiModelProperty(value = "产品id")
    private String pid;

    @ApiModelProperty(value = "备件数量（定损数量）")
    private Integer count;

    @ApiModelProperty(value = "实际出库数量")
    private Integer stockOutCount;

    @ApiModelProperty(value = "产品")
    private Product product;
}
