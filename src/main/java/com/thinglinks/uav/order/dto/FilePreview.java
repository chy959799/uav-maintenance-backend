package com.thinglinks.uav.order.dto;

import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/5/5
 */
@Data
public class FilePreview {

    private String fileName;

    private String fileUrl;
}
