package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.TimePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Data
public class PageOrderQuery extends TimePageRequest {

    @ApiModelProperty(value = "工单编号")
    private String innerCode;

    @ApiModelProperty(value = "工单状态: 0000:已取消;0001: 已作废;0002:用户发起取消待审核-操作人员审核;0003:撤销取消待审核-保险人员:1000:待收件;1001:待设备确认;1002:保险审核;1003:理赔待确认-鼎和保险;2000:待定损;2001:定损审核;2002:返厂待寄件;3000:待报价;3001:待报价审核;3002:报价待确认;3003:保险资料收集;4000:待结算;4001:返厂待收件;4002:待派单;4003:维修中;5000:待检测;5001:检测中;5002:待寄件;6000:已完成")
    private String status;

    @ApiModelProperty(value = "工单服务类型 1:保内维修 2:保外维修")
    private String serviceType;

    @ApiModelProperty(value = "工单服务方式 维修-1:本地维修 2:返厂维修 3:质保换新 保养-1:本地保养")
    private String serviceMethod;

    @ApiModelProperty(value = "保险资料审核状态；00：保险信息待确认；10：出版资料待提交；11：初版资料待审核；12：出版资料已完成;13：出版资料已驳回；20：终版资料待提交；21：终版资料待审核；22理赔待完成；23：终版资料待驳回；30：已完成")
    private String reviewInsuranceStatus;

    @ApiModelProperty(value = "单位名称")
    private String cname;

    @ApiModelProperty(value = "联系人")
    private String contact;

    @ApiModelProperty(value = "产品类型")
    private String catid;

    @ApiModelProperty(value = "设备sn编码")
    private String code;

    @ApiModelProperty(value = "遥感sn编码")
    private String remoteCode;

    @ApiModelProperty(value = "电池sn编码")
    private String batteryCode;

    @ApiModelProperty(value = "维修人员")
    private String maintainerName;

    @ApiModelProperty(value = "返厂案例号")
    private String returnCaseNumber;

    @ApiModelProperty(value = "收件单号")
    private String receiveNo;

    @ApiModelProperty(value = "项目id")
    private String pjid;

    @ApiModelProperty(value = "工单取消前的状态")
    private String preStatus;
}
