package com.thinglinks.uav.order.dto.quote;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class Quote {

    @ApiModelProperty(value = "数据库主键,新增的时候忽略")
    private Integer id;

    @ApiModelProperty(value = "快递费")
    private Double expressesPrice;

    @ApiModelProperty(value = "其他费用")
    private Double otherPrice;

    @ApiModelProperty(value = "其他费用备注")
    private String otherRemark;

    @ApiModelProperty(value = "备件清单")
    private List<QuoteItem> quoteItems;

}
