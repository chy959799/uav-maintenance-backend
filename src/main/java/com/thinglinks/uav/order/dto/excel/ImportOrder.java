package com.thinglinks.uav.order.dto.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ImportOrder {

    @ExcelProperty("送修单位")
    private String customerName;

    @ExcelProperty("联系人")
    private String contact;

    @ExcelProperty("联系电话")
    private String telphone;

    @ExcelProperty("省")
    private String province;

    @ExcelProperty("市")
    private String city;

    @ExcelProperty("区")
    private String county;

    @ExcelProperty("详细地址")
    private String address;

    @ExcelProperty("工单编号")
    private String innerCode;

    @ExcelProperty("关联工单编号")
    private String relationInnerCode;

    @ExcelProperty( "工单类型")
    private String serviceType;

    @ExcelProperty("问题描述")
    private String description;

    @ExcelProperty("产品名称")
    private String productName;

    @ExcelProperty("产品编码")
    private String productCode;

    @ExcelProperty("设备SN码")
    private String deviceCode;

    @ExcelProperty("设备SN码")
    private String batteryCode;

    @ExcelProperty("遥控器SN码")
    private String remoteCode;

    @ExcelProperty("其他配件")
    private String another;

    @ExcelProperty("保险类型")
    private String insuranceType;

    @ExcelProperty("保险有效日期")
    private Long insuranceStartAt;

    @ExcelProperty("保险到期日期")
    private Long insuranceExpireAt;

    @ExcelProperty("剩余保障额度")
    private Double quota;

    @ExcelProperty("剩余换新次数")
    private Integer exchangeCount;

    @ExcelProperty( "保险注册号")
    private String registrationNumber;

    @ExcelProperty("错误信息")
    private String message;
}
