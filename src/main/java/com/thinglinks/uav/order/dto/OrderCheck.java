package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.FilesRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Data
public class OrderCheck extends FilesRequest {

    @ApiModelProperty(value = "审核结果：passed：通过；rejected：驳回")
    @NotBlank
    private String reviewResult;

    @ApiModelProperty(value = "是否拉距测试")
    private Boolean checkDraw;

    @ApiModelProperty(value = "驳回时候备注")
    private String remark;
}
