package com.thinglinks.uav.order.dto.quote;

import com.thinglinks.uav.spare.entity.ProjectProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class QuoteItem {

    @ApiModelProperty(value = "数据库主键,新增的时候忽略")
    private Integer id;

    @ApiModelProperty(value = "产品id")
    @NotBlank
    private String pid;

    @ApiModelProperty(value = "备件数量")
    @NotNull
    private Integer count;

    private ProjectProduct product;

}
