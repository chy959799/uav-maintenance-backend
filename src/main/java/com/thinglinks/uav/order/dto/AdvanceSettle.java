package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdvanceSettle {

    @ApiModelProperty(value = "数据库主键")
    private Integer id;

    @ApiModelProperty(value = "是否预付款")
    private Boolean advanced;

    @ApiModelProperty(value = "付款对象")
    private String advanceObject;

    @ApiModelProperty(value = "付款金额")
    private String advanceAmount;

    @ApiModelProperty(value = "付款时间")
    private Long advanceAt;

    @ApiModelProperty(value = "财务单号")
    private String financeNo;
}
