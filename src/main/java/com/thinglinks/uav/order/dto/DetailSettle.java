package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DetailSettle {

    @ApiModelProperty(value = "返厂预付款")
    private List<AdvanceSettle> advanceSettle;

    @ApiModelProperty(value = "客户结算")
    private CustomerSettle customerSettle;

    @ApiModelProperty(value = "已提交结算次数")
    private Integer settleCount;
}
