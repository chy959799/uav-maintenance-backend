package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/27
 */
@Data
public class ExpressesAddress {

    @ApiModelProperty(value = "发货人或者收货人")
    @NotBlank
    private String contact;

    @ApiModelProperty(value = "联系电话")
    @NotBlank
    private String telphone;

    @ApiModelProperty(value = "省")
    @NotBlank
    private String province;

    @ApiModelProperty(value = "城市")
    @NotBlank
    private String city;

    @ApiModelProperty(value = "区县")
    @NotBlank
    private String county;

    @ApiModelProperty(value = "详细地址")
    @NotBlank
    private String address;

}
