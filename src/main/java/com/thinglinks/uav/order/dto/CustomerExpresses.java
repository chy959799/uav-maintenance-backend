package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CustomerExpresses {

    @ApiModelProperty(value = "邮寄方式：1：快递自寄；2：上门取件；3：客户自提")
    @NotBlank
    private String type;

    @ApiModelProperty(value = "快递自寄的单号")
    private String expressNo;

    @ApiModelProperty(value = "快递自寄的时间，时间戳")
    private Long expressTime;

    @ApiModelProperty("上门取件的寄送地址")
    private ExpressesAddress sender;

    @ApiModelProperty("上门取件的接收地址")
    private ExpressesAddress receiver;

}
