package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.dto.BasePush;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/8
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StatePush extends BasePush {

    private String requestId;

    private String timestamp;

    private List<OrderState> orderState;
}
