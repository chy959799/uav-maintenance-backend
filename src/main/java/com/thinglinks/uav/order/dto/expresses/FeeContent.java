package com.thinglinks.uav.order.dto.expresses;

import lombok.Data;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/8
 */
@Data
public class FeeContent {

    private String waybillNo;
    private String customerAcctCode;
    private String childNos;
    private String orderNo;
    private String meterageWeightQty;
    private String volume;
    private String quantity;
    private List<Fee> feeList;
    private String productName;

}
