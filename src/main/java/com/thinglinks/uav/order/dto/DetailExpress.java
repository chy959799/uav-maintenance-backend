package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.order.entity.Expresses;
import com.thinglinks.uav.user.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DetailExpress extends Expresses {

    @ApiModelProperty(value = "快递收件或者快递寄件时间")
    private String expressesTime;

    @ApiModelProperty("上门取件的寄送地址")
    private Address sender;

    @ApiModelProperty("上门取件的接收地址")
    private Address receiver;

    @ApiModelProperty("创建人信息")
    private User user;

    @ApiModelProperty("云打印面单地址")
    private String cloudPrintUrl;

}
