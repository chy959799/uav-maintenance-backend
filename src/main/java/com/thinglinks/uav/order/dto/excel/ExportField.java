package com.thinglinks.uav.order.dto.excel;

import lombok.Data;

import java.util.List;

@Data
public class ExportField {

    private List<ExportCell> fields;

}
