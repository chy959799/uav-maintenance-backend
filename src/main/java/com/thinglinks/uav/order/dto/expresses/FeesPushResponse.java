package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.dto.BasePushResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/8
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class FeesPushResponse extends BasePushResponse {

    private Integer code;

    private String partnerCode;

    private String service;

    private String msgData;

    public static FeesPushResponse success(String partnerCode) {
        FeesPushResponse response = new FeesPushResponse();
        response.setCode(HttpStatus.SC_OK);
        response.setPartnerCode(partnerCode);
        response.setService(StringUtils.EMPTY);
        response.setMsgData(StringUtils.EMPTY);
        return response;
    }

    public static FeesPushResponse fail(String partnerCode) {
        FeesPushResponse response = new FeesPushResponse();
        response.setCode(HttpStatus.SC_BAD_REQUEST);
        response.setPartnerCode(partnerCode);
        response.setService(StringUtils.EMPTY);
        response.setMsgData(StringUtils.EMPTY);
        return response;
    }
}
