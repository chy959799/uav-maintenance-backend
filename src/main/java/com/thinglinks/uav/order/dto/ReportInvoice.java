package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReportInvoice {

    @ApiModelProperty(value = "报案号")
    private String reportNo;

    @ApiModelProperty(value = "报案金额")
    private Double reportAmt;

    @ApiModelProperty(value = "报案时间")
    private Long reportTime;

    @ApiModelProperty(value = "开票金额")
    private String invoiceAmount;

    @ApiModelProperty(value = "发票号")
    private String invoiceNo;

    @ApiModelProperty(value = "发票代码")
    private String invoiceCode;

    @ApiModelProperty(value = "发票日期")
    private Long invoiceAt;
}
