package com.thinglinks.uav.order.dto.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

@Data
public class SecondaryOrderImport {

    @ExcelProperty(value = "送修单位",index = 0)
    @ColumnWidth(20)
    private String customerName;

    @ExcelProperty(value = "联系人",index = 1)
    @ColumnWidth(20)
    private String contact;

    @ExcelProperty(value = "联系电话",index = 2)
    @ColumnWidth(20)
    private String telphone;

    @ExcelProperty(value = "省",index = 3)
    @ColumnWidth(20)
    private String province;

    @ExcelProperty(value = "市",index = 4)
    @ColumnWidth(20)
    private String city;

    @ExcelProperty(value = "区",index = 5)
    @ColumnWidth(20)
    private String county;

    @ExcelProperty(value = "详细地址",index = 6)
    @ColumnWidth(20)
    private String address;

    @ExcelProperty(value = "工单编号",index = 7)
    @ColumnWidth(20)
    private String innerCode;

    @ExcelProperty( value = "工单类型",index = 8)
    @ColumnWidth(20)
    private String serviceType;

    @ExcelProperty(value = "关联工单编号",index = 9)
    @ColumnWidth(20)
    private String relationInnerCode;

    @ExcelProperty(value = "问题描述",index = 10)
    @ColumnWidth(20)
    private String description;

    @ExcelProperty(value = "错误信息",index = 11)
    @ColumnWidth(20)
    private String message;
}
