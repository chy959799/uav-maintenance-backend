package com.thinglinks.uav.order.dto.quote;

import com.thinglinks.uav.order.entity.OrderCategory;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DetailOrderQuote {

    @ApiModelProperty(value = "故障图片链接")
    private List<String> ordcatUrls;

    @ApiModelProperty(value = "故障集合")
    private List<OrderCategory> orderCategories;

    @ApiModelProperty(value = "故障类型集合")
    private List<String> ordcatids;

    @ApiModelProperty(value = "文件上传后的fid")
    private List<String> fids;

    @ApiModelProperty(value = "成本报价单")
    private List<QuoteExt> costQuotes;

    @ApiModelProperty(value = "客户报价单")
    private QuoteExt customerQuote;

    @ApiModelProperty(value = "已报价审核通过次数")
    private Integer quotedCount;

}
