package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.constants.ExpressionConstants;
import com.thinglinks.uav.common.dto.BasePushResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

/**
 * @author iwiFool
 * @date 2024/4/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class EmsPushResponse extends BasePushResponse {

	private String serialNo;

	private String code;

	private String codeMessage;

	private String senderNo;

	public static EmsPushResponse success(String senderNo) {
		EmsPushResponse response = new EmsPushResponse();
		response.setSerialNo(UUID.randomUUID().toString());
		response.setCode(ExpressionConstants.SUCCESS_CODE);
		response.setSenderNo(senderNo);
		return response;
	}

	public static EmsPushResponse fail(String senderNo, String codeMessage) {
		EmsPushResponse response = new EmsPushResponse();
		response.setSerialNo(UUID.randomUUID().toString());
		response.setCode(ExpressionConstants.FAIL_CODE);
		response.setCodeMessage(codeMessage);
		response.setSenderNo(senderNo);
		return response;
	}
}
