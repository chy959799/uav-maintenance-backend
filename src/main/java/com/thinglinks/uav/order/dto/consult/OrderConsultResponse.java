package com.thinglinks.uav.order.dto.consult;

import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.order.entity.OrderConsult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @package: com.thinglinks.uav.order.dto
 * @className: OrderConsultResponse
 * @author: rp
 * @description: TODO
 */
@Data
public class OrderConsultResponse extends OrderConsult {
    @ApiModelProperty(value =  "附件")
    private List<CustomFile> flies;
}
