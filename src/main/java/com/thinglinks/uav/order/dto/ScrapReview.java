package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.ReviewResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ScrapReview extends ReviewResult {

    @ApiModelProperty(value = "项目id")
    private String pjid;
}
