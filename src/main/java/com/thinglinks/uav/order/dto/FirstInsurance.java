package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class    FirstInsurance {

    @ApiModelProperty(value = "出险人")
    private String accidentUser;

    @ApiModelProperty(value = "出险人")
    private String accidentTel;

    @ApiModelProperty(value = "出险人单位")
    private String cid;

    @ApiModelProperty(value = "报案号")
    private String reportNo;

    @ApiModelProperty(value = "故障类别：1:坠机；2:碰撞")
    private String faultType;

    @ApiModelProperty(value = "故障发生时间")
    private Long faultAt;

    @ApiModelProperty(value = "故障地点")
    private String faultLocation;

    @ApiModelProperty(value = "故障原因")
    private String faultReason;

    @ApiModelProperty(value = "故障处置经过")
    private String faultProcess;

    @ApiModelProperty(value = "工作类型")
    private String workType;

    @ApiModelProperty(value = "受损情况")
    private String damaged;

    @ApiModelProperty(value = "飞手名字")
    private String pilotName;

    @ApiModelProperty(value = "证件类型：0:无,1:CAAC;2:AOPA;3:UTC;4:内部合格证书")
    private String idType;

    @ApiModelProperty(value = "证件生效时间")
    private Long effectiveStartAt;

    @ApiModelProperty(value = "证件失效时间")
    private Long effectiveEndAt;

    @ApiModelProperty(value = "飞手证书")
    private String pilotCertificate;

    @ApiModelProperty(value = "固定资产卡片")
    private String fixedAsset;

    @ApiModelProperty(value = "事故照片-事故现场")
    private List<String> accidentScene;

    @ApiModelProperty(value = "事故照片-损坏物品清单")
    private List<String> accidentDamageItems;

    @ApiModelProperty(value = "事故照片-损坏设备")
    private List<String> accidentDamageDevices;

    @ApiModelProperty(value = "事故照片-设备正面")
    private List<String> deviceFront;

    @ApiModelProperty(value = "事故照片-设备反面")
    private List<String> deviceReverse;

    @ApiModelProperty(value = "事故照片-其他")
    private List<String> accidentOther;

    @ApiModelProperty(value = "巡险工作单")
    private String lineWalkingPicture;

    @ApiModelProperty(value = "情况说明")
    private String presentation;

    @ApiModelProperty(value = "是否已开票")
    private Boolean invoiceOut;

}
