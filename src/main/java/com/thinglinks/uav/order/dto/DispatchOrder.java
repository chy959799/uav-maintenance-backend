package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/27
 */
@Data
public class DispatchOrder {

    @ApiModelProperty(value = "维修工程师")
    private String maintainerUid;
}
