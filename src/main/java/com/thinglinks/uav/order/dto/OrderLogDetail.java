package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.order.entity.OrderLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class OrderLogDetail extends OrderLog {


    @ApiModelProperty(value = "工单操作时间")
    private String operateTime;

    @ApiModelProperty(value = "附件的连接地址")
    private List<String> files;

}
