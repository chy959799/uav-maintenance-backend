package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderSparePrice {

    @ApiModelProperty(value = "仓库平均单价")
    private Double realPrice;

    @ApiModelProperty("产品出厂价格")
    private Double factoryPrice;

    @ApiModelProperty("价格库销售价")
    private Double salePrice;

    @ApiModelProperty("备件工时")
    private Double workHour;

    @ApiModelProperty("备件单位工时费")
    private Double workHourUnitCost;

    @ApiModelProperty("备件工时费")
    private Double workHourCost;

    @ApiModelProperty("产品id")
    private String pid;

    @ApiModelProperty("产品id")
    private Product product;

}
