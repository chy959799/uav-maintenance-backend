package com.thinglinks.uav.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/6
 */
@Data
public class OrderFault {

    @ApiModelProperty(value = "故障类型")
    @NotBlank
    private String catid;

    @ApiModelProperty(value = "故障解决方案", example = "故障解决方案")
    @NotBlank
    private String solution;
}
