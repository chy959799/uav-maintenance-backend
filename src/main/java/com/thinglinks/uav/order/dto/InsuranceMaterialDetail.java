package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.common.dto.FileVO;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.order.entity.InsuranceMaterial;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class InsuranceMaterialDetail extends InsuranceMaterial {

    @ApiModelProperty(value = "保险文件")
    private List<FileVO> InsuranceFiles;

    @ApiModelProperty(value = "证件生效时间-字符串")
    private String effectiveStartTime;

    @ApiModelProperty(value = "证件失效时间-字符串")
    private String effectiveEndTime;

    @ApiModelProperty(value = "飞手证书")
    private FileVO pilotCertificate;

    @ApiModelProperty(value = "固定资产卡片")
    private FileVO fixedAsset;

    @ApiModelProperty(value = "事故照片-事故现场")
    private List<FileVO> accidentScene;

    @ApiModelProperty(value = "事故照片-损坏物品清单")
    private List<FileVO> accidentDamageItems;

    @ApiModelProperty(value = "事故照片-损坏设备")
    private List<FileVO> accidentDamageDevices;

    @ApiModelProperty(value = "事故照片-设备正面")
    private List<FileVO> deviceFront;

    @ApiModelProperty(value = "事故照片-设备反面")
    private List<FileVO> deviceReverse;

    @ApiModelProperty(value = "事故照片-其他")
    private List<FileVO> accidentOther;

    @ApiModelProperty(value = "巡险工作单")
    private FileVO lineWalkingPicture;

    @ApiModelProperty(value = "情况说明")
    private FileVO presentation;

    @ApiModelProperty(value = "出险人单位")
    private Customer accidentCustomer;

    @ApiModelProperty(value = "故障发生时间")
    private String faultTime;

    @ApiModelProperty(value = "保单号截图")
    private FileVO insuranceScreenshot;
}
