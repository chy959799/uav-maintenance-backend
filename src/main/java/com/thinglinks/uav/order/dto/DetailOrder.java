package com.thinglinks.uav.order.dto;

import com.thinglinks.uav.order.entity.OrderCategory;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/27
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DetailOrder extends PageOrder {

    @ApiModelProperty(value = "工单二维码")
    private String qrCodeUrl;

    @ApiModelProperty(value = "客户结算是否可以编辑")
    private Boolean settleCanEdit;

    @ApiModelProperty(value = "工单附件")
    private List<String> orderAttachments;

    @ApiModelProperty(value = "收件附件")
    private List<String> receiveAttachments;

    @ApiModelProperty(value = "定损附件")
    private List<String> lossAttachments;

    @ApiModelProperty(value = "检查附件")
    private List<String> checkAttachments;

    @ApiModelProperty(value = "取消时间")
    private String cancelTime;

    @ApiModelProperty(value = "收件时间")
    private String receiveTime;

    @ApiModelProperty(value = "首次定损时间")
    private String firstQuoteTime;

    @ApiModelProperty(value = "是否二次定损")
    private Boolean secondaryQuote;

    @ApiModelProperty(value = "返厂寄件方式 1:快递自寄 2:快递上门")
    private String returnPickup;

    @ApiModelProperty(value = "返厂寄件单号")
    private String returnWaybillNo;

    @ApiModelProperty(value = "返厂寄件时间")
    private String returnExpressesTime;

    @ApiModelProperty(value = "客户报价")
    private Double customerQuote;

    @ApiModelProperty(value = "是否结算完成")
    private Boolean settled;

    @ApiModelProperty(value = "返厂收件时间")
    private String returnTime;

    @ApiModelProperty(value = "维修开始时间")
    private String maintainStartTime;

    @ApiModelProperty(value = "维修结束时间")
    private String maintainEndTime;

    @ApiModelProperty(value = "检查开始时间")
    private String checkStartTime;

    @ApiModelProperty(value = "检查结束时间")
    private String checkEndTime;

    @ApiModelProperty(value = "客户寄件方式 1:快递自寄 2:快递上门")
    private String customerPickup;

    @ApiModelProperty(value = "客户寄件单号")
    private String customerWaybillNo;

    @ApiModelProperty(value = "客户寄件时间")
    private String customerExpressesTime;

    @ApiModelProperty(value = "pass 通过 reject 驳回")
    private String checkResult;

    @ApiModelProperty(value = "故障集合")
    private List<OrderCategory> orderCategories;

    @ApiModelProperty(value = "故障类型集合")
    private List<String> ordCatNames;

    @ApiModelProperty(value = "保险文件是否归档")
    private Boolean insuranceArchived;

}
