package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.dto.BasePushResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

@EqualsAndHashCode(callSuper = true)
@Data
public class PushResponse extends BasePushResponse {

    private String success;

    private String msg;

    public static PushResponse success() {
        PushResponse response = new PushResponse();
        response.setSuccess(String.valueOf(Boolean.TRUE));
        response.setMsg(StringUtils.EMPTY);
        return response;
    }


    public static PushResponse fail() {
        PushResponse response = new PushResponse();
        response.setSuccess(String.valueOf(Boolean.FALSE));
        response.setMsg(StringUtils.EMPTY);
        return response;
    }
}
