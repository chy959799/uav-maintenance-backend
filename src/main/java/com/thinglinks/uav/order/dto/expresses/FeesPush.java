package com.thinglinks.uav.order.dto.expresses;

import com.thinglinks.uav.common.dto.BasePush;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/8
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class FeesPush extends BasePush {

    private String sign;

    private String content;

}
