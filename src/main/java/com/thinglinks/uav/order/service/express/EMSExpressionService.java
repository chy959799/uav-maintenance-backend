package com.thinglinks.uav.order.service.express;

import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.common.dto.BasePush;
import com.thinglinks.uav.common.dto.BasePushResponse;
import com.thinglinks.uav.order.entity.Expresses;
import com.thinglinks.uav.system.exception.CustomBizException;

/**
 * @author iwiFool
 * @date 2024/4/26
 */
public interface EMSExpressionService {

	Expresses createOrder(String orderId, Address sendAddress, Address receiveAddress) throws Exception;

	void cancelOrder(Expresses expresses) throws Exception;

	String searchRoutes(String waybillNo) throws CustomBizException;

	BasePushResponse statePush(BasePush request);

	BasePushResponse feesPush(BasePush request);

	Integer getExpressType();

}
