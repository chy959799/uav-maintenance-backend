package com.thinglinks.uav.order.service.express;

import com.alibaba.fastjson.JSONObject;
import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.dto.BasePush;
import com.thinglinks.uav.common.dto.BasePushResponse;
import com.thinglinks.uav.common.entity.City;
import com.thinglinks.uav.common.entity.County;
import com.thinglinks.uav.common.entity.Province;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.repository.CityRepository;
import com.thinglinks.uav.common.repository.CountyRepository;
import com.thinglinks.uav.common.repository.ProvinceRepository;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.EMSExpressUtils;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.order.dto.expresses.EmsFeesPush;
import com.thinglinks.uav.order.dto.expresses.EmsPushResponse;
import com.thinglinks.uav.order.dto.expresses.EmsStatePush;
import com.thinglinks.uav.order.entity.Expresses;
import com.thinglinks.uav.order.repository.ExpressesRepository;
import com.thinglinks.uav.system.exception.CustomBizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Objects;

/**
 *
 * @author iwiFool
 * @date 2024/4/26
 */
@Service
@Slf4j
public class EMSExpressServiceImpl implements EMSExpressionService {

	private static final String FILE_TYPE = "pdf";

	private static final String FINISH_CODE = "1";

	private static final String CANCEL_CODE = "0";
	private static final Integer EXPRESS_TYPE = 1;

	@Resource
	private EMSExpressUtils emsExpressUtils;
	@Resource
	private CountyRepository countyRepository;
	@Resource
	private CityRepository cityRepository;
	@Resource
	private ProvinceRepository provinceRepository;
	@Resource
	private ExpressesRepository expressesRepository;
	@Resource
	private CustomMinIOClient customMinIOClient;
	@Resource
	private FileRepository fileRepository;


	@Transactional(rollbackFor = Exception.class)
	@Override
	public Expresses createOrder(String orderId, Address sendAddress, Address receiveAddress) throws Exception {
		EMSExpressUtils.AddressDTO sender = getAddress(sendAddress);
		EMSExpressUtils.AddressDTO receiver = getAddress(receiveAddress);
		EMSExpressUtils.DeliveryDTO delivery = getDelivery(sender);

		String expid = CommonUtils.uuid();
		String pickupOrder = emsExpressUtils.pickupOrder(expid, sender, receiver, delivery);
		if (emsExpressUtils.isSuccess(pickupOrder)) {
			String waybillNo = emsExpressUtils.getWaybillNo(pickupOrder);
			Expresses expresses = new Expresses();
			expresses.setExpid(expid);
			expresses.setOrdid(orderId);
			expresses.setStatus(CommonConstants.EXPRESS_STATUS_NORMAL);
			expresses.setWaybillNo(waybillNo);
			expresses.setExpressesAt(System.currentTimeMillis());
			expresses.setExpressType(EXPRESS_TYPE);
			expresses = expressesRepository.save(expresses);
			// 尝试获取面单
			String queryRoutes = emsExpressUtils.queryRoutes(waybillNo);
			if (emsExpressUtils.isSuccess(queryRoutes)) {
				byte[] waybillPdf = emsExpressUtils.getWaybillPdf(queryRoutes);
				InputStream is = new ByteArrayInputStream(waybillPdf);
				CustomFile customFile = new CustomFile();
				customFile.setFid(CommonUtils.uuid());
				customFile.setExpid(expresses.getExpid());
				customFile.setSize((long) waybillPdf.length);
				customFile.setName(waybillNo.concat(CommonConstants.DOT).concat(FILE_TYPE));
				customFile.setExtension(CommonUtils.getExtension(customFile.getName()));
				customFile.setDownload(Boolean.TRUE);
				customFile.setDirectory(OrderConstants.ORDER_BUCKET);
				customFile.setMime(FILE_TYPE);
				String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(FILE_TYPE);
				customMinIOClient.uploadFile(OrderConstants.ORDER_BUCKET, objectName, is);
				fileRepository.save(customFile);
			} else {
				throw new CustomBizException(emsExpressUtils.getErrorMessage(queryRoutes));
			}
			return expresses;
		} else {
			throw new CustomBizException(emsExpressUtils.getErrorMessage(pickupOrder));
		}
	}

	@Override
	public void cancelOrder(Expresses expresses) throws Exception {
		if (Objects.isNull(expresses)) {
			return;
		}
		String pickupOrderCancel = emsExpressUtils.pickupOrderCancel(expresses.getExpid());
		log.info("cancelOrder ret:{}", pickupOrderCancel);
		if (emsExpressUtils.isSuccess(pickupOrderCancel)) {
			expresses.setStatus(CommonConstants.EXPRESS_STATUS_CANCEL);
			expressesRepository.save(expresses);
		} else {
			throw new CustomBizException(emsExpressUtils.getErrorMessage(pickupOrderCancel));
		}
	}

	@Override
	public String searchRoutes(String waybillNo) throws CustomBizException {
		String trackInfo = emsExpressUtils.trackInfo(waybillNo);
		log.info("searchRoutes ret:{}", trackInfo);
		if (emsExpressUtils.isSuccess(trackInfo)) {
			return emsExpressUtils.getTrackInfoListString(trackInfo);
		} else {
			throw new CustomBizException(emsExpressUtils.getErrorMessage(trackInfo));
		}
	}

	@Override
	public BasePushResponse statePush(BasePush request) {
		EmsStatePush emsStatePush = (EmsStatePush) request;
		try {
			Expresses expresses = expressesRepository.findByExpid(emsStatePush.getLogisticsOrderNo());
			if (Objects.isNull(expresses)) {
				return EmsPushResponse.fail(emsExpressUtils.getSenderNo(), "物流订单号不存在");
			}
			expresses.setState(JSONObject.toJSONString(emsStatePush));
			expressesRepository.save(expresses);
			return EmsPushResponse.success(emsExpressUtils.getSenderNo());
		} catch (Exception e) {
			log.error("statePush error", e);
			return EmsPushResponse.fail(emsExpressUtils.getSenderNo(), e.getMessage());
		}
	}

	@Override
	public BasePushResponse feesPush(BasePush request) {
		EmsFeesPush feesPush = (EmsFeesPush) request;
		try {
			String waybillNo = feesPush.getWaybillNo();
			Expresses expresses = expressesRepository.findFirstByWaybillNoOrderByCtDesc(waybillNo);
			if (Objects.nonNull(expresses)) {
				if (FINISH_CODE.equals(feesPush.getStatus())) {
					expresses.setStatus(CommonConstants.EXPRESS_STATUS_FINISH);
					expresses.setPrice(feesPush.getPostageTotal());
				}
				if (CANCEL_CODE.equals(feesPush.getStatus())) {
					expresses.setStatus(CommonConstants.EXPRESS_STATUS_CANCEL);
				}
				expressesRepository.save(expresses);
				return EmsPushResponse.success(emsExpressUtils.getSenderNo());
			}
			return EmsPushResponse.fail(emsExpressUtils.getSenderNo(), "物流订单号不存在");
		} catch (Exception e) {
			log.error("statePush error", e);
			return EmsPushResponse.fail(emsExpressUtils.getSenderNo(), e.getMessage());
		}
	}

	@Override
	public Integer getExpressType() {
		return EXPRESS_TYPE;
	}

	private EMSExpressUtils.DeliveryDTO getDelivery(EMSExpressUtils.AddressDTO sender) {
		EMSExpressUtils.DeliveryDTO deliveryDTO = new EMSExpressUtils.DeliveryDTO();
		deliveryDTO.setAddress(sender.getAddress());
		deliveryDTO.setCounty(sender.getCounty());
		deliveryDTO.setProv(sender.getProv());
		deliveryDTO.setCity(sender.getCity());
		return deliveryDTO;
	}

	private EMSExpressUtils.AddressDTO getAddress(Address address) {
		EMSExpressUtils.AddressDTO addressDTO = new EMSExpressUtils.AddressDTO();
		County county = countyRepository.findByAreaCode(address.getCounty());
		address.setCounty(county.getAreaCode());
		City city = cityRepository.findByCityCode(address.getCity());
		Province province = provinceRepository.findByProvinceCode(address.getProvince());
		addressDTO.setName(address.getContactUser());
		addressDTO.setMobile(address.getContactTel());
		addressDTO.setProv(province.getProvinceName());
		addressDTO.setCity(city.getCityName());
		addressDTO.setCounty(county.getAreaName());
		addressDTO.setAddress(address.getDetail());
		return addressDTO;
	}
}
