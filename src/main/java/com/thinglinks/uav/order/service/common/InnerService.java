package com.thinglinks.uav.order.service.common;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.category.repository.CategoryRepository;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.MessageConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.dto.FileVO;
import com.thinglinks.uav.common.dto.FilesRequest;
import com.thinglinks.uav.common.entity.City;
import com.thinglinks.uav.common.entity.County;
import com.thinglinks.uav.common.entity.Province;
import com.thinglinks.uav.common.enums.RoleEnum;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.common.minio.ContentType;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.repository.CityRepository;
import com.thinglinks.uav.common.repository.CountyRepository;
import com.thinglinks.uav.common.repository.ProvinceRepository;
import com.thinglinks.uav.common.sms.AliYunSmsService;
import com.thinglinks.uav.common.template.*;
import com.thinglinks.uav.common.utils.*;
import com.thinglinks.uav.customer.dto.CustomerDTO;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.CompanyRepository;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.insurance.entity.Insurance;
import com.thinglinks.uav.insurance.entity.InsuranceDevice;
import com.thinglinks.uav.insurance.repository.InsuranceDeviceRepository;
import com.thinglinks.uav.insurance.repository.InsuranceRepository;
import com.thinglinks.uav.message.service.MessageService;
import com.thinglinks.uav.order.dto.ExpressesAddress;
import com.thinglinks.uav.order.dto.FirstInsurance;
import com.thinglinks.uav.order.dto.PageOrderQuery;
import com.thinglinks.uav.order.dto.excel.CellEnum;
import com.thinglinks.uav.order.dto.excel.ExportCell;
import com.thinglinks.uav.order.dto.excel.ImportOrder;
import com.thinglinks.uav.order.entity.*;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.*;
import com.thinglinks.uav.order.service.ExpressesService;
import com.thinglinks.uav.order.service.InsuranceFileService;
import com.thinglinks.uav.order.service.express.EMSExpressServiceImpl;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.spare.repository.ProjectProductsRepository;
import com.thinglinks.uav.stock.repository.StockInOutItemRepository;
import com.thinglinks.uav.stock.repository.StockInOutRepository;
import com.thinglinks.uav.stock.repository.StockRepository;
import com.thinglinks.uav.stock.repository.StoreRepository;
import com.thinglinks.uav.stock.service.StockService;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.entity.UserRole;
import com.thinglinks.uav.user.repository.RoleRepository;
import com.thinglinks.uav.user.repository.UserRepository;
import com.thinglinks.uav.user.repository.UserRoleRepository;
import com.thinglinks.uav.user.service.RoleService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.thinglinks.uav.common.constants.CommonConstants.*;
import static com.thinglinks.uav.common.constants.OrderConstants.*;
import static com.thinglinks.uav.order.enums.InsuranceStatusEnums.*;
import static com.thinglinks.uav.order.enums.OrderStatusEnums.*;

//类太大了，私有方法单独放
@Data
@Service
@Slf4j
public class InnerService {


    @Resource
    private ExpressesService expressesService;

    @Resource
    private EMSExpressServiceImpl emsExpressService;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private CustomersRepository customersRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private UserRoleRepository userRoleRepository;

    @Resource
    private AddressRepository addressRepository;

    @Resource
    private ProvinceRepository provinceRepository;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private InsuranceRepository insuranceRepository;


    @Resource
    private InsuranceDeviceRepository insuranceDeviceRepository;

    @Resource
    private CustomMinIOClient customMinIOClient;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private OrderLogRepository orderLogRepository;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private OrderCategoryRepository orderCategoryRepository;

    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Resource
    private ExpressesRepository expressesRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private OrderQuoteRepository orderQuoteRepository;

    @Resource
    private OrderQuoteItemRepository orderQuoteItemRepository;

    @Resource
    private OrderSettleRepository orderSettleRepository;

    @Resource
    private StockService stockService;

    @Resource
    private StoreRepository storeRepository;

    @Resource
    private StockInOutRepository stockInOutRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private StockInOutItemRepository stockInOutItemRepository;

    @Resource
    private InsuranceMaterialRepository insuranceMaterialRepository;

    @Resource
    private ProjectProductsRepository projectProductsRepository;

    @Resource
    private ProductRepository productRepository;

    @Resource
    private CountyRepository countyRepository;

    @Resource
    private CityRepository cityRepository;

    @Resource
    private CustomConfig customConfig;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private WordUtils wordUtils;

    @Value("${expresses.type}")
    private String expressType;

    @Resource
    private AliYunSmsService aliYunSmsService;

    @Resource
    private MessageService messageService;

    @Resource
    private RoleService roleService;

    @Value("${password.default}")
    private String defaultPassword;

    @Resource
    private InsuranceFileService insuranceFileService;

    @Resource
    private CompanyRepository companyRepository;

    /**
     * @param costQuote
     * @return
     */
    public Workbook writeCostQuote(Order order, List<OrderQuote> costQuote) throws Exception {
        Customer customer = customersRepository.findByCid(order.getCid());
        Address address = addressRepository.getByAid(order.getAid());
        Device device = deviceRepository.findByDevid(order.getDevid());
        OrderLog quoteLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(order.getOrdid(), ORDER_LOG_METHOD_QUOTATION);
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        Workbook workbook = new XSSFWorkbook();
        try {
            Font font11 = workbook.createFont();
            font11.setFontName("微软雅黑");
            font11.setFontHeightInPoints((short) 11);
            //文字居中
            CellStyle frontCenter = workbook.createCellStyle();
            frontCenter.setAlignment(HorizontalAlignment.CENTER);
            frontCenter.setVerticalAlignment(VerticalAlignment.CENTER);
            frontCenter.setFont(font11);
            PoiExcelUtils.setCellBorder(frontCenter);
            //文字居左
            CellStyle frontLeftAuto = workbook.createCellStyle();
            frontLeftAuto.setAlignment(HorizontalAlignment.LEFT);
            frontLeftAuto.setVerticalAlignment(VerticalAlignment.CENTER);
            frontLeftAuto.setWrapText(true);
            frontLeftAuto.setFont(font11);
            PoiExcelUtils.setCellBorder(frontLeftAuto);
            //文字居中
            CellStyle frontCenterBold = workbook.createCellStyle();
            frontCenterBold.setAlignment(HorizontalAlignment.CENTER);
            frontCenterBold.setVerticalAlignment(VerticalAlignment.CENTER);
            Font font0 = workbook.createFont();
            font0.setBold(true);
            font0.setFontHeightInPoints((short) 11);
            frontCenterBold.setFont(font0);
            PoiExcelUtils.setCellBorder(frontCenterBold);
            //文字居中+背景
            CellStyle frontCenterBoldBack = workbook.createCellStyle();
            frontCenterBoldBack.setAlignment(HorizontalAlignment.CENTER);
            frontCenterBoldBack.setVerticalAlignment(VerticalAlignment.CENTER);
            Font font2 = workbook.createFont();
            font2.setBold(true);
            font2.setFontHeightInPoints((short) 11);
            frontCenterBoldBack.setFont(font2);
            frontCenterBoldBack.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
            frontCenterBoldBack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            PoiExcelUtils.setCellBorder(frontCenterBoldBack);
            //文字居左
            CellStyle frontLeft = workbook.createCellStyle();
            frontLeft.setAlignment(HorizontalAlignment.LEFT);
            frontLeft.setVerticalAlignment(VerticalAlignment.CENTER);
            frontLeft.setFont(font11);
            PoiExcelUtils.setCellBorder(frontLeft);
            //文字居右
            CellStyle frontRight = workbook.createCellStyle();
            frontRight.setAlignment(HorizontalAlignment.RIGHT);
            frontRight.setVerticalAlignment(VerticalAlignment.CENTER);
            frontRight.setFont(font11);
            PoiExcelUtils.setCellBorder(frontRight);
            //文字居中+背景色灰
            CellStyle frontCenterBackGray = workbook.createCellStyle();
            frontCenterBackGray.setAlignment(HorizontalAlignment.CENTER);
            frontCenterBackGray.setVerticalAlignment(VerticalAlignment.CENTER);
            frontCenterBackGray.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
            frontCenterBackGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            frontCenterBackGray.setFont(font11);
            PoiExcelUtils.setCellBorder(frontCenterBackGray);
            //文字居中+背景色红
            CellStyle frontCenterBackRed = workbook.createCellStyle();
            frontCenterBackRed.setAlignment(HorizontalAlignment.CENTER);
            frontCenterBackRed.setVerticalAlignment(VerticalAlignment.CENTER);
            frontCenterBackRed.setFillForegroundColor(IndexedColors.RED.index);
            frontCenterBackRed.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            frontCenterBackRed.setFont(font11);
            PoiExcelUtils.setCellBorder(frontCenterBackRed);
            //文字居中+字体
            CellStyle bigFrontCenter = workbook.createCellStyle();
            bigFrontCenter.setAlignment(HorizontalAlignment.CENTER);
            bigFrontCenter.setVerticalAlignment(VerticalAlignment.CENTER);
            Font font16 = workbook.createFont();
            font16.setBold(true);
            font16.setFontName("微软雅黑");
            font16.setFontHeightInPoints((short) 16);
            bigFrontCenter.setFont(font16);
            PoiExcelUtils.setCellBorder(bigFrontCenter);
            //背景灰
            CellStyle backgroundGray = workbook.createCellStyle();
            backgroundGray.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
            backgroundGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            PoiExcelUtils.setCellBorder(backgroundGray);

            Sheet costSheet = workbook.createSheet("成本报价单");
            Row row0 = costSheet.createRow(0);
            row0.setHeightInPoints(35);
            Cell c10 = row0.createCell(0);
            c10.setCellValue("无人机维修成本报价单");
            c10.setCellStyle(bigFrontCenter);
            CellRangeAddress r1 = new CellRangeAddress(0, 1, 0, 7);
            costSheet.addMergedRegion(r1);
            PoiExcelUtils.setRegionBorder(r1, costSheet);

            Row row2 = costSheet.createRow(2);
            row2.setHeightInPoints(20);
            Cell c20 = row2.createCell(0);
            c20.setCellStyle(backgroundGray);
            CellRangeAddress r2 = new CellRangeAddress(2, 2, 0, 7);
            costSheet.addMergedRegion(r2);
            PoiExcelUtils.setRegionBorder(r2, costSheet);

            Row row3 = costSheet.createRow(3);
            row3.setHeightInPoints(20);
            Cell c30 = row3.createCell(0);
            c30.setCellValue("基本信息");
            c30.setCellStyle(frontCenter);
            CellRangeAddress r3 = new CellRangeAddress(3, 7, 0, 0);
            costSheet.addMergedRegion(r3);
            PoiExcelUtils.setRegionBorder(r3, costSheet);
            Cell c31 = row3.createCell(1);
            c31.setCellValue("客户名称");
            c31.setCellStyle(frontCenterBackGray);
            Cell c32 = row3.createCell(2);
            if (Objects.nonNull(insuranceMaterial)) {
                Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
                if (Objects.nonNull(accidentCustomer)) {
                    c32.setCellValue(accidentCustomer.getName());
                }
            } else {
                c32.setCellValue(customer.getName());
            }
            c32.setCellStyle(frontCenter);
            CellRangeAddress r4 = new CellRangeAddress(3, 3, 2, 7);
            costSheet.addMergedRegion(r4);
            PoiExcelUtils.setRegionBorder(r4, costSheet);

            Row row4 = costSheet.createRow(4);
            row4.setHeightInPoints(20);
            Cell c41 = row4.createCell(1);
            c41.setCellValue("飞机型号");
            c41.setCellStyle(frontCenterBackGray);
            Cell c42 = row4.createCell(2);
            c42.setCellValue(device.getProduct().getModel());
            c42.setCellStyle(frontCenter);
            CellRangeAddress r5 = new CellRangeAddress(4, 4, 2, 7);
            costSheet.addMergedRegion(r5);
            PoiExcelUtils.setRegionBorder(r5, costSheet);

            Row row5 = costSheet.createRow(5);
            row5.setHeightInPoints(20);
            Cell c51 = row5.createCell(1);
            c51.setCellValue("飞机SN码");
            c51.setCellStyle(frontCenterBackGray);
            Cell c52 = row5.createCell(2);
            c52.setCellValue(device.getCode());
            c52.setCellStyle(frontCenter);
            CellRangeAddress r6 = new CellRangeAddress(5, 5, 2, 7);
            costSheet.addMergedRegion(r6);
            PoiExcelUtils.setRegionBorder(r6, costSheet);

            Row row6 = costSheet.createRow(6);
            row6.setHeightInPoints(20);
            Cell c61 = row6.createCell(1);
            c61.setCellValue("报价日期");
            c61.setCellStyle(frontCenterBackGray);
            Cell c62 = row6.createCell(2);
            c62.setCellValue(DateUtils.formatDateDot(quoteLog.getCt()));
            c62.setCellStyle(frontCenter);
            CellRangeAddress r7 = new CellRangeAddress(6, 6, 2, 7);
            costSheet.addMergedRegion(r7);
            PoiExcelUtils.setRegionBorder(r7, costSheet);

            Row row7 = costSheet.createRow(7);
            row7.setHeightInPoints(20);
            Cell c70 = row7.createCell(0);
            c70.setCellStyle(frontCenter);
            Cell c71 = row7.createCell(1);
            c71.setCellValue("联系人");
            c71.setCellStyle(frontCenterBackGray);
            Cell c72 = row7.createCell(2);
            if (Objects.nonNull(address)) {
                c72.setCellValue(address.getContactUser().concat(address.getContactTel()));
            } else {
                c72.setCellValue(StringUtils.EMPTY);
            }
            c72.setCellStyle(frontCenter);
            CellRangeAddress r8 = new CellRangeAddress(7, 7, 2, 7);
            costSheet.addMergedRegion(r8);
            PoiExcelUtils.setRegionBorder(r8, costSheet);

            Row row8 = costSheet.createRow(8);
            row8.setHeightInPoints(-1);
            Cell c80 = row8.createCell(0);
            c80.setCellValue("定损描述");
            c80.setCellStyle(frontCenter);
            Cell c81 = row8.createCell(1);
            c81.setCellValue("定损结果");
            c81.setCellStyle(frontCenterBackGray);
            Cell c83 = row8.createCell(2);
            List<OrderCategory> orderCategories = orderCategoryRepository.findByOrdid(order.getOrdid());
            List<String> ordcatids = orderCategories.stream().map(OrderCategory::getCatid).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(ordcatids)) {
                List<String> categories = categoryRepository.findAllByCatidIn(ordcatids).stream().map(Category::getName).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(categories)) {
                    c83.setCellValue(StringUtils.join(categories, COMMA));
                }
            }
            c83.setCellStyle(frontLeftAuto);
            CellRangeAddress r81 = new CellRangeAddress(8, 8, 2, 7);
            costSheet.addMergedRegion(r81);
            PoiExcelUtils.setRegionBorder(r81, costSheet);

            Row row9 = costSheet.createRow(9);
            row9.setHeightInPoints(20);
            Cell c90 = row9.createCell(0);
            c90.setCellValue("服务类型");
            c90.setCellStyle(frontCenter);
            Cell c91 = row9.createCell(1);
            c91.setCellValue("处理建议");
            c91.setCellStyle(frontCenterBackGray);
            Cell c93 = row9.createCell(2);
            c93.setCellValue(CommonUtils.orderTypeConvert(order.getType()));
            c93.setCellStyle(frontCenter);
            CellRangeAddress r91 = new CellRangeAddress(9, 9, 2, 7);
            costSheet.addMergedRegion(r91);
            PoiExcelUtils.setRegionBorder(r91, costSheet);

            Row row10 = costSheet.createRow(10);
            row10.setHeightInPoints(20);
            Cell c100 = row10.createCell(0);
            c100.setCellValue("服务方式");
            c100.setCellStyle(frontCenter);
            Cell c101 = row10.createCell(1);
            c101.setCellValue("服务方式");
            c101.setCellStyle(frontCenterBackGray);
            Cell c103 = row10.createCell(2);
            c103.setCellValue(CommonUtils.orderServiceTypeConvert(order.getServiceType()));
            c103.setCellStyle(frontCenter);
            CellRangeAddress r101 = new CellRangeAddress(10, 10, 2, 7);
            costSheet.addMergedRegion(r101);
            PoiExcelUtils.setRegionBorder(r101, costSheet);

            Row row11 = costSheet.createRow(11);
            row11.setHeightInPoints(20);
            Cell c110 = row11.createCell(0);
            c110.setCellValue("分类");
            c110.setCellStyle(frontCenterBold);
            Cell c111 = row11.createCell(1);
            c111.setCellValue("序号");
            c111.setCellStyle(frontCenterBoldBack);
            Cell c112 = row11.createCell(2);
            c112.setCellValue("消耗物料清单");
            c112.setCellStyle(frontCenterBoldBack);
            Cell c113 = row11.createCell(3);
            c113.setCellValue("单位");
            c113.setCellStyle(frontCenterBoldBack);
            Cell c114 = row11.createCell(4);
            c114.setCellValue("数量");
            c114.setCellStyle(frontCenterBoldBack);
            Cell c115 = row11.createCell(5);
            c115.setCellValue("成本单价（元）");
            c115.setCellStyle(frontCenterBoldBack);
            Cell c116 = row11.createCell(6);
            c116.setCellValue("工时费（小计）");
            c116.setCellStyle(frontCenterBoldBack);
            Cell c117 = row11.createCell(7);
            c117.setCellValue("金额（元）");
            c117.setCellStyle(frontCenterBoldBack);
            int size = costQuote.size();
            int totalItemSize = 0;
            for (int i = 0; i < size; i++) {
                OrderQuote orderQuote = costQuote.get(i);
                List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(orderQuote.getOqid());
                int itemSize = orderQuoteItems.size();
                //表示第一次报价
                if (i == 0) {
                    Row firstRow = costSheet.createRow(12);
                    firstRow.setHeightInPoints(20);
                    Cell c120 = firstRow.createCell(0);
                    c120.setCellValue("客户已确认费用");
                    c120.setCellStyle(frontCenter);
                    CellRangeAddress r9 = new CellRangeAddress(12, 12 + itemSize + 5 - 1, 0, 0);
                    costSheet.addMergedRegion(r9);
                    PoiExcelUtils.setRegionBorder(r9, costSheet);
                    this.writeCostQuoteItem(costSheet, firstRow, 12, orderQuote, orderQuoteItems, frontCenterBackGray, frontLeft, frontCenter, frontRight, true);
                    int endRowNo = 12 + itemSize + 5 - 1;
                    Row endRow = costSheet.createRow(endRowNo);
                    endRow.setHeightInPoints(20);
                    Cell er1 = endRow.createCell(1);
                    er1.setCellValue("开票内容");
                    er1.setCellStyle(frontCenter);
                    CellRangeAddress r10 = new CellRangeAddress(endRowNo, endRowNo, 1, 2);
                    costSheet.addMergedRegion(r10);
                    PoiExcelUtils.setRegionBorder(r10, costSheet);
                    Cell er3 = endRow.createCell(3);
                    er3.setCellValue("维修费");
                    er3.setCellStyle(frontCenter);
                    CellRangeAddress r11 = new CellRangeAddress(endRowNo, endRowNo, 3, 7);
                    costSheet.addMergedRegion(r11);
                    PoiExcelUtils.setRegionBorder(r11, costSheet);

                    int divisionNo = endRowNo + 1;
                    Row division1 = costSheet.createRow(divisionNo);
                    division1.setHeightInPoints(20);
                    Cell d0 = division1.createCell(0);
                    d0.setCellStyle(backgroundGray);
                    CellRangeAddress r12 = new CellRangeAddress(divisionNo, divisionNo, 0, 7);
                    costSheet.addMergedRegion(r12);
                    PoiExcelUtils.setRegionBorder(r12, costSheet);
                } else {
                    //开始行数=表格前面固定12行+所有报价备件列表总数+ 第一次报价固定6行+新增报价每次固定5行,加1，表示下一行
                    int begin = 12 + totalItemSize + 6 + 5 * (i - 1);
                    Row firstRow = costSheet.createRow(begin);
                    firstRow.setHeightInPoints(20);
                    Cell c120 = firstRow.createCell(0);
                    c120.setCellValue("新增定损费用");
                    c120.setCellStyle(frontCenterBackRed);
                    CellRangeAddress r9 = new CellRangeAddress(begin, begin + itemSize + 3, 0, 0);
                    costSheet.addMergedRegion(r9);
                    PoiExcelUtils.setRegionBorder(r9, costSheet);
                    this.writeCostQuoteItem(costSheet, firstRow, begin, orderQuote, orderQuoteItems, frontCenterBackGray, frontLeft, frontCenter, frontRight, false);
                    int divisionNo = begin + itemSize + 4;
                    Row division1 = costSheet.createRow(divisionNo);
                    division1.setHeightInPoints(20);
                    Cell d0 = division1.createCell(0);
                    d0.setCellStyle(backgroundGray);
                    CellRangeAddress r12 = new CellRangeAddress(divisionNo, divisionNo, 0, 7);
                    costSheet.addMergedRegion(r12);
                    PoiExcelUtils.setRegionBorder(r12, costSheet);
                }
                totalItemSize = totalItemSize + itemSize;
            }
            //成本费用合计
            int begin = 12 + totalItemSize + 6 + 5 * (size - 1);
            Row sumRow1 = costSheet.createRow(begin);
            sumRow1.setHeightInPoints(20);
            Cell sum0 = sumRow1.createCell(0);
            sum0.setCellValue("成本费用合计");
            sum0.setCellStyle(frontCenter);
            CellRangeAddress r12 = new CellRangeAddress(begin, begin + 1, 0, 0);
            costSheet.addMergedRegion(r12);
            PoiExcelUtils.setRegionBorder(r12, costSheet);
            Cell sum1 = sumRow1.createCell(1);
            sum1.setCellValue("标准价格");
            sum1.setCellStyle(frontCenter);
            CellRangeAddress r13 = new CellRangeAddress(begin, begin, 1, 2);
            costSheet.addMergedRegion(r13);
            PoiExcelUtils.setRegionBorder(r13, costSheet);
            double sparePrice = costQuote.stream().mapToDouble(OrderQuote::getSparePrice).sum();
            double expressesPrice = costQuote.stream().mapToDouble(OrderQuote::getExpressesPrice).sum();
            double otherPrice = costQuote.stream().mapToDouble(OrderQuote::getOtherPrice).sum();
            Cell sum3 = sumRow1.createCell(3);
            double totalPrice = CommonUtils.format2Point(sparePrice + expressesPrice + otherPrice);
            sum3.setCellValue(CommonConstants.MONEY.concat(String.valueOf(totalPrice)));
            sum3.setCellStyle(frontCenter);
            CellRangeAddress r131 = new CellRangeAddress(begin, begin, 3, 7);
            costSheet.addMergedRegion(r131);
            PoiExcelUtils.setRegionBorder(r131, costSheet);
            Row sumRow2 = costSheet.createRow(begin + 1);
            sumRow2.setHeightInPoints(20);
            Cell sum20 = sumRow2.createCell(0);
            sum20.setCellStyle(frontCenter);
            Cell sum21 = sumRow2.createCell(1);
            sum21.setCellValue("总计");
            sum21.setCellStyle(frontCenter);
            CellRangeAddress r14 = new CellRangeAddress(begin + 1, begin + 1, 1, 2);
            costSheet.addMergedRegion(r14);
            PoiExcelUtils.setRegionBorder(r14, costSheet);
            Cell sum23 = sumRow2.createCell(3);
            sum23.setCellValue("人民币:");
            sum23.setCellStyle(frontRight);
            CellRangeAddress r15 = new CellRangeAddress(begin + 1, begin + 1, 3, 4);
            costSheet.addMergedRegion(r15);
            PoiExcelUtils.setRegionBorder(r15, costSheet);
            Cell sum25 = sumRow2.createCell(5);
            sum25.setCellValue(CommonUtils.toChineseMoney(new BigDecimal(totalPrice)));
            sum25.setCellStyle(frontLeft);
            CellRangeAddress r16 = new CellRangeAddress(begin + 1, begin + 1, 5, 7);
            costSheet.addMergedRegion(r16);
            PoiExcelUtils.setRegionBorder(r16, costSheet);
            costSheet.setColumnWidth(0, 22 * 256);
            costSheet.setColumnWidth(1, 15 * 256);
            costSheet.setColumnWidth(2, 40 * 256);
            costSheet.setColumnWidth(3, 8 * 256);
            costSheet.setColumnWidth(4, 8 * 256);
            costSheet.setColumnWidth(5, 12 * 256);
            costSheet.setColumnWidth(6, 12 * 256);
            costSheet.setColumnWidth(7, 22 * 256);
            return workbook;
        } catch (Exception e) {
            log.error("writeCostQuote error", e);
            workbook.close();
            throw e;
        }
    }

    /**
     * @param customerQuote
     * @return
     */
    public Workbook writeCustomerQuote(Order order, OrderQuote customerQuote) throws Exception {
        Workbook workbook = new XSSFWorkbook();
        try {
            this.writeCustomerQuote(order, customerQuote, workbook);
            return workbook;
        } catch (Exception e) {
            log.error("writeCustomerQuote error", e);
            workbook.close();
            throw e;
        }
    }

    /**
     * 客户报价单
     *
     * @param order
     * @param customerQuote
     * @param workbook
     */
    public void writeCustomerQuote(Order order, OrderQuote customerQuote, Workbook workbook) throws Exception {
        Customer customer = customersRepository.findByCid(order.getCid());
        Address address = addressRepository.getByAid(order.getAid());
        Device device = deviceRepository.findByDevid(order.getDevid());
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        OrderLog quoteLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(order.getOrdid(), ORDER_LOG_METHOD_QUOTATION);
        Sheet customerSheet = workbook.createSheet("客户报价单");
        Font font11 = workbook.createFont();
        font11.setFontName("微软雅黑");
        font11.setFontHeightInPoints((short) 11);
        //文字居中
        CellStyle frontCenter = workbook.createCellStyle();
        frontCenter.setAlignment(HorizontalAlignment.CENTER);
        frontCenter.setVerticalAlignment(VerticalAlignment.CENTER);
        frontCenter.setFont(font11);
        PoiExcelUtils.setCellBorder(frontCenter);
        //文字居左
        CellStyle frontLeftAuto = workbook.createCellStyle();
        frontLeftAuto.setAlignment(HorizontalAlignment.LEFT);
        frontLeftAuto.setVerticalAlignment(VerticalAlignment.CENTER);
        frontLeftAuto.setWrapText(true);
        frontLeftAuto.setFont(font11);
        PoiExcelUtils.setCellBorder(frontLeftAuto);
        //文字居中
        CellStyle frontCenterBold = workbook.createCellStyle();
        frontCenterBold.setAlignment(HorizontalAlignment.CENTER);
        frontCenterBold.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font0 = workbook.createFont();
        font0.setBold(true);
        font0.setFontHeightInPoints((short) 11);
        frontCenterBold.setFont(font0);
        PoiExcelUtils.setCellBorder(frontCenterBold);
        //文字居中+背景+字体
        CellStyle frontCenterBoldBack = workbook.createCellStyle();
        frontCenterBoldBack.setAlignment(HorizontalAlignment.CENTER);
        frontCenterBoldBack.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font2 = workbook.createFont();
        font2.setBold(true);
        font2.setFontHeightInPoints((short) 11);
        frontCenterBoldBack.setFont(font2);
        frontCenterBoldBack.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        frontCenterBoldBack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        PoiExcelUtils.setCellBorder(frontCenterBoldBack);
        //文字居左
        CellStyle frontLeft = workbook.createCellStyle();
        frontLeft.setAlignment(HorizontalAlignment.LEFT);
        frontLeft.setVerticalAlignment(VerticalAlignment.CENTER);
        frontLeft.setFont(font11);
        PoiExcelUtils.setCellBorder(frontLeft);
        //文字居左+背景
        CellStyle frontLeftBack = workbook.createCellStyle();
        frontLeftBack.setAlignment(HorizontalAlignment.LEFT);
        frontLeftBack.setVerticalAlignment(VerticalAlignment.CENTER);
        frontLeftBack.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        frontLeftBack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        frontLeftBack.setFont(font11);
        PoiExcelUtils.setCellBorder(frontLeftBack);
        //文字居右 绿色
        CellStyle frontRightGreen = workbook.createCellStyle();
        frontRightGreen.setAlignment(HorizontalAlignment.RIGHT);
        frontRightGreen.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font3 = workbook.createFont();
        font3.setBold(true);
        font3.setColor(IndexedColors.GREEN.index);
        font3.setFontHeightInPoints((short) 11);
        frontRightGreen.setFont(font3);
        PoiExcelUtils.setCellBorder(frontRightGreen);
        //文字居右
        CellStyle frontRight = workbook.createCellStyle();
        frontRight.setAlignment(HorizontalAlignment.RIGHT);
        frontRight.setVerticalAlignment(VerticalAlignment.CENTER);
        frontRight.setFont(font11);
        PoiExcelUtils.setCellBorder(frontRight);
        //文字居中+背景色灰
        CellStyle frontCenterBackGray = workbook.createCellStyle();
        frontCenterBackGray.setAlignment(HorizontalAlignment.CENTER);
        frontCenterBackGray.setVerticalAlignment(VerticalAlignment.CENTER);
        frontCenterBackGray.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        frontCenterBackGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        frontCenterBackGray.setFont(font11);
        PoiExcelUtils.setCellBorder(frontCenterBackGray);
        //文字居中+字体
        CellStyle bigFrontCenter = workbook.createCellStyle();
        bigFrontCenter.setAlignment(HorizontalAlignment.CENTER);
        bigFrontCenter.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font1 = workbook.createFont();
        font1.setBold(true);
        font1.setFontName("微软雅黑");
        font1.setFontHeightInPoints((short) 16);
        bigFrontCenter.setFont(font1);
        PoiExcelUtils.setCellBorder(bigFrontCenter);
        //背景灰
        CellStyle backgroundGray = workbook.createCellStyle();
        backgroundGray.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        backgroundGray.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        PoiExcelUtils.setCellBorder(backgroundGray);

        Row row0 = customerSheet.createRow(0);
        row0.setHeightInPoints(35);
        Cell c10 = row0.createCell(0);
        c10.setCellValue("无人机维修成本报价单");
        c10.setCellStyle(bigFrontCenter);
        CellRangeAddress r1 = new CellRangeAddress(0, 1, 0, 6);
        customerSheet.addMergedRegion(r1);
        PoiExcelUtils.setRegionBorder(r1, customerSheet);

        Row row2 = customerSheet.createRow(2);
        row2.setHeightInPoints(20);
        Cell c20 = row2.createCell(0);
        c20.setCellStyle(backgroundGray);
        CellRangeAddress r2 = new CellRangeAddress(2, 2, 0, 6);
        customerSheet.addMergedRegion(r2);
        PoiExcelUtils.setRegionBorder(r2, customerSheet);

        Row row3 = customerSheet.createRow(3);
        row3.setHeightInPoints(20);
        Cell c30 = row3.createCell(0);
        c30.setCellValue("客户名称");
        c30.setCellStyle(frontCenterBackGray);
        Cell c31 = row3.createCell(1);
        if (Objects.nonNull(insuranceMaterial)) {
            Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
            if (Objects.nonNull(accidentCustomer)) {
                c31.setCellValue(accidentCustomer.getName());
            }
        } else {
            c31.setCellValue(customer.getName());
        }
        c31.setCellStyle(frontCenter);
        CellRangeAddress r4 = new CellRangeAddress(3, 3, 1, 6);
        customerSheet.addMergedRegion(r4);
        PoiExcelUtils.setRegionBorder(r4, customerSheet);

        Row row4 = customerSheet.createRow(4);
        row4.setHeightInPoints(20);
        Cell c40 = row4.createCell(0);
        c40.setCellValue("飞机型号");
        c40.setCellStyle(frontCenterBackGray);
        Cell c41 = row4.createCell(1);
        c41.setCellValue(device.getProduct().getModel());
        c41.setCellStyle(frontCenter);
        CellRangeAddress r5 = new CellRangeAddress(4, 4, 1, 6);
        customerSheet.addMergedRegion(r5);
        PoiExcelUtils.setRegionBorder(r5, customerSheet);

        Row row5 = customerSheet.createRow(5);
        row5.setHeightInPoints(20);
        Cell c50 = row5.createCell(0);
        c50.setCellValue("飞机SN码");
        c50.setCellStyle(frontCenterBackGray);
        Cell c51 = row5.createCell(1);
        c51.setCellValue(device.getCode());
        c51.setCellStyle(frontCenter);
        CellRangeAddress r6 = new CellRangeAddress(5, 5, 1, 6);
        customerSheet.addMergedRegion(r6);
        PoiExcelUtils.setRegionBorder(r6, customerSheet);

        Row row6 = customerSheet.createRow(6);
        row6.setHeightInPoints(20);
        Cell c60 = row6.createCell(0);
        c60.setCellValue("报价日期");
        c60.setCellStyle(frontCenterBackGray);
        Cell c61 = row6.createCell(1);
        c61.setCellValue(DateUtils.formatDateDot(quoteLog.getCt()));
        c61.setCellStyle(frontCenter);
        CellRangeAddress r7 = new CellRangeAddress(6, 6, 1, 6);
        customerSheet.addMergedRegion(r7);
        PoiExcelUtils.setRegionBorder(r7, customerSheet);

        Row row7 = customerSheet.createRow(7);
        row7.setHeightInPoints(20);
        Cell c70 = row7.createCell(0);
        c70.setCellStyle(frontCenterBackGray);
        c70.setCellValue("联系人");
        Cell c71 = row7.createCell(1);
        if (Objects.nonNull(address)) {
            c71.setCellValue(address.getContactUser().concat(address.getContactTel()));
        } else {
            c71.setCellValue(StringUtils.EMPTY);
        }
        c71.setCellStyle(frontCenter);
        CellRangeAddress r8 = new CellRangeAddress(7, 7, 1, 6);
        customerSheet.addMergedRegion(r8);
        PoiExcelUtils.setRegionBorder(r8, customerSheet);

        Row row8 = customerSheet.createRow(8);
        row8.setHeightInPoints(-1);
        Cell c80 = row8.createCell(0);
        c80.setCellValue("定损结果");
        c80.setCellStyle(frontCenterBackGray);
        Cell c81 = row8.createCell(1);
        List<OrderCategory> orderCategories = orderCategoryRepository.findByOrdid(order.getOrdid());
        List<String> ordcatids = orderCategories.stream().map(OrderCategory::getCatid).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(ordcatids)) {
            List<String> categories = categoryRepository.findAllByCatidIn(ordcatids).stream().map(Category::getName).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(categories)) {
                c81.setCellValue(StringUtils.join(categories, COMMA));
            }
        }
        c81.setCellStyle(frontLeftAuto);
        CellRangeAddress r81 = new CellRangeAddress(8, 8, 1, 6);
        customerSheet.addMergedRegion(r81);
        PoiExcelUtils.setRegionBorder(r81, customerSheet);

        Row row9 = customerSheet.createRow(9);
        row9.setHeightInPoints(20);
        Cell c90 = row9.createCell(0);
        c90.setCellValue("处理建议");
        c90.setCellStyle(frontCenterBackGray);
        Cell c91 = row9.createCell(1);
        c91.setCellValue(CommonUtils.orderTypeConvert(order.getType()));
        c91.setCellStyle(frontCenter);
        CellRangeAddress r91 = new CellRangeAddress(9, 9, 1, 6);
        customerSheet.addMergedRegion(r91);
        PoiExcelUtils.setRegionBorder(r91, customerSheet);

        Row row10 = customerSheet.createRow(10);
        row10.setHeightInPoints(20);
        Cell c100 = row10.createCell(0);
        c100.setCellStyle(backgroundGray);
        CellRangeAddress r10 = new CellRangeAddress(10, 10, 0, 6);
        customerSheet.addMergedRegion(r10);
        PoiExcelUtils.setRegionBorder(r10, customerSheet);

        Row row11 = customerSheet.createRow(11);
        row11.setHeightInPoints(20);
        Cell c111 = row11.createCell(0);
        c111.setCellValue("序号");
        c111.setCellStyle(frontCenterBoldBack);
        Cell c112 = row11.createCell(1);
        c112.setCellValue("消耗物料清单");
        c112.setCellStyle(frontCenterBoldBack);
        Cell c113 = row11.createCell(2);
        c113.setCellValue("单位");
        c113.setCellStyle(frontCenterBoldBack);
        Cell c114 = row11.createCell(3);
        c114.setCellValue("数量");
        c114.setCellStyle(frontCenterBoldBack);
        Cell c115 = row11.createCell(4);
        c115.setCellValue("单价（元）");
        c115.setCellStyle(frontCenterBoldBack);
        Cell c116 = row11.createCell(5);
        c116.setCellValue("工时费（小计）");
        c116.setCellStyle(frontCenterBoldBack);
        Cell c117 = row11.createCell(6);
        c117.setCellValue("金额（元）");
        c117.setCellStyle(frontCenterBoldBack);

        List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(customerQuote.getOqid());
        int itemSize = orderQuoteItems.size();
        int begin0 = 12;
        for (int i1 = 0; i1 < itemSize; i1++) {
            OrderQuoteItem quoteItem = orderQuoteItems.get(i1);
            Row itemRow = customerSheet.createRow(begin0 + i1);
            itemRow.setHeightInPoints(20);
            Cell c1 = itemRow.createCell(0);
            c1.setCellValue(i1 + 1);
            c1.setCellStyle(frontCenter);
            Cell c2 = itemRow.createCell(1);
            Product product = productRepository.findByPid(quoteItem.getPid());
            c2.setCellValue(product.getName());
            c2.setCellStyle(frontLeft);
            Cell c3 = itemRow.createCell(2);
            c3.setCellValue("套");
            c3.setCellStyle(frontCenter);
            Cell c4 = itemRow.createCell(3);
            c4.setCellValue(quoteItem.getCount());
            c4.setCellStyle(frontCenter);
            Cell c5 = itemRow.createCell(4);
            c5.setCellValue(quoteItem.getSalePrice());
            c5.setCellStyle(frontCenter);
            Cell c6 = itemRow.createCell(5);
            c6.setCellValue(quoteItem.getWorkHourCost());
            c6.setCellStyle(frontCenter);
            Cell c7 = itemRow.createCell(6);
            c7.setCellValue(quoteItem.getCount() * (quoteItem.getWorkHourCost() + quoteItem.getSalePrice()));
            c7.setCellStyle(frontCenter);
        }
        int begin1 = 12 + itemSize;
        Row standardRow = customerSheet.createRow(begin1);
        standardRow.setHeightInPoints(20);
        Cell s1 = standardRow.createCell(0);
        s1.setCellValue("标准价格");
        s1.setCellStyle(frontCenter);
        CellRangeAddress r11 = new CellRangeAddress(begin1, begin1, 0, 1);
        customerSheet.addMergedRegion(r11);
        PoiExcelUtils.setRegionBorder(r11, customerSheet);
        Cell s3 = standardRow.createCell(2);
        double total = customerQuote.getSparePrice() + customerQuote.getExpressesPrice() + customerQuote.getOtherPrice();
        s3.setCellValue(CommonConstants.MONEY.concat(String.valueOf(total)));
        s3.setCellStyle(frontCenter);
        CellRangeAddress r12 = new CellRangeAddress(begin1, begin1, 2, 6);
        customerSheet.addMergedRegion(r12);
        PoiExcelUtils.setRegionBorder(r12, customerSheet);

        int rowNo1 = begin1 + 1;
        Row totalRow = customerSheet.createRow(rowNo1);
        totalRow.setHeightInPoints(20);
        Cell t1 = totalRow.createCell(0);
        t1.setCellValue("总计");
        t1.setCellStyle(frontCenter);
        CellRangeAddress r13 = new CellRangeAddress(rowNo1, rowNo1, 0, 1);
        customerSheet.addMergedRegion(r13);
        PoiExcelUtils.setRegionBorder(r13, customerSheet);
        Cell t3 = totalRow.createCell(2);
        t3.setCellValue("人民币：");
        t3.setCellStyle(frontRight);
        CellRangeAddress r14 = new CellRangeAddress(rowNo1, rowNo1, 2, 3);
        customerSheet.addMergedRegion(r14);
        PoiExcelUtils.setRegionBorder(r14, customerSheet);
        Cell t5 = totalRow.createCell(4);
        t5.setCellValue(CommonUtils.toChineseMoney(new BigDecimal(total)));
        t5.setCellStyle(frontLeft);
        CellRangeAddress r15 = new CellRangeAddress(rowNo1, rowNo1, 4, 6);
        customerSheet.addMergedRegion(r15);
        PoiExcelUtils.setRegionBorder(r15, customerSheet);

        int rowNo2 = begin1 + 2;
        Row endRow = customerSheet.createRow(rowNo2);
        endRow.setHeightInPoints(20);
        Cell er1 = endRow.createCell(0);
        er1.setCellValue("开票内容");
        er1.setCellStyle(frontCenter);
        CellRangeAddress r16 = new CellRangeAddress(rowNo2, rowNo2, 0, 1);
        customerSheet.addMergedRegion(r16);
        PoiExcelUtils.setRegionBorder(r10, customerSheet);
        Cell er3 = endRow.createCell(2);
        er3.setCellValue("维修费");
        er3.setCellStyle(frontCenter);
        CellRangeAddress r17 = new CellRangeAddress(rowNo2, rowNo2, 2, 6);
        customerSheet.addMergedRegion(r17);
        PoiExcelUtils.setRegionBorder(r17, customerSheet);

        int rowNo3 = begin1 + 3;
        Row d1 = customerSheet.createRow(rowNo3);
        d1.setHeightInPoints(20);
        Cell cd10 = d1.createCell(0);
        cd10.setCellStyle(backgroundGray);
        Cell cd11 = d1.createCell(1);
        cd11.setCellStyle(backgroundGray);
        Cell cd12 = d1.createCell(2);
        cd12.setCellStyle(backgroundGray);
        Cell cd13 = d1.createCell(3);
        cd13.setCellStyle(backgroundGray);
        Cell cd14 = d1.createCell(4);
        cd14.setCellStyle(backgroundGray);
        Cell cd15 = d1.createCell(5);
        cd15.setCellStyle(backgroundGray);
        Cell cd16 = d1.createCell(6);
        cd16.setCellStyle(backgroundGray);

        int rowNo4 = begin1 + 4;
        Row remarkRow = customerSheet.createRow(rowNo4);
        remarkRow.setHeightInPoints(20);
        Cell cr0 = remarkRow.createCell(0);
        cr0.setCellValue("备注：");
        cr0.setCellStyle(frontLeftBack);
        CellRangeAddress r18 = new CellRangeAddress(rowNo4, rowNo4, 0, 6);
        customerSheet.addMergedRegion(r18);
        PoiExcelUtils.setRegionBorder(r18, customerSheet);

        int rowNo5 = begin1 + 5;
        Row companyRow = customerSheet.createRow(rowNo5);
        companyRow.setHeightInPoints(20);
        Cell cc0 = companyRow.createCell(0);
        cc0.setCellValue("报价单位：南方电网通用航空服务有限公司");
        cc0.setCellStyle(frontRightGreen);
        CellRangeAddress r19 = new CellRangeAddress(rowNo5, rowNo5, 0, 6);
        customerSheet.addMergedRegion(r19);
        PoiExcelUtils.setRegionBorder(r19, customerSheet);

        int rowNo6 = begin1 + 6;
        Row dateRow = customerSheet.createRow(rowNo6);
        dateRow.setHeightInPoints(20);
        Cell cd0 = dateRow.createCell(0);
        cd0.setCellValue("日期：".concat(DateUtils.formatDateChinese(System.currentTimeMillis())));
        cd0.setCellStyle(frontRight);
        CellRangeAddress r20 = new CellRangeAddress(rowNo6, rowNo6, 0, 6);
        customerSheet.addMergedRegion(r20);
        PoiExcelUtils.setRegionBorder(r20, customerSheet);
        customerSheet.setColumnWidth(0, 15 * 256);
        customerSheet.setColumnWidth(1, 40 * 256);
        customerSheet.setColumnWidth(2, 8 * 256);
        customerSheet.setColumnWidth(3, 8 * 256);
        customerSheet.setColumnWidth(4, 12 * 256);
        customerSheet.setColumnWidth(5, 12 * 256);
        customerSheet.setColumnWidth(6, 22 * 256);
    }

    /**
     * 写备件明细 快递费 其他费用 标准价格 总计
     *
     * @param costSheet
     * @param firstRow
     * @param orderQuote
     * @param orderQuoteItems
     * @param frontCenterBackGround
     * @param frontLeft
     * @param frontCenter
     */
    public void writeCostQuoteItem(Sheet costSheet, Row firstRow, int begin, OrderQuote orderQuote, List<OrderQuoteItem> orderQuoteItems, CellStyle frontCenterBackGround, CellStyle frontLeft, CellStyle frontCenter, CellStyle frontRight, boolean isFirstQuote) {
        int itemSize = orderQuoteItems.size();
        for (int i1 = 0; i1 < itemSize; i1++) {
            OrderQuoteItem quoteItem = orderQuoteItems.get(i1);
            //第一行已经创建了就不需要重复创建
            Row itemRow;
            if (i1 == 0) {
                itemRow = firstRow;
            } else {
                itemRow = costSheet.createRow(begin + i1);
                itemRow.setHeightInPoints(20);
            }
            Cell c1 = itemRow.createCell(1);
            c1.setCellValue(i1 + 1);
            if (isFirstQuote) {
                c1.setCellStyle(frontCenterBackGround);
            } else {
                c1.setCellStyle(frontCenter);
            }
            Cell c2 = itemRow.createCell(2);
            Product product = productRepository.findByPid(quoteItem.getPid());
            c2.setCellValue(product.getName());
            c2.setCellStyle(frontLeft);
            Cell c3 = itemRow.createCell(3);
            c3.setCellValue("套");
            c3.setCellStyle(frontCenter);
            Cell c4 = itemRow.createCell(4);
            c4.setCellValue(quoteItem.getCount());
            c4.setCellStyle(frontCenter);
            Cell c5 = itemRow.createCell(5);
            c5.setCellValue(quoteItem.getSalePrice());
            c5.setCellStyle(frontCenter);
            Cell c6 = itemRow.createCell(6);
            c6.setCellValue(quoteItem.getWorkHourCost());
            c6.setCellStyle(frontCenter);
            Cell c7 = itemRow.createCell(7);
            c7.setCellValue(quoteItem.getCount() * (quoteItem.getWorkHourCost() + quoteItem.getSalePrice()));
            c7.setCellStyle(frontCenter);
        }
        int b1 = begin + itemSize;
        Row expressRow = costSheet.createRow(b1);
        expressRow.setHeightInPoints(20);
        Cell er1 = expressRow.createCell(1);
        er1.setCellValue("快递费");
        er1.setCellStyle(frontCenter);
        Cell er2 = expressRow.createCell(2);
        er2.setCellValue("快递");
        er2.setCellStyle(frontCenter);
        CellRangeAddress r9 = new CellRangeAddress(b1, b1, 2, 6);
        costSheet.addMergedRegion(r9);
        PoiExcelUtils.setRegionBorder(r9, costSheet);
        Cell er7 = expressRow.createCell(7);
        er7.setCellValue(CommonConstants.MONEY.concat(String.valueOf(orderQuote.getExpressesPrice())));
        er7.setCellStyle(frontCenter);

        Row otherRow = costSheet.createRow(b1 + 1);
        otherRow.setHeightInPoints(20);
        Cell or1 = otherRow.createCell(1);
        or1.setCellValue("其它费用");
        or1.setCellStyle(frontCenter);
        Cell or2 = otherRow.createCell(2);
        or2.setCellValue(orderQuote.getOtherRemark());
        or2.setCellStyle(frontCenter);
        CellRangeAddress r10 = new CellRangeAddress(b1 + 1, b1 + 1, 2, 6);
        costSheet.addMergedRegion(r10);
        PoiExcelUtils.setRegionBorder(r10, costSheet);
        Cell or7 = otherRow.createCell(7);
        or7.setCellValue(CommonConstants.MONEY.concat(String.valueOf(orderQuote.getOtherPrice())));
        or7.setCellStyle(frontCenter);

        Row standardRow = costSheet.createRow(b1 + 2);
        standardRow.setHeightInPoints(20);
        Cell s1 = standardRow.createCell(1);
        s1.setCellValue("标准价格");
        s1.setCellStyle(frontCenter);
        CellRangeAddress r11 = new CellRangeAddress(b1 + 2, b1 + 2, 1, 2);
        costSheet.addMergedRegion(r11);
        PoiExcelUtils.setRegionBorder(r11, costSheet);
        Cell s3 = standardRow.createCell(3);
        double total = Optional.ofNullable(orderQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(orderQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(orderQuote.getOtherPrice()).orElse(0.00);
        s3.setCellValue(CommonConstants.MONEY.concat(String.valueOf(total)));
        s3.setCellStyle(frontCenter);
        CellRangeAddress r12 = new CellRangeAddress(b1 + 2, b1 + 2, 3, 7);
        costSheet.addMergedRegion(r12);
        PoiExcelUtils.setRegionBorder(r12, costSheet);

        Row totalRow = costSheet.createRow(b1 + 3);
        totalRow.setHeightInPoints(20);
        Cell t1 = totalRow.createCell(1);
        t1.setCellValue("总计");
        t1.setCellStyle(frontCenter);
        CellRangeAddress r13 = new CellRangeAddress(b1 + 3, b1 + 3, 1, 2);
        costSheet.addMergedRegion(r13);
        PoiExcelUtils.setRegionBorder(r13, costSheet);
        Cell t3 = totalRow.createCell(3);
        t3.setCellValue("人民币：");
        t3.setCellStyle(frontRight);
        CellRangeAddress r14 = new CellRangeAddress(b1 + 3, b1 + 3, 3, 4);
        costSheet.addMergedRegion(r14);
        PoiExcelUtils.setRegionBorder(r14, costSheet);
        Cell t5 = totalRow.createCell(5);
        t5.setCellValue(CommonUtils.toChineseMoney(new BigDecimal(total)));
        t5.setCellStyle(frontLeft);
        CellRangeAddress r15 = new CellRangeAddress(b1 + 3, b1 + 3, 5, 7);
        costSheet.addMergedRegion(r15);
        PoiExcelUtils.setRegionBorder(r15, costSheet);
    }

    /**
     * 保存工单文件
     *
     * @param orderId
     * @param request
     */
    public void saveOrderFileByLogId(String orderId, String orderLogId, FilesRequest request) throws Exception {
        this.saveOrderFile(orderId, orderLogId, null, request.getFile0());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile1());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile2());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile3());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile4());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile5());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile6());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile7());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile8());
        this.saveOrderFile(orderId, orderLogId, null, request.getFile9());
    }

    /**
     * 保存工单文件
     *
     * @param orderId
     * @param request
     */
    public void saveOrderFileByType(String orderId, String fileType, FilesRequest request) throws Exception {
        this.saveOrderFile(orderId, null, fileType, request.getFile0());
        this.saveOrderFile(orderId, null, fileType, request.getFile1());
        this.saveOrderFile(orderId, null, fileType, request.getFile2());
        this.saveOrderFile(orderId, null, fileType, request.getFile3());
        this.saveOrderFile(orderId, null, fileType, request.getFile4());
        this.saveOrderFile(orderId, null, fileType, request.getFile5());
        this.saveOrderFile(orderId, null, fileType, request.getFile6());
        this.saveOrderFile(orderId, null, fileType, request.getFile7());
        this.saveOrderFile(orderId, null, fileType, request.getFile8());
        this.saveOrderFile(orderId, null, fileType, request.getFile9());
    }

    /**
     * 保存工单文件
     * 查询文件通过工单+类型或者通过工单日志id查询
     *
     * @param orderId
     */
    public void saveOrderFile(String orderId, String orderLogId, String fileType, MultipartFile f) throws Exception {
        if (Objects.isNull(f)) {
            return;
        }
        CustomFile customFile = new CustomFile();
        customFile.setFid(CommonUtils.uuid());
        customFile.setOrdid(orderId);
        customFile.setSize(f.getSize());
        customFile.setName(f.getName());
        customFile.setExtension(CommonUtils.getExtension(customFile.getName()));
        customFile.setDownload(Boolean.TRUE);
        customFile.setDirectory(OrderConstants.ORDER_BUCKET);
        customFile.setMime(f.getContentType());
        customFile.setType(fileType);
        customFile.setOrdlogid(orderLogId);
        String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
        customMinIOClient.uploadFile(OrderConstants.ORDER_BUCKET, objectName, f.getInputStream());
        fileRepository.save(customFile);
    }


    /**
     * 保存工单日志
     */
    public OrderLog saveOrderLog(Order order, String method) {
        OrderLog orderLog = new OrderLog();
        //表示创建工单
        StringBuilder sb = new StringBuilder();
        if (OrderConstants.ORDER_LOG_METHOD_CREATE.equals(method)) {
            if (ORIGIN_WEB_SYSTEM.equals(order.getOrigin())) {
                sb.append("后台系统创建");
            }
            if (OrderConstants.ORDER_MAINTAIN.equals(order.getType())) {
                sb.append("维修");
            } else if (OrderConstants.ORDER_UPKEEP.equals(order.getType())) {
                sb.append("保养");
            }
            if (OrderConstants.ORDER_IN_INSURANCE.equals(order.getServiceType())) {
                sb.append("【保内维修】工单");
            } else if (OrderConstants.ORDER_OUT_INSURANCE.equals(order.getServiceType())) {
                sb.append("【框架合同维修】工单");
            } else if (OrderConstants.ORDER_SERVICE_QUICKLY.equals(order.getServiceType())) {
                sb.append("【快修快换】工单");
            } else if (OrderConstants.ORDER_SERVICE_INVALID.equals(order.getServiceType())) {
                sb.append("【作废】工单");
            }
            orderLog.setRemark(orderLog.getRemark());
        } else if (OrderConstants.ORDER_LOG_METHOD_CONFIRM_EXPRESSES.equals(method)) {//确认收件
            Expresses expresses = expressesRepository.findFirstByWaybillNoOrderByCtDesc(order.getReceiveNo());
            sb.append("收件单号【");
            sb.append(expresses.getWaybillNo()).append("】");
            sb.append("、收件时间【");
            sb.append(DateUtils.formatDateTime(expresses.getExpressesAt())).append("】");
            orderLog.setRemark(expresses.getRemark());
        } else if (OrderConstants.ORDER_LOG_METHOD_CONFIRM_DEVICE.equals(method)) {//设备确认
            sb.append("手动确认");
        } else if (OrderConstants.ORDER_LOG_METHOD_REVIEW_INSURANCE.equals(method)) {//保险资料审核
            sb.append("工单类型");
            if (OrderConstants.ORDER_MAINTAIN.equals(order.getType())) {
                sb.append("【维修】");
            } else if (OrderConstants.ORDER_UPKEEP.equals(order.getType())) {
                sb.append("【保养】");
            }
            sb.append("、服务类型");
            if (OrderConstants.ORDER_IN_INSURANCE.equals(order.getServiceType())) {
                sb.append("【保内维修】");
            } else if (OrderConstants.ORDER_OUT_INSURANCE.equals(order.getServiceType())) {
                sb.append("【保外维修】");
            }
            sb.append("、项目");
            Project project = projectRepository.findByPjid(order.getPjid());
            if (Objects.nonNull(project)) {
                sb.append("【").append(project.getName()).append("】");
            }
            orderLog.setRemark(order.getReviewInsuranceRemark());
        } else if (OrderConstants.ORDER_LOG_METHOD_CONFIRM_LOSS.equals(method)) {//定损
            sb.append("服务方式");
            if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
                sb.append("【本地维修】");
            } else if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {
                sb.append("【返厂维修】");
            } else if (OrderConstants.ORDER_SERVICE_METHOD_NEW_MAINTAIN.equals(order.getServiceMethod())) {
                sb.append("【质保更新】");
            }
            sb.append("、项目");
            Project project = projectRepository.findByPjid(order.getPjid());
            if (Objects.nonNull(project)) {
                sb.append("【").append(project.getName()).append("】");
            }
            if (StringUtils.isNotBlank(order.getLossRemark())) {
                sb.append("、备注【").append(order.getLossRemark()).append("】");
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_WAIT_LOSS_AUDIT.equals(method)) {//定损审核
            //表示审核未通过
            if (ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode().equals(order.getStatus())) {
                sb.append("审核未通过");
            } else {
                sb.append("审核通过");
            }
            if (StringUtils.isNotBlank(order.getReviewLossRemark())) {
                sb.append("【").append(order.getReviewLossRemark()).append("】");
            }
            orderLog.setRemark(order.getReviewLossRemark());
        } else if (OrderConstants.ORDER_LOG_METHOD_RETURN_EXPRESSES.equals(method)) {//返厂寄件
            sb.append("返厂案例号").append(order.getReturnCaseNumber()).append(" ");
            Expresses expresses = expressesRepository.findFirstByOrdidAndType(order.getOrdid(), CommonConstants.EXPRESSES_RETURN_FACTORY);
            if (Objects.nonNull(expresses)) {
                sb.append("、邮寄方式");
                if (OrderConstants.EXPRESSES_SELF.equals(expresses.getPickup())) {
                    sb.append("【快递自寄】");
                } else {
                    sb.append("【上门取件】");
                }
                if (StringUtils.isNotBlank(expresses.getWaybillNo())) {
                    sb.append("、快递单号【");
                    sb.append(expresses.getWaybillNo()).append("】");
                }
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_QUOTATION.equals(method)) {
            OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), OrderConstants.QUOTE_TYPE_CUSTOMER);
            AtomicReference<Double> totalAmt = new AtomicReference<>(0.0);
            Optional.ofNullable(customerQuote.getSparePrice()).ifPresent(x -> totalAmt.set(totalAmt.get() + x));
            Optional.ofNullable(customerQuote.getExpressesPrice()).ifPresent(x -> totalAmt.set(totalAmt.get() + x));
            Optional.ofNullable(customerQuote.getOtherPrice()).ifPresent(x -> totalAmt.set(totalAmt.get() + x));
            sb.append("客户报价【").append(totalAmt.get()).append("】");
            if (StringUtils.isNotBlank(order.getQuoteRemark())) {
                sb.append("、备注【").append(order.getQuoteRemark()).append("】");
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_QUOTATION_AUDIT.equals(method)) {
            if (CommonUtils.checkStatus(order.getStatus(), ORDER_STATUS_WAITING_QUOTATION_CONFIRM.getCode(), ORDER_STATUS_WAITING_DISPATCH.getCode(), ORDER_STATUS_WAITING_SETTLE.getCode(), ORDER_STATUS_RETURN_WAITING_RECEIVE.getCode())) {
                sb.append("审核通过");
                OrderLog log = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_QUOTATION_AUDIT);
                if (Objects.isNull(log)) {
                    if (order.getBeSettle()) {
                        sb.append("【需要收费】");
                    } else {
                        sb.append("【不需要收费】");
                    }
                }
                orderLog.setRemark(PASS);
            } else {
                sb.append("审核不通过");
                orderLog.setRemark(REJECTED);
            }
            if (StringUtils.isNotBlank(order.getReviewQuoteRemark())) {
                sb.append("、备注【").append(order.getReviewQuoteRemark()).append("】");
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_DISPATCH.equals(method)) {
            sb.append("派单给维修工程师");
            User user = userRepository.findByUid(order.getMaintainerUid());
            if (Objects.nonNull(user)) {
                sb.append(user.getUserName());
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_MAINTAIN_FEEDBACK.equals(method)) {
            if (StringUtils.isBlank(order.getMaintainFeedback())) {
                sb.append("维修反馈");
            } else {
                sb.append(order.getMaintainFeedback());
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_MAINTAINED.equals(method)) {
            OrderLog dispatchLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), OrderConstants.ORDER_LOG_METHOD_DISPATCH);
            long delta = System.currentTimeMillis() - dispatchLog.getCt();
            long hours = delta / 60000 + 1;
            sb.append("维修总用时【").append(hours).append("】分钟");
            if (StringUtils.isNotBlank(order.getMaintainResult())) {
                sb.append("、备注【").append(order.getMaintainResult()).append("】");
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_MAINTAIN_FAIL.equals(method)) {
            if (StringUtils.isBlank(order.getMaintainBack())) {
                sb.append("维修回退");
            } else {
                sb.append(order.getMaintainBack());
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_CHECKED.equals(method)) {
            if (ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode().equals(order.getStatus())) {
                orderLog.setRemark(PASS);
                sb.append("检修通过");
                if (StringUtils.isNotBlank(order.getCheckRemark())) {
                    sb.append("【").append(order.getCheckRemark()).append("】");
                }
            } else {
                orderLog.setRemark(REJECTED);
                sb.append("检修不通过【").append(order.getCheckRemark()).append("】");
            }
        } else if (OrderConstants.ORDER_LOG_METHOD_CUSTOMER_EXPRESSES.equals(method)) {
            sb.append("寄件方式");
            Expresses expresses = expressesRepository.findFirstByOrdidAndType(order.getOrdid(), CommonConstants.EXPRESSES_SEND_CUSTOMER);
            if (Objects.isNull(expresses)) {
                sb.append("【上门自提】");
            } else {
                if (OrderConstants.EXPRESSES_SELF.equals(expresses.getPickup())) {
                    sb.append("【快递自寄】");
                } else {
                    sb.append("【快递上门】");
                }
                sb.append("、快递单号").append(expresses.getWaybillNo());
            }
        } else if (ORDER_LOG_METHOD_REVIEW_CANCEL.equals(method)) {
            sb.append("审核工单取消");
            if (StringUtils.isNotBlank(order.getReviewCancelRemark())) {
                sb.append("【").append(order.getReviewCancelRemark()).append("】");
            }
            orderLog.setRemark(order.getReviewCancelRemark());
        } else if (ORDER_LOG_METHOD_CANCEL.equals(method)) {
            sb.append("工单取消");
            if (StringUtils.isNotBlank(order.getCancelReason())) {
                sb.append("【").append(order.getCancelReason()).append("】");
            }
            orderLog.setRemark(order.getCancelReason());
        } else if (ORDER_LOG_METHOD_CONFIRM_QUOTATION.equals(method)) {
            sb.append("客户已确认");
        } else if (ORDER_LOG_METHOD_RETURN_RECEIVE.equals(method)) {
            sb.append("返厂维修，已收件");
        } else if (ORDER_LOG_METHOD_SETTLE.equals(method) || ORDER_LOG_METHOD_SETTLE_SUBMIT.equals(method)) {
            if (Objects.nonNull(order.getBeSettle()) && order.getBeSettle()) {
                OrderSettle orderSettle = orderSettleRepository.findFirstByOrdidAndType(order.getOrdid(), SETTLE_TYPE_CUSTOMER);
                if (Objects.nonNull(orderSettle)) {
                    sb.append("到款时间【").append(DateUtils.formatDateTime(orderSettle.getReceiveAt()))
                            .append("】").append("、开票金额【").append(orderSettle.getInvoiceAmount()).append("】");
                }
            } else {
                sb.append("无客户费用");
            }
        } else if (ORDER_LOG_METHOD_REVOCATION.equals(method)) {
            sb.append("用户撤回取消");
        } else if (ORDER_LOG_METHOD_REVIEW_REVOCATION.equals(method)) {
            sb.append("审核撤回取消");
            if (Objects.isNull(order.getCancelType())) {
                sb.append("通过");
            } else {
                sb.append("不通过");
            }
        } else if (ORDER_LOG_METHOD_CONFIRM_RE_LOSS.equals(method)) {
            sb.append("维修失败，重新定损");
        } else if (ORDER_LOG_METHOD_REVIEW_SCRAP.equals(method)) {
            if (ORDER_STATUS_FINISH.getCode().equals(order.getStatus())) {
                sb.append("审核通过");
            } else {
                sb.append("审核不通过");
            }
        } else if (ORDER_LOG_METHOD_HANDLE_SCRAP.equals(method)) {
            sb.append("报废工单处理，服务方式");
            if (OrderConstants.ORDER_IN_INSURANCE.equals(order.getServiceType())) {
                sb.append("【保内维修】");
            } else if (OrderConstants.ORDER_OUT_INSURANCE.equals(order.getServiceType())) {
                sb.append("【框架合同维修】");
            } else if (OrderConstants.ORDER_SERVICE_QUICKLY.equals(order.getServiceType())) {
                sb.append("【快修快换】");
            } else if (OrderConstants.ORDER_SERVICE_INVALID.equals(order.getServiceType())) {
                sb.append("【作废】");
            }
        } else if (ORDER_LOG_METHOD_BACK_INSURANCE.equals(method)) {
            sb.append("保险回退【").append(order.getBackInsuranceRemark()).append("】");
        } else if (INSURANCE_ARCHIVED.equals(method)) {
            sb.append("理赔保险资料归档");
        }
        orderLog.setDescription(sb.toString());
        orderLog.setMethod(method);
        orderLog.setUid(CommonUtils.getUid());
        orderLog.setOrdid(order.getOrdid());
        orderLog.setStatus(order.getStatus());
        orderLog.setOrdlogid(CommonUtils.uuid());
        return orderLogRepository.save(orderLog);
    }

    /**
     * 根据工单创建的设备创建保单
     *
     * @param order
     */
    public Insurance createInsurance(Order order) {
        String uid = CommonUtils.getUid();
        String devid = order.getDevid();
        Device device = deviceRepository.findByDevid(devid);
        //用户自己选择的鼎和保险则需要创建保单
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            Insurance insurance = insuranceRepository.findByInsuranceCode(device.getInsuranceCode());
            if (Objects.isNull(insurance)) {
                insurance = new Insurance();
                insurance.setIid(CommonUtils.uuid());
                insurance.setInsuranceType(device.getInsuranceType());
                insurance.setInsuranceCode(device.getInsuranceCode());
                insurance.setInsuranceStartAt(device.getInsuranceStartAt());
                insurance.setInsuranceExpireAt(device.getInsuranceExpireAt());
                insurance.setTotalAmount(BigDecimal.valueOf(device.getInsuranceTotalQuota()));
                insurance.setUid(uid);
                insurance.setCid(device.getCid());
                insuranceRepository.save(insurance);
                InsuranceDevice insuranceDevice = new InsuranceDevice();
                insuranceDevice.setCode(device.getCode());
                insuranceDevice.setRegistrationNumber(device.getRegistrationNumber());
                insuranceDevice.setUid(uid);
                insuranceDevice.setAmount(BigDecimal.valueOf(device.getQuota()));
                insuranceDevice.setIdid(CommonUtils.uuid());
                insuranceDevice.setIid(insurance.getIid());
                insuranceDeviceRepository.save(insuranceDevice);
            }
            return insurance;
        }
        return null;
    }

    /**
     * 创建地址如果不存在
     * //返厂寄件
     *
     * @param v
     * @return
     */
    public Address systemIfNotPresent(ExpressesAddress v) {
        Address address = addressRepository.findByCidIsNullAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(v.getTelphone(), v.getContact(), v.getProvince(), v.getCity(), v.getCounty(), v.getAddress());
        if (Objects.isNull(address)) {
            address = new Address();
            address.setProvince(v.getProvince());
            address.setCity(v.getCity());
            address.setCounty(v.getCounty());
            address.setDetail(v.getAddress());
            address.setAid(CommonUtils.uuid());
            address.setContactTel(v.getTelphone());
            address.setContactUser(v.getContact());
            address.setIsDefault(Boolean.FALSE);
            addressRepository.save(address);
        }
        return address;
    }

    /**
     * 创建地址如果不存在
     * //客户寄件
     *
     * @param v
     * @return
     */
    public Address customerIfNotPresent(String uid, ExpressesAddress v) {
        User user = userRepository.findByUid(uid);
        Address address = addressRepository.findByCidAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(user.getCid(), v.getTelphone(), v.getContact(), v.getProvince(), v.getCity(), v.getCounty(), v.getAddress());
        if (Objects.isNull(address)) {
            address = new Address();
            address.setProvince(v.getProvince());
            address.setCity(v.getCity());
            address.setCounty(v.getCounty());
            address.setDetail(v.getAddress());
            address.setAid(CommonUtils.uuid());
            address.setContactTel(v.getTelphone());
            address.setContactUser(v.getContact());
            address.setIsDefault(Boolean.FALSE);
            addressRepository.save(address);
        }
        return address;
    }

    /**
     * 生成工单二维码
     */
    public void generateQrCode(Order order) throws Exception {
        QrConfig qrConfig = new QrConfig();
        qrConfig.setBackColor(java.awt.Color.WHITE);
        qrConfig.setForeColor(java.awt.Color.BLACK);
        qrConfig.setWidth(OrderConstants.QR_COED_WIDTH);
        qrConfig.setHeight(OrderConstants.QR_COED_HEIGHT);
        BufferedImage codeImage = QrCodeUtil.generate(order.getInnerCode(), qrConfig);
        //设置文本图片宽高
        BufferedImage textImage = CommonUtils.strToImage(order.getInnerCode(), OrderConstants.QR_COED_WIDTH, OrderConstants.QR_FONT_HEIGHT);
        Graphics2D graph = codeImage.createGraphics();
        //开启文字抗锯齿
        graph.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int width = textImage.getWidth(null);
        int height = textImage.getHeight(null);
        //画图 文字图片最终显示位置 在Y轴偏移量 从上往下算
        graph.drawImage(textImage, 0, OrderConstants.QR_COED_WIDTH, width, height, null);
        graph.dispose();
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ImageIO.write(codeImage, ContentType.PNG.getSuffix(), outStream);
        byte[] bytes = outStream.toByteArray();
        InputStream is = new ByteArrayInputStream(bytes);
        CustomFile customFile = new CustomFile();
        customFile.setFid(CommonUtils.uuid());
        customFile.setOrdid(order.getOrdid());
        customFile.setSize((long) bytes.length);
        customFile.setName(order.getOrdid().concat(CommonConstants.DOT).concat(ContentType.PNG.getSuffix()));
        customFile.setExtension(CommonUtils.getExtension(customFile.getName()));
        customFile.setDownload(Boolean.TRUE);
        customFile.setDirectory(OrderConstants.ORDER_BUCKET);
        customFile.setMime(ContentType.PNG.getType());
        customFile.setType(CommonConstants.FILE_TYPE_QR_CODE);
        String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
        customMinIOClient.uploadFile(OrderConstants.ORDER_BUCKET, objectName, is);
        fileRepository.save(customFile);
    }

    /**
     * 获取文件的访问地址
     *
     * @param customFile
     * @return
     */
    public String getFileProxyUrl(CustomFile customFile) {
        try {
            if (Objects.isNull(customFile)) {
                return StringUtils.EMPTY;
            }
            String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
            return customMinIOClient.getProxyUrl(customFile.getDirectory(), objectName);
        } catch (Exception e) {
            log.error("getFileProxyUrl error", e);
        }
        return StringUtils.EMPTY;
    }

    /**
     * 获取文件的访问地址
     *
     * @param customFile
     * @return
     */
    public FileVO convertFile2VO(CustomFile customFile) {
        try {
            if (Objects.isNull(customFile)) {
                return null;
            }
            FileVO fileVO = new FileVO();
            String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
            String url = customMinIOClient.getProxyUrl(customFile.getDirectory(), objectName);
            fileVO.setUrl(url);
            fileVO.setName(customFile.getName());
            fileVO.setCreateTime(DateUtils.formatDateTime(customFile.getCt()));
            fileVO.setFid(customFile.getFid());
            fileVO.setDownload(customFile.getDownload());
            return fileVO;
        } catch (Exception e) {
            log.error("getFileProxyUrl error", e);
        }
        return null;
    }

    /**
     * @param order
     * @param request
     */
    public void saveFirstInsuranceMaterial(Order order, FirstInsurance request) {
        //飞手证书
        if (StringUtils.isNotBlank(request.getPilotCertificate())) {
            //删除之前的
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_PILOT_CERT);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            CustomFile customFile = fileRepository.findByFid(request.getPilotCertificate());
            if (Objects.nonNull(customFile)) {
                String name = Objects.isNull(customFile.getExtension()) ? "飞手证书.png" : "飞手证书.".concat(customFile.getExtension());
                customFile.setName(name);
                customFile.setOrdid(order.getOrdid());
                customFile.setType(CommonConstants.FILE_TYPE_PILOT_CERT);
                fileRepository.save(customFile);
            }
        }
        //固定资产卡片
        if (StringUtils.isNotBlank(request.getFixedAsset())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_FIXED_ASSET);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            CustomFile customFile = fileRepository.findByFid(request.getFixedAsset());
            if (Objects.nonNull(customFile)) {
                String name = Objects.isNull(customFile.getExtension()) ? "固定资产卡片.png" : "固定资产卡片.".concat(customFile.getExtension());
                customFile.setName(name);
                customFile.setOrdid(order.getOrdid());
                customFile.setType(CommonConstants.FILE_TYPE_FIXED_ASSET);
                fileRepository.save(customFile);
            }
        }
        //工作巡视单
        if (StringUtils.isNotBlank(request.getLineWalkingPicture())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_LINE_WALK);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            CustomFile customFile = fileRepository.findByFid(request.getLineWalkingPicture());
            if (Objects.nonNull(customFile)) {
                String name = Objects.isNull(customFile.getExtension()) ? "工作巡视单.png" : "工作巡视单.".concat(customFile.getExtension());
                customFile.setName(name);
                customFile.setOrdid(order.getOrdid());
                customFile.setType(CommonConstants.FILE_TYPE_LINE_WALK);
                fileRepository.save(customFile);
            }
        }
        //情况说明
        if (StringUtils.isNotBlank(request.getPresentation())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_PRESENTATION);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            CustomFile customFile = fileRepository.findByFid(request.getPresentation());
            if (Objects.nonNull(customFile)) {
                customFile.setName("情况说明.png");
                String name = Objects.isNull(customFile.getExtension()) ? "情况说明.png" : "情况说明.".concat(customFile.getExtension());
                customFile.setName(name);
                customFile.setOrdid(order.getOrdid());
                customFile.setType(CommonConstants.FILE_TYPE_PRESENTATION);
                fileRepository.save(customFile);
            }
        }
        //事故照片-事故现场
        if (CollectionUtils.isNotEmpty(request.getAccidentScene())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_ACCIDENT_SCENE);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            request.getAccidentScene().forEach(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_ACCIDENT_SCENE);
                    fileRepository.save(customFile);
                }
            });
        }
        //事故照片-损坏物品清单
        if (CollectionUtils.isNotEmpty(request.getAccidentDamageItems())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_ACCIDENT_DAMAGE_ITEMS);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            request.getAccidentDamageItems().forEach(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_ITEMS);
                    fileRepository.save(customFile);
                }
            });
        }
        //事故照片-损坏设备
        if (CollectionUtils.isNotEmpty(request.getAccidentDamageDevices())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_ACCIDENT_DAMAGE_DEVICES);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            request.getAccidentDamageDevices().forEach(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_DEVICES);
                    fileRepository.save(customFile);
                }
            });
        }
        //事故照片-设备正面
        if (CollectionUtils.isNotEmpty(request.getDeviceFront())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_DEVICE_FRONT);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            request.getDeviceFront().forEach(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_DEVICE_FRONT);
                    fileRepository.save(customFile);
                }
            });
        }
        //事故照片-设备反面
        if (CollectionUtils.isNotEmpty(request.getDeviceReverse())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_DEVICE_REVERSE);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            request.getDeviceReverse().forEach(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_DEVICE_REVERSE);
                    fileRepository.save(customFile);
                }
            });
        }
        //事故照片-其他
        if (CollectionUtils.isNotEmpty(request.getAccidentOther())) {
            List<CustomFile> beforeFile = fileRepository.findByOrdidAndType(order.getOrdid(), FILE_TYPE_ACCIDENT_OTHER);
            if (CollectionUtils.isNotEmpty(beforeFile)) {
                beforeFile.forEach(x -> {
                    x.setOrdid(null);
                    x.setType(null);
                });
                fileRepository.saveAll(beforeFile);
            }
            request.getAccidentOther().forEach(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_ACCIDENT_OTHER);
                    fileRepository.save(customFile);
                }
            });
        }

    }

    /**
     * 更具角色不通 获取可以看到不通状态的工单
     *
     * @param user
     * @return
     */
    private List<String> getOrderStatusByUser(User user) {
        List<String> status = Lists.newArrayList();
        if (UserTypeEnum.USER.getCode().equals(user.getUserType())) {
            return status;
        }
        List<UserRole> userRole = userRoleRepository.findByUserId(user.getId());
        List<Integer> roleIds = userRole.stream().map(UserRole::getRoleId).collect(Collectors.toList());
        List<Role> roles = roleRepository.findAllByIdIn(roleIds);
        roles.forEach(x -> {
            //收寄件人员
            if (ROLE_RECEIVE.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAIT_EXPRESSES.getCode());
                status.add(ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode());
                status.add(ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode());
                status.add(ORDER_STATUS_RETURN_WAITING_EXPRESSES.getCode());
                status.add(ORDER_STATUS_RETURN_WAITING_RECEIVE.getCode());
                status.add(ORDER_STATUS_WAIT_SCRAP_HANDLE.getCode());
                status.add(ORDER_STATUS_FINISH.getCode());
            } else if (ROLE_CUSTOMER_SERVICE.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAIT_EXPRESSES.getCode());
                status.add(ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode());
                status.add(ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode());
                status.add(ORDER_STATUS_INVALIDATE.getCode());
                status.add(ORDER_STATUS_INSURANCE_MATERIAL_GATHER.getCode());
            } else if (ROLE_INSURANCE_AUDITOR.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
                status.add(ORDER_STATUS_CANCEL_WAITING1.getCode());
                status.add(ORDER_STATUS_CANCEL_WAITING2.getCode());
                status.add(ORDER_STATUS_FINISH.getCode());
                status.add(ORDER_STATUS_CANCEL.getCode());
            } else if (ROLE_LOSS_ENGINEER.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
                status.add(ORDER_STATUS_WAITING_QUOTATION.getCode());
                status.add(ORDER_STATUS_WAITING_DISPATCH.getCode());
                status.add(ORDER_STATUS_FINISH.getCode());
                status.add(ORDER_STATUS_CANCEL.getCode());
            } else if (ROLE_COMPREHENSIVE_STAFF.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAIT_LOSS_REVIEW.getCode());
                status.add(ORDER_STATUS_WAITING_QUOTATION_REVIEW.getCode());
                status.add(ORDER_STATUS_WAITING_QUOTATION_CONFIRM.getCode());
                status.add(ORDER_STATUS_WAITING_SETTLE.getCode());
                status.add(ORDER_STATUS_WAIT_SCRAP_REVIEW.getCode());
                status.add(ORDER_STATUS_FINISH.getCode());
                status.add(ORDER_STATUS_CANCEL.getCode());
            } else if (ROLE_MAINTENANCE_ENGINEER.equals(x.getCode())) {
                status.add(ORDER_STATUS_MAINTAINING.getCode());
            } else if (ROLE_TEST_ENGINEER.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAITING_CHECK.getCode());
            } else if (ROLE_SENDER.equals(x.getCode())) {
                status.add(ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode());
            } else {
                for (OrderStatusEnums value : OrderStatusEnums.values()) {
                    status.add(value.getCode());
                }
            }
        });
        return status;
    }

    /**
     * 分页查询
     *
     * @param request
     * @return
     */
    public Specification<Order> orderPageSpecification(PageOrderQuery request) {
        String uid = CommonUtils.getUid();
        String cpid = CommonUtils.getCpid();
        User user = userRepository.findByUid(uid);
        List<String> status = this.getOrderStatusByUser(user);
        Specification<Order> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> listAnd = new ArrayList<>();
            if (Objects.nonNull(request.getStartTime())) {
                listAnd.add(criteriaBuilder.greaterThan(root.get("ut"), request.getStartTime()));
            }
            if (Objects.nonNull(request.getEndTime())) {
                listAnd.add(criteriaBuilder.lessThan(root.get("ut"), request.getEndTime()));
            }
            if (StringUtils.isNotBlank(request.getInnerCode())) {
                listAnd.add(criteriaBuilder.like(root.get("innerCode"), CommonUtils.assembleLike(request.getInnerCode())));
            }
            if (StringUtils.isNotBlank(request.getStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
            }
            if (StringUtils.isNotBlank(request.getServiceType())) {
                listAnd.add(criteriaBuilder.equal(root.get("serviceType"), request.getServiceType()));
            }
            if (StringUtils.isNotBlank(request.getServiceMethod())) {
                listAnd.add(criteriaBuilder.equal(root.get("serviceMethod"), request.getServiceMethod()));
            }
            if (StringUtils.isNotBlank(request.getReviewInsuranceStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("reviewInsuranceStatus"), request.getReviewInsuranceStatus()));
            }
            if (StringUtils.isNotBlank(request.getCname())) {
                Join<Order, Customer> join = root.join("customer", JoinType.LEFT);
                listAnd.add(criteriaBuilder.like(join.get("name"), CommonUtils.assembleLike(request.getCname())));
            }

            if (StringUtils.isNotBlank(request.getContact())) {
                Join<Order, User> join = root.join("cuser", JoinType.LEFT);
                listAnd.add(criteriaBuilder.like(join.get("userName"), CommonUtils.assembleLike(request.getContact())));
            }
            if (StringUtils.isNotBlank(request.getCatid()) || StringUtils.isNotBlank(request.getCode()) || StringUtils.isNotBlank(request.getRemoteCode()) || StringUtils.isNotBlank(request.getBatteryCode())) {
                Join<Order, Device> join1 = root.join("device", JoinType.LEFT);
                if (StringUtils.isNotBlank(request.getCode())) {
                    listAnd.add(criteriaBuilder.like(join1.get("code"), CommonUtils.assembleLike(request.getCode())));
                }
                if (StringUtils.isNotBlank(request.getRemoteCode())) {
                    listAnd.add(criteriaBuilder.like(join1.get("remoteCode"), CommonUtils.assembleLike(request.getRemoteCode())));
                }
                if (StringUtils.isNotBlank(request.getBatteryCode())) {
                    listAnd.add(criteriaBuilder.like(join1.get("batteryCode"), CommonUtils.assembleLike(request.getBatteryCode())));
                }
                if (StringUtils.isNotBlank(request.getCatid())) {
                    Join<Device, Product> join2 = join1.join("product", JoinType.LEFT);
                    listAnd.add(criteriaBuilder.equal(join2.get("catid"), CommonUtils.assembleLike(request.getCatid())));
                }
            }
            if (StringUtils.isNotBlank(request.getMaintainerName())) {
                Join<Order, User> join = root.join("maintainerUser", JoinType.LEFT);
                listAnd.add(criteriaBuilder.equal(join.get("userName"), request.getMaintainerName()));
            }
            if (StringUtils.isNotBlank(request.getReturnCaseNumber())) {
                listAnd.add(criteriaBuilder.like(root.get("returnCaseNumber"), CommonUtils.assembleLike(request.getServiceMethod())));
            }
            if (StringUtils.isNotBlank(request.getReceiveNo())) {
                listAnd.add(criteriaBuilder.like(root.get("receiveNo"), CommonUtils.assembleLike(request.getReceiveNo())));
            }
            if (StringUtils.isNotBlank(request.getPjid())) {
                listAnd.add(criteriaBuilder.equal(root.get("pjid"), request.getPjid()));
            }
            if (StringUtils.isNotBlank(request.getPreStatus())) {
                listAnd.add(criteriaBuilder.equal(root.get("preStatus"), request.getPreStatus()));
            }
            if (UserTypeEnum.USER.getCode().equals(user.getUserType())) {
                listAnd.add(criteriaBuilder.equal(root.get("cuid"), uid));
                Predicate[] p1 = new Predicate[listAnd.size()];
                return criteriaBuilder.and(listAnd.toArray(p1));
            } else {
                List<String> actions = CommonUtils.getUserAuth();
                if (actions.contains(PERMISSION_ALL_ORDER) || CommonUtils.isAdmin()) {
                    Predicate[] p = new Predicate[listAnd.size()];
                    return criteriaBuilder.and(listAnd.toArray(p));
                }
                Predicate[] p = new Predicate[listAnd.size()];
                Predicate and = criteriaBuilder.and(listAnd.toArray(p));
                List<Predicate> listOr1 = new ArrayList<>();
                List<Predicate> listOr2 = new ArrayList<>();
                if (CollectionUtils.isNotEmpty(status)) {
                    CriteriaBuilder.In<Object> in = criteriaBuilder.in(root.get("status"));
                    in.value(status);
                    listOr1.add(in);
                }
                listOr2.add(criteriaBuilder.equal(root.get("uid"), uid));
                Predicate[] p1 = new Predicate[listOr1.size()];
                Predicate[] p2 = new Predicate[listOr2.size()];
                Predicate or1 = criteriaBuilder.or(listOr1.toArray(p1));
                Predicate or2 = criteriaBuilder.or(listOr2.toArray(p2));
                Predicate or = criteriaBuilder.or(or1, or2);
                return criteriaBuilder.and(or, and);
            }
        };
        return specification;
    }

    /**
     * 获取导出的一行数据
     *
     * @param order
     * @param fields
     * @param row
     */
    public void getOneLow(Order order, List<ExportCell> fields, List<Object> row) {
        Address address = addressRepository.getByAid(order.getAid());
        Device device = Optional.ofNullable(deviceRepository.findByDevid(order.getDevid())).orElse(new Device());
        OrderSettle customerSettle = orderSettleRepository.findFirstByOrdidAndType(order.getOrdid(), SETTLE_TYPE_CUSTOMER);
        OrderSettle factorySettle = orderSettleRepository.findFirstByOrdidAndType(order.getOrdid(), SETTLE_TYPE_FACTORY);
        InsuranceMaterial insuranceMaterial = Optional.ofNullable(insuranceMaterialRepository.findByOrdid(order.getOrdid())).orElse(new InsuranceMaterial());
        fields.forEach(x -> {
            try {
                if (CellEnum.CUSTOMER_1.getName().equals(x.getName())) {
                    if (Objects.nonNull(address)) {
                        Province province = provinceRepository.findByProvinceCode(address.getProvince());
                        row.add(province.getProvinceName());
                    } else {
                        row.add(NULL);
                    }
                } else if (CellEnum.CUSTOMER_2.getName().equals(x.getName())) {
                    Customer customer = customersRepository.findByCid(order.getCid());
                    row.add(customer.getCustomerParents().getName());
                } else if (CellEnum.CUSTOMER_3.getName().equals(x.getName())) {
                    Customer customer = customersRepository.findByCid(order.getCid());
                    row.add(customer.getName());
                } else if (CellEnum.CUSTOMER_4.getName().equals(x.getName())) {
                    row.add(address.getContactUser());
                } else if (CellEnum.CUSTOMER_5.getName().equals(x.getName())) {
                    row.add(address.getContactTel());
                } else if (CellEnum.CUSTOMER_6.getName().equals(x.getName())) {
                    row.add(address.getDetail());
                } else if (CellEnum.DEVICE_1.getName().equals(x.getName())) {
                    row.add(device.getProduct().getModel());
                } else if (CellEnum.DEVICE_2.getName().equals(x.getName())) {
                    row.add(device.getCode());
                } else if (CellEnum.DEVICE_3.getName().equals(x.getName())) {
                    row.add(device.getRemoteCode());
                } else if (CellEnum.DEVICE_4.getName().equals(x.getName())) {
                    row.add(device.getBatteryCode());
                } else if (CellEnum.DEVICE_5.getName().equals(x.getName())) {
                    row.add(device.getAnother());
                } else if (CellEnum.DEVICE_6.getName().equals(x.getName())) {
                    if (OrderConstants.ORDER_SERVICE_QUICKLY.equals(order.getServiceType())) {
                        row.add(device.getQuota());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.DEVICE_7.getName().equals(x.getName())) {
                    OrderLog orderLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_CONFIRM_EXPRESSES);
                    if (Objects.nonNull(orderLog)) {
                        row.add(CommonUtils.boolean2String(fileRepository.existsByOrdlogid(orderLog.getOrdlogid())));
                    } else {
                        row.add(CommonUtils.boolean2String(Boolean.FALSE));
                    }
                } else if (CellEnum.DEVICE_8.getName().equals(x.getName())) {
                    Expresses expresses = expressesRepository.findFirstByOrdidAndType(order.getOrdid(), CommonConstants.EXPRESSES_DEVICE_RECEIVE);
                    if (Objects.nonNull(expresses)) {
                        row.add(DateUtils.formatDateTime(expresses.getExpressesAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.DEVICE_9.getName().equals(x.getName())) {
                    row.add(order.getReceiveNo());
                } else if (CellEnum.MAINTAIN_1.getName().equals(x.getName())) {
                    OrderLog orderLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_CONFIRM_LOSS);
                    if (Objects.nonNull(orderLog)) {
                        row.add(DateUtils.formatDateTime(orderLog.getCt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.MAINTAIN_2.getName().equals(x.getName())) {
                    OrderLog orderLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_RETURN_EXPRESSES);
                    if (Objects.nonNull(orderLog)) {
                        row.add(DateUtils.formatDateTime(orderLog.getCt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.MAINTAIN_3.getName().equals(x.getName())) {
                    row.add(order.getReturnCaseNumber());
                } else if (CellEnum.MAINTAIN_4.getName().equals(x.getName())) {
                    String maintainerUid = order.getMaintainerUid();
                    if (StringUtils.isNotBlank(maintainerUid)) {
                        User user = userRepository.findByUid(maintainerUid);
                        row.add(user.getUserName());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.MAINTAIN_5.getName().equals(x.getName())) {
                    OrderLog orderLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_MAINTAINED);
                    if (Objects.nonNull(orderLog)) {
                        row.add(DateUtils.formatDateTime(orderLog.getCt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_1.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(orderQuoteRepository.existsByOrdid(order.getOrdid())));
                } else if (CellEnum.QUOTE_SETTLE_2.getName().equals(x.getName())) {
                    List<OrderQuote> orderQuotes = orderQuoteRepository.findByOrdidAndType(order.getOrdid(), QUOTE_TYPE_COST);
                    Double sum = orderQuotes.stream().map(y -> {
                        AtomicReference<Double> totalAmt = new AtomicReference<>(0.0);
                        Optional.ofNullable(y.getSparePrice()).ifPresent(z -> totalAmt.set(totalAmt.get() + z));
                        Optional.ofNullable(y.getExpressesPrice()).ifPresent(z -> totalAmt.set(totalAmt.get() + z));
                        Optional.ofNullable(y.getOtherPrice()).ifPresent(z -> totalAmt.set(totalAmt.get() + z));
                        return totalAmt.get();
                    }).collect(Collectors.toList()).stream().mapToDouble(v -> v).sum();
                    row.add(sum);
                } else if (CellEnum.QUOTE_SETTLE_3.getName().equals(x.getName())) {
                    OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), OrderConstants.QUOTE_TYPE_CUSTOMER);
                    AtomicReference<Double> totalAmt = new AtomicReference<>(0.0);
                    Optional.ofNullable(customerQuote.getSparePrice()).ifPresent(v -> totalAmt.set(totalAmt.get() + v));
                    Optional.ofNullable(customerQuote.getExpressesPrice()).ifPresent(v -> totalAmt.set(totalAmt.get() + v));
                    Optional.ofNullable(customerQuote.getOtherPrice()).ifPresent(v -> totalAmt.set(totalAmt.get() + v));
                    row.add(totalAmt.get());
                } else if (CellEnum.QUOTE_SETTLE_4.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(order.getStatus().compareTo(OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION_REVIEW.getCode()) > 0));
                } else if (CellEnum.QUOTE_SETTLE_5.getName().equals(x.getName())) {
                    Project project = projectRepository.findByPjid(order.getPjid());
                    row.add(project.getName());
                } else if (CellEnum.QUOTE_SETTLE_6.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(order.getStatus().compareTo(OrderStatusEnums.ORDER_STATUS_WAITING_SETTLE.getCode()) > 0));
                } else if (CellEnum.QUOTE_SETTLE_7.getName().equals(x.getName())) {
                    List<OrderQuote> orderQuotes = orderQuoteRepository.findByOrdidAndType(order.getOrdid(), QUOTE_TYPE_COST);
                    if (orderQuotes.size() > 1) {
                        orderQuotes.remove(0);
                        Double sum = orderQuotes.stream().map(y -> {
                            AtomicReference<Double> totalAmt = new AtomicReference<>(0.0);
                            Optional.ofNullable(y.getSparePrice()).ifPresent(z -> totalAmt.set(totalAmt.get() + z));
                            Optional.ofNullable(y.getExpressesPrice()).ifPresent(z -> totalAmt.set(totalAmt.get() + z));
                            Optional.ofNullable(y.getOtherPrice()).ifPresent(z -> totalAmt.set(totalAmt.get() + z));
                            return totalAmt.get();
                        }).collect(Collectors.toList()).stream().mapToDouble(v -> v).sum();
                        row.add(sum);
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_8.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(customerSettle.getReceivable());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_9.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(customerSettle.getReceived());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_10.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(DateUtils.formatDateTime(customerSettle.getReceiveAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_11.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(DateUtils.formatDateTime(customerSettle.getInvoiceAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_12.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(customerSettle.getInvoiceCode());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_13.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(customerSettle.getInvoiceNo());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.QUOTE_SETTLE_14.getName().equals(x.getName())) {
                    if (Objects.nonNull(customerSettle)) {
                        row.add(customerSettle.getInvoiceAmount());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.ADVANCED_PAY_1.getName().equals(x.getName())) {
                    if (Objects.nonNull(factorySettle)) {
                        row.add(factorySettle.getAdvanced());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.ADVANCED_PAY_2.getName().equals(x.getName())) {
                    if (Objects.nonNull(factorySettle)) {
                        row.add(factorySettle.getAdvanceObject());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.ADVANCED_PAY_3.getName().equals(x.getName())) {
                    if (Objects.nonNull(factorySettle)) {
                        row.add(factorySettle.getAdvanceAmount());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.ADVANCED_PAY_4.getName().equals(x.getName())) {
                    if (Objects.nonNull(factorySettle)) {
                        row.add(DateUtils.formatDateTime(factorySettle.getAdvanceAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.INSURANCE_1.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(!INSURANCE_FIRST_REJECT.getCode().equals(order.getReviewInsuranceStatus()) && order.getReviewInsuranceStatus().compareTo(INSURANCE_FIRST_REVIEW.getCode()) > 0));
                } else if (CellEnum.INSURANCE_2.getName().equals(x.getName())) {
                    OrderLog orderLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_FIRST_INSURANCE_SUBMIT);
                    if (Objects.nonNull(orderLog)) {
                        row.add(DateUtils.formatDateTime(orderLog.getCt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.INSURANCE_3.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(!INSURANCE_LAST_REJECT.getCode().equals(order.getReviewInsuranceStatus()) && order.getReviewInsuranceStatus().compareTo(INSURANCE_LAST_WAITING_REVIEW.getCode()) > 0));
                } else if (CellEnum.INSURANCE_4.getName().equals(x.getName())) {
                    if (Objects.nonNull(insuranceMaterial.getFaultAt())) {
                        row.add(DateUtils.formatDateTime(insuranceMaterial.getFaultAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.INSURANCE_5.getName().equals(x.getName())) {
                    row.add(insuranceMaterial.getFaultLocation());
                } else if (CellEnum.INSURANCE_6.getName().equals(x.getName())) {
                    row.add(insuranceMaterial.getFaultReason());
                } else if (CellEnum.INSURANCE_7.getName().equals(x.getName())) {
                    row.add(device.getInsuranceCode());
                } else if (CellEnum.INSURANCE_8.getName().equals(x.getName())) {
                    row.add(insuranceMaterial.getPilotName());
                } else if (CellEnum.INSURANCE_9.getName().equals(x.getName())) {
                    row.add(insuranceMaterial.getReportAmt());
                } else if (CellEnum.INSURANCE_10.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(insuranceMaterial.getBeReport()));
                } else if (CellEnum.INSURANCE_11.getName().equals(x.getName())) {
                    if (Objects.nonNull(insuranceMaterial.getReportAt())) {
                        row.add(DateUtils.formatDateTime(insuranceMaterial.getReportAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.INSURANCE_12.getName().equals(x.getName())) {
                    row.add(insuranceMaterial.getReportNo());
                } else if (CellEnum.INSURANCE_13.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(INSURANCE_FINISH.getCode().equals(order.getReviewInsuranceStatus())));
                } else if (CellEnum.UAV_CHECK_1.getName().equals(x.getName())) {
                    OrderLog orderLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(order.getOrdid(), ORDER_LOG_METHOD_CHECKED);
                    if (Objects.nonNull(orderLog)) {
                        row.add(DateUtils.formatDateTime(orderLog.getCt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.UAV_CHECK_2.getName().equals(x.getName())) {
                    String checkUid = order.getCheckUid();
                    if (StringUtils.isNotBlank(checkUid)) {
                        User user = userRepository.findByUid(checkUid);
                        row.add(user.getUserName());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.UAV_CHECK_3.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(order.getCheckDraw()));
                } else if (CellEnum.UAV_CHECK_4.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(order.getStatus().compareTo(ORDER_STATUS_WAITING_CHECK.getCode()) > 0));
                } else if (CellEnum.UAV_EXPRESSES_1.getName().equals(x.getName())) {
                    row.add(CommonUtils.boolean2String(order.getStatus().equals(ORDER_STATUS_FINISH.getCode())));
                } else if (CellEnum.UAV_EXPRESSES_2.getName().equals(x.getName())) {
                    Expresses expresses = expressesRepository.findFirstByOrdidAndType(order.getOrdid(), CommonConstants.EXPRESSES_SEND_CUSTOMER);
                    if (Objects.nonNull(expresses)) {
                        row.add(DateUtils.formatDateTime(expresses.getExpressesAt()));
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else if (CellEnum.UAV_EXPRESSES_3.getName().equals(x.getName())) {
                    Expresses expresses = expressesRepository.findFirstByOrdidAndType(order.getOrdid(), CommonConstants.EXPRESSES_SEND_CUSTOMER);
                    if (Objects.nonNull(expresses)) {
                        row.add(expresses.getWaybillNo());
                    } else {
                        row.add(CommonConstants.NULL);
                    }
                } else {
                    row.add(CommonConstants.NULL);
                }
            } catch (Exception e) {
                log.error("getOneLow error", e);
                row.add(CommonConstants.NULL);
            }
        });
    }


    /**
     * 生成赔付材料
     * 1 电力业务出险通知及索赔申请书 - 出版资料提交
     * 9 赔付意向书-出版资料提交
     * 3 无人机坠落损失分析报告-故障报告-出版资料提交
     * 6 事故照片 -出版资料提交
     * 2损失清单 - 出版资料提交
     *
     * @param order
     */
    public void generateFirstMaterial(Order order) throws Exception {
        Company company = companyRepository.findByCpid(order.getCpid());
        String code = null;
        if (Objects.nonNull(company)) {
            code = company.getTemplateCode();
        }
        //赔付意向书
        this.insuranceFileService.generateIntentionPayFile(code, order);
        //电力业务出险通知及索赔申请书
        this.insuranceFileService.generatePowerRiskFile(code, order);
        //损失清单
        this.insuranceFileService.generateLossFile(code, order);
        //无人机故障报告
        this.insuranceFileService.generateFaultFile(code, order);
        //事故照片
        this.insuranceFileService.generateFaultPhotoFile(code, order);
    }

    /**
     * 生成赔付材料--报价审核通过
     * 2 损失清单 - 报价审核通过（更新）
     * 10 报价单 -报价审核通过
     * 9 赔付意向书-报价审核通过（更新）
     *
     * @param order
     */
    public void generateQuoteMaterial(Order order) throws Exception {
        String uid = CommonUtils.getUid();
        Device device = deviceRepository.findByDevid(order.getDevid());
        Product product = productRepository.findByPid(device.getPid());
        Customer customer = customersRepository.findByCid(order.getCid());
        Address address = addressRepository.getByAid(order.getAid());
        OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
        String customerName = customer.getName();
        String contact = address.getContactUser().concat(address.getContactTel());
        //只有鼎和的生成保险文件
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        if (!CommonUtils.int2BBoolean(order.getSecondary()) && ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType()) && Objects.nonNull(insuranceMaterial)) {
            Company company = companyRepository.findByCpid(order.getCpid());
            String code = null;
            if (Objects.nonNull(company)) {
                code = company.getTemplateCode();
            }
            //损失清单
            this.insuranceFileService.generateLossFile(code, order);
            //赔付意向书
            this.insuranceFileService.generateIntentionPayFile(code, order);
        }
        //报价单
        if (!CommonUtils.int2BBoolean(order.getSecondary()) && Objects.nonNull(customerQuote)) {
            double total = CommonUtils.format2Point(Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00));
            this.deleteFile(order.getOrdid(), CUSTOMER_QUOTE);
            QuotationPrice quotationPrice = new QuotationPrice();
            quotationPrice.setCustomer(customerName);
            quotationPrice.setDeviceModel(product.getName());
            quotationPrice.setSnCode(device.getCode());
            OrderLog quoteLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(order.getOrdid(), ORDER_LOG_METHOD_QUOTATION);
            quotationPrice.setQuotationDate(DateUtils.formatDateDot(quoteLog.getCt()));
            quotationPrice.setContact(contact);
            quotationPrice.setProduct(product.getName());
            List<OrderCategory> orderCategories = orderCategoryRepository.findByOrdid(order.getOrdid());
            List<String> ordcatids = orderCategories.stream().map(OrderCategory::getCatid).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(ordcatids)) {
                List<String> categories = categoryRepository.findAllByCatidIn(ordcatids).stream().map(Category::getName).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(categories)) {
                    quotationPrice.setLossAssessmentResult(StringUtils.join(categories, COMMA));
                }
            }
            quotationPrice.setPrice(String.valueOf(total));
            quotationPrice.setTotalAmount(CommonUtils.toChineseMoney(BigDecimal.valueOf(total)));
            quotationPrice.setDate(DateUtils.formatDateChinese(System.currentTimeMillis()));
            List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(customerQuote.getOqid());
            List<MaterialItem> materialItems = orderQuoteItems.stream().map(x -> {
                MaterialItem materialItem = new MaterialItem();
                Product p = productRepository.findByPid(x.getPid());
                materialItem.setMaterialName(p.getName());
                materialItem.setUnit(p.getUnit());
                materialItem.setNumber(String.valueOf(x.getCount()));
                materialItem.setUnitPrice(String.valueOf(x.getSalePrice()));
                materialItem.setManHourCost(String.valueOf(x.getWorkHourCost()));
                materialItem.setAmount(String.valueOf(x.getCount() * (x.getWorkHourCost() + x.getSalePrice())));
                return materialItem;
            }).collect(Collectors.toList());
            quotationPrice.setMaterialList(materialItems);
            File quotationPriceFile = wordUtils.generateQuotationPrice(quotationPrice, Boolean.TRUE);
            CustomFile quoteFile = new CustomFile();
            quoteFile.setFid(CommonUtils.uuid());
            String o4 = quoteFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o4, quotationPriceFile.getAbsolutePath());
            quoteFile.setDownload(Boolean.TRUE);
            quoteFile.setDirectory(ORDER_BUCKET);
            quoteFile.setExtension(CommonConstants.FILE_PDF);
            quoteFile.setEnable(Boolean.TRUE);
            quoteFile.setMime(CommonConstants.FILE_PDF);
            quoteFile.setName("报价单.pdf");
            quoteFile.setLocation(o4);
            quoteFile.setUid(uid);
            quoteFile.setOrdid(order.getOrdid());
            quoteFile.setType(CommonConstants.CUSTOMER_QUOTE);
            fileRepository.save(quoteFile);
            FileUtils.deleteQuietly(quotationPriceFile);
            File quotationPriceFile2 = wordUtils.generateQuotationPrice(quotationPrice, Boolean.FALSE);
            CustomFile quoteFile2 = new CustomFile();
            quoteFile2.setFid(CommonUtils.uuid());
            String o42 = quoteFile2.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o42, quotationPriceFile2.getAbsolutePath());
            quoteFile2.setDownload(Boolean.TRUE);
            quoteFile2.setDirectory(ORDER_BUCKET);
            quoteFile2.setExtension(CommonConstants.FILE_PDF);
            quoteFile2.setEnable(Boolean.TRUE);
            quoteFile2.setMime(CommonConstants.FILE_PDF);
            quoteFile2.setName("报价单.pdf");
            quoteFile2.setLocation(o4);
            quoteFile2.setUid(uid);
            quoteFile2.setOrdid(order.getOrdid());
            quoteFile2.setType(CommonConstants.CUSTOMER_QUOTE_WITHOUT_SIGN);
            fileRepository.save(quoteFile2);
            FileUtils.deleteQuietly(quotationPriceFile2);
        }
        //成本报价单
        this.deleteFile(order.getOrdid(), SYSTEM_QUOTE);
        List<OrderQuote> costQuote = orderQuoteRepository.findByOrdidAndTypeOrderByOrder(order.getOrdid(), QUOTE_TYPE_COST);
        Workbook workbook = this.writeCostQuote(order, costQuote);
        if (Objects.nonNull(customerQuote)) {
            this.writeCustomerQuote(order, customerQuote, workbook);
        }
        String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid()).concat(DOT).concat(FILE_XLSX);
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        workbook.write(fileOutputStream);
        fileOutputStream.close();
        workbook.close();
        CustomFile costQuoteFile = new CustomFile();
        costQuoteFile.setFid(CommonUtils.uuid());
        String o5 = costQuoteFile.getFid().concat(CommonConstants.DOT).concat(FILE_XLSX);
        customMinIOClient.uploadFile(ORDER_BUCKET, o5, fileName);
        costQuoteFile.setDownload(Boolean.TRUE);
        costQuoteFile.setDirectory(ORDER_BUCKET);
        costQuoteFile.setExtension(CommonConstants.FILE_XLSX);
        costQuoteFile.setEnable(Boolean.TRUE);
        costQuoteFile.setMime(CommonConstants.FILE_XLSX);
        costQuoteFile.setName("系统报价单.xlsx");
        costQuoteFile.setLocation(o5);
        costQuoteFile.setUid(uid);
        costQuoteFile.setOrdid(order.getOrdid());
        costQuoteFile.setType(CommonConstants.SYSTEM_QUOTE);
        fileRepository.save(costQuoteFile);
        FileUtils.deleteQuietly(new File(fileName));
    }

    /**
     * 生成赔付材料
     * 13 报案文件- 终版资料审核通过
     * 12 案件报告- 终版资料审核通过
     * 11 收费通知单 -终版资料审核通过
     *
     * @param order
     */
    public void generateLastMaterial(Order order) throws Exception {
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            Company company = companyRepository.findByCpid(order.getCpid());
            String code = null;
            if (Objects.nonNull(company)) {
                code = company.getTemplateCode();
            }
            //收费通知单
            this.insuranceFileService.generateFeeNotification(code, order);
            //案件报告
            this.insuranceFileService.generateIncidentReport(code, order);
            //报案文件--无报案号则生成
            this.insuranceFileService.generateReport(code, order);
        }
    }

    /**
     * 获取文件链接
     *
     * @param log
     * @return
     */
    public List<String> getFileByLog(OrderLog log) {
        if (Objects.isNull(log)) {
            return Collections.emptyList();
        }
        List<CustomFile> customFiles = fileRepository.findByOrdlogid(log.getOrdlogid());
        return customFiles.stream().map(this::getFileProxyUrl).filter(StringUtils::isNotBlank).collect(Collectors.toList());
    }

    /**
     * 获取工单编号
     *
     * @return
     */
    public String getOrderInnerCode() throws Exception {
        int maxLoop = 10000;
        int loop = 1;
        while (loop < maxLoop) {
            String inderCode = "NTWX".concat(DateUtils.formatMonth(System.currentTimeMillis())).concat(String.format("%04d", loop));
            if (!orderRepository.existsByInnerCode(inderCode)) {
                return inderCode;
            }
            loop++;
        }
        throw new CustomBizException("本月工单编号已用完");
    }


    /**
     * 短信发送
     *
     * @param templateCode
     */
    public void sendSms(Order order, String templateCode) {
        try {
            //发送站内信
            Device device = deviceRepository.findByDevid(order.getDevid());
            User user = userRepository.findByUid(order.getCuid());
            if (customConfig.getConfirmDeviceTemplate1().equals(templateCode)) {
                String content = "已完成收件，请尽快登陆系统进入\"工单中心\"-\"工单详情\"完成设备信息确认，超时系统将自动确认。";
                messageService.sendPersonalOrderMessage("设备确认", content, MessageConstants.LEVEL_PERSONAL, order.getOrdid(), order.getCuid());
            } else if (customConfig.getConfirmQuote().equals(templateCode)) {
                String content = "报价单已生成，请登陆系统进入\"工单中心\"-\"工单详情\"-\"工单报价\"查看并确认报价。";
                messageService.sendPersonalOrderMessage("报价确认", content, MessageConstants.LEVEL_PERSONAL, order.getOrdid(), order.getCuid());
            } else if (customConfig.getSummitFirstInsurance().equals(templateCode)) {
                String content = "申请已提交，请登陆系统进入\"工单中心\"-\"工单详情\"-\"保险资料\"提交工单保险初版资料。";
                messageService.sendPersonalOrderMessage("初版资料待提交", content, MessageConstants.LEVEL_PERSONAL, order.getOrdid(), order.getCuid());
            } else if (customConfig.getReviewFirstInsurance().equals(templateCode)) {
                String content = "保险初版资料审核未通过，请登陆系统进入\"工单中心\"-\"工单详情\"-\"保险资料\"重新修改后提交保险初版资料。";
                messageService.sendPersonalOrderMessage("初版资料审核失败", content, MessageConstants.LEVEL_PERSONAL, order.getOrdid(), order.getCuid());
            } else if (customConfig.getSummitLastInsurance().equals(templateCode)) {
                String content = "报价已确认，请登陆系统进入\"工单中心\"-\"工单详情\"-\"保险资料\"提交工单保险终版资料。";
                messageService.sendPersonalOrderMessage("终版资料待提交", content, MessageConstants.LEVEL_PERSONAL, order.getOrdid(), order.getCuid());
            } else if (customConfig.getReviewLastInsurance().equals(templateCode)) {
                String content = "保险终版资料审核未通过，请登陆系统进入\"工单中心\"-\"工单详情\"-\"保险资料\"重新提交保险终版资料。";
                messageService.sendPersonalOrderMessage("终版资料审核失败", content, MessageConstants.LEVEL_PERSONAL, order.getOrdid(), order.getCuid());
            }
            //发送短信
            JSONObject templateParam = new JSONObject();
            templateParam.put("innerCode", order.getInnerCode());
            templateParam.put("sn", device.getCode());
            this.aliYunSmsService.sendSms(user.getMobile(), templateParam.toJSONString(), templateCode);
        } catch (Exception e) {
            log.error("sendSms error", e);
        }
    }

    /**
     * 删除文件
     *
     * @param orderId
     * @param type
     */
    private void deleteFile(String orderId, String type) throws Exception {
        List<CustomFile> oldQuoteFile = fileRepository.findByOrdidAndType(orderId, type);
        if (CollectionUtils.isNotEmpty(oldQuoteFile)) {
            for (CustomFile customFile : oldQuoteFile) {
                String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
                customMinIOClient.removeFile(customFile.getDirectory(), objectName);
                fileRepository.delete(customFile);
            }
        }
    }

    /**
     * @param request
     */
    public Customer createCustomerIfNot(CustomerDTO request) {
        Customer customer = customersRepository.findByName(request.getName());
        if (Objects.isNull(customer)) {
            customer = new Customer();
            BeanUtils.copyProperties(request, customer);
            customer.setCid(CommonUtils.uuid());
            customer.setCpid(CommonUtils.getCpid());
            customersRepository.save(customer);
        }
        return customer;
    }


    /**
     * 工单导入
     *
     * @param request
     * @param type
     */
    @Transactional(rollbackFor = Exception.class)
    public void importOrder(ImportOrder request, Integer type) throws Exception {
        String uid = CommonUtils.getUid();
        String cpid = CommonUtils.getCpid();
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setAddress(request.getAddress());
        Province province = provinceRepository.findByProvinceName(request.getProvince());
        if (Objects.isNull(province)) {
            throw new CustomBizException("省错误:".concat(request.getProvince()));
        }
        customerDTO.setProvince(province.getProvinceCode());
        City city = cityRepository.findByCityName(request.getCity());
        if (Objects.isNull(city)) {
            throw new CustomBizException("市错误:".concat(request.getCity()));
        }
        customerDTO.setCity(city.getCityCode());
        County county = countyRepository.findByAreaName(request.getCounty());
        if (Objects.isNull(county)) {
            throw new CustomBizException("区县错误:".concat(request.getCounty()));
        }
        customerDTO.setCounty(county.getAreaCode());
        customerDTO.setName(request.getCustomerName());
        customerDTO.setAddress(request.getAddress());
        customerDTO.setType("1");
        Customer customer = this.createCustomerIfNot(customerDTO);
        Address address = addressRepository.findByCidAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(customer.getCid(), request.getTelphone(), request.getContact(), province.getProvinceCode(), city.getCityCode(), county.getAreaCode(), request.getAddress());
        if (Objects.isNull(address)) {
            address = new Address();
            address.setProvince(province.getProvinceCode());
            address.setCity(city.getCityCode());
            address.setCid(customer.getCid());
            address.setCounty(county.getAreaCode());
            address.setDetail(request.getAddress());
            address.setAid(CommonUtils.uuid());
            address.setContactTel(request.getTelphone());
            address.setContactUser(request.getContact());
            address.setIsDefault(Boolean.FALSE);
            addressRepository.save(address);
        }
        User user = userRepository.findByMobile(request.getTelphone());
        if (Objects.isNull(user)) {
            user = createUser(request.getContact(), request.getTelphone(), customer);
        }
        Device device = deviceRepository.findByCode(request.getDeviceCode());
        if (Objects.isNull(device)) {
            device = new Device();
            device.setCode(request.getDeviceCode());
            device.setInnerCode(CommonUtils.uuid());
            Product product = productRepository.findByCode(request.getProductCode());
            if (Objects.isNull(product)) {
                throw new CustomBizException("产品未录入:".concat(request.getProductCode()));
            }
            device.setPid(product.getPid());
            device.setDevid(CommonUtils.uuid());
            device.setCid(customer.getCid());
            device.setUid(user.getUid());
            device.setBatteryCode(request.getBatteryCode());
            device.setRemoteCode(request.getRemoteCode());
            device.setAnother(request.getAnother());
        }
        if (ORDER_IMPORT_DH.equals(type)) {
            device.setInsuranceType(OrderConstants.INSURANCE_DH);
        } else {
            device.setInsuranceType(request.getInsuranceType());
        }
        device.setStatus(OrderConstants.DEVICE_STATUS_NORMAL);
        device.setExchangeCount(request.getExchangeCount());
        device.setRegistrationNumber(request.getRegistrationNumber());
        device.setInsuranceStartAt(request.getInsuranceStartAt());
        device.setInsuranceExpireAt(request.getInsuranceExpireAt());
        device.setQuota(request.getQuota());
        deviceRepository.save(device);
        Order order = new Order();
        order.setCid(customer.getCid());
        order.setCuid(user.getUid());
        order.setAid(address.getAid());
        order.setDevid(device.getDevid());
        order.setStatus(ORDER_STATUS_WAIT_EXPRESSES.getCode());
        order.setServiceType(request.getServiceType());
        order.setDescription(request.getDescription());
        order.setUid(uid);
        order.setCpid(cpid);
        order.setType(ORDER_MAINTAIN);
        order.setLevel(ONE_VALUE);
        order.setOrdid(CommonUtils.uuid());
        order.setInnerCode(request.getInnerCode());
        order.setOrigin(ORIGIN_WEB_SYSTEM);
        this.generateQrCode(order);
        if (ORDER_IMPORT_DH.equals(type)) {
            order.setServiceType(OrderConstants.ORDER_IN_INSURANCE);
            order.setReviewInsuranceStatus(INSURANCE_FIRST_WAITING_SUBMIT.getCode());
        } else if (ORDER_IMPORT_SECONDARY.equals(type)) {
            order.setSecondary(TURE_VALUE);
            order.setRelationOrderCode(request.getRelationInnerCode());
        } else if (ORDER_IMPORT_DJ.equals(type)) {
            order.setServiceType(OrderConstants.ORDER_IN_INSURANCE);
        } else if (ORDER_IMPORT_OUT_INSURANCE.equals(type)) {
            order.setServiceType(OrderConstants.ORDER_OUT_INSURANCE);
        } else {
            throw new CustomBizException("类型参数错误");
        }
        orderRepository.save(order);
        this.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CREATE);
    }

    public User createUser(String userName, String mobile, Customer customer) {
        User user = new User();
        user.setActivate(CommonConstants.TURE_VALUE);
        user.setUserType(UserTypeEnum.USER.getCode());
        user.setUid(CommonUtils.uuid());
        user.setUserName(userName);
        user.setMobile(mobile);
        user.setPassword(passwordEncoder.encode(StringEncryptionUtil.encode(defaultPassword)));
        user.setCid(customer.getCid());
        userRepository.save(user);

        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        Role role = roleService.getDefaultRoleByCode(RoleEnum.User);
        userRole.setRoleId(role.getId());
        userRoleRepository.save(userRole);
        return user;
    }
}
