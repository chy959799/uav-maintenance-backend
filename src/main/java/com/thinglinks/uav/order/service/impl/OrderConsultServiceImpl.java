package com.thinglinks.uav.order.service.impl;

import com.aliyun.credentials.utils.StringUtils;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.order.dto.consult.*;
import com.thinglinks.uav.order.entity.OrderConsult;
import com.thinglinks.uav.order.repository.OrderConsultRepository;
import com.thinglinks.uav.order.service.OrderConsultService;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @package: com.thinglinks.uav.order.service.impl
 * @className: OrderConsultServiceImpl
 * @author: rp
 * @description: TODO
 */
@Service
@Slf4j
public class OrderConsultServiceImpl implements OrderConsultService {
    static String ORDER_CONSULT_BUCKET_NAME = "orderconsult";

    static Long DEFAULT_TIME = 0L;

    static String PATTERN = ".*/([^/?]+)\\?.*";
    static Pattern ORDER_IMG_PATTERN = Pattern.compile(PATTERN);
    @Resource
    private OrderConsultRepository orderConsultRepository;

    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Resource
    private FileRepository fileRepository;
    @Resource
    private CustomMinIOClient minIOClient;
    @Resource
    private UserRepository usersRepository;
    @Override
    public BaseResponse<OrderConsultDetail> getOrderConsult(String ordid) throws Exception {
        OrderConsultDetail orderConsultDetail = new OrderConsultDetail();
        OrderConsult orderConsult = orderConsultRepository.findByOrdid(ordid);
        if (!Objects.isNull(orderConsult)) {
            BeanUtils.copyProperties(orderConsult, orderConsultDetail);
            orderConsultDetail.setConsultAt(DateUtils.formatDateTime(orderConsult.getConsultTime()));
            orderConsultDetail.setCreatedAt(DateUtils.formatDateTime(orderConsult.getCt()));
            orderConsultDetail.setUpdatedAt(DateUtils.formatDateTime(orderConsult.getUt()));
            List<CustomFile> customFiles = fileRepository.findAllByConsordid(ordid);
            HashMap<String, String> fileInfo = new HashMap<>();
            for (CustomFile customFile : customFiles) {
                String url = minIOClient.getProxyUrl(ORDER_CONSULT_BUCKET_NAME, customFile.getLocation());
                fileInfo.put(customFile.getName(),url);
            }
            orderConsultDetail.setFiles(fileInfo);
            return BaseResponse.success(orderConsultDetail);
        }
        return BaseResponse.fail("咨询工单不存在");
    }

    @Override
    public BaseResponse<OrderConsultResponse> addOrderConsult(OrderConsultRequest request) throws Exception {
        if (!usersRepository.existsByUidAndDid(request.getRecorderUid(), request.getRecorderDid())||StringUtils.isEmpty(request.getRecorderDid())){
            return BaseResponse.fail("接待人信息不合法");
        }
        OrderConsult orderConsult = new OrderConsult();
        OrderConsultResponse orderConsultResponse = new OrderConsultResponse();
        BeanUtils.copyProperties(request, orderConsult);
        orderConsult.setCreatedBy(CommonUtils.getUid());
        orderConsult.setOrdid(CommonUtils.uuid());
        orderConsult.setInnerCode("GDZX".concat(DateUtils.formatDate(System.currentTimeMillis())).concat(String.format("%03d", customRedisTemplate.getOrderConsultCountByDay())));
        if (Objects.nonNull(request.getConsultAt())){
            orderConsult.setConsultTime(DateUtils.parseDateStringToTimestamp(request.getConsultAt()));
        }
        OrderConsult save = orderConsultRepository.save(orderConsult);
        BeanUtils.copyProperties(save, orderConsultResponse);
        minIOClient.createMinioClient();
        ArrayList<CustomFile> customFileList = new ArrayList<>();
        if (Objects.nonNull(request.getFile0())) {
            CustomFile customFile = upLoadFile(request.getFile0(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile1())) {
            CustomFile customFile = upLoadFile(request.getFile1(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile2())) {
            CustomFile customFile = upLoadFile(request.getFile2(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile3())) {
            CustomFile customFile = upLoadFile(request.getFile3(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile4())) {
            CustomFile customFile = upLoadFile(request.getFile4(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile5())) {
            CustomFile customFile = upLoadFile(request.getFile5(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        orderConsultResponse.setFlies(customFileList);
        return BaseResponse.success(orderConsultResponse);
    }

    @Override
    public BasePageResponse<OrderConsultDto> pageOrderConsult(OrderConsultPageRequest request) {
        PageRequest pageRequest = request.of();
        Specification<OrderConsult> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (Objects.nonNull(request.getName())) {
                Join<OrderConsult, Company> comJoin = root.join("company", JoinType.LEFT);
                predicates.add(criteriaBuilder.like(comJoin.get("name"), CommonUtils.assembleLike(request.getName())));
            }

            if (Objects.nonNull(request.getInnerCode())) {
                predicates.add(criteriaBuilder.like(root.get("innerCode"), CommonUtils.assembleLike(request.getInnerCode())));
            }
            if (Objects.nonNull(request.getStartTime())&&Objects.nonNull(request.getEndTime())) {
                Path<Long> descPath = root.get("consultAt");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String startTime = (request.getStartTime() + " 00:00:00");
                String endTime = (request.getEndTime() + " 00:00:00");
                long before = DEFAULT_TIME;
                long after = DEFAULT_TIME;
                try {
                    before = (simpleDateFormat.parse(startTime).getTime());
                    after = (simpleDateFormat.parse(endTime).getTime());
                } catch (ParseException e) {
                    System.out.println("时间格式错误");
                }
                predicates.add(criteriaBuilder.between(descPath, before, after));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<OrderConsult> page = orderConsultRepository.findAll(specification, pageRequest);
        List<OrderConsultDto> result = page.stream().map(p -> {
            OrderConsultDto orderConsultDto = new OrderConsultDto();
            BeanUtils.copyProperties(p, orderConsultDto);
            orderConsultDto.setCreatedAt(DateUtils.formatDateTime(p.getCt()));
            orderConsultDto.setUpdatedAt(DateUtils.formatDateTime(p.getUt()));
            orderConsultDto.setConsultAt(DateUtils.formatDateTime(p.getConsultTime()));
            List<CustomFile> customFiles = fileRepository.findAllByConsordid(p.getOrdid());
            List<String> fileUrls = new ArrayList<>();
            for (CustomFile file : customFiles) {
                String url;
                try {
                    url = minIOClient.getProxyUrl(ORDER_CONSULT_BUCKET_NAME, file.getLocation());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                fileUrls.add(url);
            }
            orderConsultDto.setFiles(fileUrls);
            return orderConsultDto;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, result);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<OrderConsultResponse> updateOrderConsult(String ordid, OrderConsultRequest request) throws Exception {
        OrderConsult orderConsult = orderConsultRepository.findByOrdid(ordid);
        if (Objects.nonNull(request.getRecorderUid())&&!request.getRecorderUid().equals(orderConsult.getRecorderUid())){
            if (!usersRepository.existsByUidAndDid(request.getRecorderUid(), request.getRecorderDid())||StringUtils.isEmpty(request.getRecorderDid())){
                return BaseResponse.fail("接待人信息不合法");
            }
            //接待人
            orderConsult.setRecorderUid(request.getRecorderUid());
        }
        if(Objects.nonNull(request.getCid())&&!request.getCid().equals(orderConsult.getCid())){
            orderConsult.setCid(request.getCid());
        }
        if(Objects.nonNull(request.getUid())&&!request.getUid().equals(orderConsult.getUid())){
            orderConsult.setUid(request.getUid());
        }
        if(Objects.nonNull(request.getTelphone())&&!request.getTelphone().equals(orderConsult.getUser().getMobile())){
            User user = usersRepository.findByUid(request.getUid());
            user.setMobile(request.getTelphone());
            usersRepository.save(user);
        }
        if(Objects.nonNull(request.getDescription())&&!request.getDescription().equals(orderConsult.getDescription())){
            orderConsult.setDescription(request.getDescription());
        }
        if (Objects.nonNull(request.getConsultAt())){
            orderConsult.setConsultTime(DateUtils.parseDateStringToTimestamp(request.getConsultAt()));
        }
        OrderConsult save = orderConsultRepository.save(orderConsult);
        OrderConsultResponse orderConsultResponse = new OrderConsultResponse();
        BeanUtils.copyProperties(save, orderConsultResponse);
        minIOClient.createMinioClient();
        ArrayList<CustomFile> customFileList = new ArrayList<>();
        if (Objects.nonNull(request.getFile0())) {
            CustomFile customFile = upLoadFile(request.getFile0(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile1())) {
            CustomFile customFile = upLoadFile(request.getFile1(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile2())) {
            CustomFile customFile = upLoadFile(request.getFile2(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile3())) {
            CustomFile customFile = upLoadFile(request.getFile3(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile4())) {
            CustomFile customFile = upLoadFile(request.getFile4(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile5())) {
            CustomFile customFile = upLoadFile(request.getFile5(), orderConsultResponse.getOrdid());
            customFileList.add(customFile);
        }
        orderConsultResponse.setFlies(customFileList);
        return BaseResponse.success(orderConsultResponse);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<String> deleteOrderConsult(String ordid) {
        if (orderConsultRepository.existsByOrdid(ordid)){
            List<CustomFile> customFiles = fileRepository.findAllByConsordid(ordid);
            customFiles.forEach(f -> {
                try {
                    minIOClient.removeFile(ORDER_CONSULT_BUCKET_NAME,f.getLocation());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
            fileRepository.deleteAllByConsordid(ordid);
            orderConsultRepository.deleteByOrdid(ordid);
            return BaseResponse.success("删除成功");
        }
        return BaseResponse.fail("删除失败");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> delFile(String url) throws Exception {
        Matcher matcher = ORDER_IMG_PATTERN.matcher(url);
        if (!matcher.find()) {
            return BaseResponse.fail("文件路径不正确");
        }
        String fileName = matcher.group(1);
        minIOClient.removeFile(ORDER_CONSULT_BUCKET_NAME,fileName);
        String fid = "";
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex != -1) {
            fid = fileName.substring(0, dotIndex);
        }
        fileRepository.deleteByFid(fid);
        return BaseResponse.success();
    }
    public CustomFile upLoadFile(MultipartFile file, String pid) throws Exception {
        Tika tika = new Tika();
        String bucketName = ORDER_CONSULT_BUCKET_NAME;
        CustomFile f = new CustomFile();
        f.setFid(CommonUtils.uuid());
        f.setSize(file.getSize());
        f.setName(file.getOriginalFilename());
        f.setExtension(FilenameUtils.getExtension(file.getOriginalFilename()));
        f.setDownload(Boolean.TRUE);
        f.setDirectory(bucketName);
        f.setEnable(Boolean.TRUE);
        f.setPid(pid);
        f.setMime(tika.detect(file.getInputStream()));
        f.setUid(CommonUtils.getUid());
        f.setLocation(f.getFid().concat(".").concat(f.getExtension()));
        String objectName = f.getFid().concat(CommonConstants.DOT).concat(f.getExtension());
        CustomFile saveCustomFile = fileRepository.save(f);
        minIOClient.uploadFile(bucketName, objectName, file.getInputStream());
        return saveCustomFile;
    }
}
