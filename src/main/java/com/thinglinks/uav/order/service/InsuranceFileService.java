package com.thinglinks.uav.order.service;

import com.thinglinks.uav.order.dto.FilePreview;
import com.thinglinks.uav.order.dto.InsurancePreview;
import com.thinglinks.uav.order.entity.Order;

public interface InsuranceFileService {


    /**
     * 生成赔付意向书
     *
     * @param code
     * @param order
     */
    void generateIntentionPayFile(String code, Order order) throws Exception;

    /**
     * 生成赔付意向书 --预览
     *
     */
    FilePreview generateIntentionPayFile(String code, InsurancePreview request) throws Exception;

    /**
     * 电力业务出险通知及索赔申请书
     *
     * @param order
     */
    void generatePowerRiskFile(String code, Order order) throws Exception;

    /**
     * 电力业务出险通知及索赔申请书--预览
     *
     * @param
     */
    FilePreview generatePowerRiskFile(String code, InsurancePreview request) throws Exception;

    /**
     * 生成损失清单
     *
     * @param code
     * @param order
     */
    void generateLossFile(String code, Order order) throws Exception;

    /**
     * 生成损失清单
     *
     */
    FilePreview generateLossFile(String code, InsurancePreview request) throws Exception;

    /**
     * 生成无人机故障报告
     *
     * @param code
     * @param order
     */
    void generateFaultFile(String code, Order order) throws Exception;

    /**
     * 生成无人机故障报告
     *
     */
    FilePreview generateFaultFile(String code, InsurancePreview request) throws Exception;

    /**
     * 事故照片
     *
     * @param code
     * @param order
     * @throws Exception
     */
    void generateFaultPhotoFile(String code, Order order) throws Exception;

    /**
     * 事故照片
     *
     * @throws Exception
     */
    FilePreview generateFaultPhotoFile(String code, InsurancePreview request) throws Exception;

    /**
     * 收费通知单
     *
     * @param code
     * @param order
     */
    void generateFeeNotification(String code, Order order) throws Exception;

    /**
     * 生成案件报告
     *
     * @param code
     * @param order
     * @throws Exception
     */
    void generateIncidentReport(String code, Order order) throws Exception;

    /**
     * 生成宝安文件
     *
     * @param code
     * @param order
     * @throws Exception
     */
    void generateReport(String code, Order order) throws Exception;
}
