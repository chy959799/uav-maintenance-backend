package com.thinglinks.uav.order.service.impl;

import com.google.common.collect.Lists;
import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.constants.StocksConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.Remark;
import com.thinglinks.uav.common.dto.ReviewResult;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.template.MaterialItem;
import com.thinglinks.uav.common.template.QuotationPrice;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.EasyExcelUtils;
import com.thinglinks.uav.common.utils.WordUtils;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.CompanyRepository;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.insurance.entity.Insurance;
import com.thinglinks.uav.insurance.repository.InsuranceRepository;
import com.thinglinks.uav.order.dto.*;
import com.thinglinks.uav.order.dto.excel.*;
import com.thinglinks.uav.order.dto.quote.*;
import com.thinglinks.uav.order.entity.*;
import com.thinglinks.uav.order.enums.InsuranceStatusEnums;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.*;
import com.thinglinks.uav.order.service.ExpressesService;
import com.thinglinks.uav.order.service.InsuranceFileService;
import com.thinglinks.uav.order.service.OrderService;
import com.thinglinks.uav.order.service.common.InnerService;
import com.thinglinks.uav.order.service.express.EMSExpressServiceImpl;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import com.thinglinks.uav.spare.repository.ProjectProductsRepository;
import com.thinglinks.uav.stock.dto.DetailStockInOut;
import com.thinglinks.uav.stock.entity.Stock;
import com.thinglinks.uav.stock.entity.StockInOut;
import com.thinglinks.uav.stock.entity.StockInOutItem;
import com.thinglinks.uav.stock.entity.Store;
import com.thinglinks.uav.stock.repository.StockInOutItemRepository;
import com.thinglinks.uav.stock.repository.StockInOutRepository;
import com.thinglinks.uav.stock.repository.StockRepository;
import com.thinglinks.uav.stock.repository.StoreRepository;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.thinglinks.uav.common.constants.CommonConstants.*;
import static com.thinglinks.uav.common.constants.OrderConstants.*;
import static com.thinglinks.uav.order.enums.InsuranceStatusEnums.*;
import static com.thinglinks.uav.order.enums.OrderStatusEnums.*;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    private static final String CHECK_STATUS_MESSAGE = "工单状态为:%s,无法进行当前操作";
    private static final String CHECK_INSURANCE_MESSAGE = "工单保险资料状态为:%s,无法进行当前操作";

    @Resource
    private ExpressesService expressesService;

    @Resource
    private EMSExpressServiceImpl emsExpressService;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private CustomersRepository customersRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private AddressRepository addressRepository;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private InsuranceRepository insuranceRepository;

    @Resource
    private CustomMinIOClient customMinIOClient;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private OrderLogRepository orderLogRepository;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private OrderCategoryRepository orderCategoryRepository;

    @Resource
    private ExpressesRepository expressesRepository;

    @Resource
    private OrderQuoteRepository orderQuoteRepository;

    @Resource
    private OrderQuoteItemRepository orderQuoteItemRepository;

    @Resource
    private OrderSettleRepository orderSettleRepository;

    @Resource
    private StoreRepository storeRepository;

    @Resource
    private StockInOutRepository stockInOutRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private StockInOutItemRepository stockInOutItemRepository;

    @Resource
    private InsuranceMaterialRepository insuranceMaterialRepository;

    @Resource
    private ProjectProductsRepository projectProductsRepository;

    @Resource
    private ProductRepository productRepository;
    @Resource
    private CustomConfig customConfig;

    @Resource
    private InnerService innerService;

    @Resource
    private WordUtils wordUtils;

    @Value("${expresses.type}")
    private String expressType;

    @Resource
    private CompanyRepository companyRepository;

    @Resource
    private InsuranceFileService insuranceFileService;

    @Override
    public BasePageResponse<PageOrder> pageOrder(PageOrderQuery request) {
        PageRequest pageRequest = request.of();
        Specification<Order> specification = innerService.orderPageSpecification(request);
        Page<Order> page = orderRepository.findAll(specification, pageRequest);
        List<PageOrder> results = page.stream().map(x -> {
            PageOrder data = new PageOrder();
            BeanUtils.copyProperties(x, data);
            if (Objects.nonNull(x.getPjid())) {
                data.setProject(projectRepository.findByPjid(x.getPjid()));
            }
            data.setCreatedAt(DateUtils.formatDateTime(x.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(x.getUt()));
            if (Objects.nonNull(x.getFinishAt())) {
                data.setFinishTime(DateUtils.formatDateTime(x.getFinishAt()));
            }
            if (Objects.nonNull(x.getReceiveNo())) {
                Expresses expresses = expressesRepository.findFirstByWaybillNoOrderByCtDesc(x.getReceiveNo());
                Optional.ofNullable(expresses).ifPresent(y -> data.setReceiveTime(DateUtils.formatDateTime(y.getExpressesAt())));
            }
            Expresses expresses3 = expressesRepository.findFirstByOrdidAndType(x.getOrdid(), EXPRESSES_SEND_CUSTOMER);
            Optional.ofNullable(expresses3).ifPresent(y -> {
                data.setCustomerWaybillNo(y.getWaybillNo());
                data.setCustomerExpressesTime(DateUtils.formatDateTime(y.getExpressesAt()));
            });
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public void exportOrder(HttpServletResponse response, PageOrderQuery query, ExportField exportField) throws Exception {
        Specification<Order> specification = innerService.orderPageSpecification(query);
        List<Order> orders = orderRepository.findAll(specification);
        List<List<String>> head = new ArrayList<>();
        head.add(Lists.newArrayList("序号"));
        head.add(Lists.newArrayList("工单编号"));
        head.add(Lists.newArrayList("工单类型"));
        head.add(Lists.newArrayList("服务类型"));
        head.add(Lists.newArrayList("保险类型"));
        head.add(Lists.newArrayList("工单状态"));
        List<ExportCell> fields = exportField.getFields();
        fields.forEach(x -> {
            head.add(Lists.newArrayList(x.getTitle()));
        });
        List<List<Object>> data = new ArrayList<>();
        int size = orders.size();
        for (int i = 0; i < size; i++) {
            Order order = orders.get(i);
            Device device = Optional.ofNullable(deviceRepository.findByDevid(order.getDevid())).orElse(new Device());
            List<Object> row = new ArrayList<>();
            row.add(i + 1);
            row.add(order.getInnerCode());
            row.add(CommonUtils.orderTypeConvert(order.getType()));
            row.add(CommonUtils.orderServiceTypeConvert(order.getServiceType()));
            row.add(CommonUtils.orderInsuranceTypeConvert(device.getInsuranceType()));
            row.add(OrderStatusEnums.fromCode(order.getStatus()));
            innerService.getOneLow(order, fields, row);
            data.add(row);
        }
        EasyExcelUtils.exportXlsx(response, "维保中心飞机维保进展表", "Sheet1", head, data);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addOrderByReceive(OrderByReceive request) throws Exception {
        Order order = new Order();
        String uid = CommonUtils.getUid();
        if (CommonUtils.int2BBoolean(request.getSecondary())) {
            Order beforeOrder = orderRepository.findByInnerCode(request.getRelationOrderCode());
            if (Objects.isNull(beforeOrder)) {
                return BaseResponse.fail("关联工单号不存在");
            }
        }
        //处理用户信息
        Customer customer = customersRepository.getByCid(request.getCid());
        order.setCid(customer.getCid());
        User user = userRepository.findByMobile(request.getTelphone());
        if (Objects.isNull(user)) {
            user = innerService.createUser(request.getContact(), request.getTelphone(), customer);
        }
        order.setCuid(user.getUid());
        Address address = addressRepository.findByCidAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(customer.getCid(), request.getTelphone(), request.getContact(), request.getProvince(), request.getCity(), request.getCounty(), request.getAddress());
        if (Objects.isNull(address)) {
            address = new Address();
            address.setProvince(request.getProvince());
            address.setCity(request.getCity());
            address.setCid(customer.getCid());
            address.setCounty(request.getCounty());
            address.setDetail(request.getAddress());
            address.setAid(CommonUtils.uuid());
            address.setContactTel(request.getTelphone());
            address.setContactUser(request.getContact());
            address.setIsDefault(Boolean.FALSE);
            addressRepository.save(address);
        }
        order.setAid(address.getAid());
        //处理设备信息
        Device device = deviceRepository.findByCode(request.getCode());
        if (Objects.isNull(device)) {
            device = new Device();
            device.setCode(request.getCode());
            device.setInnerCode(CommonUtils.uuid());
            device.setActivateAt(request.getActivateAt());
            device.setProduceAt(request.getProduceAt());
            device.setPid(request.getPid());
            device.setDevid(CommonUtils.uuid());
            device.setCid(customer.getCid());
            device.setUid(user.getUid());
            device.setBatteryCode(request.getBatteryCode());
            device.setRemoteCode(request.getRemoteCode());
            device.setAnother(request.getAnother());
        }
        device.setStatus(OrderConstants.DEVICE_STATUS_NORMAL);
        if (OrderConstants.ORDER_MAINTAIN.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_MAINTAIN);
        }
        if (OrderConstants.ORDER_UPKEEP.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_UPKEEP);
        }
        device.setExchangeCount(request.getExchangeCount());
        device.setInsuranceCode(request.getInsuranceCode());
        device.setInsuranceType(request.getInsuranceType());
        device.setInsuranceStartAt(request.getInsuranceStartAt());
        device.setInsuranceExpireAt(request.getInsuranceExpireAt());
        device.setInsuranceTotalQuota(request.getInsuranceTotalQuota());
        device.setInsuranceUseQuota(request.getInsuranceUseQuota());
        device.setQuota(request.getQuota());
        device.setRegistrationNumber(request.getRegistrationNumber());
        deviceRepository.save(device);
        //工单信息
        order.setDevid(device.getDevid());
        order.setStatus(ORDER_STATUS_WAIT_EXPRESSES.getCode());
        order.setServiceType(request.getServiceType());
        order.setRemark(request.getRemark());
        order.setDescription(request.getDescription());
        order.setUid(uid);
        order.setCpid(request.getCpid());
        //如果是鼎和保险需要客户先提交保险资料
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(request.getInsuranceType())) {
            order.setReviewInsuranceStatus(INSURANCE_FIRST_WAITING_SUBMIT.getCode());
            innerService.sendSms(order, customConfig.getSummitFirstInsurance());
        }
        order.setRelationOrderCode(request.getRelationOrderCode());
        order.setType(request.getType());
        order.setServiceType(request.getServiceType());
        order.setSecondary(request.getSecondary());
        order.setLevel(request.getLevel());
        order.setOrdid(CommonUtils.uuid());
        order.setInnerCode(this.innerService.getOrderInnerCode());
        order.setOrigin(ORIGIN_WEB_SYSTEM);
        innerService.generateQrCode(order);
        orderRepository.save(order);
        OrderLog orderLog = innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CREATE);
        innerService.saveOrderFileByLogId(order.getOrdid(), orderLog.getOrdlogid(), request);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addScrapOrder(ScrapOrderByReceive request) throws Exception {
        Order order = new Order();
        String uid = CommonUtils.getUid();
        String cpid = CommonUtils.getCpid();
        //处理用户信息
        Customer customer = customersRepository.getByCid(request.getCid());
        order.setCid(customer.getCid());
        User user = userRepository.findByMobile(request.getTelphone());
        if (Objects.isNull(user)) {
            user = innerService.createUser(request.getContact(), request.getTelphone(), customer);
        }
        order.setCuid(user.getUid());
        Address address = addressRepository.findByCidAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(customer.getCid(), request.getTelphone(), request.getContact(), request.getProvince(), request.getCity(), request.getCounty(), request.getAddress());
        if (Objects.isNull(address)) {
            address = new Address();
            address.setProvince(request.getProvince());
            address.setCity(request.getCity());
            address.setCid(customer.getCid());
            address.setCounty(request.getCounty());
            address.setDetail(request.getAddress());
            address.setAid(CommonUtils.uuid());
            address.setContactTel(request.getTelphone());
            address.setContactUser(request.getContact());
            address.setIsDefault(Boolean.FALSE);
            addressRepository.save(address);
        }
        order.setAid(address.getAid());
        //处理设备信息
        Device device = deviceRepository.findByCode(request.getCode());
        if (Objects.isNull(device)) {
            device = new Device();
            device.setCode(request.getCode());
            device.setInnerCode(CommonUtils.uuid());
            device.setActivateAt(request.getActivateAt());
            device.setProduceAt(request.getProduceAt());
            device.setPid(request.getPid());
            device.setDevid(CommonUtils.uuid());
            device.setCid(customer.getCid());
            device.setUid(user.getUid());
        }
        device.setStatus(OrderConstants.DEVICE_STATUS_NORMAL);
        if (OrderConstants.ORDER_MAINTAIN.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_MAINTAIN);
        }
        if (OrderConstants.ORDER_UPKEEP.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_UPKEEP);
        }
        deviceRepository.save(device);
        //工单信息
        order.setDevid(device.getDevid());
        order.setStatus(ORDER_STATUS_WAIT_EXPRESSES.getCode());
        order.setServiceType(request.getServiceType());
        order.setDescription(request.getDescription());
        order.setUid(uid);
        order.setCpid(cpid);
        order.setOrdid(CommonUtils.uuid());
        order.setType(request.getType());
        order.setServiceType(request.getServiceType());
        order.setLevel(request.getLevel());
        order.setOrdid(CommonUtils.uuid());
        order.setInnerCode(this.innerService.getOrderInnerCode());
        order.setOrigin(ORIGIN_WEB_SYSTEM);
        orderRepository.save(order);
        innerService.generateQrCode(order);
        this.innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CREATE);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addOrderByUser(OrderByUser request) throws Exception {
        //是否手工录入
        if (Objects.nonNull(request.getManual()) && !request.getManual() && OrderConstants.INSURANCE_DH.equals(request.getInsuranceType())) {
            Insurance insurance = insuranceRepository.findByInsuranceCode(request.getInsuranceCode());
            if (Objects.isNull(insurance)) {
                return BaseResponse.fail("保单信息未找到");
            }
        }
        Order order = new Order();
        String uid = CommonUtils.getUid();
        User user = userRepository.findByUid(uid);
        Customer customer = customersRepository.findByCid(user.getCid());
        order.setCuid(uid);
        order.setCid(customer.getCid());
        order.setManual(request.getManual());
        Address address = addressRepository.findByCidAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(customer.getCid(), request.getTelphone(), request.getContact(), request.getProvince(), request.getCity(), request.getCounty(), request.getAddress());
        if (Objects.isNull(address)) {
            address = new Address();
            address.setProvince(request.getProvince());
            address.setCity(request.getCity());
            address.setCid(customer.getCid());
            address.setCounty(request.getCounty());
            address.setDetail(request.getAddress());
            address.setAid(CommonUtils.uuid());
            address.setContactTel(request.getTelphone());
            address.setContactUser(request.getContact());
            address.setIsDefault(Boolean.FALSE);
            addressRepository.save(address);
        }
        order.setAid(address.getAid());
        //处理设备信息
        Device device = deviceRepository.findByCode(request.getCode());
        if (Objects.isNull(device)) {
            device = new Device();
            device.setCode(request.getCode());
            device.setInnerCode(CommonUtils.uuid());
            device.setActivateAt(request.getActivateAt());
            device.setProduceAt(request.getProduceAt());
            device.setPid(request.getPid());
            device.setDevid(CommonUtils.uuid());
            device.setCid(customer.getCid());
            device.setUid(user.getUid());
            device.setBatteryCode(request.getBatteryCode());
            device.setRemoteCode(request.getRemoteCode());
            device.setAnother(request.getAnother());
        }
        device.setStatus(OrderConstants.DEVICE_STATUS_NORMAL);
        if (OrderConstants.ORDER_MAINTAIN.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_MAINTAIN);
        }
        if (OrderConstants.ORDER_UPKEEP.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_UPKEEP);
        }
        device.setExchangeCount(request.getExchangeCount());
        device.setInsuranceCode(request.getInsuranceCode());
        device.setInsuranceType(request.getInsuranceType());
        device.setInsuranceStartAt(request.getInsuranceStartAt());
        device.setInsuranceExpireAt(request.getInsuranceExpireAt());
        device.setQuota(request.getQuota());
        device.setRegistrationNumber(request.getRegistrationNumber());
        deviceRepository.save(device);
        //工单信息
        order.setDevid(device.getDevid());
        order.setStatus(ORDER_STATUS_WAIT_EXPRESSES.getCode());
        order.setServiceType(request.getServiceType());
        order.setDescription(request.getDescription());
        order.setUid(uid);
        order.setCpid(request.getCpid());
        order.setOrdid(CommonUtils.uuid());
        order.setType(request.getType());
        order.setLevel(request.getLevel());
        order.setServiceType(request.getServiceType());
        order.setSecondary(request.getSecondary());
        order.setInnerCode(this.innerService.getOrderInnerCode());
        order.setOrigin(ORIGIN_WEB_USER);
        order.setReceiveNo(request.getReceiveNo());
        innerService.generateQrCode(order);
        Expresses expresses = new Expresses();
        expresses.setExpid(CommonUtils.uuid());
        expresses.setType(CommonConstants.EXPRESSES_DEVICE_RECEIVE);
        expresses.setUid(uid);
        expresses.setOrdid(order.getOrdid());
        expresses.setStatus(CommonConstants.EXPRESS_STATUS_NORMAL);
        expresses.setWaybillNo(request.getReceiveNo());
        expressesRepository.save(expresses);
        //如果是鼎和保险需要客户先提交保险资料
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(request.getInsuranceType())) {
            //保存出险材料 保单号截图
            if (StringUtils.isNotBlank(request.getInsuranceScreenshot())) {
                CustomFile customFile = fileRepository.findByFid(request.getInsuranceScreenshot());
                if (Objects.nonNull(customFile)) {
                    customFile.setOrdid(order.getOrdid());
                    customFile.setType(CommonConstants.FILE_TYPE_INSURANCE_SHOT);
                    fileRepository.save(customFile);
                }
            }
            innerService.saveFirstInsuranceMaterial(order, request);
            InsuranceMaterial material = new InsuranceMaterial();
            BeanUtils.copyProperties(request, material);
            if (StringUtils.isNotBlank(request.getReportNo())) {
                material.setBeReport(Boolean.TRUE);
            }
            material.setOrdid(order.getOrdid());
            material.setImid(CommonUtils.uuid());
            insuranceMaterialRepository.save(material);
            order.setReviewInsuranceStatus(INSURANCE_FIRST_REVIEW.getCode());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CREATE);
        //鼎和保险生成保险文件
        if (!CommonUtils.int2BBoolean(order.getSecondary()) && ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            innerService.generateFirstMaterial(order);
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<List<FilePreview>> materialPreview(InsurancePreview request) throws Exception {
        String code = null;
        if (Objects.nonNull(request.getCpid())) {
            Company company = companyRepository.findByCpid(request.getCpid());
            if (Objects.nonNull(company)) {
                code = company.getTemplateCode();
            }
        }

        List<FilePreview> files = Lists.newArrayList();
        //赔付意向书
        files.add(this.insuranceFileService.generateIntentionPayFile(code, request));
        //电力业务出险通知及索赔申请书
        files.add(this.insuranceFileService.generatePowerRiskFile(code, request));
        //损失清单
        files.add(this.insuranceFileService.generateLossFile(code, request));
        //无人机故障报告
        files.add(this.insuranceFileService.generateFaultFile(code, request));
        //事故照片
        files.add(this.insuranceFileService.generateFaultPhotoFile(code, request));
        return BaseResponse.success(files);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> receiveExpress(String orderId, ReceiveExpress request) throws Exception {
        if (request.checkFileIsNull()) {
            return BaseResponse.fail("附件为必填");
        }
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAIT_EXPRESSES.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        String uid = CommonUtils.getUid();
        if (StringUtils.isNotBlank(request.getReceiveNo())) {
            order.setReceiveNo(request.getReceiveNo());
        }
        order.setReceiveUid(uid);
        if (ORDER_SERVICE_INVALID.equals(order.getServiceType())) {
            order.setStatus(ORDER_STATUS_WAIT_SCRAP_REVIEW.getCode());
        } else {
            order.setStatus(ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode());
        }
        order.setSecondary(request.getSecondary());
        order.setRelationOrderCode(request.getRelationOrderCode());
        if (CommonUtils.int2BBoolean(order.getSecondary())) {
            Order beforeOrder = orderRepository.findByInnerCode(order.getRelationOrderCode());
            if (Objects.isNull(beforeOrder)) {
                return BaseResponse.fail("关联工单号不存在");
            }
        }
        if (Objects.nonNull(request.getSecondary())) {
            order.setSecondary(request.getSecondary());
        }
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (Objects.isNull(device)) {
            throw new CustomBizException("未找到工单中的设备");
        }
        BeanUtils.copyProperties(request, device, CommonUtils.getNullPropertyNames(request));
        deviceRepository.save(device);
        Expresses expresses = expressesRepository.findFirstByOrdidAndType(orderId, CommonConstants.EXPRESSES_DEVICE_RECEIVE);
        if (Objects.isNull(expresses)) {
            expresses = new Expresses();
            expresses.setExpid(CommonUtils.uuid());
            expresses.setType(CommonConstants.EXPRESSES_DEVICE_RECEIVE);
            //默认自寄
            expresses.setPickup(OrderConstants.EXPRESSES_SELF);
            expresses.setUid(uid);
            expresses.setOrdid(orderId);
        }
        expresses.setRemark(request.getRemark());
        expresses.setStatus(CommonConstants.EXPRESS_STATUS_NORMAL);
        expresses.setWaybillNo(request.getReceiveNo());
        expresses.setExpressesAt(request.getReceiveAt());
        expressesRepository.save(expresses);
        //如果是二次返修，则跟上次维修方式保持一致
        if (CommonUtils.int2BBoolean(order.getSecondary())) {
            Order before = orderRepository.findByInnerCode(order.getRelationOrderCode());
            order.setServiceMethod(before.getServiceMethod());
            order.setReviewInsuranceStatus(INSURANCE_FINISH.getCode());
            //返厂维修
            if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {
                order.setStatus(ORDER_STATUS_WAIT_LOSS_REVIEW.getCode());
            } else {
                order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
            }
        } else {
            if (!ORDER_SERVICE_INVALID.equals(order.getServiceType())) {
                innerService.sendSms(order, customConfig.getConfirmDeviceTemplate1());
            }
        }
        orderRepository.save(order);
        OrderLog orderLog = innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CONFIRM_EXPRESSES);
        innerService.saveOrderFileByLogId(order.getOrdid(), orderLog.getOrdlogid(), request);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> confirmDevice(String orderId) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        Device device = deviceRepository.findByDevid(order.getDevid());
        //鼎和出版资料已完成
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            if (INSURANCE_FIRST_FINISH.getCode().equals(order.getReviewInsuranceStatus())) {
                order.setStatus(ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
            } else {
                order.setStatus(ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode());
            }
        } else {
            order.setStatus(ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CONFIRM_DEVICE);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewFirstInsurance(String orderId, ReviewResult request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        String uid = CommonUtils.getUid();
        if (!CommonUtils.checkStatus(order.getReviewInsuranceStatus(), INSURANCE_FIRST_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_INSURANCE_MESSAGE, InsuranceStatusEnums.fromCode(order.getReviewInsuranceStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            //设备已确认，则变为保险待确认，否则等待用户确认设备
            if (ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode().equals(order.getStatus())) {
                order.setStatus(ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode());
            }
            order.setReviewInsuranceStatus(INSURANCE_FIRST_FINISH.getCode());
        } else {
            order.setReviewInsuranceStatus(INSURANCE_FIRST_REJECT.getCode());
            innerService.sendSms(order, customConfig.getReviewFirstInsurance());
        }
        order.setFirstReviewRemark(request.getReviewRemark());
        order.setFirstReviewUid(uid);
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_FIRST_INSURANCE_REVIEW);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewLastInsurance(String orderId, ReviewResult request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        String uid = CommonUtils.getUid();
        if (!CommonUtils.checkStatus(order.getReviewInsuranceStatus(), INSURANCE_LAST_WAITING_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_INSURANCE_MESSAGE, InsuranceStatusEnums.fromCode(order.getReviewInsuranceStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            order.setReviewInsuranceStatus(INSURANCE_WAITING_FINISH.getCode());
            //本地维修，直接派单
            if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
                order.setStatus(ORDER_STATUS_WAITING_DISPATCH.getCode());
            } else {
                order.setStatus(ORDER_STATUS_WAITING_SETTLE.getCode());//返厂维修，厂家预付款
            }
            Device device = deviceRepository.findByDevid(order.getDevid());
            if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
                innerService.generateLastMaterial(order);
            }
        } else {
            order.setReviewInsuranceStatus(INSURANCE_LAST_REJECT.getCode());
            innerService.sendSms(order, customConfig.getReviewLastInsurance());
        }
        order.setLastReviewRemark(request.getReviewRemark());
        order.setLastReviewUid(uid);
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_LAST_INSURANCE_REVIEW);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> finishInsurance(String orderId) {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getReviewInsuranceStatus(), INSURANCE_WAITING_FINISH.getCode())) {
            return BaseResponse.fail(String.format(CHECK_INSURANCE_MESSAGE, InsuranceStatusEnums.fromCode(order.getReviewInsuranceStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        CustomFile customFile = fileRepository.getByOrdidAndType(orderId, CommonConstants.INSURANCE_ARCHIVED);
        if (Objects.isNull(customFile)) {
            return BaseResponse.fail("保险资料未归档");
        }
        order.setReviewInsuranceStatus(INSURANCE_FINISH.getCode());
        //已寄件工单就完成
        if (ORDER_STATUS_FINISH_CUSTOMER_EXPRESSES.getCode().equals(order.getStatus())) {
            order.setStatus(ORDER_STATUS_FINISH.getCode());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_FINISH_INSURANCE);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewInsurance(String orderId, AuditInsurance request) throws Exception {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode(), OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        BeanUtils.copyProperties(request, device, CommonUtils.getNullPropertyNames(request));
        deviceRepository.save(device);
        String beforeServiceType = order.getServiceType();
        order.setServiceType(request.getServiceType());
        Insurance insurance = innerService.createInsurance(order);
        //鼎和保险-需要初版资料审核通过
        if (Objects.nonNull(insurance) && INSURANCE_DH.equals(insurance.getInsuranceType())) {
            order.setIid(insurance.getIid());
            //表示之前是框架合同维修，则需要提交保险资料
            if (!ORDER_IN_INSURANCE.equals(beforeServiceType)) {
                order.setReviewInsuranceStatus(INSURANCE_FIRST_WAITING_SUBMIT.getCode());
                order.setStatus(ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode());
            } else {
                order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
            }
        } else {
            //非鼎和的，设备已确认，则变为待定损
            order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
            order.setReviewInsuranceStatus(null);
        }
        order.setPjid(request.getPjid());
        order.setReviewInsuranceUid(uid);
        order.setReviewInsuranceRemark(request.getReviewInsuranceRemark());
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_REVIEW_INSURANCE);
        orderRepository.save(order);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deleteOrder(String orderId) throws Exception {
        Order order = orderRepository.findByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), ORDER_STATUS_FINISH.getCode(), ORDER_STATUS_CANCEL.getCode(), ORDER_STATUS_INVALIDATE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, order.getStatus()));
        }
        orderRepository.deleteByOrdid(orderId);
        fileRepository.deleteByOrdid(orderId);
        expressesRepository.deleteByOrdid(orderId);
        orderSettleRepository.deleteByOrdid(orderId);
        insuranceMaterialRepository.deleteByOrdid(orderId);
        List<OrderQuote> orderQuotes = orderQuoteRepository.findByOrdid(orderId);
        orderQuotes.forEach(x -> {
            orderQuoteItemRepository.deleteByOqid(x.getOqid());
        });
        if (CollectionUtils.isNotEmpty(orderQuotes)) {
            orderQuoteRepository.deleteAll(orderQuotes);
        }
        return BaseResponse.success();
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deleteOrder(List<String> orderIds) throws Exception {
        for (String orderId : orderIds) {
            this.deleteOrder(orderId);
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<DetailOrder> detailOrder(String orderId) throws Exception {
        Order order = orderRepository.findByOrdid(orderId);
        if (Objects.isNull(order)) {
            return BaseResponse.fail("工单ID错误");
        }
        DetailOrder detailOrder = new DetailOrder();
        BeanUtils.copyProperties(order, detailOrder);
        detailOrder.setCreatedAt(DateUtils.formatDateTime(order.getCt()));
        detailOrder.setUpdatedAt(DateUtils.formatDateTime(order.getUt()));
        if (Objects.nonNull(order.getFinishAt())) {
            detailOrder.setFinishTime(DateUtils.formatDateTime(order.getFinishAt()));
        }
        if (Objects.nonNull(order.getPjid())) {
            detailOrder.setProject(projectRepository.findByPjid(order.getPjid()));
        }
        CustomFile customFile = fileRepository.getByOrdidAndType(orderId, CommonConstants.FILE_TYPE_QR_CODE);
        detailOrder.setQrCodeUrl(innerService.getFileProxyUrl(customFile));
        //只有首次定损及报价完成后，客户结算才可进行编辑，如果二次及以上的定损及维修，客户报价不允许修改，只展示首次结算的信息。
        OrderLog log1 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_SETTLE_SUBMIT);
        Boolean isSettle = order.getStatus().compareTo(OrderStatusEnums.ORDER_STATUS_WAITING_SETTLE.getCode()) > 0;
        detailOrder.setSettleCanEdit(Objects.isNull(log1) || (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod()) && !isSettle));
        OrderLog log2 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_CREATE);
        detailOrder.setOrderAttachments(innerService.getFileByLog(log2));
        OrderLog log3 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_CONFIRM_EXPRESSES);
        detailOrder.setReceiveAttachments(innerService.getFileByLog(log3));
        List<CustomFile> lossFile = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_QUOTE);
        detailOrder.setLossAttachments(lossFile.stream().map(innerService::getFileProxyUrl).filter(StringUtils::isNotBlank).collect(Collectors.toList()));
        OrderLog log4 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_CHECKED);
        detailOrder.setCheckAttachments(innerService.getFileByLog(log4));
        Optional.ofNullable(order.getCancelAt()).ifPresent(x -> detailOrder.setCancelTime(DateUtils.formatDateTime(x)));
        Expresses expresses = expressesRepository.findFirstByOrdidAndType(orderId, EXPRESSES_DEVICE_RECEIVE);
        Optional.ofNullable(expresses).ifPresent(x -> detailOrder.setReceiveTime(DateUtils.formatDateTime(expresses.getExpressesAt())));
        OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
        if (Objects.nonNull(customerQuote)) {
            double total = Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00);
            detailOrder.setCustomerQuote(total);
        }
        OrderLog log5 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_SETTLE_SUBMIT);
        if (Objects.isNull(log5)) {
            detailOrder.setSettled(Boolean.FALSE);
        } else {
            detailOrder.setSettled(Boolean.TRUE);
        }
        Expresses expresses2 = expressesRepository.findFirstByOrdidAndType(orderId, EXPRESSES_RETURN_FACTORY);
        Optional.ofNullable(expresses2).ifPresent(x -> {
            detailOrder.setReturnPickup(x.getPickup());
            detailOrder.setReturnWaybillNo(x.getWaybillNo());
            detailOrder.setReturnExpressesTime(DateUtils.formatDateTime(x.getExpressesAt()));
        });
        Expresses expresses3 = expressesRepository.findFirstByOrdidAndType(orderId, EXPRESSES_SEND_CUSTOMER);
        Optional.ofNullable(expresses3).ifPresent(x -> {
            detailOrder.setCustomerPickup(x.getPickup());
            detailOrder.setCustomerWaybillNo(x.getWaybillNo());
            detailOrder.setCustomerExpressesTime(DateUtils.formatDateTime(x.getExpressesAt()));
        });
        OrderLog log6 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_DISPATCH);
        Optional.ofNullable(log6).ifPresent(x -> detailOrder.setMaintainStartTime(DateUtils.formatDateTime(log6.getCt())));
        OrderLog log7 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_MAINTAINED);
        Optional.ofNullable(log7).ifPresent(x -> detailOrder.setMaintainEndTime(DateUtils.formatDateTime(log7.getCt())));
        Optional.ofNullable(log7).ifPresent(x -> detailOrder.setCheckStartTime(DateUtils.formatDateTime(log7.getCt())));
        OrderLog log8 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_CHECKED);
        Optional.ofNullable(log8).ifPresent(x -> detailOrder.setCheckEndTime(DateUtils.formatDateTime(log8.getCt())));
        Optional.ofNullable(log8).ifPresent(x -> detailOrder.setCheckResult(log8.getRemark()));
        OrderLog log9 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(orderId, ORDER_LOG_METHOD_QUOTATION);
        Optional.ofNullable(log9).ifPresent(x -> detailOrder.setFirstQuoteTime(DateUtils.formatDateTime(log9.getCt())));
        OrderLog log10 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_RETURN_RECEIVE);
        Optional.ofNullable(log10).ifPresent(x -> detailOrder.setReturnTime(DateUtils.formatDateTime(log10.getCt())));
        List<OrderCategory> orderCategories = orderCategoryRepository.findByOrdid(orderId);
        detailOrder.setOrderCategories(orderCategories);
        List<String> ordCatNames = orderCategories.stream().filter(x -> Objects.nonNull(x.getCategory())).map(x -> x.getCategory().getName()).collect(Collectors.toList());
        detailOrder.setOrdCatNames(ordCatNames);
        OrderLog log11 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_CONFIRM_RE_LOSS);
        OrderLog log12 = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_CHECKED);
        //重新定损或者检修不通过
        detailOrder.setSecondaryQuote(Objects.nonNull(log11) || (Objects.nonNull(log12) && REJECTED.equals(log12.getRemark())));
        CustomFile archivedFile = fileRepository.getByOrdidAndType(orderId, CommonConstants.INSURANCE_ARCHIVED);
        detailOrder.setInsuranceArchived(Objects.nonNull(archivedFile));
        return BaseResponse.success(detailOrder);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> cancelOrderIn(String orderId, Remark request) {
        String uid = CommonUtils.getUid();
        //是否结算过，结算后不允许取消
        OrderLog log = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_SETTLE_SUBMIT);
        if (Objects.nonNull(log)) {
            return BaseResponse.fail("工单已结算，不允许取消");
        }
        Order order = orderRepository.getByOrdid(orderId);
        //待收件直接取消
        if (order.getStatus().equals(ORDER_STATUS_WAIT_EXPRESSES.getCode())) {
            order.setStatus(ORDER_STATUS_CANCEL.getCode());
            //其他情况则需要保险审核员审核，审核通过则为待寄件，不通过则回到原来的状态
        } else {
            order.setPreStatus(order.getStatus());
            order.setStatus(ORDER_STATUS_CANCEL_WAITING1.getCode());
        }
        //不是用户取消，才需要填写取消原因
        if (!CANCEL_TYPE_USER.equals(order.getCancelType())) {
            order.setCancelType(CANCEL_TYPE_SYSTEM);
            order.setCancelUid(uid);
            order.setCancelAt(System.currentTimeMillis());
            order.setCancelReason(request.getRemark());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_CANCEL);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> cancelOrderOut(String orderId, Remark request) {
        String uid = CommonUtils.getUid();
        OrderLog log = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_SETTLE_SUBMIT);
        if (Objects.nonNull(log)) {
            return BaseResponse.fail("工单已结算，不允许取消");
        }
        Order order = orderRepository.getByOrdid(orderId);
        //待收件直接取消,已收件了，则需要保险审核后再取消
        if (order.getStatus().equals(ORDER_STATUS_WAIT_EXPRESSES.getCode())) {
            order.setStatus(ORDER_STATUS_CANCEL.getCode());
        }
        order.setPreStatus(order.getStatus());
        order.setCancelType(CANCEL_TYPE_USER);
        order.setCancelUid(uid);
        order.setCancelAt(System.currentTimeMillis());
        order.setCancelReason(request.getRemark());
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CANCEL);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewCancel(String orderId, ReviewResult request) {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            order.setStatus(ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode());
        } else {
            order.setStatus(order.getPreStatus());
            order.setPreStatus(null);
            order.setCancelType(null);
        }
        order.setReviewCancelUid(uid);
        order.setReviewCancelRemark(request.getReviewRemark());
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_REVIEW_CANCEL);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> revocationOrder(String orderId) {
        Order order = orderRepository.getByOrdid(orderId);
        //表示用户取消
        if (CANCEL_TYPE_USER.equals(order.getCancelType())) {
            //自己取消，但是系统未处理，则直接撤销
            if (!OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode().equals(order.getStatus())) {
                order.setPreStatus(null);
                order.setCancelType(null);
            } else {
                order.setStatus(ORDER_STATUS_CANCEL_WAITING2.getCode());
            }
            orderRepository.save(order);
            innerService.saveOrderLog(order, ORDER_LOG_METHOD_REVOCATION);
            return BaseResponse.success();
        }
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        order.setStatus(ORDER_STATUS_CANCEL_WAITING2.getCode());
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_REVOCATION);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> reviewRevocation(String orderId, ReviewResult request) {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            order.setStatus(order.getPreStatus());
            order.setPreStatus(null);
            order.setCancelType(null);
        } else {
            order.setStatus(ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode());
        }
        order.setReviewRevocationUid(uid);
        order.setReviewRevocationRemark(request.getReviewRemark());
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_REVIEW_REVOCATION);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> confirmLoss(String orderId, ConfirmLoss request) throws Exception {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        order.setLevel(request.getLevel());
        order.setPjid(request.getPjid());
        order.setServiceMethod(request.getServiceMethod());
        //本地维修，直接报价
        if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
            order.setStatus(ORDER_STATUS_WAITING_QUOTATION.getCode());
        } else if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {//返厂维修
            order.setStatus(ORDER_STATUS_WAIT_LOSS_REVIEW.getCode());
        } else {
            throw new CustomBizException("服务方式错误");
        }
        order.setLossUid(uid);
        order.setLossRemark(request.getRemark());
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CONFIRM_LOSS);
        orderRepository.save(order);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reConfirmLoss(String orderId) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (Objects.isNull(order.getReQuote()) || !order.getReQuote()) {
            return BaseResponse.fail("维修失败后才能重新定损");
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
        order.setReQuote(Boolean.FALSE);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CONFIRM_RE_LOSS);
        orderRepository.save(order);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> reviewLoss(String orderId, ReviewResult request) {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAIT_LOSS_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            //返厂维修
            if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {
                order.setStatus(ORDER_STATUS_RETURN_WAITING_EXPRESSES.getCode());
            } else {
                order.setStatus(ORDER_STATUS_WAITING_QUOTATION.getCode());
            }
        } else {
            order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
        }
        order.setReviewLossRemark(request.getReviewRemark());
        order.setReviewLossUid(uid);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_WAIT_LOSS_AUDIT);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> returnExpress(String orderId, ReturnExpresses request) throws Exception {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_RETURN_WAITING_EXPRESSES.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        order.setReturnCaseNumber(request.getReturnCaseNumber());
        //快递自寄
        if (OrderConstants.EXPRESSES_SELF.equals(request.getType())) {
            Expresses expresses = new Expresses();
            expresses.setExpid(CommonUtils.uuid());
            expresses.setType(CommonConstants.EXPRESSES_RETURN_FACTORY);
            expresses.setUid(uid);
            expresses.setOrdid(orderId);
            expresses.setStatus(CommonConstants.EXPRESS_STATUS_NORMAL);
            expresses.setWaybillNo(request.getExpressNo());
            expresses.setExpressesAt(request.getExpressTime());
            expresses.setPickup(request.getType());
            expressesRepository.save(expresses);
        } else if (OrderConstants.EXPRESSES_PICKUP.equals(request.getType())) {//快递上门
            Address sendAddress = innerService.systemIfNotPresent(request.getSender());
            Address receiveAddress = innerService.systemIfNotPresent(request.getReceiver());
            Expresses expresses;
            if (EXPRESS_EMS.equals(expressType)) {
                expresses = emsExpressService.createOrder(orderId, sendAddress, receiveAddress);
            } else {
                expresses = expressesService.createOrder(orderId, sendAddress, receiveAddress);
            }
            expresses.setType(CommonConstants.EXPRESSES_RETURN_FACTORY);
            expresses.setSenderId(uid);
            expresses.setPickup(request.getType());
            expresses.setUid(uid);
            expresses.setSenderId(sendAddress.getAid());
            expresses.setReceiverId(receiveAddress.getAid());
            expressesRepository.save(expresses);
        } else {
            throw new CustomBizException("快递类型参数错误");
        }
        order.setStatus(ORDER_STATUS_WAITING_QUOTATION.getCode());
        order.setReturnSendUid(uid);
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_RETURN_EXPRESSES);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> quoteOrder(String orderId, OrderQuoteRequest request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        String uid = CommonUtils.getUid();
        if (Objects.isNull(order)) {
            return BaseResponse.fail("工单ID错误");
        }
        List<String> ordcatids = request.getOrdcatids();
        //保存故障
        orderCategoryRepository.deleteByOrdid(orderId);
        List<OrderCategory> orderCategories = ordcatids.stream().map(x -> {
            OrderCategory orderCategory = new OrderCategory();
            orderCategory.setCatid(x);
            orderCategory.setOrdcatid(CommonUtils.uuid());
            orderCategory.setOrdid(orderId);
            return orderCategory;
        }).collect(Collectors.toList());
        orderCategoryRepository.saveAll(orderCategories);
        //关联定损照片
        List<CustomFile> existCustomFiles = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_QUOTE);
        existCustomFiles.forEach(x -> {
            x.setOrdid(null);
            x.setType(null);
        });
        fileRepository.saveAll(existCustomFiles);
        List<String> fids = request.getFids();
        if (CollectionUtils.isNotEmpty(fids)) {
            List<CustomFile> customFiles = fids.stream().map(x -> {
                CustomFile customFile = fileRepository.findByFid(x);
                customFile.setType(CommonConstants.FILE_TYPE_QUOTE);
                customFile.setOrdid(orderId);
                return customFile;
            }).collect(Collectors.toList());
            fileRepository.saveAll(customFiles);
        }
        //成本报价单
        Quote costQuote = request.getCostQuotes();
        if (Objects.nonNull(costQuote)) {
            OrderQuote orderQuote;
            if (Objects.nonNull(costQuote.getId())) {
                orderQuote = orderQuoteRepository.getById(costQuote.getId());
            } else {
                orderQuote = new OrderQuote();
                orderQuote.setOqid(CommonUtils.uuid());
                orderQuote.setType(OrderConstants.QUOTE_TYPE_COST);
                long quoteOrder = orderQuoteRepository.countByOrdidAndType(orderId, OrderConstants.QUOTE_TYPE_COST);
                orderQuote.setOrder((int) quoteOrder + 1);
            }
            orderQuote.setExpressesPrice(Optional.ofNullable(costQuote.getExpressesPrice()).orElse(0.0));
            orderQuote.setOtherPrice(Optional.ofNullable(costQuote.getOtherPrice()).orElse(0.0));
            orderQuote.setSparePrice(0.0);
            orderQuote.setOtherRemark(costQuote.getOtherRemark());
            orderQuote.setUid(uid);
            orderQuote.setOrdid(orderId);
            List<QuoteItem> quoteItems = costQuote.getQuoteItems();
            if (CollectionUtils.isNotEmpty(quoteItems)) {
                Map<String, OrderQuoteItem> items = orderQuoteItemRepository.findByOqid(orderQuote.getOqid()).stream().collect(Collectors.toMap(OrderQuoteItem::getPid, item -> item));
                List<OrderQuoteItem> orderQuoteItems = new ArrayList<>();
                for (QuoteItem y : quoteItems) {
                    OrderQuoteItem quoteItem = items.get(y.getPid());
                    if (Objects.isNull(quoteItem)) {
                        quoteItem = new OrderQuoteItem();
                        quoteItem.setOqid(orderQuote.getOqid());
                        quoteItem.setOqiid(CommonUtils.uuid());
                    }
                    quoteItem.setPid(y.getPid());
                    quoteItem.setCount(y.getCount());
                    ProjectProduct projectProduct = projectProductsRepository.findByPjidAndPid(order.getPjid(), y.getPid());
                    //本地维修,成本价为出库价格
                    if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
                        Store defaultStore = storeRepository.findFirstByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, StocksConstants.STORE_SPARE, CommonUtils.getCpid());
                        Stock stock = stockRepository.findByPidAndStoreid(y.getPid(), defaultStore.getStoreid());
                        if (Objects.isNull(stock)) {
                            throw new CustomBizException("获取存库价格失败");
                        }
                        quoteItem.setSalePrice(stock.getRealPrice());
                    } else if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {//返厂维修
                        Product product = productRepository.findByPid(y.getPid());
                        quoteItem.setSalePrice(product.getFactoryPrice().doubleValue());
                    } else {
                        quoteItem.setSalePrice(projectProduct.getSalePrice().doubleValue());
                    }
                    if (Objects.nonNull(projectProduct.getProduct())) {
                        quoteItem.setCatid(projectProduct.getProduct().getCatid());
                    }
                    //成本报价的工时费为0
                    quoteItem.setWorkHour(0.0);
                    quoteItem.setWorkHourCost(0.0);
                    quoteItem.setTotalCost(quoteItem.getWorkHourCost() + quoteItem.getCount() * quoteItem.getSalePrice());
                    orderQuoteItems.add(quoteItem);
                    items.remove(y.getPid());
                }
                orderQuote.setSparePrice(orderQuoteItems.stream().mapToDouble(OrderQuoteItem::getTotalCost).sum());
                orderQuoteItemRepository.saveAll(orderQuoteItems);
                orderQuoteItemRepository.deleteAll(items.values());
            } else {
                orderQuoteItemRepository.deleteByOqid(orderQuote.getOqid());
            }
            orderQuoteRepository.save(orderQuote);
        }
        //客户成本单保存
        Quote customerQuote = request.getCustomerQuote();
        if (Objects.nonNull(customerQuote)) {
            OrderLog log = orderLogRepository.findFirstByOrdidAndMethodOrderByCtDesc(orderId, ORDER_LOG_METHOD_QUOTATION_AUDIT);
            //有审核报价通过的日志，第二次报价,二次报价不需要修改客户的报价单
            if (Objects.nonNull(log) && PASS.equals(log.getRemark())) {
                return BaseResponse.success();
            }
            OrderQuote orderQuote;
            if (Objects.nonNull(customerQuote.getId())) {
                orderQuote = orderQuoteRepository.getById(customerQuote.getId());
            } else {
                orderQuote = new OrderQuote();
                orderQuote.setOqid(CommonUtils.uuid());
                orderQuote.setType(QUOTE_TYPE_CUSTOMER);
            }
            orderQuote.setOqid(CommonUtils.uuid());
            orderQuote.setExpressesPrice(Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.0));
            orderQuote.setOtherPrice(Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.0));
            orderQuote.setSparePrice(0.0);
            orderQuote.setOtherRemark(customerQuote.getOtherRemark());
            orderQuote.setUid(uid);
            orderQuote.setOrdid(orderId);
            List<QuoteItem> quoteItems = customerQuote.getQuoteItems();
            if (CollectionUtils.isNotEmpty(quoteItems)) {
                Map<String, OrderQuoteItem> items = orderQuoteItemRepository.findByOqid(orderQuote.getOqid()).stream().collect(Collectors.toMap(OrderQuoteItem::getPid, item -> item));
                List<OrderQuoteItem> orderQuoteItems = quoteItems.stream().map(y -> {
                    //客户报价，价格库价格
                    ProjectProduct projectProduct = projectProductsRepository.findByPjidAndPid(order.getPjid(), y.getPid());
                    OrderQuoteItem quoteItem = items.get(y.getPid());
                    if (Objects.isNull(quoteItem)) {
                        quoteItem = new OrderQuoteItem();
                        quoteItem.setOqid(orderQuote.getOqid());
                        quoteItem.setOqiid(CommonUtils.uuid());
                    }
                    quoteItem.setPid(y.getPid());
                    quoteItem.setCount(y.getCount());
                    if (Objects.nonNull(projectProduct.getProduct())) {
                        quoteItem.setCatid(projectProduct.getProduct().getCatid());
                    }
                    quoteItem.setWorkHour(projectProduct.getWorkHour().doubleValue());
                    quoteItem.setSalePrice(projectProduct.getSalePrice().doubleValue());
                    quoteItem.setWorkHourCost(projectProduct.getWorkHourCost().doubleValue());
                    quoteItem.setTotalCost((quoteItem.getWorkHourCost() + quoteItem.getSalePrice()) * quoteItem.getCount());
                    items.remove(y.getPid());
                    return quoteItem;
                }).collect(Collectors.toList());
                orderQuote.setSparePrice(orderQuoteItems.stream().mapToDouble(OrderQuoteItem::getTotalCost).sum());
                orderQuoteItemRepository.saveAll(orderQuoteItems);
                orderQuoteItemRepository.deleteAll(items.values());
            } else {
                orderQuoteItemRepository.deleteByOqid(orderQuote.getOqid());
            }
            orderQuoteRepository.save(orderQuote);
        }
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> submitQuote(String orderId, Remark request) {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        String uid = CommonUtils.getUid();
        order.setStatus(ORDER_STATUS_WAITING_QUOTATION_REVIEW.getCode());
        order.setQuoteRemark(request.getRemark());
        order.setQuoteUid(uid);
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_QUOTATION);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewQuote(String orderId, ReviewQuoteResult request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION_REVIEW.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        String uid = CommonUtils.getUid();
        order.setReviewQuoteRemark(request.getReviewRemark());
        order.setReviewQuoteUid(uid);
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            if (Objects.isNull(request.getBeSettle())) {
                return BaseResponse.fail("是否收费参数错误");
            }
            order.setBeSettle(request.getBeSettle());
            OrderLog log = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(orderId, ORDER_LOG_METHOD_QUOTATION_AUDIT);
            //有审核报价通过的日志，则位第二次报价
            if (Objects.nonNull(log) && PASS.equals(log.getRemark())) {
                //表示二次报价
                //本地维修
                if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
                    order.setStatus(ORDER_STATUS_WAITING_DISPATCH.getCode());
                } else {
                    order.setStatus(ORDER_STATUS_WAITING_SETTLE.getCode());
                }
            } else {
                //二次返修
                if (CommonUtils.int2BBoolean(order.getSecondary())) {
                    if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
                        order.setStatus(ORDER_STATUS_WAITING_DISPATCH.getCode());
                    } else {
                        order.setStatus(ORDER_STATUS_WAITING_SETTLE.getCode());
                    }
                } else {
                    //是否收费
                    if (order.getBeSettle()) {
                        order.setStatus(ORDER_STATUS_WAITING_QUOTATION_CONFIRM.getCode());
                        innerService.sendSms(order, customConfig.getConfirmQuote());
                    } else {
                        if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
                            order.setStatus(ORDER_STATUS_WAITING_DISPATCH.getCode());
                        } else {
                            order.setStatus(ORDER_STATUS_WAITING_SETTLE.getCode());
                        }
                    }
                }
            }
            innerService.generateQuoteMaterial(order);
        } else {
            order.setStatus(ORDER_STATUS_WAITING_QUOTATION.getCode());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_QUOTATION_AUDIT);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> confirmQuote(String orderId) {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION_CONFIRM.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        //报价确认后
        Device device = deviceRepository.findByDevid(order.getDevid());
        //保内维修且为鼎和保险
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            order.setReviewInsuranceStatus(INSURANCE_LAST_WAITING_SUBMIT.getCode());
            order.setStatus(ORDER_STATUS_INSURANCE_MATERIAL_GATHER.getCode());
            innerService.sendSms(order, customConfig.getSummitLastInsurance());
        } else {
            order.setStatus(ORDER_STATUS_WAITING_SETTLE.getCode());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CONFIRM_QUOTATION);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> customerSettle(String orderId, CustomerSettle request) {
        Order order = orderRepository.getByOrdid(orderId);
        if (Objects.isNull(order)) {
            return BaseResponse.fail("工单ID错误");
        }
        if (!order.getBeSettle()) {
            return BaseResponse.fail("工单不需要收费");
        }
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_SETTLE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        OrderSettle customerSettle;
        if (Objects.isNull(request.getId())) {
            customerSettle = new OrderSettle();
            customerSettle.setOrdid(orderId);
            customerSettle.setSettleId(CommonUtils.uuid());
            customerSettle.setType(SETTLE_TYPE_CUSTOMER);
        } else {
            customerSettle = orderSettleRepository.getById(request.getId());
        }
        customerSettle.setUid(CommonUtils.getUid());
        BeanUtils.copyProperties(request, customerSettle, CommonUtils.getNullPropertyNames(request));
        orderSettleRepository.save(customerSettle);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_SETTLE);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> advanceSettle(String orderId, AdvanceSettle request) {
        Order order = orderRepository.getByOrdid(orderId);
        if (Objects.isNull(order)) {
            return BaseResponse.fail("工单ID错误");
        }
        if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
            return BaseResponse.fail("本地维修不涉及返厂预付款");
        }
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_SETTLE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        OrderSettle advanceSettle;
        if (Objects.isNull(request.getId())) {
            advanceSettle = new OrderSettle();
            advanceSettle.setOrdid(orderId);
            advanceSettle.setType(SETTLE_TYPE_FACTORY);
            advanceSettle.setSettleId(CommonUtils.uuid());
        } else {
            advanceSettle = orderSettleRepository.getById(request.getId());
        }
        advanceSettle.setUid(CommonUtils.getUid());
        BeanUtils.copyProperties(request, advanceSettle, CommonUtils.getNullPropertyNames(request));
        orderSettleRepository.save(advanceSettle);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_SETTLE);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> submitSettle(String orderId) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_SETTLE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        if (CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode(), OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode())) {
            return BaseResponse.fail("工单取消中，无法进行当前操作");
        }
        order.setSettleUid(CommonUtils.getUid());
        //本地维修
        if (OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN.equals(order.getServiceMethod())) {
            order.setStatus(ORDER_STATUS_WAITING_DISPATCH.getCode());
            //改为自己申请出库
//            Store defaultStores = storeRepository.findFirstByDefaultStores(Boolean.TRUE);
//            StockOut stockOut = new StockOut();
//            stockOut.setOutType(StocksConstants.STOCK_OUT_ORDER);
//            stockOut.setStoreid(defaultStores.getStoreid());
//            stockOut.setOrdInnerCode(order.getInnerCode());
//            OrderQuote orderQuote = orderQuoteRepository.findFirstByOrdidAndTypeOrderByOrderDesc(orderId, OrderConstants.QUOTE_TYPE_COST);
//            List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(orderQuote.getOqid());
//            List<StockOutItem> stockOutItems = orderQuoteItems.stream().map(x -> {
//                StockOutItem stockOutItem = new StockOutItem();
//                stockOutItem.setPid(x.getPid());
//                stockOutItem.setCount(x.getCount());
//                return stockOutItem;
//            }).collect(Collectors.toList());
//            stockOut.setItems(stockOutItems);
//            stockService.stocksOut(stockOut);
        } else if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {//返厂维修
            order.setStatus(ORDER_STATUS_RETURN_WAITING_RECEIVE.getCode());
        } else {
            throw new CustomBizException("服务方式错误");
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_SETTLE_SUBMIT);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> returnReceive(String orderId, ReturnReceive request) {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_RETURN_WAITING_RECEIVE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (Objects.nonNull(request.getCode())) {
            order.setCodeChange(Boolean.TRUE);
            device.setCode(request.getCode());
        }
        if (Objects.nonNull(request.getBatteryCode())) {
            order.setBatteryChange(Boolean.TRUE);
            device.setBatteryCode(request.getBatteryCode());
        }
        if (Objects.nonNull(request.getRemoteCode())) {
            order.setRemoteChange(Boolean.TRUE);
            device.setRemoteCode(request.getRemoteCode());
        }
        String uid = CommonUtils.getUid();
        order.setReturnReceiveUid(uid);
        order.setStatus(ORDER_STATUS_WAITING_CHECK.getCode());
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_RETURN_RECEIVE);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> dispatchOrder(String orderId, DispatchOrder request) {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_DISPATCH.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        String uid = CommonUtils.getUid();
        order.setDispatchUid(uid);
        order.setMaintainerUid(request.getMaintainerUid());
        order.setStatus(ORDER_STATUS_MAINTAINING.getCode());
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_DISPATCH);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> maintainFeedback(String orderId, MaintainFeedback request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_MAINTAINING.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        order.setMaintainFeedback(request.getFeedback());
        orderRepository.save(order);
        OrderLog orderLog = innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_MAINTAIN_FEEDBACK);
        innerService.saveOrderFileByLogId(orderId, orderLog.getOrdlogid(), request);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> finishMaintain(String orderId, Remark request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_MAINTAINING.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        String uid = CommonUtils.getUid();
        order.setMaintainResult(request.getRemark());
        order.setMaintainerUid(uid);
        order.setStatus(ORDER_STATUS_WAITING_CHECK.getCode());
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_MAINTAINED);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> failMaintain(String orderId, Remark request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_MAINTAINING.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        String uid = CommonUtils.getUid();
        order.setMaintainBack(request.getRemark());
        order.setMaintainerUid(uid);
        //维修失败，返回重新派单
        order.setStatus(ORDER_STATUS_WAITING_DISPATCH.getCode());
        order.setReQuote(Boolean.TRUE);
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_MAINTAIN_FAIL);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> backInsurance(String orderId, Remark request) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        String uid = CommonUtils.getUid();
        order.setBackInsuranceRemark(request.getRemark());
        order.setReviewInsuranceUid(uid);
        //保险回退
        order.setStatus(ORDER_STATUS_WAIT_CONFIRMED_DEVICE.getCode());
        order.setReviewInsuranceStatus(InsuranceStatusEnums.INSURANCE_FIRST_REVIEW.getCode());
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_BACK_INSURANCE);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> orderChecked(String orderId, OrderCheck request) throws Exception {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        order.setCheckDraw(request.getCheckDraw());
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_CHECK.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            order.setCheckRemark(request.getRemark());
            order.setStatus(ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode());
        } else {
            //检测不通过，重新定损
            order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
            order.setCheckRemark(request.getRemark());
        }
        order.setCheckUid(uid);
        orderRepository.save(order);
        OrderLog orderLog = innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CHECKED);
        innerService.saveOrderFileByLogId(orderId, orderLog.getOrdlogid(), request);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> customerExpresses(String orderId, CustomerExpresses request) throws Exception {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getStatus(), OrderStatusEnums.ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode())) {
            return BaseResponse.fail(String.format(CHECK_STATUS_MESSAGE, OrderStatusEnums.fromCode(order.getStatus())));
        }
        //快递自寄
        if (OrderConstants.EXPRESSES_SELF.equals(request.getType())) {
            Expresses expresses = new Expresses();
            expresses.setExpid(CommonUtils.uuid());
            expresses.setType(CommonConstants.EXPRESSES_SEND_CUSTOMER);
            expresses.setUid(uid);
            expresses.setOrdid(orderId);
            expresses.setStatus(CommonConstants.EXPRESS_STATUS_NORMAL);
            expresses.setWaybillNo(request.getExpressNo());
            expresses.setExpressesAt(request.getExpressTime());
            expresses.setPickup(request.getType());
            expressesRepository.save(expresses);
        }
        if (OrderConstants.EXPRESSES_PICKUP.equals(request.getType())) {//快递上门
            Address sendAddress = innerService.systemIfNotPresent(request.getSender());
            Address receiveAddress = innerService.customerIfNotPresent(order.getCuid(), request.getReceiver());
            Expresses expresses;
            if ("ems".equals(expressType)) {
                expresses = emsExpressService.createOrder(orderId, sendAddress, receiveAddress);
            } else {
                expresses = expressesService.createOrder(orderId, sendAddress, receiveAddress);
            }
            expresses.setType(CommonConstants.EXPRESSES_SEND_CUSTOMER);
            expresses.setPickup(request.getType());
            expresses.setUid(uid);
            expresses.setSenderId(sendAddress.getAid());
            expresses.setReceiverId(receiveAddress.getAid());
            expresses.setPickup(request.getType());
            expressesRepository.save(expresses);
        }
        //判断是否取消
        if (Objects.nonNull(order.getCancelType())) {
            order.setStatus(ORDER_STATUS_CANCEL.getCode());
        } else {
            Device device = deviceRepository.findByDevid(order.getDevid());
            if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
                //理赔已完成才能完成
                if (INSURANCE_FINISH.getCode().equals(order.getReviewInsuranceStatus())) {
                    order.setStatus(ORDER_STATUS_FINISH.getCode());
                } else {
                    order.setStatus(ORDER_STATUS_FINISH_CUSTOMER_EXPRESSES.getCode());
                }
            } else {
                order.setStatus(ORDER_STATUS_FINISH.getCode());
            }
        }
        order.setCustomerSendUid(uid);
        order.setFinishAt(System.currentTimeMillis());
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_CUSTOMER_EXPRESSES);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<DetailOrderQuote> queryOrderQuote(String orderId) {
        Order order = orderRepository.getByOrdid(orderId);
        DetailOrderQuote detailOrderQuote = new DetailOrderQuote();
        //查询成本报价报价
        List<OrderQuote> costOrderQuote = orderQuoteRepository.findByOrdidAndTypeOrderByOrder(orderId, QUOTE_TYPE_COST);
        if (CollectionUtils.isNotEmpty(costOrderQuote)) {
            List<QuoteExt> quotes = costOrderQuote.stream().map(x -> {
                QuoteExt quote = new QuoteExt();
                BeanUtils.copyProperties(x, quote);
                quote.setOqid(x.getOqid());
                List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(x.getOqid());
                List<QuoteItemExt> quoteItems = orderQuoteItems.stream().map(y -> {
                    QuoteItemExt quoteItem = new QuoteItemExt();
                    BeanUtils.copyProperties(y, quoteItem);
                    //返厂维修
                    if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {
                        quoteItem.setFactoryPrice(y.getSalePrice());
                    } else {
                        quoteItem.setRealPrice(y.getSalePrice());
                    }
                    ProjectProduct product = projectProductsRepository.findByPjidAndPid(order.getPjid(), y.getPid());
                    quoteItem.setProduct(product);
                    return quoteItem;
                }).collect(Collectors.toList());
                quote.setQuoteItems(quoteItems);
                return quote;
            }).collect(Collectors.toList());
            detailOrderQuote.setCostQuotes(quotes);
        }
        //客户报价单
        OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(orderId, QUOTE_TYPE_CUSTOMER);
        if (Objects.nonNull(customerQuote)) {
            QuoteExt quote = new QuoteExt();
            BeanUtils.copyProperties(customerQuote, quote);
            quote.setOqid(customerQuote.getOqid());
            List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(customerQuote.getOqid());
            List<QuoteItemExt> quoteItems = orderQuoteItems.stream().map(y -> {
                QuoteItemExt quoteItem = new QuoteItemExt();
                BeanUtils.copyProperties(y, quoteItem);
                ProjectProduct product = projectProductsRepository.findByPjidAndPid(order.getPjid(), y.getPid());
                quoteItem.setProduct(product);
                return quoteItem;
            }).collect(Collectors.toList());
            quote.setQuoteItems(quoteItems);
            detailOrderQuote.setCustomerQuote(quote);
        }
        //查询故障图片
        List<CustomFile> customFiles = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_QUOTE);
        List<String> ordcatUrls = customFiles.stream().map(innerService::getFileProxyUrl).filter(StringUtils::isNotBlank).collect(Collectors.toList());
        detailOrderQuote.setOrdcatUrls(ordcatUrls);
        List<String> fids = customFiles.stream().map(CustomFile::getFid).filter(StringUtils::isNotBlank).collect(Collectors.toList());
        detailOrderQuote.setFids(fids);
        //故障类别
        List<OrderCategory> orderCategories = orderCategoryRepository.findByOrdid(orderId);
        detailOrderQuote.setOrderCategories(orderCategories);
        List<String> ordcatids = orderCategories.stream().map(OrderCategory::getOrdcatid).collect(Collectors.toList());
        detailOrderQuote.setOrdcatids(ordcatids);
        List<OrderLog> reviewQuoteLogs = orderLogRepository.findByOrdidAndMethod(orderId, ORDER_LOG_METHOD_QUOTATION_AUDIT);
        List<OrderLog> passQuoteLogs = reviewQuoteLogs.stream().filter(x -> PASS.equals(x.getRemark())).collect(Collectors.toList());
        detailOrderQuote.setQuotedCount(passQuoteLogs.size());
        return BaseResponse.success(detailOrderQuote);
    }

    @Override
    public BaseResponse<List<DetailStockInOut>> queryOrderStockOut(String orderId) {
        List<StockInOut> stockInOut = stockInOutRepository.findByOrdidAndTypeAndOutTypeOrderByCt(orderId, StocksConstants.TYPE_OUT, StocksConstants.STOCK_OUT_ORDER);
        List<DetailStockInOut> stockInOuts = stockInOut.stream().map(x -> {
            DetailStockInOut data = new DetailStockInOut();
            BeanUtils.copyProperties(x, data);
            data.setStore(storeRepository.findByStoreid(x.getStoreid()));
            data.setUser(userRepository.findByUid(x.getReviewUid()));
            data.setApplyUser(userRepository.findByUid(x.getApplyUid()));
            data.setCreatedAt(DateUtils.formatDateTime(x.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(x.getUt()));
            data.setItems(stockInOutItemRepository.findByStcInOutId(x.getStcInoutId()));
            return data;
        }).collect(Collectors.toList());
        return BaseResponse.success(stockInOuts);
    }

    @Override
    public BaseResponse<List<DetailExpress>> queryOrderExpresses(String orderId) {
        List<Expresses> expressesList = expressesRepository.findByOrdid(orderId);
        List<DetailExpress> data = expressesList.stream().map(x -> {
            DetailExpress detailExpress = new DetailExpress();
            BeanUtils.copyProperties(x, detailExpress);
            detailExpress.setExpressesTime(DateUtils.formatDateTime(x.getExpressesAt()));
            if (Objects.nonNull(x.getUid())) {
                detailExpress.setUser(userRepository.findByUid(x.getUid()));
            }
            if (Objects.nonNull(x.getSenderId())) {
                detailExpress.setSender(addressRepository.getByAid(x.getSenderId()));
            }
            if (Objects.nonNull(x.getReceiverId())) {
                detailExpress.setReceiver(addressRepository.getByAid(x.getReceiverId()));
            }
            CustomFile customFile = fileRepository.findByExpid(x.getExpid());
            detailExpress.setCloudPrintUrl(innerService.getFileProxyUrl(customFile));
            return detailExpress;
        }).collect(Collectors.toList());
        return BaseResponse.success(data);
    }

    @Override
    public BaseResponse<List<OrderLogDetail>> queryOrderLog(String orderId) {
        List<OrderLog> orderLogs = orderLogRepository.findByOrdidOrderByCtDesc(orderId);
        List<OrderLogDetail> data = orderLogs.stream().map(x -> {
            OrderLogDetail detail = new OrderLogDetail();
            BeanUtils.copyProperties(x, detail);
            List<CustomFile> customFiles = fileRepository.findByOrdlogid(x.getOrdlogid());
            List<String> fileUrls = customFiles.stream().map(innerService::getFileProxyUrl).filter(StringUtils::isNotBlank).collect(Collectors.toList());
            detail.setFiles(fileUrls);
            detail.setOperateTime(DateUtils.formatDateTime(x.getCt()));
            return detail;
        }).collect(Collectors.toList());
        return BaseResponse.success(data);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> firstInsurance(String orderId, FirstInsurance request) throws Exception {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.getByOrdid(orderId);
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (!OrderConstants.ORDER_IN_INSURANCE.equals(order.getServiceType()) || !INSURANCE_DH.equals(device.getInsuranceType())) {
            return BaseResponse.fail("工单不需要上传保险材料");
        }
        if (!CommonUtils.checkStatus(order.getReviewInsuranceStatus(), INSURANCE_FIRST_WAITING_SUBMIT.getCode(), INSURANCE_FIRST_REJECT.getCode())) {
            return BaseResponse.fail(String.format(CHECK_INSURANCE_MESSAGE, InsuranceStatusEnums.fromCode(order.getReviewInsuranceStatus())));
        }
        //保存出险材料
        innerService.saveFirstInsuranceMaterial(order, request);
        InsuranceMaterial material = insuranceMaterialRepository.findByOrdid(orderId);
        if (Objects.isNull(material)) {
            material = new InsuranceMaterial();
            material.setOrdid(order.getOrdid());
            material.setImid(CommonUtils.uuid());
            material.setUid(uid);
        }
        BeanUtils.copyProperties(request, material);
        if (StringUtils.isNotBlank(request.getReportNo())) {
            material.setBeReport(Boolean.TRUE);
        }
        insuranceMaterialRepository.save(material);
        order.setReviewInsuranceStatus(INSURANCE_FIRST_REVIEW.getCode());
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_FIRST_INSURANCE_SUBMIT);
        //鼎和保险生成保险文件
        innerService.generateFirstMaterial(order);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> lastInsurance(String orderId, List<String> fids) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getReviewInsuranceStatus(), INSURANCE_LAST_WAITING_SUBMIT.getCode(), INSURANCE_LAST_REJECT.getCode())) {
            return BaseResponse.fail(String.format(CHECK_INSURANCE_MESSAGE, InsuranceStatusEnums.fromCode(order.getReviewInsuranceStatus())));
        }
        //删除之前的
        List<CustomFile> customFiles = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_LAST_INSURANCE);
        customFiles.forEach(x -> {
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            try {
                customMinIOClient.removeFile(x.getDirectory(), objectName);
            } catch (Exception e) {
                log.error("removeFile error", e);
            }
            fileRepository.delete(x);
        });
        //关联本次上传的
        fids.forEach(x -> {
            CustomFile customFile = fileRepository.findByFid(x);
            customFile.setOrdid(orderId);
            customFile.setType(CommonConstants.FILE_TYPE_LAST_INSURANCE);
            customFile.setName("保险终版资料.zip");
            fileRepository.save(customFile);
        });
        order.setReviewInsuranceStatus(INSURANCE_LAST_WAITING_REVIEW.getCode());
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_LAST_INSURANCE_SUBMIT);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<InsuranceMaterialDetail> queryInsuranceMaterial(String orderId) {
        InsuranceMaterialDetail detail = new InsuranceMaterialDetail();
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(orderId);
        if (Objects.isNull(insuranceMaterial)) {
            return BaseResponse.success(detail);
        }
        BeanUtils.copyProperties(insuranceMaterial, detail);
        detail.setEffectiveEndTime(DateUtils.formatDateTime(insuranceMaterial.getEffectiveEndAt()));
        detail.setEffectiveStartTime(DateUtils.formatDateTime(insuranceMaterial.getEffectiveStartAt()));
        CustomFile cert = fileRepository.getByOrdidAndType(orderId, CommonConstants.FILE_TYPE_PILOT_CERT);
        detail.setPilotCertificate(innerService.convertFile2VO(cert));
        CustomFile asset = fileRepository.getByOrdidAndType(orderId, CommonConstants.FILE_TYPE_FIXED_ASSET);
        detail.setFixedAsset(innerService.convertFile2VO(asset));
        CustomFile screenshot = fileRepository.getByOrdidAndType(orderId, CommonConstants.FILE_TYPE_INSURANCE_SHOT);
        detail.setInsuranceScreenshot(innerService.convertFile2VO(screenshot));
        CustomFile lineWalk = fileRepository.getByOrdidAndType(orderId, CommonConstants.FILE_TYPE_LINE_WALK);
        detail.setLineWalkingPicture(innerService.convertFile2VO(lineWalk));
        CustomFile presentation = fileRepository.getByOrdidAndType(orderId, CommonConstants.FILE_TYPE_PRESENTATION);
        detail.setPresentation(innerService.convertFile2VO(presentation));
        List<CustomFile> scene = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_ACCIDENT_SCENE);
        detail.setAccidentScene(scene.stream().map(innerService::convertFile2VO).collect(Collectors.toList()));
        List<CustomFile> items = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_ITEMS);
        detail.setAccidentDamageItems(items.stream().map(innerService::convertFile2VO).collect(Collectors.toList()));
        List<CustomFile> devices = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_DEVICES);
        detail.setAccidentDamageDevices(devices.stream().map(innerService::convertFile2VO).collect(Collectors.toList()));
        List<CustomFile> front = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_DEVICE_FRONT);
        detail.setDeviceFront(front.stream().map(innerService::convertFile2VO).collect(Collectors.toList()));
        List<CustomFile> reverse = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_DEVICE_REVERSE);
        detail.setDeviceReverse(reverse.stream().map(innerService::convertFile2VO).collect(Collectors.toList()));
        List<CustomFile> other = fileRepository.findByOrdidAndType(orderId, CommonConstants.FILE_TYPE_ACCIDENT_OTHER);
        detail.setAccidentOther(other.stream().map(innerService::convertFile2VO).collect(Collectors.toList()));
        detail.setAccidentCustomer(customersRepository.findByCid(insuranceMaterial.getCid()));
        Optional.ofNullable(insuranceMaterial.getFaultAt()).ifPresent(x -> detail.setFaultTime(DateUtils.formatDateTime(x)));
        return BaseResponse.success(detail);
    }

    @Override
    public BaseResponse<Void> reportInvoice(String orderId, ReportInvoice request) {
        Order order = orderRepository.getByOrdid(orderId);
        InsuranceMaterial material = insuranceMaterialRepository.findByOrdid(orderId);
        if (Objects.isNull(material)) {
            return BaseResponse.fail("保险资料未提交");
        }
        BeanUtils.copyProperties(request, material, CommonUtils.getNullPropertyNames(request));
        if (StringUtils.isNotBlank(request.getReportNo())) {
            material.setBeReport(Boolean.TRUE);
        }
        insuranceMaterialRepository.save(material);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_RECORD_INVOICE);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<DetailSettle> detailSettle(String orderId) {
        DetailSettle detailSettle = new DetailSettle();
        OrderSettle orderSettle1 = orderSettleRepository.findFirstByOrdidAndType(orderId, SETTLE_TYPE_CUSTOMER);
        if (Objects.nonNull(orderSettle1)) {
            CustomerSettle customerSettle = new CustomerSettle();
            BeanUtils.copyProperties(orderSettle1, customerSettle);
            detailSettle.setCustomerSettle(customerSettle);
        }
        List<OrderSettle> orderSettle2 = orderSettleRepository.findByOrdidAndTypeOrderByUtAsc(orderId, SETTLE_TYPE_FACTORY);
        List<AdvanceSettle> advanceSettles = orderSettle2.stream().map(x -> {
            AdvanceSettle advanceSettle = new AdvanceSettle();
            BeanUtils.copyProperties(x, advanceSettle);
            return advanceSettle;
        }).collect(Collectors.toList());
        detailSettle.setAdvanceSettle(advanceSettles);
        List<OrderLog> submitSettleLogs = orderLogRepository.findByOrdidAndMethod(orderId, ORDER_LOG_METHOD_SETTLE_SUBMIT);
        detailSettle.setSettleCount(submitSettleLogs.size());
        return BaseResponse.success(detailSettle);
    }

    /**
     * 本地维修
     * 成本报价：仓库价格
     * 客户报价：价格库销售价
     * <p>
     * 返厂维修
     * 成本报价：产品表价格
     * 客户报价：价格库销售价
     * <p>
     * 工时费统一由价格库获取
     *
     * @param orderId
     * @param catid
     * @return
     */
    @Override
    public BaseResponse<List<OrderSparePrice>> orderSparePrice(String orderId, String catid) {
        List<Product> products = productRepository.findByCatid(catid);
        Order order = orderRepository.findByOrdid(orderId);
        List<OrderSparePrice> orderSparePrices = new ArrayList<>();
        Store defaultStore = storeRepository.findFirstByDefaultStoresAndStoreTypeAndCpid(Boolean.TRUE, StocksConstants.STORE_SPARE, CommonUtils.getCpid());
        for (Product product : products) {
            //返厂维修
            if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {
                ProjectProduct projectProduct = projectProductsRepository.findByPjidAndPid(order.getPjid(), product.getPid());
                if (Objects.nonNull(projectProduct)) {
                    OrderSparePrice orderSparePrice = new OrderSparePrice();
                    orderSparePrice.setFactoryPrice(product.getFactoryPrice().doubleValue());
                    orderSparePrice.setSalePrice(projectProduct.getSalePrice().doubleValue());
                    orderSparePrice.setWorkHour(projectProduct.getWorkHour().doubleValue());
                    orderSparePrice.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost().doubleValue());
                    orderSparePrice.setWorkHourCost(projectProduct.getWorkHourCost().doubleValue());
                    orderSparePrice.setPid(product.getPid());
                    orderSparePrice.setProduct(product);
                    orderSparePrices.add(orderSparePrice);
                }
            } else {
                if (Objects.isNull(defaultStore)) {
                    break;
                }
                Stock stock = stockRepository.findByPidAndStoreid(product.getPid(), defaultStore.getStoreid());
                ProjectProduct projectProduct = projectProductsRepository.findByPjidAndPid(order.getPjid(), product.getPid());
                if (Objects.nonNull(stock) && Objects.nonNull(projectProduct)) {
                    OrderSparePrice orderSparePrice = new OrderSparePrice();
                    orderSparePrice.setRealPrice(stock.getRealPrice());
                    orderSparePrice.setFactoryPrice(product.getFactoryPrice().doubleValue());
                    orderSparePrice.setSalePrice(projectProduct.getSalePrice().doubleValue());
                    orderSparePrice.setWorkHour(projectProduct.getWorkHour().doubleValue());
                    orderSparePrice.setWorkHourUnitCost(projectProduct.getWorkHourUnitCost().doubleValue());
                    orderSparePrice.setWorkHourCost(projectProduct.getWorkHourCost().doubleValue());
                    orderSparePrice.setPid(product.getPid());
                    orderSparePrice.setProduct(product);
                    orderSparePrices.add(orderSparePrice);
                }
            }

        }
        return BaseResponse.success(orderSparePrices);
    }

    @Override
    public void downloadCostQuote(HttpServletResponse response, String orderId) throws Exception {
        Order order = orderRepository.getByOrdid(orderId);
        //成本报价单
        List<OrderQuote> costQuote = orderQuoteRepository.findByOrdidAndTypeOrderByOrder(orderId, QUOTE_TYPE_COST);
        OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(orderId, QUOTE_TYPE_CUSTOMER);
        Workbook workbook = innerService.writeCostQuote(order, costQuote);
        if (Objects.nonNull(customerQuote)) {
            innerService.writeCustomerQuote(order, customerQuote, workbook);
        }
//        File file = new File("D:\\temp\\template_1.xlsx");
//        FileOutputStream fileOutputStream = new FileOutputStream(file);
//        workbook.write(fileOutputStream);
//        fileOutputStream.flush();
//        fileOutputStream.close();
//        workbook.close();
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                + URLEncoder.encode("系统报价单", "UTF-8") + ".xlsx");
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        workbook.close();
    }

    @Override
    @Transactional
    public BaseResponse<String> downloadCostQuote(String orderId) throws Exception {
        try {
            String uid = CommonUtils.getUid();
            Order order = orderRepository.getByOrdid(orderId);
            if (OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION_CONFIRM.getCode().compareTo(order.getStatus()) > 0) {
                throw new CustomBizException("报价还未审核，无法下载");
            }
            //成本报价单
            List<OrderQuote> costQuote = orderQuoteRepository.findByOrdidAndTypeOrderByOrder(orderId, QUOTE_TYPE_COST);
            OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(orderId, QUOTE_TYPE_CUSTOMER);
            Workbook workbook = innerService.writeCostQuote(order, costQuote);
            innerService.writeCustomerQuote(order, customerQuote, workbook);
            String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid()).concat(DOT).concat(FILE_XLSX);
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            workbook.close();
            CustomFile quoteFile = new CustomFile();
            quoteFile.setFid(CommonUtils.uuid());
            String o4 = quoteFile.getFid().concat(CommonConstants.DOT).concat(FILE_XLSX);
            fileRepository.deleteByOrdidAndType(orderId, CommonConstants.SYSTEM_QUOTE);
            customMinIOClient.uploadFile(ORDER_BUCKET, o4, fileName);
            quoteFile.setDownload(Boolean.TRUE);
            quoteFile.setDirectory(ORDER_BUCKET);
            quoteFile.setExtension(CommonConstants.FILE_XLSX);
            quoteFile.setEnable(Boolean.TRUE);
            quoteFile.setMime(CommonConstants.FILE_XLSX);
            quoteFile.setName("系统报价单.xlsx");
            quoteFile.setLocation(o4);
            quoteFile.setUid(uid);
            quoteFile.setOrdid(order.getOrdid());
            quoteFile.setType(CommonConstants.SYSTEM_QUOTE);
            fileRepository.save(quoteFile);
            FileUtils.deleteQuietly(new File(fileName));
            return BaseResponse.success(innerService.getFileProxyUrl(quoteFile));
        } catch (Exception e) {
            log.error("系统报价单下载失败", e);
        }
        return BaseResponse.success();
    }

    @Override
    public void downloadCustomerQuote(HttpServletResponse response, String orderId) throws Exception {
        //没有生成则生成
        CustomFile quoteFile = fileRepository.getByOrdidAndType(orderId, CommonConstants.CUSTOMER_QUOTE);
        if (Objects.isNull(quoteFile)) {
            String uid = CommonUtils.getUid();
            Order order = orderRepository.getByOrdid(orderId);
            Device device = deviceRepository.findByDevid(order.getDevid());
            InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
            Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
            Product product = productRepository.findByPid(device.getPid());
            OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
            double total = customerQuote.getSparePrice() + customerQuote.getExpressesPrice() + customerQuote.getOtherPrice();
            QuotationPrice quotationPrice = new QuotationPrice();
            quotationPrice.setCustomer(accidentCustomer.getName());
            quotationPrice.setDeviceModel(product.getModel());
            quotationPrice.setSnCode(device.getCode());
            OrderLog quoteLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(order.getOrdid(), ORDER_LOG_METHOD_QUOTATION);
            quotationPrice.setQuotationDate(DateUtils.formatDateDot(quoteLog.getCt()));
            quotationPrice.setContact(insuranceMaterial.getAccidentUser().concat(insuranceMaterial.getAccidentTel()));
            quotationPrice.setProduct(product.getName());
            quotationPrice.setLossAssessmentResult(order.getLossRemark());
            quotationPrice.setPrice(String.valueOf(total));
            quotationPrice.setTotalAmount(CommonUtils.toChineseMoney(BigDecimal.valueOf(total)));
            quotationPrice.setDate(DateUtils.formatDateChinese(System.currentTimeMillis()));
            List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(customerQuote.getOqid());
            List<MaterialItem> materialItems = orderQuoteItems.stream().map(x -> {
                MaterialItem materialItem = new MaterialItem();
                Product p = productRepository.findByPid(x.getPid());
                materialItem.setMaterialName(p.getName());
                materialItem.setUnit(p.getUnit());
                materialItem.setNumber(String.valueOf(x.getCount()));
                materialItem.setUnitPrice(String.valueOf(x.getSalePrice()));
                materialItem.setManHourCost(String.valueOf(x.getWorkHourCost()));
                materialItem.setAmount(String.valueOf(x.getWorkHourCost() + x.getCount() * x.getSalePrice()));
                return materialItem;
            }).collect(Collectors.toList());
            quotationPrice.setMaterialList(materialItems);
            File quotationPriceFile = wordUtils.generateQuotationPrice(quotationPrice, Boolean.TRUE);
            quoteFile = new CustomFile();
            quoteFile.setFid(CommonUtils.uuid());
            String o4 = quoteFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o4, quotationPriceFile.getAbsolutePath());
            quoteFile.setDownload(Boolean.TRUE);
            quoteFile.setDirectory(ORDER_BUCKET);
            quoteFile.setExtension(CommonConstants.FILE_PDF);
            quoteFile.setEnable(Boolean.TRUE);
            quoteFile.setMime(CommonConstants.FILE_PDF);
            quoteFile.setName("报价单.pdf");
            quoteFile.setLocation(o4);
            quoteFile.setUid(uid);
            quoteFile.setOrdid(order.getOrdid());
            quoteFile.setType(CommonConstants.CUSTOMER_QUOTE);
            fileRepository.save(quoteFile);
            FileUtils.deleteQuietly(quotationPriceFile);
        }
        //下载文件
        String objectName = quoteFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
        byte[] bytes = customMinIOClient.downloadFile(quoteFile.getDirectory(), objectName);
        response.setContentType("application/pdf");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename="
                + URLEncoder.encode("客户报价单", "UTF-8") + ".pdf");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
    }

    @Override
    public BaseResponse<String> downloadCustomerQuote(String orderId) throws Exception {
        try {
            //没有生成则生成
            CustomFile quoteFile = fileRepository.getByOrdidAndType(orderId, CommonConstants.CUSTOMER_QUOTE);
            if (Objects.isNull(quoteFile)) {
                String uid = CommonUtils.getUid();
                Order order = orderRepository.getByOrdid(orderId);
                Device device = deviceRepository.findByDevid(order.getDevid());
                InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
                Customer customer = customersRepository.findByCid(order.getCid());
                Address address = addressRepository.getByAid(order.getAid());
                String customerName = customer.getName();
                String contact = address.getContactUser().concat(address.getContactTel());
                if (Objects.nonNull(insuranceMaterial)) {
                    Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
                    customerName = accidentCustomer.getName();
                    contact = insuranceMaterial.getAccidentUser().concat(insuranceMaterial.getAccidentTel());
                }
                Product product = productRepository.findByPid(device.getPid());
                OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
                if (Objects.isNull(customerQuote)) {
                    return BaseResponse.fail("未查询到客户相关报价信息");
                }
                double total = Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00);
                QuotationPrice quotationPrice = new QuotationPrice();
                quotationPrice.setCustomer(customerName);
                quotationPrice.setDeviceModel(product.getModel());
                quotationPrice.setSnCode(device.getCode());
                OrderLog quoteLog = orderLogRepository.findFirstByOrdidAndMethodOrderByCtAsc(order.getOrdid(), ORDER_LOG_METHOD_QUOTATION);
                quotationPrice.setQuotationDate(DateUtils.formatDateDot(quoteLog.getCt()));
                quotationPrice.setContact(contact);
                quotationPrice.setProduct(product.getName());
                quotationPrice.setLossAssessmentResult(order.getLossRemark());
                quotationPrice.setPrice(String.valueOf(total));
                quotationPrice.setTotalAmount(CommonUtils.toChineseMoney(BigDecimal.valueOf(total)));
                quotationPrice.setDate(DateUtils.formatDateChinese(System.currentTimeMillis()));
                List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(customerQuote.getOqid());
                List<MaterialItem> materialItems = orderQuoteItems.stream().map(x -> {
                    MaterialItem materialItem = new MaterialItem();
                    Product p = productRepository.findByPid(x.getPid());
                    materialItem.setMaterialName(p.getName());
                    materialItem.setUnit(p.getUnit());
                    materialItem.setNumber(String.valueOf(x.getCount()));
                    materialItem.setUnitPrice(String.valueOf(x.getSalePrice()));
                    materialItem.setManHourCost(String.valueOf(x.getWorkHourCost()));
                    materialItem.setAmount(String.valueOf(x.getWorkHourCost() + x.getCount() * x.getSalePrice()));
                    return materialItem;
                }).collect(Collectors.toList());
                quotationPrice.setMaterialList(materialItems);
                File quotationPriceFile = wordUtils.generateQuotationPrice(quotationPrice, Boolean.TRUE);
                quoteFile = new CustomFile();
                quoteFile.setFid(CommonUtils.uuid());
                String o4 = quoteFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
                customMinIOClient.uploadFile(ORDER_BUCKET, o4, quotationPriceFile.getAbsolutePath());
                quoteFile.setDownload(Boolean.TRUE);
                quoteFile.setDirectory(ORDER_BUCKET);
                quoteFile.setExtension(CommonConstants.FILE_PDF);
                quoteFile.setEnable(Boolean.TRUE);
                quoteFile.setMime(CommonConstants.FILE_PDF);
                quoteFile.setName("报价单.pdf");
                quoteFile.setLocation(o4);
                quoteFile.setUid(uid);
                quoteFile.setOrdid(order.getOrdid());
                quoteFile.setType(CommonConstants.CUSTOMER_QUOTE);
                fileRepository.save(quoteFile);
                FileUtils.deleteQuietly(quotationPriceFile);
            }
            return BaseResponse.success(innerService.getFileProxyUrl(quoteFile));
        } catch (Exception e) {
            log.error("客户报价单下载", e);
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<List<QuoteSpare>> orderQuoteSpare(String orderId) {
        //查询成本报价报价
        List<OrderQuote> costOrderQuote = orderQuoteRepository.findByOrdidAndTypeOrderByOrder(orderId, QUOTE_TYPE_COST);
        List<QuoteSpare> quoteSpares = costOrderQuote.stream().map(x -> {
            QuoteSpare quoteSpare = new QuoteSpare();
            quoteSpare.setOrder(x.getOrder());
            quoteSpare.setOqid(x.getOqid());
            quoteSpare.setStockOut(stockInOutRepository.existsByOqidAndStatus(x.getOqid(), StocksConstants.AWAITING) || stockInOutRepository.existsByOqidAndStatus(x.getOqid(), StocksConstants.FINISH));
            AtomicReference<Integer> total = new AtomicReference<>(0);
            AtomicReference<Integer> stockOutTotal = new AtomicReference<>(0);
            List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(x.getOqid());
            StockInOut stockInOut = Optional.ofNullable(stockInOutRepository.findByOqidAndStatus(x.getOqid(), StocksConstants.AWAITING)).orElse(stockInOutRepository.findByOqidAndStatus(x.getOqid(), StocksConstants.FINISH));
            List<SpareItem> spareItems = orderQuoteItems.stream().map(y -> {
                SpareItem spareItem = new SpareItem();
                total.set(total.get() + y.getCount());
                spareItem.setCount(y.getCount());
                spareItem.setPid(y.getPid());
                spareItem.setProduct(productRepository.findByPid(y.getPid()));
                if (Objects.nonNull(stockInOut)) {
                    StockInOutItem stockOutItem = stockInOutItemRepository.findByStcInOutIdAndPid(stockInOut.getStcInoutId(), y.getPid());
                    Optional.ofNullable(stockOutItem).ifPresent(item -> spareItem.setStockOutCount(item.getCount()));
                    Optional.ofNullable(stockOutItem).ifPresent(item -> stockOutTotal.set(stockOutTotal.get() + item.getCount()));
                }
                return spareItem;
            }).collect(Collectors.toList());
            quoteSpare.setTotalCount(total.get());
            quoteSpare.setStockOutCount(stockOutTotal.get());
            quoteSpare.setItems(spareItems);
            return quoteSpare;
        }).collect(Collectors.toList());
        return BaseResponse.success(quoteSpares);
    }

    @Override
    public BaseResponse<List<OrderFile>> orderFile(String orderId) {
        String uid = CommonUtils.getUid();
        List<String> commonFile = Lists.newArrayList(INSURANCE_ARCHIVED, FILE_TYPE_LAST_INSURANCE, INTENTION_TO_PAY, POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM, LOSS_LIST, FAULT_REPORT_TEMPLATE_PATH, FILE_TYPE_FIXED_ASSET, FILE_TYPE_PILOT_CERT, FAULT_PHOTO, FILE_TYPE_LINE_WALK, FILE_TYPE_PRESENTATION, CUSTOMER_QUOTE, SYSTEM_QUOTE);
        User user = userRepository.findByUid(uid);
        //管理员还需要看到案件报告、收费清单、案件文件
        if (UserTypeEnum.ADMIN.getCode().equals(user.getUserType())) {
            commonFile.add(REPORT_DOCUMENT);
            commonFile.add(INCIDENT_REPORT);
            commonFile.add(FEE_NOTIFICATION);
        }
        List<OrderFile> orderFiles = new ArrayList<>();
        for (String t : commonFile) {
            CustomFile customFile = fileRepository.findFirstByOrdidAndTypeOrderByCtDesc(orderId, t);
            if (Objects.nonNull(customFile)) {
                try {
                    OrderFile orderFile = new OrderFile();
                    String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
                    String url = customMinIOClient.getProxyUrl(customFile.getDirectory(), objectName);
                    orderFile.setName(customFile.getName());
                    orderFile.setUrl(url);
                    orderFile.setType(t);
                    orderFile.setUpdateTime(DateUtils.formatDateTime(customFile.getCt()));
                    orderFile.setDownload(customFile.getDownload());
                    orderFiles.add(orderFile);
                } catch (Exception e) {
                    log.error("getProxyUrl error", e);
                }
            }
        }
        return BaseResponse.success(orderFiles);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> reviewScrap(String orderId, ScrapReview request) {
        String uid = CommonUtils.getUid();
        Order order = orderRepository.findByOrdid(orderId);
        order.setReviewScrapUid(request.getReviewRemark());
        order.setReviewScrapUid(uid);
        //审核通过
        if (CommonConstants.PASS.equals(request.getReviewResult())) {
            order.setStatus(ORDER_STATUS_FINISH.getCode());
        } else {
            order.setStatus(ORDER_STATUS_WAIT_SCRAP_HANDLE.getCode());
        }
        orderRepository.save(order);
        innerService.saveOrderLog(order, ORDER_LOG_METHOD_REVIEW_SCRAP);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> handleScrap(String orderId, ScrapHandle request) throws Exception {
        Order order = orderRepository.findByOrdid(orderId);
        if (Objects.isNull(request)) {
            return BaseResponse.fail("工单ID错误");
        }
        if (CommonUtils.int2BBoolean(request.getSecondary())) {
            Order beforeOrder = orderRepository.findByInnerCode(request.getRelationOrderCode());
            if (Objects.isNull(beforeOrder)) {
                return BaseResponse.fail("关联工单号不存在");
            }
        }
        //处理设备信息
        Device device = deviceRepository.findByCode(request.getCode());
        if (Objects.isNull(device)) {
            return BaseResponse.fail("设备未找到");
        }
        device.setStatus(OrderConstants.DEVICE_STATUS_NORMAL);
        if (OrderConstants.ORDER_MAINTAIN.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_MAINTAIN);
        }
        if (OrderConstants.ORDER_UPKEEP.equals(request.getType())) {
            device.setStatus(OrderConstants.DEVICE_STATUS_UPKEEP);
        }
        BeanUtils.copyProperties(request, device, CommonUtils.getNullPropertyNames(request));
        deviceRepository.save(device);
        //工单信息
        if (OrderConstants.ORDER_IN_INSURANCE.equals(order.getServiceType())
                || OrderConstants.ORDER_OUT_INSURANCE.equals(order.getServiceType())) {
            order.setStatus(ORDER_STATUS_WAIT_CONFIRM_DEVICE.getCode());
            //如果是二次返修，则跟上次维修方式保持一致
            if (CommonUtils.int2BBoolean(order.getSecondary())) {
                Order before = orderRepository.findByInnerCode(order.getRelationOrderCode());
                order.setServiceMethod(before.getServiceMethod());
                order.setReviewInsuranceStatus(INSURANCE_FINISH.getCode());
                //返厂维修
                if (OrderConstants.ORDER_SERVICE_METHOD_FACTORY_MAINTAIN.equals(order.getServiceMethod())) {
                    order.setStatus(ORDER_STATUS_WAIT_LOSS_REVIEW.getCode());
                } else {
                    order.setStatus(ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode());
                }
            } else {
                innerService.sendSms(order, customConfig.getConfirmDeviceTemplate1());
            }
        } else if (ORDER_SERVICE_INVALID.equals(order.getServiceType())) {
            order.setStatus(ORDER_STATUS_WAIT_SCRAP_REVIEW.getCode());
        } else {
            throw new CustomBizException("服务类型错误");
        }
        order.setServiceType(request.getServiceType());
        order.setRemark(request.getRemark());
        order.setDescription(request.getDescription());
        //如果是鼎和保险需要客户先提交保险资料
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(request.getInsuranceType())) {
            order.setReviewInsuranceStatus(INSURANCE_FIRST_WAITING_SUBMIT.getCode());
            innerService.sendSms(order, customConfig.getSummitFirstInsurance());
        }
        order.setRelationOrderCode(request.getRelationOrderCode());
        order.setType(request.getType());
        order.setServiceType(request.getServiceType());
        order.setSecondary(request.getSecondary());
        order.setLevel(request.getLevel());
        orderRepository.save(order);
        innerService.saveOrderLog(order, OrderConstants.ORDER_LOG_METHOD_HANDLE_SCRAP);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> insuranceArchived(String orderId, List<String> fids) {
        Order order = orderRepository.getByOrdid(orderId);
        if (!CommonUtils.checkStatus(order.getReviewInsuranceStatus(), INSURANCE_WAITING_FINISH.getCode())) {
            return BaseResponse.fail(String.format(CHECK_INSURANCE_MESSAGE, InsuranceStatusEnums.fromCode(order.getReviewInsuranceStatus())));
        }
        //删除之前的
        List<CustomFile> customFiles = fileRepository.findByOrdidAndType(orderId, CommonConstants.INSURANCE_ARCHIVED);
        customFiles.forEach(x -> {
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            try {
                customMinIOClient.removeFile(x.getDirectory(), objectName);
            } catch (Exception e) {
                log.error("removeFile error", e);
            }
            fileRepository.delete(x);
        });
        //关联本次上传的
        fids.forEach(x -> {
            CustomFile customFile = fileRepository.findByFid(x);
            customFile.setOrdid(orderId);
            customFile.setType(CommonConstants.INSURANCE_ARCHIVED);
            customFile.setName("保险归档资料.zip");
            fileRepository.save(customFile);
        });
        innerService.saveOrderLog(order, ORDER_LOG_INSURANCE_ARCHIVED);
        return BaseResponse.success();
    }

    @Override
    public void orderImport(MultipartFile file, Integer type, HttpServletResponse response) throws Exception {
        List<ImportOrder> orders = EasyExcelUtils.readXlsx(file.getInputStream(), ImportOrder.class);
        if (CollectionUtils.isNotEmpty(orders)) {
            for (ImportOrder order : orders) {
                try {
                    this.innerService.importOrder(order, type);
                } catch (Exception e) {
                    log.error("导入工单失败", e);
                    order.setMessage(e.getMessage());
                }
            }
            if (ORDER_IMPORT_DH.equals(type)) {
                List<DHOrderImport> export = orders.stream().map(x -> {
                    DHOrderImport dhOrderImport = new DHOrderImport();
                    BeanUtils.copyProperties(x, dhOrderImport);
                    return dhOrderImport;
                }).collect(Collectors.toList());
                EasyExcelUtils.exportXlsx(response, "工单导入结果", "Sheet1", export, DHOrderImport.class);
            } else if (ORDER_IMPORT_SECONDARY.equals(type)) {
                List<SecondaryOrderImport> export = orders.stream().map(x -> {
                    SecondaryOrderImport secondaryOrderImport = new SecondaryOrderImport();
                    BeanUtils.copyProperties(x, secondaryOrderImport);
                    return secondaryOrderImport;
                }).collect(Collectors.toList());
                EasyExcelUtils.exportXlsx(response, "工单导入结果", "Sheet1", export, SecondaryOrderImport.class);
            } else if (ORDER_IMPORT_DJ.equals(type)) {
                List<DJOrderImport> export = orders.stream().map(x -> {
                    DJOrderImport djOrderImport = new DJOrderImport();
                    BeanUtils.copyProperties(x, djOrderImport);
                    return djOrderImport;
                }).collect(Collectors.toList());
                EasyExcelUtils.exportXlsx(response, "工单导入结果", "Sheet1", export, DJOrderImport.class);
            } else if (ORDER_IMPORT_OUT_INSURANCE.equals(type)) {
                List<OutInsuranceImport> export = orders.stream().map(x -> {
                    OutInsuranceImport outInsuranceImport = new OutInsuranceImport();
                    BeanUtils.copyProperties(x, outInsuranceImport);
                    return outInsuranceImport;
                }).collect(Collectors.toList());
                EasyExcelUtils.exportXlsx(response, "工单导入结果", "Sheet1", export, OutInsuranceImport.class);
            } else {
                throw new CustomBizException("类型参数错误");
            }
        }
    }
}
