package com.thinglinks.uav.order.service;

import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.order.dto.DetailExpress;
import com.thinglinks.uav.order.dto.expresses.*;
import com.thinglinks.uav.order.entity.Expresses;

public interface ExpressesService {


    String getToken() throws Exception;

    /**
     * 寄件下单
     */
    Expresses createOrder(String orderId, String sendId, String receiveId) throws Exception;

    /**
     * 寄件下单
     */
    Expresses createOrder(String orderId, Address sendAddress, Address receiveAddress) throws Exception;


    /**
     * 取消快递
     *
     * @param expId
     */
    BaseResponse<Void> cancelOrder(String expId) throws Exception;

    /**
     * 取消快递
     *
     * @param expresses
     */
    void cancelOrder(Expresses expresses) throws Exception;

    /**
     *
     * @param expId
     * @return
     */
    BaseResponse<String> searchRoutes(String expId);

    /**
     * senderPhone
     *
     * @param waybillNo
     * @param senderPhone
     */
    String searchRoutes(String waybillNo, String senderPhone) throws Exception;

    /**
     * 云打印面单推送
     *
     * @param request
     * @return
     */
    PushResponse waybillPush(PrintWaybills request);

    /**
     * 订单推送
     *
     * @param request
     * @return
     */
    PushResponse statePush(StatePush request);

    /**
     * 清单运费推送
     *
     * @param request
     * @return
     */
    FeesPushResponse feesPush(FeesPush request);


    /**
     * 查询快递详情
     * @param expid
     * @return
     */
    BaseResponse<DetailExpress> queryDetailExpress(String expid);
}
