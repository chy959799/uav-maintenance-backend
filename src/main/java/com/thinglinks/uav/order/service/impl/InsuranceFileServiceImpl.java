package com.thinglinks.uav.order.service.impl;

import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.constants.TemplateConstants;
import com.thinglinks.uav.common.enums.TemplateCode;
import com.thinglinks.uav.common.enums.TemplateEnum;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.template.*;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.WordUtils;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.CompanyRepository;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.order.dto.FilePreview;
import com.thinglinks.uav.order.dto.InsurancePreview;
import com.thinglinks.uav.order.dto.InsuranceTemplate;
import com.thinglinks.uav.order.entity.InsuranceMaterial;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.entity.OrderQuote;
import com.thinglinks.uav.order.repository.InsuranceMaterialRepository;
import com.thinglinks.uav.order.repository.OrderQuoteRepository;
import com.thinglinks.uav.order.service.InsuranceFileService;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.template.entity.Template;
import com.thinglinks.uav.template.entity.TemplateFile;
import com.thinglinks.uav.template.repository.TemplateFilesRepository;
import com.thinglinks.uav.template.repository.TemplatesRepository;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.thinglinks.uav.common.constants.CommonConstants.*;
import static com.thinglinks.uav.common.constants.OrderConstants.*;

@Service
@Slf4j
public class InsuranceFileServiceImpl implements InsuranceFileService {

    private static final String GENERATE_SUFFIX = ".docx";

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private InsuranceMaterialRepository insuranceMaterialRepository;

    @Resource
    private CustomMinIOClient customMinIOClient;

    @Resource
    private CustomersRepository customersRepository;

    @Resource
    private WordUtils wordUtils;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private TemplatesRepository templatesRepository;

    @Resource
    private TemplateFilesRepository templateFilesRepository;

    @Resource
    private OrderQuoteRepository orderQuoteRepository;

    @Resource
    private CustomConfig customConfig;

    @Resource
    private ProductRepository productRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private CompanyRepository companyRepository;


    @Override
    public void generateIntentionPayFile(String code, Order order) throws Exception {
        String uid = CommonUtils.getUid();
        Device device = deviceRepository.findByDevid(order.getDevid());
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
        OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
        double total = 0;
        if (Objects.nonNull(customerQuote)) {
            total = CommonUtils.format2Point(Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00));
        }
        Repay repay = new Repay();
        CustomFile repayFile = new CustomFile();
        repay.setCustomerName(accidentCustomer.getName());
        repay.setInsurancesNumber(device.getInsuranceCode());
        repay.setIncidentNumber(StringUtils.isNotBlank(insuranceMaterial.getReportNo()) ? insuranceMaterial.getReportNo() : EMPTY_20);
        String faultDate = DateUtils.formatDate(insuranceMaterial.getFaultAt());
        repay.setYear(faultDate.substring(0, 4));
        repay.setMonth(faultDate.substring(4, 6));
        repay.setDay(faultDate.substring(6));
        repay.setAccidentLocation(insuranceMaterial.getFaultLocation());
        repay.setAccidentCause(insuranceMaterial.getFaultReason());
        repay.setAmount(String.valueOf(total));
        repay.setAmountWords(CommonUtils.toChineseMoney(BigDecimal.valueOf(total)));
        String dateNow = DateUtils.formatDate(System.currentTimeMillis());
        repay.setNowYear(dateNow.substring(0, 4));
        repay.setNowMonth(dateNow.substring(4, 6));
        repay.setNowDay(dateNow.substring(6));
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        File f1;
        if (Objects.isNull(insuranceTemplate)) {
            f1 = wordUtils.generatePdf(repay);
            repayFile.setName("赔付意向书.pdf");
        } else {
            f1 = wordUtils.generatePdf(insuranceTemplate.getFile(), repay);
            repayFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
        }
        repayFile.setFid(CommonUtils.uuid());
        String o1 = repayFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
        customMinIOClient.uploadFile(ORDER_BUCKET, o1, f1.getAbsolutePath());
        repayFile.setDownload(Boolean.FALSE);
        repayFile.setDirectory(ORDER_BUCKET);
        repayFile.setExtension(CommonConstants.FILE_PDF);
        repayFile.setEnable(Boolean.TRUE);
        repayFile.setMime(CommonConstants.FILE_PDF);
        repayFile.setLocation(o1);
        repayFile.setUid(uid);
        repayFile.setOrdid(order.getOrdid());
        repayFile.setType(INTENTION_TO_PAY);
        //删除之前的文件
        this.deleteFile(order.getOrdid(), INTENTION_TO_PAY);
        fileRepository.save(repayFile);
        FileUtils.deleteQuietly(f1);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
    }

    @Override
    public FilePreview generateIntentionPayFile(String code, InsurancePreview request) throws Exception {
        FilePreview repayFile = new FilePreview();
        Repay repay = new Repay();
        Customer customer = customersRepository.findByCid(request.getCid());
        if (Objects.nonNull(customer)) {
            repay.setCustomerName(customer.getName());
        }
        repay.setInsurancesNumber(request.getInsuranceCode());
        repay.setIncidentNumber(StringUtils.isNotBlank(request.getReportNo()) ? request.getReportNo() : EMPTY_20);
        String faultDate = DateUtils.formatDate(request.getFaultAt());
        repay.setYear(faultDate.substring(0, 4));
        repay.setMonth(faultDate.substring(4, 6));
        repay.setDay(faultDate.substring(6));
        repay.setAccidentLocation(request.getFaultLocation());
        repay.setAccidentCause(request.getFaultReason());
        String dateNow = DateUtils.formatDate(System.currentTimeMillis());
        repay.setNowYear(dateNow.substring(0, 4));
        repay.setNowMonth(dateNow.substring(4, 6));
        repay.setNowDay(dateNow.substring(6));
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        File f1;
        if (Objects.isNull(insuranceTemplate)) {
            f1 = wordUtils.generatePdf(repay);
            repayFile.setFileName("赔付意向书.pdf");
        } else {
            f1 = wordUtils.generatePdf(insuranceTemplate.getFile(), repay);
            repayFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
        }
        String o1 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
        customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o1, f1.getAbsolutePath());
        String url = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o1);
        repayFile.setFileUrl(url);
        FileUtils.deleteQuietly(f1);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
        return repayFile;
    }

    @Override
    public void generatePowerRiskFile(String code, Order order) throws Exception {
        String uid = CommonUtils.getUid();
        Device device = deviceRepository.findByDevid(order.getDevid());
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
        CustomFile claimApplicationFile = new CustomFile();
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        if (TemplateCode.T_0002.getCode().equals(code)) {
            ClaimApplicationV2 claimApplication = new ClaimApplicationV2();
            claimApplication.setCompany1(accidentCustomer.getName());
            claimApplication.setInsurancesNumber(device.getInsuranceCode());
            claimApplication.setIncidentNumber(insuranceMaterial.getReportNo());
            Product product = productRepository.findByPid(device.getPid());
            claimApplication.setProductName(product.getName());
            claimApplication.setCode(device.getCode());
            claimApplication.setCompany2(insuranceMaterial.getFaultLocation());
            claimApplication.setContactName(insuranceMaterial.getAccidentUser());
            claimApplication.setContactPhone(insuranceMaterial.getAccidentTel());
            claimApplication.setAccidentTimeDay(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
            claimApplication.setAccidentReason(this.getFaultProcess(insuranceMaterial, accidentCustomer));
            claimApplication.setAccidentTimeHour(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
            claimApplication.setPilotName(insuranceMaterial.getPilotName());
            File f2;
            if (Objects.isNull(insuranceTemplate)) {
                f2 = wordUtils.generatePdf(claimApplication);
                claimApplicationFile.setName("电力业务出险通知及索赔申请书.pdf");
            } else {
                f2 = wordUtils.generatePdf(insuranceTemplate.getFile(), claimApplication);
                claimApplicationFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            claimApplicationFile.setFid(CommonUtils.uuid());
            String o2 = claimApplicationFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o2, f2.getAbsolutePath());
            claimApplicationFile.setLocation(o2);
            FileUtils.deleteQuietly(f2);
        } else {
            ClaimApplication claimApplication = new ClaimApplication();
            claimApplication.setCompany1(accidentCustomer.getName());
            claimApplication.setInsurancesNumber(device.getInsuranceCode());
            claimApplication.setIncidentNumber(insuranceMaterial.getReportNo());
            Product product = productRepository.findByPid(device.getPid());
            claimApplication.setProductName(product.getName());
            claimApplication.setCompany2(insuranceMaterial.getFaultLocation());
            claimApplication.setContactName(insuranceMaterial.getAccidentUser());
            claimApplication.setContactPhone(insuranceMaterial.getAccidentTel());
            claimApplication.setAccidentTimeDay(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
            claimApplication.setAccidentReason(insuranceMaterial.getFaultProcess());
            claimApplication.setAccidentTimeHour(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
            claimApplication.setPilotName(insuranceMaterial.getPilotName());
            File f2;
            if (Objects.isNull(insuranceTemplate)) {
                f2 = wordUtils.generatePdf(claimApplication);
                claimApplicationFile.setName("电力业务出险通知及索赔申请书.pdf");
            } else {
                f2 = wordUtils.generatePdf(insuranceTemplate.getFile(), claimApplication);
                claimApplicationFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            claimApplicationFile.setFid(CommonUtils.uuid());
            String o2 = claimApplicationFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o2, f2.getAbsolutePath());
            claimApplicationFile.setLocation(o2);
            FileUtils.deleteQuietly(f2);
        }
        claimApplicationFile.setDownload(Boolean.TRUE);
        claimApplicationFile.setDirectory(ORDER_BUCKET);
        claimApplicationFile.setExtension(CommonConstants.FILE_PDF);
        claimApplicationFile.setEnable(Boolean.TRUE);
        claimApplicationFile.setMime(CommonConstants.FILE_PDF);
        claimApplicationFile.setUid(uid);
        claimApplicationFile.setOrdid(order.getOrdid());
        claimApplicationFile.setType(POWER_BUSINESS_ACCIDENT_NOTIFICATION_AND_CLAIM_APPLICATION_FORM);
        fileRepository.save(claimApplicationFile);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
    }

    @Override
    public FilePreview generatePowerRiskFile(String code, InsurancePreview request) throws Exception {
        Customer customer = customersRepository.findByCid(request.getCid());
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        FilePreview claimApplicationFile = new FilePreview();
        if (TemplateCode.T_0002.getCode().equals(code)) {
            ClaimApplicationV2 claimApplication = new ClaimApplicationV2();
            if (Objects.nonNull(customer)) {
                claimApplication.setCompany1(customer.getName());
            }
            claimApplication.setInsurancesNumber(request.getInsuranceCode());
            claimApplication.setIncidentNumber(request.getReportNo());
            Product product = productRepository.findByPid(request.getPid());
            claimApplication.setProductName(product.getName());
            claimApplication.setCode(request.getCode());
            claimApplication.setCompany2(request.getFaultLocation());
            claimApplication.setContactName(request.getAccidentUser());
            claimApplication.setContactPhone(request.getAccidentTel());
            claimApplication.setAccidentTimeDay(DateUtils.formatDateChinese2(request.getFaultAt()));
            claimApplication.setAccidentReason(this.getFaultProcess(request, customer));
            claimApplication.setAccidentTimeHour(DateUtils.formatDateChinese2(request.getFaultAt()));
            claimApplication.setPilotName(request.getPilotName());
            File f2;
            if (Objects.isNull(insuranceTemplate)) {
                f2 = wordUtils.generatePdf(claimApplication);
                claimApplicationFile.setFileName("电力业务出险通知及索赔申请书.pdf");
            } else {
                f2 = wordUtils.generatePdf(insuranceTemplate.getFile(), claimApplication);
                claimApplicationFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            String o2 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o2, f2.getAbsolutePath());
            String url2 = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o2);
            claimApplicationFile.setFileUrl(url2);
            FileUtils.deleteQuietly(f2);
        } else {
            ClaimApplication claimApplication = new ClaimApplication();
            if (Objects.nonNull(customer)) {
                claimApplication.setCompany1(customer.getName());
            }
            claimApplication.setInsurancesNumber(request.getInsuranceCode());
            claimApplication.setIncidentNumber(request.getReportNo());
            Product product = productRepository.findByPid(request.getPid());
            claimApplication.setProductName(product.getName());
            claimApplication.setCompany2(request.getFaultLocation());
            claimApplication.setContactName(request.getAccidentUser());
            claimApplication.setContactPhone(request.getAccidentTel());
            claimApplication.setAccidentTimeDay(DateUtils.formatDateChinese2(request.getFaultAt()));
            claimApplication.setAccidentReason(request.getFaultProcess());
            claimApplication.setAccidentTimeHour(DateUtils.formatDateChinese2(request.getFaultAt()));
            claimApplication.setPilotName(request.getPilotName());
            File f2;
            if (Objects.isNull(insuranceTemplate)) {
                f2 = wordUtils.generatePdf(claimApplication);
                claimApplicationFile.setFileName("电力业务出险通知及索赔申请书.pdf");
            } else {
                f2 = wordUtils.generatePdf(insuranceTemplate.getFile(), claimApplication);
                claimApplicationFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            String o2 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o2, f2.getAbsolutePath());
            String url2 = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o2);
            claimApplicationFile.setFileUrl(url2);
            FileUtils.deleteQuietly(f2);
        }
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
        return claimApplicationFile;
    }

    @Override
    public void generateLossFile(String code, Order order) throws Exception {
        String uid = CommonUtils.getUid();
        Device device = deviceRepository.findByDevid(order.getDevid());
        Product product = productRepository.findByPid(device.getPid());
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        if (!CommonUtils.int2BBoolean(order.getSecondary()) && ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType()) && Objects.nonNull(insuranceMaterial)) {
            OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
            double total = 0;
            if (Objects.nonNull(customerQuote)) {
                total = CommonUtils.format2Point(Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00));
            }
            Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
            List<LossItem> lossItems = new ArrayList<>();
            LossItem lossItem = new LossItem();
            CustomFile lossFile = new CustomFile();
            lossItem.setCustomerName(accidentCustomer.getName());
            List<Item> items = new ArrayList<>();
            Item item = new Item();
            item.setProductName(product.getName());
            item.setProductSpec(product.getModel());
            item.setProductUnit(product.getUnit());
            item.setLossQuantity(CommonConstants.ONE_VALUE);
            item.setCustomerPrice(String.valueOf(total));
            item.setSnCode(device.getCode());
            items.add(item);
            lossItem.setItems(items);
            lossItems.add(lossItem);
            InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
            File f3;
            if (Objects.isNull(insuranceTemplate)) {
                f3 = wordUtils.generateLossItems(lossItems);
                lossFile.setName("损失清单.pdf");
            } else {
                f3 = wordUtils.generateLossItems(lossItems, Files.newInputStream(insuranceTemplate.getFile().toPath()));
                lossFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            lossFile.setFid(CommonUtils.uuid());
            String o3 = lossFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o3, f3.getAbsolutePath());
            lossFile.setDownload(Boolean.TRUE);
            lossFile.setDirectory(ORDER_BUCKET);
            lossFile.setExtension(CommonConstants.FILE_PDF);
            lossFile.setEnable(Boolean.TRUE);
            lossFile.setMime(CommonConstants.FILE_PDF);
            lossFile.setLocation(o3);
            lossFile.setUid(uid);
            lossFile.setOrdid(order.getOrdid());
            lossFile.setType(CommonConstants.LOSS_LIST);
            this.deleteFile(order.getOrdid(), LOSS_LIST);
            fileRepository.save(lossFile);
            FileUtils.deleteQuietly(f3);
            if (Objects.nonNull(insuranceTemplate)) {
                FileUtils.deleteQuietly(insuranceTemplate.getFile());
            }
        } else {
            log.info("not need generateLossFile");
        }

    }

    @Override
    public FilePreview generateLossFile(String code, InsurancePreview request) throws Exception {
        Customer customer = customersRepository.findByCid(request.getCid());
        Product product = productRepository.findByPid(request.getPid());
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        FilePreview lossFile = new FilePreview();
        List<LossItem> lossItems = new ArrayList<>();
        LossItem lossItem = new LossItem();
        lossItem.setCustomerName(customer.getName());
        lossItem.setFaultPlace(request.getFaultLocation());
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setProductName(product.getName());
        item.setProductSpec(product.getModel());
        item.setProductUnit(product.getUnit());
        item.setLossQuantity(CommonConstants.ONE_VALUE);
        items.add(item);
        lossItem.setItems(items);
        lossItems.add(lossItem);
        File f3;
        if (Objects.isNull(insuranceTemplate)) {
            f3 = wordUtils.generateLossItems(lossItems);
            lossFile.setFileName("损失清单.pdf");
        } else {
            f3 = wordUtils.generateLossItems(lossItems, Files.newInputStream(insuranceTemplate.getFile().toPath()));
            lossFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
        }
        String o3 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
        customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o3, f3.getAbsolutePath());
        String url3 = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o3);
        lossFile.setFileUrl(url3);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
        return lossFile;
    }

    @Override
    public void generateFaultFile(String code, Order order) throws Exception {
        String uid = CommonUtils.getUid();
        Device device = deviceRepository.findByDevid(order.getDevid());
        Product product = productRepository.findByPid(device.getPid());
        InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        CustomFile faultFile = new CustomFile();
        if (TemplateCode.T_0002.getCode().equals(code)) {
            Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
            AccidentCertificate certificate = new AccidentCertificate();
            certificate.setName(insuranceMaterial.getPilotName());
            certificate.setDate(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
            certificate.setCompany(accidentCustomer.getName());
            certificate.setLocation(insuranceMaterial.getFaultLocation());
            certificate.setType(insuranceMaterial.getWorkType());
            certificate.setReason(insuranceMaterial.getFaultReason());
            certificate.setCode(device.getCode());
            certificate.setSituation(insuranceMaterial.getDamaged());
            File f4;
            if (Objects.isNull(insuranceTemplate)) {
                f4 = wordUtils.generatePdf(certificate);
                faultFile.setName("事故证明.pdf");
            } else {
                f4 = wordUtils.generatePdf(insuranceTemplate.getFile(), certificate);
                faultFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            faultFile.setFid(CommonUtils.uuid());
            String o4 = faultFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o4, f4.getAbsolutePath());
            faultFile.setLocation(o4);
            FileUtils.deleteQuietly(f4);
        } else {
            FaultReport faultReport = new FaultReport();
            faultReport.setAccidentTimeDay(DateUtils.formatDateChinese(insuranceMaterial.getFaultAt()));
            faultReport.setFaultReason(insuranceMaterial.getFaultReason());
            faultReport.setSnCode(device.getCode());
            faultReport.setHandlingProcess(insuranceMaterial.getFaultProcess());
            faultReport.setDeviceModel(Optional.ofNullable(product.getName()).orElse(StringUtils.EMPTY).concat(Optional.ofNullable(product.getModel()).orElse(StringUtils.EMPTY)).concat(Optional.ofNullable(device.getCode()).orElse(StringUtils.EMPTY)));
            faultReport.setManufacturer(product.getOrigin());
            faultReport.setProduct(Optional.ofNullable(product.getName()).orElse(StringUtils.EMPTY).concat(Optional.ofNullable(product.getModel()).orElse(StringUtils.EMPTY)));
            File f4;
            if (Objects.isNull(insuranceTemplate)) {
                f4 = wordUtils.generatePdf(faultReport);
                faultFile.setName("无人机坠落损失分析报告-故障报告.pdf");
            } else {
                f4 = wordUtils.generatePdf(insuranceTemplate.getFile(), faultReport);
                faultFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            faultFile.setFid(CommonUtils.uuid());
            String o4 = faultFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o4, f4.getAbsolutePath());
            faultFile.setLocation(o4);
            FileUtils.deleteQuietly(f4);
        }
        faultFile.setDownload(Boolean.TRUE);
        faultFile.setDirectory(ORDER_BUCKET);
        faultFile.setExtension(CommonConstants.FILE_PDF);
        faultFile.setEnable(Boolean.TRUE);
        faultFile.setMime(CommonConstants.FILE_PDF);
        faultFile.setUid(uid);
        faultFile.setOrdid(order.getOrdid());
        faultFile.setType(CommonConstants.FAULT_REPORT_TEMPLATE_PATH);
        fileRepository.save(faultFile);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
    }

    @Override
    public FilePreview generateFaultFile(String code, InsurancePreview request) throws Exception {
        Product product = productRepository.findByPid(request.getPid());
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        FilePreview faultFile = new FilePreview();
        if (TemplateCode.T_0002.getCode().equals(code)) {
            Customer customer = customersRepository.findByCid(request.getCid());
            AccidentCertificate certificate = new AccidentCertificate();
            certificate.setName(request.getPilotName());
            certificate.setDate(DateUtils.formatDateChinese2(request.getFaultAt()));
            certificate.setCompany(customer.getName());
            certificate.setLocation(request.getFaultLocation());
            certificate.setType(request.getWorkType());
            certificate.setReason(request.getFaultReason());
            certificate.setCode(request.getCode());
            certificate.setSituation(request.getDamaged());
            File f4;
            if (Objects.isNull(insuranceTemplate)) {
                f4 = wordUtils.generatePdf(certificate);
                faultFile.setFileName("事故证明.pdf");
            } else {
                f4 = wordUtils.generatePdf(insuranceTemplate.getFile(), certificate);
                faultFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            String o4 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o4, f4.getAbsolutePath());
            String url4 = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o4);
            faultFile.setFileUrl(url4);
            FileUtils.deleteQuietly(f4);
        } else {
            FaultReport faultReport = new FaultReport();
            faultReport.setAccidentTimeDay(DateUtils.formatDateChinese(request.getFaultAt()));
            faultReport.setFaultReason(request.getFaultReason());
            faultReport.setSnCode(request.getCode());
            faultReport.setHandlingProcess(request.getFaultProcess());
            faultReport.setDeviceModel(
                    Optional.ofNullable(product.getName())
                            .orElse(StringUtils.EMPTY)
                            .concat(Objects.nonNull(product.getModel()) ? product.getModel() : StringUtils.EMPTY)
                            .concat(Objects.nonNull(request.getCode()) ? request.getCode() : StringUtils.EMPTY)
            );
            faultReport.setManufacturer(product.getOrigin());
            faultReport.setProduct(Optional.ofNullable(product.getName()).orElse(StringUtils.EMPTY).concat(Optional.ofNullable(product.getModel()).orElse(StringUtils.EMPTY)));
            File f4;
            if (Objects.isNull(insuranceTemplate)) {
                f4 = wordUtils.generatePdf(faultReport);
                faultFile.setFileName("无人机坠落损失分析报告-故障报告.pdf");
            } else {
                f4 = wordUtils.generatePdf(insuranceTemplate.getFile(), faultReport);
                faultFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            String o4 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o4, f4.getAbsolutePath());
            String url4 = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o4);
            faultFile.setFileUrl(url4);
            FileUtils.deleteQuietly(f4);
        }
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
        return faultFile;
    }

    @Override
    public void generateFaultPhotoFile(String code, Order order) throws Exception {
        String uid = CommonUtils.getUid();
        CustomFile faultPhotoFile = new CustomFile();
        FaultPhoto faultPhoto = new FaultPhoto();
        StringBuilder accidentScene = new StringBuilder();
        List<CustomFile> accidentSceneFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_SCENE);
        accidentSceneFiles.forEach(x -> {
            if (accidentScene.length() > 0) {
                accidentScene.append(";");
            }
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            accidentScene.append(x.getDirectory()).append("/").append(objectName);
        });
        faultPhoto.setAccidentScene(accidentScene.toString());
        StringBuilder manifest = new StringBuilder();
        List<CustomFile> manifestFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_ITEMS);
        manifestFiles.forEach(x -> {
            if (manifest.length() > 0) {
                manifest.append(";");
            }
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            manifest.append(x.getDirectory()).append("/").append(objectName);
        });
        faultPhoto.setManifest(manifest.toString());
        StringBuilder damageDevice = new StringBuilder();
        List<CustomFile> damageDeviceFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_DEVICES);
        damageDeviceFiles.forEach(x -> {
            if (damageDevice.length() > 0) {
                damageDevice.append(";");
            }
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            damageDevice.append(x.getDirectory()).append("/").append(objectName);
        });
        faultPhoto.setDamageDevice(damageDevice.toString());
        StringBuilder front = new StringBuilder();
        List<CustomFile> frontFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_DEVICE_FRONT);
        frontFiles.forEach(x -> {
            if (front.length() > 0) {
                front.append(";");
            }
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            front.append(x.getDirectory()).append("/").append(objectName);
        });
        faultPhoto.setFront(front.toString());
        StringBuilder back = new StringBuilder();
        List<CustomFile> backFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_DEVICE_REVERSE);
        backFiles.forEach(x -> {
            if (back.length() > 0) {
                back.append(";");
            }
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            back.append(x.getDirectory()).append("/").append(objectName);
        });
        faultPhoto.setBack(back.toString());
        StringBuilder other = new StringBuilder();
        List<CustomFile> otherFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_OTHER);
        otherFiles.forEach(x -> {
            if (other.length() > 0) {
                other.append(";");
            }
            String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
            other.append(x.getDirectory()).append("/").append(objectName);
        });
        faultPhoto.setOther(other.toString());
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        File f5;
        if (Objects.isNull(insuranceTemplate)) {
            f5 = wordUtils.generatePdf(faultPhoto);
            faultPhotoFile.setName("事故照片.pdf");
        } else {
            f5 = wordUtils.generatePdf(insuranceTemplate.getFile(), faultPhoto);
            faultPhotoFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
        }
        faultPhotoFile.setFid(CommonUtils.uuid());
        String o5 = faultPhotoFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
        customMinIOClient.uploadFile(ORDER_BUCKET, o5, f5.getAbsolutePath());
        faultPhotoFile.setDownload(Boolean.TRUE);
        faultPhotoFile.setDirectory(ORDER_BUCKET);
        faultPhotoFile.setExtension(CommonConstants.FILE_PDF);
        faultPhotoFile.setEnable(Boolean.TRUE);
        faultPhotoFile.setMime(CommonConstants.FILE_PDF);
        faultPhotoFile.setLocation(o5);
        faultPhotoFile.setUid(uid);
        faultPhotoFile.setOrdid(order.getOrdid());
        faultPhotoFile.setType(CommonConstants.FAULT_PHOTO);
        fileRepository.save(faultPhotoFile);
        FileUtils.deleteQuietly(f5);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
    }

    @Override
    public FilePreview generateFaultPhotoFile(String code, InsurancePreview request) throws Exception {
        InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
        FilePreview faultPhotoFile = new FilePreview();
        FaultPhoto faultPhoto = new FaultPhoto();
        StringBuilder accidentScene = new StringBuilder();
        List<String> accidentSceneFids = request.getAccidentScene();
        if (CollectionUtils.isNotEmpty(accidentSceneFids)) {
            accidentSceneFids.forEach(x -> {
                CustomFile cf = fileRepository.findByFid(x);
                if (Objects.nonNull(cf)) {
                    if (accidentScene.length() > 0) {
                        accidentScene.append(";");
                    }
                    String objectName = cf.getFid().concat(CommonConstants.DOT).concat(cf.getExtension());
                    accidentScene.append(cf.getDirectory()).append("/").append(objectName);
                }
            });
            faultPhoto.setAccidentScene(accidentScene.toString());
        }
        StringBuilder manifest = new StringBuilder();
        List<String> manifestFids = request.getAccidentDamageItems();
        if (CollectionUtils.isNotEmpty(manifestFids)) {
            manifestFids.forEach(x -> {
                CustomFile cf = fileRepository.findByFid(x);
                if (Objects.nonNull(cf)) {
                    if (manifest.length() > 0) {
                        manifest.append(";");
                    }
                    String objectName = cf.getFid().concat(CommonConstants.DOT).concat(cf.getExtension());
                    manifest.append(cf.getDirectory()).append("/").append(objectName);
                }
            });
            faultPhoto.setManifest(manifest.toString());
        }
        StringBuilder damageDevice = new StringBuilder();
        List<String> damageDeviceFids = request.getAccidentDamageItems();
        if (CollectionUtils.isNotEmpty(damageDeviceFids)) {
            damageDeviceFids.forEach(x -> {
                CustomFile cf = fileRepository.findByFid(x);
                if (Objects.nonNull(cf)) {
                    if (damageDevice.length() > 0) {
                        damageDevice.append(";");
                    }
                    String objectName = cf.getFid().concat(CommonConstants.DOT).concat(cf.getExtension());
                    damageDevice.append(cf.getDirectory()).append("/").append(objectName);
                }
            });
            faultPhoto.setDamageDevice(damageDevice.toString());
        }
        StringBuilder front = new StringBuilder();
        List<String> frontFids = request.getDeviceFront();
        if (CollectionUtils.isNotEmpty(frontFids)) {
            frontFids.forEach(x -> {
                CustomFile cf = fileRepository.findByFid(x);
                if (Objects.nonNull(cf)) {
                    if (front.length() > 0) {
                        front.append(";");
                    }
                    String objectName = cf.getFid().concat(CommonConstants.DOT).concat(cf.getExtension());
                    front.append(cf.getDirectory()).append("/").append(objectName);
                }
            });
            faultPhoto.setFront(front.toString());
        }
        StringBuilder back = new StringBuilder();
        List<String> backFids = request.getDeviceReverse();
        if (CollectionUtils.isNotEmpty(backFids)) {
            backFids.forEach(x -> {
                CustomFile cf = fileRepository.findByFid(x);
                if (Objects.nonNull(cf)) {
                    if (back.length() > 0) {
                        back.append(";");
                    }
                    String objectName = cf.getFid().concat(CommonConstants.DOT).concat(cf.getExtension());
                    back.append(cf.getDirectory()).append("/").append(objectName);
                }
            });
            faultPhoto.setBack(back.toString());
        }
        StringBuilder other = new StringBuilder();
        List<String> otherFids = request.getAccidentOther();
        if (CollectionUtils.isNotEmpty(otherFids)) {
            otherFids.forEach(x -> {
                CustomFile cf = fileRepository.findByFid(x);
                if (Objects.nonNull(cf)) {
                    if (other.length() > 0) {
                        other.append(";");
                    }
                    String objectName = cf.getFid().concat(CommonConstants.DOT).concat(cf.getExtension());
                    other.append(cf.getDirectory()).append("/").append(objectName);
                }
            });
            faultPhoto.setOther(other.toString());
        }
        File f5;
        if (Objects.isNull(insuranceTemplate)) {
            f5 = wordUtils.generatePdf(faultPhoto);
            faultPhotoFile.setFileName("事故照片.pdf");
        } else {
            f5 = wordUtils.generatePdf(insuranceTemplate.getFile(), faultPhoto);
            faultPhotoFile.setFileName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
        }
        String o5 = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
        customMinIOClient.uploadFile(CommonConstants.TEMP_BUCKET, o5, f5.getAbsolutePath());
        String url5 = customMinIOClient.getProxyUrl(CommonConstants.TEMP_BUCKET, o5);
        faultPhotoFile.setFileUrl(url5);
        FileUtils.deleteQuietly(f5);
        if (Objects.nonNull(insuranceTemplate)) {
            FileUtils.deleteQuietly(insuranceTemplate.getFile());
        }
        return faultPhotoFile;
    }

    @Override
    public void generateFeeNotification(String code, Order order) throws Exception {
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            String uid = CommonUtils.getUid();
            InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
            OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
            double total = 0;
            if (Objects.nonNull(customerQuote)) {
                total = Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00);
            }
            //收费通知单
            FeeNotification feeNotification = new FeeNotification();
            CustomFile feeNotificationFile = new CustomFile();
            feeNotification.setInsurancesNumber(device.getInsuranceCode());
            feeNotification.setIncidentNumber(insuranceMaterial.getReportNo());
            feeNotification.setInsuranceType(CommonUtils.orderInsuranceTypeConvert(device.getInsuranceType()));
            feeNotification.setAccidentReason(insuranceMaterial.getFaultReason());
            feeNotification.setAccidentTimeDay(DateUtils.formatDateChinese(insuranceMaterial.getFaultAt()));
            feeNotification.setReportContact(insuranceMaterial.getAccidentUser());
            feeNotification.setAccidentLocation(insuranceMaterial.getFaultLocation());
            feeNotification.setEntrustDate(DateUtils.formatDateChinese(insuranceMaterial.getFaultAt()));
            feeNotification.setContactPhone(insuranceMaterial.getAccidentTel());
            feeNotification.setTotalFee(String.valueOf(total));
            feeNotification.setRepairFeeSubtotal(String.valueOf(total));
            InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
            File f1;
            if (Objects.isNull(insuranceTemplate)) {
                f1 = wordUtils.generatePdf(feeNotification);
                feeNotificationFile.setName("收费通知单.pdf");
            } else {
                f1 = wordUtils.generatePdf(insuranceTemplate.getFile(), feeNotification);
                feeNotificationFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            feeNotificationFile.setFid(CommonUtils.uuid());
            String o1 = feeNotificationFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_PDF);
            customMinIOClient.uploadFile(ORDER_BUCKET, o1, f1.getAbsolutePath());
            feeNotificationFile.setDownload(Boolean.TRUE);
            feeNotificationFile.setDirectory(ORDER_BUCKET);
            feeNotificationFile.setExtension(CommonConstants.FILE_PDF);
            feeNotificationFile.setEnable(Boolean.TRUE);
            feeNotificationFile.setMime(CommonConstants.FILE_PDF);
            feeNotificationFile.setLocation(o1);
            feeNotificationFile.setUid(uid);
            feeNotificationFile.setOrdid(order.getOrdid());
            feeNotificationFile.setType(CommonConstants.FEE_NOTIFICATION);
            fileRepository.save(feeNotificationFile);
            FileUtils.deleteQuietly(f1);
            if (Objects.nonNull(insuranceTemplate)) {
                FileUtils.deleteQuietly(insuranceTemplate.getFile());
            }
        }
    }

    @Override
    public void generateIncidentReport(String code, Order order) throws Exception {
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            String uid = CommonUtils.getUid();
            InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
            Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
            Product product = productRepository.findByPid(device.getPid());
            OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
            double total = 0;
            if (Objects.nonNull(customerQuote)) {
                total = Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00);
            }
            IncidentReport incidentReport = new IncidentReport();
            CustomFile incidentReportFile = new CustomFile();
            incidentReport.setCustomer(accidentCustomer.getName());
            incidentReport.setAccidentTime(DateUtils.formatDateChinese(insuranceMaterial.getFaultAt()));
            incidentReport.setPolicyNo(device.getInsuranceCode());
            incidentReport.setReportNo(insuranceMaterial.getReportNo());
            incidentReport.setInsuranceType(CommonUtils.orderInsuranceTypeConvert(device.getInsuranceType()));
            incidentReport.setLossSubjectMatter(product.getName());
            incidentReport.setAccidentLocation(insuranceMaterial.getFaultLocation());
            incidentReport.setCompensationAmount(String.valueOf(total));
            incidentReport.setDeviceCode(device.getCode());
            incidentReport.setUnderwritingTime(DateUtils.formatDateChinese(device.getInsuranceStartAt()));
            incidentReport.setUnderwritingInformation(String.format("%s，保险期限自%s 零时起 至 %s  二十四时止，保险金额CNY%s。", device.getInsuranceCode(), DateUtils.formatDateLine(device.getInsuranceStartAt()), DateUtils.formatDateLine(device.getInsuranceExpireAt()), CommonUtils.formatTosepara(device.getInsuranceTotalQuota())));
            incidentReport.setAccidentReason(insuranceMaterial.getFaultProcess());
            if (TemplateCode.T_0002.getCode().equals(code)) {
                incidentReport.setAccidentReason(this.getFaultProcess(insuranceMaterial, accidentCustomer));
            }
            incidentReport.setAccidentDate(DateUtils.formatDateChinese(insuranceMaterial.getFaultAt()));
            incidentReport.setStartDate(DateUtils.formatDateChinese(device.getInsuranceStartAt()));
            incidentReport.setEndDate(DateUtils.formatDateChinese(device.getInsuranceExpireAt()));
            StringBuilder accidentScene = new StringBuilder();
            List<CustomFile> accidentSceneFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_SCENE);
            accidentSceneFiles.forEach(x -> {
                if (accidentScene.length() > 0) {
                    accidentScene.append(";");
                }
                String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
                accidentScene.append(x.getDirectory()).append("/").append(objectName);
            });
            incidentReport.setAccidentPhoto(accidentScene.toString());
            StringBuilder damageDevice = new StringBuilder();
            List<CustomFile> damageDeviceFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_DAMAGE_DEVICES);
            damageDeviceFiles.forEach(x -> {
                if (damageDevice.length() > 0) {
                    damageDevice.append(";");
                }
                String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
                damageDevice.append(x.getDirectory()).append("/").append(objectName);
            });
            incidentReport.setSnCodePhoto(damageDevice.toString());
            StringBuilder front = new StringBuilder();
            List<CustomFile> frontFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_DEVICE_FRONT);
            frontFiles.forEach(x -> {
                if (front.length() > 0) {
                    front.append(";");
                }
                String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
                front.append(x.getDirectory()).append("/").append(objectName);
            });
            incidentReport.setDeviceFrontPhoto(front.toString());
            StringBuilder back = new StringBuilder();
            List<CustomFile> backFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_DEVICE_REVERSE);
            backFiles.forEach(x -> {
                if (back.length() > 0) {
                    back.append(";");
                }
                String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
                back.append(x.getDirectory()).append("/").append(objectName);
            });
            incidentReport.setDeviceBackPhoto(back.toString());
            StringBuilder other = new StringBuilder();
            List<CustomFile> otherFiles = fileRepository.findByOrdidAndType(order.getOrdid(), CommonConstants.FILE_TYPE_ACCIDENT_OTHER);
            otherFiles.forEach(x -> {
                if (other.length() > 0) {
                    other.append(";");
                }
                String objectName = x.getFid().concat(CommonConstants.DOT).concat(x.getExtension());
                other.append(x.getDirectory()).append("/").append(objectName);
            });
            incidentReport.setDeviceOtherPhoto(other.toString());
            CustomFile customerQuoteFile = fileRepository.getByOrdidAndType(order.getOrdid(), CUSTOMER_QUOTE_WITHOUT_SIGN);
            if (Objects.nonNull(customerQuoteFile)) {
                String objectName = customerQuoteFile.getFid().concat(CommonConstants.DOT).concat(customerQuoteFile.getExtension());
                incidentReport.setMaintenanceQuotation(customerQuoteFile.getDirectory().concat("/").concat(objectName));
            }
            incidentReport.setReportTime(DateUtils.formatDateChinese(System.currentTimeMillis()));
            InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
            File f2;
            if (Objects.isNull(insuranceTemplate)) {
                f2 = wordUtils.generatePdf(incidentReport);
                incidentReportFile.setName("案件报告.docx");
            } else {
                f2 = wordUtils.generatePdf(insuranceTemplate.getFile(), incidentReport);
                incidentReportFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
            }
            incidentReportFile.setFid(CommonUtils.uuid());
            String o2 = incidentReportFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_DOCX);
            customMinIOClient.uploadFile(ORDER_BUCKET, o2, f2.getAbsolutePath());
            incidentReportFile.setDownload(Boolean.TRUE);
            incidentReportFile.setDirectory(ORDER_BUCKET);
            incidentReportFile.setExtension(CommonConstants.FILE_DOCX);
            incidentReportFile.setEnable(Boolean.TRUE);
            incidentReportFile.setMime(CommonConstants.FILE_DOCX);
            incidentReportFile.setLocation(o2);
            incidentReportFile.setUid(uid);
            incidentReportFile.setOrdid(order.getOrdid());
            incidentReportFile.setType(CommonConstants.INCIDENT_REPORT);
            fileRepository.save(incidentReportFile);
            FileUtils.deleteQuietly(f2);
            if (Objects.nonNull(insuranceTemplate)) {
                FileUtils.deleteQuietly(insuranceTemplate.getFile());
            }
        }
    }

    @Override
    public void generateReport(String code, Order order) throws Exception {
        Device device = deviceRepository.findByDevid(order.getDevid());
        if (ORDER_IN_INSURANCE.equals(order.getServiceType()) && OrderConstants.INSURANCE_DH.equals(device.getInsuranceType())) {
            InsuranceMaterial insuranceMaterial = insuranceMaterialRepository.findByOrdid(order.getOrdid());
            //报案文件--无报案号则生成
            if (StringUtils.isBlank(insuranceMaterial.getReportNo())) {
                String uid = CommonUtils.getUid();
                Customer accidentCustomer = customersRepository.findByCid(insuranceMaterial.getCid());
                OrderQuote customerQuote = orderQuoteRepository.findFirstByOrdidAndType(order.getOrdid(), QUOTE_TYPE_CUSTOMER);
                double total = 0;
                if (Objects.nonNull(customerQuote)) {
                    total = Optional.ofNullable(customerQuote.getSparePrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getExpressesPrice()).orElse(0.00) + Optional.ofNullable(customerQuote.getOtherPrice()).orElse(0.00);
                }
                Report report = new Report();
                CustomFile reportFile = new CustomFile();
                report.setDate(DateUtils.formatDateChinese(System.currentTimeMillis()));
                report.setReporter(insuranceMaterial.getAccidentUser());
                report.setReporterPhone(insuranceMaterial.getAccidentTel());
                report.setInsurancesNumber(device.getInsuranceCode());
                report.setCompany(accidentCustomer.getName());
                report.setAccidentTimeDay(DateUtils.formatDateChinese(insuranceMaterial.getFaultAt()));
                report.setDeviceCode(device.getCode());
                report.setAccidentLocation(insuranceMaterial.getFaultLocation());
                User user = userRepository.findByUid(uid);
                report.setOperator(user.getUserName());
                report.setDamageAmount(String.valueOf(total));
                InsuranceTemplate insuranceTemplate = this.getTemplateFile(code, TemplateEnum.intention_to_pay.getType());
                File f3;
                if (Objects.isNull(insuranceTemplate)) {
                    f3 = wordUtils.generatePdf(report);
                    reportFile.setName("报案文件.docx");
                } else {
                    f3 = wordUtils.generatePdf(insuranceTemplate.getFile(), report);
                    reportFile.setName(insuranceTemplate.getDisplayName().concat(DOT).concat(FILE_PDF));
                }
                reportFile.setFid(CommonUtils.uuid());
                String o3 = reportFile.getFid().concat(CommonConstants.DOT).concat(CommonConstants.FILE_DOCX);
                customMinIOClient.uploadFile(ORDER_BUCKET, o3, f3.getAbsolutePath());
                reportFile.setDownload(Boolean.TRUE);
                reportFile.setDirectory(ORDER_BUCKET);
                reportFile.setExtension(CommonConstants.FILE_DOCX);
                reportFile.setEnable(Boolean.TRUE);
                reportFile.setMime(CommonConstants.FILE_DOCX);
                reportFile.setLocation(o3);
                reportFile.setUid(uid);
                reportFile.setOrdid(order.getOrdid());
                reportFile.setType(CommonConstants.REPORT_DOCUMENT);
                fileRepository.save(reportFile);
                FileUtils.deleteQuietly(f3);
                if (Objects.nonNull(insuranceTemplate)) {
                    FileUtils.deleteQuietly(insuranceTemplate.getFile());
                }
            }
        }
    }

    /**
     * 删除文件
     *
     * @param orderId
     * @param type
     */
    private void deleteFile(String orderId, String type) throws Exception {
        List<CustomFile> oldQuoteFile = fileRepository.findByOrdidAndType(orderId, type);
        if (CollectionUtils.isNotEmpty(oldQuoteFile)) {
            for (CustomFile customFile : oldQuoteFile) {
                String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
                customMinIOClient.removeFile(customFile.getDirectory(), objectName);
                fileRepository.delete(customFile);
            }
        }
    }

    /**
     * 获取上传的模板文件
     *
     * @param code
     * @param type
     * @return
     */
    private InsuranceTemplate getTemplateFile(String code, String type) {
        try {
            if (StringUtils.isBlank(code)) {
                return null;
            }
            Template template = templatesRepository.findByCode(code);
            if (Objects.isNull(template)) {
                return null;
            }
            TemplateFile templateFile = templateFilesRepository.findByTidAndType(template.getTid(), type);
            if (Objects.isNull(templateFile)) {
                return null;
            }
            InsuranceTemplate insuranceTemplate = new InsuranceTemplate();
            byte[] data = customMinIOClient.downloadFile(TemplateConstants.TEMPLATE_BUCKET, templateFile.getName());
            String fileName = customConfig.getPdfTempDir().concat(CommonUtils.uuid().concat(GENERATE_SUFFIX));
            File file = new File(fileName);
            FileUtils.writeByteArrayToFile(file, data, false);
            insuranceTemplate.setFile(file);
            insuranceTemplate.setDisplayName(templateFile.getDisplayName());
            return insuranceTemplate;
        } catch (Exception e) {
            log.error("getTemplateFile error", e);
        }
        return null;
    }


    /**
     * 获取处理经过
     *
     * @param insuranceMaterial
     * @return
     */
    private String getFaultProcess(InsuranceMaterial insuranceMaterial, Customer accidentCustomer) {
        StringBuilder sb = new StringBuilder();
        sb.append(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
        sb.append(COMMA);
        sb.append(accidentCustomer.getName());
        sb.append(insuranceMaterial.getPilotName());
        sb.append("在");
        sb.append(insuranceMaterial.getFaultLocation());
        sb.append("进行");
        sb.append(insuranceMaterial.getWorkType());
        sb.append("，因");
        sb.append(StringUtils.replaceChars(insuranceMaterial.getFaultReason(), "&", ""));
        sb.append("造成");
        sb.append(insuranceMaterial.getDamaged());
        sb.append("受损");
        return sb.toString();
    }

    /**
     * 获取处理经过
     *
     * @param insuranceMaterial
     * @return
     */
    private String getFaultProcess(InsurancePreview insuranceMaterial, Customer accidentCustomer) {
        StringBuilder sb = new StringBuilder();
        sb.append(DateUtils.formatDateChinese2(insuranceMaterial.getFaultAt()));
        sb.append(COMMA);
        sb.append(accidentCustomer.getName());
        sb.append(insuranceMaterial.getPilotName());
        sb.append("在");
        sb.append(insuranceMaterial.getFaultLocation());
        sb.append("进行");
        sb.append(insuranceMaterial.getWorkType());
        sb.append("，因");
        sb.append(StringUtils.replaceChars(insuranceMaterial.getFaultReason(), "&", ""));
        sb.append("造成");
        sb.append(insuranceMaterial.getDamaged());
        sb.append("受损");
        return sb.toString();
    }
}
