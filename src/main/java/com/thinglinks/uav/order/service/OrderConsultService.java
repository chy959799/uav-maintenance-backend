package com.thinglinks.uav.order.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.order.dto.consult.*;
import org.springframework.transaction.annotation.Transactional;

/**
 * @package: com.thinglinks.uav.order.service
 * @className: OrderConsultService
 * @author: rp
 * @description: TODO
 */
public interface OrderConsultService {
    BaseResponse<OrderConsultDetail> getOrderConsult(String ordid) throws Exception;

    BaseResponse<OrderConsultResponse> addOrderConsult(OrderConsultRequest request) throws Exception;

    BasePageResponse<OrderConsultDto> pageOrderConsult(OrderConsultPageRequest request);

    BaseResponse<OrderConsultResponse> updateOrderConsult(String ordid, OrderConsultRequest request) throws Exception;

    BaseResponse<String> deleteOrderConsult(String ordid);

    @Transactional(rollbackFor = Exception.class)
    BaseResponse<Void> delFile(String url) throws Exception;
}
