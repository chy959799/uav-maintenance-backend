package com.thinglinks.uav.order.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.delay.DelayQueueManager;
import com.thinglinks.uav.common.delay.DelayTask;
import com.thinglinks.uav.common.delay.TaskBase;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.entity.City;
import com.thinglinks.uav.common.entity.County;
import com.thinglinks.uav.common.entity.Province;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.repository.CityRepository;
import com.thinglinks.uav.common.repository.CountyRepository;
import com.thinglinks.uav.common.repository.ProvinceRepository;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.HttpUtils;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.order.dto.DetailExpress;
import com.thinglinks.uav.order.dto.expresses.*;
import com.thinglinks.uav.order.entity.Expresses;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.entity.OrderLog;
import com.thinglinks.uav.order.repository.ExpressesRepository;
import com.thinglinks.uav.order.repository.OrderLogRepository;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.order.service.ExpressesService;
import com.thinglinks.uav.order.service.express.EMSExpressionService;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

@Service
@Slf4j
public class ExpressesServiceImpl implements ExpressesService {

    private static final Integer SEND_TYPE = 1;
    private static final Integer DEAL_TYPE_CANCEL = 2;
    private static final Integer RECEIVE_TYPE = 2;

    private static final long DELAY_TIME = 500;
    private static final String FILE_TYPE = "pdf";

    private static final String FINISH_CODE = "05-40003";

    private static final List<String> CANCEL_CODE = Lists.newArrayList("00-40045-1000", "00-2000");

    @Resource
    private AddressRepository addressRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private ProvinceRepository provinceRepository;

    @Resource
    private ExpressesRepository expressesRepository;

    @Resource
    private CityRepository cityRepository;

    @Resource
    private CountyRepository countyRepository;

    @Resource
    private CustomConfig customConfig;

    @Resource
    private DelayQueueManager delayQueueManager;

    @Resource
    private CustomMinIOClient customMinIOClient;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderLogRepository orderLogRepository;

    @Resource
    private EMSExpressionService emsExpressionService;

    /**
     * 获取token
     *
     * @return
     */
    public String getToken() throws Exception {
        //todo 优化缓存
        String url = "https://sfapi-sbox.sf-express.com/oauth2/accessToken";
        Map<String, String> forms = new HashMap<>();
        forms.put("partnerID", customConfig.getSfPartnerID());
        forms.put("secret", customConfig.getSfSecret());
        forms.put("grantType", "password");
        String ret = HttpUtils.httpPostForm(url, forms);
        JSONObject jsonObject = JSONObject.parseObject(ret);
        String apiResultCode = jsonObject.getString("apiResultCode");
        if (CommonConstants.SUCCESS_CODE.equals(apiResultCode)) {
            return jsonObject.getString("accessToken");
        }
        log.info("getToken error:{}", ret);
        throw new CustomBizException("获取token失败");
    }

    @Override
    public Expresses createOrder(String orderId, String sendId, String receiveId) throws Exception {
        Address sendAddress = addressRepository.getByAid(sendId);
        Address receiveAddress = addressRepository.getByAid(sendId);
        return this.createOrder(orderId, sendAddress, receiveAddress);
    }

    @Override
    public Expresses createOrder(String orderId, Address sendAddress, Address receiveAddress) throws Exception {
        Map<String, String> parameters = this.getCommonParameter("EXP_RECE_CREATE_ORDER");
        JSONObject msgData = new JSONObject();
        JSONArray cargoDetails = new JSONArray();
        //类别
        JSONObject cargoDetail = new JSONObject();
        cargoDetail.put("name", "电子产品");
        cargoDetail.put("unit", "件");
        cargoDetails.add(cargoDetail);
        msgData.put("cargoDetails", cargoDetails);
        //联系人
        JSONArray contactInfoList = new JSONArray();
        JSONObject contactInfo1 = new JSONObject();
        JSONObject contactInfo2 = new JSONObject();
        contactInfo1.put("address", sendAddress.getDetail());
        contactInfo1.put("contact", sendAddress.getContactUser());
        contactInfo1.put("mobile", sendAddress.getContactTel());
        contactInfo1.put("contactType", SEND_TYPE);
        contactInfo1.put("country", "CN");
        County county1 = countyRepository.findByAreaCode(sendAddress.getCounty());
        if (Objects.isNull(county1)) {
            log.info("county code :{}", sendAddress.getCounty());
            throw new CustomBizException("区县编码错误");
        }
        contactInfo1.put("county", county1.getAreaName());
        City city1 = cityRepository.findByCityCode(sendAddress.getCity());
        if (Objects.nonNull(city1)) {
            contactInfo1.put("city", city1.getCityName());
        }
        Province province1 = provinceRepository.findByProvinceCode(sendAddress.getProvince());
        if (Objects.nonNull(province1)) {
            contactInfo1.put("province", province1.getProvinceCode());
        }
        contactInfo2.put("address", receiveAddress.getDetail());
        contactInfo2.put("contact", receiveAddress.getContactUser());
        contactInfo2.put("mobile", receiveAddress.getContactTel());
        contactInfo2.put("contactType", RECEIVE_TYPE);
        contactInfo2.put("country", "CN");
        County county2 = countyRepository.findByAreaCode(receiveAddress.getCounty());
        if (Objects.isNull(county2)) {
            log.info("county code :{}", receiveAddress.getCounty());
            throw new CustomBizException("区县编码错误");
        }
        contactInfo2.put("county", county2.getAreaName());
        City city2 = cityRepository.findByCityCode(receiveAddress.getCity());
        if (Objects.nonNull(city2)) {
            contactInfo2.put("city", city2.getCityName());
        }
        Province province2 = provinceRepository.findByProvinceCode(receiveAddress.getProvince());
        if (Objects.nonNull(province2)) {
            contactInfo2.put("province", province2.getProvinceCode());
        }
        contactInfoList.add(contactInfo1);
        contactInfoList.add(contactInfo2);
        msgData.put("contactInfoList", contactInfoList);
        msgData.put("orderId", CommonUtils.uuid());
        msgData.put("monthlyCard", customConfig.getMonthlyCard());
        msgData.put("cargoDesc", "电子产品");
        msgData.put("language", "zh-CN");
        //1：返回路由标签
        msgData.put("isReturnRoutelabel", 1);
        //1:寄方付
        msgData.put("payMethod", 1);
        //1:要快递小哥手持终端上门
        msgData.put("isDocall", 1);
        parameters.put("msgData", msgData.toJSONString());
        String ret = HttpUtils.httpPostForm(customConfig.getSfUrl(), parameters);
        log.info("createOrder ret:{}", ret);
        JSONObject jsonObject = JSONObject.parseObject(ret);
        String apiResultCode = jsonObject.getString("apiResultCode");
        if (CommonConstants.SUCCESS_CODE.equals(apiResultCode)) {
            JSONObject apiResultData = jsonObject.getJSONObject("apiResultData");
            if (apiResultData.getBoolean("success")) {
                JSONObject retMsgData = apiResultData.getJSONObject("msgData");
                JSONArray waybillNoInfoList = retMsgData.getJSONArray("waybillNoInfoList");
                JSONObject waybillNoInfo = waybillNoInfoList.getJSONObject(0);
                String waybillNo = waybillNoInfo.getString("waybillNo");
                String waybillType = waybillNoInfo.getString("waybillType");
                Expresses expresses = new Expresses();
                expresses.setExpid(CommonUtils.uuid());
                expresses.setOrdid(orderId);
                expresses.setStatus(CommonConstants.EXPRESS_STATUS_NORMAL);
                expresses.setWaybillNo(waybillNo);
                expresses.setWaybillType(waybillType);
                expresses.setWaybillList(waybillNoInfoList.toJSONString());
                expresses.setExpressesAt(System.currentTimeMillis());
                expresses = expressesRepository.save(expresses);
                TaskBase taskBase = TaskBase.builder().identifier(waybillNo).build();
                DelayTask delayTask = new DelayTask(taskBase, DELAY_TIME);
                delayQueueManager.put(delayTask);
                return expresses;
            }
            throw new CustomBizException(apiResultData.getString("errorMsg"));
        }
        throw new CustomBizException(jsonObject.getString("apiErrorMsg"));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> cancelOrder(String expId) throws Exception{
        Expresses expresses = expressesRepository.findByExpid(expId);
        if (Objects.isNull(expresses)) {
            return BaseResponse.fail("快递ID错误");
        }
        if (emsExpressionService.getExpressType().equals(expresses.getExpressType())) {
            emsExpressionService.cancelOrder(expresses);
        } else {
            this.cancelOrder(expresses);
        }
        Order order = orderRepository.getByOrdid(expresses.getOrdid());
        OrderLog orderLog = new OrderLog();
        orderLog.setDescription("快递单号【" + expresses.getWaybillNo() + "】");
        orderLog.setMethod(OrderConstants.ORDER_LOG_METHOD_EXPRESSES_CANCEL);
        orderLog.setUid(CommonUtils.getUid());
        orderLog.setOrdid(order.getOrdid());
        orderLog.setStatus(order.getStatus());
        orderLog.setOrdlogid(CommonUtils.uuid());
        orderLogRepository.save(orderLog);
        return BaseResponse.success();
    }

    @Override
    public void cancelOrder(Expresses expresses) throws Exception {
        if (Objects.isNull(expresses)) {
            return;
        }
        Map<String, String> parameters = this.getCommonParameter("EXP_RECE_UPDATE_ORDER");
        JSONObject msgData = new JSONObject();
        msgData.put("orderId", expresses.getOrdid());
        msgData.put("dealType", DEAL_TYPE_CANCEL);
        parameters.put("msgData", msgData.toJSONString());
        String ret = HttpUtils.httpPostForm(customConfig.getSfUrl(), parameters);
        log.info("cancelOrder ret:{}", ret);
        JSONObject jsonObject = JSONObject.parseObject(ret);
        String apiResultCode = jsonObject.getString("apiResultCode");
        if (CommonConstants.SUCCESS_CODE.equals(apiResultCode)) {
            JSONObject apiResultData = jsonObject.getJSONObject("apiResultData");
            if (apiResultData.getBoolean("success")) {
                expresses.setStatus(CommonConstants.EXPRESS_STATUS_CANCEL);
                expressesRepository.save(expresses);
                return;
            }
            throw new CustomBizException(apiResultData.getString("errorMsg"));
        }
        throw new CustomBizException(jsonObject.getString("apiErrorMsg"));
    }

    @Override
    public BaseResponse<String> searchRoutes(String expId) {
        Expresses expresses = expressesRepository.findByExpid(expId);
        if (Objects.isNull(expresses)) {
            return BaseResponse.fail("快递ID错误");
        }
        try {
			String senderId = expresses.getSenderId();
			Address address = addressRepository.getByAid(senderId);
			if (Objects.isNull(address)) {
				return BaseResponse.fail("未找到寄件人信息");
			}
			String ret;
			if (emsExpressionService.getExpressType().equals(expresses.getExpressType())) {
                ret = emsExpressionService.searchRoutes(expresses.getWaybillNo());
			} else {
				ret = this.searchRoutes(expresses.getWaybillNo(), address.getContactTel());
			}
			return BaseResponse.success(ret);
		} catch (Exception e) {
            log.error("cancelOrder error", e);
            return BaseResponse.fail(e.getMessage());
        }
    }

    @Override
    public String searchRoutes(String waybillNo, String senderPhone) throws Exception {
        Map<String, String> parameters = this.getCommonParameter("EXP_RECE_SEARCH_ROUTES");
        JSONObject msgData = new JSONObject();
        msgData.put("trackingNumber", waybillNo);
        //手机号后面4位
        msgData.put("checkPhoneNo", senderPhone.substring(7));
        msgData.put("trackingType", 1);
        msgData.put("methodType", 1);
        parameters.put("msgData", msgData.toJSONString());
        String ret = HttpUtils.httpPostForm(customConfig.getSfUrl(), parameters);
        log.info("searchRoutes ret:{}", ret);
        JSONObject jsonObject = JSONObject.parseObject(ret);
        String apiResultCode = jsonObject.getString("apiResultCode");
        if (CommonConstants.SUCCESS_CODE.equals(apiResultCode)) {
            JSONObject apiResultData = jsonObject.getJSONObject("apiResultData");
            if (apiResultData.getBoolean("success")) {
                return apiResultData.getString("msgData");
            }
            throw new CustomBizException(apiResultData.getString("errorMsg"));
        }
        throw new CustomBizException(jsonObject.getString("apiErrorMsg"));

    }

    @Override
    public PushResponse waybillPush(PrintWaybills request) {
        try {
            String msgData = request.getMsgData();
            JSONObject msgDataObject = JSONObject.parseObject(msgData);
            String waybillNo = msgDataObject.getString("waybillNo");
            Expresses expresses = expressesRepository.findFirstByWaybillNoOrderByCtDesc(waybillNo);
            String fileName = msgDataObject.getString("fileName");
            String content = msgDataObject.getString("content");
            byte[] decode = Base64.getDecoder().decode(content);
            InputStream is = new ByteArrayInputStream(decode);
            CustomFile customFile = new CustomFile();
            customFile.setFid(CommonUtils.uuid());
            customFile.setExpid(expresses.getExpid());
            customFile.setSize((long) decode.length);
            customFile.setName(fileName);
            customFile.setExtension(CommonUtils.getExtension(customFile.getName()));
            customFile.setDownload(Boolean.TRUE);
            customFile.setDirectory(OrderConstants.ORDER_BUCKET);
            customFile.setMime(FILE_TYPE);
            String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
            customMinIOClient.uploadFile(OrderConstants.ORDER_BUCKET, objectName, is);
            fileRepository.save(customFile);
            return PushResponse.success();
        } catch (Exception e) {
            log.error("waybillPush error", e);
        }
        return PushResponse.fail();
    }

    @Override
    public PushResponse statePush(StatePush request) {
        try {
            List<OrderState> orderState = request.getOrderState();
            if (CollectionUtils.isNotEmpty(orderState)) {
                orderState.forEach(x -> {
                    Expresses expresses = expressesRepository.findFirstByWaybillNoOrderByCtDesc(x.getWaybillNo());
                    if (Objects.nonNull(expresses)) {
                        if (FINISH_CODE.equals(x.getOrderStateCode())) {
                            expresses.setStatus(CommonConstants.EXPRESS_STATUS_FINISH);
                        }
                        if (CANCEL_CODE.contains(x.getOrderStateCode())) {
                            expresses.setStatus(CommonConstants.EXPRESS_STATUS_CANCEL);
                        }
                        expresses.setState(JSONObject.toJSONString(x));
                        expressesRepository.save(expresses);
                    }
                });
            }
            return PushResponse.success();
        } catch (Exception e) {
            log.error("statePush error", e);
        }
        return PushResponse.fail();
    }

    @Override
    public FeesPushResponse feesPush(FeesPush request) {
        try {
            String content = request.getContent();
            FeeContent feeContent = JSONObject.parseObject(content, FeeContent.class);
            String waybillNo = feeContent.getWaybillNo();
            Expresses expresses = expressesRepository.findFirstByWaybillNoOrderByCtDesc(waybillNo);
            if (Objects.nonNull(expresses)) {
                expresses.setFeesList(JSONObject.toJSONString(feeContent.getFeeList()));
                JSONObject pkgInfo = new JSONObject();
                pkgInfo.put("orderNo", feeContent.getOrderNo());
                pkgInfo.put("customerAcctCode", feeContent.getCustomerAcctCode());
                pkgInfo.put("meterageWeightQty", feeContent.getMeterageWeightQty());
                pkgInfo.put("productName", feeContent.getProductName());
                pkgInfo.put("quantity", feeContent.getQuantity());
                pkgInfo.put("volume", feeContent.getVolume());
                expresses.setPkgInfo(pkgInfo.toJSONString());
                List<Fee> feeList = feeContent.getFeeList();
                if (CollectionUtils.isNotEmpty(feeList)) {
                    Double price = feeList.stream().mapToDouble(x -> Double.parseDouble(x.getFeeAmt())).sum();
                    expresses.setPrice(price);
                }
                expressesRepository.save(expresses);
            }
            FeesPushResponse.success(customConfig.getSfPartnerID());
        } catch (Exception e) {
            log.error("statePush error", e);
        }
        return FeesPushResponse.success(customConfig.getSfPartnerID());
    }

    @Override
    public BaseResponse<DetailExpress> queryDetailExpress(String expid) {
        Expresses expresses = expressesRepository.findByExpid(expid);
        if (Objects.isNull(expresses)) {
            return BaseResponse.fail("");
        }
        DetailExpress detailExpress = new DetailExpress();
        BeanUtils.copyProperties(expresses, detailExpress);
        detailExpress.setExpressesTime(DateUtils.formatDateTime(expresses.getExpressesAt()));
        if (Objects.nonNull(expresses.getUid())) {
            detailExpress.setUser(userRepository.findByUid(expresses.getUid()));
        }
        if (Objects.nonNull(expresses.getSenderId())) {
            detailExpress.setSender(addressRepository.getByAid(expresses.getSenderId()));
        }
        if (Objects.nonNull(expresses.getReceiverId())) {
            detailExpress.setReceiver(addressRepository.getByAid(expresses.getReceiverId()));
        }
        CustomFile customFile = fileRepository.findByExpid(expresses.getExpid());
        detailExpress.setCloudPrintUrl(this.getFileProxyUrl(customFile));
        return BaseResponse.success(detailExpress);
    }

    /**
     * 获取公共参数
     *
     * @return
     */
    private Map<String, String> getCommonParameter(String serviceCode) throws Exception {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("partnerID", customConfig.getSfPartnerID());
        parameters.put("requestID", CommonUtils.uuid());
        parameters.put("timestamp", String.valueOf(System.currentTimeMillis()));
        parameters.put("accessToken", this.getToken());
        parameters.put("serviceCode", serviceCode);
        return parameters;
    }

    /**
     * 获取文件的访问地址
     *
     * @param customFile
     * @return
     */
    private String getFileProxyUrl(CustomFile customFile) {
        try {
            if (Objects.isNull(customFile)) {
                return StringUtils.EMPTY;
            }
            String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
            return customMinIOClient.getProxyUrl(OrderConstants.ORDER_BUCKET, objectName);
        } catch (Exception e) {
            log.error("getFileProxyUrl error", e);
        }
        return StringUtils.EMPTY;
    }
}
