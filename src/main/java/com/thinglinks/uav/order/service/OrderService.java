package com.thinglinks.uav.order.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.Remark;
import com.thinglinks.uav.common.dto.ReviewResult;
import com.thinglinks.uav.order.dto.*;
import com.thinglinks.uav.order.dto.excel.ExportField;
import com.thinglinks.uav.order.dto.quote.DetailOrderQuote;
import com.thinglinks.uav.order.dto.quote.OrderQuoteRequest;
import com.thinglinks.uav.order.dto.quote.QuoteSpare;
import com.thinglinks.uav.stock.dto.DetailStockInOut;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface OrderService {

    /**
     * 工单分页查询
     *
     * @param request
     * @return
     */
    BasePageResponse<PageOrder> pageOrder(PageOrderQuery request);


    /**
     * 工单导出
     *
     * @param response
     * @param query
     * @param exportField
     */
    void exportOrder(HttpServletResponse response, PageOrderQuery query, ExportField exportField) throws Exception;

    /**
     * 工单分页查询
     *
     * @param request
     * @return
     */
    BaseResponse<Void> addOrderByReceive(OrderByReceive request) throws Exception;


    /**
     * 新增报废工单
     *
     * @param request
     * @return
     */
    BaseResponse<Void> addScrapOrder(ScrapOrderByReceive request) throws Exception;

    /**
     * @param request
     * @return
     */
    BaseResponse<Void> addOrderByUser(OrderByUser request) throws Exception;

    /**
     * 出版资料文件预览
     *
     * @param request
     * @return
     */
    BaseResponse<List<FilePreview>> materialPreview(InsurancePreview request) throws Exception;

    /**
     * 确认收件
     *
     * @param request
     * @return
     */
    BaseResponse<Void> receiveExpress(String orderId, ReceiveExpress request) throws Exception;

    /**
     * 确认收件
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> confirmDevice(String orderId) throws Exception;

    /**
     * 出版资料审核
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> reviewFirstInsurance(String orderId, ReviewResult request) throws Exception;

    /**
     * 出版资料审核
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> reviewLastInsurance(String orderId, ReviewResult request) throws Exception;


    /**
     * 出版资料审核
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> finishInsurance(String orderId);

    /**
     * 保险确认
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> reviewInsurance(String orderId, AuditInsurance request) throws Exception;

    /**
     * 删除保单
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> deleteOrder(String orderId) throws Exception;

    /**
     * 删除保单
     *
     * @param orderIds
     * @return
     */
    BaseResponse<Void> deleteOrder(List<String> orderIds) throws Exception;

    /**
     * 获取工单详情
     *
     * @param orderId
     * @return
     */
    BaseResponse<DetailOrder> detailOrder(String orderId) throws Exception;

    /**
     * 取消工单
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> cancelOrderIn(String orderId, Remark request);

    /**
     * 用户取消工单
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> cancelOrderOut(String orderId, Remark request);


    /**
     * 取消工单
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> reviewCancel(String orderId, ReviewResult request);

    /**
     * 客户撤销取消
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> revocationOrder(String orderId);

    /**
     * 取消工单
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> reviewRevocation(String orderId, ReviewResult request);

    /**
     * 定损
     *
     * @param request
     * @return
     */
    BaseResponse<Void> confirmLoss(String orderId, ConfirmLoss request) throws Exception;

    /**
     * 定损
     *
     * @return
     */
    BaseResponse<Void> reConfirmLoss(String orderId) throws Exception;

    /**
     * 定损
     *
     * @param request
     * @return
     */
    BaseResponse<Void> reviewLoss(String orderId, ReviewResult request);

    /**
     * 返厂寄件
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> returnExpress(String orderId, ReturnExpresses request) throws Exception;

    /**
     * 提交报价
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> quoteOrder(String orderId, OrderQuoteRequest request) throws Exception;


    /**
     * 提交报价
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> submitQuote(String orderId, Remark request);

    /**
     * 报价审核
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> reviewQuote(String orderId, ReviewQuoteResult request) throws Exception;


    /**
     * 客户确认报价
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> confirmQuote(String orderId);

    /**
     * 结算编辑
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> customerSettle(String orderId, CustomerSettle request);

    /**
     * 结算编辑
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> advanceSettle(String orderId, AdvanceSettle request);

    /**
     * 结算编辑
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> submitSettle(String orderId) throws Exception;

    /**
     * 返厂收件
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> returnReceive(String orderId, ReturnReceive request);

    /**
     * 派单
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> dispatchOrder(String orderId, DispatchOrder request);

    /**
     * 维修反馈
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> maintainFeedback(String orderId, MaintainFeedback request) throws Exception;

    /**
     * 维修反馈
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> finishMaintain(String orderId, Remark request) throws Exception;

    /**
     * 维修任务回退
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> failMaintain(String orderId, Remark request) throws Exception;

    /**
     * 保险回退
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> backInsurance(String orderId, Remark request) throws Exception;


    /**
     * 维修任务回退
     *
     * @param orderId
     * @return
     */
    BaseResponse<Void> orderChecked(String orderId, OrderCheck request) throws Exception;


    /**
     * 设备自寄
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> customerExpresses(String orderId, CustomerExpresses request) throws Exception;


    /**
     * 查询设备报价
     *
     * @param orderId
     * @return
     */
    BaseResponse<DetailOrderQuote> queryOrderQuote(String orderId);

    /**
     * 查询工单的出库记录
     *
     * @param orderId
     * @return
     */
    BaseResponse<List<DetailStockInOut>> queryOrderStockOut(String orderId);

    /**
     * 查询工单的快递列表
     *
     * @param orderId
     * @return
     */
    BaseResponse<List<DetailExpress>> queryOrderExpresses(String orderId);

    /**
     * 查询工单操作时间
     *
     * @param orderId
     * @return
     */
    BaseResponse<List<OrderLogDetail>> queryOrderLog(String orderId);

    /**
     * 初版资料提交
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> firstInsurance(String orderId, FirstInsurance request) throws Exception;

    /**
     * 终版资料提交
     *
     * @param orderId
     * @param fids
     * @return
     */
    BaseResponse<Void> lastInsurance(String orderId, List<String> fids) throws Exception;

    /**
     * 查询保险材料
     *
     * @param orderId
     * @return
     */
    BaseResponse<InsuranceMaterialDetail> queryInsuranceMaterial(String orderId);


    /**
     * 录入理赔报案信息
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> reportInvoice(String orderId, ReportInvoice request);


    /**
     * 结算详情
     *
     * @param orderId
     * @return
     */
    BaseResponse<DetailSettle> detailSettle(String orderId);

    /**
     * 备件价格
     *
     * @param orderId
     * @return
     */
    BaseResponse<List<OrderSparePrice>> orderSparePrice(String orderId, String catid);


    /**
     * 报价单下载
     *
     * @param response
     * @param orderId
     * @throws Exception
     */
    void downloadCostQuote(HttpServletResponse response, String orderId) throws Exception;

    /**
     * 报价单下载
     *
     * @param orderId
     * @throws Exception
     */
    BaseResponse<String> downloadCostQuote(String orderId) throws Exception;

    /**
     * 报价单下载
     *
     * @param response
     * @param orderId
     * @throws Exception
     */
    void downloadCustomerQuote(HttpServletResponse response, String orderId) throws Exception;

    /**
     * 报价单下载
     *
     * @param orderId
     * @throws Exception
     */
    BaseResponse<String> downloadCustomerQuote(String orderId) throws Exception;

    /**
     * 工单备件明细
     *
     * @param orderId
     * @return
     */
    BaseResponse<List<QuoteSpare>> orderQuoteSpare(String orderId);

    /**
     * 工单文件列表
     *
     * @param orderId
     * @return
     */
    BaseResponse<List<OrderFile>> orderFile(String orderId);


    /**
     * 报废工单审核
     *
     * @param request
     * @return
     */
    BaseResponse<Void> reviewScrap(String orderId, ScrapReview request);

    /**
     * 报废工单审核不通过处理
     *
     * @param orderId
     * @param request
     * @return
     */
    BaseResponse<Void> handleScrap(String orderId, ScrapHandle request) throws Exception;

    /**
     * 保险资料归档
     *
     * @param orderId
     * @param fids
     * @return
     */
    BaseResponse<Void> insuranceArchived(String orderId, List<String> fids);

    /**
     * 工单导入
     *
     * @param file
     * @return
     */
    void orderImport(MultipartFile file, Integer type, HttpServletResponse response) throws Exception;
}
