package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderQuoteItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OrderQuoteItemRepository  extends JpaRepository<OrderQuoteItem, Integer>, JpaSpecificationExecutor<OrderQuoteItem> {

    List<OrderQuoteItem> findByOqid(String oqid);

    void deleteByOqid(String oqid);

    OrderQuoteItem findByOqidAndPid(String oqid, String pid);
    List<OrderQuoteItem> findByPid(String pid);
    List<OrderQuoteItem> findAllByPidAndCatid(String pid, String catid);
}
