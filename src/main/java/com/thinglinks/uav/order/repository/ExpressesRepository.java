package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.Expresses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface ExpressesRepository extends JpaRepository<Expresses, Integer>, JpaSpecificationExecutor<Expresses> {

    Expresses findFirstByWaybillNoOrderByCtDesc(String number);

    Expresses findByExpid(String expid);

    Expresses findFirstByOrdidAndType(String orderId, String type);


    Expresses findFirstByOrdidOrderByCt(String orderId);

    List<Expresses> findByOrdid(String orderId);

    void deleteByOrdid(String orderId);

    List<Expresses> findByTypeAndExpressesAtBetween(String type, Long start, Long end);

}
