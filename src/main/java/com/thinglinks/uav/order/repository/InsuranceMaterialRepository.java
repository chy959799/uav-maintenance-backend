package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.InsuranceMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface InsuranceMaterialRepository extends JpaRepository<InsuranceMaterial, Integer>, JpaSpecificationExecutor<InsuranceMaterial> {

    InsuranceMaterial findByOrdid(String orderId);

    void deleteByOrdid(String orderId);
}
