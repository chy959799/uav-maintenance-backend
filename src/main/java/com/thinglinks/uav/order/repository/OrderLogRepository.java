package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @package: com.thinglinks.uav.project.repository
 * @className: OrderRepository
 * @author: rp
 * @description: TODO
 */
@Repository
public interface OrderLogRepository extends JpaRepository<OrderLog, Integer>, JpaSpecificationExecutor<OrderLog> {


    OrderLog findFirstByOrdidAndMethodOrderByCtDesc(String orderId, String method);


    List<OrderLog> findByOrdidOrderByCtDesc(String orderId);


    OrderLog findFirstByOrdidAndMethodOrderByCtAsc(String orderId, String method);

    void deleteByOrdid(String orderId);


    List<OrderLog> findByOrdidAndMethod(String orderId, String method);
}
