package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderConsult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface OrderConsultRepository extends JpaRepository<OrderConsult, Integer>, JpaSpecificationExecutor<OrderConsult> {
    @Query("select  count(distinct c.cid) from OrderConsult c")
    long countDistinctByCid();

    OrderConsult findByOrdid(String ordid);

    Integer deleteByOrdid(String ordid);

    boolean existsByOrdid(String ordid);
}
