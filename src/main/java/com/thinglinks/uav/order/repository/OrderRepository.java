package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @package: com.thinglinks.uav.project.repository
 * @className: OrderRepository
 * @author: rp
 * @description: TODO
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>, JpaSpecificationExecutor<Order> {

    boolean existsByDevid(String devid);

    boolean existsByAid(String aid);

    Order getByOrdid(String orderId);

    Order findByOrdid(String orderId);

    Order findByInnerCode(String code);

    boolean existsByInnerCode(String code);

    void deleteByOrdid(String orderId);

    @Query("select count(distinct o.devid) from Order o")
    long countDistinctByDevid();

    @Query("select count(distinct o.cid) from Order o")
    long countDistinctByCid();

    @Query("select avg(o.totalTime) from Order o")
    @Transactional(readOnly = true)
    Optional<Long> avgTime();

    @Query("select count(o) from Order o where o.ct >= :startTime and o.ct <= :endTime and o.serviceType = :serviceType")
    long countByTimesTampRange(@Param("serviceType")String serviceType, @Param("startTime")long startTime, @Param("endTime")long endTime);

    Integer countByStatus(String status);

    long countByServiceType(String serviceType);

    Integer countByStatusAndReviewInsuranceStatus(String status, String reviewInsuranceStatus);

    Integer countByReviewInsuranceStatus(String reviewInsuranceStatus);

    Integer countByServiceMethodAndStatusAndMaintainerUid(String serviceMethod, String status, String maintainerUid);
    Integer countByServiceMethodAndStatus(String serviceMethod, String status);

    boolean existsByCid(String cid);

    boolean existsByCidIn(List<String> ids);

    List<Order> findByStatus(String status);

    List<Order> findByReviewInsuranceStatus(String reviewInsuranceStatus);

    List<Order> findAllByPjid(String pjid);

    @Query("FROM Order o WHERE o.status > :status")
    List<Order> findAllAfterStatus(@Param("status") String status);

    List<Order> findByOrdidIn(List<String> orderIds);

    List<Order> findByCtBetween(long startTime, long endTime);

    List<Order> findAllByOrdidIn(List<String> ordid);
}
