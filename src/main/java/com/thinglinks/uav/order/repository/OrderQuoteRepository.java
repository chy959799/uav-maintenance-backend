package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderQuote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderQuoteRepository extends JpaRepository<OrderQuote, Integer>, JpaSpecificationExecutor<OrderQuote> {

    OrderQuote findByOqid(String oqid);
    /**
     * 客户报价单
     *
     * @return
     */
    OrderQuote findFirstByOrdidAndType(String orderId, String type);


    List<OrderQuote> findByOrdidAndType(String orderId, String type);

    List<OrderQuote> findByOrdid(String orderId);


    OrderQuote findFirstByOrdidAndTypeOrderByOrderDesc(String orderId, String type);

    List<OrderQuote> findByOrdidAndTypeOrderByOrder(String orderId, String type);


    Boolean existsByOrdid(String orderId);


    long countByOrdidAndType(String orderId, String type);

    void  deleteByOrdidAndType(String orderId, String type);

    @Query("SELECT SUM(o.sparePrice + o.expressesPrice + o.otherPrice) FROM OrderQuote o WHERE o.ordid =?1")
    Double sumPricesByOrdid(String ordid);

}
