package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.Approve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface ApproveRepository extends JpaRepository<Approve, Integer>, JpaSpecificationExecutor<Approve> {
}
