package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderSettle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface OrderSettleRepository extends JpaRepository<OrderSettle, Integer>, JpaSpecificationExecutor<OrderSettle> {

    OrderSettle findFirstByOrdidAndType(String orderId, String type);

    List<OrderSettle> findByOrdidAndTypeOrderByUtAsc(String orderId, String type);


    void deleteByOrdid(String orderId);

}
