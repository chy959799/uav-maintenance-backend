package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderLost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @package: com.thinglinks.uav.project.repository
 * @className: OrderRepository
 * @author: rp
 * @description: TODO
 */
@Repository
public interface OrderLostRepository extends JpaRepository<OrderLost, Integer>, JpaSpecificationExecutor<OrderLost> {

}
