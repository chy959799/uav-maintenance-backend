package com.thinglinks.uav.order.repository;

import com.thinglinks.uav.order.entity.OrderCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2024/3/24
 */
public interface OrderCategoryRepository extends JpaRepository<OrderCategory, Integer>, JpaSpecificationExecutor<OrderCategory> {


    void deleteByOrdid(String orderId);

    List<OrderCategory> findByOrdid(String orderId);

    List<OrderCategory> findAllByCtBetween(Long startTime, Long endTime);
}
