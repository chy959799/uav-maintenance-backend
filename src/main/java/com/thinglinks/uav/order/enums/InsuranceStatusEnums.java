package com.thinglinks.uav.order.enums;

import lombok.Getter;

import java.util.HashMap;

public enum InsuranceStatusEnums {

    UNKNOWN("-1", "未知"),
    INSURANCE_FIRST_WAITING_SUBMIT("10", "初版资料待提交"),
    INSURANCE_FIRST_REVIEW("11", "初版资料待审核"),
    INSURANCE_FIRST_FINISH("12", "初版资料已完成"),
    INSURANCE_FIRST_REJECT("13", "初版资料已驳回"),
    INSURANCE_LAST_WAITING_SUBMIT("20", "终版资料待提交"),
    INSURANCE_LAST_WAITING_REVIEW("21", "终版资料待审核"),
    INSURANCE_LAST_REJECT("22", "终版资料已驳回"),
    INSURANCE_WAITING_FINISH("23", "待完成"),
    INSURANCE_FINISH("30", "已完成");

    private final static HashMap<String, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (InsuranceStatusEnums type : InsuranceStatusEnums.values()) {
            CODE_TO_VALUE.put(type.code, type.status);
        }
    }

    public static String fromCode(String code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.status;
        }
        return value;
    }


    /**
     * 枚举编码
     */
    @Getter
    private final String code;

    /**
     * 枚举值
     */
    private final String status;

    InsuranceStatusEnums(String code, String message) {
        this.code = code;
        this.status = message;
    }

}
