package com.thinglinks.uav.order.enums;

import lombok.Getter;

import java.util.HashMap;

/**
 * @author: fanhaiqiu
 * @date: 2024/4/18
 */
public enum OrderStatusEnums {

    UNKNOWN("-1", "未知"),
    ORDER_STATUS_CANCEL("0000", "已取消"),
    ORDER_STATUS_INVALIDATE("0001", "已失效"),
    ORDER_STATUS_CANCEL_WAITING1("0002", "取消待审核"),
    ORDER_STATUS_CANCEL_WAITING2("0003", "撤销取消待审核"),
    ORDER_STATUS_WAIT_EXPRESSES("1000", "待收件"),
    ORDER_STATUS_WAIT_CONFIRM_DEVICE("1001", "设备待确认"),
    ORDER_STATUS_WAIT_CONFIRMED_DEVICE("1002", "设备已确认"),
    ORDER_STATUS_WAIT_CONFIRM_INSURANCE("1003", "设备保险待确认"),
    ORDER_STATUS_WAIT_SCRAP_REVIEW("1004", "报废待审核"),
    ORDER_STATUS_WAIT_SCRAP_HANDLE("1005", "报废待处理"),
    ORDER_STATUS_WAIT_CONFIRM_LOSS("2000", "待定损"),
    ORDER_STATUS_WAIT_LOSS_REVIEW("2001", "定损待审核"),
    ORDER_STATUS_RETURN_WAITING_EXPRESSES("2002", "返厂待寄件"),
    ORDER_STATUS_WAITING_QUOTATION("3000", "待报价"),
    ORDER_STATUS_WAITING_QUOTATION_REVIEW("3001", "报价待审核"),
    ORDER_STATUS_WAITING_QUOTATION_CONFIRM("3002", "报价待确认"),
    ORDER_STATUS_INSURANCE_MATERIAL_GATHER("3003", "保险资料收集"),
    ORDER_STATUS_WAITING_SETTLE("4000", "待结算"),
    ORDER_STATUS_RETURN_WAITING_RECEIVE("4001", "返厂待收件"),
    ORDER_STATUS_WAITING_DISPATCH("4002", "待派单"),
    ORDER_STATUS_MAINTAINING("4003", "维修中"),
    ORDER_STATUS_WAITING_CHECK("5000", "待检测"),
    ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES("5001", "待寄件"),
    ORDER_STATUS_FINISH_CUSTOMER_EXPRESSES("5002", "已寄件"),
    ORDER_STATUS_FINISH("6000", "已完成");

    private final static HashMap<String, String> CODE_TO_VALUE = new HashMap<>();

    static {
        for (OrderStatusEnums type : OrderStatusEnums.values()) {
            CODE_TO_VALUE.put(type.code, type.message);
        }
    }

    public static String fromCode(String code) {
        String value = CODE_TO_VALUE.get(code);
        if (value == null) {
            return UNKNOWN.message;
        }
        return value;
    }


    /**
     * 枚举编码
     */
    @Getter
    private final String code;

    /**
     * 枚举值
     */
    private final String message;

    OrderStatusEnums(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
