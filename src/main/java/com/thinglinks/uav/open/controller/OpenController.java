package com.thinglinks.uav.open.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "系统对接公开接口")
@RequestMapping(value = "system/open")
@Slf4j
public class OpenController {


}
