package com.thinglinks.uav.departments.repository;

import com.thinglinks.uav.departments.entity.Department;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (Departments)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-18 20:23:45
 */
public interface DepartmentsRepository extends JpaRepository<Department, Integer>, JpaSpecificationExecutor<Department> {


	boolean existsByName(String name);

	void deleteByDid(String did);

	void deleteAllByDidIn(List<String> ids);

	Department getByDid(String did);

	List<Department> findAllByParentIsNull();

	Integer countByPdid(String did);

	Integer countByPdidIn(List<String> ids);

	Department findByName(String organizationName);

	Department findByDid(String did);
}