package com.thinglinks.uav.departments.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * (Departments)实体类
 *
 * @author iwiFool
 * @since 2024-03-18 20:23:45
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "departments")
@Data
public class Department extends BaseEntity {

	/**
	 * 部门ID唯一
	 */
	private String did;
	/**
	 * 部门名称
	 */
	private String name;
	/**
	 * 部门描述
	 */
	private String description;
	/**
	 * 部门类型 1:普通 2:系统默认
	 */
	private String type;

	private String cpid;

	/**
	 * 父级部门ID
	 */
	private String pdid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pdid", referencedColumnName = "did", insertable = false, updatable = false)
	private Department parent;
}
