package com.thinglinks.uav.departments.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.departments.dto.DepartmentDTO;
import com.thinglinks.uav.departments.dto.DepartmentPageQuery;
import com.thinglinks.uav.departments.service.DepartmentService;
import com.thinglinks.uav.departments.vo.DepartmentTreeNode;
import com.thinglinks.uav.departments.vo.DepartmentVO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * (Departments)表控制层
 *
 * @author iwiFool
 * @since 2024-03-18 20:23:45
 */
@Api(tags = "组织机构")
@RestController
@RequestMapping("departments")
public class DepartmentController {

	@Resource
	private DepartmentService departmentService;

	@Authority(action = "departmentManagementAdd")
	@PostMapping
	@Operation(summary = "新增组织机构")
	public BaseResponse<String> insert(@RequestBody @Valid DepartmentDTO dto) {
		return departmentService.insert(dto);
	}

	@Authority(action = "departmentManagementDelete")
	@DeleteMapping("{did}")
	@Operation(summary = "删除指定组织机构")
	public BaseResponse<String> delete(@PathVariable("did") String did) {
		return departmentService.deleteByDid(did);
	}

	@Authority(action = "departmentManagementDelete")
	@DeleteMapping
	@Operation(summary = "批量删除组织机构")
	public BaseResponse<String> deleteAll(@RequestBody DeleteDTO dto) {
		return departmentService.deleteAllByDids(dto.getIds());
	}

	@Authority(action = "departmentManagementEdit")
	@PatchMapping("/{did}")
	@Operation(summary = "修改组织机构")
	public BaseResponse<String> update(@PathVariable("did") String did, @RequestBody @Valid DepartmentDTO dto) {
		return departmentService.update(did, dto);
	}

	@GetMapping
	@Operation(summary = "获取组织机构列表")
	public BasePageResponse<DepartmentVO> getPage(DepartmentPageQuery pageQuery) {
		return departmentService.getPage(pageQuery);
	}

	@GetMapping("/{did}")
	@Operation(summary = "获取组织机构详情")
	public DepartmentVO getDepartmentInfo(@PathVariable String did) {
		return departmentService.getByDid(did);
	}

	@GetMapping(path = "tree")
	@Operation(summary = "组织机构树形菜单")
	public BaseResponse<List<DepartmentTreeNode>> getTree() {
		return departmentService.getTree();
	}

}


