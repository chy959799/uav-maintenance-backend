package com.thinglinks.uav.departments.service.impl;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.departments.dto.DepartmentDTO;
import com.thinglinks.uav.departments.dto.DepartmentPageQuery;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.departments.repository.DepartmentsRepository;
import com.thinglinks.uav.departments.service.DepartmentService;
import com.thinglinks.uav.departments.vo.DepartmentTreeNode;
import com.thinglinks.uav.departments.vo.DepartmentVO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * (Departments)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-18 20:23:45
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {
	@Resource
	private DepartmentsRepository departmentsRepository;

	@Override
	public BaseResponse<String> insert(DepartmentDTO dto) {
		if (departmentsRepository.existsByName(dto.getName())) {
			return BaseResponse.fail("该组织机构名称已存在");
		}

		Department department = new Department();
		BeanUtils.copyProperties(dto, department);
		department.setDid(CommonUtils.uuid());
		department.setType("1");
		department.setCpid(CommonUtils.getCpid());
		departmentsRepository.save(department);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> deleteByDid(String did) {
		Integer count = departmentsRepository.countByPdid(did);
		if (count > 0) {
			return BaseResponse.fail("该组织机构下存在子节点，不能删除");
		}
		departmentsRepository.deleteByDid(did);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> deleteAllByDids(List<String> ids) {
		Integer count = departmentsRepository.countByPdidIn(ids);
		if (count > 0) {
			return BaseResponse.fail("该组织机构下存在子节点，不能删除");
		}
		departmentsRepository.deleteAllByDidIn(ids);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> update(String did, DepartmentDTO dto) {
		Department temp = departmentsRepository.findByName(dto.getName());
		if (Objects.nonNull(temp) && !Objects.equals(temp.getDid(), did)) {
			return BaseResponse.fail("该组织机构名称已存在");
		}

		Department department = departmentsRepository.getByDid(did);
		if (!Objects.isNull(department)) {
			BeanUtils.copyProperties(dto, department);
			departmentsRepository.save(department);
			return BaseResponse.success();
		}
		return BaseResponse.fail("组织机构不存在");
	}

	@Override
	public BasePageResponse<DepartmentVO> getPage(DepartmentPageQuery pageQuery) {
		Specification<Department> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();

			if (!Objects.isNull(pageQuery.getName()) && !pageQuery.getName().isEmpty()) {
				predicate = builder.and(predicate, builder.like(root.get("name"), "%" + pageQuery.getName() + "%"));
			}

			if (!Objects.isNull(pageQuery.getDid()) && !pageQuery.getDid().isEmpty()) {
				Predicate didPredicate = builder.equal(root.get("did"), pageQuery.getDid());
				Predicate pdidPredicate = builder.equal(root.get("pdid"), pageQuery.getDid());

				Join<Department, Department> selfJoin = root.join("parent", JoinType.LEFT);
				Predicate recursivePredicate = builder.or(
						builder.equal(selfJoin.get("did"), pageQuery.getDid()),
						builder.equal(selfJoin.get("pdid"), pageQuery.getDid())
				);

				predicate = builder.and(predicate, builder.or(didPredicate, pdidPredicate, recursivePredicate));
			}

			return predicate;
		};

		Page<Department> departmentPage = departmentsRepository.findAll(specification, pageQuery.of());
		List<DepartmentVO> departmentVOList = departmentPage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(departmentPage, departmentVOList);
	}

	@Override
	public DepartmentVO getByDid(String did) {
		Department department = departmentsRepository.getByDid(did);
		return convertToVO(department);
	}

	@Override
	public BaseResponse<List<DepartmentTreeNode>> getTree() {
		List<Department> departments = departmentsRepository.findAll();
		List<DepartmentTreeNode> tree = buildDepartmentTree(departments);
		return BaseResponse.success(tree);
	}

	private DepartmentVO convertToVO(Department department) {
		DepartmentVO departmentVO = new DepartmentVO();
		BeanUtils.copyProperties(department, departmentVO);
		departmentVO.setCreatedAt(DateUtils.formatDateTime(department.getCt()));
		departmentVO.setUpdatedAt(DateUtils.formatDateTime(department.getUt()));
		if (department.getPdid() != null) {
			Department pDepartment = departmentsRepository.getByDid(department.getPdid());
			departmentVO.setParent(convertToVO(pDepartment));
		}
		return departmentVO;
	}

	private List<DepartmentTreeNode> buildDepartmentTree(List<Department> departments) {
		List<DepartmentTreeNode> tree = new ArrayList<>();

		// 以部门的Did为Key，以部门节点对象为Value，方便通过ID查找节点
		Map<String, DepartmentTreeNode> nodeMap = new HashMap<>();
		for (Department department : departments) {
			DepartmentTreeNode node = new DepartmentTreeNode(department);
			nodeMap.put(department.getDid(), node);
		}

		// 构建树形结构
		for (Department department : departments) {
			DepartmentTreeNode node = nodeMap.get(department.getDid());
			DepartmentTreeNode parent = nodeMap.get(department.getPdid());
			if (parent != null) {
				parent.getChildren().add(node);
				parent.setCount(parent.getCount() + 1);
			} else {
				tree.add(node);
			}
		}

		return tree;
	}

}


