package com.thinglinks.uav.departments.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.departments.dto.DepartmentDTO;
import com.thinglinks.uav.departments.dto.DepartmentPageQuery;
import com.thinglinks.uav.departments.vo.DepartmentTreeNode;
import com.thinglinks.uav.departments.vo.DepartmentVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (Departments)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-18 20:23:45
 */
public interface DepartmentService {

	BaseResponse<String> insert(DepartmentDTO dto);

	@Transactional
	BaseResponse<String> deleteByDid(String did);

	@Transactional
	BaseResponse<String> deleteAllByDids(List<String> ids);

	BaseResponse<String> update(String did, DepartmentDTO dto);

	BasePageResponse<DepartmentVO> getPage(DepartmentPageQuery pageQuery);

	DepartmentVO getByDid(String did);

	BaseResponse<List<DepartmentTreeNode>> getTree();
}


