package com.thinglinks.uav.departments.vo;

import com.thinglinks.uav.departments.entity.Department;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/23
 */
@Data
@ApiModel(description = "组织机构树形节点")
public class DepartmentTreeNode {

	@ApiModelProperty(value = "部门ID唯一", example = "123456789")
	private String did;
	@ApiModelProperty(value = "部门名称", example = "测试")
	private String name;
	@ApiModelProperty(value = "子节点")
	private List<DepartmentTreeNode> children;
	@ApiModelProperty(value = "子节点数量")
	private Integer count;
	@ApiModelProperty(value = "父节点ID", example = "123456789")
	private String pdId;

	public DepartmentTreeNode(Department department) {
		this.did = department.getDid();
		this.name = department.getName();
		this.pdId = department.getPdid();
		this.count = 0;
		this.children = new ArrayList<>();
	}
}
