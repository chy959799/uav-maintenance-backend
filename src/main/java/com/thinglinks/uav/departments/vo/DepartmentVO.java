package com.thinglinks.uav.departments.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/18
 */
@Data
@ApiModel(description = "组织机构 VO")
public class DepartmentVO {

	@ApiModelProperty(value = "部门ID唯一", example = "123456789")
	private String did;
	@ApiModelProperty(value = "部门名称", example = "测试")
	private String name;
	@ApiModelProperty(value = "部门描述", example = "测试部门")
	private String description;
	@ApiModelProperty(value = "部门类型 1:普通 2:系统默认", example = "1")
	private String type;
	@ApiModelProperty(value = "创建时间", example = "2021-01-01 10:00:00")
	private String createdAt;
	@ApiModelProperty(value = "更新时间", example = "2021-02-01 15:30:00")
	private String updatedAt;
	@ApiModelProperty(value = "公司ID", example = "123456789")
	private String cpid;
	@ApiModelProperty(value = "父级部门ID", example = "123456789")
	private String pdid;
	private DepartmentVO parent;
}
