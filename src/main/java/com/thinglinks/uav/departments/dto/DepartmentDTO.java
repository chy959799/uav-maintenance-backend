package com.thinglinks.uav.departments.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author iwiFool
 * @date 2024/3/18
 */
@Data
@ApiModel(value = "组织机构DTO")
public class DepartmentDTO {

	@ApiModelProperty(value = "组织机构名称", example = "测试组织机构名称1")
	@NotBlank
	private String name;

	@ApiModelProperty(value = "组织机构描述", example = "这是测试的组织机构1描述")
	private String description;

	@ApiModelProperty(value = "上级组织机构")
	private String pdid;

}
