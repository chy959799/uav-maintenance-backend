package com.thinglinks.uav.departments.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DepartmentPageQuery extends BasePageRequest {

	@ApiModelProperty(value = "组织机构名称", example = "测试")
	private String name;
	@ApiModelProperty(value = "did", example = "1")
	private String did;
}
