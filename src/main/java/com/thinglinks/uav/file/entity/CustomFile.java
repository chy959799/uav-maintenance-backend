package com.thinglinks.uav.file.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/22 22:07
 */
@Entity
@Data
@Table(name = "files")
public class CustomFile extends BaseEntity {
    @Column(name = "fid")
    @ApiModelProperty("内部id")
    private String fid;

    @Column(name = "name")
    @ApiModelProperty("文件名称")
    private String name;

    @Column(name = "mime")
    @ApiModelProperty("文件类型")
    private String mime;

    @Column(name = "extension")
    @JsonProperty("extension")
    @ApiModelProperty("扩展名")
    private String extension;

    @Column(name = "directory")
    @JsonProperty("directory")
    @ApiModelProperty("所属文件夹 - commom: 通用 material: 资料相关, product: 产品相关, user: 用户相关, temp: 临时文件 fault: 故障相关 order: 工单相关")
    private String directory;

    @Column(name = "location")
    @JsonProperty("location")
    @ApiModelProperty("对象存在中的文件路径")
    private String location;

    @Column(name = "filepath")
    @ApiModelProperty("文件本地路径")
    private String filepath;

    @Column(name = "enable")
    @ApiModelProperty("是否可用")
    private Boolean enable;

    @Column(name = "download")
    @ApiModelProperty("是否可下载")
    private Boolean download;

    @Column(name = "uid")
    @ApiModelProperty("用户id")
    private String uid;

    @Column(name = "pid")
    @ApiModelProperty("产品id")
    private String pid;

    @Column(name = "materid")
    @ApiModelProperty("材料id")
    private String materid;

    @Column(name = "fauid")
    @ApiModelProperty("故障id")
    private String fauid;

    @Column(name = "ordid")
    @ApiModelProperty("工单id")
    private String ordid;

    @Column(name = "consordid")
    @ApiModelProperty("咨询工单id")
    private String consordid;

    @Column(name = "ordlogid")
    @ApiModelProperty("工单日志id")
    private String ordlogid;

    @Column(name = "expid")
    @ApiModelProperty("快递id")
    private String expid;

    @Column(name = "size")
    @ApiModelProperty("文件大小")
    private Long size;

    @Column(name = "type")
    @ApiModelProperty("文件类型：1:工单定损报价；2:工单二维码;")
    private String type;

    @Column(name = "imid")
    @ApiModelProperty("保险理赔材料id")
    private String imid;
}
