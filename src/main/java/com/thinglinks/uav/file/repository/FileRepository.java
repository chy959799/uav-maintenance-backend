package com.thinglinks.uav.file.repository;

import com.thinglinks.uav.file.entity.CustomFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author huiyongchen
 */
public interface FileRepository extends JpaRepository<CustomFile, Long>, JpaSpecificationExecutor<CustomFile> {
    List<CustomFile> findAllByPid(String pid);

    List<CustomFile> findAllByFauid(String fauid);

    void deleteAllByFauid(String fauid);

    void deleteAllByMaterid(String materid);

    void deleteAllByFauidIn(List<String> ids);

    List<CustomFile> findAllByPidIn(List<String> ids);

    void deleteAllByPidIn(List<String> ids);

    void deleteByFid(String fid);

    void deleteByOrdid(String orderId);

    void deleteByName(String name);

    List<CustomFile> findAllByConsordid(String ordid);

    void deleteAllByConsordid(String ordid);

    List<CustomFile> findByOrdlogid(String orderLogId);

    CustomFile findByFid(String fid);

    List<CustomFile> findByOrdidAndType(String orderId, String type);

    CustomFile getByOrdidAndType(String orderId, String type);

    CustomFile findByExpid(String expid);

    Boolean existsByOrdlogid(String ordlogid);

    void deleteByOrdidAndType(String orderId, String type);

    CustomFile findFirstByOrdidAndTypeOrderByCtDesc(String orderId, String type);

}
