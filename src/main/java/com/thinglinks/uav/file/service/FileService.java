package com.thinglinks.uav.file.service;

import com.thinglinks.uav.common.dto.BaseResponse;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    void insertMaterialFile(MultipartFile file, String materid);

    @Transactional
    void deleteByMaterid(String materid);

    String uploadFile(MultipartFile file);

    @Transactional
    BaseResponse<Void> deleteByFid(String fid) throws Exception;

    /**
     * 文件上传
     *
     * @param file
     * @return
     * @throws Exception
     */
    BaseResponse<String> uploadFile2Bucket(MultipartFile file) throws Exception;

    /**
     * 获取文件的外链地址
     *
     * @param fid
     * @return
     */
    String getProxyUrlByFile(String fid);
}
