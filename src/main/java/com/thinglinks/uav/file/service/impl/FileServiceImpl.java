package com.thinglinks.uav.file.service.impl;

import cn.hutool.crypto.digest.DigestUtil;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.file.service.FileService;
import com.thinglinks.uav.system.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/22 22:15
 */
@Service
@Slf4j
public class FileServiceImpl implements FileService {
    @Resource
    private FileRepository fileRepository;

    @Resource
    private CustomMinIOClient minioClient;

    @Value(value = "${minio.bucketName}")
    private String bucketName;

    @Override
    public void insertMaterialFile(MultipartFile file, String materid) {
        CustomFile f = new CustomFile();
        f.setMaterid(materid);
        insertFile(file, f);
    }

    private void insertFile(MultipartFile file, CustomFile f) {
        String objectName = uploadFile(file);

        f.setFid(CommonUtils.uuid());
        f.setUid(CommonUtils.getUid());
        f.setEnable(Boolean.TRUE);
        f.setDownload(Boolean.TRUE);
        f.setDirectory(bucketName);
        f.setExtension(CommonUtils.getExtension(file.getOriginalFilename()));
        f.setMime(file.getContentType());
        f.setName(file.getOriginalFilename());
        f.setLocation(objectName);
        f.setSize(file.getSize());

        fileRepository.save(f);
    }

    @Override
    public void deleteByMaterid(String materid) {
        fileRepository.deleteAllByMaterid(materid);
    }

    @Override
    public String uploadFile(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            String objectName = DigestUtil.md5Hex(bytes) + "." + CommonUtils.getExtension(file.getOriginalFilename());

            if (!minioClient.isObjectExist(bucketName, objectName)) {
                minioClient.uploadFile(bucketName, objectName, file.getInputStream());
            }
            return objectName;
        } catch (Exception e) {
            throw new BusinessException("文件上传失败");
        }
    }

    @Override
    public BaseResponse<Void> deleteByFid(String fid) throws Exception {
        CustomFile customFile = fileRepository.findByFid(fid);
        if (Objects.nonNull(customFile)) {
            String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
            minioClient.removeFile(customFile.getDirectory(), objectName);
            fileRepository.delete(customFile);
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<String> uploadFile2Bucket(MultipartFile f) throws Exception {
        CustomFile customFile = new CustomFile();
        customFile.setFid(CommonUtils.uuid());
        customFile.setSize(f.getSize());
        customFile.setName(CommonUtils.fileName(f));
        customFile.setExtension(CommonUtils.getExtension(customFile.getName()));
        customFile.setDownload(Boolean.TRUE);
        customFile.setDirectory(bucketName);
        customFile.setMime(f.getContentType());
        String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
        minioClient.uploadFile(bucketName, objectName, f.getInputStream());
        fileRepository.save(customFile);
        return BaseResponse.success(customFile.getFid());
    }

    @Override
    public String getProxyUrlByFile(String fid) {
        try {
            CustomFile customFile = fileRepository.findByFid(fid);
            if (Objects.isNull(customFile)) {
                return StringUtils.EMPTY;
            }
            String objectName = customFile.getFid().concat(CommonConstants.DOT).concat(customFile.getExtension());
            return minioClient.getProxyUrl(OrderConstants.ORDER_BUCKET, objectName);
        } catch (Exception e) {
            log.error("getFileProxyUrl error", e);
        }
        return StringUtils.EMPTY;
    }
}
