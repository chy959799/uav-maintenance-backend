package com.thinglinks.uav.file.controller;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.file.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author iwiFool
 * @date 2024/4/1
 */
@Api(tags = "文件管理")
@RestController
@RequestMapping("files")
public class FileController {

    @Resource
    private FileService fileService;

    @Operation(summary = "删除文件")
    @DeleteMapping("{fid}")
    public BaseResponse<Void> delete(@PathVariable String fid) throws Exception {
        return fileService.deleteByFid(fid);
    }

    @Operation(summary = "文件上传")
    @PostMapping(path = "upload")
    public BaseResponse<String> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        return fileService.uploadFile2Bucket(file);
    }
}
