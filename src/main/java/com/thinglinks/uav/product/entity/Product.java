package com.thinglinks.uav.product.entity;

import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/21 16:20
 */
@Entity
@Data
@Table(name="products")
@TenantTable
public class Product extends BaseEntity {
    /**
     * 产品内部ID
     */
    @ApiModelProperty("产品内部ID")
    @Column(name="pid")
    private String pid;

    /**
     * 产品名称存
     */
    @ApiModelProperty("产品名称存")
    @Column(name="name")
    private String name;

    /**
     * 产品编码
     */
    @ApiModelProperty("产品编码")
    @Column(name="code")
    private String code;

    /**
     * 产品内部编码   目前弃用
     */
    @ApiModelProperty("产品内部编码")
    @Column(name="inner_code")
    private String innerCode;

    /**
     * 规格型号
     */
    @ApiModelProperty("规格型号")
    @Column(name="model")
    private String model;

    /**
     * 计量单位
     */
    @ApiModelProperty("计量单位")
    @Column(name="unit")
    private String unit;

    /**
     * 原产地
     */
    @ApiModelProperty("原产地")
    @Column(name="origin")
    private String origin;

    /**
     * 经销商
     */
    @ApiModelProperty("经销商")
    @Column(name="agent")
    private String agent;

    /**
     * 产品出厂价格
     */
    @ApiModelProperty("产品出厂价格")
    @Column(name="factory_price")
    private BigDecimal factoryPrice;

    /**
     * 产品建议销售价格
     */
    @ApiModelProperty("产品建议销售价格")
    @Column(name="sales_price")
    private Double salesPrice;

    /**
     * 启用状态
     */
    @ApiModelProperty("启用状态")
    @Column(name="enable")
    private Integer enable;

    /**
     * 备注（当类型为备件时，该字段表示备件适用设备）
     */
    @ApiModelProperty("备注（当类型为备件时，该字段表示备件适用设备）")
    @Column(name="remark")
    private String remark;

    /**
     * 产品描述
     */
    @ApiModelProperty("产品描述")
    @Column(name="description")
    private String description;

    /**
     * cpid
     */
    @ApiModelProperty("cpid")
    @Column(name="cpid")
    private String cpid;

    /**
     * uid
     */
    @ApiModelProperty("uid")
    @Column(name="uid")
    private String uid;

    /**
     * catid
     */
    @ApiModelProperty("分类Id")
    @Column(name="catid")
    private String catid;

    /**
     * brandid
     */
    @ApiModelProperty("品牌Id")
    @Column(name="brandid")
    private String brandId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "catid", referencedColumnName = "catid", insertable = false, updatable = false)
    private Category category;
}
