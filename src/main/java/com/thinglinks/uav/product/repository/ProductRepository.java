package com.thinglinks.uav.product.repository;

import com.thinglinks.uav.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/21 16:23
 */
public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {
    Product findByPid(String pid);

    boolean existsByName(String name);

    boolean existsByCode(String code);

    boolean existsByCatidIn(List<String> catid);

    void deleteAllByPidIn(List<String> ids);

    boolean existsByNameAndCatid(String name, String catid);

    List<Product> findByCatid(String catid);

    List<Product> findAllByPidIn(List<String> ids);

    List<Product> findListByCode(String spareCode);

    Product findByCode(String code);
}
