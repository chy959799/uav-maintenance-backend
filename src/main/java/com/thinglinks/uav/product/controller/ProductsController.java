package com.thinglinks.uav.product.controller;

import com.thinglinks.uav.category.dto.IdRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.product.dto.ProductRequest;
import com.thinglinks.uav.product.dto.AddProductResponse;
import com.thinglinks.uav.product.dto.ProductDetailDTO;
import com.thinglinks.uav.product.dto.ProductPageRequest;
import com.thinglinks.uav.product.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/21 16:40
 */
@Api(tags = "产品目录管理")
@RestController
@RequestMapping(value = "/products")
@Slf4j
public class ProductsController {
    @Resource
    private ProductService productService;


    @Operation(summary = "获取目录详情")
    @GetMapping(path = "/{pid}")
    public BaseResponse<ProductDetailDTO> getProduct(@PathVariable("pid") String pid) throws Exception {
        return productService.getProduct(pid);
    }

    @Operation(summary = "分页获取目录")
    @GetMapping
    public BasePageResponse<ProductDetailDTO> getPage(ProductPageRequest request){
        return productService.getPage(request);
    }

    @Operation(summary = "新增目录详情")
    @PostMapping
    public BaseResponse<AddProductResponse> addProduct(@Valid ProductRequest productRequest) throws Exception {
        return productService.addProduct(productRequest);
    }

    @Operation(summary = "批量删除目录")
    @DeleteMapping
    public BaseResponse<Void> product(@RequestBody @Valid IdRequest request) throws Exception{
        return productService.deleteProduct(request);
    }

    @Operation(summary = "批量导入产品目录")
    @PostMapping(path = "import")
    public BaseResponse<Void> importProduct(@RequestParam(value = "file") MultipartFile file,HttpServletResponse response) throws IOException {
        return productService.importProduct(file,response);
    }

    @Operation(summary = "编辑目录")
    @PatchMapping(path = "{pid}")
    public BaseResponse<Void> editProduct(@PathVariable("pid") String pid,@Valid ProductRequest request) throws Exception {
        return productService.editProduct(pid,request);
    }

    @Operation(summary = "文件删除")
    @DeleteMapping(path = "file")
    public BaseResponse<Void> delFile(String url) throws Exception {
        return productService.delFile(url);
    }


    @Operation(summary = "批量导入备件目录")
    @PostMapping(path = "import/spare")
    public BaseResponse<Void> importProductSpare(@RequestParam(value = "file") MultipartFile file, HttpServletResponse response,@RequestParam @Nonnull String type) throws IOException {
        return productService.importProductSpare(file,response,type);
    }
}
