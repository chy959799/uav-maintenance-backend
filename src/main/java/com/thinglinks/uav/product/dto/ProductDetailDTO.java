package com.thinglinks.uav.product.dto;

import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/21 17:11
 */
@Data
public class ProductDetailDTO extends Product {

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "文件信息")
    private List<String> files;
}
