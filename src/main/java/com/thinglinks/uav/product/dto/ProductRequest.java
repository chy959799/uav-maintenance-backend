package com.thinglinks.uav.product.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/22 9:09
 */
@Data
public class ProductRequest {

    @ApiModelProperty(value = "目录名称")
    @NotNull
    private String name;

    @ApiModelProperty(value = "目录分类")
    @NotNull
    private String catid;

    @ApiModelProperty(value = "产品编码")
    @NotNull
    private String code;

    @ApiModelProperty(value = "规格型号")
    private String model;

    @ApiModelProperty(value = "品牌id")
    private String brandid;

    @ApiModelProperty(value = "原产地")
    private String origin;

    @ApiModelProperty(value = "计量单位")
    private String unit;

    @ApiModelProperty(value = "产品出厂价")
    private BigDecimal factoryPrice;

    @ApiModelProperty(value = "产品建议销售价格")
    private double salesPrice;

    @ApiModelProperty("采购成本价")
    private BigDecimal purchasePrice;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "请求标识 1:产品 4:备件")
    private String type;

    @ApiModelProperty("文件0")
    MultipartFile file0;

    @ApiModelProperty("文件1")
    MultipartFile file1;

    @ApiModelProperty("文件0")
    MultipartFile file2;

    @ApiModelProperty("文件0")
    MultipartFile file3;
    @ApiModelProperty("文件0")
    MultipartFile file4;
    @ApiModelProperty("文件0")
    MultipartFile file5;

}
