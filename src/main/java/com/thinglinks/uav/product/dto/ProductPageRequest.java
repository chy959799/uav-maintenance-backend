package com.thinglinks.uav.product.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/22 13:00
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductPageRequest extends BasePageRequest {

    @ApiModelProperty(value = "目录分类")
    private String catid ;

    @ApiModelProperty(value = "目录类型")
    private String types;

    @ApiModelProperty(value = "编码")
    private String code;

    @ApiModelProperty(value = "目录名称")
    private String name;

    @ApiModelProperty(value = "是否带出子类")
    private boolean showSon = true;
}
