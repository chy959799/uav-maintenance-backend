package com.thinglinks.uav.product.dto;

import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.product.entity.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/22 9:22
 */
@Data
public class AddProductResponse extends Product {
    @ApiModelProperty(value =  "产品图片")
    private List<CustomFile> flies;
}
