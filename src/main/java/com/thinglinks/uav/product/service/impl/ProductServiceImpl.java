package com.thinglinks.uav.product.service.impl;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.thinglinks.uav.category.dto.CategoryTreeRequest;
import com.thinglinks.uav.category.dto.IdRequest;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.category.repository.CategoryRepository;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.ExcelUtils;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.faults.repository.FaultsRepository;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.product.dto.AddProductResponse;
import com.thinglinks.uav.product.dto.ProductDetailDTO;
import com.thinglinks.uav.product.dto.ProductPageRequest;
import com.thinglinks.uav.product.dto.ProductRequest;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.product.service.ProductService;
import com.thinglinks.uav.spare.repository.ProjectProductsRepository;
import com.thinglinks.uav.stock.repository.StockRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.util.Strings;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.tika.Tika;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/21 16:25
 */
@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    static String PRODUCT_TYPE = "1";
    static String SPARE_TYPE = "4";
    static String SPARE_PARTS_TYPE = "6";

    static String[] PRODUCT_TITLE = {"所属分类", "产品名", "厂商编码", "规格型号", "计量单位", "原产地", "出厂价", "指导价", "描述"};
    static String[] SPARE_TITLE = {"所属分类", "备件/备品名", "厂商编码", "规格型号", "计量单位", "原产地", "返厂价格", "指导价", "描述"};

    static Integer PRODUCT_DEFAULT_ACTIVE = 1;

    static String PATTERN = ".*/([^/?]+)\\?.*";
    static Pattern PRODUCT_IMG_PATTERN = Pattern.compile(PATTERN);
    static String PRODUCT_BUCKET_NAME = "product";
    @Resource
    private ProductRepository productRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private FaultsRepository faultsRepository;

    @Resource
    private StockRepository stockRepository;

    @Resource
    private CustomMinIOClient minIOClient;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private ProjectProductsRepository projectProductsRepository;

    @Override
    public BaseResponse<ProductDetailDTO> getProduct(String pid) throws Exception {
        ProductDetailDTO productsDetailDTO = new ProductDetailDTO();
        Product product = productRepository.findByPid(pid);
        if (!Objects.isNull(product)) {
            BeanUtils.copyProperties(product, productsDetailDTO);
            List<CustomFile> customFiles = fileRepository.findAllByPid(pid);
            List<String> fileUrls = new ArrayList<>();
            for (CustomFile customFile : customFiles) {
                String url = minIOClient.getProxyUrl("product", customFile.getLocation());
                fileUrls.add(url);
            }
            productsDetailDTO.setFiles(fileUrls);
            return BaseResponse.success(productsDetailDTO);
        }
        return BaseResponse.fail("产品不存在");

    }

    @Override
    public BaseResponse<AddProductResponse> addProduct(ProductRequest request) throws Exception {
        Category category = categoryRepository.findByCatid(request.getCatid());
        if (Objects.isNull(category)) {
            return BaseResponse.fail("分类不存在");
        }

        Map<String, String> authMap = new HashMap<>();
        authMap.put(PRODUCT_TYPE, "productAdd");
        authMap.put(SPARE_TYPE, "spareAdd");
        authMap.put(SPARE_PARTS_TYPE, "sparePartsAdd");

        List<String> userAuth = CommonUtils.getUserAuth();
        String requiredAuth = authMap.get(category.getType());
        if (requiredAuth != null && !userAuth.contains(requiredAuth)) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        }

        AddProductResponse response = new AddProductResponse();
        List<CustomFile> customFileList = new ArrayList<>();
        Product product = new Product();
        BeanUtils.copyProperties(request, product);
        // 产品 同分类下唯一
        if (!Strings.isNotEmpty(product.getName()) && productRepository.existsByNameAndCatid(product.getName(), product.getCatid())) {
            return BaseResponse.fail("产品名称重复");
        }
        // 如果添加的是备件/备品则必须设置出厂价
        if (SPARE_TYPE.equals(request.getType()) || SPARE_PARTS_TYPE.equals(request.getType())) {
            if (Objects.isNull(product.getFactoryPrice())) {
                return BaseResponse.fail("返厂价格不能为空");
            }
            BigDecimal factoryPrice = request.getFactoryPrice();
            if(factoryPrice.compareTo(BigDecimal.ZERO) <= 0){
                return BaseResponse.fail("返厂价格不能小于等于0");
            }

        }
        // code不能为空
        if ((request.getType().equals(SPARE_TYPE) || request.getType().equals(SPARE_PARTS_TYPE)) && Objects.isNull(product.getCode())) {
            return BaseResponse.fail("产品编码不能为空");
        }
        if (productRepository.existsByCode(product.getCode())) {
            return BaseResponse.fail("产品编码重复");
        }
        product.setPid(CommonUtils.uuid());
        product.setEnable(PRODUCT_DEFAULT_ACTIVE);
        product.setInnerCode(CommonUtils.uuid());
        Product save = productRepository.save(product);
        BeanUtils.copyProperties(save, response);
        minIOClient.createMinioClient();
        if (Objects.nonNull(request.getFile0())) {
            CustomFile customFile = upLoadFile(request.getFile0(), response.getPid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile1())) {
            CustomFile customFile = upLoadFile(request.getFile1(), response.getPid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile2())) {
            CustomFile customFile = upLoadFile(request.getFile2(), response.getPid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile3())) {
            CustomFile customFile = upLoadFile(request.getFile3(), response.getPid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile4())) {
            CustomFile customFile = upLoadFile(request.getFile4(), response.getPid());
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile5())) {
            CustomFile customFile = upLoadFile(request.getFile5(), response.getPid());
            customFileList.add(customFile);
        }
        response.setFlies(customFileList);

        return BaseResponse.success(response);
    }

    public CustomFile upLoadFile(MultipartFile file, String pid) throws Exception {
        Tika tika = new Tika();
        String bucketName = PRODUCT_BUCKET_NAME;
        CustomFile f = new CustomFile();
        f.setFid(CommonUtils.uuid());
        f.setSize(file.getSize());
        f.setName(file.getOriginalFilename());
        f.setExtension(FilenameUtils.getExtension(file.getOriginalFilename()));
        f.setDownload(Boolean.TRUE);
        f.setDirectory(bucketName);
        f.setEnable(Boolean.TRUE);
        f.setPid(pid);
        f.setMime(tika.detect(file.getInputStream()));
        f.setUid(CommonUtils.getUid());
        f.setLocation(f.getFid().concat(".").concat(f.getExtension()));
        String objectName = f.getFid().concat(CommonConstants.DOT).concat(f.getExtension());
        CustomFile saveCustomFile = fileRepository.save(f);
        minIOClient.uploadFile(bucketName, objectName, file.getInputStream());
        return saveCustomFile;
    }

    @Override
    public BasePageResponse<ProductDetailDTO> getPage(ProductPageRequest request) {
        List<String> newTypes = new ArrayList<>();
        if (Strings.isNotEmpty(request.getTypes())) {
            newTypes = Arrays.asList(request.getTypes().split("-"));
        }
        PageRequest pageRequest = request.of();
        List<String> finalNewTypes = newTypes;
        Specification<Product> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (!finalNewTypes.isEmpty()) {
                Join<Product, Category> categoryJoin = root.join("category", JoinType.LEFT);
                predicates.add(criteriaBuilder.in(categoryJoin.get("type")).value(finalNewTypes));
            }

            if (Objects.nonNull(request.getName())) {
                predicates.add(criteriaBuilder.like(root.get("name"), CommonUtils.assembleLike(request.getName())));
            }
            if (Objects.nonNull(request.getCode()) && !request.getCode().isEmpty()) {
                predicates.add(criteriaBuilder.like(root.get("code"), CommonUtils.assembleLike(request.getCode())));
            }
            if(request.isShowSon()){
                if (Objects.nonNull(request.getCatid())) {
                    List<Category> all = categoryRepository.findAllByTypeIn(finalNewTypes);
                    List<CategoryTreeRequest> collect = all.stream().map(category -> {
                        CategoryTreeRequest categoryTreeRequest = new CategoryTreeRequest();
                        BeanUtils.copyProperties(category, categoryTreeRequest);
                        return categoryTreeRequest;
                    }).collect(Collectors.toList());
                    List<CategoryTreeRequest> allList = CategoryTreeRequest.buildTree2(request.getCatid(), collect);
                    List<String> categoryIds = getCategoryIds(allList);
                    categoryIds.add(request.getCatid());
                    predicates.add(root.get("catid").in(criteriaBuilder.literal(categoryIds)));
                }
            }else {
                if (Objects.nonNull(request.getCatid())) {
                    predicates.add(criteriaBuilder.equal(root.get("catid"), request.getCatid()));
                }
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<Product> page = productRepository.findAll(specification, pageRequest);

        List<ProductDetailDTO> result = page.stream().map(p -> {
            ProductDetailDTO productDetailDTO = new ProductDetailDTO();
            BeanUtils.copyProperties(p, productDetailDTO);
            productDetailDTO.setCreatedAt(DateUtils.formatDateTime(p.getCt()));
            productDetailDTO.setUpdatedAt(DateUtils.formatDateTime(p.getUt()));
            List<CustomFile> customFiles = fileRepository.findAllByPid(p.getPid());
            List<String> fileUrls = new ArrayList<>();
            for (CustomFile file : customFiles) {
                try {
                    String objectName = file.getFid().concat(CommonConstants.DOT).concat(file.getExtension());
                    String url = minIOClient.getProxyUrl(PRODUCT_BUCKET_NAME, objectName);
                    fileUrls.add(url);
                } catch (Exception e) {
                    log.error("获取产品链接失败", e);
                }
            }
            productDetailDTO.setFiles(fileUrls);
            return productDetailDTO;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, result);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> deleteProduct(IdRequest request) throws Exception {

        Map<String, String> authMap = new HashMap<>();
        authMap.put("1", "productDelete");
        authMap.put("4", "spareDelete");
        List<Product> products = productRepository.findAllByPidIn(request.getIds());
        List<String> userAuth = CommonUtils.getUserAuth();
        Set<String> requiredAuth = products.stream()
                .map(product -> {
                    String type = product.getCategory().getType();
                    return authMap.get(type);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        if (!requiredAuth.isEmpty() && !new HashSet<>(userAuth).containsAll(requiredAuth)) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        }

        if (stockRepository.existsByPidIn(request.getIds()) || faultsRepository.existsByPidIn(request.getIds()) || projectProductsRepository.existsByPidIn(request.getIds()) || deviceRepository.existsByPidIn(request.getIds())) {
            return BaseResponse.fail("删除失败，该产品存关联信息");
        }
        productRepository.deleteAllByPidIn(request.getIds());
        // 删除文件
        List<String> ids = request.getIds();
        List<CustomFile> customFiles = fileRepository.findAllByPidIn(ids);
        for (CustomFile customFile : customFiles) {
            minIOClient.removeFile(PRODUCT_BUCKET_NAME, customFile.getLocation());
        }
        fileRepository.deleteAllByPidIn(request.getIds());
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> importProduct(MultipartFile file, HttpServletResponse response) throws IOException {
        InputStream fileInputStream = file.getInputStream();
        ExcelReader reader = ExcelUtil.getReader(fileInputStream);
        List<List<Object>> fileTitle = reader.read(0, 0);
        if (!ExcelUtils.checkTemplate(PRODUCT_TITLE, fileTitle)) {
            return BaseResponse.fail("模板不合法");
        }
        reader.addHeaderAlias("所属分类", "catid");
        reader.addHeaderAlias("产品名", "name");
        reader.addHeaderAlias("厂商编码", "code");
        reader.addHeaderAlias("规格型号", "model");
        reader.addHeaderAlias("计量单位", "unit");
        reader.addHeaderAlias("原产地", "origin");
        reader.addHeaderAlias("出厂价", "factoryPrice");
        reader.addHeaderAlias("指导价", "salesPrice");
        reader.addHeaderAlias("描述", "description");
        List<Product> products = reader.readAll(Product.class);
        Map<Product, String> failProducts = new HashMap<>();
        for (Product product : products) {
            log.info("导入产品：{}", product);

            // 文件中的分类名
            String catName = product.getCatid();
            if (!Strings.isNotEmpty(product.getCatid())) {
                failProducts.put(product, "产品所属分类为空，请检查");
                continue;
            } else if (!Strings.isNotEmpty(product.getName())) {
                failProducts.put(product, "产品名称为空，请检查");
                continue;
            } else if (!Strings.isNotEmpty(product.getCode())) {
                failProducts.put(product, "产品编码为空，请检查");
                continue;
            }
            if (productRepository.existsByCode(product.getCode())) {
                failProducts.put(product, "产品编码已存在");
                continue;
            }
            String[] split = product.getCatid().split(Pattern.quote(">"));
            for (int i = 0; i < split.length; i++) {
                split[i] = split[i].trim();
            }
            List<Category> cattegoryList = new ArrayList<>();
            for (int i = 0; i < split.length; i++) {
                if (categoryRepository.existsByNameAndType(split[i], PRODUCT_TYPE)) {
                    if (i == 0) {
                        Category category = categoryRepository.findByNameAndType(split[i], PRODUCT_TYPE).get(0);
                        cattegoryList.add(category);
                    } else {
                        List<Category> categoryList = categoryRepository.findByNameAndType(split[i], PRODUCT_TYPE);
                        boolean flag = false;
                        for (Category c : categoryList) {
                            if (c.getPcatid().equals(cattegoryList.get(i - 1).getCatid())) {
                                cattegoryList.add(c);
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) {
                            Category category = new Category();
                            category.setLevel(i + 1);
                            category.setName(split[i]);
                            category.setType(PRODUCT_TYPE);
                            category.setCatid(CommonUtils.uuid());
                            category.setPcatid(cattegoryList.get(i - 1).getCatid());
                            category.setInnerCode(CommonUtils.uuid());
                            category.setEnable(1);
                            Category newCategory = categoryRepository.save(category);
                            cattegoryList.add(newCategory);
                        }
                    }
                } else {
                    Category category = new Category();
                    if (i == 0) {
                        category.setLevel(1);
                        category.setName(split[i]);
                        category.setType(PRODUCT_TYPE);
                        category.setCatid(CommonUtils.uuid());
                        category.setInnerCode(CommonUtils.uuid());
                        category.setEnable(1);
                        Category newCategory = categoryRepository.save(category);
                        cattegoryList.add(newCategory);
                    } else {
                        category.setLevel(i + 1);
                        category.setName(split[i]);
                        category.setType(PRODUCT_TYPE);
                        category.setCatid(CommonUtils.uuid());
                        category.setPcatid(cattegoryList.get(i - 1).getCatid());
                        category.setInnerCode(CommonUtils.uuid());
                        category.setEnable(1);
                        Category newCategory = categoryRepository.save(category);
                        cattegoryList.add(newCategory);
                    }
                }
            }
            // 同一分类下不能重复产品名称
            if (productRepository.existsByNameAndCatid(product.getName(), product.getCatid())) {
                product.setCatid(catName);
                failProducts.put(product, "产品名重复");
                continue;
            }
            product.setCatid(cattegoryList.get(cattegoryList.size() - 1).getCatid());
            product.setEnable(1);
            product.setPid(CommonUtils.uuid());
            product.setInnerCode(CommonUtils.uuid());
            productRepository.save(product);
        }
        if (!failProducts.isEmpty()) {
            // 设置写出错误文件表头 添加错误描述
            Map<String, String> headersMap = new HashMap<>();
            headersMap.put("catid", "所属分类");
            headersMap.put("name", "产品名");
            headersMap.put("code", "厂商编码");
            headersMap.put("model", "规格型号");
            headersMap.put("unit", "计量单位");
            headersMap.put("origin", "原产地");
            headersMap.put("factoryPrice", "出厂价");
            headersMap.put("salesPrice", "指导价");
            headersMap.put("description", "描述");
            Workbook workbook = ExcelUtils.createErrExcel(failProducts, headersMap, "productInfoError");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
            String fileName = URLEncoder.encode("failProductsSpare", "UTF-8");
            response.setHeader("Content-Disposition", "attachment;fileName=" + fileName + ".xlsx");
            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
            return BaseResponse.fail("数据不合法");
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> importProductSpare(MultipartFile file, HttpServletResponse response, String type) throws IOException {
        String curType = getType(type);

        InputStream fileInputStream = file.getInputStream();
        ExcelReader reader = ExcelUtil.getReader(fileInputStream);
        List<List<Object>> fileTitle = reader.read(0, 0);
        if (!ExcelUtils.checkTemplate(SPARE_TITLE, fileTitle)) {
            return BaseResponse.fail("模板不合法");
        }

        reader.addHeaderAlias("所属分类", "catid")
                .addHeaderAlias("备件/备品名", "name")
                .addHeaderAlias("厂商编码", "code")
                .addHeaderAlias("规格型号", "model")
                .addHeaderAlias("计量单位", "unit")
                .addHeaderAlias("原产地", "origin")
                .addHeaderAlias("返厂价格", "factoryPrice")
                .addHeaderAlias("指导价", "salesPrice")
                .addHeaderAlias("描述", "description");
        List<Product> products = reader.readAll(Product.class);
        Map<Product, String> failProducts = new HashMap<>();
        for (Product product : products) {
            if (!Strings.isNotEmpty(product.getCatid())) {
                failProducts.put(product, "所属分类不存在，请检查");
                continue;
            } else if (!Strings.isNotEmpty(product.getName())) {
                failProducts.put(product, "备件名称为空，请检查");
                continue;
            } else if (!Strings.isNotEmpty(product.getCode())) {
                failProducts.put(product, "备件编码为空，请检查");
                continue;
            }

            BigDecimal factoryPrice = product.getFactoryPrice();
            if (factoryPrice == null || factoryPrice.compareTo(BigDecimal.ZERO) <= 0) {
                failProducts.put(product, "返厂价格不能为空，请检查");
                continue;
            }


            if (productRepository.existsByCode(product.getCode())) {
                failProducts.put(product, "备件编码重复");
                continue;
            }

            // 所属分类
            String[] split = product.getCatid().split(Pattern.quote(">"));
            for (int i = 0; i < split.length; i++) {
                split[i] = split[i].trim();
            }
            List<Category> cattegoryList = new ArrayList<>();
            for (int i = 0; i < split.length; i++) {
                if (categoryRepository.existsByNameAndType(split[i], curType)) {
                    if (i == 0) {
                        Category category = categoryRepository.findByNameAndType(split[i], curType).get(0);
                        cattegoryList.add(category);
                    } else {
                        List<Category> categoryList = categoryRepository.findByNameAndType(split[i], curType);
                        boolean flag = false;
                        for (Category c : categoryList) {
                            if (c.getPcatid().equals(cattegoryList.get(i - 1).getCatid())) {
                                cattegoryList.add(c);
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) {
                            Category category = new Category();
                            category.setCpid(CommonUtils.getCpid());
                            category.setLevel(i + 1);
                            category.setName(split[i]);
                            category.setType(curType);
                            category.setCatid(CommonUtils.uuid());
                            category.setPcatid(cattegoryList.get(i - 1).getCatid());
                            category.setInnerCode(CommonUtils.uuid());
                            category.setEnable(1);
                            Category newCategory = categoryRepository.save(category);
                            cattegoryList.add(newCategory);
                        }
                    }
                } else {
                    Category category = new Category();
                    if (i == 0) {
                        category.setLevel(1);
                        category.setName(split[i]);
                        category.setType(curType);
                        category.setCatid(CommonUtils.uuid());
                        category.setInnerCode(CommonUtils.uuid());
                        category.setEnable(1);
                        category.setCpid(CommonUtils.getCpid());
                        Category newCategory = categoryRepository.save(category);
                        cattegoryList.add(newCategory);
                    } else {
                        category.setLevel(i + 1);
                        category.setName(split[i]);
                        category.setCpid(CommonUtils.getCpid());
                        category.setType(curType);
                        category.setCatid(CommonUtils.uuid());
                        category.setPcatid(cattegoryList.get(i - 1).getCatid());
                        category.setInnerCode(CommonUtils.uuid());
                        category.setEnable(1);
                        Category newCategory = categoryRepository.save(category);
                        cattegoryList.add(newCategory);
                    }
                }
            }
            product.setCatid(cattegoryList.get(cattegoryList.size() - 1).getCatid());
            product.setEnable(1);
            product.setPid(CommonUtils.uuid());
            product.setInnerCode(CommonUtils.uuid());
            productRepository.save(product);
        }
        if (!failProducts.isEmpty()) {
            // 设置写出错误文件表头 添加错误描述
            Map<String, String> headersMap = new HashMap<>();
            headersMap.put("catid", "所属分类");
            headersMap.put("name", "备件/备品名");
            headersMap.put("code", "厂商编码");
            headersMap.put("model", "规格型号");
            headersMap.put("unit", "计量单位");
            headersMap.put("origin", "原产地");
            headersMap.put("factoryPrice", "返厂价格");
            headersMap.put("salesPrice", "指导价");
            headersMap.put("description", "描述");
            Workbook workbook = ExcelUtils.createErrExcel(failProducts, headersMap, "productInfoErrorSpare");
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
            String fileName = URLEncoder.encode("failProductsSpare", "UTF-8");
            response.setHeader("Content-Disposition", "attachment;fileName=" + fileName + ".xlsx");
            ServletOutputStream outputStream = response.getOutputStream();
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
            return BaseResponse.fail("数据不合法");
        }
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> editProduct(String pid, ProductRequest request) throws Exception {

        Category category = categoryRepository.findByCatid(request.getCatid());
        Map<String, String> authMap = new HashMap<>();
        authMap.put("1", "productEdit");
        authMap.put("4", "spareEdit");

        List<String> userAuth = CommonUtils.getUserAuth();
        String requiredAuth = authMap.get(category.getType());
        if (requiredAuth != null && !userAuth.contains(requiredAuth)) {
            return BaseResponse.fail(StatusEnum.NO_AUTH);
        }

        Product product = productRepository.findByPid(pid);
        BeanUtils.copyProperties(request, product);
        productRepository.save(product);
        minIOClient.createMinioClient();
        List<CustomFile> customFileList = new ArrayList<>();
        if (Objects.nonNull(request.getFile0())) {
            CustomFile customFile = upLoadFile(request.getFile0(), pid);
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile1())) {
            CustomFile customFile = upLoadFile(request.getFile1(), pid);
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile2())) {
            CustomFile customFile = upLoadFile(request.getFile2(), pid);
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile3())) {
            CustomFile customFile = upLoadFile(request.getFile3(), pid);
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile4())) {
            CustomFile customFile = upLoadFile(request.getFile4(), pid);
            customFileList.add(customFile);
        }
        if (Objects.nonNull(request.getFile5())) {
            CustomFile customFile = upLoadFile(request.getFile5(), pid);
            customFileList.add(customFile);
        }
        fileRepository.saveAll(customFileList);
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> delFile(String url) throws Exception {
        Matcher matcher = PRODUCT_IMG_PATTERN.matcher(url);
        if (!matcher.find()) {
            return BaseResponse.fail("文件路径不正确");
        }
        String fileName = matcher.group(1);
        minIOClient.removeFile(PRODUCT_BUCKET_NAME, fileName);
        String fid = "";
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex != -1) {
            fid = fileName.substring(0, dotIndex);
        }
        fileRepository.deleteByFid(fid);
        return BaseResponse.success();
    }

    public List<String> getCategoryIds(List<CategoryTreeRequest> categoryList) {
        List<String> categoryIds = new ArrayList<>();
        for (CategoryTreeRequest category : categoryList) {
            categoryIds.add(category.getCatid());
            categoryIds.addAll(getCategoryIds(category.getChildren()));
        }
        return categoryIds;
    }

    public String getType(String type){
        if(type.equals(SPARE_TYPE)){
            return SPARE_TYPE;
        }
        return SPARE_PARTS_TYPE;
    }
}
