package com.thinglinks.uav.product.service;

import com.thinglinks.uav.category.dto.IdRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.product.dto.ProductRequest;
import com.thinglinks.uav.product.dto.AddProductResponse;
import com.thinglinks.uav.product.dto.ProductDetailDTO;
import com.thinglinks.uav.product.dto.ProductPageRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author huiyongchen
 */
public interface ProductService {
    BaseResponse<ProductDetailDTO> getProduct(String pid) throws Exception;

    BaseResponse<AddProductResponse> addProduct(ProductRequest request) throws Exception;

    BasePageResponse<ProductDetailDTO> getPage(ProductPageRequest request);

    BaseResponse<Void> deleteProduct(IdRequest request) throws Exception;

    BaseResponse<Void> importProduct(MultipartFile file,HttpServletResponse response) throws IOException;

    BaseResponse<Void> importProductSpare(MultipartFile file, HttpServletResponse response,String type) throws IOException;

    BaseResponse<Void> editProduct(String pid,ProductRequest request) throws Exception;

    BaseResponse<Void> delFile(String url) throws Exception;
}
