package com.thinglinks.uav.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/5/20
 */
@Data
@Schema(description = "路由VO")
public class RouterVO {

    @Schema(description = "路由名称", example = "Root")
    private String name;

    @Schema(description = "路由地址", example = "/")
    private String path;

    @Schema(description = "组件路径", example = "Layout")
    private String component;

    @Schema(description = "重定向地址", example = "null")
    private String redirect;

    @Schema(description = "路由元信息")
    private MetaVO meta;

    @Schema(description = "子路由")
    private List<RouterVO> children = new ArrayList<>();

    @Schema(description = "子路由数量")
    private Integer count = 0;

    private Integer sort;
}
