package com.thinglinks.uav.user.vo;

import com.thinglinks.uav.customer.vo.CompanyVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/18
 */
@Data
@ApiModel(description = "用户详情VO")
public class UserDetailVO {

	@ApiModelProperty(value = "ID", example = "76")
	private Long id;

	@ApiModelProperty(value = "用户ID", example = "88fe6fcc-de2c-4a42-8383-fc098456fb43")
	private String uid;

	@ApiModelProperty(value = "用户名", example = "测试企业")
	private String username;

	@ApiModelProperty(value = "昵称", example = "John")
	private String nickName;

	@ApiModelProperty(value = "手机号码", example = "17623073974")
	private String mobile;

	@ApiModelProperty(value = "密码", example = "********")
	private String password;

	@ApiModelProperty(value = "性别", example = "1")
	private String gender;

	@ApiModelProperty(value = "头像", example = "https://example.com/avatar.jpg")
	private String avatar;

	@ApiModelProperty(value = "电子邮箱", example = "example@example.com")
	private String email;

	@ApiModelProperty(value = "微信OpenID", example = "wx1234567890")
	private String openid;

	@ApiModelProperty(value = "角色", example = "admin")
	private String role;

	@ApiModelProperty(value = "类型", example = "1")
	private String type;

	@ApiModelProperty(value = "登录类型", example = "1")
	private String loginType;

	@ApiModelProperty(value = "登录时间", example = "2024-03-18 15:41:25")
	private String loginTime;

	@ApiModelProperty(value = "登录IP", example = "106.83.33.21")
	private String loginIp;

	@ApiModelProperty(value = "登录代理", example = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0")
	private String loginAgent;

	@ApiModelProperty(value = "是否激活", example = "true")
	private Boolean activate;

	@ApiModelProperty(value = "创建时间", example = "2024-03-17 12:19:01")
	private String createdAt;

	@ApiModelProperty(value = "更新时间", example = "2024-03-18 17:28:42")
	private String updatedAt;

	@ApiModelProperty(value = "CPID", example = "137eb28e-16df-4791-89fa-101b4a1a4f8a")
	private String cpid;

	@ApiModelProperty(value = "设备ID", example = "device123")
	private String did;

	@ApiModelProperty(value = "公司ID", example = "company123")
	private String cid;

	@ApiModelProperty(value = "用户公司ID", example = "usercompany123")
	private String ucid;

	@ApiModelProperty(value = "公司信息", example = "{...}")
	private CompanyVO company;

	@ApiModelProperty(value = "角色列表", example = "[admin]")
	private List<String> roles;

	@ApiModelProperty(value = "角色名称", example = "企业管理员")
	private String rolesName;

	@ApiModelProperty(value = "权限列表", example = "[\"projectEnable\", \"personalCenterResetPaswd\", \"personalCenterEdit\"]")
	private List<String> permissions;
}
