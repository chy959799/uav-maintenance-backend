package com.thinglinks.uav.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/5/20
 */
@Data
@Schema(description = "路由meta")
public class MetaVO {

    @Schema(description = "路由标题", example = "首页")
    private String title;

    @Schema(description = "路由图标", example = "home-2-line")
    private String icon;

    @Schema(description = "是否隐藏路由", example = "false")
    private Boolean hidden;

    @Schema(description = "是否不缓存", example = "false")
    private Boolean keepAlive;

    @Schema(description = "是否固定在 tabs 上", example = "false")
    private Boolean noClosable;
}
