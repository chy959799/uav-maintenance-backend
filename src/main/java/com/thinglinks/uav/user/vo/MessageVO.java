package com.thinglinks.uav.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/28
 */
@Data
@ApiModel(description = "消息VO")
public class MessageVO {

	@ApiModelProperty(value = "消息标题")
	private String title;

	@ApiModelProperty(value = "消息内容")
	private String content;

	@ApiModelProperty(value = "消息类型")
	private String type;
}
