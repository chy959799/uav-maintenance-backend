package com.thinglinks.uav.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/21
 */
@ApiModel(description = "用户消息VO")
@Data
public class UserMessageVO {

	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "消息外部ID")
	private String msgid;

	@ApiModelProperty(value = "消息状态")
	private String status;

	@ApiModelProperty(value = "创建时间", example = "2024-03-17 12:19:01")
	private String createdAt;

	@ApiModelProperty(value = "消息")
	private MessageVO message;
}
