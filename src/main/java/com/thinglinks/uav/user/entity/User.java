package com.thinglinks.uav.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.departments.entity.Department;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author fanhaiqiu
 * @date 2019/6/25
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "users")
@Data
//@TenantTable
public class User extends BaseEntity {

    @Column(name = "uid")
    @ApiModelProperty(value = "用户id")
    private String uid;

    @Column(name = "user_name")
    @ApiModelProperty(value = "用户名")
    private String userName;

    @Column(name = "nick_name")
    @ApiModelProperty(value = "昵称")
    private String nickName;

    @Column(name = "mobile")
    @ApiModelProperty(value = "电话")
    private String mobile;

    @Column(name = "email")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Column(name = "password")
    @ApiModelProperty(value = "密码")
    @JsonIgnore
    private String password;

    @Column(name = "type")
    @JsonProperty("type")
    @ApiModelProperty(value = "用户类型1:系统用户 2:客户用户")
    private Integer userType;

    @Column(name = "gender")
    @ApiModelProperty(value = "性别")
    private String gender;

    @Column(name = "image_url")
    @ApiModelProperty(value = "image路径")
    private String imageUrl;

    @Column(name = "openid")
    @ApiModelProperty(value = "微信Openid")
    private String openId;

    @Column(name = "login_type")
    @ApiModelProperty(value = "登录类型")
    private Integer loginType;

    @Column(name = "login_time")
    @ApiModelProperty(value = "登录时间")
    private Long loginTime;

    @Column(name = "login_ip")
    @ApiModelProperty(value = "登录ip")
    private String loginIp;

    @Column(name = "login_agent")
    @ApiModelProperty(value = "客户端代理")
    private String loginAgent;

    @Column(name = "activate")
    @ApiModelProperty(value = "用户启用状态")
    private Integer activate;

    @Column(name = "order_protocol")
    @ApiModelProperty(value = "新增个人工单是否同意协议")
    private Integer orderProtocol;

    @Column(name = "price_protocol")
    @ApiModelProperty(value = "是否同意确认报价协议")
    private Integer priceProtocol;

    @Column(name = "cpid")
    @ApiModelProperty(value = "cpid")
    private String cpid;

    @Column(name = "did")
    @ApiModelProperty(value = "did")
    private String did;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "did", referencedColumnName = "did", insertable = false, updatable = false)
    private Department department;

    @Column(name = "cid")
    @ApiModelProperty(value = "cid")
    private String cid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cid", referencedColumnName = "cid", insertable = false, updatable = false)
    private Customer customer;

    @Column(name = "ucid")
    @ApiModelProperty(value = "ucid")
    private String ucid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cpid", referencedColumnName = "cpid", insertable = false, updatable = false)
    private Company company;
}
