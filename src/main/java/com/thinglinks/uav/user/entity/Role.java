package com.thinglinks.uav.user.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 16:21
 */
@Entity
@Table(name = "roles")
@Data
@ToString(exclude = {"users"})
@EqualsAndHashCode(exclude= {"users"}, callSuper = false)
public class Role extends BaseEntity {
    /**
     * 角色名称
     */
    @Column(name="name")
    @ApiModelProperty(value = "角色名称")
    private String name;

    /**
     * 角色编码
     */
    @Column(name="code")
    @ApiModelProperty(value = "角色编码")
    private String code;

    /**
     * 角色备注
     */
    @Column(name="remark")
    @ApiModelProperty(value = "角色备注")
    private String remark;

    /**
     * 角色状态 0:禁用 1:启用
     */
    @Column(name="status")
    @ApiModelProperty(value = "启用状态")
    private Integer status;

    /**
     * 角色状态 0:非系统角色 1:系统角色
     */
    @Column(name="is_system")
    @ApiModelProperty(value = "是否为系统角色")
    private Integer isSystem;

    /**
     * cpid
     */
    @Column(name="cpid")
    @ApiModelProperty(value = "cpid")
    private String cpid;

    // @ManyToMany(mappedBy = "roles",fetch = FetchType.EAGER)
    // @LazyCollection(LazyCollectionOption.EXTRA)
    // private List<User> users;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_permissions",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id")

    )
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<Permission> permissions;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_menus",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "menu_id")
    )
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<Menu> menus;
}
