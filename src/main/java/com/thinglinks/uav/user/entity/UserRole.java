package com.thinglinks.uav.user.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/22 21:57
 */
@Data
@Entity
@Table(name = "user_roles")
public class UserRole extends BaseEntity {

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "role_id")
    private Integer roleId;
}
