package com.thinglinks.uav.user.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/19 13:08
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "permissions")
@Data
public class Permission extends BaseEntity {
    @Column(name = "name")
    @ApiModelProperty(value = "权限名称")
    private String name;

    @Column(name = "code")
    @ApiModelProperty(value = "权限编码")
    private String code;

    @Column(name = "enable")
    @ApiModelProperty(value = "权限启用状态 false:禁用 true:启用")
    private boolean enable;

    @Column(name = "menu_id")
    @ApiModelProperty(value = "菜单id")
    private Integer menuId;

    // @ManyToMany(mappedBy = "permissions",fetch = FetchType.EAGER)
    // @LazyCollection(LazyCollectionOption.EXTRA)
    // private List<Role> roles;
}
