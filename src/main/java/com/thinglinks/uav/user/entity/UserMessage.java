package com.thinglinks.uav.user.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.message.entity.Message;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * (UserMessages)实体类
 *
 * @author iwiFool
 * @since 2024-03-20 20:31:43
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "user_messages")
public class UserMessage extends BaseEntity {

	/**
	 * 消息处理状态0:未读 1:已读
	 */
	private String status;
	/**
	 * 告警处理时间
	 */
	private Long handleAt;
	/**
	 * 用户ID外键
	 */
	private String uid;

	/**
	 * 消息ID外键
	 */
	private String msgid;

	private Long messageId;

	private Long userId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "msgid", referencedColumnName = "msgid", insertable = false, updatable = false)
	private Message message;
}
