package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author fanhaiqiu
 * @date 2022/1/28
 */
@Data
public class LoginResponse {

    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "用户账号")
    private String username;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "用户角色")
    private List<String> roles;
}
