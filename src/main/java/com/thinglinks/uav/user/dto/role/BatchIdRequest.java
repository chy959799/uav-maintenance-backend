package com.thinglinks.uav.user.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/18 10:24
 */
@Data
public class BatchIdRequest {
    @ApiModelProperty(value = "ids",required = true)
    @NotNull
    private List<Integer> ids;

    @ApiModelProperty(value = "角色状态",required = true)
    @NotNull
    private Integer status;
}
