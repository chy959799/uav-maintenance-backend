package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 15:03
 */
@Data
public class AddUserRequest {

    @ApiModelProperty(value = "用户名",required = true)
    @NotNull
    private String userName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "电话")
    @NotNull
    private String mobile;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "角色")
    private List<Integer> roles;

    @ApiModelProperty(value = "部门")
    private String did;

}
