package com.thinglinks.uav.user.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/18 9:41
 */
@Data
public class RoleRequest {
    @ApiModelProperty(value = "角色名称",required = true)
    @NotNull
    private String name;

    @ApiModelProperty(value = "角色编码",required = true)
    @NotNull
    private String code;

    @ApiModelProperty(value = "菜单权限")
    private List<Integer> menus;

    @ApiModelProperty(value = "操作权限")
    private List<Integer> permissions;

    @ApiModelProperty(value = "角色备注")
    private String remark;
}
