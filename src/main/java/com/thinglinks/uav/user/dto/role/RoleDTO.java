package com.thinglinks.uav.user.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/19 11:13
 */
@Data
public class RoleDTO {
    @ApiModelProperty(value = "角色ID")
    private Integer id;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "角色编码")
    private String code;

    @ApiModelProperty(value = "角色备注")
    private String remark;

    @ApiModelProperty(value = "角色状态，0:禁用 1:启用")
    private Integer status;

    @ApiModelProperty(value = "cpid")
    private String cpid;

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "更新时间")
    private String updatedAt;
}

