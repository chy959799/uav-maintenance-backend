package com.thinglinks.uav.user.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 13:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserPageQuery extends BasePageRequest {
    @ApiModelProperty(value = "所属部门")
    private String did;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "联系电话")
    private String mobile;
}
