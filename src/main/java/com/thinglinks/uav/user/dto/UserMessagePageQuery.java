package com.thinglinks.uav.user.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserMessagePageQuery extends BasePageRequest {

	@ApiModelProperty(value = "状态：1-已读, 0-未读")
	private String status;

	@ApiModelProperty(value = "类型：1-普通消息，2-工单消息")
	private String type;
}
