package com.thinglinks.uav.user.dto.role;


import com.thinglinks.uav.user.dto.UserDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/19 15:26
 */
@Data
public class RoleUserDTO {

    @ApiModelProperty(value = "用户ID")
    private Integer userId;

    @ApiModelProperty(value = "角色ID")
    private Integer roleId;

    @ApiModelProperty(value = "角色对应用户信息")
    private UserDTO user;
}
