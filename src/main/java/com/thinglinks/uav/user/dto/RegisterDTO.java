package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author iwiFool
 * @date 2024/3/17
 */
@Data
@ApiModel(description = "注册用户信息")
public class RegisterDTO {

	@ApiModelProperty(value = "cid")
	@NotBlank(message = "单位为空")
	private String cid;

	@NotBlank(message = "用户名不能为空")
	@ApiModelProperty(value = "用户名")
	private String userName;

	@ApiModelProperty(value = "联系电话")
	@NotBlank(message = "手机号码不能为空")
	@Length(min = 11, max = 11, message = "手机号只能为11位")
	@Pattern(regexp = "^1[3-9]\\d{9}$", message = "手机号格式有误")
	private String mobile;

	@ApiModelProperty(value = "验证码")
	@NotBlank(message = "验证码不能为空")
	private String captcha;

	@ApiModelProperty(value = "密码")
	@NotBlank(message = "密码不能为空")
	private String password;
}
