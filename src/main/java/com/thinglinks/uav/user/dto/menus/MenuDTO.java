package com.thinglinks.uav.user.dto.menus;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/25 9:38
 */
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class MenuDTO extends MenuNoRoleDTO {

    @ApiModelProperty(value = "父菜单")
    @JsonManagedReference
    private MenuNoRoleDTO parent;

    @ApiModelProperty(value = "菜单权限列表")
    private List<PermissionDTO> permissions;

    @ApiModelProperty(value = "菜单权限数量")
    private int permissionCount;

    @ApiModelProperty(value = "子菜单")
    @JsonManagedReference
    private List<MenuDTO> children;

    @ApiModelProperty(value = "子菜单数")
    private int count;
}
