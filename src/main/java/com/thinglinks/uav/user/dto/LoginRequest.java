package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
@Data
@NoArgsConstructor
public class LoginRequest {

    @ApiModelProperty(value = "用户名", example = "admin")
    @NotBlank
    private String username;

    @ApiModelProperty(value = "密码", example = "123456(传出为密文：02UDNzITM)")
    @NotBlank
    private String password;

    @ApiModelProperty(value = "验证码", example = "123456")
    @NotBlank
    private String captcha;
}
