package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author iwiFool
 * @date 2024/3/18
 */
@Data
public class UpdatePasswordRequest {

	@ApiModelProperty(value = "旧密码")
	@NotBlank
	private String oldPassword;

	@ApiModelProperty(value = "新密码")
	@NotBlank
	private String newPassword;
}
