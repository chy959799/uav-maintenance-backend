package com.thinglinks.uav.user.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/18 21:08
 */
@Data
public class UnBindRequest {
    @ApiModelProperty(value = "角色id",required = true)
    @NotNull
    private Integer role;

    @ApiModelProperty(value = "用户id",required = true)
    @NotNull
    private Integer user;
}
