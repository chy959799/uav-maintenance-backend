package com.thinglinks.uav.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.user.dto.role.RolePage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 13:50
 */
@Data
public class UserPage {
    @ApiModelProperty(value = "uid")
    private String uid;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "电话")
    private String mobile;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "头像")
    @JsonProperty(value = "avatar")
    private String imageUrl;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "微信OpenId")
    private String openid;

    @ApiModelProperty(value = "角色")
    private String role;

    @ApiModelProperty(value = "用户类型")
    @JsonProperty(value = "type")
    private Integer userType;

    @ApiModelProperty(value = "登录类型 1:账号密码,2:微信授权")
    private Integer loginType;

    @ApiModelProperty(value = "登录时间")
    private String loginTime;

    @ApiModelProperty(value = "登录IP")
    private String loginIp;

    @ApiModelProperty(value = "客户端代理")
    private String loginAgent;

    @ApiModelProperty(value =  "用户状态 1:正常,2:禁用")
    private boolean activate;

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value =  "更新时间")
    private String updatedAt;

    @ApiModelProperty(value = "cpid")
    private String cpid;

    @ApiModelProperty(value = "did")
    private String did;

    @ApiModelProperty(value = "cid")
    private String cid;

    @ApiModelProperty(value = "ucid")
    private String ucid;

    @ApiModelProperty(value = "用户所属单位")
    private String customerCorporation;

    @ApiModelProperty(value = "用户角色信息")
    private List<RolePage> roles;
}
