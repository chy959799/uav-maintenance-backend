package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 10:15
 */
@Data
public class ActivateRequest {
    @ApiModelProperty(value = "用户状态",required = true)
    @NotNull
    private boolean activate;
}
