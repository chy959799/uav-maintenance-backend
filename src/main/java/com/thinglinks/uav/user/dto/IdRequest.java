package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 12:51
 */
@Data
public class IdRequest {
    @ApiModelProperty(value = "uid", required = true)
    @NotNull
    private List<String> ids;
}
