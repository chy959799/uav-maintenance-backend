package com.thinglinks.uav.user.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 21:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePage {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "角色码")
    private String code;

    @ApiModelProperty(value = "角色备注")
    private String remark;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    private String createdAt;

    @ApiModelProperty(value = "创建时间")
    private String updatedAt;

    @ApiModelProperty(value = "是否系统角色")
    private Integer isSystem;

    @ApiModelProperty(value = "cpid")
    private String cpid;

}
