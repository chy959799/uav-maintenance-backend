package com.thinglinks.uav.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author iwiFool
 * @date 2024/3/29
 */
@Data
@ApiModel(description = "用户更新请求")
public class UpdateUserRequest {

	@Email
	@ApiModelProperty(value = "Email")
	private String email;

	@ApiModelProperty(value = "昵称")
	private String nickName;

	@ApiModelProperty(value = "电话")
	@NotBlank(message = "手机号码不能为空")
	@Length(min = 11, max = 11, message = "手机号只能为11位")
	@Pattern(regexp = "^1[3-9]\\d{9}$", message = "手机号格式有误")
	private String mobile;

	@ApiModelProperty(value = "性别[1-男，2-女]")
	@Pattern(regexp = "^[1-2]$", message = "性别只能为1或2")
	private String gender;
}
