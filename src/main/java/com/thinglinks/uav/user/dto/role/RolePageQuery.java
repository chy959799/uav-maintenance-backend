package com.thinglinks.uav.user.dto.role;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 21:39
 */
@Data
public class RolePageQuery extends BasePageRequest {

    @ApiModelProperty(value = "角色名称")
    private String name;
}
