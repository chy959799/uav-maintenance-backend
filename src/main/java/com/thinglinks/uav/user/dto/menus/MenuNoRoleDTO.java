package com.thinglinks.uav.user.dto.menus;

import com.thinglinks.uav.common.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/25 17:22
 */
@Data
public class MenuNoRoleDTO extends BaseEntity {
    @Column(name = "name")
    @ApiModelProperty(value = "菜单名称,首字母大写,一定要与vue文件export的name对应起来,name名不可重复,用于noKeepAlive缓存控制")
    private String name;

    @Column(name = "path")
    @ApiModelProperty(value = "菜单路径,注意根路由是斜线,第一级路由必须带斜线,第二级路由开始不能带斜线,并且path名不可重复")
    private String path;

    @Column(name = "component")
    @ApiModelProperty(value = "页面组件,第一级路由是为Layout,其余为层级为vue的文件路径")
    private String component;

    @Column(name = "redirect")
    @ApiModelProperty(value = "重定向到子路由，格式从第一级路由开始拼接")
    private String redirect;

    @Column(name = "title")
    @ApiModelProperty(value = "菜单、面包屑、多标签页显示的名称")
    private String title;

    @Column(name = "icon")
    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @Column(name = "hidden")
    @ApiModelProperty(value = "是否显示在菜单中显示隐藏路由")
    private boolean hidden;

    @Column(name = "no_closable")
    @ApiModelProperty(value = "当前路由是否可关闭多标签Tab页")
    private boolean noClosable;

    @Column(name = "no_keep_alive")
    @ApiModelProperty(value = "当前路由是否不缓存")
    private boolean noKeepAlive;

    @Column(name = "sort")
    @ApiModelProperty(value = "菜单顺序")
    private int sort;

    @Column(name = "enable")
    @ApiModelProperty(value = "菜单启用状态 false:禁用 true:启用")
    private boolean enable;

    @Column(name = "is_system")
    @ApiModelProperty(value = "是否为系统菜单")
    private boolean isSystem;

    @Column(name = "cpid")
    @ApiModelProperty(value = "单位id")
    private String cpid;

    @Column(name = "pid")
    @ApiModelProperty(value = "pid")
    private Integer pid;
}
