package com.thinglinks.uav.user.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.user.dto.UserMessagePageQuery;
import com.thinglinks.uav.user.service.UserMessageService;
import com.thinglinks.uav.user.vo.UserMessageVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (UserMessages)表控制层
 *
 * @author iwiFool
 * @since 2024-03-20 20:31:42
 */
@Api(tags = "消息管理")
@RestController
@RequestMapping("userMessage")
public class UserMessagesController {

	@Resource
	private UserMessageService userMessageService;

	@Operation(summary = "获取未读消息数")
	@GetMapping("unread")
	public BaseResponse<Long> getUnread() {
		return userMessageService.getUnread();
	}

	@Operation(summary = "获取消息列表")
	@GetMapping
	public BasePageResponse<UserMessageVO> getUserMessagePage(UserMessagePageQuery pageQuery) {
		return userMessageService.getUserMessagePage(pageQuery);
	}

	@Operation(summary = "标记已读")
	@PostMapping("status")
	public BaseResponse<String> updateStatus(@RequestBody DeleteDTO dto) {
		return userMessageService.updateStatus(dto);
	}

	@Operation(summary = "删除消息")
	@DeleteMapping
	public BaseResponse<String> delete(@RequestBody DeleteDTO dto) {
		return userMessageService.delete(dto);
	}

}


