package com.thinglinks.uav.user.controller;

import cn.hutool.captcha.AbstractCaptcha;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.StringEncryptionUtil;
import com.thinglinks.uav.user.dto.LoginRequest;
import com.thinglinks.uav.user.dto.LoginResponse;
import com.thinglinks.uav.user.dto.RegisterDTO;
import com.thinglinks.uav.user.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;

@Api(tags = "鉴权")
@RestController
@RequestMapping
@Slf4j
public class AuthController {

    @Resource
    private AuthService authService;

    @Operation(summary = "登录")
    @PostMapping("/login")
    public BaseResponse<LoginResponse> login(@RequestBody @Valid LoginRequest loginRequest, HttpServletRequest httpServletRequest){
        String clientIp = CommonUtils.getClientIp(httpServletRequest);
        String userAgent = httpServletRequest.getHeader("User-Agent");
        return authService.login(loginRequest, clientIp, userAgent);
    }

    @Operation(summary = "注销")
    @GetMapping("/logout")
    public BaseResponse<String> logout() {
        return authService.logout();
    }

    @Operation(summary = "获取图形验证码")
    @GetMapping("captchas/create")
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response) {
        String clientIp = CommonUtils.getClientIp(request);
        AbstractCaptcha captcha = authService.generateCaptcha(clientIp);

        try (OutputStream outputStream = response.getOutputStream()) {
            // 创建验证码图片并写入响应流
            response.setContentType("image/png");
            captcha.write(outputStream);
        } catch (IOException e) {
            log.error("生成验证码失败", e);
        }
    }

    @Operation(summary = "获取手机验证码")
    @GetMapping("captchas/mobile")
    public BaseResponse<String> getMobileCaptcha(@RequestParam("mobile") String mobile) throws Exception {
        return authService.getMobileCaptcha(mobile);
    }

    @Operation(summary = "用户注册")
    @PostMapping(value = "register")
    public BaseResponse<LoginResponse> register(@RequestBody @Valid RegisterDTO dto, HttpServletRequest httpServletRequest) {
        String clientIp = CommonUtils.getClientIp(httpServletRequest);
        String userAgent = httpServletRequest.getHeader("User-Agent");
        String decode = StringEncryptionUtil.decode(dto.getPassword());
        // 验证密码是否包含大小写字母和特殊字符
        if (!StringEncryptionUtil.validate(decode)) {
            return BaseResponse.fail("要求包含大小写字母和特殊字符");
        }
        return authService.register(dto, clientIp, userAgent);
    }

}
