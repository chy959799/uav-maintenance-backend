package com.thinglinks.uav.user.controller;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.dto.menus.MenuDTO;
import com.thinglinks.uav.user.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/25 13:03
 */
@Api(tags = "菜单管理")
@RestController
@RequestMapping(value = "menus")
@Slf4j
public class MenuController {
    @Resource
    private MenuService menuService;

    @Operation(summary = "获取菜单树")
    @GetMapping(path = "tree")
    public BaseResponse<List<MenuDTO>> getMenuTree(@RequestParam("hidden")  boolean hidden) {
        return menuService.getMenuTree(hidden);
    }


}
