package com.thinglinks.uav.user.controller;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.service.RouterService;
import com.thinglinks.uav.user.vo.RouterVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/5/20
 */
@Api(tags = "路由管理")
@RestController
@RequestMapping(value = "routers")
public class RouterController {

    @Resource
    private RouterService routerService;

    @Operation(summary = "获取路由")
    @GetMapping
    public BaseResponse<List<RouterVO>> getRouter() {
        return routerService.getRouter();
    }
}
