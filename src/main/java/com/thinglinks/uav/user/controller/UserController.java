package com.thinglinks.uav.user.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.system.annotation.Authority;
import com.thinglinks.uav.user.dto.*;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.service.UserService;
import com.thinglinks.uav.user.vo.UserDetailVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 0:20
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping(value = "users")
@Slf4j
public class UserController {

    @Resource
    private UserService userService;


    @Authority(action = "userManagementAdd")
    @Operation(summary = "新增用户")
    @PostMapping
    public BaseResponse<Void> addUser(@RequestBody @Valid AddUserRequest request) {
        return userService.addUser(request);
    }

    @Authority(action = "userManagementDelete")
    @Operation(summary = "删除用户")
    @DeleteMapping(path = "{uid}")
    public BaseResponse<Void> delUser(@PathVariable("uid") String uid) {
        return userService.delUser(uid);
    }

    @Authority(action = "userManagementDelete")
    @Operation(summary = "批量删除用户")
    @DeleteMapping
    public BaseResponse<Void> delUser(@RequestBody @Valid IdRequest request) {
        return userService.delUsers(request);
    }

    @Authority(action = "userManagementEnable")
    @Operation(summary = "修改用户启用状态")
    @PatchMapping(path = "{uid}/enable")
    public BaseResponse<Void> editUserActivate(@PathVariable("uid") String uid,@RequestBody ActivateRequest activateRequest) {
        return userService.editUserActivate(uid,activateRequest);
    }

    @Authority(action = "userManagementEdit")
    @Operation(summary = "编辑用户信息")
    @PatchMapping(path = "{uid}")
    public BaseResponse<Void> editUser(@PathVariable("uid") String uid,@RequestBody @Valid EditUserRequest request) {
        return userService.editUser(uid,request);
    }

    @Operation(summary = "分页条件查询用户")
    @GetMapping
    public BasePageResponse<UserPage> userPage(UserPageQuery request) {
        return userService.page(request);
    }

    @Authority(action = "userManagementReset")
    @Operation(summary = "用户重置密码")
    @PatchMapping(path = "{uid}/reset/password")
    public BaseResponse<Void> resetPassword(@PathVariable("uid") String uid) {
        return userService.resetPassword(uid);
    }

    @Operation(summary = "获取指定用户信息")
    @GetMapping("{uid}")
    public BaseResponse<User> user(@PathVariable("uid") String uid){
        return userService.getUser(uid);
    }

    @Operation(summary = "个人用户获取详情")
    @GetMapping(path = "detail")
    public BaseResponse<UserDetailVO> getUserDetail() {
        return userService.getUserDetail();
    }

    @Authority(action = "personalCenterEdit")
    @Operation(summary = "修改个人信息")
    @PatchMapping(path = "update")
    public BaseResponse<UserDetailVO> updateUser(@RequestBody @Valid UpdateUserRequest request) {
        return userService.updateUser(request);
    }

    @Operation(summary = "新增个人工单协议确认")
    @PatchMapping(path = "orderProtocolValidation")
    public BaseResponse<String> orderProtocolValidation() {
        return userService.orderProtocolValidation();
    }

    @Operation(summary = "报价单协议确认")
    @PatchMapping(path = "priceProtocolValidation")
    public BaseResponse<String> priceProtocolValidation() {
        return userService.priceProtocolValidation();
    }

    @Authority(action = "personalCenterResetPaswd")
    @Operation(summary = "修改个人密码")
    @PatchMapping(path = "update/password")
    public BaseResponse<String> updatePassword(@RequestBody @Valid UpdatePasswordRequest request) {
        return userService.updatePassword(request);
    }

    @Authority(action = "personalCenterEdit")
    @Operation(summary = "上传用户头像")
    @PostMapping(path = "/avatar")
    public BaseResponse<String> uploadAvatar(@RequestPart("file") MultipartFile file) {
        return userService.uploadAvatar(file);
    }
}
