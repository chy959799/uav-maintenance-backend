package com.thinglinks.uav.user.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.system.annotation.Authority;
import com.thinglinks.uav.user.dto.role.*;
import com.thinglinks.uav.user.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 21:06
 */
@Api(tags = "角色管理")
@RestController
@RequestMapping(value = "roles")
@Slf4j
public class RoleController {

    @Resource
    private RoleService roleService;

    @Authority(action = "roleManagementAdd")
    @Operation(summary = "新建角色")
    @PostMapping
    public BaseResponse<RoleDTO> addRole(@RequestBody @Valid RoleRequest roleRequest){
        return roleService.addRole(roleRequest);
    }

    @Authority(action = "roleManagementDelete")
    @Operation(summary = "删除角色")
    @DeleteMapping(path = "{ID}")
    public BaseResponse<Void> delRole(@PathVariable("ID") Integer id){
        return roleService.delRole(id);
    }

    @Authority(action = "roleManagementDelete")
    @Operation(summary = "批量删除角色")
    @DeleteMapping
    public BaseResponse<Void> delRoles(@RequestBody Map<String, List<Integer>> request){
        return roleService.delRoles(request.get("ids"));
    }

    @Authority(action = "roleManagementEnable")
    @Operation(summary = "修改角色状态")
    @PatchMapping(path = "{id}/enable")
    public BaseResponse<Void> enable(@PathVariable("id") Integer id,@RequestBody Map<String, Integer> request) {
        return roleService.editRoleStatus(id,request.get("status"));
    }

    @Authority(action = "roleManagementEnable")
    @Operation(summary = "批量修改角色状态")
    @PatchMapping
    public BaseResponse<Void> batchEnable(@RequestBody @Valid BatchIdRequest batchIdRequest) {
        return roleService.editRolesStatus(batchIdRequest);
    }

    @Authority(action = "roleManagementEdit")
    @Operation(summary = "角色信息更改")
    @PatchMapping(path = "{id}")
    public BaseResponse<Void> editRole(@PathVariable("id") Integer id,@RequestBody @Valid RoleRequest roleRequest) {
        return roleService.editRole(id,roleRequest);
    }

    @Operation(summary = "分页条件查询角色")
    @GetMapping
    public BasePageResponse<RolePage> rolePage(RolePageQuery request) {
        return roleService.page(request);
    }

    @Authority(action = "roleUserManagement")
    @Operation(summary = "角色解绑用户")
    @PostMapping(path = "unbind")
    public BaseResponse<Void> unbindUser(@RequestBody @Valid UnBindRequest unBindRequest) {
        return roleService.unbindUser(unBindRequest);
    }

    @Operation(summary = "角色用户列表")
    @GetMapping(path = "{id}/users")
    public BasePageResponse<RoleUserDTO> userPage(@PathVariable("id") Integer id) {
        return roleService.getUser(id);
    }

    @Operation(summary = "角色菜单列表")
    @GetMapping(path = "{id}/menus")
    public BaseResponse<List<Integer>> roleMenus(@PathVariable("id") Integer id) {
        return roleService.roleMenus(id);
    }

    @Operation(summary = "角色权限列表")
    @GetMapping(path = "{id}/permissions")
    public BaseResponse<List<Integer>> rolePermissions(@PathVariable("id") Integer id) {
        return roleService.rolePermissions(id);
    }

}
