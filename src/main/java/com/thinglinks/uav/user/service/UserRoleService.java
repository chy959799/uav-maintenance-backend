package com.thinglinks.uav.user.service;

import com.thinglinks.uav.user.entity.User;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/7/14
 */
public interface UserRoleService {

    void modifyUserRoles(User user, List<Integer> roles);
}
