package com.thinglinks.uav.user.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.RoleEnum;
import com.thinglinks.uav.user.dto.role.*;
import com.thinglinks.uav.user.entity.Role;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author huiyongchen
 */
public interface RoleService {
    BasePageResponse<RolePage> page(RolePageQuery request);

    BaseResponse<Void> editRoleStatus(Integer id, Integer status);

    BaseResponse<Void> delRole(Integer id);

    BaseResponse<Void> delRoles(List<Integer> ids);

    BaseResponse<RoleDTO> addRole(RoleRequest roleRequest);

    BaseResponse<Void> editRolesStatus(BatchIdRequest batchIdRequest);

    BaseResponse<Void> editRole(Integer id, RoleRequest roleRequest);

    @Transactional
    BaseResponse<Void> unbindUser(UnBindRequest unBindRequest);

    BasePageResponse<RoleUserDTO> getUser(Integer id);

    BaseResponse<List<Integer>> roleMenus(Integer id);

    BaseResponse<List<Integer>> rolePermissions(Integer id);

    @Transactional
    Role getRoleById(Integer roleId);

    List<Role> getAllByIdIn(List<Integer> roleIds);

    /**
     * 根据默认角色获取角色，如果没有就创建
     *
     * @param defaultRole 默认角色
     * @return 角色
     */
    Role getDefaultRoleByCode(RoleEnum defaultRole);
}
