package com.thinglinks.uav.user.service.impl;

import cn.hutool.captcha.AbstractCaptcha;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.core.util.RandomUtil;
import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.UserConstants;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.RoleEnum;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.sms.AliYunSmsService;
import com.thinglinks.uav.common.sms.DefaultMobileCaptchaException;
import com.thinglinks.uav.common.utils.CaptchaMathGenerator;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.JwtUtils;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.system.exception.CustomBizException;
import com.thinglinks.uav.user.dto.LoginRequest;
import com.thinglinks.uav.user.dto.LoginResponse;
import com.thinglinks.uav.user.dto.RegisterDTO;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.entity.UserRole;
import com.thinglinks.uav.user.repository.UserRepository;
import com.thinglinks.uav.user.repository.UserRoleRepository;
import com.thinglinks.uav.user.service.AuthService;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author: fanhaiqiu
 * @date: 2023/3/16
 */
@Service
public class AuthServiceImpl implements AuthService {

    @Resource
    private UserRepository userRepository;

	@Resource
	private CustomersRepository customersRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private AliYunSmsService smsService;

    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Resource
    private UserRoleRepository userRoleRepository;

    @Resource
    private RoleServiceImpl roleService;

    @Override
    public AbstractCaptcha generateCaptcha(String clientIp) {
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(120, 40, 4, 0);
        captcha.setGenerator(new CaptchaMathGenerator(1));
        String code = captcha.getCode();
        customRedisTemplate.setCaptcha(code, clientIp);
        return captcha;
    }

    @Override
    public BaseResponse<LoginResponse> login(LoginRequest request, String clientIp, String userAgent) {

        String cacheCode = customRedisTemplate.getCaptcha(clientIp);
        boolean verify = new CaptchaMathGenerator().verify(cacheCode, request.getCaptcha());
        if (!verify) {
            throw new BusinessException("图形验证码错误");
        }

        User user = userRepository.findByUserName(request.getUsername());
        if (Objects.isNull(user)) {
            throw new BusinessException("登录失败,账号名或密码错误");
        }

        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new BusinessException("登录失败,账号名或密码错误");
        }

        List<String> rolesCode = getUserRolesCode(user);

        if (!rolesCode.contains(UserConstants.ADMIN_ROLE)) {
            checkUser(user);
        }

        LoginResponse loginResponse = loginSusses(clientIp, userAgent, user, UserConstants.LOGIN_TYPE_PASSWORD, rolesCode);
        customRedisTemplate.deleteCaptcha(clientIp);

        return BaseResponse.success(loginResponse);
    }

    private List<String> getUserRolesCode(User user) {
        List<UserRole> userRoles = userRoleRepository.findByUserId(user.getId());
        List<Integer> roleIds = userRoles.stream().map(UserRole::getRoleId).collect(Collectors.toList());
        List<Role> roles = roleService.getAllByIdIn(roleIds);
        List<String> rolesCode = Collections.emptyList();
        if (!Objects.isNull(roles)) {
            rolesCode = roles.stream().map(Role::getCode).collect(Collectors.toList());
        }
        return rolesCode;
    }

    @Override
    public void checkUser(User user) {
        if (!Objects.equals(user.getActivate(), CommonConstants.TURE_VALUE)) {
            throw new BusinessException("用户未启用");
        }

        Company company = user.getCompany();
        if(Objects.nonNull(company)) {
            if (!Objects.equals(company.getActivate(), CommonConstants.TURE_VALUE)) {
                throw new BusinessException("维修点未启用");
            }

            if (company.getExpiredAt() < System.currentTimeMillis()) {
                throw new BusinessException("维修点已过期");
            }

            if (!customRedisTemplate.isCompanyUserTokenExist(company.getCpid(), user.getUid()) && customRedisTemplate.getOnlineUserCount(company.getCpid()) >= company.getUserCount()) {
                throw new BusinessException("维修点用户已满");
            }
        }
    }

    private LoginResponse loginSusses(String clientIp, String userAgent, User user, Integer loginType, List<String> rolesCode) {
		LoginResponse loginResponse = new LoginResponse();
		BeanUtils.copyProperties(user, loginResponse);
        loginResponse.setRoles(rolesCode);
		loginResponse.setAvatar(user.getImageUrl());
		loginResponse.setUsername(user.getUserName());

		String token = JwtUtils.generateToken(user.getId());
		loginResponse.setToken(token);

        customRedisTemplate.setUserToken(token, user.getUid());
        Company company = user.getCompany();
        if (Objects.nonNull(company)) {
            customRedisTemplate.setCompanyUserToken(token, company.getCpid(), user.getUid());
        }

		user.setLoginIp(clientIp);
		user.setLoginTime(System.currentTimeMillis());
		user.setLoginType(loginType);
		user.setLoginAgent(userAgent);
		userRepository.save(user);
		return loginResponse;
	}

    @Override
    public BaseResponse<String> logout() {
		if (!CommonUtils.isAdmin()) {
			customRedisTemplate.delUserToken(CommonUtils.getUid());
		}
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<String> getMobileCaptcha(String mobile) throws Exception {
        String code = RandomUtil.randomNumbers(4);
        try {
            if (!smsService.sendSms(mobile, code)) {
                return BaseResponse.fail("发送短信失败");
            }
        } catch (DefaultMobileCaptchaException e) {
            code = e.getDefaultMobileCaptcha();
        } catch (CustomBizException e) {
            return BaseResponse.fail(e.getMessage());
        }
        customRedisTemplate.setMobileCaptcha(mobile, code);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<LoginResponse> register(RegisterDTO dto, String clientIp, String userAgent) {
        String mobileCaptcha = customRedisTemplate.getMobileCaptcha(dto.getMobile());
        String code = dto.getCaptcha();
        if (Objects.isNull(mobileCaptcha) || !mobileCaptcha.equals(code)) {
            return BaseResponse.fail("验证码错误");
        }

        if (userRepository.existsByUserName(dto.getUserName())) {
            return BaseResponse.fail("用户名已存在");
        }

		if (userRepository.existsByMobile(dto.getMobile())) {
			return BaseResponse.fail("手机号已存在");
		}

		Customer customer = customersRepository.findByCid(dto.getCid());
		if (Objects.isNull(customer)) {
			return BaseResponse.fail("单位不存在");
		}

        User user = new User();
        user.setCid(dto.getCid());
        user.setUid(CommonUtils.uuid());
        user.setUserName(dto.getUserName());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setMobile(dto.getMobile());
        user.setActivate(CommonConstants.TURE_VALUE);
        user.setUserType(UserTypeEnum.USER.getCode());
        user = userRepository.save(user);

        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());

        Role role = roleService.getDefaultRoleByCode(RoleEnum.User);
        userRole.setRoleId(role.getId());
        userRoleRepository.save(userRole);
        List<String> rolesCode = getUserRolesCode(user);

        LoginResponse loginResponse = loginSusses(clientIp, userAgent, user, UserConstants.LOGIN_TYPE_PASSWORD, rolesCode);
        customRedisTemplate.deleteMobileCaptcha(dto.getMobile());

        return BaseResponse.success(loginResponse);
    }
}
