package com.thinglinks.uav.user.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.user.dto.UserMessagePageQuery;
import com.thinglinks.uav.user.vo.UserMessageVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (UserMessages)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-20 20:31:43
 */
public interface UserMessageService {

	BaseResponse<Long> getUnread();

	BasePageResponse<UserMessageVO> getUserMessagePage(UserMessagePageQuery pageQuery);

	@Transactional
	BaseResponse<String> updateStatus(DeleteDTO dto);

	@Transactional
	BaseResponse<String> delete(DeleteDTO dto);

	@Transactional
	void insert(String msgid, List<String> list);
}


