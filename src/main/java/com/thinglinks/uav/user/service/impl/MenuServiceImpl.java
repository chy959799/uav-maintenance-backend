package com.thinglinks.uav.user.service.impl;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.dto.menus.MenuDTO;
import com.thinglinks.uav.user.dto.menus.PermissionDTO;
import com.thinglinks.uav.user.entity.Menu;
import com.thinglinks.uav.user.repository.MenuRepository;
import com.thinglinks.uav.user.repository.PermissionRepository;
import com.thinglinks.uav.user.service.MenuService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/25 9:12
 */
@Service
public class MenuServiceImpl implements MenuService {
    @Resource
    private MenuRepository menusRepository;

    @Resource
    private PermissionRepository permissionRepository;

    @Override
    public BaseResponse<List<MenuDTO>> getMenuTree(boolean hidden) {
        List<Menu> all = menusRepository.findAllByHiddenOrderBySort(hidden);
        List<MenuDTO> menus;
        menus = all.stream()
                .map(menu -> {
                    MenuDTO menuDTO = new MenuDTO();
                    BeanUtils.copyProperties(menu, menuDTO);
                    List<PermissionDTO> permissionDTOS = permissionRepository.findAllByMenuId(menu.getId()).stream()
                            .map(p -> {
                                PermissionDTO permissionDTO = new PermissionDTO();
                                BeanUtils.copyProperties(p, permissionDTO);
                                return permissionDTO;
                            }).collect(Collectors.toList());
                    menuDTO.setPermissions(permissionDTOS);
                    menuDTO.setPermissionCount(permissionDTOS.size());

                    return menuDTO;
                }).collect(Collectors.toList());

        List<MenuDTO> rootMenu = menus.stream()
                .filter(menu -> menu.getPid() == null).collect(Collectors.toList());

        List<MenuDTO> menusHasParent = menus.stream()
                .filter(menu -> menu.getPid() != null)
                .peek(menu -> menu.setParent(getParentMenu(menu.getPid(), rootMenu))).collect(Collectors.toList());

        for (MenuDTO menu : rootMenu) {
            List<MenuDTO> children = menusHasParent.stream()
                    .filter(cmenu -> Objects.equals(cmenu.getPid(), menu.getId())).collect(Collectors.toList());
            menu.setChildren(children);
            menu.setCount(children.size());
        }

        return BaseResponse.success(rootMenu);
    }

    @Override
    public List<Menu> getAllMenu() {
        return menusRepository.findAll();
    }

    @Override
    public List<Menu> getHiddenMenuListByPidIn(List<Integer> list) {
        return menusRepository.findAllByHiddenAndPidIn(true, list);
    }

    private MenuDTO getParentMenu(Integer pid, List<MenuDTO> menus) {
        return menus.stream().filter(menu -> menu.getId().equals(pid)).findFirst().orElse(null);
    }
}
