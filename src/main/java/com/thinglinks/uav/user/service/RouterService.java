package com.thinglinks.uav.user.service;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.vo.RouterVO;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/5/20
 */
public interface RouterService {

    BaseResponse<List<RouterVO>> getRouter();
}
