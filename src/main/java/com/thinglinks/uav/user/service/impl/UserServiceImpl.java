package com.thinglinks.uav.user.service.impl;

import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.UserConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.common.utils.StringEncryptionUtil;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.repository.CompanyRepository;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.customer.vo.CompanyVO;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.file.entity.CustomFile;
import com.thinglinks.uav.file.repository.FileRepository;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.user.dto.*;
import com.thinglinks.uav.user.dto.role.RolePage;
import com.thinglinks.uav.user.entity.Permission;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.entity.UserRole;
import com.thinglinks.uav.user.repository.PermissionRepository;
import com.thinglinks.uav.user.repository.RoleRepository;
import com.thinglinks.uav.user.repository.UserRepository;
import com.thinglinks.uav.user.repository.UserRoleRepository;
import com.thinglinks.uav.user.service.UserRoleService;
import com.thinglinks.uav.user.service.UserService;
import com.thinglinks.uav.user.vo.UserDetailVO;
import com.thinglinks.uav.workbench.enums.RoleTypeEnums;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Value("${password.default}")
    private String defaultPassword;

    @Resource
    private UserRepository userRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private UserRoleService userRoleService;

    @Resource
    private CustomMinIOClient customMinIOClient;

    @Resource
    private FileRepository fileRepository;

    @Resource
    private UserRoleRepository userRoleRepository;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private CompanyRepository companyRepository;
    @Resource
    private PermissionRepository permissionRepository;

    @Resource
    private CustomersRepository customersRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> delUsers(IdRequest request) {
        List<String> ids = request.getIds();
        for (String id : ids) {
            User user = userRepository.findByUid(id);
            if (Objects.isNull(user)) {
                return BaseResponse.fail("用户不存在");
            }
            if (user.getActivate() == 1) {
                return BaseResponse.fail("用户已激活，无法删除");
            }
            if (deviceRepository.existsByUid(id)) {
                return BaseResponse.fail("用户存在关联信息，无法删除");
            }

            List<UserRole> role = userRoleRepository.findByUserId(user.getId());
            List<Role> roles = roleRepository.findAllByIdIn(role.stream().map(UserRole::getRoleId).collect(Collectors.toList()));
            if(roles.stream().anyMatch(r ->
                    r.getCode().equals(RoleTypeEnums.ADMIN.getCode()) || r.getCode().equals(RoleTypeEnums.ROOT.getCode()))){
                return BaseResponse.fail("系统管理员或超级管理员不能删除无法删除");
            }


            userRepository.delete(user);
        }
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> editUserActivate(String uid, ActivateRequest activateRequest) {
        User user = userRepository.findByUid(uid);
        if (!Objects.isNull(user)) {
            user.setActivate(activateRequest.isActivate() ? 1 : 0);
            userRepository.save(user);
            return BaseResponse.success();
        } else {
            return BaseResponse.fail("用户不存在");
        }
    }

    @Override
    public BasePageResponse<UserPage> page(UserPageQuery request) {
        Specification<User> specification = (root, query, builder) -> {
            Predicate predicate = builder.conjunction();

            if (Objects.nonNull(request.getNickName()) && !request.getNickName().isEmpty()) {
                predicate = builder.and(predicate, builder.like(root.get("nickName"), CommonUtils.assembleLike(request.getNickName())));
            }
            if (Objects.nonNull(request.getUserName()) && !request.getUserName().isEmpty()) {
                predicate = builder.and(predicate, builder.like(root.get("userName"), CommonUtils.assembleLike(request.getUserName())));
            }

            if (Objects.nonNull(request.getMobile()) && !request.getMobile().isEmpty()) {
                predicate = builder.and(predicate, builder.equal(root.get("mobile"), request.getMobile()));
            }

            if (Objects.nonNull(request.getDid()) && !request.getDid().isEmpty()) {
                Join<User, Department> departmentJoin = root.join("department", JoinType.LEFT);

                Predicate didPredicate = builder.equal(departmentJoin.get("did"), request.getDid());
                Predicate pdidPredicate = builder.equal(departmentJoin.get("pdid"), request.getDid());

                Join<Department, Department> parentDepartmentJoin = departmentJoin.join("parent", JoinType.LEFT);
                Predicate recursivePredicate = builder.or(
                        builder.equal(parentDepartmentJoin.get("did"), request.getDid()),
                        builder.equal(parentDepartmentJoin.get("pdid"), request.getDid())
                );

                predicate = builder.and(predicate, builder.or(didPredicate, pdidPredicate, recursivePredicate));
            }

            String cpid = CommonUtils.getCpid();
            List<String> userRoles = CommonUtils.getUserRoles();

            if(cpid != null && !cpid.isEmpty() && !userRoles.contains(RoleTypeEnums.ROOT.getCode())){
                predicate = builder.and(predicate, builder.equal(root.get("cpid"), cpid));
            }

            return predicate;
        };

        Page<User> page = userRepository.findAll(specification, request.of());

        List<UserPage> results = page.stream().map(u -> {
            UserPage data = new UserPage();
            List<RolePage> roles = new ArrayList<>();
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(u, userDTO);
            userDTO.setCreatedAt(DateUtils.formatDateTime(u.getCt()));
            userDTO.setUpdatedAt(DateUtils.formatDateTime(u.getUt()));
            userDTO.setLoginTime(u.getLoginTime() != null ? DateUtils.formatDateTime(u.getLoginTime()) : "");
            BeanUtils.copyProperties(userDTO, data);
            data.setActivate(Objects.equals(userDTO.getActivate(), UserConstants.USER_ACTIVATE));

            // 手动查询角色
            List<UserRole> userRoles = userRoleRepository.findByUserId(u.getId());
            List<Role> role = userRoles.stream().map(x -> roleRepository.getById(x.getRoleId())).collect(Collectors.toList());

            for (Role value : role) {
                RolePage rolePage = new RolePage();
                rolePage.setId(value.getId());
                rolePage.setName(value.getName());
                rolePage.setCode(value.getCode());
                rolePage.setRemark(value.getRemark());
                rolePage.setStatus(value.getStatus());
                rolePage.setCpid(value.getCpid());
                rolePage.setCreatedAt(DateUtils.formatDateTime(value.getCt()));
                rolePage.setUpdatedAt(DateUtils.formatDateTime(value.getUt()));
                roles.add(rolePage);
            }
            data.setRoles(roles);
            // 用户所属单位
            if (Objects.nonNull(u.getCid())) {
                Customer customer = customersRepository.findByCid(u.getCid());
                data.setCustomerCorporation(customer.getName());
            }
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> addUser(AddUserRequest request) {
        if (userRepository.existsByUserName(request.getUserName()) || userRepository.existsByMobile(request.getMobile())) {
            return BaseResponse.fail("用户名或手机号已存在");
        }
        User user = new User();
        user.setUid(CommonUtils.uuid());
        user.setUserName(request.getUserName());
        user.setNickName(request.getNickName());
        user.setMobile(request.getMobile());
        user.setGender(request.getGender());
        user.setEmail(request.getEmail());
        if (StringUtils.isNotBlank(request.getDid())) {
            user.setDid(request.getDid());
        }
        if(CommonUtils.getCpid() != null && !CommonUtils.getCpid().isEmpty()){
            user.setCpid(CommonUtils.getCpid());
        }
        if (CommonUtils.getCid() != null && !CommonUtils.getCid().isEmpty()) {
            user.setCid(CommonUtils.getCid());
        }
        user.setActivate(UserConstants.USER_ACTIVATE);
        user.setUserType(UserTypeEnum.ADMIN.getCode());
        user.setPassword(passwordEncoder.encode(StringEncryptionUtil.encode(defaultPassword)));
        userRepository.save(user);
        if (request.getRoles().isEmpty()) {
            Role role = roleRepository.findByCode(RoleTypeEnums.Users.getCode());
            request.getRoles().add(role.getId());
        }
        // 手动维护用户角色关系
        List<Integer> roles = request.getRoles();
        for (Integer r : roles) {
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getId());
            userRole.setRoleId(r);
            userRoleRepository.save(userRole);
        }
        return BaseResponse.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> editUser(String uid, EditUserRequest request) {
        User user = userRepository.findByUid(uid);
        if (!Objects.isNull(user)) {
            if (StringUtils.isNotBlank(request.getNickName())) {
                user.setNickName(request.getNickName());
            }
            if (StringUtils.isNotBlank(request.getGender())) {
                user.setGender(request.getGender());
            }
            if (StringUtils.isNotBlank(request.getMobile()) &&(Objects.nonNull(user.getMobile()) && !user.getMobile().equals(request.getMobile()))) {
                if (userRepository.existsByMobile(request.getMobile())) {
                    return BaseResponse.fail("电话号码重复");
                }
                user.setMobile(request.getMobile());
            }
            if (StringUtils.isNotBlank(request.getDid())) {
                user.setDid(request.getDid());
            }
            if (StringUtils.isNotBlank(request.getEmail())) {
                user.setEmail(request.getEmail());
            }
            userRepository.save(user);
            List<Integer> roles = request.getRoles();
            userRoleService.modifyUserRoles(user, roles);
            return BaseResponse.success();
        }
        return BaseResponse.fail("无效的用户ID");
    }

    @Override
    public BaseResponse<Void> resetPassword(String uid) {
        User user = userRepository.findByUid(uid);
        if (!Objects.isNull(user)) {
            String encodePassword = StringEncryptionUtil.encode(defaultPassword);
            user.setPassword(passwordEncoder.encode(encodePassword));
            userRepository.save(user);
            return BaseResponse.success();
        }
        return BaseResponse.fail("用户不存在");
    }

    @Override
    public BaseResponse<UserDetailVO> getUserDetail() {
        User user = userRepository.getById(CommonUtils.getUserId());
        UserDetailVO userDetailVO = coverToDetailVO(user);
        return BaseResponse.success(userDetailVO);
    }

    private UserDetailVO coverToDetailVO(User user) {
        UserDetailVO userDetailVO = new UserDetailVO();
        BeanUtils.copyProperties(user, userDetailVO);
        userDetailVO.setCreatedAt(DateUtils.formatDateTime(user.getCt()));
        userDetailVO.setUpdatedAt(DateUtils.formatDateTime(user.getUt()));
        userDetailVO.setActivate(Objects.equals(user.getActivate(), UserConstants.USER_ACTIVATE));
        userDetailVO.setAvatar(getProxyUrl(user.getImageUrl()));
        userDetailVO.setUsername(user.getUserName());

        List<Role> roles = getUserRoleListByUserId(user.getId());

        List<String> roleCodes = roles.stream().map(Role::getCode).collect(Collectors.toList());
        userDetailVO.setRoles(roleCodes);
        userDetailVO.setRolesName(roles.stream().map(Role::getName).collect(Collectors.joining(",")));

        List<Permission> permissions;
        if (roleCodes.contains(UserConstants.ADMIN_ROLE)) {
            permissions = permissionRepository.findAll();
        } else {
            permissions = roles.stream()
                    .flatMap(role -> role.getPermissions().stream())
                    .collect(Collectors.toList());
        }

        userDetailVO.setPermissions(permissions.stream().map(Permission::getCode).collect(Collectors.toList()));

        Company company = user.getCompany();
        if (Objects.nonNull(company)) {
            CompanyVO companyVO = new CompanyVO();
            BeanUtils.copyProperties(company, companyVO);
            companyVO.setCreatedAt(DateUtils.formatDateTime(company.getCt()));
            userDetailVO.setCompany(companyVO);
        }

        return userDetailVO;
    }

    @Override
    public List<Role> getUserRoleListByUserId(Integer userId) {
        List<UserRole> userRole = userRoleRepository.findByUserId(userId);
        List<Integer> roleIds = userRole.stream().map(UserRole::getRoleId).collect(Collectors.toList());

        return roleRepository.findAllByIdIn(roleIds);
    }

    @Override
    public BaseResponse<String> updatePassword(UpdatePasswordRequest request) {

        String oldPassword = request.getOldPassword();
        String newPassword = request.getNewPassword();

        if (oldPassword.equals(newPassword)) {
            return BaseResponse.fail("新密码不能与旧密码相同");
        }

        User user = userRepository.getById(CommonUtils.getUserId());
        if (passwordEncoder.matches(oldPassword, user.getPassword())) {
            user.setPassword(passwordEncoder.encode(newPassword));
            userRepository.save(user);
            return BaseResponse.success();
        } else {
            return BaseResponse.fail("旧密码错误");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<Void> delUser(String uid) {
        User user = userRepository.findByUid(uid);
        if (!Objects.isNull(user)) {
            if(user.getUid().equals(CommonUtils.getUid())){
                return BaseResponse.fail("不能删除自己");
            }
            List<UserRole> role = userRoleRepository.findByUserId(user.getId());
            List<Role> roles = roleRepository.findAllByIdIn(role.stream().map(UserRole::getRoleId).collect(Collectors.toList()));
            if (roles.stream().anyMatch(r ->
                    r.getCode().equals(RoleTypeEnums.ADMIN.getCode()) ||
                            r.getCode().equals(RoleTypeEnums.ROOT.getCode()))) {
                return BaseResponse.fail("系统管理员或超级管理员无法删除");
            }
            if (!deviceRepository.existsByUid(uid)) {
                userRepository.deleteById(user.getId());
            } else {
                return BaseResponse.fail("用户存关联信息，无法删除");
            }
        } else {
            return BaseResponse.fail("用户不存在");
        }

        return BaseResponse.success();
    }

    @Override
    public BaseResponse<UserDetailVO> updateUser(UpdateUserRequest request) {
        User user = userRepository.getById(CommonUtils.getUserId());
        BeanUtils.copyProperties(request, user);
        userRepository.save(user);
        UserDetailVO userDetailVO = coverToDetailVO(user);
        return BaseResponse.success(userDetailVO);
    }

    @Override
    @Transactional
    public BaseResponse<String> uploadAvatar(MultipartFile file) {
        User user = userRepository.getById(CommonUtils.getUserId());
        CustomFile f = new CustomFile();
        f.setFid(CommonUtils.uuid());
        f.setUid(user.getUid());
        f.setSize(file.getSize());
        f.setName(file.getOriginalFilename());
        f.setExtension(CommonUtils.getExtension(file.getOriginalFilename()));
        f.setDownload(Boolean.TRUE);
        f.setDirectory(UserConstants.USER_BUCKET);
        f.setMime(file.getContentType());
        String objectName = f.getFid().concat(CommonConstants.DOT).concat(f.getExtension());
        try {
            customMinIOClient.uploadFile(UserConstants.USER_BUCKET, objectName, file.getInputStream());
        } catch (Exception e) {
            throw new BusinessException("文件上传失败");
        }
        fileRepository.save(f);
        user.setImageUrl(objectName);
        userRepository.save(user);
        return BaseResponse.success(getProxyUrl(user.getImageUrl()));
    }

    @Override
    public BaseResponse<User> getUser(String uid) {
        return BaseResponse.success(userRepository.findByUid(uid));
    }

    /**
     * @return
     */
    @Override
    public BaseResponse<String> orderProtocolValidation() {
        String uid = CommonUtils.getUid();
        User user = userRepository.findByUid(uid);
        user.setOrderProtocol(1);
        userRepository.save(user);
        return BaseResponse.success();
    }

    /**
     * @return
     */
    @Override
    public BaseResponse<String> priceProtocolValidation() {
        String uid = CommonUtils.getUid();
        User user = userRepository.findByUid(uid);
        user.setPriceProtocol(1);
        userRepository.save(user);
        return BaseResponse.success();
    }

    /**
     * 获取文件的访问地址
     *
     * @param objectName
     * @return
     */
    private String getProxyUrl(String objectName) {
        try {
            if (StringUtils.isBlank(objectName)) {
                return StringUtils.EMPTY;
            }
            return customMinIOClient.getProxyUrl(UserConstants.USER_BUCKET, objectName);
        } catch (Exception e) {
            log.error("getFileProxyUrl error", e);
        }
        return StringUtils.EMPTY;
    }
}
