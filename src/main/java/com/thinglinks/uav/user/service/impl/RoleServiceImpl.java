package com.thinglinks.uav.user.service.impl;

import com.thinglinks.uav.common.constants.RoleConstants;
import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.common.enums.RoleEnum;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.customer.repository.CustomersRepository;
import com.thinglinks.uav.user.dto.UserDTO;
import com.thinglinks.uav.user.dto.role.*;
import com.thinglinks.uav.user.entity.*;
import com.thinglinks.uav.user.repository.*;
import com.thinglinks.uav.user.service.RoleService;
import com.thinglinks.uav.workbench.enums.RoleTypeEnums;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 16:32
 */
@Service
public class RoleServiceImpl implements RoleService {

    private static final int ROLE_ENABLED_STATUS = 1;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private UserRoleRepository userRoleRepository;

    @Resource
    private MenuRepository menusRepository;

    @Resource
    private PermissionRepository permissionsRepository;
    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Resource
    private CustomersRepository customersRepository;

    @Override
    public BasePageResponse<RolePage> page(RolePageQuery request) {
        PageRequest pageRequest = request.of();
        Role role = new Role();
        role.setName(request.getName());
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains());
        Example<Role> example = Example.of(role, matcher);
        Page<Role> page = roleRepository.findAll(example, pageRequest);

        List<RolePage> results = page.stream().map(r -> {
            RolePage data = new RolePage();
            BeanUtils.copyProperties(r, data);
            data.setCreatedAt(DateUtils.formatDateTime(r.getCt()));
            data.setUpdatedAt(DateUtils.formatDateTime(r.getUt()));
            return data;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public BaseResponse<Void> editRoleStatus(Integer id, Integer status) {
        Optional<Role> role = roleRepository.findById(id);
        if (role.isPresent()) {
            Role r = role.get();
            r.setStatus(status);
            roleRepository.save(r);
            return BaseResponse.success();
        }
        return BaseResponse.fail("角色不存在");
    }

    @Override
    public BaseResponse<Void> delRole(Integer id) {
        Optional<Role> role = roleRepository.findById(id);
        if (role.isPresent()) {
            if (RoleTypeEnums.exists(role.get().getCode())){
                return BaseResponse.fail("系统角色不能删除");
            }
            if (role.get().getStatus()==ROLE_ENABLED_STATUS){
                return BaseResponse.fail("该角色已经启用无法删除");
            }
            List<UserRole> byRoleId = userRoleRepository.findByRoleId(id);
            if (CollectionUtils.isNotEmpty(byRoleId)){
                return BaseResponse.fail("该角色下存在用户无法删除");
            }
            roleRepository.deleteById(id);
            return BaseResponse.success();
        }
        return BaseResponse.fail("角色不存在");
    }

    @Override
    public BaseResponse<Void> delRoles(List<Integer> ids) {
        List<Role> allByIdIn = roleRepository.findAllByIdIn(ids);
        if (CollectionUtils.isNotEmpty(allByIdIn)){
            for (Role role : allByIdIn) {
                if (RoleTypeEnums.exists(role.getCode())){
                    return BaseResponse.fail("系统角色不能删除");
                }
                if (role.getStatus()==ROLE_ENABLED_STATUS){
                    return BaseResponse.fail("该角色已经启用无法删除");
                }
                List<UserRole> byRoleId = userRoleRepository.findByRoleId(role.getId());
                if (CollectionUtils.isNotEmpty(byRoleId)){
                    return BaseResponse.fail("该角色下存在用户无法删除");
                }
            }
        }
        roleRepository.deleteAllById(ids);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<RoleDTO> addRole(RoleRequest roleRequest) {
        if (roleRepository.existsByName(roleRequest.getName())) {
            return BaseResponse.fail("角色名称已存在");
        }
        Role role = new Role();
        role.setStatus(1);
        List<Menu> menus = menusRepository.findAllById(roleRequest.getMenus());
        List<Permission> permissions = permissionsRepository.findAllById(roleRequest.getPermissions());
        BeanUtils.copyProperties(roleRequest, role);
        role.setMenus(menus);
        role.setPermissions(permissions);
        Role save = roleRepository.save(role);
        customRedisTemplate.updateRole(role);
        RoleDTO roleDTO = new RoleDTO();
        BeanUtils.copyProperties(save, roleDTO);
        roleDTO.setCreatedAt(DateUtils.formatDateTime(save.getCt()));
        roleDTO.setUpdatedAt(DateUtils.formatDateTime(save.getUt()));
        return BaseResponse.success(roleDTO);
    }

    @Override
    public BaseResponse<Void> editRolesStatus(BatchIdRequest batchIdRequest) {
        roleRepository.batchUpdataStatus(batchIdRequest.getStatus(), batchIdRequest.getIds());
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> editRole(Integer id, RoleRequest roleRequest) {
        Optional<Role> role = roleRepository.findById(id);

        if (role.isPresent()) {
            Role r = role.get();
            if (!r.getName().equals(roleRequest.getName())&&roleRepository.existsByName(roleRequest.getName())) {
                return BaseResponse.fail("角色名称已存在");
            } else {
                r.setName(roleRequest.getName());
            }
            if (!Objects.isNull(r.getCode()) && !r.getCode().equals(roleRequest.getCode())) {
                r.setCode(roleRequest.getCode());
            }

            if (!Objects.isNull(r.getRemark()) && !r.getRemark().equals(roleRequest.getRemark())) {
                r.setRemark(roleRequest.getRemark());
            }

            // 菜单
            List<Menu> menus = menusRepository.findAllById(roleRequest.getMenus());
            r.setMenus(menus);
            // 操作权限
            List<Permission> permissions = permissionsRepository.findAllById(roleRequest.getPermissions());
            r.setPermissions(permissions);
            roleRepository.save(r);
            customRedisTemplate.updateRole(r);
            return BaseResponse.success();
        }
        return BaseResponse.fail("角色信息不存在");
    }

    @Override
    public BaseResponse<Void> unbindUser(UnBindRequest unBindRequest) {
        Optional<User> user = userRepository.findById(unBindRequest.getUser());
        if (user.isPresent()) {
            userRoleRepository.deleteByRoleIdAndUserId(unBindRequest.getRole(), unBindRequest.getUser());
            customRedisTemplate.deleteUserRole(user.get().getUid());
            return BaseResponse.success();
        }
        return BaseResponse.fail("成员不存在");
    }

    @Override
    public BasePageResponse<RoleUserDTO> getUser(Integer id) {
        Optional<Role> role = roleRepository.findById(id);
        if (role.isPresent()) {
            Role r = role.get();
            ArrayList<Integer> ids = new ArrayList<>();
            List<UserRole> userRoles = userRoleRepository.findByRoleId(r.getId());
            userRoles.forEach(x -> ids.add(x.getUserId()));
            BasePageRequest request = new BasePageRequest();
            PageRequest pageRequest = request.of();
            Page<User> page = userRepository.findByIdIn(ids, pageRequest);
            List<RoleUserDTO> result = page.stream().map(u -> {
                RoleUserDTO dto = new RoleUserDTO();
                dto.setRoleId(id);
                dto.setUserId(u.getId());
                UserDTO userDTO = new UserDTO();
                BeanUtils.copyProperties(u, userDTO);
                userDTO.setLoginTime(DateUtils.formatDateTime(u.getLoginTime()));
                userDTO.setCreatedAt(DateUtils.formatDateTime(u.getCt()));
                userDTO.setUpdatedAt(DateUtils.formatDateTime(u.getUt()));
                if(Objects.nonNull(u.getCid())){
                    Customer customer = customersRepository.findByCid(u.getCid());
                    userDTO.setCustomerCorporation(customer.getName());
                }
                dto.setUser(userDTO);
                return dto;
            }).collect(Collectors.toList());
            return BasePageResponse.success(page, result);
        }
        return BasePageResponse.fail("角色不存在");
    }

    @Override
    public BaseResponse<List<Integer>> roleMenus(Integer id) {
        Role role = roleRepository.findById(id).get();
        List<Integer> menus = role.getMenus().stream().map(BaseEntity::getId).collect(Collectors.toList());
        return BaseResponse.success(menus);
    }

    @Override
    public BaseResponse<List<Integer>> rolePermissions(Integer id) {
        Role role = roleRepository.findById(id).get();
        List<Integer> permissions = role.getPermissions().stream().map(BaseEntity::getId).collect(Collectors.toList());
        return BaseResponse.success(permissions);
    }

    @Override
    public Role getRoleById(Integer roleId) {
        Role role = customRedisTemplate.getRole(roleId);
        if (Objects.isNull(role)) {
            role = roleRepository.getById(roleId);
            customRedisTemplate.updateRole(role);
        }
        return role;
    }

    @Override
    public List<Role> getAllByIdIn(List<Integer> roleIds) {
        return roleRepository.findAllByIdIn(roleIds);
    }

    @Override
    public Role getDefaultRoleByCode(RoleEnum defaultRole) {
        Role role = roleRepository.findByCode(defaultRole.getCode());
        if (Objects.isNull(role)) {
            role = createDefaultRole(defaultRole);
            customRedisTemplate.updateRole(role);
        }
        return role;
    }

    private Role createDefaultRole(RoleEnum defaultRole) {
        Role role = new Role();
        role.setCode(defaultRole.getCode());
        role.setName(defaultRole.getName());
        role.setRemark(defaultRole.getRemark());
        role.setStatus(1);

        Menu menu = menusRepository.findByName(RoleConstants.DEFAULT_MENU_NAME);
        if (menu != null) {
            List<Menu> subMenuLists = menusRepository.findAllByPid(menu.getId());
            ArrayList<Menu> menus = new ArrayList<>();
            menus.add(menu);
            menus.addAll(subMenuLists);

            role.setMenus(menus);

            List<Integer> menuIds = menus.stream()
                    .map(BaseEntity::getId)
                    .collect(Collectors.toList());

            if (!menuIds.isEmpty()) {
                List<Permission> permissions = permissionsRepository.findAllByMenuIdIn(menuIds);
                role.setPermissions(permissions);
            }
        }

        return roleRepository.save(role);
    }
}
