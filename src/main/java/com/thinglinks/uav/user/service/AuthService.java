package com.thinglinks.uav.user.service;

import cn.hutool.captcha.AbstractCaptcha;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.dto.LoginRequest;
import com.thinglinks.uav.user.dto.LoginResponse;
import com.thinglinks.uav.user.dto.RegisterDTO;
import com.thinglinks.uav.user.entity.User;

/**
 * @author: fanhaiqiu
 * @date: 2023/3/16
 */
public interface AuthService {

	AbstractCaptcha generateCaptcha(String clientIp);

	BaseResponse<LoginResponse> login(LoginRequest request, String clientIp, String userAgent);

	void checkUser(User user);

	BaseResponse<String> logout();

	BaseResponse<LoginResponse> register(RegisterDTO dto, String clientIp, String userAgent);

	BaseResponse<String> getMobileCaptcha(String mobile) throws Exception;
}
