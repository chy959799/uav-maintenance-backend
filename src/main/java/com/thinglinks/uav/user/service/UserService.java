package com.thinglinks.uav.user.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.dto.*;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.vo.UserDetailVO;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
public interface UserService {

    @Modifying
    BaseResponse<Void> delUsers(IdRequest request);

    BaseResponse<Void> editUserActivate(String uid,ActivateRequest activateRequest);

    BasePageResponse<UserPage> page(UserPageQuery request);

    BaseResponse<Void> addUser(AddUserRequest request);

    BaseResponse<Void> editUser(String uid,EditUserRequest request);

    BaseResponse<Void> resetPassword(String uid);

    BaseResponse<UserDetailVO> getUserDetail();

    List<Role> getUserRoleListByUserId(Integer userId);

    BaseResponse<String> updatePassword(UpdatePasswordRequest request);

    @Modifying
    BaseResponse<Void> delUser(String uid);

    BaseResponse<UserDetailVO> updateUser(UpdateUserRequest request);

	BaseResponse<String> uploadAvatar(MultipartFile file);

    BaseResponse<User> getUser(String uid);

    BaseResponse<String> orderProtocolValidation();

    BaseResponse<String> priceProtocolValidation();
}
