package com.thinglinks.uav.user.service.impl;

import com.thinglinks.uav.common.constants.MessageConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.message.entity.Message;
import com.thinglinks.uav.user.dto.UserMessagePageQuery;
import com.thinglinks.uav.user.entity.UserMessage;
import com.thinglinks.uav.user.repository.UserMessageRepository;
import com.thinglinks.uav.user.service.UserMessageService;
import com.thinglinks.uav.user.vo.MessageVO;
import com.thinglinks.uav.user.vo.UserMessageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (UserMessages)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-20 20:31:43
 */
@Service
public class UserMessageServiceImpl implements UserMessageService {

	@Resource
	private UserMessageRepository userMessageRepository;

	@Override
	public BaseResponse<Long> getUnread() {
		Long count = userMessageRepository.countByStatusAndUid(MessageConstants.STATUS_UNREAD, CommonUtils.getUid());
		return BaseResponse.success(count != null ? count : 0);
	}

	@Override
	public BasePageResponse<UserMessageVO> getUserMessagePage(UserMessagePageQuery pageQuery) {
		Specification<UserMessage> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();

			predicate = builder.and(predicate, builder.equal(root.get("uid"), CommonUtils.getUid()));

			if (!Objects.isNull(pageQuery.getType()) && !pageQuery.getType().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("message").get("type"), pageQuery.getType()));
			}
			if (!Objects.isNull(pageQuery.getStatus()) && !pageQuery.getStatus().isEmpty()) {
				predicate = builder.and(predicate, builder.equal(root.get("status"),pageQuery.getStatus()));
			}
			return predicate;
		};
		// 未读的在上面
		pageQuery.setOrder("status");
		Page<UserMessage> userMessagePage = userMessageRepository.findAll(specification, pageQuery.of());
		List<UserMessageVO> userMessageList = userMessagePage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(userMessagePage, userMessageList);
	}

	@Override
	public BaseResponse<String> updateStatus(DeleteDTO dto) {
		userMessageRepository.updateStatus(dto.getIds(), MessageConstants.STATUS_READ);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> delete(DeleteDTO dto) {
		userMessageRepository.deleteByMsgidIn(dto.getIds());
		return BaseResponse.success();
	}

	@Override
	public void insert(String msgid, List<String> list) {
		ArrayList<UserMessage> userMessageList = new ArrayList<>();
		for (String uid : list) {
			UserMessage userMessage = new UserMessage();
			userMessage.setMsgid(msgid);
			userMessage.setUid(uid);
			userMessage.setStatus(MessageConstants.STATUS_UNREAD);
			userMessageList.add(userMessage);
		}
		userMessageRepository.saveAll(userMessageList);
	}

	private UserMessageVO convertToVO(UserMessage userMessage) {
		UserMessageVO userMessageVO = new UserMessageVO();
		BeanUtils.copyProperties(userMessage, userMessageVO);
		userMessageVO.setCreatedAt(DateUtils.formatDateTime(userMessage.getCt()));

		Message message = userMessage.getMessage();
		MessageVO messageVO = new MessageVO();
		messageVO.setType(message.getType());
		messageVO.setTitle(message.getTitle());
		messageVO.setContent(message.getContent());

		userMessageVO.setMessage(messageVO);
		return userMessageVO;
	}
}


