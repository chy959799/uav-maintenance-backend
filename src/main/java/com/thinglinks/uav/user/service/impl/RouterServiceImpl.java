package com.thinglinks.uav.user.service.impl;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.user.entity.Menu;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.service.MenuService;
import com.thinglinks.uav.user.service.RouterService;
import com.thinglinks.uav.user.service.UserService;
import com.thinglinks.uav.user.vo.MetaVO;
import com.thinglinks.uav.user.vo.RouterVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author iwiFool
 * @date 2024/5/20
 */
@Service
public class RouterServiceImpl implements RouterService {

    @Resource
    private UserService userService;
    @Resource
    private MenuService menuService;

    @Override
    public BaseResponse<List<RouterVO>> getRouter() {
        List<Menu> menuList;

        if (CommonUtils.isAdmin()) {
            menuList = menuService.getAllMenu();
        } else {
            List<Role> userRoleList = userService.getUserRoleListByUserId(CommonUtils.getUserId());
            menuList = userRoleList.stream().flatMap(role -> role.getMenus().stream())
                    .distinct()
                    .collect(Collectors.toList());

            List<Integer> list = menuList.stream().map(BaseEntity::getId).collect(Collectors.toList());
            menuList.addAll(menuService.getHiddenMenuListByPidIn(list));
        }

        List<RouterVO> routerVOS = buildRouterTree(menuList);
        return BaseResponse.success(routerVOS);
    }

    private List<RouterVO> buildRouterTree(List<Menu> menuList) {
        List<RouterVO> tree = new ArrayList<>();

        Map<Integer, RouterVO> nodeMap = new HashMap<>();
        for (Menu menu : menuList) {
            RouterVO node = new RouterVO();
            node.setName(menu.getName());
            node.setPath(menu.getPath());
            node.setComponent(menu.getComponent());
            node.setRedirect(menu.getRedirect());

            MetaVO metaVO = new MetaVO();
            metaVO.setTitle(menu.getTitle());
            metaVO.setIcon(menu.getIcon());
            metaVO.setHidden(menu.isHidden());
            metaVO.setKeepAlive(menu.isNoKeepAlive());
            metaVO.setNoClosable(menu.isNoClosable());

            node.setMeta(metaVO);
            node.setSort(menu.getSort());
            nodeMap.put(menu.getId(), node);
        }

        for (Menu menu : menuList) {
            RouterVO node = nodeMap.get(menu.getId());
            RouterVO parent = nodeMap.get(menu.getPid());
            if (parent != null) {
                parent.getChildren().add(node);
                parent.setCount(parent.getCount() + 1);
            } else {
                tree.add(node);
            }
        }

        for (RouterVO routerVO : tree) {
            routerVO.getChildren().sort(Comparator.comparing(RouterVO::getSort));
        }

        tree.sort(Comparator.comparing(RouterVO::getSort));
        return tree;
    }
}
