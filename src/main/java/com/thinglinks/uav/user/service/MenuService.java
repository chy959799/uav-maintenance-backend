package com.thinglinks.uav.user.service;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.user.dto.menus.MenuDTO;
import com.thinglinks.uav.user.entity.Menu;

import java.util.List;

/**
 * @author: fanhaiqiu
 * @date: 2022/1/19
 */
public interface MenuService {

    BaseResponse<List<MenuDTO>> getMenuTree(boolean hidden);

    List<Menu> getAllMenu();

    List<Menu> getHiddenMenuListByPidIn(List<Integer> list);
}
