package com.thinglinks.uav.user.service.impl;

import com.thinglinks.uav.common.enums.RoleEnum;
import com.thinglinks.uav.common.enums.UserTypeEnum;
import com.thinglinks.uav.common.redis.CustomRedisTemplate;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.user.entity.Role;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.entity.UserRole;
import com.thinglinks.uav.user.repository.UserRoleRepository;
import com.thinglinks.uav.user.service.RoleService;
import com.thinglinks.uav.user.service.UserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/7/14
 */
@Service
@Slf4j
public class UserRoleServiceImpl implements UserRoleService {

    @Resource
    private UserRoleRepository userRoleRepository;

    @Resource
    private RoleService roleService;

    @Resource
    private CustomRedisTemplate customRedisTemplate;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void modifyUserRoles(User user, List<Integer> roles) {
        if (roles.isEmpty()) {
            throw new BusinessException("角色不能为空");
        }

        Role defaultUserRole = roleService.getDefaultRoleByCode(RoleEnum.User);
        if (!user.getUserType().equals(UserTypeEnum.ADMIN.getCode())) {
            if (roles.size() > 1 || !roles.contains(defaultUserRole.getId())) {
                throw new BusinessException("非系统用户，无法添加系统角色");
            }
        }

        try {
            userRoleRepository.deleteByUserId(user.getId());

            createNewUserRoleRelationships(user, roles);

            customRedisTemplate.updateUserRoles(user.getUid(), roles);
        } catch (Exception e) {
            log.error("修改角色失败", e);
            throw new BusinessException("修改角色失败");
        }
    }


    private void createNewUserRoleRelationships(User user, List<Integer> roles) {
        List<UserRole> userRoles = new ArrayList<>();
        for (Integer r : roles) {
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getId());
            userRole.setRoleId(r);
            userRoles.add(userRole);
        }

        userRoleRepository.saveAll(userRoles);
    }
}
