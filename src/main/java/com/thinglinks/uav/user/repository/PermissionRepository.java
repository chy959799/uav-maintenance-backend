package com.thinglinks.uav.user.repository;

import com.thinglinks.uav.user.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author huiyongchen
 */
public interface PermissionRepository extends JpaRepository<Permission, Integer>, JpaSpecificationExecutor<Permission> {
    List<Permission> findAllByMenuId(Integer menuId);

    List<Permission> findAllByMenuIdIn(List<Integer> list);
}
