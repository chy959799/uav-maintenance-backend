package com.thinglinks.uav.user.repository;


import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author fanhaiqiu
 * @date 2019/6/25
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Integer>, JpaSpecificationExecutor<User> {
    List<UserRole> findByUserId(Integer userId);


    List<UserRole> findByRoleId(Integer roleId);

    void deleteByUserId(Integer userId);

    void deleteByRoleIdAndUserId(Integer roleId,Integer userId);

    void deleteAllByUserId(Integer id);
}
