package com.thinglinks.uav.user.repository;

import com.thinglinks.uav.user.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Collection;
import java.util.List;

/**
 * @author huiyongchen
 */
public interface MenuRepository extends JpaRepository<Menu, Integer>, JpaSpecificationExecutor<Menu> {

    List<Menu> findAllByHiddenOrderBySort(boolean hidden);

    List<Menu> findAllByHiddenAndPidIn(boolean hidden, Collection<Integer> pid);

    Menu findByName(String personnel);

    List<Menu> findAllByPid(Integer pid);
}
