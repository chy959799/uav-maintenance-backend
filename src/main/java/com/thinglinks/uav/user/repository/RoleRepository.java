package com.thinglinks.uav.user.repository;

import com.thinglinks.uav.user.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;


/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/3/17 16:31
 */
public interface RoleRepository extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {
    Role findByName(String name);

    boolean existsByName(String name);

    boolean existsByCode(String code);

    List<Role> findAllByIdIn(List<Integer> ids);

    @Transactional
    @Modifying
    @Query(value = "update Role r set r.status = :status where r.id in :ids")
    void batchUpdataStatus(@Param("status") Integer status, @Param("ids") List<Integer> ids);

    Role findByCode(String code);
}
