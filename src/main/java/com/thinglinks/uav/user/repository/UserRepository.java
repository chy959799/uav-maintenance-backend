package com.thinglinks.uav.user.repository;


import com.thinglinks.uav.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author fanhaiqiu
 * @date 2019/6/25
 */
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    User findByUserName(String name);

    User findByMobile(String mobile);

    Long countByUserType(Integer userType);

    List<User> findByUserType(Integer userType);

    User findByUid(String uid);

    boolean existsByUserName(String userName);

    boolean existsByNickName(String nikeName);

    boolean existsByMobile(String mobile);

    Page<User> findByIdIn(List<Integer> ids, Pageable pageable);

    void deleteAllByUidIn(List<String> ids);

    void deleteByUid(String uid);

    boolean existsByUidAndDid(String recorderUid, String recorderDid);

    List<User> findAllByDid(String receiverId);

    List<User> findAllByCpid(String receiverId);

    boolean existsByCid(String cid);

    boolean existsByCidIn(List<String> ids);

    Integer countByCpid(String cpid);

    int countByCpidIn(List<String> ids);

    User findByCid(String cid);
}
