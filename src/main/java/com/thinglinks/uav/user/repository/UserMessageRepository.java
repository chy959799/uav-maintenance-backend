package com.thinglinks.uav.user.repository;

import com.thinglinks.uav.user.entity.UserMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * (UserMessages)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-20 20:31:43
 */
public interface UserMessageRepository extends JpaRepository<UserMessage, Integer>, JpaSpecificationExecutor<UserMessage> {

	@Modifying
	@Query("UPDATE UserMessage um SET um.status = :status WHERE um.msgid in :ids")
	void updateStatus(@Param("ids") List<String> ids, @Param("status") String status);

	void deleteByMsgidIn(List<String> ids);

	Long countByStatusAndUid(String number, String uid);
}


