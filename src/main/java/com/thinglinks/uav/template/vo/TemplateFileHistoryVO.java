package com.thinglinks.uav.template.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/8/26
 */
@Data
@ApiModel(value = "保险模板文件历史VO")
public class TemplateFileHistoryVO {

    /**
     * 文件id
     */
    @ApiModelProperty(value = "tfhid", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
    private String tfhid;

    /**
     * 文件名称
     */
    @ApiModelProperty(value = "文件名称", example = "历史版本1.docx")
    private String name;

    /**
     * 文件大小
     */
    @ApiModelProperty(value = "文件大小", example = "100")
    private Long size;

    /**
     * 操作部门
     */
    @ApiModelProperty(value = "操作部门", example = "部门A")
    private String departmentName;

    /**
     * 操作人
     */
    @ApiModelProperty(value = "操作人", example = "Admin")
    private String userName;

    /**
     * 文件更新时间
     */
    @ApiModelProperty(value = "文件更新时间", example = "2024-08-25 12:00:00")
    private String updatedAt;

    /**
     * 文件url
     */
    @ApiModelProperty(value = "文件url", example = "http://192.168.3.20:19000/minio/b-common/2024-08-25/839910b7-2b1d-4bf1-88b4-4e8dbc804b55.pdf")
    private String url;
}
