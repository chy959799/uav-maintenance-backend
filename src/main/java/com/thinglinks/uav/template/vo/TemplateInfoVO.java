package com.thinglinks.uav.template.vo;

import com.thinglinks.uav.customer.vo.CompanyVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author iwiFool
 * @date 2024/8/24
 */
@Data
@ApiModel(value = "保险模板详情VO")
public class TemplateInfoVO {

    /**
     * 模板信息
     */
    @ApiModelProperty(value = "模板信息")
    private TemplateVO template;

    /**
     * 关联维修点列表
     */
    @ApiModelProperty(value = "关联维修点列表")
    private List<CompanyVO> companyList;

    /**
     * 模板文件列表
     */
    private List<TemplateFileVO> templateFileList;
}
