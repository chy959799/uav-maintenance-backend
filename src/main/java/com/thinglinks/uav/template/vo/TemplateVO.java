package com.thinglinks.uav.template.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/8/24
 */
@Data
@ApiModel(value = "保险模板VO")
public class TemplateVO {

    /**
     * 模板id
     */
    @ApiModelProperty(value = "模板id", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
    private String tid;

    /**
     * 模板名称
     */
    @ApiModelProperty(value = "模板名称", example = "模板1")
    private String name;

    /**
     * 模板标识
     */
    @ApiModelProperty(value = "模板标识", example = "1001")
    private String code;

    /**
     * 模板描述
     */
    @ApiModelProperty(value = "模板描述", example = "这是A维修部的保险文件模板")
    private String description;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "2024-03-21 12:00:00")
    private String createdAt;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2024-03-21 12:00:00")
    private String updatedAt;
}
