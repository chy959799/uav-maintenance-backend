package com.thinglinks.uav.template.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/8/25
 */
@Data
@ApiModel(value = "保险模板文件VO")
public class TemplateFileVO {

    /**
     * 模板文件id
     */
    @ApiModelProperty(value = "模板文件id", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
    private String tfid;

    /**
     * 文件类型
     */
    @ApiModelProperty(value = "文件类型:[001-赔付意向书, 002-电力业务事故通知与索赔申请单, 003-损失清单, 004-无人机故障报告, 005-事故照片, 006-收费通知单, 007-案件报告, 008-报案文件]", example = "001")
    private String type;

    /**
     * 文件展示名称
     */
    @ApiModelProperty(value = "文件展示名称", example = "赔付意向书")
    private String displayName;

    /**
     * 文件大小
     */
    @ApiModelProperty(value = "文件大小", example = "100")
    private Long size;

    /**
     * 文件url
     */
    @ApiModelProperty(value = "文件url", example = "http://192.168.3.20:19000/minio/b-common/2024-08-25/839910b7-2b1d-4bf1-88b4-4e8dbc804b55.pdf")
    private String url;

    /**
     * 单位名称
     */
    @ApiModelProperty(value = "单位名称", example = "客户A")
    private String customerName;

    /**
     * 操作部门
     */
    @ApiModelProperty(value = "操作部门", example = "部门A")
    private String departmentName;

    /**
     * 操作人
     */
    @ApiModelProperty(value = "操作人", example = "Admin")
    private String userName;

    /**
     * 文件更新时间
     */
    @ApiModelProperty(value = "文件更新时间", example = "2024-08-25 12:00:00")
    private String updatedAt;
}
