package com.thinglinks.uav.template.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * (TemplateFileHistory)实体类
 *
 * @author iwiFool
 * @since 2024-08-26 22:21:56
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "template_file_histories")
public class TemplateFileHistory extends BaseEntity {
    /**
     * 模板文件历史id
     */
    private String tfhid;
    /**
     * 模板文件id
     */
    private String tfid;
    /**
     * 文件名
     */
    private String name;
    /**
     * 文件展示名
     */
    private String displayName;
    /**
     * 大小
     */
    private Long size;
    /**
     * 用户id
     */
    private String uid;
    /**
     * 部门id
     */
    private String did;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tfid", referencedColumnName = "tfid", insertable = false, updatable = false)
    private TemplateFile templateFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "did", referencedColumnName = "did", insertable = false, updatable = false)
    private Department department;
}
