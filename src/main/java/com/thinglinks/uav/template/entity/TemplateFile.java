package com.thinglinks.uav.template.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.departments.entity.Department;
import com.thinglinks.uav.user.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

/**
 * (TemplateFile)实体类
 *
 * @author iwiFool
 * @since 2024-08-24 19:16:04
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "template_files")
public class TemplateFile extends BaseEntity {
    /**
     * 模板文件内部id
     */
    private String tfid;
    /**
     * 模板内部id
     */
    private String tid;
    /**
     * 文件类型
     */
    private String type;
    /**
     * 文件展示名称
     */
    private String displayName;
    /**
     * 文件名
     */
    private String name;
    /**
     * 文件大小
     */
    private Long size;
    /**
     * 单位
     */
    private String cid;
    /**
     * 操作部门
     */
    private String did;
    /**
     * 操作人
     */
    private String uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cid", referencedColumnName = "cid", insertable = false, updatable = false)
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "did", referencedColumnName = "did", insertable = false, updatable = false)
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uid", referencedColumnName = "uid", insertable = false, updatable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tid", referencedColumnName = "tid", insertable = false, updatable = false)
    private Template template;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "templateFile")
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<TemplateFileHistory> fileHistory;
}
