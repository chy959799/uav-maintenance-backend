package com.thinglinks.uav.template.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * (Template)实体类
 *
 * @author iwiFool
 * @since 2024-08-24 18:34:17
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "templates")
public class Template extends BaseEntity {
    /**
     * 模板内部id
     */
    private String tid;
    /**
     * 模板名称
     */
    private String name;
    /**
     * 模板标识
     */
    private String code;
    /**
     * 模板描述
     */
    private String description;
    /**
     * 默认模板
     */
    private Boolean isDefault;

    /**
     * 模板文件集合
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "template")
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<TemplateFile> templateFiles;
}
