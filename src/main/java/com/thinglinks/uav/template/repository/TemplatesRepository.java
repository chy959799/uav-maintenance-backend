package com.thinglinks.uav.template.repository;

import com.thinglinks.uav.template.entity.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * (Template)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-08-24 18:34:17
 */
public interface TemplatesRepository extends JpaRepository<Template, Long>, JpaSpecificationExecutor<Template> {


    void deleteByTid(String tid);

    Template findByTid(String tid);

    Template findByIsDefault(Boolean b);

    Long countByIsDefault(boolean b);

    Boolean existsByCode(String templateCode);

    Template findByCode(String templateCode);
}


