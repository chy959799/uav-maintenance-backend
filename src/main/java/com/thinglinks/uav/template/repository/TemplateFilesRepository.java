package com.thinglinks.uav.template.repository;

import com.thinglinks.uav.template.entity.TemplateFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * (TemplateFile)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-08-24 19:16:04
 */
public interface TemplateFilesRepository extends JpaRepository<TemplateFile, Long> {


    List<TemplateFile> findAllByTid(String tid);

    TemplateFile findByTfid(String tfid);

    TemplateFile findByTidAndType(String tid, String type);
}


