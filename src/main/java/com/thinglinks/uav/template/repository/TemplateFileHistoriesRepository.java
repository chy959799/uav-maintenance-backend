package com.thinglinks.uav.template.repository;

import com.thinglinks.uav.template.entity.TemplateFileHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * (TemplateFileHistory)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-08-26 22:21:56
 */
public interface TemplateFileHistoriesRepository extends JpaRepository<TemplateFileHistory, Long> {


    List<TemplateFileHistory> findAllByTfidOrderByUtDesc(String tfid);

    TemplateFileHistory findByTfhid(String tfhid);
}


