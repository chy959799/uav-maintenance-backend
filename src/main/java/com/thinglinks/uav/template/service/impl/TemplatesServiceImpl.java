package com.thinglinks.uav.template.service.impl;

import com.thinglinks.uav.common.constants.CommonConstants;
import com.thinglinks.uav.common.constants.TemplateConstants;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.enums.TemplateCode;
import com.thinglinks.uav.common.enums.TemplateEnum;
import com.thinglinks.uav.common.minio.CustomMinIOClient;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.entity.Company;
import com.thinglinks.uav.customer.repository.CompanyRepository;
import com.thinglinks.uav.customer.vo.CompanyVO;
import com.thinglinks.uav.system.exception.BusinessException;
import com.thinglinks.uav.template.dto.TemplateDTO;
import com.thinglinks.uav.template.dto.TemplatePageQuery;
import com.thinglinks.uav.template.entity.Template;
import com.thinglinks.uav.template.entity.TemplateFile;
import com.thinglinks.uav.template.entity.TemplateFileHistory;
import com.thinglinks.uav.template.repository.TemplateFileHistoriesRepository;
import com.thinglinks.uav.template.repository.TemplateFilesRepository;
import com.thinglinks.uav.template.repository.TemplatesRepository;
import com.thinglinks.uav.template.service.TemplatesService;
import com.thinglinks.uav.template.vo.TemplateFileHistoryVO;
import com.thinglinks.uav.template.vo.TemplateFileVO;
import com.thinglinks.uav.template.vo.TemplateInfoVO;
import com.thinglinks.uav.template.vo.TemplateVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (Template)表服务实现类
 *
 * @author iwiFool
 * @since 2024-08-24 18:34:17
 */
@Slf4j
@Service("templatesService")
public class TemplatesServiceImpl implements TemplatesService {
    @Resource
    private TemplatesRepository templatesRepository;
    @Resource
    private TemplateFilesRepository templateFilesRepository;
    @Resource
    private CustomMinIOClient customMinIOClient;
    @Resource
    private TemplateFileHistoriesRepository templateFileHistoriesRepository;
    @Resource
    private CompanyRepository companyRepository;

    @PostConstruct
    public void initDefaultTemplate() {
        if (templatesRepository.countByIsDefault(true) == 0) {
            Template template = createDefaultTemplate();
            List<TemplateFile> templateFiles = new ArrayList<>();
            TemplateEnum[] templateEnums = TemplateEnum.values();
            for (TemplateEnum templateEnum : templateEnums) {
                try {
                    TemplateFile templateFile = createTemplateFile(template, templateEnum);
                    templateFiles.add(templateFile);
                } catch (Exception e) {
                    throw new BusinessException("初始化模板文件失败: " + templateEnum.getName() + " - " + e.getMessage(), e);
                }
            }

            template.setTemplateFiles(templateFiles);
            templatesRepository.save(template);
        }
    }

    private Template createDefaultTemplate() {
        Template template = new Template();
        template.setTid(CommonUtils.uuid());
        template.setName(TemplateConstants.DEFAULT_TEMPLATE_NAME);
        template.setCode(TemplateCode.T_0001.getCode());
        template.setDescription(TemplateConstants.DEFAULT_TEMPLATE_DESCRIPTION);
        template.setIsDefault(true);
        return template;
    }

    private TemplateFile createTemplateFile(Template template, TemplateEnum templateEnum) {
        TemplateFile templateFile = new TemplateFile();
        templateFile.setTid(template.getTid());
        templateFile.setTfid(CommonUtils.uuid());
        templateFile.setType(templateEnum.getType());

        String filePath = getFilePath(templateEnum.getFilePath());
        File file = new File(filePath);

        if (!file.exists()) {
            throw new BusinessException("模板文件未找到: " + templateEnum.getFilePath());
        }

        long fileSize = file.length();
        String objectName = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonUtils.getExtension(templateEnum.getFilePath()));
        uploadFileToMinIO(objectName, file);

        templateFile.setDisplayName(templateEnum.getName());
        templateFile.setName(objectName);
        templateFile.setSize(fileSize);

        return templateFile;
    }

    private String getFilePath(String resourceDir) throws BusinessException {
        URL resource = this.getClass().getClassLoader().getResource(resourceDir);
        if (resource == null) {
            throw new BusinessException("无法获取文件路径: " + resourceDir);
        }
        return resource.getPath();
    }

    private void uploadFileToMinIO(String objectName, File file) throws BusinessException {
        try (InputStream fileInputStream = Files.newInputStream(file.toPath())) {
            customMinIOClient.uploadFile(TemplateConstants.TEMPLATE_BUCKET, objectName, fileInputStream);
        } catch (IOException e) {
            throw new BusinessException("文件上传失败: " + e.getMessage(), e);
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public BaseResponse<String> insert(TemplateDTO dto) {
        Template template = templatesRepository.findByCode(dto.getCode());
        if (template != null) {
            return BaseResponse.fail("模板标识已存在");
        }

        Template enetiy = new Template();
        BeanUtils.copyProperties(dto, enetiy);
        enetiy.setTid(CommonUtils.uuid());
        enetiy.setIsDefault(false);
        Template defaultTemplate = templatesRepository.findByIsDefault(true);
        List<TemplateFile> defaultTemplateFiles = defaultTemplate.getTemplateFiles();
        ArrayList<TemplateFile> templateFiles = new ArrayList<>(defaultTemplateFiles.size());
        for (TemplateFile defaultTemplateFile : defaultTemplateFiles) {
            TemplateFile templateFile = new TemplateFile();
            templateFile.setTid(enetiy.getTid());
            templateFile.setTfid(CommonUtils.uuid());
            templateFile.setType(defaultTemplateFile.getType());
            templateFile.setDisplayName(defaultTemplateFile.getDisplayName());
            templateFile.setName(defaultTemplateFile.getName());
            templateFile.setSize(defaultTemplateFile.getSize());
            templateFile.setUid(CommonUtils.getUid());
            templateFile.setDid(CommonUtils.getDid());
            templateFile.setCid(CommonUtils.getCid());
            templateFiles.add(templateFile);
        }
        enetiy.setTemplateFiles(templateFiles);
        templatesRepository.save(enetiy);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<String> delete(String tid) {
        Template template = templatesRepository.findByTid(tid);
        if (Objects.isNull(template)) {
            throw new BusinessException("模板不存在");
        }
        List<Company> companies = companyRepository.findAllByTemplateCode(template.getCode());
        if (!companies.isEmpty()) {
            throw new BusinessException("模板已被使用，无法删除");
        }
        if (template.getIsDefault()) {
            throw new BusinessException("默认模板不能删除");
        }
        templatesRepository.deleteByTid(tid);

        return BaseResponse.success();
    }

    @Override
    public BasePageResponse<TemplateVO> getPage(TemplatePageQuery pageQuery) {
        Specification<Template> specification = (root, query, builder) -> {
            Predicate predicate = builder.conjunction();
            if (StringUtils.isNotBlank(pageQuery.getName())) {
                predicate = builder.and(predicate, builder.like(root.get("name"), CommonUtils.assembleLike(pageQuery.getName())));
            }
            return predicate;
        };

        Page<Template> page = templatesRepository.findAll(specification, pageQuery.of());
        List<TemplateVO> voList = page.getContent().stream()
                .map(this::convertToVO)
                .collect(Collectors.toList());

        return BasePageResponse.success(page, voList);
    }

    @Override
    public BaseResponse<TemplateInfoVO> getInfo(String tid) {
        Template entity = templatesRepository.findByTid(tid);
        if (Objects.isNull(entity)) {
            throw new BusinessException("模板不存在");
        }

        return BaseResponse.success(convertToInfoVO(entity));
    }

    @Override
    public BaseResponse<String> updateDisplayName(String tfid, String displayName) {
        if (StringUtils.isBlank(displayName)) {
            return BaseResponse.fail("文件展示名称不能为空");
        }

        TemplateFile templateFile = templateFilesRepository.findByTfid(tfid);
        if (Objects.isNull(templateFile)) {
            return BaseResponse.fail("文件不存在");
        }

        Template template = templateFile.getTemplate();
        if (template.getIsDefault()) {
            return BaseResponse.fail("默认模板不能修改");
        }

        templateFile.setDisplayName(displayName);
        templateFile.setUid(CommonUtils.getUid());
        templateFile.setDid(CommonUtils.getDid());
        templateFile.setCid(CommonUtils.getCid());
        templateFilesRepository.save(templateFile);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<String> upload(String tfid, MultipartFile file) {
        TemplateFile templateFile = templateFilesRepository.findByTfid(tfid);
        if (Objects.isNull(templateFile)) {
            return BaseResponse.fail("文件不存在");
        }

        Template template = templateFile.getTemplate();
        if (template.getIsDefault()) {
            return BaseResponse.fail("默认模板不能修改");
        }

        // TODO:文件需要校验
        String objectName = CommonUtils.uuid().concat(CommonConstants.DOT).concat(CommonUtils.getExtension(file.getOriginalFilename()));
        try {
            customMinIOClient.uploadFile(TemplateConstants.TEMPLATE_BUCKET, objectName, file.getInputStream());
        } catch (Exception e) {
            throw new BusinessException("文件上传失败");
        }

        List<TemplateFileHistory> histories = templateFileHistoriesRepository.findAllByTfidOrderByUtDesc(tfid);
        TemplateFileHistory history;

        // 只保留两次历史版本文件
        if (histories.size() < 2) {
            history = new TemplateFileHistory();
            history.setTfhid(CommonUtils.uuid());
            history.setTfid(templateFile.getTfid());
        } else {
            history = histories.get(1);
        }
        history.setSize(templateFile.getSize());
        history.setName(templateFile.getName());
        history.setDisplayName(templateFile.getDisplayName());
        history.setDisplayName(templateFile.getDisplayName());
        history.setUid(CommonUtils.getUid());
        history.setDid(CommonUtils.getDid());
        templateFileHistoriesRepository.save(history);

        templateFile.setName(objectName);
        templateFile.setSize(file.getSize());
        templateFile.setUid(CommonUtils.getUid());
        templateFile.setDid(CommonUtils.getDid());
        templateFile.setCid(CommonUtils.getCid());
        templateFilesRepository.save(templateFile);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<List<TemplateFileHistoryVO>> getFileHistory(String tfid) {
        List<TemplateFileHistory> list = templateFileHistoriesRepository.findAllByTfidOrderByUtDesc(tfid);
        List<TemplateFileHistoryVO> fileHistoryVOList = list.stream().map(this::convertToHistoryVO).collect(Collectors.toList());

        return BaseResponse.success(fileHistoryVOList);
    }

    @Override
    public BaseResponse<String> recovery(String tfid, String tfhid) {
        TemplateFile templateFile = templateFilesRepository.findByTfid(tfid);
        if (Objects.isNull(templateFile)) {
            return BaseResponse.fail("文件不存在");
        }

        TemplateFileHistory history = templateFileHistoriesRepository.findByTfhid(tfhid);
        if (Objects.isNull(history)) {
            return BaseResponse.fail("文件历史版本不存在");
        }

        String oldFileName = templateFile.getName();
        templateFile.setName(history.getName());
        history.setName(oldFileName);
        history.setUid(CommonUtils.getUid());
        history.setDid(CommonUtils.getDid());
        templateFile.setUid(CommonUtils.getUid());
        templateFile.setDid(CommonUtils.getDid());
        templateFile.setCid(CommonUtils.getCid());
        templateFilesRepository.save(templateFile);
        templateFileHistoriesRepository.save(history);

        return BaseResponse.success();
    }

    private TemplateFileHistoryVO convertToHistoryVO(TemplateFileHistory entity) {
        TemplateFileHistoryVO vo = new TemplateFileHistoryVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setUrl(getProxyUrl(entity.getName()));
        vo.setDepartmentName(entity.getDepartment().getName());
        vo.setUserName(entity.getUser().getUserName());
        vo.setUpdatedAt(DateUtils.formatDateTime(entity.getUt()));
        return vo;
    }


    private TemplateInfoVO convertToInfoVO(Template entity) {
        TemplateInfoVO infoVO = new TemplateInfoVO();
        TemplateVO templateVO = convertToVO(entity);
        infoVO.setTemplate(templateVO);

        List<Company> companies = companyRepository.findAllByTemplateCode(entity.getCode());
        List<CompanyVO> companyList = companies.stream().map(this::companyConvertToVO).collect(Collectors.toList());
        infoVO.setCompanyList(companyList);

        List<TemplateFile> templateFiles = entity.getTemplateFiles();
        List<TemplateFileVO> templateFileVOList = templateFiles.stream().map(this::templateFileConvertToVO).collect(Collectors.toList());
        infoVO.setTemplateFileList(templateFileVOList);

        return infoVO;
    }

    private CompanyVO companyConvertToVO(Company entity) {
        CompanyVO vo = new CompanyVO();
        BeanUtils.copyProperties(entity, vo);
        return vo;
    }

    private TemplateFileVO templateFileConvertToVO(TemplateFile entity) {
        TemplateFileVO vo = new TemplateFileVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setUrl(getProxyUrl(entity.getName()));
        vo.setCustomerName(Objects.nonNull(entity.getCustomer()) ? entity.getCustomer().getName() : null);
        vo.setDepartmentName(Objects.nonNull(entity.getDepartment()) ? entity.getDepartment().getName() : null);
        vo.setUserName(Objects.nonNull(entity.getUser()) ? entity.getUser().getUserName() : null);
        vo.setUpdatedAt(DateUtils.formatDateTime(entity.getUt()));
        return vo;
    }

    private TemplateVO convertToVO(Template entity) {
        TemplateVO vo = new TemplateVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setCreatedAt(DateUtils.formatDateTime(entity.getCt()));
        vo.setUpdatedAt(DateUtils.formatDateTime(entity.getUt()));
        return vo;
    }

    /**
     * 获取文件的访问地址
     *
     * @param objectName
     * @return
     */
    private String getProxyUrl(String objectName) {
        try {
            if (StringUtils.isBlank(objectName)) {
                return StringUtils.EMPTY;
            }
            return customMinIOClient.getProxyUrl(TemplateConstants.TEMPLATE_BUCKET, objectName);
        } catch (Exception e) {
            log.error("getFileProxyUrl error", e);
        }
        return StringUtils.EMPTY;
    }
}


