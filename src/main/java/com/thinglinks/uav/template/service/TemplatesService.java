package com.thinglinks.uav.template.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.template.dto.TemplateDTO;
import com.thinglinks.uav.template.dto.TemplatePageQuery;
import com.thinglinks.uav.template.vo.TemplateFileHistoryVO;
import com.thinglinks.uav.template.vo.TemplateInfoVO;
import com.thinglinks.uav.template.vo.TemplateVO;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * (Template)表服务接口
 *
 * @author iwiFool
 * @since 2024-08-24 18:34:16
 */
public interface TemplatesService {

    @Transactional(rollbackFor = Exception.class)
    BaseResponse<String> insert(TemplateDTO dto);

    @Transactional(rollbackFor = Exception.class)
    BaseResponse<String> delete(String tid);

    BasePageResponse<TemplateVO> getPage(TemplatePageQuery pageQuery);

    BaseResponse<TemplateInfoVO> getInfo(String tid);

    @Transactional(rollbackFor = Exception.class)
    BaseResponse<String> updateDisplayName(String tfid, String displayName);

    @Transactional(rollbackFor = Exception.class)
    BaseResponse<String> upload(String tfid, MultipartFile file);

    BaseResponse<List<TemplateFileHistoryVO>> getFileHistory(String tfid);

    @Transactional(rollbackFor = Exception.class)
    BaseResponse<String> recovery(String tfid, String tfhid);
}


