package com.thinglinks.uav.template.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/8/24
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TemplatePageQuery extends BasePageRequest {

    /**
     * 保险模板名称
     */
    @ApiModelProperty(value = "保险模板名称", example = "模板1")
    private String name;
}
