package com.thinglinks.uav.template.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author iwiFool
 * @date 2024/8/24
 */
@Data
@ApiModel(description = "保险模板DTO")
public class TemplateDTO {

    /**
     * 模板名称
     */
    @NotBlank(message = "模板名称不能为空")
    @ApiModelProperty(value = "模板名称", example = "模板名称")
    private String name;

    /**
     * 模板标识
     */
    @NotBlank(message = "模板标识不能为空")
    @ApiModelProperty(value = "模板标识", example = "模板标识")
    private String code;

    /**
     * 模板描述
     */
    @NotBlank(message = "模板描述不能为空")
    @ApiModelProperty(value = "模板描述", example = "模板描述")
    private String description;
}
