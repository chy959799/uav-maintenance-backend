package com.thinglinks.uav.template.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.template.dto.TemplateDTO;
import com.thinglinks.uav.template.dto.TemplatePageQuery;
import com.thinglinks.uav.template.service.TemplatesService;
import com.thinglinks.uav.template.vo.TemplateFileHistoryVO;
import com.thinglinks.uav.template.vo.TemplateInfoVO;
import com.thinglinks.uav.template.vo.TemplateVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * (Template)表控制层
 *
 * @author iwiFool
 * @since 2024-08-24 18:34:16
 */
@Api(tags = "保险模板管理")
@RestController
@RequestMapping("templates")
public class TemplatesController {
    /**
     * 服务对象
     */
    @Resource
    private TemplatesService templatesService;

    @Operation(summary = "新增保险模板")
    @PostMapping
    public BaseResponse<String> add(@RequestBody @Valid TemplateDTO dto) {
        return templatesService.insert(dto);
    }

    @Operation(summary = "删除保险模板")
    @DeleteMapping("{tid}")
    public BaseResponse<String> delete(@PathVariable String tid) {
        return templatesService.delete(tid);
    }

    @Operation(summary = "保险模板列表")
    @GetMapping
    public BasePageResponse<TemplateVO> getPage(TemplatePageQuery pageQuery) {
        return templatesService.getPage(pageQuery);
    }

    @Operation(summary = "保险模板详情")
    @GetMapping("{tid}")
    public BaseResponse<TemplateInfoVO> getInfo(@PathVariable String tid) {
        return templatesService.getInfo(tid);
    }

    @Operation(summary = "编辑文件展示名称")
    @PatchMapping("{tfid}/displayName")
    public BaseResponse<String> updateDisplayName(@PathVariable String tfid, @RequestParam("displayName") String displayName) {
        return templatesService.updateDisplayName(tfid, displayName);
    }

    @Operation(summary = "上传模板文件")
    @PatchMapping("{tfid}/upload")
    public BaseResponse<String> upload(@PathVariable String tfid, @RequestParam("file") MultipartFile file) {
        return templatesService.upload(tfid, file);
    }

    @Operation(summary = "模板文件历史版本")
    @GetMapping("{tfid}/fileHistory")
    public BaseResponse<List<TemplateFileHistoryVO>> fileHistory(@PathVariable String tfid) {
        return templatesService.getFileHistory(tfid);
    }

    @Operation(summary = "恢复历史版本")
    @PatchMapping("{tfid}/recovery")
    public BaseResponse<String> recovery(@PathVariable String tfid, @RequestParam("tfhid") String tfhid) {
        return templatesService.recovery(tfid, tfhid);
    }
}


