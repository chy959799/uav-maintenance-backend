package com.thinglinks.uav.workbench.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: OrderStatisticsVO
 * @date 2024/4/3 9:45
 */
@Data
public class OrderStatisticsVO {
    @ApiModelProperty(value = "累计服务工单")
    private long orderCount;

    @ApiModelProperty(value = "服务客户数量")
    private long customerCount;

    @ApiModelProperty(value = "维保设备数量")
    private long deviceCount;

    @ApiModelProperty(value = "平均处理时长")
    private double averageProcessingTime;
}
