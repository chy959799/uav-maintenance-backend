package com.thinglinks.uav.workbench.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TrendVO
 * @date 2024/4/4 13:40
 */
@Data
public class TrendVO {
    @ApiModelProperty("日期")
    private String date;

    @ApiModelProperty("保内维修")
    private long warrantyRepair;

    @ApiModelProperty("框架合同维修")
    private long frameContractMaintenance;

    @ApiModelProperty("快换快修")
    private long quickReplacementAndRepair;

    @ApiModelProperty("报废工单")
    private long scrapWorkOrder;
}
