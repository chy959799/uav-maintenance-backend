package com.thinglinks.uav.workbench.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 趋势时间
 * @date 2024/4/3 11:11
 */
@Data
@AllArgsConstructor
public class DateAndTimestampRange {
    private LocalDate date;
    private long startTimestamp;
    private long endTimestamp;
}
