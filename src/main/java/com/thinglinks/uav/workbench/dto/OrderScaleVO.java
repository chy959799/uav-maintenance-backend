package com.thinglinks.uav.workbench.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 订单统计数据
 * @date 2024/4/24 21:49
 */
@Data
public class OrderScaleVO {
    @ApiModelProperty("保内维修数量")
    private double warrantyRepairNum;

    @ApiModelProperty("保内维修占比")
    private double warrantyRepair;

    @ApiModelProperty("框架合同维修数量")
    private double frameContractMaintenanceNum;

    @ApiModelProperty("框架合同维修占比")
    private double frameContractMaintenance;

    @ApiModelProperty("快换快修数量")
    private double quickReplacementAndRepairNum;
    @ApiModelProperty("快换快修占比")
    private double quickReplacementAndRepair;

    @ApiModelProperty("报废工单数量")
    private double scrapWorkOrderNum;
    @ApiModelProperty("报废工单占比")
    private double scrapWorkOrder;
}
