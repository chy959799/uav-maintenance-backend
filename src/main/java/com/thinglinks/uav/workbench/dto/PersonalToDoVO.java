package com.thinglinks.uav.workbench.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: PersonalToDoVO
 * @date 2024/4/24 22:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonalToDoVO {
    @ApiModelProperty(value = "待办事项")
    private String status;
    @ApiModelProperty(value = "待办数量")
    private Integer count;
    @ApiModelProperty(value = "状态码")
    private String statusCode;
}
