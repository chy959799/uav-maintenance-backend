package com.thinglinks.uav.workbench.enums;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: RoleTypeEnums
 * @date 2024/4/25 16:18
 */
public enum RoleTypeEnums {
    RECEIVING("receiving", "收寄件人员"),
    CUSTOMERSERVICE("customerService", "客服"),
    INSURANCEAUDITOR("insuranceAuditor", "保险审核员"),
    COMPREHENSIVESTAFF("comprehensiveStaff", "综合员"),
    LOSSASSESSMENTENGINEER("lossAssessmentEngineer", "定损工程师"),
    MAINTENANCEENGINEER("maintenanceEngineer", "维修工程师"),
    TESTINGENGINEER("testingEngineer", "检测工程师"),
    SENDER("sender", "寄件人员"),
    Users("Users", "用户"),
    ROOT("root", "系统超级管理员角色编码"),
    ADMIN("admin", "系统企业管理员角色编码");

    private final String code;
    private final String name;

    public static boolean exists(String code) {
        for (RoleTypeEnums role : RoleTypeEnums.values()) {
            if (role.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    RoleTypeEnums(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
