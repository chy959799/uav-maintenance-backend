package com.thinglinks.uav.workbench.service.impl;

import com.thinglinks.uav.common.constants.OrderConstants;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.order.enums.InsuranceStatusEnums;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.OrderConsultRepository;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.user.repository.RoleRepository;
import com.thinglinks.uav.user.repository.UserRepository;
import com.thinglinks.uav.user.repository.UserRoleRepository;
import com.thinglinks.uav.workbench.dto.*;
import com.thinglinks.uav.workbench.enums.RoleTypeEnums;
import com.thinglinks.uav.workbench.service.WorkbenchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/3 18:05
 */
@Service
@Slf4j
public class WorkbenchServiceImpl implements WorkbenchService {

    private static final String WARRANTY_REPAIR = "1";
    private static final String FRAME_CONTRACT_MAINTENANCE = "2";
    private static final String QUICK_REPLACEMENT_AND_REPAIR = "3";
    private static final String SCRAP_WORK_ORDER = "4";

    @Resource
    private UserRepository userRepository;

    @Resource
    private RoleRepository roleRepository;

    @Resource
    private UserRoleRepository userRoleRepository;
    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderConsultRepository orderConsultRepository;

    @Override
    public BaseResponse<OrderStatisticsVO> orderStatistics() {
        OrderStatisticsVO statistics = new OrderStatisticsVO();
        // 累计服务工单
        statistics.setOrderCount(orderRepository.count());
        // 服务客户数量
        statistics.setCustomerCount(orderRepository.countDistinctByCid() + orderConsultRepository.countDistinctByCid());
        // 维保设备数量
        statistics.setDeviceCount(orderRepository.countDistinctByDevid());
        // 平均处理时长
        Long averageProcessingTime = 0L;
        Optional<Long> totalTime = orderRepository.avgTime();
        if (totalTime.isPresent()) {
            averageProcessingTime = totalTime.get();
        }
        statistics.setAverageProcessingTime(Math.round((double) averageProcessingTime / 86400 * 100.0) / 100.0);
        return BaseResponse.success(statistics);
    }

    @Override
    public BaseResponse<List<TrendVO>> orderTrend() {
        List<DateAndTimestampRange> ranges = createDateAndTimestampRanges();
        List<TrendVO> trend = ranges.stream()
                .map(range -> {
                    TrendVO vo = new TrendVO();
                    vo.setDate(String.valueOf(range.getDate()));
                    vo.setWarrantyRepair(orderRepository.countByTimesTampRange(WARRANTY_REPAIR, range.getStartTimestamp(), range.getEndTimestamp()));
                    vo.setFrameContractMaintenance(orderRepository.countByTimesTampRange(FRAME_CONTRACT_MAINTENANCE, range.getStartTimestamp(), range.getEndTimestamp()));
                    vo.setQuickReplacementAndRepair(orderRepository.countByTimesTampRange(QUICK_REPLACEMENT_AND_REPAIR, range.getStartTimestamp(), range.getEndTimestamp()));
                    vo.setScrapWorkOrder(orderRepository.countByTimesTampRange(SCRAP_WORK_ORDER, range.getStartTimestamp(), range.getEndTimestamp()));
                    return vo;
                })
                .collect(Collectors.toList());
        return BaseResponse.success(trend);
    }

    @Override
    public BaseResponse<Integer> relatedOrder(String status) {
        Integer count = orderRepository.countByStatus(status);
        return BaseResponse.success(count);
    }

    @Override
    public BaseResponse<OrderScaleVO> scale() {
        OrderScaleVO orderScaleVO = new OrderScaleVO();
        long total = orderRepository.count();
        double totalDouble = (double) total;

        long warrantyRepairCount = orderRepository.countByServiceType(WARRANTY_REPAIR);
        orderScaleVO.setWarrantyRepairNum(warrantyRepairCount);
        orderScaleVO.setWarrantyRepair(Math.round((double) warrantyRepairCount / totalDouble * 100));

        long frameContractMaintenanceCount = orderRepository.countByServiceType(FRAME_CONTRACT_MAINTENANCE);
        orderScaleVO.setFrameContractMaintenanceNum(frameContractMaintenanceCount);
        orderScaleVO.setFrameContractMaintenance(Math.round((double) frameContractMaintenanceCount / totalDouble * 100));

        long quickReplacementAndRepairCount = orderRepository.countByServiceType(QUICK_REPLACEMENT_AND_REPAIR);
        orderScaleVO.setQuickReplacementAndRepairNum(quickReplacementAndRepairCount);
        orderScaleVO.setQuickReplacementAndRepair(Math.round((double) quickReplacementAndRepairCount / totalDouble * 100));

        long scrapWorkOrderCount = orderRepository.countByServiceType(SCRAP_WORK_ORDER);
        orderScaleVO.setScrapWorkOrderNum(scrapWorkOrderCount);
        orderScaleVO.setScrapWorkOrder(Math.round((double) scrapWorkOrderCount / totalDouble * 100));

        return BaseResponse.success(orderScaleVO);
    }


    @Override
    public BaseResponse<List<PersonalToDoVO>> personalToDo() {
        List<String> roles = CommonUtils.getUserRoles();
        Set<PersonalToDoVO> personalToDoVO = roles.stream()
                .map(role -> {
                    Map<String, List<PersonalToDoVO>> roleToTasks = new HashMap<>();
                    roleToTasks.put(RoleTypeEnums.RECEIVING.getCode(), receiving());
                    roleToTasks.put(RoleTypeEnums.CUSTOMERSERVICE.getCode(), customerService());
                    roleToTasks.put(RoleTypeEnums.INSURANCEAUDITOR.getCode(), insuranceAuditor());
                    roleToTasks.put(RoleTypeEnums.COMPREHENSIVESTAFF.getCode(), comprehensiveStaff());
                    roleToTasks.put(RoleTypeEnums.LOSSASSESSMENTENGINEER.getCode(), lossAssessmentEngineer());
                    roleToTasks.put(RoleTypeEnums.MAINTENANCEENGINEER.getCode(), maintenanceEngineer());
                    roleToTasks.put(RoleTypeEnums.TESTINGENGINEER.getCode(), testingEngineer());
                    roleToTasks.put(RoleTypeEnums.SENDER.getCode(), sender());

                    return roleToTasks.getOrDefault(role, new ArrayList<>());
                })
                .flatMap(List::stream)
                .collect(Collectors.toSet());

        // 对于ROOT和ADMIN角色，直接合并所有任务
        if (roles.contains(RoleTypeEnums.ROOT.getCode()) || roles.contains(RoleTypeEnums.ADMIN.getCode())) {
            personalToDoVO.addAll(receiving());
            personalToDoVO.addAll(customerService());
            personalToDoVO.addAll(insuranceAuditor());
            personalToDoVO.addAll(comprehensiveStaff());
            personalToDoVO.addAll(lossAssessmentEngineer());
            personalToDoVO.addAll(maintenanceEngineer());
            personalToDoVO.addAll(testingEngineer());
            personalToDoVO.addAll(sender());
        }
        ArrayList<PersonalToDoVO> personalToDoVOS = new ArrayList<>(personalToDoVO);
        personalToDoVOS.sort(Comparator.comparingInt(PersonalToDoVO::getCount).reversed());
        return BaseResponse.success(personalToDoVOS);
    }

    // 收寄人员
    public List<PersonalToDoVO> receiving(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 代收件
        String statusWaitExpressesCode = OrderStatusEnums.ORDER_STATUS_WAIT_EXPRESSES.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(statusWaitExpressesCode), orderRepository.countByStatus(statusWaitExpressesCode), statusWaitExpressesCode));
        // 返厂待收件
        String statusReturnWaitingReceiveCode = OrderStatusEnums.ORDER_STATUS_RETURN_WAITING_RECEIVE.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(statusReturnWaitingReceiveCode), orderRepository.countByStatus(statusReturnWaitingReceiveCode), statusReturnWaitingReceiveCode));
        // 返厂待寄件
        String orderStatusReturnWaitingExpressesCode = OrderStatusEnums.ORDER_STATUS_RETURN_WAITING_EXPRESSES.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusReturnWaitingExpressesCode), orderRepository.countByStatus(orderStatusReturnWaitingExpressesCode), orderStatusReturnWaitingExpressesCode));
        // 报废待处理
        String orderStatusWaitScrapHandleCode = OrderStatusEnums.ORDER_STATUS_WAIT_SCRAP_HANDLE.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitScrapHandleCode), orderRepository.countByStatus(orderStatusWaitScrapHandleCode), orderStatusWaitScrapHandleCode));
        return personalToDoVO;
    }

    // 客服
    public List<PersonalToDoVO> customerService(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 初版资料待审核
        PersonalToDoVO firstReview = new PersonalToDoVO();
        String insuranceFirstReviewCode = InsuranceStatusEnums.INSURANCE_FIRST_REVIEW.getCode();
        firstReview.setStatus(InsuranceStatusEnums.fromCode(insuranceFirstReviewCode));
        firstReview.setStatusCode(insuranceFirstReviewCode);
        Integer firstCount = orderRepository.countByReviewInsuranceStatus(insuranceFirstReviewCode);
        firstReview.setCount(firstCount);
        personalToDoVO.add(firstReview);
        // 终版资料待审核
        PersonalToDoVO lastWaitingReview = new PersonalToDoVO();
        String insuranceLastWaitingReviewCode = InsuranceStatusEnums.INSURANCE_LAST_WAITING_REVIEW.getCode();
        lastWaitingReview.setStatusCode(insuranceLastWaitingReviewCode);
        lastWaitingReview.setStatus(InsuranceStatusEnums.fromCode(insuranceLastWaitingReviewCode));
        Integer lastCount = orderRepository.countByReviewInsuranceStatus(insuranceLastWaitingReviewCode);
        lastWaitingReview.setCount(lastCount);
        personalToDoVO.add(lastWaitingReview);
        return personalToDoVO;
    }

    // 保险审核员
    public List<PersonalToDoVO> insuranceAuditor(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 设备保险待确认
        String orderStatusWaitConfirmInsuranceCode = OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_INSURANCE.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitConfirmInsuranceCode), orderRepository.countByStatus(orderStatusWaitConfirmInsuranceCode), orderStatusWaitConfirmInsuranceCode));
        // 工单取消待审核
        String orderStatusCancelWaiting1Code = OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING1.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusCancelWaiting1Code), orderRepository.countByStatus(orderStatusCancelWaiting1Code), orderStatusCancelWaiting1Code));
        // 撤销工单取消待审核
        String orderStatusCancelWaiting2Code = OrderStatusEnums.ORDER_STATUS_CANCEL_WAITING2.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusCancelWaiting2Code), orderRepository.countByStatus(orderStatusCancelWaiting2Code), orderStatusCancelWaiting2Code));
        // 保险资料状态理赔报案待完成
        String insuranceWaitingFinishCode = InsuranceStatusEnums.INSURANCE_WAITING_FINISH.getCode();
        personalToDoVO.add(new PersonalToDoVO("理赔报案待完成", orderRepository.countByReviewInsuranceStatus(insuranceWaitingFinishCode), insuranceWaitingFinishCode));
        return personalToDoVO;
    }

    // 综合员
    public List<PersonalToDoVO> comprehensiveStaff(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 定损待审核
        String orderStatusWaitLossAuditCode = OrderStatusEnums.ORDER_STATUS_WAIT_LOSS_REVIEW.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitLossAuditCode), orderRepository.countByStatus(orderStatusWaitLossAuditCode), orderStatusWaitLossAuditCode));
        // 报价待审核
        String orderStatusWaitingQuotationAuditCode = OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION_REVIEW.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitingQuotationAuditCode), orderRepository.countByStatus(orderStatusWaitingQuotationAuditCode), orderStatusWaitingQuotationAuditCode));
        // 待结算
        String orderStatusWaitingSettleCode = OrderStatusEnums.ORDER_STATUS_WAITING_SETTLE.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitingSettleCode), orderRepository.countByStatus(orderStatusWaitingSettleCode), orderStatusWaitingSettleCode));
        // 报废待审核
        String orderStatusWaitScrapReviewCode = OrderStatusEnums.ORDER_STATUS_WAIT_SCRAP_REVIEW.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitScrapReviewCode), orderRepository.countByStatus(orderStatusWaitScrapReviewCode), orderStatusWaitScrapReviewCode));
        return personalToDoVO;
    }

    // 定损工程师
    public List<PersonalToDoVO> lossAssessmentEngineer(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 待定损
        String orderStatusWaitConfirmLossCode = OrderStatusEnums.ORDER_STATUS_WAIT_CONFIRM_LOSS.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitConfirmLossCode), orderRepository.countByStatus(orderStatusWaitConfirmLossCode), orderStatusWaitConfirmLossCode));
        // 待报价
        String orderStatusWaitingQuotationCode = OrderStatusEnums.ORDER_STATUS_WAITING_QUOTATION.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitingQuotationCode), orderRepository.countByStatus(orderStatusWaitingQuotationCode), orderStatusWaitingQuotationCode));
        // 待派单
        String orderStatusWaitingDispatchCode = OrderStatusEnums.ORDER_STATUS_WAITING_DISPATCH.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitingDispatchCode), orderRepository.countByStatus(orderStatusWaitingDispatchCode), orderStatusWaitingDispatchCode));
        return personalToDoVO;
    }

    // 维修工程师
    public List<PersonalToDoVO> maintenanceEngineer(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 本地维修 维修中
        String uid = CommonUtils.getUid();
        String orderStatusMaintainingCode = OrderStatusEnums.ORDER_STATUS_MAINTAINING.getCode();
        Integer count = orderRepository.countByServiceMethodAndStatusAndMaintainerUid(OrderConstants.ORDER_SERVICE_METHOD_LOCAL_MAINTAIN, orderStatusMaintainingCode, uid);
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusMaintainingCode), count, orderStatusMaintainingCode));
        return personalToDoVO;
    }

    //测试工程师
    public List<PersonalToDoVO> testingEngineer(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 待检测
        String orderStatusWaitingCheckCode = OrderStatusEnums.ORDER_STATUS_WAITING_CHECK.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitingCheckCode), orderRepository.countByStatus(orderStatusWaitingCheckCode), orderStatusWaitingCheckCode));
        return personalToDoVO;
    }

    // 寄件员
    public List<PersonalToDoVO> sender(){
        List<PersonalToDoVO> personalToDoVO = new ArrayList<>();
        // 待寄件
        String orderStatusWaitingCustomerExpressesCode = OrderStatusEnums.ORDER_STATUS_WAITING_CUSTOMER_EXPRESSES.getCode();
        personalToDoVO.add(new PersonalToDoVO(OrderStatusEnums.fromCode(orderStatusWaitingCustomerExpressesCode), orderRepository.countByStatus(orderStatusWaitingCustomerExpressesCode), orderStatusWaitingCustomerExpressesCode));
        return personalToDoVO;
    }

    public static List<DateAndTimestampRange> createDateAndTimestampRanges() {
        List<DateAndTimestampRange> dateRanges = new ArrayList<>();

        LocalDate currentDate = LocalDate.now();
        for (int i = 0; i < 15; i++) {
            LocalDate previousDate = currentDate.minusDays(i);
            LocalDateTime startOfDay = previousDate.atStartOfDay();
            long startTimestamp = startOfDay.toEpochSecond(ZoneOffset.UTC) * 1000;
            LocalDateTime endOfDay = previousDate.atTime(23, 59, 59, 999999999);
            long endTimestamp = endOfDay.toEpochSecond(ZoneOffset.UTC) * 1000;

            dateRanges.add(new DateAndTimestampRange(previousDate, startTimestamp, endTimestamp));
        }
        return dateRanges;
    }
}
