package com.thinglinks.uav.workbench.service;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.workbench.dto.OrderScaleVO;
import com.thinglinks.uav.workbench.dto.OrderStatisticsVO;
import com.thinglinks.uav.workbench.dto.PersonalToDoVO;
import com.thinglinks.uav.workbench.dto.TrendVO;

import java.util.List;

/**
 * @author huiyongchen
 */
public interface WorkbenchService {
    /**
     * 工作台工单统计
     * @param
     * @return
     */
    BaseResponse<OrderStatisticsVO> orderStatistics();

    BaseResponse<List<TrendVO>> orderTrend();

    BaseResponse<Integer> relatedOrder(String status);

    BaseResponse<OrderScaleVO> scale();

    BaseResponse<List<PersonalToDoVO>> personalToDo();
}
