package com.thinglinks.uav.workbench.controller;

import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.workbench.dto.OrderScaleVO;
import com.thinglinks.uav.workbench.dto.OrderStatisticsVO;
import com.thinglinks.uav.workbench.dto.PersonalToDoVO;
import com.thinglinks.uav.workbench.dto.TrendVO;
import com.thinglinks.uav.workbench.service.WorkbenchService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: 工作台接口
 * @date 2024/4/3 18:03
 */
@Api(tags = "工作台")
@RestController
@RequestMapping(value = "workbench")
@Slf4j
public class WorkbenchController {

    @Resource
    private WorkbenchService workbenchService;

    //工作台
    @Operation(summary = "工单数据统计")
    @GetMapping(path = "orderStatistics")
    public BaseResponse<OrderStatisticsVO> orderStatistics() {
        return workbenchService.orderStatistics();
    }

    @Operation(summary = "工单趋势")
    @GetMapping(path = "orderTrend")
    public BaseResponse<List<TrendVO>> orderTrend() {
        return workbenchService.orderTrend();
    }

    @Operation(summary = "维保工单类型占比")
    @GetMapping(path = "scale")
    public BaseResponse<OrderScaleVO> scale() {
        return workbenchService.scale();
    }

    @Operation(summary = "个人待办")
    @GetMapping(path = "personalToDo")
    public BaseResponse<List<PersonalToDoVO>> personalToDo() {
        return workbenchService.personalToDo();
    }
}
