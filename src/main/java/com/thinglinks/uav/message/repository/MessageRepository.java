package com.thinglinks.uav.message.repository;

import com.thinglinks.uav.message.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * (Messages)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-21 16:39:44
 */
public interface MessageRepository extends JpaRepository<Message, Integer> {


}


