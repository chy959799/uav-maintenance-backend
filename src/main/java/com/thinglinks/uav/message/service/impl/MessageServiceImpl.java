package com.thinglinks.uav.message.service.impl;

import com.thinglinks.uav.common.constants.MessageConstants;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.message.entity.Message;
import com.thinglinks.uav.message.repository.MessageRepository;
import com.thinglinks.uav.message.service.MessageService;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.system.config.CustomConfig;
import com.thinglinks.uav.user.entity.User;
import com.thinglinks.uav.user.repository.UserRepository;
import com.thinglinks.uav.user.service.UserMessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * (Messages)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-21 16:39:44
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Resource
    private MessageRepository messageRepository;
    @Resource
    private OrderRepository orderRepository;
    @Resource
    private DeviceRepository deviceRepository;
    @Resource
    private UserMessageService userMessageService;
    @Resource
    private UserRepository userRepository;
    @Resource
    private MessageService messageService;
    @Resource
    private CustomConfig customConfig;


    @Override
    public void sendMessage(String title, String content, String type, String level, String ordid, List<String> uidList, String uid) {
        String msgid = CommonUtils.uuid();
        Message message = new Message();
        message.setMsgid(msgid);
        message.setTitle(title);
        message.setContent(content);
        message.setType(type);
        message.setLevel(level);
        message.setUid(uid);
        messageRepository.save(message);

        userMessageService.insert(msgid, uidList);
    }

    @Override
    public void sendNormalMessage(String title, String content, String level, String receiverId) {
        List<String> userIds = getUserIdList(level, receiverId);
        String uid = CommonUtils.getUid();
        messageService.sendMessage(title, content, MessageConstants.TYPE_ORDER, level, null, userIds, uid);
    }

    @Override
    public void sendNormalMessage(String title, String content, String level, String receiverId, String uid) {
        List<String> userIds = getUserIdList(level, receiverId);
        messageService.sendMessage(title, content, MessageConstants.TYPE_ORDER, level, null, userIds, uid);
    }

    @Override
    public void sendNormalMessage(String title, String content, String level, List<String> uidList) {
        String uid = CommonUtils.getUid();
        messageService.sendMessage(title, content, MessageConstants.TYPE_ORDER, level, null, uidList, uid);
    }

    @Override
    public void sendNormalMessage(String title, String content, String level, List<String> uidList, String uid) {
        messageService.sendMessage(title, content, MessageConstants.TYPE_ORDER, level, null, uidList, uid);
    }

    @Override
    public void sendSystemOrderMessage(String title, String content, String level, String ordid, String receiverId) {
        List<String> userIds = getUserIdList(level, receiverId);
        Order order = orderRepository.getByOrdid(ordid);
        if (order != null) {
            String url = buildUrl(order, MessageConstants.SYSTEM_ORDER_URL);
            title = "【" + url + "】" + title;
            content = "<div>工单" + "【" + url + "】" + content + "</div>";
        }
        sendOrderMessage(title, content, level, ordid, userIds);
    }

    @Override
    public void sendPersonalOrderMessage(String title, String content, String level, String ordid, String receiverId) {
        List<String> userIds = getUserIdList(level, receiverId);
        Order order = orderRepository.getByOrdid(ordid);
        if (order != null) {
            String url = buildUrl(order, MessageConstants.PERSONAL_ORDER_URL);
            title = "【" + url + "】" + title;
            Device device = deviceRepository.findByDevid(order.getDevid());
            content = "<div>您的服务工单（工单编号 " + url + ", 设备SN码" + device.getCode() + "）" + content + "</div>";
        }
        sendOrderMessage(title, content, level, ordid, userIds);
    }

    @Override
    public void sendOrderMessage(String title, String content, String level, String ordid, List<String> uidList) {
        String uid = CommonUtils.getUid();
        messageService.sendMessage(title, content, MessageConstants.TYPE_ORDER, level, ordid, uidList, uid);
    }

    @Override
    public void sendInsuranceMessage(String title, String content, String level, String receiverId) {
        messageService.sendNormalMessage(title, content, level, receiverId);
    }

    /**
     * @param project 项目
     * @return
     */

    @Override
    public String generateProjectAlarmContent(Project project, String content) {
        if (project != null) {
            String url = buildProjectUrl(project, MessageConstants.PROJECT_URL);
            content = "<div>项目" + "【" + url + "】" + content + "</div>";
            return content;
        }
        return "";
    }

    @Override
    public String generateOrderAlarmWithName(Order order, String tag, String validity, String name) {
//		String url = String.format("<a href='%s%s'>%s</a>", MessageConstants.SYSTEM_ORDER_URL, order.getOrdid(), order.getInnerCode());
//		return "<div>工单【" + url + "】" + tag + "，【" + validity + "】，请联系工程师【" + name + "】尽快处理</div>";
        return "工单【" + order.getOrdid() + "】" + tag + "，【" + validity + "】，请联系工程师【" + name + "】尽快处理";
    }

    @Override
    public String generateOrderAlarm(Order order, String tag, String validity) {
//		String url = String.format("<a href='%s%s'>%s</a>", MessageConstants.SYSTEM_ORDER_URL, order.getOrdid(), order.getInnerCode());
//		return "<div>工单【" + url + "】" + tag + "，【" + validity + "】";
        return "工单【" + order.getOrdid() + "】" + tag + "," + validity;
    }

    /**
     * 根据级别和接收者ID获取用户ID列表
     */
    private List<String> getUserIdList(String level, String receiverId) {
        switch (level) {
            case MessageConstants.LEVEL_PERSONAL:
                return Collections.singletonList(receiverId);

            case MessageConstants.LEVEL_DEPARTMENT:
                return userRepository.findAllByDid(receiverId)
                        .stream()
                        .map(User::getUid)
                        .collect(Collectors.toList());

            case MessageConstants.LEVEL_COMPANY:
                return userRepository.findAllByCpid(receiverId)
                        .stream()
                        .map(User::getUid)
                        .collect(Collectors.toList());

            case MessageConstants.LEVEL_SYSTEM:
                return userRepository.findAll()
                        .stream()
                        .map(User::getUid)
                        .collect(Collectors.toList());

            default:
                return Collections.emptyList();
        }
    }

    /**
     * 构造跳转链接
     */
    private String buildUrl(Order order, String jumpUrl) {
        String innerCode = order.getInnerCode();
        jumpUrl = "http://" + customConfig.getUavServiceIp() + jumpUrl;
        return String.format("<a href='%s%s'>%s</a>", jumpUrl, order.getOrdid(), innerCode);
    }

    /**
     * 构造项目详情跳转链接
     */
    private String buildProjectUrl(Project project, String jumpUrl) {
        String pjid = project.getPjid();
//		jumpUrl = "http://" + customConfig.getUavServiceIp() + jumpUrl;
        return String.format("<a href='%s%s'>%s</a>", jumpUrl, project.getPjid(), project.getName());
    }


}


