package com.thinglinks.uav.message.service;

import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.project.entity.Project;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (Messages)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-21 16:39:44
 */
public interface MessageService {


	/**
	 * 发送消息
	 * @param title 消息标题
	 * @param content 消息内容
	 * @param type 消息类型
	 * @param level 消息级别
	 * @param ordid 工单id
	 * @param uidList 接收用户ID列表
	 */
	@Transactional
	void sendMessage(String title, String content, String type, String level, String ordid, List<String> uidList, String uid);

	/**
	 * 发送普通消息
	 * @param title 消息标题
	 * @param content 消息内容
	 * @param level 消息级别
	 * @param receiverId 接收者ID
	 */
	void sendNormalMessage(String title, String content, String level, String receiverId);

	void sendNormalMessage(String title, String content, String level, String receiverId, String uid);

	/**
	 * 发送普通消息
	 * @param title 消息标题
	 * @param content 消息内容
	 * @param level 消息级别
	 * @param uidList 接收用户ID列表
	 */
	void sendNormalMessage(String title, String content, String level, List<String> uidList);

	void sendNormalMessage(String title, String content, String level, List<String> uidList, String uid);

	/**
	 * 发送系统侧工单消息
	 * @param title 消息标题
	 * @param content 消息内容
	 * @param level 消息级别
	 * @param ordid 工单id
	 * @param receiverId 接收者ID
	 */
	void sendSystemOrderMessage(String title, String content, String level, String ordid, String receiverId);

	/**
	 * 发送用户侧工单消息
	 * @param title 消息标题
	 * @param content 消息内容
	 * @param level 消息级别
	 * @param ordid 工单id
	 * @param receiverId 接收者ID
	 */
	void sendPersonalOrderMessage(String title, String content, String level, String ordid, String receiverId);

	/**
	 * 发送工单消息
	 * @param title 消息标题
	 * @param content 消息内容
	 * @param level 消息级别
	 * @param ordid 工单id
	 * @param uidList 接收者ID列表
	 */
	void sendOrderMessage(String title, String content, String level, String ordid, List<String> uidList);

	void sendInsuranceMessage(String title, String content, String level, String receiverId);

	/**
	 * 生成项目消息内容
	 * @param project 项目
	 * @return 消息内容
	 */
	String generateProjectAlarmContent(Project project,String content);

	String generateOrderAlarmWithName(Order order, String tag, String validity, String name);

	String generateOrderAlarm(Order order, String tag, String validity);
}


