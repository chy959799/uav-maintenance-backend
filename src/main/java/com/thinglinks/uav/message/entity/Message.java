package com.thinglinks.uav.message.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * (Messages)实体类
 *
 * @author iwiFool
 * @since 2024-03-21 16:39:44
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "messages")
@Data
public class Message extends BaseEntity {

	/**
	 * 消息内部ID
	 */
	private String msgid;
	/**
	 * 消息标题
	 */
	private String title;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 消息类型1:普通消息 2:工单消息
	 */
	private String type;
	/**
	 * 消息级别 1:个人级 2:部门级 3:公司级 4:系统级
	 */
	private String level;

	private String uid;

	private String ordid;
}
