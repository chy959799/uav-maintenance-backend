package com.thinglinks.uav.project.mapper;

import com.thinglinks.uav.project.dto.OrderPage;
import com.thinglinks.uav.project.dto.OrderPageQuery;
import com.thinglinks.uav.project.dto.OrderProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * @package: com.thinglinks.uav.project.mapper
 * @className: OrderMapper
 * @author: rp
 * @description: TODO
 */
@Mapper
public interface OrderMapper{
    @SelectProvider(type= OrderProvider.class, method = "findOrdersByManyCondition")
    List<OrderPage> getOrderPageList(@Param("pjid") String pjid, @Param("request")OrderPageQuery request);

}
