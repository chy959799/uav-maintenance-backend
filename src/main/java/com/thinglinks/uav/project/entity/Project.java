package com.thinglinks.uav.project.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.customer.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * (Projects)实体类
 *
 * @author HHG
 * @since 2024-03-16 14:56:56
 */
@Entity
@Table(name = "projects")
@Data
public class Project extends BaseEntity {
    /**
     * 项目ID
     */
    @Column(name = "pjid")
    private String pjid;
    /**
     * 项目名称
     */
    @Column(name = "name")
    private String name;
    /**
     * 项目类型 1: 框架合同 2: 独立合同 3：系统默认（小程序用户工单自动挂载到默认项目）
     */
    @Column(name = "type")
    private String type;
    /**
     * 项目编号
     */
    @Column(name = "code")
    private String code;
    /**
     * 内部项目编号
     */
    @Column(name = "inner_code")
    private String innerCode;
    /**
     * 联系人
     */
    @Column(name = "contact")
    private String contact;
    /**
     * 联系人电话
     */
    @Column(name = "mobile")
    private String mobile;
    /**
     * 省编码
     */
    @Column(name = "province")
    private String province;
    /**
     * 市编码
     */
    @Column(name = "city")
    private String city;
    /**
     * 区编码
     */
    @Column(name = "county")
    private String county;
    /**
     * 详细地址
     */
    @Column(name = "detail")
    private String detail;
    /**
     * 项目开始时间
     */
    @Column(name = "start_time")
    private Long startTime;
    /**
     * 项目结束时间
     */
    @Column(name = "end_time")
    private Long endTime;
    /**
     * 项目金额
     */
    @Column(name = "total_price")
    private double totalPrice;
    /**
     * 项目备注
     */
    @Column(name = "remark")
    private String remark;
    /**
     * 项目描述
     */
    @Column(name = "descript")
    private String descript;
    /**
     * 启用状态
     */
    @Column(name = "enable")
    private Boolean enable;
    /**
     * 过期状态
     */
    @Column(name = "active")
    private Boolean active;

    @Column(name = "cid")
    private String cid;

    @Column(name = "uid")
    private String uid;

    @Column(name = "cpid")
    private String cpid;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "单位")
    @JoinColumn(name = "cid", referencedColumnName = "cid", insertable = false, updatable = false)
    private Customer customer;

}

