package com.thinglinks.uav.project.service;

import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.project.dto.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @package: com.thinglinks.uav.projects.service
 * @className: ProjectsService
 * @author: rp
 * @description: TODO
 */
public interface ProjectService {
    BaseResponse<Void> addProject(AddProject addProject) ;

    BaseResponse<Void> deleteProject(String id);

    BaseResponse<Void> editProject(String id, EditProject request);

    BaseResponse<ProjectDetail> getProjectDetail(String id);

    BasePageResponse<ProjectPage> queryProjects(ProjectPageQuery request);

    ProjectOrderPage<ProjectOrderVO> getOrders(String id, OrderPageQuery request);

    BaseResponse<Void> enableProject(String id, EnableProject request);

    BaseResponse<Void> deleteProjects(ProjectDTO pjids);

    void exportOrder(HttpServletResponse response, String id, OrderPageQuery request) throws Exception;

    BasePageResponse<ProjectMaterialConsumptionDTO> statisticsProductsUseCondition(String projectId, BasePageRequest request);

    void exportCondition(HttpServletResponse response, String pjid) throws Exception;

    BasePageResponse<ProjectMaterialConsumptionDetail> statisticsProductCount(ProjectMaterialConsumptionDetailRequest request);
}
