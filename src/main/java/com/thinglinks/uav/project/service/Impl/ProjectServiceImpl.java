package com.thinglinks.uav.project.service.Impl;

import com.aliyun.credentials.utils.StringUtils;
import com.google.common.collect.Lists;
import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.category.repository.CategoryRepository;
import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.PageMeta;
import com.thinglinks.uav.common.enums.StatusEnum;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.EasyExcelUtils;
import com.thinglinks.uav.customer.entity.Device;
import com.thinglinks.uav.customer.repository.DeviceRepository;
import com.thinglinks.uav.order.dto.PageOrder;
import com.thinglinks.uav.order.dto.PageOrderQuery;
import com.thinglinks.uav.order.dto.excel.CellEnum;
import com.thinglinks.uav.order.dto.excel.ExportCell;
import com.thinglinks.uav.order.entity.Order;
import com.thinglinks.uav.order.entity.OrderQuote;
import com.thinglinks.uav.order.entity.OrderQuoteItem;
import com.thinglinks.uav.order.enums.OrderStatusEnums;
import com.thinglinks.uav.order.repository.ExpressesRepository;
import com.thinglinks.uav.order.repository.OrderQuoteItemRepository;
import com.thinglinks.uav.order.repository.OrderQuoteRepository;
import com.thinglinks.uav.order.repository.OrderRepository;
import com.thinglinks.uav.order.service.common.InnerService;
import com.thinglinks.uav.order.service.impl.OrderServiceImpl;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.product.repository.ProductRepository;
import com.thinglinks.uav.project.dto.*;
import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.mapper.OrderMapper;
import com.thinglinks.uav.project.repository.ProjectRepository;
import com.thinglinks.uav.project.service.ProjectService;
import com.thinglinks.uav.project.utils.ProjectSpecifications;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import com.thinglinks.uav.spare.repository.ProjectProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.thinglinks.uav.common.constants.OrderConstants.QUOTE_TYPE_COST;

/**
 * @package: com.thinglinks.uav.projects.service.Impl
 * @className: ProjectsServiceImpl
 * @author: rp
 * @description: TODO
 */
@Service
public class ProjectServiceImpl implements ProjectService {
    private static final Logger log = LoggerFactory.getLogger(ProjectServiceImpl.class);
    @Resource
    private ProjectProductsRepository projectProductsRepository;

    @Resource
    private ProjectRepository projectRepository;

    @Resource
    private CategoryRepository categoryRepository;

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private OrderServiceImpl orderService;

    @Resource
    private InnerService innerService;

    @Resource
    private DeviceRepository deviceRepository;

    @Resource
    private ProductRepository productRepository;

    @Resource
    private OrderQuoteItemRepository orderQuoteItemRepository;

    @Resource
    private OrderQuoteRepository orderQuoteRepository;

    @Resource
    private ExpressesRepository expressesRepository;

    @Override
    @Transactional
    public BaseResponse<Void> addProject(AddProject request) {
        //判断是否有该项目存在
        Project p = projectRepository.findByCode(request.getCode());
        if (p != null) {
            return BaseResponse.fail("项目编号已存在");
        }
        Project addProject = new Project();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        request.setStartTime(request.getStartTime() + " 00:00:00");
        String startTime = request.getStartTime();
        request.setEndTime(request.getEndTime() + " 00:00:00");
        String endTime = request.getEndTime();
        try {
            addProject.setStartTime(simpleDateFormat.parse(startTime).getTime());
            addProject.setEndTime(simpleDateFormat.parse(endTime).getTime());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        addProject.setPjid(CommonUtils.uuid());
        addProject.setName(request.getName());
        addProject.setCode(request.getCode());
        addProject.setInnerCode(request.getInnerCode());
        addProject.setTotalPrice(request.getTotalPrice());
        addProject.setProvince(request.getProvince());
        addProject.setCity(request.getCity());
        addProject.setCounty(request.getCounty());
        addProject.setDetail(request.getDetail());
        addProject.setType(request.getType());
        addProject.setRemark(request.getRemark());
        addProject.setDescript(request.getDescript());
        addProject.setContact(request.getContact());
        addProject.setMobile(request.getMobile());
        addProject.setCid(request.getCid());
        addProject.setEnable(true);
        addProject.setCpid(CommonUtils.getCpid());
        addProject.setActive(addProject.getEndTime() > System.currentTimeMillis());
        projectRepository.save(addProject);
        ProjectProduct pp = new ProjectProduct();
        if (request.getProjectProductList() != null && request.getProjectProductList().size() > 0) {
            request.getProjectProductList().forEach(projectProduct -> {
                BeanUtils.copyProperties(projectProduct, pp);
                pp.setPjid(addProject.getPjid());
                pp.setPpid(CommonUtils.uuid());
                projectProductsRepository.save(pp);
            });
        }
        return BaseResponse.success();
    }

    @Transactional
    @Override
    public BaseResponse<Void> deleteProject(String id) {
        if (!projectRepository.existsByPjid(id)) {
            return BaseResponse.fail("项目不存在");
        }
        projectRepository.deleteByPjid(id);
        return BaseResponse.success();
    }

    @Transactional
    @Override
    public BaseResponse<Void> deleteProjects(ProjectDTO pjids) {
        if (projectRepository.existsByPjidIn(pjids.getIds())) {
            return BaseResponse.fail("该项目含有关联工单");
        }
        projectRepository.deleteAllByPjidIn(pjids.getIds());
        return BaseResponse.success();
    }

    @Override
    public void exportOrder(HttpServletResponse response, String id, OrderPageQuery request) throws Exception {
        List<ProjectOrderVO> orderVOList = getOrders(id, request).getResults();
        List<List<String>> head = new ArrayList<>();
        head.add(Lists.newArrayList("序号"));
        head.add(Lists.newArrayList("工单编号"));
        head.add(Lists.newArrayList("工单类型"));
        head.add(Lists.newArrayList("服务类型"));
        head.add(Lists.newArrayList("保险类型"));
        head.add(Lists.newArrayList("工单状态"));
        head.add(Lists.newArrayList("成本价"));
        head.add(Lists.newArrayList("客户价"));
        List<ExportCell> fields = CellEnum.getCells();
        fields.forEach(x -> {
            head.add(Lists.newArrayList(x.getTitle()));
        });
        List<List<Object>> data = new ArrayList<>();
        int size = orderVOList.size();
        for (int i = 0; i < size; i++) {
            Order order = orderVOList.get(i);
            ProjectOrderVO orderVO = orderVOList.get(i);
            Device device = Optional.ofNullable(deviceRepository.findByDevid(order.getDevid())).orElse(new Device());
            ;
            List<Object> row = new ArrayList<>();
            row.add(i + 1);
            row.add(order.getInnerCode());
            row.add(CommonUtils.orderTypeConvert(order.getType()));
            row.add(CommonUtils.orderServiceTypeConvert(order.getServiceType()));
            row.add(CommonUtils.orderInsuranceTypeConvert(device.getInsuranceType()));
            row.add(OrderStatusEnums.fromCode(order.getStatus()));
            row.add(orderVO.getCostPrice());
            row.add(orderVO.getCustomerPrice());
            innerService.getOneLow(order, fields, row);
            data.add(row);
        }
        EasyExcelUtils.exportXlsx(response, "项目工单信息", "Sheet1", head, data);
    }

    /**
     * @param projectId
     * @param request
     * @return
     */
    @Override
    public BasePageResponse<ProjectMaterialConsumptionDTO> statisticsProductsUseCondition(String projectId, BasePageRequest request) {
        List<ProjectMaterialConsumptionDTO> result = getProductsUseConditions(projectId);
        List<ProjectMaterialConsumptionDTO> lastResult = result.stream().skip(request.getOffset()).limit(request.getLimit()).collect(Collectors.toList());
        return customePaging(request.getOffset(), request.getLimit(), lastResult);
    }

    private List<ProjectMaterialConsumptionDTO> getProductsUseConditions(String pjid) {
        List<Order> orderList = orderRepository.findAllByPjid(pjid);
        List<OrderQuote> orderQuotes = orderList.stream().map(o -> orderQuoteRepository.findByOrdidAndType(o.getOrdid(), QUOTE_TYPE_COST))
                .flatMap(List::stream).collect(Collectors.toList());
        List<ProjectMaterialConsumptionDTO> projectMaterialConsumptionDTOS = orderQuotes.stream().map(oq -> {
            List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(oq.getOqid());
            return orderQuoteItems.stream().map(oqi -> {
                ProjectMaterialConsumptionDTO projectMaterialConsumptionDTO = new ProjectMaterialConsumptionDTO();
                Product product = productRepository.findByPid(oqi.getPid());
                projectMaterialConsumptionDTO.setProductName(product.getName());
                projectMaterialConsumptionDTO.setProductCode(product.getCode());
                projectMaterialConsumptionDTO.setOrigin(product.getOrigin());
                projectMaterialConsumptionDTO.setFactoryPrice(product.getFactoryPrice());
                projectMaterialConsumptionDTO.setUnit(product.getUnit());
                projectMaterialConsumptionDTO.setFrequency(oqi.getCount());
                projectMaterialConsumptionDTO.setDescription(product.getDescription());
                projectMaterialConsumptionDTO.setPid(oqi.getPid());
                projectMaterialConsumptionDTO.setCatid(oqi.getCatid());
                return projectMaterialConsumptionDTO;
            }).collect(Collectors.toList());
        }).flatMap(List::stream).collect(Collectors.toList());
        Map<String, Map<String, List<ProjectMaterialConsumptionDTO>>> groupedMap = projectMaterialConsumptionDTOS.stream()
                .collect(Collectors.groupingBy(ProjectMaterialConsumptionDTO::getCatid,
                        Collectors.groupingBy(ProjectMaterialConsumptionDTO::getPid)));
        Map<String, Map<String, ProjectMaterialConsumptionDTO>> desiredMap = groupedMap.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().entrySet().stream()
                                .collect(Collectors.toMap(
                                        Map.Entry::getKey,
                                        e -> {
                                            int totalFrequency = e.getValue().stream()
                                                    .mapToInt(ProjectMaterialConsumptionDTO::getFrequency)
                                                    .sum();
                                            ProjectMaterialConsumptionDTO dto = e.getValue().get(0);
                                            dto.setFrequency(totalFrequency);
                                            return dto;
                                        }))));
        List<ProjectMaterialConsumptionDTO> result = new ArrayList<>();
        desiredMap.forEach((catid, pidMap) -> pidMap.forEach((pid, dto) -> result.add(dto)));
        return result;
    }

    public static <T> BasePageResponse<T> customePaging(int offset, int limit, List<T> result) {
        BasePageResponse<T> response = new BasePageResponse<>();
        response.setCode(StatusEnum.SUCCESS.getCode());
        response.setMsg(StatusEnum.SUCCESS.getMessage());
        response.setResults(result);
        PageMeta meta = new PageMeta();
        meta.setOffset(offset);
        meta.setLimit(limit);
        meta.setCount((long) result.size());
        meta.setOrder("-default");
        response.setMeta(meta);
        return response;
    }


    @Override
    public void exportCondition(HttpServletResponse response, String pjid) throws Exception {
        List<ProjectMaterialConsumptionDTO> exportDate = getProductsUseConditions(pjid);
        EasyExcelUtils.exportExcel(response, "维修消耗的物料情况", "维修消耗的物料情况", exportDate, ProjectMaterialConsumptionDTO.class);
    }

    @Override
    public BasePageResponse<ProjectMaterialConsumptionDetail> statisticsProductCount(ProjectMaterialConsumptionDetailRequest request) {
        Project project = projectRepository.findByPjid(request.getPjid());
        List<Order> orderList = orderRepository.findAllByPjid(request.getPjid());
        List<ProjectMaterialConsumptionDetail> projectMaterialConsumptionDetails = new ArrayList<>();
        orderList.forEach(o -> {
            ProjectMaterialConsumptionDetail detail = new ProjectMaterialConsumptionDetail();
            Category category = categoryRepository.findByCatid(request.getCatid());
            Product product = productRepository.findByPid(request.getPid());
            List<ProductsUseCondition> productsUseConditionList = new ArrayList<>();
            detail.setProjectName(project.getName());
            detail.setOrderCode(o.getInnerCode());
            detail.setOrderType(o.getType().equals("1") ? "维修" : "保养");
            detail.setOrderServiceType(o.getServiceType());
            detail.setProductCategory(category.getName());
            detail.setProductCode(product.getCode());
            detail.setProductName(product.getName());
            List<OrderQuote> orderQuotes = orderQuoteRepository.findByOrdidAndType(o.getOrdid(), QUOTE_TYPE_COST);
            orderQuotes.forEach(oq -> {
                List<OrderQuoteItem> orderQuoteItems = orderQuoteItemRepository.findByOqid(oq.getOqid());
                orderQuoteItems.forEach(item -> {
                    if (item.getCatid().equals(request.getCatid()) && item.getPid().equals(request.getPid())){
                        ProductsUseCondition p = new ProductsUseCondition();
                        p.setServiceMethod(o.getServiceMethod().equals("-1") ? "本地维修" : "返厂维修");
                        p.setCategoryName(category.getName());
                        p.setProductName(product.getName());
                        p.setCode(product.getCode());
                        p.setCount(item.getCount());
                        p.setSalePrice(item.getSalePrice());
                        p.setWorkHour(item.getWorkHour());
                        p.setWorkHourUnitCost(item.getWorkHourCost());
                        p.setTotalCost(item.getTotalCost());
                        productsUseConditionList.add(p);
                    }
                });
            });
            detail.setProductCount(productsUseConditionList.size());
            detail.setProductsUseConditions(productsUseConditionList);
            projectMaterialConsumptionDetails.add(detail);
        });
        List<ProjectMaterialConsumptionDetail> result = projectMaterialConsumptionDetails.stream().filter(detail -> !detail.getProductsUseConditions().isEmpty()).collect(Collectors.toList());

        List<ProjectMaterialConsumptionDetail> lastResult = result.stream().skip(request.getOffset()).limit(request.getLimit()).collect(Collectors.toList());
        return customePaging(request.getOffset(), request.getLimit(),lastResult);
    }

    @Override
    public BaseResponse<Void> editProject(String id, EditProject request) {
        if (projectRepository.findByPjid(id) == null) {
            return BaseResponse.fail("项目不存在");
        }
        Project project = projectRepository.findByPjid(id);
        project.setInnerCode(request.getInnerCode());
        project.setContact(request.getContact());
        project.setMobile(request.getMobile());
        project.setProvince(request.getProvince());
        project.setCity(request.getCity());
        project.setCounty(request.getCounty());
        project.setRemark(request.getRemark());
        project.setDetail(request.getDetail());
        project.setDescript(request.getDescript());
        project.setTotalPrice(request.getTotalPrice());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startTime = (request.getStartTime() + " 00:00:00");
        String endTime = (request.getEndTime() + " 00:00:00");
        try {
            if (request.getEndTime() == null) {
                project.setEndTime(null);
            } else {
                project.setEndTime(simpleDateFormat.parse(endTime).getTime());
            }
            if (request.getStartTime() == null) {
                project.setStartTime(null);
            } else {
                project.setStartTime(simpleDateFormat.parse(startTime).getTime());
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        project.setActive(project.getEndTime() > System.currentTimeMillis());
        projectRepository.save(project);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<ProjectDetail> getProjectDetail(String id) {
        Project project = projectRepository.findByPjid(id);
        ProjectDetail projectDetail = new ProjectDetail();
        BeanUtils.copyProperties(project, projectDetail);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        projectDetail.setStartTime(simpleDateFormat.format(project.getStartTime()));
        projectDetail.setEndTime(simpleDateFormat.format(project.getEndTime()));
        return BaseResponse.success(projectDetail);
    }

    @Override
    public BasePageResponse<ProjectPage> queryProjects(ProjectPageQuery request) {
        PageRequest pageRequest = request.of();
        Specification<Project> project = ProjectSpecifications.searchProject(request);
        Page<Project> page = projectRepository.findAll(project, pageRequest);
        List<ProjectPage> results = page.stream().map(x -> {
            ProjectPage response = new ProjectPage();
            BeanUtils.copyProperties(x, response);
            response.setProjectCode(x.getCode());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            response.setStartTime(x.getStartTime() == null ? null : simpleDateFormat.format(x.getStartTime()));
            response.setEndTime(x.getEndTime() == null ? null : simpleDateFormat.format(x.getEndTime()));
            return response;
        }).collect(Collectors.toList());
        return BasePageResponse.success(page, results);
    }

    @Override
    public ProjectOrderPage<ProjectOrderVO> getOrders(String id, OrderPageQuery request) {
        PageOrderQuery pageOrderQuery = new PageOrderQuery();
        pageOrderQuery.setPjid(id);
        BasePageResponse<PageOrder> pageOrderBasePageResponse = orderService.pageOrder(pageOrderQuery);
        List<PageOrder> results = pageOrderBasePageResponse.getResults();
        if (!StringUtils.isEmpty(request.getStatus())) {
            results = results.stream().filter(x -> x.getStatus().equals(request.getStatus())).collect(Collectors.toList());
        }
        if (!StringUtils.isEmpty(request.getServiceType())) {
            results = results.stream().filter(x -> x.getServiceType().equals(request.getServiceType())).collect(Collectors.toList());
        }
        List<ProjectOrderVO> list = results.stream().map(x -> {
            List<BigDecimal> costPriceList = new ArrayList<>();
            List<BigDecimal> customerPriceList = new ArrayList<>();
            Integer order = 0;
            List<OrderQuote> orderQuoteList = orderQuoteRepository.findByOrdid(x.getOrdid());
            for (int i = 0; i < orderQuoteList.size(); i++) {
                OrderQuote orderQuote = orderQuoteList.get(i);
                if (orderQuote.getType().equals("1")) {
                    if (orderQuote.getOrder() > order) {
                        order = orderQuote.getOrder();
                    }
                } else {
                    customerPriceList.add(BigDecimal.valueOf(orderQuote.getOtherPrice()));
                    customerPriceList.add(BigDecimal.valueOf(orderQuote.getSparePrice()));
                    customerPriceList.add(BigDecimal.valueOf(orderQuote.getExpressesPrice()));
                }
            }
            Integer finalOrder = order;
            orderQuoteList.forEach(orderQuote -> {
                if (orderQuote.getType().equals("1") && null != orderQuote.getOrder() && orderQuote.getOrder().equals(finalOrder)) {
                    costPriceList.add(BigDecimal.valueOf(orderQuote.getOtherPrice()));
                    costPriceList.add(BigDecimal.valueOf(orderQuote.getSparePrice()));
                    costPriceList.add(BigDecimal.valueOf(orderQuote.getExpressesPrice()));
                }
            });
            ProjectOrderVO projectOrderVO = new ProjectOrderVO();
            BeanUtils.copyProperties(x, projectOrderVO);
            projectOrderVO.setCostPrice(costPriceList.stream().reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());
            projectOrderVO.setCustomerPrice(customerPriceList.stream().reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());
            projectOrderVO.setPrice(BigDecimal.valueOf(projectOrderVO.getCostPrice()).add(BigDecimal.valueOf(projectOrderVO.getCustomerPrice())).doubleValue());
            return projectOrderVO;
        }).collect(Collectors.toList());
        int start = request.getOffset() * request.getLimit();
        List<ProjectOrderVO> result = new ArrayList<>();
        int end = Math.min((start + request.getLimit()), list.size());
        for (int i = start; i < end; i++) {
            result.add(list.get(i));
        }
        ProjectOrderPage projectOrder = new ProjectOrderPage();
        BeanUtils.copyProperties(pageOrderBasePageResponse, projectOrder);
        projectOrder.setResults(result);
        BigDecimal costTotalPrice = BigDecimal.ZERO;
        BigDecimal customerTotalPrice = BigDecimal.ZERO;
        for (int i = 0; i < result.size(); i++) {
            ProjectOrderVO projectOrderVO = result.get(i);
            costTotalPrice = costTotalPrice.add(BigDecimal.valueOf(projectOrderVO.getCostPrice()));
            customerTotalPrice = customerTotalPrice.add(BigDecimal.valueOf(projectOrderVO.getCustomerPrice()));
        }
        projectOrder.setCostTotalPrice(costTotalPrice.doubleValue());
        projectOrder.setCustomerTotalPrice(customerTotalPrice.doubleValue());
        projectOrder.setTotalPrice(costTotalPrice.add(customerTotalPrice).doubleValue());
        return projectOrder;
    }

    @Override
    public BaseResponse<Void> enableProject(String id, EnableProject request) {
        if (projectRepository.findByPjid(id) == null) {
            return BaseResponse.fail("项目不存在");
        }
        Project project = projectRepository.findByPjid(id);
        project.setEnable(request.getEnable());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            project.setUt(simpleDateFormat.parse(simpleDateFormat.format(new Date())).getTime());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        projectRepository.save(project);
        return BaseResponse.success();
    }
}
