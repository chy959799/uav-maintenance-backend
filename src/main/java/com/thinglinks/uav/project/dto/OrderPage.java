package com.thinglinks.uav.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.thinglinks.uav.common.dto.BasePageResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@JsonSerialize
@NoArgsConstructor
public class OrderPage extends BasePageResponse {
    /**
     * 工单编号
     */
    @ApiModelProperty(value = "内部编号")
    private String innerCode;

    /**
     * 工单服务类型 1:维修 2:保养
     */
    @ApiModelProperty(value = "服务类型")
    private String serviceType;

    /**
     * 工单状态 0: 已作废 1:待派单 2:待接单 3:待收件 4:定损中 5:定损待审批 6:维修中 7:完工待审批 8:待寄件 9:已完成 10:已评价 11:待结算
     */
    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "完成时间")
    private Long finishAt;

    @ApiModelProperty(value = "设备编号")
    @JsonProperty("code")
    private String SNCode;

    @ApiModelProperty(value = "创建部门")
    private String companyName;

    @ApiModelProperty(value = "创建人")
    private String customerName;

    @ApiModelProperty(value = "接单人")
    private String maintainerName;

    @ApiModelProperty(value = "接单部门人")
    private String assistName;

}
