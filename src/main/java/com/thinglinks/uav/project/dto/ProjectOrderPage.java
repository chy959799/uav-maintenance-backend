package com.thinglinks.uav.project.dto;

import com.thinglinks.uav.common.dto.BasePageResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProjectOrderPage<T> extends BasePageResponse<T> {
    @ApiModelProperty(value = "成本总金额")
    private Double costTotalPrice;
    @ApiModelProperty(value = "报价总金额")
    private Double customerTotalPrice;
    @ApiModelProperty(value = "项目总费用")
    private Double totalPrice;
}
