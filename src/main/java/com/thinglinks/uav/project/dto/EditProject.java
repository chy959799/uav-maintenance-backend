package com.thinglinks.uav.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EditProject {
    @ApiModelProperty(value = "内部编号")
    private String innerCode;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "地区")
    private String county;

    @ApiModelProperty(value = "详细地址")
    private String detail;

    @ApiModelProperty(value = "开始时间", required = true)
    private String startTime;

    @ApiModelProperty(value = "结束时间", required = true)
    private String endTime;

    @ApiModelProperty(value = "项目联系电话")
    private String mobile;

    @ApiModelProperty(value = "项目联系人")
    private String contact;

    @ApiModelProperty(value = "项目总金额")
    private double totalPrice;

    @ApiModelProperty(value = "项目备注")
    private String remark;

    @ApiModelProperty(value = "项目描述")
    private String descript;

    @ApiModelProperty(value = "项目状态")
    private Integer enable;

}
