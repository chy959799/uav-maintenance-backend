package com.thinglinks.uav.project.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProjectMaterialConsumptionDetailRequest extends BasePageRequest {
    @ApiModelProperty(value = "项目ID")
    @NotBlank(message = "项目ID不能为空")
    private String pjid;
    @ApiModelProperty(value = "产品ID")
    @NotBlank(message = "产品ID不能为空")
    private String pid;
    @ApiModelProperty(value = "产品分类ID")
    @NotBlank(message = "产品分类ID不能为空")
    private String catid;
}
