package com.thinglinks.uav.project.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ExcelIgnoreUnannotated
public class ProductsUseCondition {
    /**
     * 维修方式
     */
    @ApiModelProperty(value = "维修方式")
    @ExcelProperty("维修方式")
    private String serviceMethod;
    /**
     * 机型
     */
    @ApiModelProperty(value = "机型")
    @ExcelProperty("机型")
    private String categoryName;
    /**
     * 配件名称
     */
    @ApiModelProperty(value = "配件名称")
    @ExcelProperty("配件名称")
    private String productName;
    /**
     * 厂家编号
     */
    @ApiModelProperty(value = "厂家编号")
    @ExcelProperty("厂家编号")
    private String code;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    @ExcelProperty("单位")
    private String unit;
    /**
     * 消耗数量
     */
    @ApiModelProperty(value = "消耗数量")
    @ExcelProperty("消耗数量")
    private Integer count;
    /**
     * 单价
     */
    @ApiModelProperty(value = "单价")
    @ExcelProperty("单价")
    private Double salePrice;
    /**
     * 工时
     */
    @ApiModelProperty(value = "工时")
    @ExcelProperty("工时")
    private Double workHour;
    /**
     * 工时单价
     */
    @ApiModelProperty(value = "工时单价")
    @ExcelProperty("工时单价")
    private Double workHourUnitCost;
    /**
     * 合计
     */
    @ApiModelProperty(value = "合计")
    @ExcelProperty("合计")
    private Double totalCost;
}
