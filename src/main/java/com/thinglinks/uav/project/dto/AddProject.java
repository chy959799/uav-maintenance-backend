package com.thinglinks.uav.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.customer.entity.Customer;
import com.thinglinks.uav.product.entity.Product;
import com.thinglinks.uav.spare.entity.ProjectProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;


@Data
public class AddProject {

    @ApiModelProperty(value = "项目名称", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(value = "项目类型", required = true)
    @NotBlank
    private String type;

    @ApiModelProperty(value = "项目编号", required = true)
    @NotBlank
    private String code;

    @ApiModelProperty(value = "项目内部编号")
    private String innerCode;

    @ApiModelProperty(value = "项目联系人")
    private String contact;

    @ApiModelProperty(value = "项目联系电话")
    private String mobile;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "地区")
    private String county;

    @ApiModelProperty(value = "详细地址")
    private String detail;

    @ApiModelProperty(value = "项目开始时间", required = true)
    @NotBlank
    private String startTime;

    @ApiModelProperty(value = "项目结束时间", required = true)
    @NotBlank
    private String endTime;

    @ApiModelProperty(value = "项目总金额")
    private Long totalPrice;

    @ApiModelProperty(value = "项目备注")
    private String remark;

    @ApiModelProperty(value = "项目描述")
    private String descript;

    @ApiModelProperty(value = "项目状态")
    private Integer enable;

    @ApiModelProperty(value = "所选设备")
    private List<ProjectProduct> projectProductList;

    @ApiModelProperty(value = "单位id")
    private String cid;
}
