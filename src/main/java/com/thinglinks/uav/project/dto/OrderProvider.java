package com.thinglinks.uav.project.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @package: com.thinglinks.uav.project.dto
 * @className: OrderProvider
 * @author: rp
 * @description: TODO
 */
public class OrderProvider {

    public  String findOrdersByManyCondition(Map<String,Object> request){

        StringBuffer sql = new StringBuffer("select o.service_type,o.STATUS,o.finish_at,d.CODE AS SN_code, o.inner_code," +
                "co.`name` AS company_name ,u.user_name AS maintainer_name,u1.user_name AS assist_name ," +
                "cu.`name` AS customer_name from orders o LEFT JOIN devices d ON o.devid = d.devid " +
                "LEFT JOIN customers cu ON o.cid = cu.cid LEFT JOIN companies co ON o.cpid = co.cpid " +
                "LEFT JOIN users u ON u.uid = o.maintainer_uid LEFT JOIN users u1 ON u1.uid = o.assist_uid where pjid="+request.get("pjid")+" ");

        String status , serviceType , order, limit ,offset;
        OrderPageQuery request1 = (OrderPageQuery) request.get("request");
        serviceType = request1.getServiceType();
        status = request1.getStatus();
        order = request1.getOrder();
        limit = String.valueOf( request1.getLimit());
        offset = String.valueOf(request1.getOffset()) ;
        if(StringUtils.isNotEmpty(status)){
            sql.append(" and status= "+status+" ");
        }
        if(StringUtils.isNotEmpty(serviceType)){
            sql.append(" and service_type= "+serviceType+" ");
        }
        sql.append("limit "+limit+" offset "+offset+" ");
        if (StringUtils.isNotEmpty(order)){
            sql.append("order by "+order+" ");
        }
        return sql.toString();
    }
}
