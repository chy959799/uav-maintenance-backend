package com.thinglinks.uav.project.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "删除项目信息DTO")
public class ProjectDTO {
    @ApiModelProperty(value = "pjid数组")
    private List<String> ids;
}
