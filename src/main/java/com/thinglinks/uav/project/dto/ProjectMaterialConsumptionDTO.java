package com.thinglinks.uav.project.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProjectMaterialConsumptionDTO {
    @ApiModelProperty(value = "备件名称")
    @ExcelProperty("备件名称")
    private String productName;

    @ApiModelProperty(value = "厂家编号")
    @ExcelProperty("厂家编号")
    private String productCode;

    @ApiModelProperty(value = "产地")
    @ExcelProperty("产地")
    private String origin;

    @ApiModelProperty(value = "出厂价格")
    @ExcelProperty("出厂价格")
    private BigDecimal factoryPrice;

    @ApiModelProperty(value = "单位")
    @ExcelProperty("单位")
    private String unit;

    @ApiModelProperty(value = "使用频率")
    @ExcelProperty("使用频率")
    private Integer frequency;

    @ApiModelProperty(value = "备件描述")
    @ExcelProperty("备件描述")
    private String description;

    @ApiModelProperty(value = "备件id")
    @ExcelIgnore
    private String pid;

    @ApiModelProperty(value = "备件分类id")
    @ExcelIgnore
    private String catid;
}
