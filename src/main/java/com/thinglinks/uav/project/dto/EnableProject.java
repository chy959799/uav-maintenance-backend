package com.thinglinks.uav.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.project.dto
 * @className: EnableProject
 * @author: rp
 * @description: TODO
 */
@Data
public class EnableProject {

    @ApiModelProperty(value = "项目状态")
    private Boolean enable;
}
