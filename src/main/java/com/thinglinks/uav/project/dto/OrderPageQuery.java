package com.thinglinks.uav.project.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @package: com.thinglinks.uav.project.dto
 * @className: OrderPageQuery
 * @author: rp
 * @description: TODO
 */
@Data
public class OrderPageQuery extends BasePageRequest {

    @ApiModelProperty(value = "工单状态")
    private String status;
    @ApiModelProperty(value = "服务类型")
    private String serviceType;

}
