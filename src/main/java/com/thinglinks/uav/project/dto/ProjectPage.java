package com.thinglinks.uav.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.customer.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 */
@Data
public class ProjectPage{
    @ApiModelProperty(value = "项目id")
    private String pjid;

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "项目类型")
    private String type;

    @ApiModelProperty(value = "项目编号")
    @JsonProperty("code")
    private String projectCode;

    @ApiModelProperty(value = "项目内部编号")
    private String innerCode;

    @ApiModelProperty(value = "项目联系人")
    private String contact;

    @ApiModelProperty(value = "项目联系电话")
    private String mobile;

    @ApiModelProperty(value = "项目省份")
    private String province;

    @ApiModelProperty(value = "项目城市")
    private String city;

    @ApiModelProperty(value = "项目地区")
    private String county;

    @ApiModelProperty(value = "详细地址")
    private String detail;

    @ApiModelProperty(value = "项目开始时间")
    private String startTime;

    @ApiModelProperty(value = "项目结束时间")
    private String endTime;

    @ApiModelProperty(value = "项目总金额")
    private double totalPrice;

    @ApiModelProperty(value = "项目备注")
    private String remark;

    @ApiModelProperty(value = "项目描述")
    private String descript;

    @ApiModelProperty(value = "项目状态")
    private Boolean enable;

    @ApiModelProperty(value = "过期状态")
    private Boolean active;

    @ApiModelProperty(value = "单位")
    private Customer customer;
}
