package com.thinglinks.uav.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProjectMaterialConsumptionDetail {
    @ApiModelProperty(value = "项目名称")
    private String projectName;
    @ApiModelProperty(value = "工单编号")
    private String orderCode;

    @ApiModelProperty(value = "订单类型")
    private String orderType;

    @ApiModelProperty(value = "订单服务类型")
    private String orderServiceType;

    @ApiModelProperty(value = "备件类别")
    private String productCategory;

    @ApiModelProperty(value = "备件名称")
    private String productName;

    @ApiModelProperty(value = "备件编号")
    private String productCode;

    @ApiModelProperty(value = "消耗数量")
    private Integer productCount;

    @ApiModelProperty(value = "项目订单下消耗物料列表")
    private List<ProductsUseCondition> productsUseConditions;
}
