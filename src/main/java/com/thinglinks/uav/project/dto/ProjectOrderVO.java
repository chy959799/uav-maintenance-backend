package com.thinglinks.uav.project.dto;


import com.thinglinks.uav.order.dto.PageOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProjectOrderVO extends PageOrder {
    @ApiModelProperty(value = "成本价")
    private Double costPrice;
    @ApiModelProperty(value = "客户价")
    private Double customerPrice;
    @ApiModelProperty(value = "费用")
    private Double price;
}
