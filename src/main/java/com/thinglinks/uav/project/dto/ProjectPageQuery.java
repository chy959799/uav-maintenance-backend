package com.thinglinks.uav.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProjectPageQuery extends BasePageRequest {
    @ApiModelProperty(value = "项目状态")
    private Boolean enable;

    @ApiModelProperty(value = "项目名称")
    private String name;

    @ApiModelProperty(value = "项目编号")
    private String code;

    @ApiModelProperty(value = "开始时间")
    private String endTimeBefore;

    @ApiModelProperty(value = "结束时间")
    private String endTimeAfter;
}
