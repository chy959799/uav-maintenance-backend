package com.thinglinks.uav.project.dto;


import com.thinglinks.uav.category.entity.Category;
import com.thinglinks.uav.product.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class ProductCount extends Product{
    /**
     * 备件使用次数
     */
    private Integer count;
}
