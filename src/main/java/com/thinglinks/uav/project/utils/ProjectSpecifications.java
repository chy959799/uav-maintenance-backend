package com.thinglinks.uav.project.utils;

import com.alibaba.excel.util.BooleanUtils;
import com.aliyun.credentials.utils.StringUtils;
import com.thinglinks.uav.project.dto.OrderPageQuery;
import com.thinglinks.uav.project.dto.ProjectPageQuery;
import com.thinglinks.uav.project.entity.Project;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class ProjectSpecifications {
    // 假设有一个Project实体类，需要对它的name和code字段进行模糊查询
    public static Specification<Project> searchProject(ProjectPageQuery projectPageQuery) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.isEmpty(projectPageQuery.getName()) &&
                    StringUtils.isEmpty(projectPageQuery.getCode()) &&
                    StringUtils.isEmpty(projectPageQuery.getEndTimeBefore()) &&
                    StringUtils.isEmpty(projectPageQuery.getEndTimeAfter()) &&
                    projectPageQuery.getEnable() == null) {
                return criteriaBuilder.conjunction();
            }
            if (!StringUtils.isEmpty(projectPageQuery.getName())) {
                Path<String> namePath = root.get("name");
                predicates.add(criteriaBuilder.like(namePath, "%" + projectPageQuery.getName() + "%"));
            }

            if (!StringUtils.isEmpty(projectPageQuery.getCode())) {
                Path<String> descPath = root.get("code");
                predicates.add(criteriaBuilder.like(descPath, "%" + projectPageQuery.getCode() + "%"));
            }
            if (!StringUtils.isEmpty(projectPageQuery.getEndTimeBefore()) && !StringUtils.isEmpty(projectPageQuery.getEndTimeAfter())) {
                Path<Long> descPath = root.get("endTime");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String startTime = (projectPageQuery.getEndTimeBefore() + " 00:00:00");
                String endTime = (projectPageQuery.getEndTimeAfter() + " 00:00:00");
                long before;
                long after;
                try {
                    after = (simpleDateFormat.parse(endTime).getTime());
                    before = (simpleDateFormat.parse(startTime).getTime());
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                predicates.add(criteriaBuilder.between(descPath, before, after));
            }
            if (projectPageQuery.getEnable() != null) {
                Path<Integer> descPath = root.get("enable");
                predicates.add(criteriaBuilder.equal(descPath, BooleanUtils.isTrue(projectPageQuery.getEnable()) ? 1 : 0));
            }
            // 如果有多个条件，则将所有Predicate通过and或or连接起来
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

}
