package com.thinglinks.uav.project.task;

import com.thinglinks.uav.project.entity.Project;
import com.thinglinks.uav.project.repository.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @package: com.thinglinks.uav.common.task
 * @className: ProjectTask
 * @author: HHG
 */
@Component
@Slf4j
public class ProjectTask {

    @Resource
    private ProjectRepository projectRepository;

    @Scheduled(cron = "0 0 0 * * ?")
    public void doTask(){
        List<Project> projectList = projectRepository.findByActive(Boolean.TRUE);
        projectList.forEach(project -> {
            if (project.getEndTime() < System.currentTimeMillis()){
                project.setActive(Boolean.FALSE);
                projectRepository.save(project);
                log.info("项目{}已过期", project);
            }
        });
    }
}
