package com.thinglinks.uav.project.repository;

import com.thinglinks.uav.project.entity.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @package: com.thinglinks.uav.projects.dao
 * @className: ProjectesDao
 * @author: rp
 * @description: TODO
 */
@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> , JpaSpecificationExecutor<Project> {
    Page<Project> findAll(Specification<Project> spec, Pageable pageable);
    Project findByPjid(String pjid);
    Project findByCode(String code);
    void deleteByPjid(String pjid);
    Boolean existsByPjidIn(List<String> ids);

    void deleteAllByPjidIn(List<String> ids);

    Boolean existsByPjid(String id);

    @Query("SELECT p FROM Project p WHERE p.endTime >= :now")
    List<Project> findActiveProjects(@Param("now") Long now);

    @Query("SELECT p FROM Project p WHERE p.endTime < :now")
    List<Project> findExpiredProjects(@Param("now") Long now);

    List<Project> findByCtBetween(Long startTime, Long endTime);
    List<Project> findByActive(Boolean active);
}
