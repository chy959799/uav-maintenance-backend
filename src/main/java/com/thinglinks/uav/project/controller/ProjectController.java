package com.thinglinks.uav.project.controller;

import com.thinglinks.uav.common.dto.BasePageRequest;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.project.dto.*;
import com.thinglinks.uav.project.service.ProjectService;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * @package: com.thinglinks.uav.projects.controller
 * @className: ProjectsConreoller
 * @author: rp
 * @description: TODO
 */
@Api(tags = "项目管理")
@RestController
@RequestMapping(value = "/projects")
@Slf4j
public class ProjectController {
    @Resource
    private ProjectService projectService;

    @Authority(action = "projectAdd")
    @PostMapping
    @Operation(summary = "新建项目")
    public BaseResponse<Void> addProject(@RequestBody @Valid AddProject request) {
        return projectService.addProject(request);
    }

    @Authority(action = "projectDelete")
    @DeleteMapping(path = "{ID}")
    @Operation(summary = "删除项目")
    public BaseResponse<Void> deleteProject(@PathVariable("ID") String id) {
        return projectService.deleteProject(id);
    }

    @Authority(action = "projectDelete")
    @DeleteMapping("")
    @Operation(summary = "批量删除项目")
    public BaseResponse<Void> deleteCompanies(@RequestBody @Valid ProjectDTO pjids) {
        return projectService.deleteProjects(pjids);
    }

    @Authority(action = "projectEdit")
    @PatchMapping(path = "{ID}")
    @Operation(summary = "更新项目")
    public BaseResponse<Void> editProject(@PathVariable("ID") String id, @RequestBody @Valid EditProject request) {
        return projectService.editProject(id, request);
    }

    @Authority(action = "projectEnable")
    @PatchMapping(path = "{ID}/enable")
    @Operation(summary = "更新状态")
    public BaseResponse<Void> enableProject(@PathVariable("ID") String id, @RequestBody @Valid EnableProject request) {
        return projectService.enableProject(id, request);
    }
    @GetMapping(path = "{ID}")
    @Operation(summary = "获取项目详情")
    public BaseResponse<ProjectDetail> getProjectDetail(@PathVariable("ID") String id) {
        return projectService.getProjectDetail(id);
    }

    @GetMapping
    @Operation(summary = "根据条件获取项目列表")
    public BasePageResponse<ProjectPage>  getProjects(ProjectPageQuery request) {
        return projectService.queryProjects(request);
    }

    @GetMapping(path = "{ID}/getOrders")
    @Operation(summary = "获取项目工单")
    public ProjectOrderPage<ProjectOrderVO> getOrders(@PathVariable("ID") String id, OrderPageQuery request) {
        return projectService.getOrders(id, request);
    }
    @Operation(summary = "项目工单信息导出")
    @GetMapping(path = "orderExport/{ID}")
    public void exportOrder(HttpServletResponse response, @PathVariable("ID") String id, OrderPageQuery request) throws Exception{
        projectService.exportOrder(response,id, request);
    }
    @Operation(summary = "项目维修消耗物料统计")
    @GetMapping(path = "statistics/{ID}")
    public BasePageResponse<ProjectMaterialConsumptionDTO> statistics(@PathVariable("ID") String id, BasePageRequest request){
        return projectService.statisticsProductsUseCondition(id,request);
    }
    @Operation(summary = "维修消耗的物料导出")
    @GetMapping(path = "conditionExport/{ID}")
    public void conditionExport(HttpServletResponse response, @PathVariable("ID") String id) throws Exception{
        projectService.exportCondition(response,id);
    }
    @Operation(summary = "项目维修消耗物料统计详情")
    @GetMapping(path = "statisticsProductCount")
    public BasePageResponse<ProjectMaterialConsumptionDetail> statisticsProductCount(@Valid ProjectMaterialConsumptionDetailRequest request) {
        return projectService.statisticsProductCount(request);
    }

}
