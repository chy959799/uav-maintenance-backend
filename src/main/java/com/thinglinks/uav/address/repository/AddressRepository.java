package com.thinglinks.uav.address.repository;

import com.thinglinks.uav.address.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * (Addresses)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-22 11:29:54
 */
public interface AddressRepository extends JpaRepository<Address, Integer>, JpaSpecificationExecutor<Address> {


    void deleteByAid(String aid);

    void deleteAllByAidIn(List<String> ids);

    Address getByAid(String aid);

    Boolean existsByCidAndIsDefault(String cid, Boolean isDefault);

    Address getByCidAndIsDefault(String cid, Boolean isDefault);

    Address getByIsDefaultAndCid(Boolean isDefault, String cid);

    Address getByCid(String cid);

    Address findByCidAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(String cid, String tel, String name, String province, String city, String county, String detail);


    Address findByCidIsNullAndContactTelAndContactUserAndProvinceAndCityAndCountyAndDetail(String tel, String name, String province, String city, String county, String detail);

    boolean existsByCid(String cid);

    boolean existsByCidIn(List<String> ids);
}


