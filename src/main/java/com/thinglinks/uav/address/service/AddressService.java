package com.thinglinks.uav.address.service;

import com.thinglinks.uav.address.dto.AddressDTO;
import com.thinglinks.uav.address.dto.AddressPageRequest;
import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.address.vo.AddressVO;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.customer.dto.CustomerAddressRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * (Addresses)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-22 11:29:54
 */
public interface AddressService {
	BaseResponse<String> insert(String cid, AddressDTO customer);
	@Transactional
	BaseResponse<String> deleteByAid(String aid);

	@Transactional
	BaseResponse<String> deleteAll(List<String> dto);

	@Transactional
	BaseResponse<String> enable(String aid);

	@Transactional
	BaseResponse<String> update(String cid, String aid);

	@Transactional
	BaseResponse<String> update(String aid,AddressDTO dto);

	Address getDefaultAddress(String cid);

	BasePageResponse<AddressVO> getPage(String cid, CustomerAddressRequest pageQuery);

	BasePageResponse<AddressVO> getPage(AddressPageRequest request);

	BaseResponse<String> insert(AddressDTO dto);

	BaseResponse<AddressVO> getDefault(String cid);
}


