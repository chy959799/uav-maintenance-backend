package com.thinglinks.uav.address.service.impl;

import com.thinglinks.uav.address.dto.AddressDTO;
import com.thinglinks.uav.address.dto.AddressPageRequest;
import com.thinglinks.uav.address.entity.Address;
import com.thinglinks.uav.address.repository.AddressRepository;
import com.thinglinks.uav.address.service.AddressService;
import com.thinglinks.uav.address.vo.AddressVO;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.common.utils.DateUtils;
import com.thinglinks.uav.customer.dto.CustomerAddressRequest;
import com.thinglinks.uav.order.repository.OrderRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (Addresses)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-22 11:29:55
 */
@Service
public class AddressServiceImpl implements AddressService {

	@Resource
	private AddressRepository addressRepository;
	@Resource
	private OrderRepository orderRepository;

	@Override
	public BaseResponse<String> insert(String cid, AddressDTO dto) {
		Address address = new Address();
		BeanUtils.copyProperties(dto, address);
		address.setAid(CommonUtils.uuid());
		address.setCid(cid);
		if (addressRepository.existsByCidAndIsDefault(cid, true)) {
			address.setIsDefault(false);
		}else {
			address.setIsDefault(true);
		}
		addressRepository.save(address);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> deleteByAid(String aid) {
		if (orderRepository.existsByAid(aid)) {
			return BaseResponse.fail("该工单下还有地址，请先删除该用户下的地址");
		}
		addressRepository.deleteByAid(aid);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> deleteAll(List<String> ids) {
		addressRepository.deleteAllByAidIn(ids);
		for (String id : ids){
			if (orderRepository.existsByAid(id)) {
				return BaseResponse.fail("该工单下还有地址，请先删除该用户下的地址");
			}
		}
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> enable(String aid) {
		Address address = addressRepository.getByAid(aid);
		Address oldAddress = addressRepository.getByIsDefaultAndCid(true,null);
		System.out.println(oldAddress);
		if (oldAddress != null) {
			Address add = new Address();
			oldAddress.setIsDefault(false);
			BeanUtils.copyProperties(oldAddress,add );
			addressRepository.save(add);
		}
		if (address != null) {
			Address add = new Address();
			address.setIsDefault(true);
			BeanUtils.copyProperties(address, add);
			addressRepository.save(address);
			return BaseResponse.success();
		} else {
			return BaseResponse.fail("地址信息不存在");
		}
	}

	@Override
	public BaseResponse<String> update(String cid, String aid) {
		Address defaultAddress = getDefaultAddress(cid);

		if (!Objects.isNull(defaultAddress) && defaultAddress.getAid().equals(aid)) {
			return BaseResponse.success();
		}

		List<Address> addressList = new ArrayList<>();

		if (!Objects.isNull(defaultAddress)) {
			defaultAddress.setIsDefault(false);
			addressList.add(defaultAddress);
		}

        Address address = addressRepository.getByAid(aid);
        if (address != null) {
            address.setIsDefault(true);
            addressList.add(address);
            addressRepository.saveAll(addressList);
            return BaseResponse.success();
        } else {
            return BaseResponse.fail("地址信息不存在");
        }
    }

    @Override
    public BaseResponse<String> update(String aid, AddressDTO dto) {
        Address add = addressRepository.getByAid(aid);
        if (add != null) {
            BeanUtils.copyProperties(dto, add);
            addressRepository.save(add);
            return BaseResponse.success();
        }
        return BaseResponse.fail("地址信息不存在");
    }

	@Override
	public Address getDefaultAddress(String cid) {
		return addressRepository.getByCidAndIsDefault(cid, true);
	}


	@Override
	public BasePageResponse<AddressVO> getPage(String cid, CustomerAddressRequest pageQuery) {

		Specification<Address> specification = (root, query, builder) -> {
			Predicate predicate = builder.conjunction();
			predicate = builder.and(predicate, builder.equal(root.get("cid"), cid));
			if (pageQuery.getIsDefault()!=null){
				predicate = builder.and(predicate, builder.equal(root.get("default"), pageQuery.getIsDefault()));
			}
			return predicate;
		};
		Page<Address> addressPage = addressRepository.findAll(specification, pageQuery.of());
		List<AddressVO> customerVOList = addressPage.getContent().stream()
				.map(this::convertToVO)
				.collect(Collectors.toList());

		return BasePageResponse.success(addressPage, customerVOList);
	}

	@Override
	public BasePageResponse<AddressVO> getPage(AddressPageRequest request) {
		PageRequest pageRequest = request.of();
		Specification<Address> specification = (root, query, builder) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (Objects.nonNull(request.getCid())){
				predicates.add(builder.equal(root.get("cid"), request.getCid()));
			}else {
				if (Objects.nonNull(request.getIsSystem()) && Boolean.parseBoolean(request.getIsSystem())){
					predicates.add(builder.isNull(root.get("cid")));
				}
			}
			if (Objects.nonNull(request.getIsDefault())){
				predicates.add(builder.equal(root.get("isDefault"), Boolean.valueOf(request.getIsDefault())));
			}
			return builder.and(predicates.toArray(new Predicate[0]));
		};
		Page<Address> page = addressRepository.findAll(specification, pageRequest);
		List<AddressVO> addressPages = page.stream().map(c -> {
			AddressVO addressVO = new AddressVO();
			BeanUtils.copyProperties(c, addressVO);
			addressVO.setCreatedAt(DateUtils.formatDateTime(c.getCt()));
			addressVO.setUpdatedAt(DateUtils.formatDateTime(c.getUt()));
			return addressVO;
		}).collect(Collectors.toList());
		return BasePageResponse.success(page, addressPages);
	}

    @Override
    public BaseResponse<String> insert(AddressDTO dto) {
        Address address = new Address();
        BeanUtils.copyProperties(dto, address);
        address.setAid(CommonUtils.uuid());
        if (addressRepository.existsByCid(null)) {
            address.setIsDefault(false);
            addressRepository.save(address);
            return BaseResponse.success();
        }
        address.setIsDefault(true);
        addressRepository.save(address);
        return BaseResponse.success();
    }

	@Override
	public BaseResponse<AddressVO> getDefault(String cid) {
		if (addressRepository.existsByCidAndIsDefault(cid, true)) {
			return BaseResponse.success(convertToVO(addressRepository.getByCidAndIsDefault(cid, true)));
		}
		return BaseResponse.fail("地址信息不存在");
	}

	private AddressVO convertToVO(Address address) {
		AddressVO addressVO = new AddressVO();
		BeanUtils.copyProperties(address, addressVO);
		addressVO.setCreatedAt(DateUtils.formatDateTime(address.getCt()));
		addressVO.setUpdatedAt(DateUtils.formatDateTime(address.getUt()));
		return addressVO;
	}
}


