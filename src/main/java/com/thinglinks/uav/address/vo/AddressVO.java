package com.thinglinks.uav.address.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author iwiFool
 * @date 2024/3/22
 */
@Data
@ApiModel(description = "收货地址VO")
public class AddressVO {

	@ApiModelProperty(value = "aid", example = "1")
	private String aid;

	@ApiModelProperty(value = "联系人姓名", example = "1")
	private String contactUser;

	@ApiModelProperty(value = "联系人电话", example = "1")
	private String contactTel;

	@ApiModelProperty(value = "省编码", example = "1")
	private String province;

	@ApiModelProperty(value = "市编码", example = "1")
	private String city;

	@ApiModelProperty(value = "区编码", example = "1")
	private String county;

	@ApiModelProperty(value = "详细地址", example = "1")
	private String detail;

	@ApiModelProperty(value = "是否默认地址", example = "1")
	@JsonProperty("default")
	private Boolean isDefault;

	@ApiModelProperty(value = "创建时间", example = "1")
	private String createdAt;

	@ApiModelProperty(value = "更新时间", example = "1")
	private String updatedAt;

	@ApiModelProperty(value = "cid", example = "1")
	private String cid;

	private String areaName;
}
