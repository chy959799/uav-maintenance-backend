package com.thinglinks.uav.address.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinglinks.uav.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * (Addresses)实体类
 *
 * @author iwiFool
 * @since 2024-03-22 11:29:55
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "addresses")
public class Address extends BaseEntity {
	/**
	 * 收货地址内部ID
	 */
	@Column(name ="aid")
	private String aid;
	/**
	 * 联系人姓名
	 */
	@Column(name ="contact_user")
	private String contactUser;
	/**
	 * 联系人电话
	 */
	@Column(name ="contact_tel")
	private String contactTel;
	/**
	 * 省编码,非空
	 */
	@Column(name ="province")
	private String province;
	/**
	 * 市编码,非空
	 */
	@Column(name ="city")
	private String city;
	/**
	 * 区编码,非空
	 */
	@Column(name ="county")
	private String county;
	/**
	 * 详细地址,非空
	 */
	@Column(name ="detail")
	private String detail;
	/**
	 * 是否默认地址,默认为否
	 */
	@Column(name = "`default`")
	@JsonProperty(value = "default")
	private Boolean isDefault;

	/**
	 * 客户ID
	 */
	@Column(name ="cid")
	private String cid;
}
