package com.thinglinks.uav.address.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author iwiFool
 * @date 2024/3/22
 */
@ApiModel(description = "收货地址DTO")
@Data
public class AddressDTO {

	@ApiModelProperty(value = "联系人,非空", example = "这是联系人")
	@NotBlank
	private String contactUser;

	@ApiModelProperty(value = "联系电话,非空", example = "13896120331")
	@Pattern(regexp = "^1\\d{10}$", message = "联系电话格式不正确")
	private String contactTel;

	@ApiModelProperty(value = "省编码,非空", example = "540000")
	@NotBlank
	private String province;

	@ApiModelProperty(value = "市编码,非空", example = "540001")
	@NotBlank
	private String city;

	@ApiModelProperty(value = "区编码,非空", example = "540002")
	@NotBlank
	private String county;

	@ApiModelProperty(value = "详细地址,非空", example = "这是详细地址")
	@NotBlank
	private String detail;
}
