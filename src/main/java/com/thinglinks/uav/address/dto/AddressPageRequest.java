package com.thinglinks.uav.address.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author huiyongchen
 * @version 1.0
 * @description: TODO
 * @date 2024/4/8 10:31
 */
@Data
public class AddressPageRequest extends BasePageRequest {
    @ApiModelProperty(value = "单位id")
    private String cid;

    @ApiModelProperty(value = "是否默认地址")
    private String isDefault;

    @ApiModelProperty(value = "是否系统地址")
    private String isSystem;
}
