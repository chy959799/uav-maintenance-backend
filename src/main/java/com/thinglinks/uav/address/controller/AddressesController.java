package com.thinglinks.uav.address.controller;

import com.thinglinks.uav.address.dto.AddressDTO;
import com.thinglinks.uav.address.dto.AddressPageRequest;
import com.thinglinks.uav.address.service.AddressService;
import com.thinglinks.uav.address.vo.AddressVO;
import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.dto.DeleteDTO;
import com.thinglinks.uav.system.annotation.Authority;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * (Addresses)表控制层
 *
 * @author iwiFool
 * @since 2024-03-22 11:29:54
 */
@Api(tags = "收货地址")
@RestController
@RequestMapping("address")
public class AddressesController {

	@Resource
	private AddressService addressService;

	@Authority(action = "addressManagementDelete")
	@DeleteMapping("/{aid}")
	@Operation(summary = "删除地址")
	public BaseResponse<String> delete(@PathVariable("aid") String aid) {
		return addressService.deleteByAid(aid);
	}

	@Authority(action = "addressManagementDelete")
	@DeleteMapping
	@Operation(summary = "批量删除地址")
	public BaseResponse<String> deleteAll(@RequestBody DeleteDTO dto) {
		return addressService.deleteAll(dto.getIds());
	}

	@GetMapping
	@Operation(summary = "获取地址列表")
	public BasePageResponse<AddressVO> getPage(AddressPageRequest request) {
		return addressService.getPage(request);
	}

	@Authority(action = "addressManagementAdd")
	@PostMapping
	@Operation(summary = "新增地址")
	public BaseResponse<String> insert(@RequestBody @Valid AddressDTO dto) {
		return addressService.insert(dto);
	}

	@Authority(action = "addressManagementEdit")
	@PatchMapping("/{aid}")
	@Operation(summary = "修改地址")
	public BaseResponse<String> update(@PathVariable("aid") String aid, @RequestBody @Valid AddressDTO dto) {
		return addressService.update(aid, dto);
	}

	@Authority(action = "addressManagementDefault")
	@PatchMapping("/enable/{aid}")
	@Operation(summary = "修改默认地址")
	public BaseResponse<String> enable(@PathVariable("aid") String aid) {
		return addressService.enable(aid);
	}
}


