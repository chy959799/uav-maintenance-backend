package com.thinglinks.uav.material.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author iwiFool
 * @date 2024/3/26
 */
@Data
@ApiModel(description = "资料DTO")
public class MaterialDTO {

	@ApiModelProperty(value = "标题", example = "这是标题3")
	@NotBlank
	private String title;

	@ApiModelProperty(value = "内容", example = "这是内容")
	@NotBlank
	private String content;

	@ApiModelProperty(value = "catid", example = "839910b7-2b1d-4bf1-88b4-4e8dbc804b55")
	@NotBlank
	private String catid;

	@ApiModelProperty(value = "文件")
	List<MultipartFile> file;
}
