package com.thinglinks.uav.material.dto;

import com.thinglinks.uav.common.dto.BasePageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author iwiFool
 * @date 2024/3/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MaterialPageQuery extends BasePageRequest {


}
