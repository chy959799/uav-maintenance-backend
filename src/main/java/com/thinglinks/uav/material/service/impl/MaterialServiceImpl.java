package com.thinglinks.uav.material.service.impl;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.common.utils.CommonUtils;
import com.thinglinks.uav.file.service.FileService;
import com.thinglinks.uav.material.dto.MaterialDTO;
import com.thinglinks.uav.material.dto.MaterialPageQuery;
import com.thinglinks.uav.material.entity.Material;
import com.thinglinks.uav.material.repository.MaterialsRepository;
import com.thinglinks.uav.material.service.MaterialService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * (Materials)表服务实现类
 *
 * @author iwiFool
 * @since 2024-03-26 18:41:35
 */
@Service
public class MaterialServiceImpl implements MaterialService {

	@Resource
	private MaterialsRepository materialsRepository;

	@Resource
	private FileService fileService;

	@Override
	public BaseResponse<String> insert(MaterialDTO dto) {
		Material material = new Material();
		BeanUtils.copyProperties(dto, material);
		material.setMaterid(CommonUtils.uuid());
		material.setCpid(CommonUtils.getCpid());
		material.setUid(CommonUtils.getUid());
		return uploadFile(dto.getFile(), material);
	}

	private BaseResponse<String> uploadFile(List<MultipartFile> file, Material material) {
		materialsRepository.save(material);
		for (MultipartFile multipartFile : file) {
			fileService.insertMaterialFile(multipartFile, material.getMaterid());
		}
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> delete(String materid) {
		fileService.deleteByMaterid(materid);
		materialsRepository.deleteByMaterid(materid);
		return BaseResponse.success();
	}

	@Override
	public BaseResponse<String> update(String materid, MaterialDTO dto) {
		Material material = materialsRepository.findByMaterid(materid);
		if (Objects.isNull(material)) {
			return BaseResponse.fail("该故障不存在");
		}
		BeanUtils.copyProperties(dto, material);

		fileService.deleteByMaterid(material.getMaterid());
		return uploadFile(dto.getFile(), material);
	}

	@Override
	public BasePageResponse<Material> getMaterialPage(MaterialPageQuery pageQuery) {
		return null;
	}
}


