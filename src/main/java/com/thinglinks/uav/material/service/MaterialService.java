package com.thinglinks.uav.material.service;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.material.dto.MaterialDTO;
import com.thinglinks.uav.material.dto.MaterialPageQuery;
import com.thinglinks.uav.material.entity.Material;

import javax.transaction.Transactional;

/**
 * (Materials)表服务接口
 *
 * @author iwiFool
 * @since 2024-03-26 18:41:34
 */
public interface MaterialService {

	BaseResponse<String> insert(MaterialDTO dto);

	@Transactional
	BaseResponse<String> delete(String materid);

	BaseResponse<String> update(String materid, MaterialDTO dto);

	BasePageResponse<Material> getMaterialPage(MaterialPageQuery pageQuery);
}


