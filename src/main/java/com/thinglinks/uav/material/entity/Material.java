package com.thinglinks.uav.material.entity;

import com.thinglinks.uav.common.entity.BaseEntity;
import com.thinglinks.uav.system.annotation.TenantTable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * (Materials)实体类
 *
 * @author iwiFool
 * @since 2024-03-26 18:41:35
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "materials")
@Data
@TenantTable
public class Material extends BaseEntity {

	/**
	 * 资料内部ID
	 */
	private String materid;
	/**
	 * 资料标题
	 */
	private String title;
	/**
	 * 资料内容
	 */
	private String content;

	private String cpid;

	private String uid;

	private String catid;
}
