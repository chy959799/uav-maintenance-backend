package com.thinglinks.uav.material.controller;

import com.thinglinks.uav.common.dto.BasePageResponse;
import com.thinglinks.uav.common.dto.BaseResponse;
import com.thinglinks.uav.material.dto.MaterialDTO;
import com.thinglinks.uav.material.dto.MaterialPageQuery;
import com.thinglinks.uav.material.entity.Material;
import com.thinglinks.uav.material.service.MaterialService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * (Materials)表控制层
 *
 * @author iwiFool
 * @since 2024-03-26 18:41:34
 */
@Api(tags = "资料管理")
@RestController
@RequestMapping("materials")
public class MaterialController {

	@Resource
	private MaterialService materialService;

	@Operation(summary = "新增资料")
	@PostMapping
	public BaseResponse<String> addMaterial(@Valid MaterialDTO dto) {
		return materialService.insert(dto);
	}

	@Operation(summary = "删除资料")
	@DeleteMapping("{materid}")
	public BaseResponse<String> deleteMaterial(@PathVariable String materid) {
		return materialService.delete(materid);
	}

	@Operation(summary = "修改资料")
	@PatchMapping("{materid}")
	public BaseResponse<String> updateMaterial(@PathVariable String materid, @RequestBody @Valid MaterialDTO dto) {
		return materialService.update(materid, dto);
	}

	@Operation(summary = "资料列表")
	@GetMapping
	public BasePageResponse<Material> getMaterialPage(MaterialPageQuery pageQuery) {
		return materialService.getMaterialPage(pageQuery);
	}

}


