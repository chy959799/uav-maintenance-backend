package com.thinglinks.uav.material.repository;

import com.thinglinks.uav.material.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * (Materials)表数据库访问层
 *
 * @author iwiFool
 * @since 2024-03-26 18:41:34
 */
public interface MaterialsRepository extends JpaRepository<Material, Integer>, JpaSpecificationExecutor<Material> {


	void deleteByMaterid(String materid);

	Material findByMaterid(String materid);
}


